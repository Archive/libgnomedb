/* gnome-db-canvas-table.c
 *
 * Copyright (C) 2002 - 2007 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <gtk/gtk.h>
#include <libgda/libgda.h>
#include "gnome-db-canvas.h"
#include "gnome-db-canvas-table.h"
#include "gnome-db-canvas-column.h"
#include <glib/gi18n-lib.h>

static void gnome_db_canvas_table_class_init (GnomeDbCanvasTableClass * class);
static void gnome_db_canvas_table_init       (GnomeDbCanvasTable * drag);
static void gnome_db_canvas_table_dispose    (GObject *object);
static void gnome_db_canvas_table_finalize   (GObject *object);

static void gnome_db_canvas_table_set_property (GObject *object,
					     guint param_id,
					     const GValue *value,
					     GParamSpec *pspec);
static void gnome_db_canvas_table_get_property (GObject *object,
					     guint param_id,
					     GValue *value,
					     GParamSpec *pspec);

enum
{
	PROP_0,
	PROP_TABLE,
	PROP_MENU_FUNC
};

struct _GnomeDbCanvasTablePrivate
{
	GdaMetaTable       *table;

	/* UI building information */
        GSList             *column_items; /* list of GooCanvasItem for the columns */
	GSList             *other_items; /* list of GooCanvasItem for other purposes */
	gdouble            *column_ypos; /* array for each column's Y position in this canvas group */
	GtkWidget          *(*popup_menu_func) (GnomeDbCanvasTable *ce);

	/* presentation parameters */
        gdouble             x_text_space;
        gdouble             y_text_space;
};

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *table_parent_class = NULL;

GType
gnome_db_canvas_table_get_type (void)
{
	static GType type = 0;

        if (G_UNLIKELY (type == 0)) {
		static const GTypeInfo info = {
			sizeof (GnomeDbCanvasTableClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_canvas_table_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbCanvasTable),
			0,
			(GInstanceInitFunc) gnome_db_canvas_table_init
		};		

		type = g_type_register_static (GNOME_DB_TYPE_CANVAS_ITEM, "GnomeDbCanvasTable", &info, 0);
	}

	return type;
}

	
static void
gnome_db_canvas_table_class_init (GnomeDbCanvasTableClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	table_parent_class = g_type_class_peek_parent (class);

	object_class->dispose = gnome_db_canvas_table_dispose;
	object_class->finalize = gnome_db_canvas_table_finalize;

	/* Properties */
	object_class->set_property = gnome_db_canvas_table_set_property;
	object_class->get_property = gnome_db_canvas_table_get_property;

	g_object_class_install_property
                (object_class, PROP_TABLE,
                 g_param_spec_pointer ("table", NULL, NULL,
				       (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	g_object_class_install_property 
		(object_class, PROP_MENU_FUNC,
                 g_param_spec_pointer ("popup_menu_func", "Popup menu function", 
				       "Function to create a popup menu on each GnomeDbCanvasTable", 
				       G_PARAM_WRITABLE));
}

static gboolean button_press_event_cb (GnomeDbCanvasTable *ce, GooCanvasItem *target_item, GdkEventButton *event,
				       gpointer unused_data);

static void
gnome_db_canvas_table_init (GnomeDbCanvasTable *table)
{
	table->priv = g_new0 (GnomeDbCanvasTablePrivate, 1);
	table->priv->table = NULL;
	table->priv->column_ypos = NULL;
	table->priv->popup_menu_func = NULL;

	table->priv->x_text_space = 3.;
	table->priv->y_text_space = 3.;

	g_signal_connect (G_OBJECT (table), "button-press-event",
			  G_CALLBACK (button_press_event_cb), NULL);
}

static void clean_items (GnomeDbCanvasTable *ce);
static void create_items (GnomeDbCanvasTable *ce);

static void
gnome_db_canvas_table_dispose (GObject *object)
{
	GnomeDbCanvasTable *ce;

	g_return_if_fail (GNOME_DB_IS_CANVAS_TABLE (object));

	ce = GNOME_DB_CANVAS_TABLE (object);

	/* REM: let the GooCanvas library destroy the items itself */
	ce->priv->table = NULL;

	/* for the parent class */
	table_parent_class->dispose (object);
}


static void
gnome_db_canvas_table_finalize (GObject   * object)
{
	GnomeDbCanvasTable *ce;
	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOME_DB_IS_CANVAS_TABLE (object));

	ce = GNOME_DB_CANVAS_TABLE (object);
	if (ce->priv) {
		g_slist_free (ce->priv->column_items);
		g_slist_free (ce->priv->other_items);
		if (ce->priv->column_ypos)
			g_free (ce->priv->column_ypos);

		g_free (ce->priv);
		ce->priv = NULL;
	}

	/* for the parent class */
	table_parent_class->finalize (object);
}

static void 
gnome_db_canvas_table_set_property (GObject *object,
				 guint param_id,
				 const GValue *value,
				 GParamSpec *pspec)
{
	GnomeDbCanvasTable *ce = NULL;

	ce = GNOME_DB_CANVAS_TABLE (object);

	switch (param_id) {
	case PROP_TABLE: {
		GdaMetaTable *table;
		table = g_value_get_pointer (value);
		if (table && (table == ce->priv->table))
			return;

		if (ce->priv->table) {
			ce->priv->table = NULL;
			clean_items (ce);
		}

		if (table) {
			ce->priv->table = (GdaMetaTable*) table;
			create_items (ce);
		}
		break;
	}
	case PROP_MENU_FUNC:
		ce->priv->popup_menu_func = (GtkWidget *(*) (GnomeDbCanvasTable *ce)) g_value_get_pointer (value);
		break;
	}
}

static void 
gnome_db_canvas_table_get_property (GObject *object,
				 guint param_id,
				 GValue *value,
				 GParamSpec *pspec)
{
	TO_IMPLEMENT;
}

/* 
 * destroy any existing GooCanvasItem obejcts 
 */
static void 
clean_items (GnomeDbCanvasTable *ce)
{
	GSList *list;
	/* destroy all the items in the group */
	while (ce->priv->column_items)
		g_object_unref (G_OBJECT (ce->priv->column_items->data));

	for (list = ce->priv->other_items; list; list = list->next)
		g_object_unref (G_OBJECT (list->data));
	g_slist_free (ce->priv->other_items);
	ce->priv->other_items = NULL;

	/* free the columns positions */
	if (ce->priv->column_ypos) {
		g_free (ce->priv->column_ypos);
		ce->priv->column_ypos = NULL;
	}
}

/*
 * create new GooCanvasItem objects
 */
static void 
create_items (GnomeDbCanvasTable *ce)
{
	GooCanvasItem *item, *frame, *title;
        gdouble y, ysep;
#define Y_PAD 0.
#define X_PAD 3.
#define RADIUS_X 5.
#define RADIUS_Y 5.
#define MIN_HEIGHT 75.
        GooCanvasBounds border_bounds;
        GooCanvasBounds bounds;
	const gchar *cstr;
	gchar *tmpstr = NULL;
	GSList *columns, *list;
	gint column_nb;
	gdouble column_width;

	clean_items (ce);
	g_assert (ce->priv->table);

        /* title */
	cstr = NULL;
	cstr = GDA_META_DB_OBJECT (ce->priv->table)->obj_short_name;
	if (cstr)
		tmpstr = g_markup_printf_escaped ("<b>%s</b>", cstr);
	else
		tmpstr = g_strdup_printf ("<b>%s</b>", _("No name"));	

	y = RADIUS_Y;
        title = goo_canvas_text_new  (GOO_CANVAS_ITEM (ce), tmpstr, RADIUS_X + X_PAD, y, 
				      -1, GTK_ANCHOR_NORTH_WEST, 
				      "use-markup", TRUE, NULL);

	g_free (tmpstr);
        goo_canvas_item_get_bounds (title, &bounds);
        border_bounds = bounds;
        border_bounds.x1 = 0.;
        border_bounds.y1 = 0.;
        y += bounds.y2 - bounds.y1 + Y_PAD;

	/* separator's placeholder */
        ysep = y;
        y += Y_PAD;

	/* columns' vertical position */
	columns = ce->priv->table->columns;
	ce->priv->column_ypos = g_new0 (gdouble, g_slist_length (columns) + 1);

	/* columns */
	for (column_nb = 0, list = columns; list; list = list->next, column_nb++) {
		ce->priv->column_ypos [column_nb] = y;
		item = gnome_db_canvas_column_new (GOO_CANVAS_ITEM (ce),
						GDA_META_TABLE_COLUMN (list->data),
						X_PAD, ce->priv->column_ypos [column_nb], NULL);
		ce->priv->column_items = g_slist_append (ce->priv->column_items, item);
		
		goo_canvas_item_get_bounds (item, &bounds);
		border_bounds.x1 = MIN (border_bounds.x1, bounds.x1);
                border_bounds.x2 = MAX (border_bounds.x2, bounds.x2);
                border_bounds.y1 = MIN (border_bounds.y1, bounds.y1);
                border_bounds.y2 = MAX (border_bounds.y2, bounds.y2);

                y += bounds.y2 - bounds.y1 + Y_PAD;
	}
	if (!columns && (border_bounds.y2 - border_bounds.y1 < MIN_HEIGHT))
		border_bounds.y2 += MIN_HEIGHT - (border_bounds.y2 - border_bounds.y1);

	/* border */
	column_width = border_bounds.x2 - border_bounds.x1;
        border_bounds.y2 += RADIUS_Y;
        border_bounds.x2 += RADIUS_X;
        frame = goo_canvas_rect_new (GOO_CANVAS_ITEM (ce), border_bounds.x1, border_bounds.y1, 
				     border_bounds.x2, border_bounds.y2,
				    "radius-x", RADIUS_X, "radius-y", RADIUS_Y,
				    "fill-color", "white", NULL);		
	ce->priv->other_items = g_slist_prepend (ce->priv->other_items, frame);

	/* separator */
        item = goo_canvas_polyline_new_line (GOO_CANVAS_ITEM (ce), border_bounds.x1, ysep, border_bounds.x2, ysep,
					     "close-path", FALSE,
					     "line-width", .7, NULL);
	ce->priv->other_items = g_slist_prepend (ce->priv->other_items, item);

	goo_canvas_item_lower (frame, NULL);

	/* setting the columns' background width to be the same for all */
	for (list = ce->priv->column_items; list; list = list->next) 
		g_object_set (G_OBJECT (list->data), "width", column_width, NULL);
}

static gboolean
button_press_event_cb (GnomeDbCanvasTable *ce, GooCanvasItem  *target_item, GdkEventButton *event,
		       gpointer unused_data)
{
	if (ce->priv->popup_menu_func) {
		GtkWidget *menu;
		menu = ce->priv->popup_menu_func (ce);
		gtk_menu_popup (GTK_MENU (menu), NULL, NULL,
				NULL, NULL, ((GdkEventButton *)event)->button,
				((GdkEventButton *)event)->time);
		return TRUE;
	}

	return FALSE;	
}

/**
 * gnome_db_canvas_table_get_column_item
 * @ce: a #GnomeDbCanvasTable object
 * @column: a #GdaMetaTableColumn object
 *
 * Get the #GnomeDbCanvasColumn object representing @column
 * in @ce.
 *
 * Returns: the corresponding #GnomeDbCanvasColumn
 */
GnomeDbCanvasColumn *
gnome_db_canvas_table_get_column_item (GnomeDbCanvasTable *ce, GdaMetaTableColumn *column)
{
	gint pos;

	g_return_val_if_fail (ce && GNOME_DB_IS_CANVAS_TABLE (ce), NULL);
	g_return_val_if_fail (ce->priv, NULL);
	g_return_val_if_fail (ce->priv->table, NULL);

	pos = g_slist_index (ce->priv->table->columns, column);
	g_return_val_if_fail (pos >= 0, NULL);

	return g_slist_nth_data (ce->priv->column_items, pos);
}


/**
 * gnome_db_canvas_table_get_column_ypos
 * @ce: a #GnomeDbCanvasTable object
 * @column: a #GdaMetaTableColumn object
 *
 * Get the Y position of the middle of the #GnomeDbCanvasColumn object representing @column
 * in @ce, in @ce's coordinates.
 *
 * Returns: the Y coordinate.
 */
gdouble
gnome_db_canvas_table_get_column_ypos (GnomeDbCanvasTable *ce, GdaMetaTableColumn *column)
{
	gint pos;

	g_return_val_if_fail (ce && GNOME_DB_IS_CANVAS_TABLE (ce), 0.);
	g_return_val_if_fail (ce->priv, 0.);
	g_return_val_if_fail (ce->priv->table, 0.);
	g_return_val_if_fail (ce->priv->column_ypos, 0.);

	pos = g_slist_index (ce->priv->table->columns, column);
	g_return_val_if_fail (pos >= 0, 0.);
	return (0.75 * ce->priv->column_ypos[pos+1] + 0.25 * ce->priv->column_ypos[pos]);
}


/**
 * gnome_db_canvas_table_new
 * @parent: the parent item, or NULL. 
 * @table: a #GdaMetaTable to display
 * @x: the x coordinate
 * @y: the y coordinate
 * @...: optional pairs of property names and values, and a terminating NULL.
 *
 * Creates a new canvas item to display the @table table
 *
 * Returns: a new #GooCanvasItem object
 */
GooCanvasItem *
gnome_db_canvas_table_new (GooCanvasItem *parent, GdaMetaTable *table, 
			 gdouble x, gdouble y, ...)
{
	GooCanvasItem *item;
	const char *first_property;
	va_list var_args;
		
	item = g_object_new (GNOME_DB_TYPE_CANVAS_TABLE, NULL);

	if (parent) {
		goo_canvas_item_add_child (parent, item, -1);
		g_object_unref (item);
	}

	g_object_set (item, "table", table, NULL);

	va_start (var_args, y);
	first_property = va_arg (var_args, char*);
	if (first_property)
		g_object_set_valist ((GObject*) item, first_property, var_args);
	va_end (var_args);

	goo_canvas_item_translate (item, x, y);

	return item;
}
