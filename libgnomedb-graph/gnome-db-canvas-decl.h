/* gnome-db-canvas-decl.h
 *
 * Copyright (C) 2008 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNOME_DB_CANVAS_DECL_H_
#define __GNOME_DB_CANVAS_DECL_H_

typedef struct _GnomeDbCanvas        GnomeDbCanvas;
typedef struct _GnomeDbCanvasClass   GnomeDbCanvasClass;
typedef struct _GnomeDbCanvasPrivate GnomeDbCanvasPrivate;

typedef struct _GnomeDbCanvasItem        GnomeDbCanvasItem;
typedef struct _GnomeDbCanvasItemClass   GnomeDbCanvasItemClass;
typedef struct _GnomeDbCanvasItemPrivate GnomeDbCanvasItemPrivate;

typedef struct _GnomeDbCanvasTable        GnomeDbCanvasTable;
typedef struct _GnomeDbCanvasTableClass   GnomeDbCanvasTableClass;
typedef struct _GnomeDbCanvasTablePrivate GnomeDbCanvasTablePrivate;

typedef struct _GnomeDbCanvasColumn        GnomeDbCanvasColumn;
typedef struct _GnomeDbCanvasColumnClass   GnomeDbCanvasColumnClass;
typedef struct _GnomeDbCanvasColumnPrivate GnomeDbCanvasColumnPrivate;

typedef struct _GnomeDbGraph GnomeDbGraph;
typedef struct _GnomeDbGraphClass GnomeDbGraphClass;
typedef struct _GnomeDbGraphPrivate GnomeDbGraphPrivate;

typedef struct _GnomeDbGraphItem GnomeDbGraphItem;
typedef struct _GnomeDbGraphItemClass GnomeDbGraphItemClass;
typedef struct _GnomeDbGraphItemPrivate GnomeDbGraphItemPrivate;


#endif
