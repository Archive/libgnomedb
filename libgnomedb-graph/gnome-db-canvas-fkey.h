/* gnome-db-canvas-fkey.h
 *
 * Copyright (C) 2005 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNOME_DB_CANVAS_FKEY__
#define __GNOME_DB_CANVAS_FKEY__

#include "gnome-db-canvas-item.h"

G_BEGIN_DECLS

/*
 * 
 * "Drag item" GooCanvas item: a GnomeDbCanvasItem item which is used to represent
 * an element being dragged, and destroys itself when the mouse button is released
 *
 */

#define GNOME_DB_TYPE_CANVAS_FKEY          (gnome_db_canvas_fkey_get_type())
#define GNOME_DB_CANVAS_FKEY(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, gnome_db_canvas_fkey_get_type(), GnomeDbCanvasFkey)
#define GNOME_DB_CANVAS_FKEY_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, gnome_db_canvas_fkey_get_type (), GnomeDbCanvasFkeyClass)
#define GNOME_DB_IS_CANVAS_FKEY(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, gnome_db_canvas_fkey_get_type ())


typedef struct _GnomeDbCanvasFkey        GnomeDbCanvasFkey;
typedef struct _GnomeDbCanvasFkeyClass   GnomeDbCanvasFkeyClass;
typedef struct _GnomeDbCanvasFkeyPrivate GnomeDbCanvasFkeyPrivate;


/* struct for the object's data */
struct _GnomeDbCanvasFkey
{
	GnomeDbCanvasItem         object;
	GnomeDbCanvasFkeyPrivate *priv;
};

/* struct for the object's class */
struct _GnomeDbCanvasFkeyClass
{
	GnomeDbCanvasItemClass    parent_class;
};

GType          gnome_db_canvas_fkey_get_type       (void) G_GNUC_CONST;
GooCanvasItem *gnome_db_canvas_fkey_new            (GooCanvasItem *parent, GdaMetaTableForeignKey *fk, ...);

G_END_DECLS

#endif
