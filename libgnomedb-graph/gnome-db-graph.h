/* gnome-db-graph.h
 *
 * Copyright (C) 2004 - 2008 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */


#ifndef __GNOME_DB_GRAPH_H_
#define __GNOME_DB_GRAPH_H_

#include <glib-object.h>
#include <libgnomedb-graph/gnome-db-canvas-decl.h>
#include <libgda/libgda.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_GRAPH          (gnome_db_graph_get_type())
#define GNOME_DB_GRAPH(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, gnome_db_graph_get_type(), GnomeDbGraph)
#define GNOME_DB_GRAPH_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, gnome_db_graph_get_type (), GnomeDbGraphClass)
#define GNOME_DB_IS_GRAPH(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, gnome_db_graph_get_type ())

/* error reporting */
extern GQuark gnome_db_graph_error_quark (void);
#define GNOME_DB_GRAPH_ERROR gnome_db_graph_error_quark ()

typedef enum
{
	GNOME_DB_GRAPH__ERROR
} GnomeDbGraphError;

/* struct for the object's data */
struct _GnomeDbGraph
{
	GObject         object;
	GnomeDbGraphPrivate  *priv;
};

/* struct for the object's class */
struct _GnomeDbGraphClass
{
	GObjectClass   parent_class;
	
	/* signals */
	void        (*item_added)   (GnomeDbGraph *graph, GnomeDbGraphItem *item);
	void        (*item_dropped) (GnomeDbGraph *graph, GnomeDbGraphItem *item);
	void        (*item_moved)   (GnomeDbGraph *graph, GnomeDbGraphItem *item);
};

GType            gnome_db_graph_get_type            (void) G_GNUC_CONST;

GnomeDbGraph    *gnome_db_graph_new                 (GdaMetaStore *store, guint id);

void             gnome_db_graph_add_item            (GnomeDbGraph *graph, GnomeDbGraphItem *item);
void             gnome_db_graph_del_item            (GnomeDbGraph *graph, GnomeDbGraphItem *item);
GSList          *gnome_db_graph_get_items           (GnomeDbGraph *graph);

void             gnome_db_graph_delete              (GdaMetaStore *store, guint id);

G_END_DECLS

#endif
