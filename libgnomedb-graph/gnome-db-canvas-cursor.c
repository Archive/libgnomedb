/* gnome-db-canvas-cursor.c
 *
 * Copyright (C) 2002 - 2007 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "gnome-db-decl.h"
#include "gnome-db-canvas-cursor.h"
#include <libgnomedb/marshal.h>

static void gnome_db_canvas_cursor_class_init (GnomeDbCanvasCursorClass * class);
static void gnome_db_canvas_cursor_init       (GnomeDbCanvasCursor * drag);
static void gnome_db_canvas_cursor_dispose    (GObject   * object);

static void gnome_db_canvas_cursor_set_property    (GObject *object,
						guint param_id,
						const GValue *value,
						GParamSpec *pspec);
static void gnome_db_canvas_cursor_get_property    (GObject *object,
						guint param_id,
						GValue *value,
						GParamSpec *pspec);


enum
{
	PROP_00,
	PROP_X,
	PROP_Y,
	PROP_HEIGHT,
	PROP_WIDTH,
	PROP_TEXT,
	PROP_FILLCOLOR
};


/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *cursor_parent_class = NULL;

GType
gnome_db_canvas_cursor_get_type (void)
{
	static GType type = 0;

        if (G_UNLIKELY (type == 0)) {
		static const GTypeInfo info = {
			sizeof (GnomeDbCanvasCursorClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_canvas_cursor_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbCanvasCursor),
			0,
			(GInstanceInitFunc) gnome_db_canvas_cursor_init
		};		

		type = g_type_register_static (GNOME_DB_TYPE_CANVAS_ITEM, "GnomeDbCanvasCursor", &info, 0);
	}

	return type;
}

	

static void
gnome_db_canvas_cursor_class_init (GnomeDbCanvasCursorClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	cursor_parent_class = g_type_class_peek_parent (class);

	object_class->dispose = gnome_db_canvas_cursor_dispose;

	/* Properties */
	object_class->set_property = gnome_db_canvas_cursor_set_property;
	object_class->get_property = gnome_db_canvas_cursor_get_property;

	g_object_class_install_property
                (object_class, PROP_FILLCOLOR,
                 g_param_spec_pointer ("fill_color", NULL, NULL, G_PARAM_READABLE | G_PARAM_WRITABLE));
	g_object_class_install_property 
		(object_class, PROP_X,
		 g_param_spec_double ("x", NULL, NULL, -G_MAXDOUBLE, G_MAXDOUBLE, 0., G_PARAM_WRITABLE));
	g_object_class_install_property 
		(object_class, PROP_Y,
		 g_param_spec_double ("y", NULL, NULL, -G_MAXDOUBLE, G_MAXDOUBLE, 0., G_PARAM_WRITABLE));
	g_object_class_install_property 
		(object_class, PROP_HEIGHT,
		 g_param_spec_double ("height", NULL, NULL, 0., G_MAXDOUBLE, 0., G_PARAM_READABLE | G_PARAM_WRITABLE));
	g_object_class_install_property 
		(object_class, PROP_WIDTH,
		 g_param_spec_double ("width", NULL, NULL, 0., G_MAXDOUBLE, 0., G_PARAM_READABLE | G_PARAM_WRITABLE));
}

static int button_release_cb (GooCanvasItem  *item, GooCanvasItem  *target_item,
			      GdkEventButton *event, gpointer data);

static void
gnome_db_canvas_cursor_init (GnomeDbCanvasCursor * cursor)
{
	cursor->item = NULL;

	g_signal_connect (G_OBJECT(cursor), "button-release-event",
			  G_CALLBACK (button_release_cb), NULL);
}

static int 
button_release_cb (GooCanvasItem *item, GooCanvasItem  *target_item,
		   GdkEventButton *event, gpointer data)
{
	goo_canvas_pointer_ungrab (goo_canvas_item_get_canvas (item), item, event->time);
	goo_canvas_item_remove (item);
	return TRUE;
}

static void
gnome_db_canvas_cursor_dispose (GObject   * object)
{
	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOME_DB_IS_CANVAS_CURSOR (object));

	/* for the parent class */
	cursor_parent_class->dispose (object);
}

static void post_init (GnomeDbCanvasCursor * cursor);

static void 
gnome_db_canvas_cursor_set_property    (GObject *object,
				    guint param_id,
				    const GValue *value,
				    GParamSpec *pspec)
{
	GnomeDbCanvasCursor *cd;
	gchar *str;

	cd = GNOME_DB_CANVAS_CURSOR (object);
	if (!cd->item)
		post_init (cd);

	switch (param_id) {
	case PROP_FILLCOLOR:
		str = g_value_get_pointer (value);
		g_object_set (G_OBJECT (cd->item), 
			      "fill_color", str, NULL);
		break;
	case PROP_X:
		g_object_set (G_OBJECT (cd->item), "x", g_value_get_double (value), NULL);
		break;
	case PROP_Y:
		g_object_set (G_OBJECT (cd->item), "y", g_value_get_double (value), NULL);
		break;
	case PROP_WIDTH:
		g_object_set (G_OBJECT (cd->item), "width", g_value_get_double (value), NULL);
		break;
	case PROP_HEIGHT:
		g_object_set (G_OBJECT (cd->item), "height", g_value_get_double (value), NULL);
		break;
	}
}

static void 
gnome_db_canvas_cursor_get_property    (GObject *object,
				     guint param_id,
				     GValue *value,
				     GParamSpec *pspec)
{
	GnomeDbCanvasCursor *cd;
	gchar *str;
	gdouble dval;

	cd = GNOME_DB_CANVAS_CURSOR (object);
	if (!cd->item)
		post_init (cd);

	switch (param_id) {
	case PROP_FILLCOLOR:
		g_object_get (G_OBJECT (cd->item), "fill_color", &str, NULL);
		g_value_take_string (value, str);
		break;
	case PROP_X:
		g_object_get (G_OBJECT (cd->item), "x", &dval, NULL);
		g_value_set_double (value, dval);
		break;
	case PROP_Y:
		g_object_set (G_OBJECT (cd->item), "y", &dval, NULL);
		g_value_set_double (value, dval);
		break;
	case PROP_WIDTH:
		g_object_set (G_OBJECT (cd->item), "width", &dval, NULL);
		g_value_set_double (value, dval);
		break;
	case PROP_HEIGHT:
		g_object_set (G_OBJECT (cd->item), "height", &dval, NULL);
		g_value_set_double (value, dval);
		break;
	}
}



static void 
post_init (GnomeDbCanvasCursor * cursor)
{
	cursor->item = goo_canvas_rect_new (GOO_CANVAS_ITEM (cursor),
					    0., 0., 20., 8.,
					    "stroke-color", "black", NULL);
}

/**
 * gnome_db_canvas_cursor_new
 * @parent: the parent item, or NULL. 
 * @...: optional pairs of property names and values, and a terminating NULL.
 *
 * Creates a new canvas item to display the @txt message
 *
 * Returns: a new #GooCanvasItem object
 */
GooCanvasItem *
gnome_db_canvas_cursor_new (GooCanvasItem *parent, ...)
{
	GooCanvasItem *item;
	const char *first_property;
	va_list var_args;
		
	item = g_object_new (GNOME_DB_TYPE_CANVAS_CURSOR, NULL);

	if (parent) {
		goo_canvas_item_add_child (parent, item, -1);
		g_object_unref (item);
	}

	va_start (var_args, parent);
	first_property = va_arg (var_args, char*);
	if (first_property)
		g_object_set_valist ((GObject*) item, first_property, var_args);
	va_end (var_args);

	return item;
}
