/* gnome-db-canvas-db-relations.c
 *
 * Copyright (C) 2002 - 2007 Vivien Malerba
 * Copyright (C) 2002 Fernando Martins
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <gtk/gtk.h>
#include <libgnomedb/marshal.h>
#include <libgda/libgda.h>
#include <glib/gi18n-lib.h>
#include "gnome-db-canvas-db-relations.h"
#include "gnome-db-canvas-table.h"
#include "gnome-db-canvas-column.h"
#include "gnome-db-canvas-fkey.h"
#include "gnome-db-graph.h"
#include "gnome-db-graph-item.h"

static void gnome_db_canvas_db_relations_class_init (GnomeDbCanvasDbRelationsClass *class);
static void gnome_db_canvas_db_relations_init       (GnomeDbCanvasDbRelations *canvas);
static void gnome_db_canvas_db_relations_dispose   (GObject *object);

static void gnome_db_canvas_db_relations_set_property (GObject *object,
						    guint param_id,
						    const GValue *value,
						    GParamSpec *pspec);
static void gnome_db_canvas_db_relations_get_property (GObject *object,
						    guint param_id,
						    GValue *value,
						    GParamSpec *pspec);

/* virtual functions */
static void       create_canvas_items (GnomeDbCanvas *canvas);
static void       clean_canvas_items  (GnomeDbCanvas *canvas);
static void       graph_item_added    (GnomeDbCanvas *canvas, GnomeDbGraphItem *item);
static void       graph_item_dropped  (GnomeDbCanvas *canvas, GnomeDbGraphItem *item);
static GtkWidget *build_context_menu  (GnomeDbCanvas *canvas);

static GdaMetaStruct *get_mstruct (GnomeDbCanvasDbRelations *canvas);
static void       drag_action_dcb     (GnomeDbCanvas *canvas, GnomeDbCanvasItem *from_item, GnomeDbCanvasItem *to_item);

static void meta_store_changed_cb (GdaMetaStore *store, GSList *changes, GnomeDbCanvasDbRelations *canvas);
static void meta_store_reset_cb (GdaMetaStore *store, GnomeDbCanvasDbRelations *canvas);

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *parent_class = NULL;

enum
{
        PROP_0,
        PROP_META_STORE,
	PROP_META_STRUCT
};

struct _GnomeDbCanvasDbRelationsPrivate
{
	GHashTable       *hash_tables; /* key = GdaMetaTable, value = GnomeDbCanvasMetaTable (and the reverse) */
	GHashTable       *hash_fkeys; /* key = GdaMetaTableForeignKey, value = GnomeDbCanvasFkey */
	GSList           *all_items; /* list of all the GnomeDbCanvasItem objects */

	GdaMetaStore     *store;
	GdaMetaStruct    *mstruct; /* re-computed when @store changes */
};

GType
gnome_db_canvas_db_relations_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0)) {
		static const GTypeInfo info = {
			sizeof (GnomeDbCanvasDbRelationsClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_canvas_db_relations_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbCanvasDbRelations),
			0,
			(GInstanceInitFunc) gnome_db_canvas_db_relations_init
		};		

		type = g_type_register_static (GNOME_DB_TYPE_CANVAS, "GnomeDbCanvasDbRelations", &info, 0);
	}
	return type;
}

static void
gnome_db_canvas_db_relations_init (GnomeDbCanvasDbRelations * canvas)
{
	canvas->priv = g_new0 (GnomeDbCanvasDbRelationsPrivate, 1);
	canvas->priv->hash_tables = g_hash_table_new (NULL, NULL);
	canvas->priv->hash_fkeys = g_hash_table_new (NULL, NULL);
	canvas->priv->store = NULL;
	canvas->priv->mstruct = NULL;
}

static void
gnome_db_canvas_db_relations_class_init (GnomeDbCanvasDbRelationsClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);
	parent_class = g_type_class_peek_parent (class);

	/* GnomeDbCanvas virtual functions */
	GNOME_DB_CANVAS_CLASS (class)->create_canvas_items = create_canvas_items;
	GNOME_DB_CANVAS_CLASS (class)->clean_canvas_items = clean_canvas_items;
	GNOME_DB_CANVAS_CLASS (class)->graph_item_added = graph_item_added;
	GNOME_DB_CANVAS_CLASS (class)->graph_item_dropped = graph_item_dropped;
	GNOME_DB_CANVAS_CLASS (class)->build_context_menu = build_context_menu;

	GNOME_DB_CANVAS_CLASS (class)->drag_action = drag_action_dcb;
	
	object_class->dispose = gnome_db_canvas_db_relations_dispose;

	/* properties */
	object_class->set_property = gnome_db_canvas_db_relations_set_property;
        object_class->get_property = gnome_db_canvas_db_relations_get_property;
	g_object_class_install_property (object_class, PROP_META_STORE,
                                         g_param_spec_object ("meta-store", "GdaMetaStore", NULL,
							      GDA_TYPE_META_STORE,
							      G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY));
	g_object_class_install_property (object_class, PROP_META_STRUCT,
                                         g_param_spec_object ("meta-struct", "GdaMetaStruct", NULL,
							      GDA_TYPE_META_STRUCT,
							      G_PARAM_READABLE | G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY));
}

static void
gnome_db_canvas_db_relations_dispose (GObject *object)
{
	GnomeDbCanvasDbRelations *canvas;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOME_DB_IS_CANVAS_DB_RELATIONS (object));

	canvas = GNOME_DB_CANVAS_DB_RELATIONS (object);

	if (canvas->priv) {
		clean_canvas_items (GNOME_DB_CANVAS (canvas));
		if (canvas->priv->store) {
			g_signal_handlers_disconnect_by_func (G_OBJECT (canvas->priv->store),
							      G_CALLBACK (meta_store_changed_cb), canvas);
			g_signal_handlers_disconnect_by_func (G_OBJECT (canvas->priv->store),
							      G_CALLBACK (meta_store_reset_cb), canvas);
			g_object_unref (canvas->priv->store);
			canvas->priv->store = NULL;
		}

		if (canvas->priv->mstruct) {
			g_object_unref (canvas->priv->mstruct);
			canvas->priv->mstruct = NULL;
		}

		g_hash_table_destroy (canvas->priv->hash_tables);
		g_hash_table_destroy (canvas->priv->hash_fkeys);

		g_free (canvas->priv);
		canvas->priv = NULL;
	}

	/* for the parent class */
	parent_class->dispose (object);
}

static void
meta_store_changed_cb (GdaMetaStore *store, GSList *changes, GnomeDbCanvasDbRelations *canvas)
{
	meta_store_reset_cb (store, canvas);
}

static void
meta_store_reset_cb (GdaMetaStore *store, GnomeDbCanvasDbRelations *canvas)
{
	TO_IMPLEMENT;
}

static void
gnome_db_canvas_db_relations_set_property (GObject *object,
					guint param_id,
					const GValue *value,
					GParamSpec *pspec)
{
	GnomeDbCanvasDbRelations *canvas;

        canvas = GNOME_DB_CANVAS_DB_RELATIONS (object);
        if (canvas->priv) {
                switch (param_id) {
                case PROP_META_STORE:
                        canvas->priv->store = g_value_get_object (value);
                        if (canvas->priv->store) {
                                g_object_ref (canvas->priv->store);
                                g_signal_connect (G_OBJECT (canvas->priv->store), "meta-changed",
                                                  G_CALLBACK (meta_store_changed_cb), canvas);
                                g_signal_connect (G_OBJECT (canvas->priv->store), "meta-reset",
                                                  G_CALLBACK (meta_store_reset_cb), canvas);
                        }
                        break;
		case PROP_META_STRUCT:
			canvas->priv->mstruct = g_value_get_object (value);
                        if (canvas->priv->mstruct) 
                                g_object_ref (canvas->priv->mstruct);
			break;
		}
	}
}

static void
gnome_db_canvas_db_relations_get_property (GObject *object,
					guint param_id,
					GValue *value,
					GParamSpec *pspec)
{
	GnomeDbCanvasDbRelations *canvas;

        canvas = GNOME_DB_CANVAS_DB_RELATIONS (object);
        if (canvas->priv) {
                switch (param_id) {
                case PROP_META_STORE:
			g_value_set_object (value, canvas->priv->store);
                        break;
		case PROP_META_STRUCT:
			g_value_set_object (value, canvas->priv->mstruct);
			break;
		}
	}
}

static void
drag_action_dcb (GnomeDbCanvas *canvas, GnomeDbCanvasItem *from_item, GnomeDbCanvasItem *to_item)
{
	TO_IMPLEMENT;
}

/**
 * gnome_db_canvas_db_relations_new
 * @dict: a #GdaDict object
 *
 * Creates a new canvas widget to display the relations between the database's tables.
 * The database is the one managed by the #GdaDict object which @graph refers.
 *
 * After the #GnomeDbCanvasDbRelations has been created, it is possible to display
 * the tables as listed in a #GdaGdaph by setting the "graph" property (using g_object_set()).
 *
 * Returns: a new #GnomeDbCanvasDbRelations widget
 */
GtkWidget *
gnome_db_canvas_db_relations_new (GdaMetaStore *store)
{
	g_return_val_if_fail (!store || GDA_IS_META_STORE (store), NULL);
        return GTK_WIDGET (g_object_new (GNOME_DB_TYPE_CANVAS_DB_RELATIONS, "meta-store", store, NULL));
}


/*
 * Add all the required GooCanvasItem objects for the associated #GnomeDbGraph object 
 */
static void
create_canvas_items (GnomeDbCanvas *canvas)
{
	GSList *list, *graph_items;
	GnomeDbGraph *graph = gnome_db_canvas_get_graph (canvas);

	graph_items = gnome_db_graph_get_items (graph);
	list = graph_items;
	while (list) {
		graph_item_added (canvas, GNOME_DB_GRAPH_ITEM (list->data));
		list = g_slist_next (list);
	}
	g_slist_free (graph_items);
}

static void
clean_canvas_items (GnomeDbCanvas *canvas)
{
	GnomeDbCanvasDbRelations *dbrel = GNOME_DB_CANVAS_DB_RELATIONS (canvas);

	/* clean memory */
	g_hash_table_destroy (dbrel->priv->hash_tables);
	g_hash_table_destroy (dbrel->priv->hash_fkeys);
	dbrel->priv->hash_tables = g_hash_table_new (NULL, NULL);
	dbrel->priv->hash_fkeys = g_hash_table_new (NULL, NULL);
	if (dbrel->priv->all_items) {
		g_slist_free (dbrel->priv->all_items);
		dbrel->priv->all_items = NULL;
	}
}

static GdaMetaStruct *
get_mstruct (GnomeDbCanvasDbRelations *canvas)
{
	if (!canvas->priv->mstruct) {
		if (!canvas->priv->store)
			return NULL;
		canvas->priv->mstruct = gda_meta_struct_new (canvas->priv->store, GDA_META_STRUCT_FEATURE_FOREIGN_KEYS);
		gda_meta_struct_complement_default (canvas->priv->mstruct, NULL);
	}
	return canvas->priv->mstruct;
}


static GtkWidget *canvas_entity_popup_func (GnomeDbCanvasTable *ce);

/*
 * Add the GnomeDbCanvasMetaTable corresponding to the graph item
 */
static void
graph_item_added (GnomeDbCanvas *canvas, GnomeDbGraphItem *item)
{
	GValue *table_catalog;
	GValue *table_schema;
	GValue *table_name;
	GdaMetaTable *mtable;
	GnomeDbCanvasDbRelations *dbrel = GNOME_DB_CANVAS_DB_RELATIONS (canvas);

	table_catalog = g_object_get_data (G_OBJECT (item), "tcat");
	table_schema = g_object_get_data (G_OBJECT (item), "tschema");
	table_name = g_object_get_data (G_OBJECT (item), "tname");
	
	if (!get_mstruct (dbrel))
		return;

	mtable = GDA_META_TABLE (gda_meta_struct_get_db_object (dbrel->priv->mstruct, table_catalog,
								table_schema, table_name));
	if (mtable) {
		gdouble x, y;
		GooCanvasItem *table_item;
		gnome_db_graph_item_get_position (item, &x, &y);
		table_item = gnome_db_canvas_table_new (goo_canvas_get_root_item (GOO_CANVAS (canvas)), 
						     mtable, x, y, NULL);
		g_hash_table_insert (dbrel->priv->hash_tables, mtable, table_item);
		g_hash_table_insert (dbrel->priv->hash_tables,table_item, mtable);
		dbrel->priv->all_items = g_slist_prepend (dbrel->priv->all_items, table_item);
		g_object_set (G_OBJECT (table_item), 
			      "graph-item", item,
			      "popup_menu_func", canvas_entity_popup_func, NULL);
		gnome_db_canvas_declare_item (canvas, GNOME_DB_CANVAS_ITEM (table_item));

		/* if there are some FK links, then also add them */
		GSList *list;
		for (list = mtable->fk_list; list; list = list->next) {
			GooCanvasItem *ref_table_item;
			GdaMetaTableForeignKey *fk = (GdaMetaTableForeignKey*) list->data;
			ref_table_item = g_hash_table_lookup (dbrel->priv->hash_tables, fk->depend_on);
			if (ref_table_item) {
				GooCanvasItem *fk_item;
				fk_item = gnome_db_canvas_fkey_new (goo_canvas_get_root_item (GOO_CANVAS (canvas)), fk, NULL);
				gnome_db_canvas_declare_item (canvas, GNOME_DB_CANVAS_ITEM (fk_item));
				g_hash_table_insert (dbrel->priv->hash_fkeys, fk, fk_item);
			}
		}
		for (list = mtable->reverse_fk_list; list; list = list->next) {
			GooCanvasItem *ref_table_item;
			GdaMetaTableForeignKey *fk = (GdaMetaTableForeignKey*) list->data;
			ref_table_item = g_hash_table_lookup (dbrel->priv->hash_tables, fk->meta_table);
			if (ref_table_item) {
				GooCanvasItem *fk_item;
				fk_item = gnome_db_canvas_fkey_new (goo_canvas_get_root_item (GOO_CANVAS (canvas)), fk, NULL);
				gnome_db_canvas_declare_item (canvas, GNOME_DB_CANVAS_ITEM (fk_item));
				g_hash_table_insert (dbrel->priv->hash_fkeys, fk, fk_item);
			}
		}
	}
	else {
		/* remove the graph item as it is now obsolete */
		gnome_db_graph_del_item (gnome_db_graph_item_get_graph (item), item);
	}
}

static void popup_func_delete_cb (GtkMenuItem *mitem, GnomeDbCanvasTable *ce);
static void popup_func_add_depend_cb (GtkMenuItem *mitem, GnomeDbCanvasTable *ce);
static GtkWidget *
canvas_entity_popup_func (GnomeDbCanvasTable *ce)
{
	GtkWidget *menu, *entry;

	menu = gtk_menu_new ();
	entry = gtk_menu_item_new_with_label (_("Remove"));
	g_signal_connect (G_OBJECT (entry), "activate", G_CALLBACK (popup_func_delete_cb), ce);
	gtk_menu_append (GTK_MENU (menu), entry);
	gtk_widget_show (entry);
	entry = gtk_menu_item_new_with_label (_("Add referenced tables"));
	g_signal_connect (G_OBJECT (entry), "activate", G_CALLBACK (popup_func_add_depend_cb), ce);
	gtk_menu_append (GTK_MENU (menu), entry);
	gtk_widget_show (entry);

	return menu;
}

static void
popup_func_delete_cb (GtkMenuItem *mitem, GnomeDbCanvasTable *ce)
{
	GnomeDbGraphItem *gitem;

	gitem = gnome_db_canvas_item_get_graph_item (GNOME_DB_CANVAS_ITEM (ce));
	gnome_db_graph_del_item (gnome_db_graph_item_get_graph (gitem), gitem);
}

static void
popup_func_add_depend_cb (GtkMenuItem *mitem, GnomeDbCanvasTable *ce)
{
	GnomeDbCanvasDbRelations *dbrel;
	GdaMetaDbObject *dbo;

	dbrel = GNOME_DB_CANVAS_DB_RELATIONS (goo_canvas_item_get_canvas (GOO_CANVAS_ITEM (ce)));
	dbo = g_hash_table_lookup (dbrel->priv->hash_tables, ce);
	if (!dbo || (dbo->obj_type != GDA_META_DB_TABLE))
		return;

	if (!get_mstruct (dbrel))
		return;
	
	if (dbrel->priv->store)
		gda_meta_struct_complement_depend (dbrel->priv->mstruct, dbo, NULL);
	GdaMetaTable *mtable = GDA_META_TABLE (dbo);
	GSList *list;
	GooCanvasBounds bounds;
	goo_canvas_item_get_bounds (GOO_CANVAS_ITEM (ce), &bounds);
	bounds.y1 = bounds.y2 + 35.;
	bounds.x2 = bounds.x1 - 20.;

	for (list = mtable->fk_list; list; list = list->next) {
		GdaMetaTableForeignKey *fk = GDA_META_TABLE_FOREIGN_KEY (list->data);
		if (fk->depend_on->obj_type != GDA_META_DB_TABLE)
			continue;
		if (g_hash_table_lookup (dbrel->priv->hash_tables, fk->depend_on))
			continue;

		GnomeDbGraphItem *gitem;
		GnomeDbGraph *graph;

		graph = gnome_db_canvas_get_graph (GNOME_DB_CANVAS (dbrel));
		gitem = gnome_db_graph_item_new (graph);
		gnome_db_graph_item_set_position (gitem, bounds.x2 + 20., bounds.y1);

		GValue *value;
		g_value_set_string ((value = gda_value_new (G_TYPE_STRING)), fk->depend_on->obj_catalog);
		g_object_set_data_full (G_OBJECT (gitem), "tcat", value, (GDestroyNotify) gda_value_free);
		g_value_set_string ((value = gda_value_new (G_TYPE_STRING)), fk->depend_on->obj_schema);
		g_object_set_data_full (G_OBJECT (gitem), "tschema", value, (GDestroyNotify) gda_value_free);
		g_value_set_string ((value = gda_value_new (G_TYPE_STRING)), fk->depend_on->obj_name);
		g_object_set_data_full (G_OBJECT (gitem), "tname", value, (GDestroyNotify) gda_value_free);
		gnome_db_graph_add_item (graph, gitem);
		g_object_unref (G_OBJECT (gitem));

		GnomeDbCanvasTable *new_item;
		new_item = gnome_db_canvas_db_relations_get_table_item (dbrel, GDA_META_TABLE (fk->depend_on));
		goo_canvas_item_get_bounds (GOO_CANVAS_ITEM (new_item), &bounds);
	}
}


/*
 * Remove the GnomeDbCanvasTable corresponding to the graph item
 */
static void
graph_item_dropped (GnomeDbCanvas *canvas, GnomeDbGraphItem *item)
{
	GdaMetaTable *mtable;
	GnomeDbCanvasItem *citem = NULL;
	GSList *list;
	GnomeDbCanvasDbRelations *dbrel = GNOME_DB_CANVAS_DB_RELATIONS (canvas);

	for (list = dbrel->priv->all_items; list; list = list->next) {
		if (gnome_db_canvas_item_get_graph_item ((GnomeDbCanvasItem*) list->data) == item) {
			citem = (GnomeDbCanvasItem*) list->data;
			break;
		}
	}

	if (citem) {
		mtable = g_hash_table_lookup (dbrel->priv->hash_tables, citem);
		g_hash_table_remove (dbrel->priv->hash_tables, citem);
		g_hash_table_remove (dbrel->priv->hash_tables, mtable);
		goo_canvas_item_remove (GOO_CANVAS_ITEM (citem));
		dbrel->priv->all_items = g_slist_remove (dbrel->priv->all_items, citem);
	}
}

static void popup_add_table_cb (GtkMenuItem *mitem, GnomeDbCanvasDbRelations *canvas);
static GtkWidget *
build_context_menu (GnomeDbCanvas *canvas)
{
	GtkWidget *menu, *mitem, *submenu, *submitem;
	GSList *dbolist, *list;
	GnomeDbCanvasDbRelations *dbrel = GNOME_DB_CANVAS_DB_RELATIONS (canvas);

	if (!get_mstruct (dbrel))
		return NULL;

	menu = gtk_menu_new ();
	submitem = gtk_menu_item_new_with_label (_("Add table"));
	gtk_widget_show (submitem);
	gtk_menu_append (menu, submitem);
	submenu = NULL;

	/* build list of tables */
	dbolist = gda_meta_struct_get_all_db_objects (dbrel->priv->mstruct);
	for (list = dbolist; list; list = list->next) {
		GdaMetaDbObject *dbo = GDA_META_DB_OBJECT (list->data);
		GdaMetaTable *mtable;

		if (dbo->obj_type != GDA_META_DB_TABLE)
			continue;
		
		mtable = GDA_META_TABLE (dbo);
		if (mtable && g_hash_table_lookup (dbrel->priv->hash_tables, mtable))
			continue; /* skip that table as it is already present in the canvas */

		if (!submenu) {
			submenu = gtk_menu_new ();
			gtk_menu_item_set_submenu (GTK_MENU_ITEM (submitem), submenu);
			gtk_widget_show (submenu);
		}
		
		mitem = gtk_menu_item_new_with_label (dbo->obj_name);
		gtk_widget_show (mitem);
		gtk_menu_append (submenu, mitem);

		GValue *tcatalog, *tschema, *tname;
		g_value_set_string ((tcatalog = gda_value_new (G_TYPE_STRING)), dbo->obj_catalog);
		g_value_set_string ((tschema = gda_value_new (G_TYPE_STRING)), dbo->obj_schema);
		g_value_set_string ((tname = gda_value_new (G_TYPE_STRING)), dbo->obj_name);
		g_object_set_data_full (G_OBJECT (mitem), "tcat", tcatalog, (GDestroyNotify) gda_value_free);
		g_object_set_data_full (G_OBJECT (mitem), "tschema",  tschema, (GDestroyNotify) gda_value_free);
		g_object_set_data_full (G_OBJECT (mitem), "tname", tname, (GDestroyNotify) gda_value_free);
		g_signal_connect (G_OBJECT (mitem), "activate", G_CALLBACK (popup_add_table_cb), canvas);
	}
	g_slist_free (dbolist);

	/* sub menu is incensitive if there are no more tables left to add */
	gtk_widget_set_sensitive (submitem, submenu ? TRUE : FALSE);

	return menu;
}

static void
popup_add_table_cb (GtkMenuItem *mitem, GnomeDbCanvasDbRelations *canvas)
{
	GdaMetaTable *mtable;
	GValue *table_catalog;
	GValue *table_schema;
	GValue *table_name;

	table_catalog = g_object_get_data (G_OBJECT (mitem), "tcat");
	table_schema = g_object_get_data (G_OBJECT (mitem), "tschema");
	table_name = g_object_get_data (G_OBJECT (mitem), "tname");

	/*g_print ("Add %s.%s.%s\n", g_value_get_string (table_catalog), 
	  g_value_get_string (table_schema), g_value_get_string (table_name));*/
	mtable = (GdaMetaTable*) gda_meta_struct_complement (canvas->priv->mstruct, GDA_META_DB_TABLE,
							     table_catalog, table_schema, table_name, NULL);
	if (mtable) {
		GnomeDbGraphItem *gitem;
		GnomeDbGraph *graph;

		graph = gnome_db_canvas_get_graph (GNOME_DB_CANVAS (canvas));
		gitem = gnome_db_graph_item_new (graph);
		gnome_db_graph_item_set_position (gitem, GNOME_DB_CANVAS (canvas)->xmouse, 
						  GNOME_DB_CANVAS (canvas)->ymouse);
		g_object_set_data_full (G_OBJECT (gitem), "tcat", gda_value_copy (table_catalog), 
					(GDestroyNotify) gda_value_free);
		g_object_set_data_full (G_OBJECT (gitem), "tschema", gda_value_copy (table_schema),
					(GDestroyNotify) gda_value_free);
		g_object_set_data_full (G_OBJECT (gitem), "tname", gda_value_copy (table_name),
					(GDestroyNotify) gda_value_free);
		gnome_db_graph_add_item (graph, gitem);
		g_object_unref (G_OBJECT (gitem));
	}
	else
		g_print ("Unknown...\n");
}

/**
 * gnome_db_canvas_db_relations_get_table_item
 * @canvas:
 * @table:
 *
 * Returns:
 */
GnomeDbCanvasTable *
gnome_db_canvas_db_relations_get_table_item  (GnomeDbCanvasDbRelations *canvas, GdaMetaTable *table)
{
	GnomeDbCanvasTable *table_item;
	g_return_val_if_fail (GNOME_DB_IS_CANVAS_DB_RELATIONS (canvas), NULL);
	g_return_val_if_fail (canvas->priv, NULL);

	table_item = g_hash_table_lookup (canvas->priv->hash_tables, table);
	return GNOME_DB_CANVAS_TABLE (table_item);
}

/**
 * gnome_db_canvas_db_relations_add_table_item
 * @canvas: a #GnomeDbCanvasDbRelations canvas
 * @table_catalog: the catalog in which the table is, or %NULL
 * @table_schema: the schema in which the table is, or %NULL
 * @table_name: the table's name
 *
 * Add a table to @canvas.
 *
 * Returns: the corresponding canvas item, or %NULL if the table was not found.
 */
GnomeDbCanvasTable *
gnome_db_canvas_db_relations_add_table_item  (GnomeDbCanvasDbRelations *canvas, 
					   const GValue *table_catalog, const GValue *table_schema,
					   const GValue *table_name)
{
	g_return_val_if_fail (GNOME_DB_IS_CANVAS_DB_RELATIONS (canvas), NULL);
	g_return_val_if_fail (canvas->priv, NULL);

	GdaMetaTable *mtable;

	if (!get_mstruct (canvas))
		return NULL;
	mtable = (GdaMetaTable *) gda_meta_struct_complement (canvas->priv->mstruct, GDA_META_DB_TABLE,
							      table_catalog, table_schema, table_name, NULL);
	if (mtable) {
		GnomeDbGraphItem *gitem;
		GnomeDbGraph *graph;
		
		graph = gnome_db_canvas_get_graph (GNOME_DB_CANVAS (canvas));
		gitem = gnome_db_graph_item_new (graph);
		gnome_db_graph_item_set_position (gitem, GNOME_DB_CANVAS (canvas)->xmouse, 
						  GNOME_DB_CANVAS (canvas)->ymouse);
		if (table_catalog)
			g_object_set_data_full (G_OBJECT (gitem), "tcat", gda_value_copy (table_catalog), 
						(GDestroyNotify) gda_value_free);
		if (table_schema)
			g_object_set_data_full (G_OBJECT (gitem), "tschema", gda_value_copy (table_schema), 
						(GDestroyNotify) gda_value_free);
		g_object_set_data_full (G_OBJECT (gitem), "tname", gda_value_copy (table_name), 
					(GDestroyNotify) gda_value_free);
		gnome_db_graph_add_item (graph, gitem);
		g_object_unref (G_OBJECT (gitem));

		return gnome_db_canvas_db_relations_get_table_item (canvas, mtable);
	}
	else
		return NULL;
}
