/* gnome-db-canvas-tip.h
 *
 * Copyright (C) 2007 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNOME_DB_CANVAS_TIP__
#define __GNOME_DB_CANVAS_TIP__

#include <goocanvas.h>

G_BEGIN_DECLS


/*
 * 
 * "Tooltip item" GooCanvas item: a CanvasGroup item which is used to present
 * a tip when the mouse does not move for a time (handled by the CanvasBase object)
 *
 */

#define GNOME_DB_TYPE_CANVAS_TIP          (gnome_db_canvas_tip_get_type())
#define GNOME_DB_CANVAS_TIP(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, gnome_db_canvas_tip_get_type(), GnomeDbCanvasTip)
#define GNOME_DB_CANVAS_TIP_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, gnome_db_canvas_tip_get_type (), GnomeDbCanvasTipClass)
#define GNOME_DB_IS_CANVAS_TIP(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, gnome_db_canvas_tip_get_type ())


typedef struct _GnomeDbCanvasTip        GnomeDbCanvasTip;
typedef struct _GnomeDbCanvasTipClass   GnomeDbCanvasTipClass;
typedef struct _GnomeDbCanvasTipPrivate GnomeDbCanvasTipPrivate;


/* struct for the object's data */
struct _GnomeDbCanvasTip
{
	GooCanvasGroup        object;

	GnomeDbCanvasTipPrivate *priv;
};

/* struct for the object's class */
struct _GnomeDbCanvasTipClass
{
	GooCanvasGroupClass   parent_class;
};

GType          gnome_db_canvas_tip_get_type (void) G_GNUC_CONST;
GooCanvasItem *gnome_db_canvas_tip_new      (GooCanvasItem *parent, const gchar *txt, gdouble x, gdouble y, ...);
	
G_END_DECLS

#endif
