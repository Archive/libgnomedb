/* gnome-db-canvas-item.h
 *
 * Copyright (C) 2007 - 2008 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNOME_DB_CANVAS_ITEM__
#define __GNOME_DB_CANVAS_ITEM__

#include <goocanvas.h>
#include <libgnomedb-graph/gnome-db-canvas-decl.h>

G_BEGIN_DECLS

/*
 * 
 * GnomeDbCanvasItem item: an item with the following capabilities:
 *    - can be moved around and emits the "moved" signal after the move.
 *    - is being put on top when the user clicks on it (button 1)
 *    - manage dragging operation (if allowed to)
 *
 * The following arguments are available (added to the ones from GooCanvasGroup):
 * name                 type                    read/write      description
 * ------------------------------------------------------------------------------------------
 * allow_move           boolean                 RW              Allow the item to be moved
 * allow_drag           boolean                 RW              The item can initiate a grag process
 * tooltip_text         string                  RW              The textual tooltip to display, if any
 * graph_item           pointer                 RW              The corresponding GnomeDbGraphItem, if any
 * 
 * NOTE: 1 - any of "allow_move" and "allow_drag" props MUST be set.
 *       2 - these 2 props can't be TRUE at the same time.
 * 
 */

#define GNOME_DB_TYPE_CANVAS_ITEM          (gnome_db_canvas_item_get_type())
#define GNOME_DB_CANVAS_ITEM(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, gnome_db_canvas_item_get_type(), GnomeDbCanvasItem)
#define GNOME_DB_CANVAS_ITEM_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, gnome_db_canvas_item_get_type (), GnomeDbCanvasItemClass)
#define GNOME_DB_IS_CANVAS_ITEM(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, gnome_db_canvas_item_get_type ())


/* struct for the object's data */
struct _GnomeDbCanvasItem
{
	GooCanvasGroup         object;

	GnomeDbCanvasItemPrivate *priv;
};

/* struct for the object's class */
struct _GnomeDbCanvasItemClass
{
	GooCanvasGroupClass    parent_class;

	/* signals */
	void (*moved)        (GnomeDbCanvasItem *citem);
	void (*moving)       (GnomeDbCanvasItem *citem);
	void (*shifted)      (GnomeDbCanvasItem *citem);
	void (*drag_action)  (GnomeDbCanvasItem *citem, GnomeDbCanvasItem * dragged_from, GnomeDbCanvasItem * dragged_to);
	void (*destroy)      (GnomeDbCanvasItem *citem);

	/* virtual functions */
	void (*extra_event)  (GnomeDbCanvasItem *citem, GdkEventType event_type);
	void (*get_edge_nodes)(GnomeDbCanvasItem *citem, GnomeDbCanvasItem **from, GnomeDbCanvasItem **to);
};

GType              gnome_db_canvas_item_get_type       (void) G_GNUC_CONST;

GnomeDbCanvas     *gnome_db_canvas_item_get_canvas     (GnomeDbCanvasItem *item);
GnomeDbGraphItem  *gnome_db_canvas_item_get_graph_item (GnomeDbCanvasItem *item);
void               gnome_db_canvas_item_get_edge_nodes (GnomeDbCanvasItem *item, 
							GnomeDbCanvasItem **from, GnomeDbCanvasItem **to);

G_END_DECLS

#endif
