/* gnome-db-graph-item.c
 *
 * Copyright (C) 2004 - 2008 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <string.h>
#include <glib/gi18n-lib.h>
#include "gnome-db-graph-item.h"
#include "gnome-db-graph.h"
#include <libgda/libgda.h>

/* 
 * Main static functions 
 */
static void gnome_db_graph_item_class_init (GnomeDbGraphItemClass *class);
static void gnome_db_graph_item_init (GnomeDbGraphItem *graph);
static void gnome_db_graph_item_dispose (GObject *object);
static void gnome_db_graph_item_finalize (GObject *object);

static void gnome_db_graph_item_set_property (GObject *object,
					      guint param_id,
					      const GValue *value,
					      GParamSpec *pspec);
static void gnome_db_graph_item_get_property (GObject *object,
					      guint param_id,
					      GValue *value,
					      GParamSpec *pspec);

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

/* signals */
enum
{
	MOVED,
	LAST_SIGNAL
};

static gint gnome_db_graph_item_signals[LAST_SIGNAL] = { 0 };

/* properties */
enum
{
	PROP_0,
	PROP_GRAPH
};


struct _GnomeDbGraphItemPrivate
{
	GnomeDbGraph    *graph;
	gdouble          x;
	gdouble          y;
};

/* module error */
GQuark gnome_db_graph_item_error_quark (void)
{
	static GQuark quark;
	if (!quark)
		quark = g_quark_from_static_string ("gnome_db_graph_item_error");
	return quark;
}

GType
gnome_db_graph_item_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0)) {
		static const GTypeInfo info = {
			sizeof (GnomeDbGraphItemClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_graph_item_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbGraphItem),
			0,
			(GInstanceInitFunc) gnome_db_graph_item_init
		};
		
		type = g_type_register_static (G_TYPE_OBJECT, "GnomeDbGraphItem", &info, 0);
	}
	return type;
}

static void
gnome_db_graph_item_class_init (GnomeDbGraphItemClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);
	
	gnome_db_graph_item_signals[MOVED] =
		g_signal_new ("moved",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (GnomeDbGraphItemClass, moved),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID, G_TYPE_NONE, 0);

	object_class->dispose = gnome_db_graph_item_dispose;
	object_class->finalize = gnome_db_graph_item_finalize;

	/* Properties */
	object_class->set_property = gnome_db_graph_item_set_property;
	object_class->get_property = gnome_db_graph_item_get_property;

	g_object_class_install_property (object_class, PROP_GRAPH,
					 g_param_spec_object ("graph", NULL, NULL, 
							      GNOME_DB_TYPE_GRAPH,
							      (G_PARAM_READABLE | G_PARAM_CONSTRUCT_ONLY |
							       G_PARAM_WRITABLE)));
}

static void
gnome_db_graph_item_init (GnomeDbGraphItem *graph)
{
	graph->priv = g_new0 (GnomeDbGraphItemPrivate, 1);
	graph->priv->graph = NULL;
	graph->priv->x = 0.;
	graph->priv->y = 0.;
}

/**
 * gnome_db_graph_item_new
 * @graph: a #GnomeDbGraph graph object
 *
 * Creates a new #GnomeDbGraphItem object
 *
 * Returns: the newly created object
 */
GnomeDbGraphItem *
gnome_db_graph_item_new (GnomeDbGraph *graph)
{
	g_return_val_if_fail (GNOME_DB_IS_GRAPH (graph), NULL);

	return GNOME_DB_GRAPH_ITEM (g_object_new (GNOME_DB_TYPE_GRAPH_ITEM, "graph", graph, NULL));
}

static void
gnome_db_graph_item_dispose (GObject *object)
{
	GnomeDbGraphItem *graph;

	graph = GNOME_DB_GRAPH_ITEM (object);
	if (graph->priv) {		
		if (graph->priv->graph) {
			g_object_unref (graph->priv->graph);
                        graph->priv->graph = NULL;
                }
	}

	/* parent class */
	parent_class->dispose (object);
}


static void
gnome_db_graph_item_finalize (GObject   * object)
{
	GnomeDbGraphItem *graph;

	graph = GNOME_DB_GRAPH_ITEM (object);
	if (graph->priv) {
		g_free (graph->priv);
		graph->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}


static void 
gnome_db_graph_item_set_property (GObject *object,
				  guint param_id,
				  const GValue *value,
				  GParamSpec *pspec)
{
	GnomeDbGraphItem *item;

	item = GNOME_DB_GRAPH_ITEM (object);
	if (item->priv) {
		switch (param_id) {
		case PROP_GRAPH:
			item->priv->graph = g_value_get_object (value);
                        if (item->priv->graph) 
                                g_object_ref (item->priv->graph);
			break;
		}
	}
}

static void
gnome_db_graph_item_get_property (GObject *object,
				  guint param_id,
				  GValue *value,
				  GParamSpec *pspec)
{
	GnomeDbGraphItem *item;

	item = GNOME_DB_GRAPH_ITEM (object);
        if (item->priv) {
                switch (param_id) {
		case PROP_GRAPH:
			g_value_set_object (value, item->priv->graph);
			break;
		}
        }
}

/**
 * gnome_db_graph_item_set_position
 * @item: a #GnomeDbGraphItemItem object
 * @x:
 * @y:
 *
 * Sets the position to be remembered for @item.
 */
void
gnome_db_graph_item_set_position (GnomeDbGraphItem *item, gdouble x, gdouble y)
{
	g_return_if_fail (item && GNOME_DB_IS_GRAPH_ITEM (item));
	g_return_if_fail (item->priv);
	
	if ((item->priv->x == x) && (item->priv->y == y))
		return;

	item->priv->x = x;
	item->priv->y = y;
	
	TO_IMPLEMENT; /* Also in meta store */
#ifdef GNOME_DB_DEBUG_signal
	g_print (">> 'MOVED' from %s::%s()\n", __FILE__, __FUNCTION__);
#endif
	g_signal_emit (G_OBJECT (item), gnome_db_graph_item_signals[MOVED], 0);
#ifdef GNOME_DB_DEBUG_signal
	g_print ("<< 'MOVED' from %s::%s()\n", __FILE__, __FUNCTION__);
#endif
}

/**
 * gnome_db_graph_item_get_position
 * @item: a #GnomeDbGraphItemItem object
 * @x: a place where to store the X part of the position, or %NULL
 * @y: a place where to store the Y part of the position, or %NULL
 *
 * Get @item's position.
 */
void
gnome_db_graph_item_get_position (GnomeDbGraphItem *item, gdouble *x, gdouble *y)
{
	g_return_if_fail (GNOME_DB_IS_GRAPH_ITEM (item));
	g_return_if_fail (item->priv);
	
	if (x)
		*x = item->priv->x;
	if (y)
		*y = item->priv->y;
}


/**
 * gnome_db_graph_item_get_graph
 * 
 */
GnomeDbGraph *
gnome_db_graph_item_get_graph (GnomeDbGraphItem *item)
{
	g_return_val_if_fail (GNOME_DB_IS_GRAPH_ITEM (item), NULL);
	g_return_val_if_fail (item->priv, NULL);

	return item->priv->graph;
}
