/* gnome-db-canvas-item.c
 *
 * Copyright (C) 2007 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <gtk/gtk.h>
#include <libgda/libgda.h>
#include <libgnomedb/marshal.h>
#include "gnome-db-graph-item.h"
#include "gnome-db-canvas-item.h"
#include "gnome-db-canvas-tip.h"
#include "gnome-db-canvas-cursor.h"

static void gnome_db_canvas_item_class_init (GnomeDbCanvasItemClass * class);
static void gnome_db_canvas_item_init       (GnomeDbCanvasItem * item);
static void gnome_db_canvas_item_dispose    (GObject *object);


static void gnome_db_canvas_item_set_property (GObject *object,
					    guint param_id,
					    const GValue *value,
					    GParamSpec *pspec);
static void gnome_db_canvas_item_get_property (GObject *object,
					    guint param_id,
					    GValue *value,
					    GParamSpec *pspec);
static void m_drag_action (GnomeDbCanvasItem *citem, GnomeDbCanvasItem *dragged_from, GnomeDbCanvasItem * dragged_to);

struct _GnomeDbCanvasItemPrivate
{
	gboolean            moving;
	double              xstart;
	double              ystart;
	gboolean            allow_move;
	gboolean            allow_drag;
	gchar              *tooltip_text;
	GnomeDbGraphItem   *graph_item;
};

enum
{
	MOVED,
	MOVING,
	SHIFTED,
	DRAG_ACTION,
	DESTROY,
	LAST_SIGNAL
};

enum
{
	PROP_0,
	PROP_ALLOW_MOVE,
	PROP_ALLOW_DRAG,
	PROP_TOOLTIP_TEXT,
	PROP_GRAPH_ITEM
};

static gint gnome_db_canvas_item_signals[LAST_SIGNAL] = { 0, 0, 0, 0, 0 };

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *base_parent_class = NULL;

GType
gnome_db_canvas_item_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0)) {
		static const GTypeInfo info = {
			sizeof (GnomeDbCanvasItemClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_canvas_item_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbCanvasItem),
			0,
			(GInstanceInitFunc) gnome_db_canvas_item_init
		};
		type = g_type_register_static (GOO_TYPE_CANVAS_GROUP, "GnomeDbCanvasItem", &info, 0);
	}

	return type;
}


static void
gnome_db_canvas_item_class_init (GnomeDbCanvasItemClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	base_parent_class = g_type_class_peek_parent (class);

	gnome_db_canvas_item_signals[MOVED] =
		g_signal_new ("moved",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (GnomeDbCanvasItemClass, moved),
			      NULL, NULL,
			      gnome_db_marshal_VOID__VOID, G_TYPE_NONE,
			      0);
	gnome_db_canvas_item_signals[MOVING] =
		g_signal_new ("moving",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (GnomeDbCanvasItemClass, moving),
			      NULL, NULL,
			      gnome_db_marshal_VOID__VOID, G_TYPE_NONE,
			      0);
	gnome_db_canvas_item_signals[SHIFTED] =
		g_signal_new ("shifted",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (GnomeDbCanvasItemClass, shifted),
			      NULL, NULL,
			      gnome_db_marshal_VOID__VOID, G_TYPE_NONE,
			      0);
	gnome_db_canvas_item_signals[DRAG_ACTION] =
		g_signal_new ("drag_action",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (GnomeDbCanvasItemClass, drag_action),
			      NULL, NULL,
			      gnome_db_marshal_VOID__POINTER_POINTER,
			      G_TYPE_NONE, 2, G_TYPE_POINTER, G_TYPE_POINTER);
	gnome_db_canvas_item_signals[DESTROY] =
		g_signal_new ("destroy",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (GnomeDbCanvasItemClass, destroy),
			      NULL, NULL,
			      gnome_db_marshal_VOID__VOID, G_TYPE_NONE,
			      0);


	class->moved = NULL;
	class->moving = NULL;
	class->shifted = NULL;
	class->drag_action = m_drag_action;
	class->destroy = NULL;
	object_class->dispose = gnome_db_canvas_item_dispose;

	/* virtual funstionc */
	class->extra_event = NULL;

	/* Properties */
	object_class->set_property = gnome_db_canvas_item_set_property;
	object_class->get_property = gnome_db_canvas_item_get_property;

	g_object_class_install_property
                (object_class, PROP_ALLOW_MOVE,
                 g_param_spec_boolean ("allow_move", NULL, NULL, TRUE, (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	g_object_class_install_property
                (object_class, PROP_ALLOW_DRAG,
                 g_param_spec_boolean ("allow_drag", NULL, NULL, FALSE, (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	g_object_class_install_property
		(object_class, PROP_TOOLTIP_TEXT,
		 g_param_spec_string ("tip_text", NULL, NULL, NULL, (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	g_object_class_install_property (
		object_class, PROP_GRAPH_ITEM,
		g_param_spec_object ("graph_item", NULL, NULL, GNOME_DB_TYPE_GRAPH_ITEM, (G_PARAM_READABLE | G_PARAM_WRITABLE)));
}

static void
m_drag_action  (GnomeDbCanvasItem *citem, GnomeDbCanvasItem *dragged_from, GnomeDbCanvasItem *dragged_to)
{
	GooCanvasItem *parent;

	/* make sure the parent GnomeDbCanvasItem forwards the DND info... */
	g_object_get (G_OBJECT (citem), "parent", &parent, NULL);
	if (parent)
		g_object_unref (parent); /* we don't want to keep a reference on @parent */
	if (GNOME_DB_IS_CANVAS_ITEM (parent)) {
#ifdef debug_signal
		g_print (">> 'DRAG_ACTION' from %s::%s()\n", __FILE__, __FUNCTION__);
#endif
		g_signal_emit (G_OBJECT (parent), gnome_db_canvas_item_signals[DRAG_ACTION], 0, 
			       dragged_from, dragged_to);
#ifdef debug_signal
		g_print ("<< 'DRAG_ACTION' from %s::%s()\n", __FILE__, __FUNCTION__);
#endif
	}
}

static gboolean enter_notify_event (GnomeDbCanvasItem *citem, GooCanvasItem *target_item,
				    GdkEventCrossing *event, gpointer data);
static gboolean leave_notify_event (GnomeDbCanvasItem *citem, GooCanvasItem *target_item,
				    GdkEventCrossing *event, gpointer data);
static gboolean button_press_event (GnomeDbCanvasItem *citem, GooCanvasItem *target_item,
				    GdkEventButton *event, gpointer data);
static gboolean button_release_event (GnomeDbCanvasItem *citem, GooCanvasItem *target_item,
				      GdkEventButton *event, gpointer data);
static gboolean motion_notify_event (GnomeDbCanvasItem *citem, GooCanvasItem *target_item,
				     GdkEventMotion *event, gpointer data);


static void
gnome_db_canvas_item_init (GnomeDbCanvasItem * item)
{
	item->priv = g_new0 (GnomeDbCanvasItemPrivate, 1);
	item->priv->moving = FALSE;
	item->priv->xstart = 0;
	item->priv->ystart = 0;
	item->priv->allow_move = TRUE;
	item->priv->allow_drag = FALSE;
	item->priv->tooltip_text = NULL;
	item->priv->graph_item = NULL;
	
	g_signal_connect (G_OBJECT (item), "enter-notify-event",
			  G_CALLBACK (enter_notify_event), NULL);
	g_signal_connect (G_OBJECT (item), "leave-notify-event",
			  G_CALLBACK (leave_notify_event), NULL);
	g_signal_connect (G_OBJECT (item), "motion-notify-event",
			  G_CALLBACK (motion_notify_event), NULL);
	g_signal_connect (G_OBJECT (item), "button-press-event",
			  G_CALLBACK (button_press_event), NULL);
	g_signal_connect (G_OBJECT (item), "button-release-event",
			  G_CALLBACK (button_release_event), NULL);
	
}

static void release_graph_item (GnomeDbCanvasItem *citem, GnomeDbGraphItem *item);
static void graph_item_moved_cb (GnomeDbGraphItem *item, GnomeDbCanvasItem *citem);

static void
gnome_db_canvas_item_dispose (GObject *object)
{
	GnomeDbCanvasItem *citem;
	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOME_DB_IS_CANVAS_ITEM (object));
	
	citem = GNOME_DB_CANVAS_ITEM (object);
	if (citem->priv) {
		g_signal_emit (object, gnome_db_canvas_item_signals[DESTROY], 0);

		if (citem->priv->graph_item) {
			g_object_weak_unref (G_OBJECT (citem->priv->graph_item), 
					     (GWeakNotify) release_graph_item, citem);
			release_graph_item (citem, citem->priv->graph_item);
		}

		if (citem->priv->tooltip_text) 
			g_free (citem->priv->tooltip_text);

		g_free (citem->priv);
		citem->priv = NULL;
	}

	/* for the parent class */
	base_parent_class->dispose (object);
}

static void 
gnome_db_canvas_item_set_property    (GObject *object,
				guint param_id,
				const GValue *value,
				GParamSpec *pspec)
{
	GnomeDbCanvasItem *citem = NULL;
	const gchar *str = NULL;
	GObject* propobject = NULL;

	citem = GNOME_DB_CANVAS_ITEM (object);

	switch (param_id) {
	case PROP_ALLOW_MOVE:
		citem->priv->allow_move = g_value_get_boolean (value);
		if (citem->priv->allow_move && citem->priv->allow_drag)
			citem->priv->allow_drag = FALSE;
		break;
	case PROP_ALLOW_DRAG:
		citem->priv->allow_drag = g_value_get_boolean (value);
		if (citem->priv->allow_drag && citem->priv->allow_move)
			citem->priv->allow_move = FALSE;
		break;
	case PROP_TOOLTIP_TEXT:
		str = g_value_get_string (value);
		if (citem->priv->tooltip_text) {
			g_free (citem->priv->tooltip_text);
			citem->priv->tooltip_text = NULL;
		}
		if (str)
			citem->priv->tooltip_text = g_strdup (str);
		break;
	case PROP_GRAPH_ITEM:
		propobject = g_value_get_object (value);
		if (propobject == G_OBJECT (citem->priv->graph_item))
			return;

		if (citem->priv->graph_item) {
			release_graph_item (citem, citem->priv->graph_item);
			citem->priv->graph_item = NULL;
		}

		if (propobject) {
			g_object_weak_ref (G_OBJECT (propobject), (GWeakNotify) release_graph_item, citem);
			g_signal_connect (G_OBJECT (propobject), "moved",
					  G_CALLBACK (graph_item_moved_cb), citem);

			/* set the position according to the GnomeDbGraphItem */
			citem->priv->graph_item = GNOME_DB_GRAPH_ITEM (propobject);
			graph_item_moved_cb (citem->priv->graph_item, citem);
		}
	}
}

static void
gnome_db_canvas_item_get_property    (GObject *object,
				guint param_id,
				GValue *value,
				GParamSpec *pspec)
{
        GnomeDbCanvasItem *citem = NULL;
        
        citem = GNOME_DB_CANVAS_ITEM (object);
        switch (param_id) {
        case PROP_ALLOW_MOVE:
                g_value_set_boolean (value, citem->priv->allow_move);
                break;
        case PROP_ALLOW_DRAG:
                g_value_set_boolean (value, citem->priv->allow_drag);
                break;
        case PROP_TOOLTIP_TEXT:
                g_value_set_string (value, citem->priv->tooltip_text);
                break;
        case PROP_GRAPH_ITEM:
                g_value_set_object (value, citem->priv->graph_item);
                break;
        default:
                G_OBJECT_WARN_INVALID_PROPERTY_ID(object, param_id, pspec);
                break;
        }
        
}

/**
 * gnome_db_canvas_item_get_canvas
 * @item: a #GnomeDbCanvasItem object
 *
 * Get the #GnomeDbCanvas on which @item is drawn
 *
 * Returns: the #GnomeDbCanvas widget
 */
GnomeDbCanvas *
gnome_db_canvas_item_get_canvas (GnomeDbCanvasItem *item)
{
	g_return_val_if_fail (GNOME_DB_IS_CANVAS_ITEM (item), NULL);
	return (GnomeDbCanvas *) goo_canvas_item_get_canvas (GOO_CANVAS_ITEM (item));
}

/**
 * gnome_db_canvas_item_get_graph_item
 * @item: a #GnomeDbCanvasItem object
 *
 * Get the associated #GnomeDbGraphItem to @item.
 *
 * Returns: the #GnomeDbGraphItem, or %NULL
 */
GnomeDbGraphItem *
gnome_db_canvas_item_get_graph_item (GnomeDbCanvasItem *item)
{
	g_return_val_if_fail (GNOME_DB_IS_CANVAS_ITEM (item), NULL);
	g_return_val_if_fail (item->priv, NULL);

	return item->priv->graph_item;
}

/**
 * gnome_db_canvas_item_get_edge_nodes
 * @item: a #GnomeDbCanvasItem object
 * @from: a place to store the FROM part of the edge, or %NULL
 * @to: a place to store the TO part of the edge, or %NULL
 *
 * If the @item canvas item represents a "link" between two other canvas items (an edge), then
 * set @from and @to to those items.
 */
void 
gnome_db_canvas_item_get_edge_nodes (GnomeDbCanvasItem *item, 
				  GnomeDbCanvasItem **from, GnomeDbCanvasItem **to)
{
	GnomeDbCanvasItemClass *class;

	g_return_if_fail (GNOME_DB_IS_CANVAS_ITEM (item));
	g_return_if_fail (item->priv);

	class = GNOME_DB_CANVAS_ITEM_CLASS (G_OBJECT_GET_CLASS (item));
	if (class->get_edge_nodes)
		(class->get_edge_nodes) (item, from, to);
	else {
		if (from)
			*from = NULL;
		if (to)
			*to = NULL;
	}
}


static void
release_graph_item (GnomeDbCanvasItem *citem, GnomeDbGraphItem *item)
{
	g_assert (citem->priv->graph_item == item);
	g_signal_handlers_disconnect_by_func (G_OBJECT (item),
					      G_CALLBACK (graph_item_moved_cb), citem);
	citem->priv->graph_item = NULL;
}

static void
graph_item_moved_cb (GnomeDbGraphItem *item, GnomeDbCanvasItem *citem)
{
	gdouble x, y;
	GooCanvasBounds bounds;

	g_assert (citem->priv->graph_item == item);
	gnome_db_graph_item_get_position (item, &x, &y);

	goo_canvas_item_get_bounds (GOO_CANVAS_ITEM (citem), &bounds);
	goo_canvas_item_translate (GOO_CANVAS_ITEM (citem), x-bounds.x1, y-bounds.y1);

#ifdef debug_signal
	g_print (">> 'SHIFTED' from %s::graph_item_moved_cb()\n", __FILE__);
#endif
	g_signal_emit (G_OBJECT (citem), gnome_db_canvas_item_signals[SHIFTED], 0);
#ifdef debug_signal
	g_print ("<< 'SHIFTED' from %s::graph_item_moved_cb()\n", __FILE__);
#endif
}

static void end_of_drag_cb (GnomeDbCanvasItem *citem, GObject *cursor);
static void end_of_drag_cb_d (GooCanvasItem *ci, GnomeDbCanvasItem *citem);
static gboolean add_tip_timeout (GnomeDbCanvasItem *citem);
static gboolean display_tip_timeout (GnomeDbCanvasItem *citem);

static gboolean
enter_notify_event (GnomeDbCanvasItem *citem, GooCanvasItem *target_item,
		    GdkEventCrossing *event, gpointer data)
{
	GooCanvasItem *root;
	GooCanvasItem *ci;

	/* Drag management */
	root = goo_canvas_get_root_item (goo_canvas_item_get_canvas (GOO_CANVAS_ITEM (citem)));
	ci = g_object_get_data (G_OBJECT (root), "dragged_from");
	if (ci && GNOME_DB_IS_CANVAS_ITEM (ci)) {
#ifdef debug_signal
		g_print (">> 'DRAG_ACTION' from %s::%s()\n", __FILE__, __FUNCTION__);
#endif
		g_signal_emit (G_OBJECT (citem), gnome_db_canvas_item_signals[DRAG_ACTION], 0, ci, citem);
#ifdef debug_signal
		g_print ("<< 'DRAG_ACTION' from %s::%s()\n", __FILE__, __FUNCTION__);
#endif
		g_object_set_data (G_OBJECT (root), "dragged_from", NULL);
	}
	return FALSE;
}

static gboolean
leave_notify_event (GnomeDbCanvasItem *citem, GooCanvasItem *target_item,
		    GdkEventCrossing *event, gpointer data)
{
	GObject *obj;
	guint id;

	/* remove tooltip */
	obj = g_object_get_data (G_OBJECT (citem), "tip");
	if (obj) {
		goo_canvas_item_remove (GOO_CANVAS_ITEM (obj));
		g_object_set_data (G_OBJECT (citem), "tip", NULL);
	}
	
	id = GPOINTER_TO_UINT (g_object_get_data (G_OBJECT (citem), "addtipid"));
	if (id != 0) {
		g_source_remove (id);
		g_object_set_data (G_OBJECT (citem), "addtipid", NULL);
	}
	id = GPOINTER_TO_UINT (g_object_get_data (G_OBJECT (citem), "displaytipid"));
	if (id != 0) {
		g_source_remove (id);
		g_object_set_data (G_OBJECT (citem), "displaytipid", NULL);
	}

	return FALSE;
}

static gboolean
button_press_event (GnomeDbCanvasItem *citem, GooCanvasItem *target_item,
		    GdkEventButton *event, gpointer data)
{
	GnomeDbCanvasItemClass *class = GNOME_DB_CANVAS_ITEM_CLASS (G_OBJECT_GET_CLASS (citem));
	gboolean done = FALSE;
	GooCanvasItem *ci;
	GObject *obj;
	guint id;

	switch (event->button) {
	case 1:
		if (citem->priv->allow_move) {
			/* movement management */
			goo_canvas_item_raise (GOO_CANVAS_ITEM (citem), NULL);
			citem->priv->xstart = event->x;
			citem->priv->ystart = event->y;
			citem->priv->moving = TRUE;
			done = TRUE;
		}
		if (citem->priv->allow_drag) {
			GooCanvasBounds bounds;
			GooCanvas *canvas = goo_canvas_item_get_canvas (GOO_CANVAS_ITEM (citem));
			
			goo_canvas_item_get_bounds (GOO_CANVAS_ITEM (citem), &bounds);

			/* remove tooltip */
			obj = g_object_get_data (G_OBJECT (citem), "tip");
			if (obj) {
				goo_canvas_item_remove (GOO_CANVAS_ITEM (obj));
				g_object_set_data (G_OBJECT (citem), "tip", NULL);
			}
			id = GPOINTER_TO_UINT (g_object_get_data (G_OBJECT (citem), "addtipid"));
			if (id != 0) {
				g_source_remove (id);
				g_object_set_data (G_OBJECT (citem), "addtipid", NULL);
			}
			id = GPOINTER_TO_UINT (g_object_get_data (G_OBJECT (citem), "displaytipid"));
			if (id != 0) {
				g_source_remove (id);
				g_object_set_data (G_OBJECT (citem), "displaytipid", NULL);
			}
			
			/* start dragging */
			if (class->extra_event)
				(class->extra_event) (citem, GDK_LEAVE_NOTIFY);
			
			ci = gnome_db_canvas_cursor_new (goo_canvas_get_root_item (canvas),
						      "x", bounds.x1,
						      "y", bounds.y1,
						      "width", bounds.x2 - bounds.x1,
						      "height", bounds.y2 - bounds.y1,
						      "fill-color-rgba", 0x3cb3f1A0,
						      "line-width", .5,
						      NULL);
			g_object_weak_ref (G_OBJECT (ci), (GWeakNotify) end_of_drag_cb, citem);
			
			/* this weak ref is in case citem is destroyed before ci */
			g_object_weak_ref (G_OBJECT (citem), (GWeakNotify) end_of_drag_cb_d, ci);
			
			GNOME_DB_CANVAS_ITEM (ci)->priv->xstart = bounds.x1 + event->x;
			GNOME_DB_CANVAS_ITEM (ci)->priv->ystart = bounds.y1 + event->y;
			GNOME_DB_CANVAS_ITEM (ci)->priv->moving = TRUE;
			goo_canvas_item_raise (GOO_CANVAS_ITEM (ci), NULL);
			goo_canvas_pointer_grab (canvas, GOO_CANVAS_ITEM (ci), 
						 GDK_POINTER_MOTION_MASK | GDK_BUTTON_RELEASE_MASK,
						 NULL, GDK_CURRENT_TIME);
			done = TRUE;
		}
		break;
	default:
		break;
	}

	return done;
}

static gboolean
button_release_event (GnomeDbCanvasItem *citem, GooCanvasItem *target_item,
		      GdkEventButton *event, gpointer data)
{
	if (citem->priv->allow_move) {
		citem->priv->moving = FALSE;
		if (citem->priv->graph_item) {
			GooCanvasBounds bounds;
			goo_canvas_item_get_bounds (GOO_CANVAS_ITEM (citem), &bounds);
			gnome_db_graph_item_set_position (citem->priv->graph_item, bounds.x1, bounds.y1);
		}
#ifdef debug_signal
		g_print (">> 'MOVED' from %s::item_event()\n", __FILE__);
#endif
		g_signal_emit (G_OBJECT (citem), gnome_db_canvas_item_signals[MOVED], 0);
#ifdef debug_signal
		g_print ("<< 'MOVED' from %s::item_event()\n", __FILE__);
#endif
	}
	
	return FALSE;
}

static gboolean
motion_notify_event (GnomeDbCanvasItem *citem, GooCanvasItem *target_item,
		     GdkEventMotion *event, gpointer data)
{
	guint id;
	gdouble pos;
	gboolean retval = FALSE;

	if (citem->priv->moving && (event->state & GDK_BUTTON1_MASK)) {
		g_assert (GNOME_DB_IS_CANVAS_ITEM (citem));
		goo_canvas_item_translate (GOO_CANVAS_ITEM (citem), 
					   (gdouble) event->x - citem->priv->xstart, 
					   (gdouble) event->y - citem->priv->ystart);
		g_signal_emit (G_OBJECT (citem), gnome_db_canvas_item_signals[MOVING], 0);
		retval = TRUE;
	}
	else {
		if (! g_object_get_data (G_OBJECT (citem), "tip")) {
			/* set tooltip */
			GooCanvasBounds bounds;

			id = GPOINTER_TO_UINT (g_object_get_data (G_OBJECT (citem), "addtipid"));
			if (id != 0) {
				g_source_remove (id);
				g_object_set_data (G_OBJECT (citem), "addtipid", NULL);
			}
			id = GPOINTER_TO_UINT (g_object_get_data (G_OBJECT (citem), "displaytipid"));
			if (id != 0) {
				g_source_remove (id);
				g_object_set_data (G_OBJECT (citem), "displaytipid", NULL);
			}
			id = g_timeout_add (200, (GSourceFunc) add_tip_timeout, citem);
			g_object_set_data (G_OBJECT (citem), "addtipid", GUINT_TO_POINTER (id));

			goo_canvas_item_get_bounds (GOO_CANVAS_ITEM (citem), &bounds);

			pos = event->x + bounds.x1;
			g_object_set_data (G_OBJECT (citem), "mousex", GINT_TO_POINTER ((gint) pos));
			pos = event->y + bounds.y1;
			g_object_set_data (G_OBJECT (citem), "mousey", GINT_TO_POINTER ((gint) pos));
		}
	}
	
	return retval;
}

static void 
end_of_drag_cb (GnomeDbCanvasItem *citem, GObject *cursor)
{
	GooCanvasItem *root;

	/* Drag management */
	root = goo_canvas_get_root_item (goo_canvas_item_get_canvas (GOO_CANVAS_ITEM (citem)));
	g_object_set_data (G_OBJECT (root), "dragged_from", citem);
	g_object_weak_unref (G_OBJECT (citem), (GWeakNotify) end_of_drag_cb_d, cursor);
}

static void
end_of_drag_cb_d (GooCanvasItem *ci, GnomeDbCanvasItem *citem)
{
	g_object_weak_unref (G_OBJECT (ci), (GWeakNotify) end_of_drag_cb, citem);
}

static gboolean add_tip_timeout (GnomeDbCanvasItem *citem)
{
        guint id;

        id = gtk_timeout_add (100, (GSourceFunc) display_tip_timeout, citem);
        g_object_set_data (G_OBJECT (citem), "addtipid", NULL);
        g_object_set_data (G_OBJECT (citem), "displaytipid", GUINT_TO_POINTER (id));
        return FALSE;
}


static void tip_destroy (GnomeDbCanvasItem *citem, GObject *tip);
static gboolean display_tip_timeout (GnomeDbCanvasItem *citem)
{
        GooCanvasItem *tip;
        gdouble x, y;

	if (citem->priv->tooltip_text) {
		/* display the tip */
		g_object_set_data (G_OBJECT (citem), "displaytipid", NULL);
		x = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (citem), "mousex"));
		y = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (citem), "mousey"));

		tip = gnome_db_canvas_tip_new (goo_canvas_get_root_item (goo_canvas_item_get_canvas (GOO_CANVAS_ITEM (citem))),
					    citem->priv->tooltip_text, x + 7., y + 3., NULL);
		goo_canvas_item_raise (tip, NULL);
		g_object_weak_ref (G_OBJECT (tip), (GWeakNotify) tip_destroy, citem);

		g_object_set_data (G_OBJECT (citem), "tip", tip);
	}
	return FALSE;
}

static void tip_destroy (GnomeDbCanvasItem *citem, GObject *tip)
{
        g_object_set_data (G_OBJECT (citem), "tip", NULL);
}
