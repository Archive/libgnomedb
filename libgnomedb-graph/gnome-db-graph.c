/* gnome-db-graph.c
 *
 * Copyright (C) 2004 - 2008 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <string.h>
#include <glib/gi18n-lib.h>
#include <glib-object.h>
#include "gnome-db-graph.h"
#include "gnome-db-graph-item.h"

/* 
 * Main static functions 
 */
static void gnome_db_graph_class_init (GnomeDbGraphClass *class);
static void gnome_db_graph_init (GnomeDbGraph *graph);
static void gnome_db_graph_dispose (GObject *object);
static void gnome_db_graph_finalize (GObject *object);

static void gnome_db_graph_set_property (GObject *object,
					 guint param_id,
					 const GValue *value,
					 GParamSpec *pspec);
static void gnome_db_graph_get_property (GObject *object,
					 guint param_id,
					 GValue *value,
					 GParamSpec *pspec);


static void graph_item_moved_cb (GnomeDbGraphItem *item, GnomeDbGraph *graph);

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

/* signals */
enum
{
	ITEM_ADDED,
	ITEM_DROPPED,
	ITEM_MOVED,
	LAST_SIGNAL
};

static gint gnome_db_graph_signals[LAST_SIGNAL] = { 0, 0 };

/* properties */
enum
{
	PROP_0,
	PROP_META_STORE,
	PROP_STORE_ID
};


struct _GnomeDbGraphPrivate
{
	guint             store_id;
	gboolean          store_id_set;
	GdaMetaStore     *store;
	GSList           *graph_items;
};

/* module error */
GQuark gnome_db_graph_error_quark (void)
{
	static GQuark quark;
	if (!quark)
		quark = g_quark_from_static_string ("gnome_db_graph_error");
	return quark;
}


GType
gnome_db_graph_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0)) {
		static const GTypeInfo info = {
			sizeof (GnomeDbGraphClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_graph_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbGraph),
			0,
			(GInstanceInitFunc) gnome_db_graph_init
		};
		
		type = g_type_register_static (G_TYPE_OBJECT, "GnomeDbGraph", &info, 0);
	}
	return type;
}


static void
gnome_db_graph_class_init (GnomeDbGraphClass *klass)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);
	
	gnome_db_graph_signals[ITEM_ADDED] =
		g_signal_new ("item_added",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (GnomeDbGraphClass, item_added),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__OBJECT, G_TYPE_NONE,
			      1, GNOME_DB_TYPE_GRAPH_ITEM);
	gnome_db_graph_signals[ITEM_DROPPED] =
		g_signal_new ("item_dropped",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (GnomeDbGraphClass, item_dropped),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__OBJECT, G_TYPE_NONE,
			      1, GNOME_DB_TYPE_GRAPH_ITEM);
	gnome_db_graph_signals[ITEM_MOVED] =
		g_signal_new ("item_moved",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (GnomeDbGraphClass, item_moved),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__OBJECT, G_TYPE_NONE,
			      1, GNOME_DB_TYPE_GRAPH_ITEM);
	
	klass->item_added = NULL;
	klass->item_dropped = NULL;
	klass->item_moved = NULL;

	object_class->dispose = gnome_db_graph_dispose;
	object_class->finalize = gnome_db_graph_finalize;

	/* Properties */
	object_class->set_property = gnome_db_graph_set_property;
	object_class->get_property = gnome_db_graph_get_property;

	g_object_class_install_property (object_class, PROP_META_STORE,
                                         g_param_spec_object ("meta-store",
							      _("GdaMetaStore"),
                                                              NULL, GDA_TYPE_META_STORE,
                                                              G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY |
                                                              G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_STORE_ID,
                                         g_param_spec_uint ("id",
							    _("Id of the graph in GdaMetaStore"), NULL,
							    0, G_MAXUINT, G_MAXUINT,
							    G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY |
							    G_PARAM_READABLE));
}

static void
gnome_db_graph_init (GnomeDbGraph *graph)
{
	graph->priv = g_new0 (GnomeDbGraphPrivate, 1);
	graph->priv->store = NULL;
	graph->priv->graph_items = NULL;
	graph->priv->store_id = 0;
	graph->priv->store_id_set = FALSE;
}

/**
 * gnome_db_graph_new
 * @store: a #GdaMetastore object, or %NULL
 * @id: an existing ID, or G_MAXUINT if ID is to be determined by the library
 *
 * Creates a new #GnomeDbGraph object, which will be stored in @store.
 *
 * Returns: the newly created object
 */
GnomeDbGraph *
gnome_db_graph_new (GdaMetaStore *store, guint id)
{
	g_return_val_if_fail (!store || GDA_IS_META_STORE (store), NULL);

	return GNOME_DB_GRAPH (g_object_new (GNOME_DB_TYPE_GRAPH, "meta-store", store, "id", id, NULL));
}

static void
gnome_db_graph_dispose (GObject *object)
{
	GnomeDbGraph *graph;

	graph = GNOME_DB_GRAPH (object);
	if (graph->priv->graph_items) {
		g_slist_foreach (graph->priv->graph_items, (GFunc) g_object_unref, NULL);
		g_slist_free (graph->priv->graph_items);
		graph->priv->graph_items = NULL;
	}

	/* parent class */
	parent_class->dispose (object);
}

static void
gnome_db_graph_finalize (GObject   * object)
{
	GnomeDbGraph *graph;

	graph = GNOME_DB_GRAPH (object);
	if (graph->priv) {
		g_free (graph->priv);
		graph->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}


static void 
gnome_db_graph_set_property (GObject *object,
		       guint param_id,
		       const GValue *value,
		       GParamSpec *pspec)
{
	GnomeDbGraph *graph;

	graph = GNOME_DB_GRAPH (object);
	if (graph->priv) {
		switch (param_id) {
		case PROP_META_STORE:
                        graph->priv->store = g_value_get_object (value);
                        if (graph->priv->store) {
                                g_object_ref (graph->priv->store);
				TO_IMPLEMENT; /* check that meta store has the "_graphs" table */
			}
                        break;
		case PROP_STORE_ID: 
			graph->priv->store_id = g_value_get_uint (value);
			graph->priv->store_id_set = TRUE;
			break;
		}
	}

	if (graph->priv->store && graph->priv->store_id_set) {
		if (graph->priv->store_id == G_MAXUINT) {
			/* no ID specified => create a new entry in store */
			TO_IMPLEMENT;
		}
		else {
			/* ID specified => load it from store */
			TO_IMPLEMENT;
		}
	}
}

static void
gnome_db_graph_get_property (GObject *object,
		       guint param_id,
		       GValue *value,
		       GParamSpec *pspec)
{
	GnomeDbGraph *graph;

	graph = GNOME_DB_GRAPH (object);
        if (graph->priv) {
                switch (param_id) {
		case PROP_META_STORE:
                        g_value_set_object (value, graph->priv->store);
                        break;
		case PROP_STORE_ID:
			g_value_set_uint (value, graph->priv->store_id);
			break;
		}
        }
}

/**
 * gnome_db_graph_get_items
 * @graph: a #GnomeDbGraph object
 *
 * Get a list of #GnomeDbGraphItem objects which are items of @graph
 *
 * Returns: a new list of #GnomeDbGraphItem objects
 */
GSList *
gnome_db_graph_get_items (GnomeDbGraph *graph)
{
	g_return_val_if_fail (graph && GNOME_DB_IS_GRAPH (graph), NULL);
	g_return_val_if_fail (graph->priv, NULL);

	if (graph->priv->graph_items)
		return g_slist_copy (graph->priv->graph_items);
	else
		return NULL;
}

/**
 * gnome_db_graph_add_item
 * @graph: a #GnomeDbGraph object
 * @item: a #GnomeDbGraphItem object
 *
 * Adds @item to @graph.
 */
void
gnome_db_graph_add_item (GnomeDbGraph *graph, GnomeDbGraphItem *item)
{
	g_return_if_fail (graph && GNOME_DB_IS_GRAPH (graph));
	g_return_if_fail (graph->priv);
	g_return_if_fail (GNOME_DB_IS_GRAPH_ITEM (item));

	g_object_ref (G_OBJECT (item));
	graph->priv->graph_items = g_slist_append (graph->priv->graph_items, item);

	g_signal_connect (G_OBJECT (item), "moved",
			  G_CALLBACK (graph_item_moved_cb), graph);

	if (graph->priv->store)
		TO_IMPLEMENT; /* In meta store as well */

#ifdef GNOME_DB_DEBUG_signal
	g_print (">> 'ITEM_ADDED' from %s::%s()\n", __FILE__, __FUNCTION__);
#endif
	g_signal_emit (G_OBJECT (graph), gnome_db_graph_signals [ITEM_ADDED], 0, item);
#ifdef GNOME_DB_DEBUG_signal
	g_print ("<< 'ITEM_ADDED' from %s::%s()\n", __FILE__, __FUNCTION__);
#endif
}

static void
graph_item_moved_cb (GnomeDbGraphItem *item, GnomeDbGraph *graph)
{
#ifdef GNOME_DB_DEBUG_signal
	g_print (">> 'ITEM_MOVED' from %s::%s()\n", __FILE__, __FUNCTION__);
#endif
	g_signal_emit (G_OBJECT (graph), gnome_db_graph_signals [ITEM_MOVED], 0, item);
#ifdef GNOME_DB_DEBUG_signal
	g_print ("<< 'ITEM_MOVED' from %s::%s()\n", __FILE__, __FUNCTION__);
#endif
}

/**
 * gnome_db_graph_del_item
 * @graph: a #GnomeDbGraph object
 * @item: a #GnomeDbGraphItem object
 *
 * Removes @item from @graph
 */
void
gnome_db_graph_del_item (GnomeDbGraph *graph, GnomeDbGraphItem *item)
{
	g_return_if_fail (graph && GNOME_DB_IS_GRAPH (graph));
	g_return_if_fail (graph->priv);
	g_return_if_fail (GNOME_DB_IS_GRAPH_ITEM (item));

	g_signal_handlers_disconnect_by_func (G_OBJECT (item),
                                              G_CALLBACK (graph_item_moved_cb) , graph);
	
        graph->priv->graph_items = g_slist_remove (graph->priv->graph_items, item);
	if (graph->priv->store)
		TO_IMPLEMENT; /* In meta store as well */

#ifdef GDA_DEBUG_signal
        g_print (">> 'ITEM_DROPPED' from %s::%s()\n", __FILE__, __FUNCTION__);
#endif
        g_signal_emit (G_OBJECT (graph), gnome_db_graph_signals [ITEM_DROPPED], 0, item);
#ifdef GDA_DEBUG_signal
        g_print ("<< 'ITEM_DROPPED' from %s::%s()\n", __FILE__, __FUNCTION__);
#endif
        g_object_unref (G_OBJECT (item));
}

/**
 * gnome_db_graph_delete
 *
 * Remove a graph from the meta store.
 */
void
gnome_db_graph_delete (GdaMetaStore *store, guint id)
{
	TO_IMPLEMENT;
}
