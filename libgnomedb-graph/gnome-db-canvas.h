/* gnome-db-canvas.h
 *
 * Copyright (C) 2007 - 2008 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNOME_DB_CANVAS__
#define __GNOME_DB_CANVAS__

#include <goocanvas.h>
#include <libgda/gda-decl.h>
#include <libgnomedb-graph/gnome-db-canvas-decl.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_CANVAS          (gnome_db_canvas_get_type())
#define GNOME_DB_CANVAS(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, gnome_db_canvas_get_type(), GnomeDbCanvas)
#define GNOME_DB_CANVAS_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, gnome_db_canvas_get_type (), GnomeDbCanvasClass)
#define GNOME_DB_IS_CANVAS(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, gnome_db_canvas_get_type ())

typedef enum {
	GNOME_DB_CANVAS_LAYOUT_DEFAULT,
	GNOME_DB_CANVAS_LAYOUT_RADIAL
} GnomeDbCanvasLayoutAlgorithm;

/* struct for the object's data */
struct _GnomeDbCanvas
{
	GooCanvas           widget;

	/* pointer position when a context menu was last opened */
	gdouble             xmouse;
	gdouble             ymouse;
	/* private */
	GnomeDbCanvasPrivate  *priv;
};

/* struct for the object's class */
struct _GnomeDbCanvasClass
{
	GooCanvasClass      parent_class;

	/* virtual functions */
	void           (*create_canvas_items) (GnomeDbCanvas *canvas); /* create individual GnomeDbCanvasItem objects */
	void           (*clean_canvas_items)  (GnomeDbCanvas *canvas); /* clean any extra structure, not the individual items */

	void           (*graph_item_added)    (GnomeDbCanvas *canvas, GnomeDbGraphItem *item);
	void           (*graph_item_dropped)  (GnomeDbCanvas *canvas, GnomeDbGraphItem *item);

	GtkWidget     *(*build_context_menu)  (GnomeDbCanvas *canvas);

	/* signals */
	void           (*drag_action)        (GnomeDbCanvas *canvas, GnomeDbCanvasItem *from_item, GnomeDbCanvasItem *to_item);
};

/* generic widget's functions */
GType              gnome_db_canvas_get_type                (void) G_GNUC_CONST;

GnomeDbGraph      *gnome_db_canvas_get_graph               (GnomeDbCanvas *canvas);
GtkWidget         *gnome_db_canvas_set_in_scrolled_window  (GnomeDbCanvas *canvas);
void               gnome_db_canvas_declare_item            (GnomeDbCanvas *canvas, GnomeDbCanvasItem *item);

void               gnome_db_canvas_set_zoom_factor         (GnomeDbCanvas *canvas, gdouble n);
gdouble            gnome_db_canvas_get_zoom_factor         (GnomeDbCanvas *canvas);
gdouble            gnome_db_canvas_fit_zoom_factor         (GnomeDbCanvas *canvas);
gboolean           gnome_db_canvas_auto_layout_enabled     (GnomeDbCanvas *canvas);
void               gnome_db_canvas_perform_auto_layout     (GnomeDbCanvas *canvas, gboolean animate, 
							    GnomeDbCanvasLayoutAlgorithm algorithm);
void               gnome_db_canvas_center                  (GnomeDbCanvas *canvas);

#define GNOME_DB_CANVAS_ENTITY_COLOR      "yellow"
#define GNOME_DB_CANVAS_DB_TABLE_COLOR    "lightblue"
#define GNOME_DB_CANVAS_QUERY_COLOR       "lightgreen"

G_END_DECLS

#endif
