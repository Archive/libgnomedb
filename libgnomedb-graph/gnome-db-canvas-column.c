/* gnome-db-canvas-column.c
 *
 * Copyright (C) 2002 - 2008 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <libgda/libgda.h>
#include <glib/gi18n-lib.h>
#include "gnome-db-canvas.h"
#include "gnome-db-canvas-column.h"
#include "gnome-db-canvas-table.h"

static void gnome_db_canvas_column_class_init (GnomeDbCanvasColumnClass * class);
static void gnome_db_canvas_column_init       (GnomeDbCanvasColumn * drag);
static void gnome_db_canvas_column_dispose   (GObject *object);

static void gnome_db_canvas_column_set_property (GObject *object,
					     guint param_id,
					     const GValue *value,
					     GParamSpec *pspec);
static void gnome_db_canvas_column_get_property (GObject *object,
					     guint param_id,
					     GValue *value,
					     GParamSpec *pspec);

static void gnome_db_canvas_column_extra_event  (GnomeDbCanvasItem * citem, GdkEventType event_type);
enum
{
	PROP_0,
	PROP_COLUMN,
};

struct _GnomeDbCanvasColumnPrivate
{
	GdaMetaTableColumn *column;
};

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *column_parent_class = NULL;

GType
gnome_db_canvas_column_get_type (void)
{
	static GType type = 0;

        if (G_UNLIKELY (type == 0)) {
		static const GTypeInfo info = {
			sizeof (GnomeDbCanvasColumnClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_canvas_column_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbCanvasColumn),
			0,
			(GInstanceInitFunc) gnome_db_canvas_column_init
		};		

		type = g_type_register_static (GNOME_DB_TYPE_CANVAS_TEXT, "GnomeDbCanvasColumn", &info, 0);
	}

	return type;
}

	

static void
gnome_db_canvas_column_class_init (GnomeDbCanvasColumnClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	column_parent_class = g_type_class_peek_parent (class);

	object_class->dispose = gnome_db_canvas_column_dispose;
	GNOME_DB_CANVAS_ITEM_CLASS (class)->extra_event = gnome_db_canvas_column_extra_event;

	/* Properties */
	object_class->set_property = gnome_db_canvas_column_set_property;
	object_class->get_property = gnome_db_canvas_column_get_property;

	g_object_class_install_property
                (object_class, PROP_COLUMN,
                 g_param_spec_pointer ("column", NULL, NULL, 
				       (G_PARAM_READABLE | G_PARAM_WRITABLE)));
}

static void
gnome_db_canvas_column_init (GnomeDbCanvasColumn * column)
{
	column->priv = g_new0 (GnomeDbCanvasColumnPrivate, 1);
	column->priv->column = NULL;
}

static void
gnome_db_canvas_column_dispose (GObject   * object)
{
	GnomeDbCanvasColumn *cf;
	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOME_DB_IS_CANVAS_COLUMN (object));

	cf = GNOME_DB_CANVAS_COLUMN (object);
	if (cf->priv) {
		if (cf->priv->column)
			cf->priv->column = NULL;
		g_free (cf->priv);
		cf->priv = NULL;
	}

	/* for the parent class */
	column_parent_class->dispose (object);
}

static void 
gnome_db_canvas_column_set_property (GObject *object,
				 guint param_id,
				 const GValue *value,
				 GParamSpec *pspec)
{
	GnomeDbCanvasColumn *cf = NULL;
	GdaMetaTableColumn* column = NULL;
	GString *string = NULL;

	cf = GNOME_DB_CANVAS_COLUMN (object);

	switch (param_id) {
	case PROP_COLUMN:
		column = g_value_get_pointer (value);

		cf->priv->column = column;
		/* column name */
		g_object_set (object, "text", column->column_name, NULL);
		
		/* attributes setting */
		string = g_string_new ("");
		if (column->column_type)
			g_string_append_printf (string, _("Type: %s"), column->column_type);
		
		g_object_set (object, 
			      "highlight_color", GNOME_DB_CANVAS_DB_TABLE_COLOR, 
			      "text_underline", !column->nullok,
			      "text_bold", column->pkey,
			      NULL);
		
		if (*string->str)
			g_object_set (object, "tip_text", string->str, NULL);
		else
			g_object_set (object, "tip_text", NULL, NULL);
		g_string_free (string, TRUE);
		break;
	}
}

static void 
gnome_db_canvas_column_get_property    (GObject *object,
				 guint param_id,
				 GValue *value,
				 GParamSpec *pspec)
{
	GnomeDbCanvasColumn *cf;

	cf = GNOME_DB_CANVAS_COLUMN (object);

	switch (param_id) {
	case PROP_COLUMN:
		g_value_set_pointer (value, cf->priv->column);
		break;
	}
}

static void
gnome_db_canvas_column_extra_event  (GnomeDbCanvasItem *citem, GdkEventType event_type)
{
	if (event_type == GDK_LEAVE_NOTIFY)
		gnome_db_canvas_text_set_highlight (GNOME_DB_CANVAS_TEXT (citem), FALSE);
}

/**
 * gnome_db_canvas_column_new
 * @parent: the parent item, or %NULL
 * @column: the represented entity's column
 * @x: the x coordinate of the text
 * @y: the y coordinate of the text
 * @...: optional pairs of property names and values, and a terminating %NULL.
 *
 * Creates a new #GnomeDbCanvasColumn object
 */
GooCanvasItem*
gnome_db_canvas_column_new (GooCanvasItem *parent, GdaMetaTableColumn *column,
			gdouble x, gdouble y, ...)
{
	GooCanvasItem *item;
	GnomeDbCanvasColumn *goocolumn;
	const char *first_property;
	va_list var_args;

	item = g_object_new (GNOME_DB_TYPE_CANVAS_COLUMN, NULL);
	goocolumn = (GnomeDbCanvasColumn*) item;

	if (parent) {
		goo_canvas_item_add_child (parent, item, -1);
		g_object_unref (item);
	}

	va_start (var_args, y);
	first_property = va_arg (var_args, char*);
	if (first_property)
		g_object_set_valist ((GObject*) item, first_property, var_args);
	va_end (var_args);

	g_object_set (G_OBJECT (item), "column", column, NULL);
	goo_canvas_item_translate (item, x, y);
	
	return item;
}


/**
 * gnome_db_canvas_column_get_column
 * @column: a #GnomeDbCanvasColumn object
 *
 * Get the #GdaMetaTableColumn which @column represents
 *
 * Returns: the object implementing the #GdaMetaTableColumn interface
 */
GdaMetaTableColumn *
gnome_db_canvas_column_get_column (GnomeDbCanvasColumn *column)
{
	g_return_val_if_fail (GNOME_DB_IS_CANVAS_COLUMN (column), NULL);
	g_return_val_if_fail (column->priv, NULL);

	return column->priv->column;
}


/**
 * gnome_db_canvas_column_get_parent_item
 * @column: a #GnomeDbCanvasColumn object
 *
 * Get the #GnomeDbCanvasTable in which @column is
 *
 * Returns: the #GnomeDbCanvasTable in which @column is, or %NULL
 */
GnomeDbCanvasTable *
gnome_db_canvas_column_get_parent_item (GnomeDbCanvasColumn *column)
{
	GooCanvasItem *ci;

	g_return_val_if_fail (GNOME_DB_IS_CANVAS_COLUMN (column), NULL);
	for (ci = goo_canvas_item_get_parent (GOO_CANVAS_ITEM (column));
	     ci && !GNOME_DB_IS_CANVAS_TABLE (ci);
	     ci = goo_canvas_item_get_parent (ci));

	return (GnomeDbCanvasTable *) ci;
}
