/* gnome-db-graph-item.h
 *
 * Copyright (C) 2004 - 2008 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */


#ifndef __GNOME_DB_GRAPH_ITEM_H_
#define __GNOME_DB_GRAPH_ITEM_H_

#include <glib-object.h>
#include <libgnomedb-graph/gnome-db-canvas-decl.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_GRAPH_ITEM          (gnome_db_graph_item_get_type())
#define GNOME_DB_GRAPH_ITEM(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, gnome_db_graph_item_get_type(), GnomeDbGraphItem)
#define GNOME_DB_GRAPH_ITEM_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, gnome_db_graph_item_get_type (), GnomeDbGraphItemClass)
#define GNOME_DB_IS_GRAPH_ITEM(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, gnome_db_graph_item_get_type ())

/* error reporting */
extern GQuark gnome_db_graph_item_error_quark (void);
#define GNOME_DB_GRAPH_ITEM_ERROR gnome_db_graph_item_error_quark ()

typedef enum
{
	GNOME_DB_GRAPH_ITEM__ERROR
} GnomeDbGraphItemError;


/* struct for the object's data */
struct _GnomeDbGraphItem
{
	GObject               object;
	GnomeDbGraphItemPrivate  *priv;
};

/* struct for the object's class */
struct _GnomeDbGraphItemClass
{
	GObjectClass   parent_class;
	
	/* signals */
	void        (*moved) (GnomeDbGraphItem *item);
};

GType             gnome_db_graph_item_get_type            (void) G_GNUC_CONST;

GnomeDbGraphItem *gnome_db_graph_item_new                 (GnomeDbGraph *graph);
GnomeDbGraph     *gnome_db_graph_item_get_graph           (GnomeDbGraphItem *item);
void              gnome_db_graph_item_set_position        (GnomeDbGraphItem *item, gdouble x, gdouble y);
void              gnome_db_graph_item_get_position        (GnomeDbGraphItem *item, gdouble *x, gdouble *y);


G_END_DECLS

#endif
