/* gnome-db-canvas.c
 *
 * Copyright (C) 2007 - 2008 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <glib/gi18n-lib.h>
#include <gtk/gtk.h>
#include <libgnomedb/marshal.h>
#include "gnome-db-canvas.h"
#include "gnome-db-canvas-tip.h"
#include "gnome-db-canvas-cursor.h"
#include "gnome-db-canvas-print.h"
#include "gnome-db-graph.h"
#include "gnome-db-graph-item.h"
#include <libgda/libgda.h>

#ifdef HAVE_GRAPHVIZ
#include <stddef.h>
#include <gvc.h>
#ifndef ND_coord_i
    #define ND_coord_i ND_coord
#endif
static GVC_t* gvc = NULL;
#endif
#include <cairo.h>
#include <cairo-svg.h>
#include <math.h>

static void gnome_db_canvas_class_init (GnomeDbCanvasClass * class);
static void gnome_db_canvas_init       (GnomeDbCanvas * canvas);
static void gnome_db_canvas_dispose    (GObject   * object);
static void gnome_db_canvas_finalize   (GObject   * object);
static void gnome_db_canvas_post_init  (GnomeDbCanvas * canvas);

static void gnome_db_canvas_set_property    (GObject *object,
				       guint param_id,
				       const GValue *value,
				       GParamSpec *pspec);
static void gnome_db_canvas_get_property    (GObject *object,
				       guint param_id,
				       GValue *value,
				       GParamSpec *pspec);

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *parent_class = NULL;


struct _GnomeDbCanvasPrivate
{
	GnomeDbGraph       *graph;
	GSList             *items; /* GnomeDbCanvasItem objects, non ordered */
	gboolean            force_center;
};


enum
{
	DRAG_ACTION,
	LAST_SIGNAL
};

enum
{
	PROP_0,
	PROP_GRAPH
};

static gint canvas_signals[LAST_SIGNAL] = { 0 };

GType
gnome_db_canvas_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0)) {
		static const GTypeInfo info = {
			sizeof (GnomeDbCanvasClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_canvas_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbCanvas),
			0,
			(GInstanceInitFunc) gnome_db_canvas_init
		};		

		type = g_type_register_static (GOO_TYPE_CANVAS, "GnomeDbCanvas", &info, 0);
	}
	return type;
}

static void
gnome_db_canvas_class_init (GnomeDbCanvasClass * class)
{
	GtkWidgetClass *widget_class;
	GObjectClass   *object_class = G_OBJECT_CLASS (class);
	parent_class = g_type_class_peek_parent (class);

	widget_class = (GtkWidgetClass *) class;

	canvas_signals[DRAG_ACTION] =
		g_signal_new ("drag_action",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_FIRST,
			      G_STRUCT_OFFSET (GnomeDbCanvasClass, drag_action),
			      NULL, NULL,
			      gnome_db_marshal_VOID__POINTER_POINTER, G_TYPE_NONE, 2,
			      G_TYPE_POINTER, G_TYPE_POINTER);

	class->drag_action = NULL;

	/* properties */
	object_class->set_property = gnome_db_canvas_set_property;
	object_class->get_property = gnome_db_canvas_get_property;

	g_object_class_install_property (object_class, PROP_GRAPH,
					 g_param_spec_object ("graph", NULL, NULL, GNOME_DB_TYPE_GRAPH,
							      (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	
	/* virtual functions */
	class->create_canvas_items = NULL;
	class->clean_canvas_items = NULL;
	class->graph_item_added = NULL;
	class->graph_item_dropped = NULL;
	class->build_context_menu = NULL;

	object_class->dispose = gnome_db_canvas_dispose;
	object_class->finalize = gnome_db_canvas_finalize;
}

static int canvas_event (GooCanvas *gcanvas, GdkEvent *event, GnomeDbCanvas *canvas);
static void
gnome_db_canvas_init (GnomeDbCanvas * canvas)
{
	canvas->priv = g_new0 (GnomeDbCanvasPrivate, 1);
	canvas->priv->graph = NULL;
	canvas->priv->items = NULL;
	canvas->priv->force_center = FALSE;

	canvas->xmouse = 50.;
	canvas->ymouse = 50.;
	
	g_signal_connect (G_OBJECT (canvas), "event-after",
			  G_CALLBACK (canvas_event), canvas);
	g_object_set (G_OBJECT (canvas), "automatic-bounds", TRUE,
		      "bounds-padding", 5., 
		      "bounds-from-origin", FALSE, 
		      "anchor", GTK_ANCHOR_CENTER, NULL);
}

static void popup_zoom_in_cb (GtkMenuItem *mitem, GnomeDbCanvas *canvas);
static void popup_zoom_out_cb (GtkMenuItem *mitem, GnomeDbCanvas *canvas);
static void popup_zoom_fit_cb (GtkMenuItem *mitem, GnomeDbCanvas *canvas);
static void popup_export_cb (GtkMenuItem *mitem, GnomeDbCanvas *canvas);
#ifdef HAVE_GTKTWOTEN
static void popup_print_cb (GtkMenuItem *mitem, GnomeDbCanvas *canvas);
#endif
static int 
canvas_event (GooCanvas *gcanvas, GdkEvent *event, GnomeDbCanvas *canvas)
{
	gboolean done = TRUE;
	GooCanvasItem *item;
	GnomeDbCanvasClass *class = GNOME_DB_CANVAS_CLASS (G_OBJECT_GET_CLASS (canvas));
	gdouble x, y;

	/*
	if (g_object_get_data (G_OBJECT (goo_canvas_get_root_item (GOO_CANVAS (canvas))), "dragged_from")) {
		g_print ("DRAG %s(CANCELLED)\n", __FUNCTION__);
		g_object_set_data (G_OBJECT (goo_canvas_get_root_item (GOO_CANVAS (canvas))), "dragged_from", NULL);
	}
	*/

	switch (event->type) {
	case GDK_BUTTON_PRESS:
		x = ((GdkEventButton *) event)->x;
		y = ((GdkEventButton *) event)->y;
		goo_canvas_convert_from_pixels (GOO_CANVAS (canvas), &x, &y);
		item = goo_canvas_get_item_at (GOO_CANVAS (canvas), x, y, TRUE);

		if (!item) {
			if ((((GdkEventButton *) event)->button == 3) && (class->build_context_menu)) {
				GtkWidget *menu, *mitem;
				
				canvas->xmouse = x;
				canvas->ymouse = y;

				/* extra menu items, if any */
				menu = (class->build_context_menu) (canvas);
				
				/* default menu items */
				if (!menu)
					menu = gtk_menu_new ();
				else {
					mitem = gtk_separator_menu_item_new ();
					gtk_widget_show (mitem);
					gtk_menu_append (menu, mitem);
				}
				mitem = gtk_image_menu_item_new_from_stock (GTK_STOCK_ZOOM_IN, NULL);
				gtk_widget_show (mitem);
				gtk_menu_append (menu, mitem);
				g_signal_connect (G_OBJECT (mitem), "activate", G_CALLBACK (popup_zoom_in_cb), canvas);
				mitem = gtk_image_menu_item_new_from_stock (GTK_STOCK_ZOOM_OUT, NULL);
				gtk_widget_show (mitem);
				gtk_menu_append (menu, mitem);
				g_signal_connect (G_OBJECT (mitem), "activate", G_CALLBACK (popup_zoom_out_cb), canvas);
				mitem = gtk_image_menu_item_new_from_stock (GTK_STOCK_ZOOM_FIT, NULL);
				gtk_widget_show (mitem);
				gtk_menu_append (menu, mitem);
				g_signal_connect (G_OBJECT (mitem), "activate", G_CALLBACK (popup_zoom_fit_cb), canvas);

				mitem = gtk_separator_menu_item_new ();
				gtk_widget_show (mitem);
				gtk_menu_append (menu, mitem);
				
				mitem = gtk_image_menu_item_new_from_stock (GTK_STOCK_SAVE_AS, NULL);
				gtk_widget_show (mitem);
				gtk_menu_append (menu, mitem);
				g_signal_connect (G_OBJECT (mitem), "activate", G_CALLBACK (popup_export_cb), canvas);

				mitem = gtk_image_menu_item_new_from_stock (GTK_STOCK_PRINT, NULL);
				gtk_widget_show (mitem);
#ifdef HAVE_GTKTWOTEN
				g_signal_connect (G_OBJECT (mitem), "activate", G_CALLBACK (popup_print_cb), canvas);
#else
				gtk_widget_set_sensitive (mitem, FALSE);
#endif
				gtk_menu_append (menu, mitem);

				gtk_menu_popup (GTK_MENU (menu), NULL, NULL,
						NULL, NULL, ((GdkEventButton *)event)->button,
						((GdkEventButton *)event)->time);
				
				done = TRUE;
			}
		}
		break;
	default:
		done = FALSE;
		break;
	}
	return done;	
}

static void
popup_zoom_in_cb (GtkMenuItem *mitem, GnomeDbCanvas *canvas)
{
	gnome_db_canvas_set_zoom_factor (canvas, gnome_db_canvas_get_zoom_factor (canvas) + .05);
}

static void
popup_zoom_out_cb (GtkMenuItem *mitem, GnomeDbCanvas *canvas)
{
	gnome_db_canvas_set_zoom_factor (canvas, gnome_db_canvas_get_zoom_factor (canvas) - .05);
}

static void
popup_zoom_fit_cb (GtkMenuItem *mitem, GnomeDbCanvas *canvas)
{
	gnome_db_canvas_fit_zoom_factor (canvas);
}

static void
popup_export_cb (GtkMenuItem *mitem, GnomeDbCanvas *canvas)
{
	GtkWidget *dlg;
	gint result;
	GtkWidget *toplevel = gtk_widget_get_toplevel (GTK_WIDGET (canvas));
	GtkFileFilter *filter;

#define MARGIN 5.

	if (!GTK_WIDGET_TOPLEVEL (toplevel))
		toplevel = NULL;

	dlg = gtk_file_chooser_dialog_new (_("Save diagram as"), (GtkWindow*) toplevel,
					   GTK_FILE_CHOOSER_ACTION_SAVE, 
					   GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					   GTK_STOCK_SAVE, GTK_RESPONSE_ACCEPT,
					   NULL);
	filter = gtk_file_filter_new ();
	gtk_file_filter_set_name (filter, _("PNG Image"));
	gtk_file_filter_add_mime_type (filter, "image/png");
	gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (dlg), filter);

	filter = gtk_file_filter_new ();
	gtk_file_filter_set_name (filter, _("SVG file"));
	gtk_file_filter_add_mime_type (filter, "image/svg+xml");
	gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (dlg), filter);

	result = gtk_dialog_run (GTK_DIALOG (dlg));
	if (result == GTK_RESPONSE_ACCEPT) {
		gchar *filename;
		gchar *lcfilename;
		cairo_surface_t *surface = NULL;

		filename = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dlg));
		if (filename) {
			GooCanvasBounds bounds;
			gdouble width, height;
			gchar *error = NULL;
			enum {
				OUT_UNKNOWN,
				OUT_PNG,
				OUT_SVG
			} otype = OUT_UNKNOWN;

			goo_canvas_item_get_bounds (goo_canvas_get_root_item (GOO_CANVAS (canvas)), &bounds);
			width = (bounds.x2 - bounds.x1) + 2. * MARGIN;
			height = (bounds.y2 - bounds.y1) + 2. * MARGIN;
			
			lcfilename = g_ascii_strdown (filename, -1);
			if (g_str_has_suffix (lcfilename, "png")) {
				otype = OUT_PNG;
				surface = cairo_image_surface_create (CAIRO_FORMAT_ARGB32, width, height);
			}
			if (g_str_has_suffix (lcfilename, "svg")) {
				cairo_status_t status;
				otype = OUT_SVG;
				surface = cairo_svg_surface_create (filename, width, height);
				status = cairo_surface_status (surface);
				if (status != CAIRO_STATUS_SUCCESS)
					error = g_strdup_printf ("<b>%s</b>:\n%s",
								 _("Failed to create SVG file"), 
								 cairo_status_to_string (status));
				cairo_surface_destroy (surface);
				surface = NULL;
			}
			if (otype == OUT_UNKNOWN)
				error = g_strdup_printf ("<b>%s</b>",
							 _("File format to save to is not recognized."));
			
			if (surface) {
				cairo_t *cr;
				cairo_status_t status;

				cr = cairo_create (surface);
				cairo_set_antialias (cr, CAIRO_ANTIALIAS_GRAY);
				cairo_set_line_width (cr, goo_canvas_get_default_line_width (GOO_CANVAS (canvas)));
				cairo_translate (cr, MARGIN - bounds.x1, MARGIN - bounds.y1);

				goo_canvas_render (GOO_CANVAS (canvas), cr, NULL, 0.8);

				cairo_show_page (cr);

				switch (otype) {
				case OUT_PNG:
					status = cairo_surface_write_to_png (surface, filename);
					if (status != CAIRO_STATUS_SUCCESS)
						error = g_strdup_printf ("<b>%s</b>:\n%s",
									 _("Failed to create PNG file"), 
									 cairo_status_to_string (status));
					break;
				default:
					break;
				}

				cairo_surface_destroy (surface);
				cairo_destroy (cr);
			}

			if (error) {
				GtkWidget *errdlg;

				errdlg = gtk_message_dialog_new_with_markup ((GtkWindow*) toplevel,
									     GTK_DIALOG_MODAL, GTK_MESSAGE_ERROR, 
									     GTK_BUTTONS_CLOSE, error);
				g_free (error);
				gtk_dialog_run (GTK_DIALOG (errdlg));
				gtk_widget_destroy (errdlg);
			}
				
			g_free (filename);
			g_free (lcfilename);
		}
	}
	gtk_widget_destroy (dlg);
}

#ifdef HAVE_GTKTWOTEN

static void
popup_print_cb (GtkMenuItem *mitem, GnomeDbCanvas *canvas)
{
	gnome_db_canvas_print (canvas);
}

#endif /* HAVE_GTKTWOTEN */

static void release_graph (GnomeDbGraph *graph, GnomeDbCanvas *canvas);
static void graph_item_added_cb (GnomeDbGraph *graph, GnomeDbGraphItem *item, GnomeDbCanvas *canvas);
static void graph_item_dropped_cb (GnomeDbGraph *graph, GnomeDbGraphItem *item, GnomeDbCanvas *canvas);
static void item_destroyed_cb (GnomeDbCanvasItem *item, GnomeDbCanvas *canvas);
static void drag_action_cb (GnomeDbCanvasItem *citem, GnomeDbCanvasItem *drag_from, 
			    GnomeDbCanvasItem *drag_to, GnomeDbCanvas *canvas);
static void
gnome_db_canvas_dispose (GObject   * object)
{
	GnomeDbCanvas *canvas;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOME_DB_IS_CANVAS (object));

	canvas = GNOME_DB_CANVAS (object);

	if (canvas->priv->graph) 
		release_graph (canvas->priv->graph, canvas);

	/* get rid of the GooCanvasItems */
	if (canvas->priv->items) {
		GSList *list;
		for (list = canvas->priv->items; list; list = list->next) {
			g_signal_handlers_disconnect_by_func (G_OBJECT (list->data), G_CALLBACK (drag_action_cb), canvas);
			g_signal_handlers_disconnect_by_func (G_OBJECT (list->data), G_CALLBACK (item_destroyed_cb), canvas);
		}
		g_slist_free (canvas->priv->items);
		canvas->priv->items = NULL;
	}

	/* for the parent class */
	parent_class->dispose (object);
}


static void
gnome_db_canvas_finalize (GObject   * object)
{
	GnomeDbCanvas *canvas;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOME_DB_IS_CANVAS (object));
	canvas = GNOME_DB_CANVAS (object);

	if (canvas->priv) {
		g_free (canvas->priv);
		canvas->priv = NULL;
	}

	/* for the parent class */
	parent_class->finalize (object);
}


static void 
gnome_db_canvas_set_property (GObject *object,
			   guint param_id,
			   const GValue *value,
			   GParamSpec *pspec)
{
	GnomeDbCanvas *canvas;
	GObject *ptr;
	
	canvas = GNOME_DB_CANVAS (object);

	switch (param_id) {
	case PROP_GRAPH:
		ptr = g_value_get_object (value);

		if (ptr == (GObject*) canvas->priv->graph)
			return;

		if (canvas->priv->graph) 
			release_graph (canvas->priv->graph, canvas);
		
		if (ptr) {
			g_return_if_fail (GNOME_DB_IS_GRAPH (ptr));
			canvas->priv->graph = GNOME_DB_GRAPH (ptr);
			g_object_ref (G_OBJECT (ptr));
			g_signal_connect (G_OBJECT (ptr), "item_added",
					  G_CALLBACK (graph_item_added_cb), canvas);
			g_signal_connect (G_OBJECT (ptr), "item_dropped",
					  G_CALLBACK (graph_item_dropped_cb), canvas);
		}

		break;
	}
	
	if (canvas->priv->graph) 
		gnome_db_canvas_post_init (canvas);
}

static void
gnome_db_canvas_get_property (GObject *object,
			   guint param_id,
			   GValue *value,
			   GParamSpec *pspec)
{
	GnomeDbCanvas *canvas;
	
	canvas = GNOME_DB_CANVAS (object);

	switch (param_id) {
	case PROP_GRAPH:
		g_value_set_object (value, canvas->priv->graph);
		break;
	}
}

static void
release_graph (GnomeDbGraph *graph, GnomeDbCanvas *canvas)
{
	g_return_if_fail (canvas->priv->graph == graph);

	g_signal_handlers_disconnect_by_func (G_OBJECT (graph),
					      G_CALLBACK (graph_item_added_cb), canvas);
	g_signal_handlers_disconnect_by_func (G_OBJECT (graph),
					      G_CALLBACK (graph_item_dropped_cb), canvas);
	canvas->priv->graph = NULL;

	/* some clean-up if there are already some canvas items */
	while (canvas->priv->items)
		goo_canvas_item_remove (GOO_CANVAS_ITEM (canvas->priv->items->data));
	
	g_object_unref (G_OBJECT (graph));
}

static void
graph_item_added_cb (GnomeDbGraph *graph, GnomeDbGraphItem *item, GnomeDbCanvas *canvas)
{
	GnomeDbCanvasClass *class = GNOME_DB_CANVAS_CLASS (G_OBJECT_GET_CLASS (canvas));
	if (class->graph_item_added)
		(class->graph_item_added) (canvas, item);
}

static void
graph_item_dropped_cb (GnomeDbGraph *graph, GnomeDbGraphItem *item, GnomeDbCanvas *canvas)
{
	GnomeDbCanvasClass *class = GNOME_DB_CANVAS_CLASS (G_OBJECT_GET_CLASS (canvas));
	if (class->graph_item_dropped)
		(class->graph_item_dropped) (canvas, item);
}

static void 
gnome_db_canvas_post_init  (GnomeDbCanvas *canvas)
{
	GnomeDbCanvasClass *class = GNOME_DB_CANVAS_CLASS (G_OBJECT_GET_CLASS (canvas));

	/* some clean-up if there are already some canvas items */
	if (class->clean_canvas_items)
		(class->clean_canvas_items) (canvas);

	/* reseting the zoom */
	goo_canvas_set_scale (GOO_CANVAS (canvas), 1.0);

	/* adding some new canvas items */
	if (class->create_canvas_items)
		(class->create_canvas_items) (canvas);
}

GnomeDbGraph *
gnome_db_canvas_get_graph (GnomeDbCanvas *canvas)
{
	g_return_val_if_fail (GNOME_DB_IS_CANVAS (canvas), NULL);
	g_return_val_if_fail (canvas->priv, NULL);

	if (!canvas->priv->graph) {
		GnomeDbGraph *graph;
		graph = (GnomeDbGraph*) gnome_db_graph_new (NULL, G_MAXUINT);
		g_object_set (G_OBJECT (canvas), "graph", graph, NULL);
		g_object_unref (graph);
	}

	return canvas->priv->graph;
}

/**
 * gnome_db_canvas_declare_item
 * @canvas: a #GnomeDbCanvas widget
 * @item: a #GnomeDbCanvasItem object
 *
 * Declares @item to be listed by @canvas as one of its items.
 * This functions should be called after each #GnomeDbCanvasItem is added to
 * @canvas.
 *
 * If it was not called for one item, then that item won't be used
 * in @canvas's computations (no drag and drop, cleanup, etc).
 */
void
gnome_db_canvas_declare_item (GnomeDbCanvas *canvas, GnomeDbCanvasItem *item)
{
	g_return_if_fail (GNOME_DB_IS_CANVAS (canvas));
	g_return_if_fail (canvas->priv);
	g_return_if_fail (item && GNOME_DB_IS_CANVAS_ITEM (item));

	if (g_slist_find (canvas->priv->items, item))
		return;

	canvas->priv->items = g_slist_prepend (canvas->priv->items, item);
	g_signal_connect (G_OBJECT (item), "drag_action",
			  G_CALLBACK (drag_action_cb), canvas);
	g_signal_connect (G_OBJECT (item), "destroy",
			  G_CALLBACK (item_destroyed_cb), canvas);
}

static void
item_destroyed_cb (GnomeDbCanvasItem *item, GnomeDbCanvas *canvas)
{
	g_return_if_fail (g_slist_find (canvas->priv->items, item));
	g_signal_handlers_disconnect_by_func (G_OBJECT (item), G_CALLBACK (drag_action_cb), canvas);
	g_signal_handlers_disconnect_by_func (G_OBJECT (item), G_CALLBACK (item_destroyed_cb), canvas);
	canvas->priv->items = g_slist_remove (canvas->priv->items, item);
}
 
static void 
drag_action_cb (GnomeDbCanvasItem *citem, GnomeDbCanvasItem *drag_from, GnomeDbCanvasItem *drag_to, GnomeDbCanvas *canvas)
{
#ifdef debug_signal
        g_print (">> 'DRAG_ACTION' from %s, %s\n", __FILE__, __FUNCTION__);
#endif
        g_signal_emit (G_OBJECT (canvas), canvas_signals[DRAG_ACTION], 0, drag_from, drag_to);
#ifdef debug_signal
        g_print ("<< 'DRAG_ACTION' from %s, %s\n", __FILE__, __FUNCTION__);
#endif
}


/**
 * gnome_db_canvas_set_in_scrolled_window
 * @canvas: a #GnomeDbCanvas widget
 *
 * Creates a new #GtkScrolledWindow object and put @canvas
 * in it. @canvas can be retrieved using a "canvas" user property of
 * the new scrolled window.
 *
 * Returns: the new scrolled window.
 */
GtkWidget *
gnome_db_canvas_set_in_scrolled_window (GnomeDbCanvas *canvas)
{
	GtkWidget *sw;
	g_return_val_if_fail (GNOME_DB_IS_CANVAS (canvas), NULL);
	g_return_val_if_fail (canvas->priv, NULL);

	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw), 
					GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
        gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (sw), GTK_SHADOW_IN);
	gtk_container_add (GTK_CONTAINER (sw), GTK_WIDGET (canvas));
	gtk_widget_show (GTK_WIDGET (canvas));
	g_object_set_data (G_OBJECT (sw), "canvas", canvas);

	return sw;
}

/**
 * gnome_db_canvas_set_zoom_factor
 * @canvas: a #GnomeDbCanvas widget
 * @n: the zoom factor
 *
 * Sets the zooming factor of a canvas by specifying the number of pixels that correspond 
 * to one canvas unit. A zoom factor of 1.0 is the default value; greater than 1.0 makes a zoom in
 * and lower than 1.0 makes a zoom out.
 */
void
gnome_db_canvas_set_zoom_factor (GnomeDbCanvas *canvas, gdouble n)
{
	g_return_if_fail (GNOME_DB_IS_CANVAS (canvas));
	g_return_if_fail (canvas->priv);

	goo_canvas_set_scale (GOO_CANVAS (canvas), n);
}

/**
 * gnome_db_canvas_get_zoom_factor
 * @canvas: a #GnomeDbCanvas widget
 *
 * Get the current zooming factor of a canvas.
 *
 * Returns: the zooming factor.
 */
gdouble
gnome_db_canvas_get_zoom_factor (GnomeDbCanvas *canvas)
{
	g_return_val_if_fail (GNOME_DB_IS_CANVAS (canvas), 1.);
	g_return_val_if_fail (canvas->priv, 1.);

	return goo_canvas_get_scale (GOO_CANVAS (canvas));
}

/**
 * gnome_db_canvas_fit_zoom_factor
 * @canvas: a #GnomeDbCanvas widget
 *
 * Compute and set the correct zoom factor so that all the items on @canvas can be displayed
 * at once.
 *
 * Returns: the new zooming factor.
 */
gdouble
gnome_db_canvas_fit_zoom_factor (GnomeDbCanvas *canvas)
{
	gdouble zoom, xall, yall;
	GooCanvasBounds bounds;

	g_return_val_if_fail (GNOME_DB_IS_CANVAS (canvas), 1.);
	g_return_val_if_fail (canvas->priv, 1.);

	xall = GTK_WIDGET (canvas)->allocation.width;
	yall = GTK_WIDGET (canvas)->allocation.height;

	goo_canvas_item_get_bounds (GOO_CANVAS_ITEM (goo_canvas_get_root_item (GOO_CANVAS (canvas))),
				    &bounds);
	bounds.y1 -= 5.; bounds.y2 += 5.;
	bounds.x1 -= 5.; bounds.x2 += 5.;
	zoom = yall / (bounds.y2 - bounds.y1);
	if (xall / (bounds.x2 - bounds.x1) < zoom)
		zoom = xall / (bounds.x2 - bounds.x1);

	/* set a limit to the zoom */
	if (zoom > 1.0)
		zoom = 1.0;
	
	gnome_db_canvas_set_zoom_factor (GNOME_DB_CANVAS (canvas), zoom);

	return zoom;
}

/**
 * gnome_db_canvas_center
 * @canvas: a #GnomeDbCanvas widget
 *
 * Centers the display on the layout
 */ 
void
gnome_db_canvas_center (GnomeDbCanvas *canvas)
{
	/* remove top and left margins if we are running out of space */
	if (GOO_CANVAS (canvas)->hadjustment && GOO_CANVAS (canvas)->vadjustment) {
		gdouble hlow, hup, vlow, vup, hmargin, vmargin;
		gdouble left, top, right, bottom;
		GooCanvasBounds bounds;

		goo_canvas_get_bounds (GOO_CANVAS (canvas), &left, &top, &right, &bottom);
		goo_canvas_item_get_bounds (goo_canvas_get_root_item (GOO_CANVAS (canvas)), &bounds);

		g_object_get (G_OBJECT (GOO_CANVAS (canvas)->hadjustment), "lower", &hlow, "upper", &hup, NULL);
		g_object_get (G_OBJECT (GOO_CANVAS (canvas)->vadjustment), "lower", &vlow, "upper", &vup, NULL);

		/*
		g_print ("Canvas's bounds: %.2f,%.2f -> %.2f,%.2f\n", left, top, right, bottom);
		g_print ("Root's bounds: %.2f,%.2f -> %.2f,%.2f\n", bounds.x1, bounds.y1, bounds.x2, bounds.y2);
		g_print ("Xm: %.2f, Ym: %.2f\n", hup - hlow - (right - left), vup - vlow - (bottom - top));
		*/
		hmargin = hup - hlow - (bounds.x2 - bounds.x1);
		if (hmargin > 0) 
			left -= hmargin / 2. + (left - bounds.x1);
		vmargin = vup - vlow - (bounds.y2 - bounds.y1);
		if (vmargin > 0) 
			top -= vmargin / 2. + (top - bounds.y1);
		if ((hmargin > 0) || (vmargin > 0)) {
			goo_canvas_set_bounds (GOO_CANVAS (canvas), left, top, right, bottom);
			/*g_print ("Canvas's new bounds: %.2f,%.2f -> %.2f,%.2f\n", left, top, right, bottom);*/
			goo_canvas_set_scale (GOO_CANVAS (canvas), GOO_CANVAS (canvas)->scale);	
		}
	}
}

/**
 * gnome_db_canvas_auto_layout_enabled
 * @canvas: a #GnomeDbCanvas widget
 *
 * Tells if @canvas has the possibility to automatically adjust its layout
 * using the GraphViz library.
 *
 * Returns: TRUE if @canvas can automatically adjust its layout
 */
gboolean
gnome_db_canvas_auto_layout_enabled (GnomeDbCanvas *canvas)
{
	g_return_val_if_fail (GNOME_DB_IS_CANVAS (canvas), FALSE);
	g_return_val_if_fail (canvas->priv, FALSE);

#ifdef HAVE_GRAPHVIZ
	return TRUE;
#else
	return FALSE;
#endif
}

#ifdef HAVE_GRAPHVIZ
typedef struct {
	GnomeDbCanvas    *canvas;
	Agraph_t      *graph;
	GSList        *nodes_list; /* list of NodeLayout structures */
} GraphLayout;

typedef struct {
	GnomeDbCanvasItem    *item; /* item to be moved */
	GnomeDbGraphItem      *graph_item;
	Agnode_t          *node;
	gdouble            start_x;
	gdouble            start_y;
	gdouble            end_x;
	gdouble            end_y;
	gdouble            width;
	gdouble            height;
	gdouble            dx;
	gdouble            dy;
	gboolean           stop;
} NodeLayout;
static gboolean canvas_animate_to (GraphLayout *gl);
#endif

/**
 * gnome_db_canvas_auto_layout
 * @canvas: a #GnomeDbCanvas widget
 *
 * Re-organizes the layout of the @canvas' items using the GraphViz
 * layout engine.
 */
void
gnome_db_canvas_perform_auto_layout (GnomeDbCanvas *canvas, gboolean animate, GnomeDbCanvasLayoutAlgorithm algorithm)
{
	g_return_if_fail (GNOME_DB_IS_CANVAS (canvas));
	g_return_if_fail (canvas->priv);

#define GV_SCALE 72.

#ifndef HAVE_GRAPHVIZ
	g_message ("GraphViz library support not compiled, cannot do graph layout...\n");
	return FALSE;
#else
	GSList *list;
	Agraph_t *graph;
	GHashTable *nodes_hash; /* key = GnomeDbCanvasItem, value = Agnode_t *node */
	GSList *nodes_list = NULL; /* list of NodeLayout structures */

	if (!gvc)
		gvc = gvContext ();

	graph = agopen ("GnomedbCanvasLayout", AGDIGRAPHSTRICT);
        agnodeattr (graph, "shape", "box");
        agnodeattr (graph, "height", ".1");
        agnodeattr (graph, "width", ".1");
        agnodeattr (graph, "fixedsize", "true");
        agnodeattr (graph, "pack", "true");
	agnodeattr (graph, "packmode", "node");

	/* Graph nodes creation */
	nodes_hash = g_hash_table_new (NULL, NULL);
	for (list = canvas->priv->items; list; list = list->next) {
		GnomeDbCanvasItem *item = GNOME_DB_CANVAS_ITEM (list->data);

		if (gnome_db_canvas_item_get_graph_item (item)) {
			Agnode_t *node;
			gchar *tmp;
			double val;
			GooCanvasBounds bounds;

			NodeLayout *nl;
			nl = g_new0 (NodeLayout, 1);
			nl->item = item;
			nl->graph_item = gnome_db_canvas_item_get_graph_item (item);
			gnome_db_graph_item_get_position (nl->graph_item, &(nl->start_x), &(nl->start_y));
			nodes_list = g_slist_prepend (nodes_list, nl);
			
			tmp = g_strdup_printf ("%p", item);
			node = agnode (graph, tmp);
			nl->node = node;
			g_hash_table_insert (nodes_hash, item, node);
			
			tmp = g_strdup_printf ("%p", node);
			agset (node, "label", tmp);
			g_free (tmp);

			goo_canvas_item_get_bounds (GOO_CANVAS_ITEM (item), &bounds);
			nl->width = bounds.x2 - bounds.x1;
			nl->height = bounds.y2 - bounds.y1;
			val = (bounds.y2 - bounds.y1) / GV_SCALE;
			tmp = g_strdup_printf ("%.3f", val);
			agset (node, "height", tmp);
			g_free (tmp);
			val = (bounds.x2 - bounds.x1) / GV_SCALE;
			tmp = g_strdup_printf ("%.3f", val);
			agset (node, "width", tmp);
			g_free (tmp);

			/*g_print ("Before: Node %p: HxW: %.3f %.3f\n", node, (bounds.y2 - bounds.y1) / GV_SCALE, 
			  (bounds.x2 - bounds.x1) / GV_SCALE);*/
		}
	}
	/* Graph edges creation */
	for (list = canvas->priv->items; list; list = list->next) {
		GnomeDbCanvasItem *item = GNOME_DB_CANVAS_ITEM (list->data);
		GnomeDbCanvasItem *from, *to;
		gnome_db_canvas_item_get_edge_nodes (item, &from, &to);
		if (from && to) {
			Agnode_t *from_node, *to_node;
			from_node = (Agnode_t*) g_hash_table_lookup (nodes_hash, from);
			to_node = (Agnode_t*) g_hash_table_lookup (nodes_hash, to);
			if (from_node && to_node)
				agedge (graph, from_node, to_node);
		}
	}

	switch (algorithm) {
	default:
	case GNOME_DB_CANVAS_LAYOUT_DEFAULT:
		gvLayout (gvc, graph, "dot");
		break;
	case GNOME_DB_CANVAS_LAYOUT_RADIAL:
		gvLayout (gvc, graph, "circo");
		break;
	}
        gvRender (gvc, graph, "dot", NULL);
	/*gvRenderFilename (gvc, graph, "png", "out.png");*/
        /*gvRender (gvc, graph, "dot", stdout);*/

	canvas->priv->force_center = TRUE;

	for (list = nodes_list; list; list = list->next) {
		NodeLayout *nl = (NodeLayout*) list->data;
		nl->end_x = ND_coord_i (nl->node).x - (nl->width / 2.);
		nl->end_y = - ND_coord_i (nl->node).y - (nl->height / 2.);
		nl->dx = fabs (nl->end_x - nl->start_x);
		nl->dy = fabs (nl->end_y - nl->start_y);
		nl->stop = FALSE;
		/*g_print ("After: Node %p: HxW: %.3f %.3f XxY = %d, %d\n", nl->node, 
			 ND_height (nl->node), ND_width (nl->node),
			 ND_coord_i (nl->node).x, - ND_coord_i (nl->node).y);*/
		if (!animate)
			gnome_db_graph_item_set_position (nl->graph_item, nl->end_x, nl->end_y);
	}

	g_hash_table_destroy (nodes_hash);
	gvFreeLayout (gvc, graph);

	if (animate) {
		GraphLayout *gl;
		gl = g_new0 (GraphLayout, 1);
		gl->canvas = canvas;
		gl->graph = graph;
		gl->nodes_list = nodes_list;
		while (canvas_animate_to (gl));
	}
	else {
		agclose (graph);
		g_slist_foreach (nodes_list, (GFunc) g_free, NULL);
		g_slist_free (nodes_list);
	}

	canvas->priv->force_center = FALSE;
#endif
}

#ifdef HAVE_GRAPHVIZ
static gdouble
compute_animation_inc (float start, float stop, float current)
{
        gdouble inc;
#ifndef PI
#define PI 3.14159265
#endif
#define STEPS 20.

        if (stop == start)
                return 0.;

	inc = (stop - start) / STEPS;

        return inc;
}

static gboolean
canvas_animate_to (GraphLayout *gl) 
{
	gboolean stop = TRUE;
	GSList *list;

#define EPSILON 1.
	for (list = gl->nodes_list; list; list = list->next) {
		NodeLayout *nl = (NodeLayout*) list->data;
		if (!nl->stop) {
			gdouble cur_x, cur_y, dx, dy, ndx, ndy;
			gnome_db_graph_item_get_position (nl->graph_item, &cur_x, &cur_y);
			dx = compute_animation_inc (nl->start_x, nl->end_x, cur_x);
			dy = compute_animation_inc (nl->start_y, nl->end_y, cur_y);
			ndx = fabs (cur_x + dx - nl->end_x);
			ndy = fabs (cur_y + dy - nl->end_y);
			gnome_db_graph_item_set_position (nl->graph_item, cur_x + dx, cur_y + dy);
			if (((ndx <= EPSILON) || (ndx >= nl->dx)) &&
			    ((ndy <= EPSILON) || (ndy >= nl->dy)))
				nl->stop = TRUE;
			else {
				stop = FALSE;
				nl->dx = ndx;
				nl->dy = ndy;
			}
		}
	}

	goo_canvas_request_update (GOO_CANVAS (gl->canvas));
	goo_canvas_update (GOO_CANVAS (gl->canvas));
	while (gtk_events_pending ())
		gtk_main_iteration ();

	if (stop) {
		agclose (gl->graph);
		g_slist_foreach (gl->nodes_list, (GFunc) g_free, NULL);
		g_slist_free (gl->nodes_list);
		g_free (gl);
	}
	return !stop;
}
#endif
