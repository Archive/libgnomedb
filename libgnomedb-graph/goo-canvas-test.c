#include <stdlib.h>
#include <goocanvas.h>
#include <libgnomedb/libgnomedb.h>
#include <gnome-db-canvas-text.h>
#include <gnome-db-canvas-table.h>
#include <gnome-db-canvas-db-relations.h>

static gboolean on_delete_event (GtkWidget *window, GdkEvent *event, gpointer unused_data);
static void auto_layout_cb (GtkButton *button, GnomeDbCanvas *canvas);  
static void center_display_cb (GtkButton *button, GnomeDbCanvas *canvas);  
static void destroy_widget_cb (GtkButton *button, GtkWidget *wid);  

int
main (int argc, char *argv[])
{
	GtkWidget *window, *scrolled_win, *canvas, *nb, *vb, *button, *bbox;
	GooCanvasItem *root, *item;
	GnomeDbGraph *graph;

	GdaConnection *cnc;
        GdaMetaStore *store;
	GdaMetaStruct *mstruct;

	/* Initialize GTK+. */
	gtk_set_locale ();
	gnome_db_init ();
	gtk_init (&argc, &argv);
  
	/* connection */
	cnc = gda_connection_open_from_string (NULL, "Sqlite://DB_DIR=../extra/demos;DB_NAME=demo_db", 
					       NULL, 0, NULL);
        if (!cnc) {
                g_print ("Can't open connection...\n");
                exit (1);
        }
	store = gda_connection_get_meta_store (cnc);
	g_print ("Updating meta store...\n");
	gda_connection_update_meta_store (cnc, NULL, NULL);

	/* Create the window and widgets. */
	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_default_size (GTK_WINDOW (window), 640, 600);
	gtk_widget_show (window);
	g_signal_connect (window, "delete_event", (GtkSignalFunc) on_delete_event,
			  NULL);
  
	/* notebook */
	nb = gtk_notebook_new ();
	gtk_container_add (GTK_CONTAINER (window), nb);
	gtk_notebook_set_tab_pos (GTK_NOTEBOOK (nb), GTK_POS_LEFT);

	/*
	 * Basic GooCanvas 
	 */
	scrolled_win = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_win),
					GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_widget_show (scrolled_win);
	gtk_notebook_append_page (GTK_NOTEBOOK (nb), scrolled_win, gtk_label_new ("Basic"));
  
	canvas = goo_canvas_new ();
	gtk_widget_set_size_request (canvas, 600, 450);
	goo_canvas_set_bounds (GOO_CANVAS (canvas), 0, 0, 600, 450);
	gtk_container_add (GTK_CONTAINER (scrolled_win), canvas);
  
	root = goo_canvas_get_root_item (GOO_CANVAS (canvas));

	/* GnomeDbCanvasText */
	item = gnome_db_canvas_text_new (root, "GnomeDbCanvasText", 100., 100., NULL);
	item = gnome_db_canvas_text_new (root, "GnomeDbCanvasText", 100., 120., NULL);
	item = gnome_db_canvas_text_new (root, "GnomeDbCanvasText", 100., 140., NULL);
  
	/* Entity: table */
	mstruct = gda_meta_struct_new (store, GDA_META_STRUCT_FEATURE_FOREIGN_KEYS);
	GdaMetaTable *mtable;
	GValue *value;
	g_value_set_string ((value = gda_value_new (G_TYPE_STRING)), "customers");
	mtable = gda_meta_struct_complement (mstruct, GDA_META_DB_TABLE, 
					     NULL, NULL, value, NULL);
	if (mtable) 
		item = gnome_db_canvas_table_new (root, mtable, 200., 200., NULL);

	/*
	 * test GnomeDbCanvasDbRelations
	 */
	vb = gtk_vbox_new (FALSE, 5);
	gtk_notebook_append_page (GTK_NOTEBOOK (nb), vb, gtk_label_new ("DbRelations"));

	scrolled_win = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_win),
					GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_box_pack_start (GTK_BOX (vb), scrolled_win, TRUE, TRUE, 0);

	canvas = gnome_db_canvas_db_relations_new (store);
	//gtk_widget_set_size_request (canvas, 600, 450);
	gtk_container_add (GTK_CONTAINER (scrolled_win), canvas);
	//g_object_set (canvas, "graph", graph, NULL);

	bbox = gtk_hbutton_box_new ();
	gtk_box_pack_start (GTK_BOX (vb), bbox, FALSE, TRUE, 0);

	button = gtk_button_new_with_label ("Auto layout");
	gtk_box_pack_start (GTK_BOX (bbox), button, FALSE, TRUE, 0);
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (auto_layout_cb), canvas);

	button = gtk_button_new_with_label ("Center");
	gtk_box_pack_start (GTK_BOX (bbox), button, FALSE, TRUE, 0);
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (center_display_cb), canvas);

	button = gtk_button_new_with_label ("Destroy");
	gtk_box_pack_start (GTK_BOX (bbox), button, FALSE, TRUE, 0);
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (destroy_widget_cb), canvas);

#define ALLBUILD_NO
#ifdef ALLBUILD
	/*
	 * test GnomeDbCanvasDbRelations side by side
	 */
	GtkWidget *paned;

	paned = gtk_hpaned_new ();
	gtk_notebook_append_page (GTK_NOTEBOOK (nb), paned, gtk_label_new ("2 DbRelations"));

	graph = (GnomeDbGraph*) gda_dict_get_object_by_name (dict, GNOME_DB_TYPE_GRAPH, "Global");

	
	scrolled_win = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_win),
					GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_paned_add1 (GTK_PANED (paned), scrolled_win);

	canvas = gnome_db_canvas_db_relations_new (dict);
	gtk_widget_set_size_request (canvas, 400, 450);
	gtk_container_add (GTK_CONTAINER (scrolled_win), canvas);
	g_object_set (canvas, "graph", graph, NULL);

	scrolled_win = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scrolled_win),
					GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_paned_add2 (GTK_PANED (paned), scrolled_win);

	canvas = gnome_db_canvas_db_relations_new (dict);
	gtk_widget_set_size_request (canvas, 400, 450);
	gtk_container_add (GTK_CONTAINER (scrolled_win), canvas);
	g_object_set (canvas, "graph", graph, NULL);
#endif
       
	/* Pass control to the GTK+ main event loop. */
	gtk_widget_show_all (nb);
	gtk_notebook_set_current_page (GTK_NOTEBOOK (nb), 1);
	gtk_main ();
  
	return 0;
}  

static void
auto_layout_cb (GtkButton *button, GnomeDbCanvas *canvas)
{
	gnome_db_canvas_perform_auto_layout (canvas, TRUE, GNOME_DB_CANVAS_LAYOUT_DEFAULT);
}

static void
center_display_cb (GtkButton *button, GnomeDbCanvas *canvas)
{
	gnome_db_canvas_center (canvas);
}

static void
destroy_widget_cb (GtkButton *button, GtkWidget *wid)
{
	gtk_widget_destroy (wid);
}
  
static gboolean
on_delete_event (GtkWidget *window, GdkEvent  *event, gpointer   unused_data)
{
	exit (0);
}

