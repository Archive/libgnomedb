/* GNOME DB library
 * Copyright (C) 2007 - 2008 The GNOME Foundation.
 *
 * AUTHORS:
 *      Vivien Malerba <malerba@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#ifndef __LIBGNOME_DB_GRAPH_H_
#define __LIBGNOME_DB_GRAPH_H_

#include <libgnomedb-graph/gnome-db-canvas-cursor.h>
#include <libgnomedb-graph/gnome-db-canvas-db-relations.h>
#include <libgnomedb-graph/gnome-db-canvas-table.h>
#include <libgnomedb-graph/gnome-db-canvas-column.h>
#include <libgnomedb-graph/gnome-db-canvas-fkey.h>
#include <libgnomedb-graph/gnome-db-canvas-item.h>
#include <libgnomedb-graph/gnome-db-canvas-text.h>
#include <libgnomedb-graph/gnome-db-canvas-tip.h>
#include <libgnomedb-graph/gnome-db-canvas.h>

#include <libgnomedb-graph/gnome-db-graph.h>
#include <libgnomedb-graph/gnome-db-graph-item.h>


#endif
