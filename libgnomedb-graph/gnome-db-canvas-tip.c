/* gnome-db-canvas-tip.c
 *
 * Copyright (C) 2002 - 2004 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "gnome-db-decl.h"
#include "gnome-db-canvas-tip.h"
#include <libgnomedb/marshal.h>
#include <libgda/gda-debug-macros.h>

static void gnome_db_canvas_tip_class_init (GnomeDbCanvasTipClass * class);
static void gnome_db_canvas_tip_init       (GnomeDbCanvasTip * tip);
static void gnome_db_canvas_tip_finalize    (GObject   * object);

static void gnome_db_canvas_tip_set_property    (GObject *object,
					guint param_id,
					const GValue *value,
					GParamSpec *pspec);
static void gnome_db_canvas_tip_get_property    (GObject *object,
					guint param_id,
					GValue *value,
					GParamSpec *pspec);

static void gnome_db_canvas_tip_post_init  (GnomeDbCanvasTip * tip);

struct _GnomeDbCanvasTipPrivate
{
	gchar  *text;

	/* presentation parameters */
	gdouble x_text_space;
	gdouble y_text_space;
};

enum
{
	PROP_0,
	PROP_TEXT
};


/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *tip_parent_class = NULL;


GType
gnome_db_canvas_tip_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0)) {
		static const GTypeInfo info = {
			sizeof (GnomeDbCanvasTipClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_canvas_tip_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbCanvasTip),
			0,
			(GInstanceInitFunc) gnome_db_canvas_tip_init
		};		

		type = g_type_register_static (GOO_TYPE_CANVAS_GROUP, "GnomeDbCanvasTip", &info, 0);
	}

	return type;
}

static void
gnome_db_canvas_tip_class_init (GnomeDbCanvasTipClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	tip_parent_class = g_type_class_peek_parent (class);

	object_class->finalize = gnome_db_canvas_tip_finalize;

	/* Properties */
	object_class->set_property = gnome_db_canvas_tip_set_property;
	object_class->get_property = gnome_db_canvas_tip_get_property;

	g_object_class_install_property (object_class, PROP_TEXT,
					 g_param_spec_string ("tip_text", NULL, NULL, NULL,
							      (G_PARAM_READABLE | G_PARAM_WRITABLE)));
}

static void
gnome_db_canvas_tip_init (GnomeDbCanvasTip * tip)
{
	tip->priv = g_new0 (GnomeDbCanvasTipPrivate, 1);
	tip->priv->text = NULL;
	tip->priv->x_text_space = 3.;
	tip->priv->y_text_space = 3.;
}


static void
gnome_db_canvas_tip_finalize (GObject   * object)
{
	GnomeDbCanvasTip *tip;
	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOME_DB_IS_CANVAS_TIP (object));

	tip = GNOME_DB_CANVAS_TIP (object);
	if (tip->priv) {
		if (tip->priv->text)
			g_free (tip->priv->text);
		g_free (tip->priv);
		tip->priv = NULL;
	}

	/* for the parent class */
	tip_parent_class->finalize (object);
}

static void 
gnome_db_canvas_tip_set_property    (GObject *object,
			    guint param_id,
			    const GValue *value,
			    GParamSpec *pspec)
{
	GnomeDbCanvasTip *tip;
	const gchar *str;

	tip = GNOME_DB_CANVAS_TIP (object);

	switch (param_id) {
	case PROP_TEXT:
		if (tip->priv->text) {
			g_free (tip->priv->text);
			tip->priv->text = NULL;
		}
		str = g_value_get_string (value);
		if (str)
			tip->priv->text = g_strdup (str);
		break;
	}

	if (tip->priv->text)
		gnome_db_canvas_tip_post_init (tip);
}

static void 
gnome_db_canvas_tip_get_property (GObject *object,
			       guint param_id,
			       GValue *value,
			       GParamSpec *pspec)
{
	TO_IMPLEMENT;
}


static void 
gnome_db_canvas_tip_post_init  (GnomeDbCanvasTip * tip)
{
	GooCanvasItem *text, *bg;
	GooCanvasBounds bounds;

	text = goo_canvas_text_new (GOO_CANVAS_ITEM (tip),
				    tip->priv->text,
				    tip->priv->x_text_space,
				    tip->priv->y_text_space, -1, GTK_ANCHOR_NORTH_WEST,
				    "fill-color-rgba", 0x000000BB,
				    "alignment", PANGO_ALIGN_LEFT, 
				    NULL);
	goo_canvas_item_get_bounds (text, &bounds);

	bg = goo_canvas_rect_new (GOO_CANVAS_ITEM (tip),
				  (double) 0., (double) 0,
				  bounds.x2 - bounds.x1 + 2*tip->priv->x_text_space,
				  bounds.y2 - bounds.y1 + 2*tip->priv->y_text_space,
				  "stroke-color", "black",
				  "fill-color-rgba", 0xFFEE44BB,
				  "line_width", .7,
				  NULL);
	
	goo_canvas_item_lower (bg, text);
}

/**
 * gnome_db_canvas_tip_new
 * @parent: the parent item, or NULL. 
 * @txt: text to display in the tooltip
 * @x: the x coordinate of the text.
 * @y: the y coordinate of the text.
 * @...: optional pairs of property names and values, and a terminating NULL.
 *
 * Creates a new canvas item to display the @txt message
 *
 * Returns: a new #GooCanvasItem object
 */
GooCanvasItem *
gnome_db_canvas_tip_new (GooCanvasItem *parent, const gchar *txt, gdouble x, gdouble y, ...)
{
	GooCanvasItem *item;
	const char *first_property;
	va_list var_args;
		
	item = g_object_new (GNOME_DB_TYPE_CANVAS_TIP, NULL);

	if (parent) {
		goo_canvas_item_add_child (parent, item, -1);
		g_object_unref (item);
	}

	va_start (var_args, y);
	first_property = va_arg (var_args, char*);
	if (first_property)
		g_object_set_valist ((GObject*) item, first_property, var_args);
	va_end (var_args);

	g_object_set (item, "tip_text", txt, NULL);
	goo_canvas_item_translate (item, x, y);

	return item;
}
