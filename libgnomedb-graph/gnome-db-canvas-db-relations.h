/* gnome-db-canvas-db-relations.h
 *
 * Copyright (C) 2004 - 2007 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNOME_DB_CANVAS_DB_RELATIONS__
#define __GNOME_DB_CANVAS_DB_RELATIONS__

#include "gnome-db-canvas.h"

G_BEGIN_DECLS

#define GNOME_DB_TYPE_CANVAS_DB_RELATIONS          (gnome_db_canvas_db_relations_get_type())
#define GNOME_DB_CANVAS_DB_RELATIONS(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, gnome_db_canvas_db_relations_get_type(), GnomeDbCanvasDbRelations)
#define GNOME_DB_CANVAS_DB_RELATIONS_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, gnome_db_canvas_db_relations_get_type (), GnomeDbCanvasDbRelationsClass)
#define GNOME_DB_IS_CANVAS_DB_RELATIONS(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, gnome_db_canvas_db_relations_get_type ())


typedef struct _GnomeDbCanvasDbRelations        GnomeDbCanvasDbRelations;
typedef struct _GnomeDbCanvasDbRelationsClass   GnomeDbCanvasDbRelationsClass;
typedef struct _GnomeDbCanvasDbRelationsPrivate GnomeDbCanvasDbRelationsPrivate;


/* struct for the object's data */
struct _GnomeDbCanvasDbRelations
{
	GnomeDbCanvas                       widget;

	GnomeDbCanvasDbRelationsPrivate    *priv;
};

/* struct for the object's class */
struct _GnomeDbCanvasDbRelationsClass
{
	GnomeDbCanvasClass                  parent_class;
};

/* generic widget's functions */
GType            gnome_db_canvas_db_relations_get_type        (void) G_GNUC_CONST;

GtkWidget       *gnome_db_canvas_db_relations_new             (GdaMetaStore *store);

GnomeDbCanvasTable *gnome_db_canvas_db_relations_get_table_item  (GnomeDbCanvasDbRelations *canvas, GdaMetaTable *table);
GnomeDbCanvasTable *gnome_db_canvas_db_relations_add_table_item  (GnomeDbCanvasDbRelations *canvas, 
							    const GValue *table_catalog, const GValue *table_schema,
							    const GValue *table_name);

G_END_DECLS

#endif
