/* gnome-db-canvas-fkey.c
 *
 * Copyright (C) 2004 - 2007 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <gtk/gtk.h>
#include <math.h>
#include <libgda/libgda.h>
#include <glib/gi18n-lib.h>
#include "gnome-db-canvas.h"
#include "gnome-db-canvas-fkey.h"
#include "gnome-db-canvas-table.h"
#include "gnome-db-canvas-text.h"
#include "gnome-db-canvas-utility.h"
#include "gnome-db-canvas-db-relations.h"

static void gnome_db_canvas_fkey_class_init (GnomeDbCanvasFkeyClass * class);
static void gnome_db_canvas_fkey_init       (GnomeDbCanvasFkey * cc);
static void gnome_db_canvas_fkey_dispose    (GObject *object);
static void gnome_db_canvas_fkey_finalize   (GObject *object);

static void gnome_db_canvas_fkey_set_property (GObject *object,
					    guint param_id,
					    const GValue *value,
					    GParamSpec *pspec);
static void gnome_db_canvas_fkey_get_property (GObject *object,
					    guint param_id,
					    GValue *value,
					    GParamSpec *pspec);

static void gnome_db_canvas_fkey_get_edge_nodes (GnomeDbCanvasItem *citem, 
					      GnomeDbCanvasItem **from, GnomeDbCanvasItem **to);

static void clean_items (GnomeDbCanvasFkey *cc);
static void create_items (GnomeDbCanvasFkey *cc);
static void update_items (GnomeDbCanvasFkey *cc);

enum
{
	PROP_0,
	PROP_FK_CONSTRAINT
};

struct _GnomeDbCanvasFkeyPrivate
{
	GdaMetaTableForeignKey *fk;
	GnomeDbCanvasTable        *fk_table_item;
	GnomeDbCanvasTable        *ref_pk_table_item;
	GSList                 *shapes; /* list of GnomeDbCanvasCanvasShape structures */
};

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *parent_class = NULL;

GType
gnome_db_canvas_fkey_get_type (void)
{
	static GType type = 0;

        if (G_UNLIKELY (type == 0)) {
		static const GTypeInfo info = {
			sizeof (GnomeDbCanvasFkeyClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_canvas_fkey_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbCanvasFkey),
			0,
			(GInstanceInitFunc) gnome_db_canvas_fkey_init
		};		

		type = g_type_register_static (GNOME_DB_TYPE_CANVAS_ITEM, "GnomeDbCanvasFkey", &info, 0);
	}

	return type;
}	

static void
gnome_db_canvas_fkey_class_init (GnomeDbCanvasFkeyClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	GNOME_DB_CANVAS_ITEM_CLASS (class)->get_edge_nodes = gnome_db_canvas_fkey_get_edge_nodes;

	object_class->dispose = gnome_db_canvas_fkey_dispose;
	object_class->finalize = gnome_db_canvas_fkey_finalize;

	/* Properties */
	object_class->set_property = gnome_db_canvas_fkey_set_property;
	object_class->get_property = gnome_db_canvas_fkey_get_property;
	g_object_class_install_property (object_class, PROP_FK_CONSTRAINT,
					 g_param_spec_pointer ("fk_constraint", "FK constraint", 
							       NULL, 
							       G_PARAM_WRITABLE));
       
}

static void
gnome_db_canvas_fkey_init (GnomeDbCanvasFkey *cc)
{
	cc->priv = g_new0 (GnomeDbCanvasFkeyPrivate, 1);
	cc->priv->fk = NULL;
	cc->priv->fk_table_item = NULL;
	cc->priv->ref_pk_table_item = NULL;
	cc->priv->shapes = NULL;
}

static void
gnome_db_canvas_fkey_dispose (GObject *object)
{
	GnomeDbCanvasFkey *cc;
	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOME_DB_IS_CANVAS_FKEY (object));

	cc = GNOME_DB_CANVAS_FKEY (object);

	clean_items (cc);
	cc->priv->fk = NULL;
	cc->priv->fk_table_item = NULL;
	cc->priv->ref_pk_table_item = NULL;

	/* for the parent class */
	parent_class->dispose (object);
}


static void
gnome_db_canvas_fkey_finalize (GObject *object)
{
	GnomeDbCanvasFkey *cc;
	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOME_DB_IS_CANVAS_FKEY (object));

	cc = GNOME_DB_CANVAS_FKEY (object);
	if (cc->priv) {
		g_slist_free (cc->priv->shapes);
		g_free (cc->priv);
		cc->priv = NULL;
	}

	/* for the parent class */
	parent_class->finalize (object);
}

static void 
gnome_db_canvas_fkey_set_property (GObject *object,
				guint param_id,
				const GValue *value,
				GParamSpec *pspec)
{
	GnomeDbCanvasFkey *cc;

	cc = GNOME_DB_CANVAS_FKEY (object);

	switch (param_id) {
	case PROP_FK_CONSTRAINT:
		if (cc->priv->fk != g_value_get_pointer (value)) {
			cc->priv->fk = g_value_get_pointer (value);
			clean_items (cc);
			create_items (cc);
		}
		break;
	}
}

static void 
gnome_db_canvas_fkey_get_property (GObject *object,
				guint param_id,
				GValue *value,
				GParamSpec *pspec)
{
	GnomeDbCanvasFkey *cc;

	cc = GNOME_DB_CANVAS_FKEY (object);

	switch (param_id) {
	default:
		g_warning ("No such property!");
		break;
	}
}

static void
gnome_db_canvas_fkey_get_edge_nodes (GnomeDbCanvasItem *citem, 
					  GnomeDbCanvasItem **from, GnomeDbCanvasItem **to)
{
	GnomeDbCanvasFkey *cc;

	cc = GNOME_DB_CANVAS_FKEY (citem);

	if (from)
		*from = (GnomeDbCanvasItem*) cc->priv->fk_table_item;
	if (to)
		*to = (GnomeDbCanvasItem*) cc->priv->ref_pk_table_item;
}

static gboolean single_item_enter_notify_event_cb (GooCanvasItem *ci, GooCanvasItem *target_item,
						   GdkEventCrossing *event, GnomeDbCanvasFkey *cc);
static gboolean single_item_leave_notify_event_cb (GooCanvasItem *ci, GooCanvasItem *target_item,
						   GdkEventCrossing *event, GnomeDbCanvasFkey *cc);
static gboolean single_item_button_press_event_cb (GooCanvasItem *ci, GooCanvasItem *target_item,
						   GdkEventButton *event, GnomeDbCanvasFkey *cc);
static void table_item_moved_cb (GooCanvasItem *table, GnomeDbCanvasFkey *cc);
static void table_destroy_cb (GnomeDbCanvasTable *table, GnomeDbCanvasFkey *cc);

/* 
 * destroy any existing GooCanvasItem objects 
 */
static void 
clean_items (GnomeDbCanvasFkey *cc)
{
	if (cc->priv->fk_table_item) {
		g_signal_handlers_disconnect_by_func (G_OBJECT (cc->priv->fk_table_item),
						      G_CALLBACK (table_item_moved_cb), cc);
		g_signal_handlers_disconnect_by_func (G_OBJECT (cc->priv->fk_table_item),
                                                      G_CALLBACK (table_destroy_cb), cc);
		cc->priv->fk_table_item = NULL;
	}

	if (cc->priv->ref_pk_table_item) {
		g_signal_handlers_disconnect_by_func (G_OBJECT (cc->priv->ref_pk_table_item),
						      G_CALLBACK (table_item_moved_cb), cc);
		g_signal_handlers_disconnect_by_func (G_OBJECT (cc->priv->ref_pk_table_item),
						      G_CALLBACK (table_destroy_cb), cc);
		cc->priv->ref_pk_table_item = NULL;
	}
	
	/* remove all the GooCanvasItem objects */
	gnome_db_canvas_canvas_shapes_remove_all (cc->priv->shapes);
	cc->priv->shapes = NULL;
}

/*
 * create new GooCanvasItem objects
 */
static void 
create_items (GnomeDbCanvasFkey *cc)
{
	GSList *list, *canvas_shapes;
	GnomeDbCanvasTable *table_item;
	GnomeDbCanvas *canvas = gnome_db_canvas_item_get_canvas (GNOME_DB_CANVAS_ITEM (cc));

	g_assert (cc->priv->fk);

	/* Analyse FK constraint */
	table_item = gnome_db_canvas_db_relations_get_table_item (GNOME_DB_CANVAS_DB_RELATIONS (canvas),
							       GDA_META_TABLE (cc->priv->fk->meta_table));
	cc->priv->fk_table_item = table_item;
	g_return_if_fail (table_item);
	g_signal_connect (G_OBJECT (table_item), "moving",
			  G_CALLBACK (table_item_moved_cb), cc);
	g_signal_connect (G_OBJECT (table_item), "moved",
			  G_CALLBACK (table_item_moved_cb), cc);
	g_signal_connect (G_OBJECT (table_item), "shifted",
			  G_CALLBACK (table_item_moved_cb), cc);
	g_signal_connect (G_OBJECT (table_item), "destroy",
			  G_CALLBACK (table_destroy_cb), cc);

	table_item = gnome_db_canvas_db_relations_get_table_item (GNOME_DB_CANVAS_DB_RELATIONS (canvas),
							       GDA_META_TABLE (cc->priv->fk->depend_on));
	cc->priv->ref_pk_table_item = table_item;
	g_return_if_fail (table_item);
	g_signal_connect (G_OBJECT (table_item), "moving",
			  G_CALLBACK (table_item_moved_cb), cc);
	g_signal_connect (G_OBJECT (table_item), "moved",
			  G_CALLBACK (table_item_moved_cb), cc);
	g_signal_connect (G_OBJECT (table_item), "shifted",
			  G_CALLBACK (table_item_moved_cb), cc);
	g_signal_connect (G_OBJECT (table_item), "destroy",
			  G_CALLBACK (table_destroy_cb), cc);

	/* actual line(s) */
	g_assert (!cc->priv->shapes);
	canvas_shapes = gnome_db_canvas_util_compute_anchor_shapes (GOO_CANVAS_ITEM (cc), NULL,
								 cc->priv->fk_table_item, 
								 cc->priv->ref_pk_table_item, 
								 MAX (cc->priv->fk->cols_nb, 1), 0, TRUE);

	cc->priv->shapes = gnome_db_canvas_canvas_shapes_remove_obsolete_shapes (canvas_shapes);
	for (list = canvas_shapes; list; list = list->next) {
		GooCanvasItem *item = GNOME_DB_CANVAS_CANVAS_SHAPE (list->data)->item;
		gchar *color = "black";
		g_object_set (G_OBJECT (item), 
			      "stroke-color", color,
			      NULL);
		
		if (G_OBJECT_TYPE (item) == GOO_TYPE_CANVAS_POLYLINE) {
			g_object_set (G_OBJECT (item), 
				      "start-arrow", TRUE,
				      "arrow-tip-length", 4.,
				      "arrow-length", 5.,
				      "arrow-width", 4.,
				      NULL);
		}
		else if (G_OBJECT_TYPE (item) == GOO_TYPE_CANVAS_ELLIPSE)
			g_object_set (G_OBJECT (item), 
				      "fill-color", color,
				      NULL);

		g_object_set_data (G_OBJECT (item), "fkcons", cc->priv->fk);
		g_signal_connect (G_OBJECT (item), "enter-notify-event", 
				  G_CALLBACK (single_item_enter_notify_event_cb), cc);
		g_signal_connect (G_OBJECT (item), "leave-notify-event", 
				  G_CALLBACK (single_item_leave_notify_event_cb), cc);
		g_signal_connect (G_OBJECT (item), "button-press-event",
				  G_CALLBACK (single_item_button_press_event_cb), cc);
		
	}
}

/*
 * update GooCanvasItem objects
 */
static void 
update_items (GnomeDbCanvasFkey *cc)
{
	cc->priv->shapes = gnome_db_canvas_util_compute_anchor_shapes (GOO_CANVAS_ITEM (cc), cc->priv->shapes,
								    cc->priv->fk_table_item, 
								    cc->priv->ref_pk_table_item, 
								    MAX (cc->priv->fk->cols_nb, 1), 0, TRUE);
	cc->priv->shapes = gnome_db_canvas_canvas_shapes_remove_obsolete_shapes (cc->priv->shapes);
}

static void popup_delete_cb (GtkMenuItem *mitem, GnomeDbCanvasFkey *cc);

/*
 * item is for a single FK constraint
 */
static gboolean
single_item_enter_notify_event_cb (GooCanvasItem *ci, GooCanvasItem *target_item,
				   GdkEventCrossing *event, GnomeDbCanvasFkey *cc)
{
	gint i;

	for (i = 0; i < cc->priv->fk->cols_nb; i++) {
		GdaMetaTableColumn *tcol;
		GnomeDbCanvasColumn *column;

		/* fk column */
		tcol = g_slist_nth_data (GDA_META_TABLE (cc->priv->fk->meta_table)->columns, 
					 cc->priv->fk->fk_cols_array[i]);

		column = gnome_db_canvas_table_get_column_item (cc->priv->fk_table_item, tcol);
		gnome_db_canvas_text_set_highlight (GNOME_DB_CANVAS_TEXT (column), TRUE);
		
		/* ref pk column */
		tcol = g_slist_nth_data (GDA_META_TABLE (cc->priv->fk->depend_on)->columns, 
					 cc->priv->fk->ref_pk_cols_array[i]);

		column = gnome_db_canvas_table_get_column_item (cc->priv->ref_pk_table_item, tcol);
		gnome_db_canvas_text_set_highlight (GNOME_DB_CANVAS_TEXT (column), TRUE);
	}

	return FALSE;
}

static gboolean
single_item_leave_notify_event_cb (GooCanvasItem *ci, GooCanvasItem *target_item,
				   GdkEventCrossing *event, GnomeDbCanvasFkey *cc)
{
	gint i;

	for (i = 0; i < cc->priv->fk->cols_nb; i++) {
		GdaMetaTableColumn *tcol;
		GnomeDbCanvasColumn *column;

		/* fk column */
		tcol = g_slist_nth_data (GDA_META_TABLE (cc->priv->fk->meta_table)->columns, 
					 cc->priv->fk->fk_cols_array[i]);

		column = gnome_db_canvas_table_get_column_item (cc->priv->fk_table_item, tcol);
		gnome_db_canvas_text_set_highlight (GNOME_DB_CANVAS_TEXT (column), FALSE);
		
		/* ref pk column */
		tcol = g_slist_nth_data (GDA_META_TABLE (cc->priv->fk->depend_on)->columns, 
					 cc->priv->fk->ref_pk_cols_array[i]);

		column = gnome_db_canvas_table_get_column_item (cc->priv->ref_pk_table_item, tcol);
		gnome_db_canvas_text_set_highlight (GNOME_DB_CANVAS_TEXT (column), FALSE);
	}

	return FALSE;
}

static gboolean
single_item_button_press_event_cb (GooCanvasItem *ci, GooCanvasItem *target_item,
				   GdkEventButton *event, GnomeDbCanvasFkey *cc)
{
	GdaMetaTableForeignKey *fkcons = g_object_get_data (G_OBJECT (ci), "fkcons");
	GtkWidget *menu, *entry;

	menu = gtk_menu_new ();
	entry = gtk_menu_item_new_with_label (_("Remove"));
	g_object_set_data (G_OBJECT (entry), "fkcons", fkcons);
	g_signal_connect (G_OBJECT (entry), "activate", G_CALLBACK (popup_delete_cb), cc);
	gtk_menu_append (GTK_MENU (menu), entry);
	gtk_widget_show (entry);
	gtk_menu_popup (GTK_MENU (menu), NULL, NULL,
			NULL, NULL, ((GdkEventButton *)event)->button,
			((GdkEventButton *)event)->time);
	return TRUE;
}


static void
popup_delete_cb (GtkMenuItem *mitem, GnomeDbCanvasFkey *cc)
{
	TO_IMPLEMENT;
}

static void
table_item_moved_cb (GooCanvasItem *table, GnomeDbCanvasFkey *cc)
{
	update_items (cc);
}

static void
table_destroy_cb (GnomeDbCanvasTable *table, GnomeDbCanvasFkey *cc)
{
        goo_canvas_item_remove (GOO_CANVAS_ITEM (cc));
}

/**
 * gnome_db_canvas_fkey_new
 * @parent: the parent item, or NULL. 
 * @fkcons: the #GdaMetaTableForeignKey to represent
 * @...: optional pairs of property names and values, and a terminating NULL.
 *
 * Creates a new canvas item to represent the @fkcons FK constraint
 *
 * Returns: a new #GooCanvasItem object
 */
GooCanvasItem *
gnome_db_canvas_fkey_new (GooCanvasItem *parent, GdaMetaTableForeignKey *fkcons, ...)
{
	GooCanvasItem *item;
	const char *first_property;
	va_list var_args;
		
	item = g_object_new (GNOME_DB_TYPE_CANVAS_FKEY, NULL);

	if (parent) {
		goo_canvas_item_add_child (parent, item, -1);
		g_object_unref (item);
	}

	g_object_set (item, "fk_constraint", fkcons, NULL);

	va_start (var_args, fkcons);
	first_property = va_arg (var_args, char*);
	if (first_property)
		g_object_set_valist ((GObject*) item, first_property, var_args);
	va_end (var_args);

	return item;
}
