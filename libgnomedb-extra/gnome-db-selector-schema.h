/* 
 * Copyright (C) 2008 The GNOME Foundation.
 *
 * AUTHORS:
 *      Vivien Malerba <malerba@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GNOME_DB_SELECTOR_SCHEMA_H__
#define __GNOME_DB_SELECTOR_SCHEMA_H__

#include <glib-object.h>
#include <gtk/gtk.h>
#include <libgnomedb-extra/gnome-db-selector-part.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_SELECTOR_SCHEMA            (gnome_db_selector_schema_get_type())
#define GNOME_DB_SELECTOR_SCHEMA(obj)            (G_TYPE_CHECK_INSTANCE_CAST (obj, GNOME_DB_TYPE_SELECTOR_SCHEMA, GnomeDbSelectorSchema))
#define GNOME_DB_SELECTOR_SCHEMA_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST (klass, GNOME_DB_TYPE_SELECTOR_SCHEMA, GnomeDbSelectorSchemaClass))
#define GNOME_DB_IS_SELECTOR_SCHEMA(obj)         (G_TYPE_CHECK_INSTANCE_TYPE (obj, GNOME_DB_TYPE_SELECTOR_SCHEMA))
#define GNOME_DB_IS_SELECTOR_SCHEMA_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GNOME_DB_TYPE_SELECTOR_SCHEMA))

typedef struct _GnomeDbSelectorSchema        GnomeDbSelectorSchema;
typedef struct _GnomeDbSelectorSchemaClass   GnomeDbSelectorSchemaClass;
typedef struct _GnomeDbSelectorSchemaPrivate GnomeDbSelectorSchemaPrivate;

typedef enum {
	GNOME_DB_SELECTOR_SCHEMA_TABLES = 0,
	GNOME_DB_SELECTOR_SCHEMA_VIEWS,
	GNOME_DB_SELECTOR_SCHEMA_SEQUENCES,
	GNOME_DB_SELECTOR_SCHEMA_PROCEDURES,
	GNOME_DB_SELECTOR_SCHEMA_DOMAINS,

	GNOME_DB_SELECTOR_SCHEMA_LAST
} GnomeDbSelectorSchemaNodeType;

struct _GnomeDbSelectorSchema {
	GObject                 object;
	GnomeDbSelectorSchemaPrivate *priv;
};

struct _GnomeDbSelectorSchemaClass {
	GObjectClass            parent_class;
};

GType                gnome_db_selector_schema_get_type       (void) G_GNUC_CONST;
GnomeDbSelectorPart *gnome_db_selector_schema_new (void);
void                 gnome_db_selector_schema_set_sub_part (GnomeDbSelectorSchema *part, 
							    GnomeDbSelectorSchemaNodeType type,
							    GnomeDbSelectorPart *subpart);

G_END_DECLS

#endif
