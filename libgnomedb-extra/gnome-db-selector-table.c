/* 
 * Copyright (C) 2008 The GNOME Foundation.
 *
 * AUTHORS:
 *      Vivien Malerba <malerba@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <glib/gi18n-lib.h>
#include "gnome-db-selector-table.h"
#include "gnome-db-selector.h"
#include "gnome-db-selector-meta.h"

static const gchar *node_names [GNOME_DB_SELECTOR_TABLE_LAST] = {
	N_("Columns"),
	N_("Indexes"),
	N_("Constraints"),
	N_("Triggers"),
	N_("Rules"),
};

static const gchar *node_icons [GNOME_DB_SELECTOR_TABLE_LAST] = {
	"Columns.png",
	"Indexes.png",
	"Constraints.png",
	"Triggers.png",
	"Rules.png",
};

struct _GnomeDbSelectorTablePrivate {
	GnomeDbSelectorPart    *parent;
	gchar                  *name;
	GdaMetaStore           *store;
	GnomeDbSelectorPart   **parts;
	GdaSet                 *set;
	GnomeDbSelectorFeature  auto_features;
};

/* properties */
enum
{
        PROP_0,
	PROP_META_STORE,
	PROP_AUTO_FEATURES
};

static void gnome_db_selector_table_class_init (GnomeDbSelectorTableClass *klass);
static void gnome_db_selector_table_init       (GnomeDbSelectorTable *part, GnomeDbSelectorTableClass *klass);
static void gnome_db_selector_table_finalize   (GObject *object);
static void gnome_db_selector_table_set_property (GObject *object,
						  guint param_id,
						  const GValue *value,
						  GParamSpec *pspec);
static void gnome_db_selector_table_get_property (GObject *object,
						  guint param_id,
						  GValue *value,
						  GParamSpec *pspec);

static void gnome_db_selector_table_set_sub_part (GnomeDbSelectorTable *part, GnomeDbSelectorTableNodeType type,
						  GnomeDbSelectorPart *subpart);

/* GnomeDbSelectorPart interface */
static void       gnome_db_selector_table_part_iface_init (GnomeDbSelectorPartClass *iface);
static void       gnome_db_selector_table_set_name        (GnomeDbSelectorPart *part, const gchar *name);
static const gchar *gnome_db_selector_table_get_name        (GnomeDbSelectorPart *part);
static void       gnome_db_selector_table_set_parent_part (GnomeDbSelectorPart *part, GnomeDbSelectorPart *parent_part);
static GnomeDbSelectorPart *gnome_db_selector_table_get_parent_part (GnomeDbSelectorPart *part);
static GdkPixbuf *gnome_db_selector_table_get_pixbuf (GnomeDbSelectorPart *part);
static void       gnome_db_selector_table_fill_tree_store (GnomeDbSelectorPart *part, 
							   GtkTreeStore *store, GtkTreeIter *parent_iter);
static GdaSet    *gnome_db_selector_table_get_params (GnomeDbSelectorPart *part, GtkTreeStore *store, GtkTreeIter *at_iter);
static GdaSet    *gnome_db_selector_table_get_data (GnomeDbSelectorPart *part, GtkTreeStore *store, GtkTreeIter *at_iter);
static GObjectClass *parent_class = NULL;

/*
 * GnomeDbSelectorTable class implementation
 */

static void
gnome_db_selector_table_class_init (GnomeDbSelectorTableClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gnome_db_selector_table_finalize;

	/* Properties */
        object_class->set_property = gnome_db_selector_table_set_property;
        object_class->get_property = gnome_db_selector_table_get_property;

	g_object_class_install_property (object_class, PROP_META_STORE,
                                         g_param_spec_object ("meta-store",
							      _("GdaMetaStore"),
							      NULL, GDA_TYPE_META_STORE,
							      G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY |
							      G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_AUTO_FEATURES,
                                         g_param_spec_uint ("auto-features",
							    _("Automatically added features"),
							    NULL, 0, G_MAXUINT, 0,
							    G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY |
							    G_PARAM_READABLE));
}

static void
gnome_db_selector_table_part_iface_init (GnomeDbSelectorPartClass *iface)
{
	iface->set_name = gnome_db_selector_table_set_name;
	iface->get_name = gnome_db_selector_table_get_name;
	iface->set_parent_part = gnome_db_selector_table_set_parent_part;
	iface->get_parent_part = gnome_db_selector_table_get_parent_part;
	iface->get_pixbuf = gnome_db_selector_table_get_pixbuf;
	iface->fill_tree_store = gnome_db_selector_table_fill_tree_store;
	iface->get_params = gnome_db_selector_table_get_params;
	iface->get_data = gnome_db_selector_table_get_data;
}

static void
gnome_db_selector_table_init (GnomeDbSelectorTable *part, GnomeDbSelectorTableClass *klass)
{
	part->priv = g_new0 (GnomeDbSelectorTablePrivate, 1);
	part->priv->parts = g_new0 (GnomeDbSelectorPart*, GNOME_DB_SELECTOR_TABLE_LAST);
	part->priv->set = gda_set_new_inline (3, "catalog", G_TYPE_STRING, NULL,
					      "schema", G_TYPE_STRING, NULL,
					      "table_name", G_TYPE_STRING, NULL);
	part->priv->parent = NULL;
	part->priv->name = g_strdup ("TABLE");
}

static void
gnome_db_selector_table_finalize (GObject *object)
{
	GnomeDbSelectorTable *part = (GnomeDbSelectorTable *) object;
	GnomeDbSelectorTableNodeType type;

	/* free memory */
	if (part->priv->parent)
		g_object_remove_weak_pointer ((GObject*) part->priv->parent, (gpointer*) &(part->priv->parent));

	for (type = GNOME_DB_SELECTOR_TABLE_COLUMNS; type < GNOME_DB_SELECTOR_TABLE_LAST; type++) {
		if (part->priv->parts [type])
			g_object_unref (part->priv->parts [type]);
	}
	if (part->priv->store)
		g_object_unref (part->priv->store);
	g_free (part->priv->parts);
	g_object_unref (part->priv->set);
	g_free (part->priv->name);
	g_free (part->priv);
	part->priv = NULL;

	/* chain to parent class */
	parent_class->finalize (object);
}

GType
gnome_db_selector_table_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0)) {
		static GTypeInfo info = {
			sizeof (GnomeDbSelectorTableClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_selector_table_class_init,
			NULL, NULL,
			sizeof (GnomeDbSelectorTable),
			0,
			(GInstanceInitFunc) gnome_db_selector_table_init
		};
		
		static const GInterfaceInfo selector_part = {
			(GInterfaceInitFunc) gnome_db_selector_table_part_iface_init,
			NULL,
			NULL
		};
		
		type = g_type_register_static (G_TYPE_OBJECT, "GnomeDbSelectorTable", &info, 0);
		g_type_add_interface_static (type, GNOME_DB_TYPE_SELECTOR_PART, &selector_part);
	}

	return type;
}

static GdkPixbuf *
constraint_pix_func (const GValue *value)
{
	static GHashTable *table = NULL;
	GdkPixbuf *pixbuf;
	if (!table) {
		table = g_hash_table_new_full (g_str_hash, g_str_equal, NULL, g_object_unref);
		g_hash_table_insert (table, "CHECK", _gnome_db_selector_create_pixbuf ("CheckConstraint.png"));
		g_hash_table_insert (table, "FOREIGN KEY", _gnome_db_selector_create_pixbuf ("ForeignKey.png"));
		g_hash_table_insert (table, "PRIMARY KEY", _gnome_db_selector_create_pixbuf ("PrimaryKey.png"));
		g_hash_table_insert (table, "UNIQUE", _gnome_db_selector_create_pixbuf ("UniqueConstraint.png"));
	}
	pixbuf = g_hash_table_lookup (table, g_value_get_string (value));
	if (pixbuf)
		g_object_ref (pixbuf);
	return pixbuf;
}

static void
gnome_db_selector_table_set_property (GObject *object,
				      guint param_id,
				      const GValue *value,
				      GParamSpec *pspec)
{
        GnomeDbSelectorTable *part;

        part = GNOME_DB_SELECTOR_TABLE (object);
        if (part->priv) {
                switch (param_id) {
		case PROP_META_STORE:
			part->priv->store = g_value_get_object (value);
			if (part->priv->store) 
				g_object_ref (part->priv->store);
			break;
		case PROP_AUTO_FEATURES:
			part->priv->auto_features = g_value_get_uint (value);
			break;
                }
        }

	if (part->priv->store && part->priv->auto_features) {
		/* Columns sub parts */
		GnomeDbSelectorPart *spart;

		if (part->priv->auto_features & GNOME_DB_SELECTOR_FEATURE_SCHEMA_TABLE_COLUMNS) {
			spart =  gnome_db_selector_meta_new_with_sql (part->priv->store,
			     "SELECT column_name FROM _columns "
			     "WHERE table_catalog = ##catalog::string AND table_schema = ##schema::string "
			     "AND table_name = ##table_name::string "
			     "ORDER BY ordinal_position", NULL);
			if (!spart) 
				g_warning (_("Could not create GnomeDbSelectorMeta for table's columns"));
			else {
				if (!gnome_db_selector_meta_add_sql (GNOME_DB_SELECTOR_META (spart),
			      "SELECT column_name FROM _columns AS c NATURAL JOIN _tables "
                              "WHERE table_short_name != table_full_name AND c.table_name = ##table_name::string "
			      "ORDER BY ordinal_position", NULL))
					g_warning (_("Could not create GnomeDbSelectorMeta for table's columns"));
				g_object_set (G_OBJECT (spart), "pixbuf-file", "Column.png", NULL);
				gnome_db_selector_table_set_sub_part (part, GNOME_DB_SELECTOR_TABLE_COLUMNS, spart);
				g_object_unref (spart);
			}
		}

		if (part->priv->auto_features & GNOME_DB_SELECTOR_FEATURE_SCHEMA_TABLE_CONSTRAINTS) {
			spart =  gnome_db_selector_meta_new_with_sql (part->priv->store,
			     "SELECT constraint_name, constraint_type FROM _table_constraints "
			     "WHERE table_catalog = ##catalog::string AND table_schema = ##schema::string "
			     "AND table_name = ##table_name::string "
			     "ORDER BY constraint_name", NULL);
			if (!spart) 
				g_warning (_("Could not create GnomeDbSelectorMeta for table's constraints"));
			else {
				if (!gnome_db_selector_meta_add_sql (GNOME_DB_SELECTOR_META (spart),
			     "SELECT constraint_name, constraint_type FROM _table_constraints AS c "
                             "NATURAL JOIN _tables "
                             "WHERE table_short_name != table_full_name AND c.table_name = ##table_name::string "
			     "ORDER BY constraint_name", NULL))
					g_warning (_("Could not create GnomeDbSelectorMeta for table's constraints"));
				gnome_db_selector_meta_set_pixbuf_func (GNOME_DB_SELECTOR_META (spart), 1,
									constraint_pix_func);
				gnome_db_selector_table_set_sub_part (part, GNOME_DB_SELECTOR_TABLE_CONSTRAINTS, 
								      spart);
				g_object_unref (spart);
			}
		}

		if (part->priv->auto_features & GNOME_DB_SELECTOR_FEATURE_SCHEMA_TABLE_TRIGGERS) {
			spart =  gnome_db_selector_meta_new_with_sql (part->priv->store,
			     "SELECT trigger_name FROM _triggers "
			     "WHERE event_object_catalog = ##catalog::string AND event_object_schema = ##schema::string "
			     "AND event_object_table = ##table_name::string "
			     "ORDER BY trigger_name", NULL);
			if (!spart) 
				g_warning (_("Could not create GnomeDbSelectorMeta for table's triggers"));
			else {
				if (!gnome_db_selector_meta_add_sql (GNOME_DB_SELECTOR_META (spart),
			     "SELECT trigger_name FROM _triggers AS r INNER JOIN _tables AS t "
			     "ON (t.table_catalog = r.event_object_catalog AND t.table_schema = r.event_object_schema "
                             "AND t.table_name = r.event_object_table) "
			     "WHERE table_short_name != table_full_name "
			     "AND event_object_table = ##table_name::string "
			     "ORDER BY trigger_name", NULL))
					g_warning (_("Could not create GnomeDbSelectorMeta for table's triggers"));
				g_object_set (G_OBJECT (spart), "pixbuf-file", "Trigger.png", NULL);
				gnome_db_selector_table_set_sub_part (part, GNOME_DB_SELECTOR_TABLE_TRIGGERS, spart);
				g_object_unref (spart);
			}
		}
	}
}

static void
gnome_db_selector_table_get_property (GObject *object,
				 guint param_id,
				 GValue *value,
				 GParamSpec *pspec)
{
        GnomeDbSelectorTable *part;

        part = GNOME_DB_SELECTOR_TABLE (object);
        if (part->priv) {
                switch (param_id) {
		case PROP_META_STORE:
			g_value_set_object (value, part->priv->store);
			break;
		case PROP_AUTO_FEATURES:
			g_value_set_uint (value, part->priv->auto_features);
			break;
                }
        }
}

static void
gnome_db_selector_table_set_name (GnomeDbSelectorPart *part, const gchar *name)
{
	GnomeDbSelectorTable *spart = (GnomeDbSelectorTable*) part;
	g_free (spart->priv->name);
	if (name)
		spart->priv->name = g_strdup (name);
	else
		spart->priv->name = NULL;
}

static const gchar *
gnome_db_selector_table_get_name (GnomeDbSelectorPart *part)
{
	GnomeDbSelectorTable *spart = (GnomeDbSelectorTable*) part;
	return spart->priv->name;
}

static void
gnome_db_selector_table_set_parent_part (GnomeDbSelectorPart *part, GnomeDbSelectorPart *parent_part)
{
	GnomeDbSelectorTable *spart = (GnomeDbSelectorTable*) part;
	if (spart->priv->parent) 
		g_object_remove_weak_pointer ((GObject*) spart->priv->parent, (gpointer *) &(spart->priv->parent));
	spart->priv->parent = parent_part;
	g_object_add_weak_pointer ((GObject*) spart->priv->parent, (gpointer *) &(spart->priv->parent));
}

static GnomeDbSelectorPart *
gnome_db_selector_table_get_parent_part (GnomeDbSelectorPart *part)
{
	return ((GnomeDbSelectorTable*) part)->priv->parent;
}

static GdkPixbuf *
gnome_db_selector_table_get_pixbuf (GnomeDbSelectorPart *part)
{
	return _gnome_db_selector_create_pixbuf ("Table.png");
}

static void
gnome_db_selector_table_fill_tree_store (GnomeDbSelectorPart *part,
					 GtkTreeStore *store, GtkTreeIter *parent_iter)
{
	GtkTreeIter iter;
	GdkPixbuf *pixbuf;
	GnomeDbSelectorTableNodeType type;
	GnomeDbSelectorTable *spart = (GnomeDbSelectorTable*) part;
	GnomeDbSelectorPartFillMode mode = GNOME_DB_SELECTOR_PART_FILL_MODE_FIRST_FILL;

	/* find the first iter below @parent_iter for which GNOME_DB_SELECTOR_PART_PART_COLUMN = @part */
	if (gtk_tree_model_iter_children ((GtkTreeModel*) store, &iter, parent_iter)) {
		do {
			GnomeDbSelectorPart *apart;
			gtk_tree_model_get ((GtkTreeModel*) store, &iter,
					    GNOME_DB_SELECTOR_PART_PART_COLUMN, &apart, -1);
			if (apart) {
				g_object_unref (apart);
				if (apart == part) {
					mode = GNOME_DB_SELECTOR_PART_FILL_MODE_OVW_FIRST;
					break;
				}
			}
		}
		while (gtk_tree_model_iter_next ((GtkTreeModel*) store, &iter));
	}

	for (type = GNOME_DB_SELECTOR_TABLE_COLUMNS; type < GNOME_DB_SELECTOR_TABLE_LAST; type++) {
		if (!spart->priv->parts [type])
			continue;
		pixbuf = NULL;
		gnome_db_selector_part_obtain_new_store_row (part, &mode, store, &iter, parent_iter);
		pixbuf = gnome_db_selector_part_get_pixbuf (spart->priv->parts [type]);
		if (!pixbuf)
			pixbuf = _gnome_db_selector_create_pixbuf (node_icons[type]);
		gtk_tree_store_set (store, &iter,
				    GNOME_DB_SELECTOR_PART_LABEL_COLUMN, node_names[type], 
				    GNOME_DB_SELECTOR_PART_PIXBUF_COLUMN, pixbuf, 
				    GNOME_DB_SELECTOR_PART_PART_COLUMN, part, -1);
		if (pixbuf)
			g_object_unref (pixbuf);

		/* sub part */
		gnome_db_selector_part_fill_tree_store (spart->priv->parts [type], store, &iter);
	}
}

static GdaSet *
gnome_db_selector_table_get_params (GnomeDbSelectorPart *part, GtkTreeStore *store, GtkTreeIter *at_iter)
{
	GnomeDbSelectorTable *spart = (GnomeDbSelectorTable*) part;
	return spart->priv->set;
}

static GdaSet *
gnome_db_selector_table_get_data (GnomeDbSelectorPart *part, GtkTreeStore *store, GtkTreeIter *at_iter)
{
	return NULL;
}


/**
 * gnome_db_selector_table_new
 * @store: a #GdaMetaStore object
 *
 * Returns: a new #GnomeDbSelectorPart object
 */
GnomeDbSelectorPart *
gnome_db_selector_table_new (GdaMetaStore *store)
{
	g_return_val_if_fail (GDA_IS_META_STORE (store), NULL);
	return GNOME_DB_SELECTOR_PART (g_object_new (GNOME_DB_TYPE_SELECTOR_TABLE, "meta-store", store, NULL));
}

/*
 * gnome_db_selector_table_set_sub_part
 * @part:
 * @type:
 * @subpart:
 *
 * Attach a #GnomeDbSelectorPart as a sub part of @part
 */
static void
gnome_db_selector_table_set_sub_part (GnomeDbSelectorTable *part, GnomeDbSelectorTableNodeType type,
				      GnomeDbSelectorPart *subpart)
{
	g_return_if_fail (GNOME_DB_IS_SELECTOR_TABLE (part));
	g_return_if_fail (GNOME_DB_IS_SELECTOR_PART (subpart));
	g_return_if_fail ((type >= 0) && (type < GNOME_DB_SELECTOR_TABLE_LAST));

	/* add new sub part */
	if (part->priv->parts [type])
		g_object_unref (part->priv->parts [type]);
	part->priv->parts [type] = subpart;
	g_object_ref (subpart);

	gnome_db_selector_part_set_parent_part (subpart, (GnomeDbSelectorPart *) part);
}
