/* 
 * Copyright (C) 2008 The GNOME Foundation.
 *
 * AUTHORS:
 *      Vivien Malerba <malerba@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GNOME_DB_SELECTOR_META_H__
#define __GNOME_DB_SELECTOR_META_H__

#include <glib-object.h>
#include <gtk/gtk.h>
#include <libgnomedb-extra/gnome-db-selector-part.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_SELECTOR_META            (gnome_db_selector_meta_get_type())
#define GNOME_DB_SELECTOR_META(obj)            (G_TYPE_CHECK_INSTANCE_CAST (obj, GNOME_DB_TYPE_SELECTOR_META, GnomeDbSelectorMeta))
#define GNOME_DB_SELECTOR_META_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST (klass, GNOME_DB_TYPE_SELECTOR_META, GnomeDbSelectorMetaClass))
#define GNOME_DB_IS_SELECTOR_META(obj)         (G_TYPE_CHECK_INSTANCE_TYPE (obj, GNOME_DB_TYPE_SELECTOR_META))
#define GNOME_DB_IS_SELECTOR_META_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GNOME_DB_TYPE_SELECTOR_META))

typedef struct _GnomeDbSelectorMeta        GnomeDbSelectorMeta;
typedef struct _GnomeDbSelectorMetaClass   GnomeDbSelectorMetaClass;
typedef struct _GnomeDbSelectorMetaPrivate GnomeDbSelectorMetaPrivate;


struct _GnomeDbSelectorMeta {
	GObject                 object;
	GnomeDbSelectorMetaPrivate *priv;
};

struct _GnomeDbSelectorMetaClass {
	GObjectClass            parent_class;
};

typedef GdkPixbuf *(*GnomeDbSelectorMetaPixbufFunc) (const GValue *value);
GType                gnome_db_selector_meta_get_type        (void) G_GNUC_CONST;
GnomeDbSelectorPart *gnome_db_selector_meta_new_with_sql    (GdaMetaStore *store, const gchar *sql, GError **error);
gboolean             gnome_db_selector_meta_add_sql         (GnomeDbSelectorMeta *part, const gchar *sql, GError **error);
void                 gnome_db_selector_meta_set_sub_part    (GnomeDbSelectorMeta *part, GnomeDbSelectorPart *subpart);
void                 gnome_db_selector_meta_set_pixbuf_func (GnomeDbSelectorMeta *part, gint column,
							     GnomeDbSelectorMetaPixbufFunc func);

G_END_DECLS

#endif
