/* 
 * Copyright (C) 2008 The GNOME Foundation.
 *
 * AUTHORS:
 *      Vivien Malerba <malerba@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GNOME_DB_SELECTOR_PART__
#define __GNOME_DB_SELECTOR_PART__

#include <libgda/libgda.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_SELECTOR_PART            (gnome_db_selector_part_get_type())
#define GNOME_DB_SELECTOR_PART(obj)            (G_TYPE_CHECK_INSTANCE_CAST (obj, GNOME_DB_TYPE_SELECTOR_PART, GnomeDbSelectorPart))
#define GNOME_DB_IS_SELECTOR_PART(obj)         (G_TYPE_CHECK_INSTANCE_TYPE (obj, GNOME_DB_TYPE_SELECTOR_PART))
#define GNOME_DB_SELECTOR_PART_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_INTERFACE ((obj), GNOME_DB_TYPE_SELECTOR_PART, GnomeDbSelectorPartClass))

typedef struct _GnomeDbSelectorPartClass   GnomeDbSelectorPartClass;
typedef struct _GnomeDbSelectorPart        GnomeDbSelectorPart;

#define GNOME_DB_SELECTOR_PART_PIXBUF_COLUMN 0
#define GNOME_DB_SELECTOR_PART_LABEL_COLUMN 1
#define GNOME_DB_SELECTOR_PART_PART_COLUMN 2
#define GNOME_DB_SELECTOR_PART_PRIVATE_OBJ_COLUMN 3
#define GNOME_DB_SELECTOR_PART_PRIVATE_INT_COLUMN 4

/* struct for the interface */
struct _GnomeDbSelectorPartClass {
	GTypeInterface           g_iface;

	/* virtual table */
	void                 (*set_name)        (GnomeDbSelectorPart *part, const gchar *name);
	const gchar         *(*get_name)        (GnomeDbSelectorPart *part);
	void                 (*set_parent_part) (GnomeDbSelectorPart *part, GnomeDbSelectorPart *parent_part);
	GnomeDbSelectorPart *(*get_parent_part) (GnomeDbSelectorPart *part);
	GdkPixbuf           *(*get_pixbuf)      (GnomeDbSelectorPart *part);
	void                 (*fill_tree_store) (GnomeDbSelectorPart *part, 
						 GtkTreeStore *store, GtkTreeIter *parent_iter);
	GdaSet              *(*get_params)      (GnomeDbSelectorPart *part, GtkTreeStore *store, GtkTreeIter *at_iter);
	GdaSet              *(*get_data)        (GnomeDbSelectorPart *part, GtkTreeStore *store, GtkTreeIter *at_iter);
};

GType                gnome_db_selector_part_get_type        (void) G_GNUC_CONST;

void                 gnome_db_selector_part_set_name        (GnomeDbSelectorPart *part, const gchar *name);
const gchar         *gnome_db_selector_part_get_name        (GnomeDbSelectorPart *part);

void                 gnome_db_selector_part_set_parent_part (GnomeDbSelectorPart *part, 
							     GnomeDbSelectorPart *parent_part);
GnomeDbSelectorPart *gnome_db_selector_part_get_parent_part (GnomeDbSelectorPart *part);
GdkPixbuf           *gnome_db_selector_part_get_pixbuf      (GnomeDbSelectorPart *part);
void                 gnome_db_selector_part_fill_tree_store (GnomeDbSelectorPart *part, 
							     GtkTreeStore *store, GtkTreeIter *parent);

GdaSet              *gnome_db_selector_part_get_params      (GnomeDbSelectorPart *part, 
							     GtkTreeStore *store, GtkTreeIter *at_iter);
GdaSet              *gnome_db_selector_part_get_data        (GnomeDbSelectorPart *part, 
							     GtkTreeStore *store, GtkTreeIter *at_iter);
const GValue        *gnome_db_selector_part_get_value       (GnomeDbSelectorPart *part, const gchar *id,
							     GtkTreeStore *store, GtkTreeIter *at_iter);

/* helper functions when implementing new GnomeDbSelectorPart objects */
typedef enum {
	GNOME_DB_SELECTOR_PART_FILL_MODE_FIRST_FILL,
	GNOME_DB_SELECTOR_PART_FILL_MODE_OVW_FIRST,
	GNOME_DB_SELECTOR_PART_FILL_MODE_OVW_NEXT
} GnomeDbSelectorPartFillMode;

void gnome_db_selector_part_obtain_new_store_row (GnomeDbSelectorPart *part, GnomeDbSelectorPartFillMode *in_out_mode,
						  GtkTreeStore *store, GtkTreeIter *iter, GtkTreeIter *parent_iter);

G_END_DECLS

#endif
