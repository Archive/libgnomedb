/* GNOME DB library
 * Copyright (C) 1998 - 2008 The GNOME Foundation.
 *
 * AUTHORS:
 *	Michael Lausch <michael@lausch.at>
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <string.h>
#include <libgda/gda-connection-event.h>
#include <libgnomedb-extra/gnome-db-error.h>
#include <libgnomedb/gnome-db-util.h>
#include <gtk/gtkbutton.h>
#include <gtk/gtkentry.h>
#include <gtk/gtkimage.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkstock.h>
#include <gtk/gtktable.h>
#include <gtk/gtktextview.h>
#include <glib/gi18n-lib.h>

#define PARENT_TYPE GTK_TYPE_VBOX

struct _GnomeDbErrorPrivate {
	GList *current_list;
	gint nerrors;
	gint current_pos;

	GtkWidget *error_number;
	GtkWidget *error_description;

	gint height; // Height of the dialog window when no details shown
	GtkWidget *detail_button;
	GtkWidget *detail_container;
	GtkWidget *error_source;
};

static void gnome_db_error_class_init (GnomeDbErrorClass *klass);
static void gnome_db_error_init       (GnomeDbError *error, GnomeDbErrorClass *klass);
static void gnome_db_error_finalize   (GObject *object);

static GObjectClass *parent_class = NULL;

/*
 * Private functions
 */
static void
display_current_error (GnomeDbError *error_widget)
{
	GdaConnectionEvent *error;
	GList *l;
	gchar *tmp;

	g_return_if_fail (GNOME_DB_IS_ERROR (error_widget));

	l = g_list_nth (error_widget->priv->current_list,
			error_widget->priv->current_pos);
	if (!l)
		return;

	error = GDA_CONNECTION_EVENT (l->data);
	tmp = g_strdup_printf ("%ld", gda_connection_event_get_code (error));
	gtk_entry_set_text (GTK_ENTRY (error_widget->priv->error_number), tmp);
	g_free (tmp);

	tmp = (gchar *) gda_connection_event_get_description (error);
	gnome_db_text_set_text (GTK_TEXT_VIEW (error_widget->priv->error_description),
				tmp, strlen (tmp));

	gtk_entry_set_text (GTK_ENTRY (error_widget->priv->error_source),
			    gda_connection_event_get_source (error));
}

/*
 * Callbacks
 */
static void
detail_button_clicked_cb (GtkButton *button, gpointer user_data)
{
	GtkButton *btn;
	GnomeDbError *error_widget = (GnomeDbError *) user_data;
	gint width;
	gint height;
	GtkWindow *window;

	g_return_if_fail (GNOME_DB_IS_ERROR (error_widget));

	window = GTK_WINDOW (gtk_widget_get_toplevel (GTK_WIDGET (error_widget)));
	if (!GTK_WIDGET_TOPLEVEL (window))
		window = NULL;

	btn = GTK_BUTTON (error_widget->priv->detail_button);
	if (GTK_WIDGET_VISIBLE (error_widget->priv->detail_container)) {
		gtk_widget_hide (error_widget->priv->detail_container);
		g_object_set (G_OBJECT (btn), "label", _("Show detail >>"), NULL);
		if (window){
			gtk_window_get_size (window, &width, &height);
			gtk_window_resize (window,
					   width,
					   error_widget->priv->height);
		}
	} else {
		gtk_widget_show_all (error_widget->priv->detail_container);
		g_object_set (G_OBJECT (btn), "label", _("<< Hide detail"), NULL);
		if (window){
			gtk_window_get_size (window,
					     &width,
					     &error_widget->priv->height);
		}
	}
}

/*
 * GnomeDbError class implementation
 */

GType
gnome_db_error_get_type(void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0)) {
		static const GTypeInfo info = {
			sizeof (GnomeDbErrorClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_error_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbError),
			0,
			(GInstanceInitFunc) gnome_db_error_init
		};
		type = g_type_register_static (GTK_TYPE_VBOX, "GnomeDbError", &info, 0);
	}
	return type;
}

static void
gnome_db_error_class_init (GnomeDbErrorClass *klass)
{
	GObjectClass* object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gnome_db_error_finalize;
}

static void
gnome_db_error_init (GnomeDbError *error_widget, GnomeDbErrorClass *klass)
{
	GtkWidget *table;
	GtkWidget *label;
	GtkWidget *scroll;
	GtkWidget *image;
	gchar *str;

	error_widget->priv = g_new (GnomeDbErrorPrivate, 1);
	error_widget->priv->current_list = NULL;
	error_widget->priv->current_pos = -1;

	/* create main container */
	table = gnome_db_new_table_widget (4, 4, FALSE);
	gtk_box_pack_start (GTK_BOX (error_widget), table, TRUE, TRUE, 0);

	image = gtk_image_new_from_stock (GTK_STOCK_DIALOG_ERROR, GTK_ICON_SIZE_DIALOG);
	gtk_widget_show (image);
	gtk_table_attach (GTK_TABLE (table), image, 0, 1, 0, 1, GTK_FILL, GTK_FILL, 3, 3);

	label = gnome_db_new_label_widget ("");
	gtk_misc_set_alignment (GTK_MISC (label), 0.5, 0.5);
	str = g_strdup_printf ("<b>%s</b>",
			       _("An error has occurred in the underlying database"));
	gtk_label_set_markup (GTK_LABEL (label), str);
	g_free (str);
	gtk_table_attach (GTK_TABLE (table), label, 1, 4, 0, 1, GTK_FILL, GTK_FILL, 3, 3);
 
	label = gnome_db_new_label_widget (_("Error number"));
	gtk_table_attach (GTK_TABLE (table), label, 1, 2, 1, 2, GTK_FILL, GTK_FILL, 3, 3);
	error_widget->priv->error_number = gnome_db_new_entry_widget (0, FALSE);
	gtk_table_attach (GTK_TABLE (table), error_widget->priv->error_number,
			  2, 3, 1, 2, GTK_FILL, GTK_FILL, 3, 3);

	error_widget->priv->detail_button = gnome_db_new_button_widget (_("Show detail >>"));
	gtk_signal_connect (GTK_OBJECT (error_widget->priv->detail_button),
			    "clicked",
			    GTK_SIGNAL_FUNC (detail_button_clicked_cb),
			    error_widget);
	gtk_table_attach (GTK_TABLE (table), error_widget->priv->detail_button,
			  3, 4, 1, 2, GTK_FILL, GTK_FILL, 3, 3);

	label = gnome_db_new_label_widget (_("Description"));
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.0);
	gtk_table_attach (GTK_TABLE (table), label, 1, 2, 2, 3, GTK_FILL, GTK_FILL, 3, 3);
	scroll = gnome_db_new_scrolled_window_widget ();
	gtk_table_attach (GTK_TABLE (table), scroll, 2, 3, 2, 3,
			  GTK_FILL | GTK_EXPAND | GTK_SHRINK,
			  GTK_FILL | GTK_EXPAND | GTK_SHRINK, 3, 3);
	error_widget->priv->error_description = gnome_db_new_text_widget (NULL);
	gtk_container_add (GTK_CONTAINER (scroll), error_widget->priv->error_description);

	/* create the detail container */
	error_widget->priv->detail_container = gnome_db_new_table_widget (2, 3, FALSE);
	gtk_table_attach (GTK_TABLE (table), error_widget->priv->detail_container,
			  0, 4, 3, 4, GTK_FILL | GTK_SHRINK | GTK_EXPAND,
			  GTK_FILL | GTK_SHRINK | GTK_EXPAND, 3, 3);

	label = gnome_db_new_label_widget (NULL);
	str = g_strdup_printf ("<b>%s</b>", _("Details"));
	gtk_label_set_markup (GTK_LABEL (label), str);
	g_free (str);
	gtk_table_attach (GTK_TABLE (error_widget->priv->detail_container), label, 0, 3, 0, 1,
			  GTK_FILL | GTK_EXPAND | GTK_SHRINK,
			  GTK_FILL, 6, 6);

	label = gnome_db_new_label_widget (_("Source"));
	gtk_table_attach (GTK_TABLE (error_widget->priv->detail_container), label, 1, 2, 1, 2,
			  GTK_FILL, GTK_FILL, 6, 6);
	error_widget->priv->error_source = gnome_db_new_entry_widget (0, FALSE);
	gtk_table_attach (GTK_TABLE (error_widget->priv->detail_container), error_widget->priv->error_source,
			  2, 3, 1, 2, GTK_FILL | GTK_EXPAND | GTK_SHRINK, GTK_FILL, 6, 6);

	gtk_widget_hide (error_widget->priv->detail_container);
}

static void
gnome_db_error_finalize (GObject *object)
{
	GnomeDbError *error_widget = (GnomeDbError *) object;

	g_return_if_fail (GNOME_DB_IS_ERROR (error_widget));

	/* free memory */
	if (error_widget->priv->current_list) {
		g_list_foreach (error_widget->priv->current_list, (GFunc) g_object_unref, NULL);
		g_list_free (error_widget->priv->current_list);
		error_widget->priv->current_list = NULL;
	}
	g_free (error_widget->priv);
	error_widget->priv = NULL;

	parent_class->finalize (object);
}

/**
 * gnome_db_error_new
 *
 * Create a new GnomeDbError widget, which is a special widget used to
 * traverse errors occurred for a given connection. It is a simple container
 * widget that you can pack into any widget you want.
 *
 * Returns: a pointer to the new widget, or NULL on error
 */
GtkWidget*
gnome_db_error_new (void)
{
	GnomeDbError *error_widget;
  
	error_widget = g_object_new (GNOME_DB_TYPE_ERROR, NULL);
	return GTK_WIDGET (error_widget);
}

/**
 * gnome_db_error_clear
 * @error_widget:
 * 
 */
void
gnome_db_error_clear (GnomeDbError *error_widget)
{
	g_return_if_fail (GNOME_DB_IS_ERROR (error_widget));

	gtk_entry_set_text (GTK_ENTRY (error_widget->priv->error_number), "");
	gnome_db_text_clear (GTK_TEXT_VIEW (error_widget->priv->error_description));
	gtk_entry_set_text (GTK_ENTRY (error_widget->priv->error_source), "");

	if (error_widget->priv->current_list) {
		g_list_foreach (error_widget->priv->current_list, (GFunc) g_object_unref, NULL);
		g_list_free (error_widget->priv->current_list);
		error_widget->priv->current_list = NULL;
	}
	error_widget->priv->nerrors = 0;
	error_widget->priv->current_pos = 0;
}

/**
 * gnome_db_error_show
 * @error_widget: the GnomeDbError widget
 * @errors:
 *
 * Update the given #GnomeDbError widget to show the latest errors
 * from the connection associated with it.
 */
void
gnome_db_error_show (GnomeDbError *error_widget, GList *errors)
{
	g_return_if_fail (GNOME_DB_IS_ERROR (error_widget));

	gnome_db_error_clear (error_widget);

	error_widget->priv->current_list = g_list_copy (errors);
	g_list_foreach (error_widget->priv->current_list, (GFunc) g_object_ref, NULL);
	error_widget->priv->nerrors = g_list_length (error_widget->priv->current_list);

	display_current_error (error_widget);
}

/**
 * gnome_db_error_prev
 * @error_widget: the GnomeDbError widget
 *
 * Display the previous error in the given GnomeDbError widget
 */
void
gnome_db_error_prev (GnomeDbError* error_widget)
{
	g_return_if_fail (GNOME_DB_IS_ERROR (error_widget));

	if (error_widget->priv->current_pos > 0) {
		error_widget->priv->current_pos--;
		display_current_error (error_widget);
	}
}

/**
 * gnome_db_error_next
 * @error_widget: the GnomeDbError widget
 *
 * Display the next error in the given GnomeDbError widget
 */
void
gnome_db_error_next (GnomeDbError* error_widget)
{
	g_return_if_fail (GNOME_DB_IS_ERROR (error_widget));

	if (error_widget->priv->current_pos < error_widget->priv->nerrors - 1) {
		error_widget->priv->current_pos++;
		display_current_error (error_widget);
	}
}

/**
 * gnome_db_error_get_position
 * @error_widget: the GnomeDbError widget.
 *
 * Get the position of the error being displayed.
 *
 * Returns: a non-negative integer on success.
 */
gint
gnome_db_error_get_position (GnomeDbError *error_widget)
{
	g_return_val_if_fail (GNOME_DB_IS_ERROR (error_widget), -1);

	if (!GTK_WIDGET_VISIBLE (GTK_WIDGET (error_widget)))
		return -1;

	return error_widget->priv->current_pos;
}

