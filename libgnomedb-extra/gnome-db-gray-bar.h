/* GNOME DB library
 * Copyright (C) 1999-2002 The GNOME Foundation.
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#if !defined(__gnome_db_gray_bar_h__)
#  define __gnome_db_gray_bar_h__

#include <gtk/gtkbin.h>
#include <gtk/gtkimage.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_GRAY_BAR            (gnome_db_gray_bar_get_type())
#define GNOME_DB_GRAY_BAR(obj)            (G_TYPE_CHECK_INSTANCE_CAST (obj, GNOME_DB_TYPE_GRAY_BAR, GnomeDbGrayBar))
#define GNOME_DB_GRAY_BAR_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST (klass, GNOME_DB_TYPE_GRAY_BAR, GnomeDbGrayBarClass))
#define GNOME_DB_IS_GRAY_BAR(obj)         (G_TYPE_CHECK_INSTANCE_TYPE (obj, GNOME_DB_TYPE_GRAY_BAR))
#define GNOME_DB_IS_GRAY_BAR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GNOME_DB_TYPE_GRAY_BAR))

typedef struct _GnomeDbGrayBar        GnomeDbGrayBar;
typedef struct _GnomeDbGrayBarClass   GnomeDbGrayBarClass;
typedef struct _GnomeDbGrayBarPrivate GnomeDbGrayBarPrivate;

struct _GnomeDbGrayBar {
	GtkBin bin;
	GnomeDbGrayBarPrivate *priv;
};

struct _GnomeDbGrayBarClass {
	GtkBinClass parent_class;
};

GType        gnome_db_gray_bar_get_type (void) G_GNUC_CONST;
GtkWidget   *gnome_db_gray_bar_new (const gchar *label);
const gchar *gnome_db_gray_bar_get_text (GnomeDbGrayBar *bar);
void         gnome_db_gray_bar_set_text (GnomeDbGrayBar *bar, const gchar *text);
void         gnome_db_gray_bar_set_icon_from_file  (GnomeDbGrayBar *bar, const gchar *file);
void         gnome_db_gray_bar_set_icon_from_stock (GnomeDbGrayBar *bar, const gchar *stock_id, GtkIconSize size);
void         gnome_db_gray_bar_set_show_icon       (GnomeDbGrayBar *bar, gboolean show);
gboolean     gnome_db_gray_bar_get_show_icon       (GnomeDbGrayBar *bar);

G_END_DECLS

#endif
