/* gnome-db-selector.c
 *
 * Copyright (C) 2002 - 2008 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <string.h>
#include "gnome-db-selector.h"
#include "extra_marshal.h"

#include <libgda/libgda.h>
#include <glib/gi18n-lib.h>
#include <libgnomedb/binreloc/gnome-db-binreloc.h>
#include "gnome-db-selector-schema.h"

#define PART_CLASS(part) (GNOME_DB_SELECTOR_PART_GET_CLASS (part))

static void gnome_db_selector_class_init (GnomeDbSelectorClass * class);
static void gnome_db_selector_init (GnomeDbSelector * wid);
static void gnome_db_selector_dispose (GObject   * object);

typedef struct {
	gchar               *name;
	GnomeDbSelectorPart *part;
} SelPart;


struct _GnomeDbSelectorPriv {
	GSList              *parts; /* list of @SelPart structures */
};

enum
{
	PROP_0,
};

enum
{
        SELECTED_OBJECT_CHANGED,
        LAST_SIGNAL
};

static gint gnome_db_selector_signals[LAST_SIGNAL] = { 0 };

enum {
	 PIXBUF_COLUMN    = GNOME_DB_SELECTOR_PART_PIXBUF_COLUMN,
	 DISPLAY_COLUMN   = GNOME_DB_SELECTOR_PART_LABEL_COLUMN,
	 PART_COLUMN      = GNOME_DB_SELECTOR_PART_PART_COLUMN,
	 PRIV_OBJ_COLUMN  = GNOME_DB_SELECTOR_PART_PRIVATE_OBJ_COLUMN,
	 PRIV_DATA_COLUMN = GNOME_DB_SELECTOR_PART_PRIVATE_INT_COLUMN,
	 NUM_COLUMNS
 };

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *parent_class = NULL;

static void gnome_db_selector_set_property (GObject *object,
					    guint param_id,
					    const GValue *value,
					    GParamSpec *pspec);
static void gnome_db_selector_get_property (GObject *object,
					    guint param_id,
					    GValue *value,
					    GParamSpec *pspec);


GType
gnome_db_selector_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0)) {
		static const GTypeInfo info = {
			sizeof (GnomeDbSelectorClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_selector_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbSelector),
			0,
			(GInstanceInitFunc) gnome_db_selector_init
		};		
		
		type = g_type_register_static (GTK_TYPE_TREE_VIEW, "GnomeDbSelector", &info, 0);
	}

	return type;
}

static void
gnome_db_selector_class_init (GnomeDbSelectorClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);
	
	parent_class = g_type_class_peek_parent (class);
	gnome_db_selector_signals [SELECTED_OBJECT_CHANGED] = 
		g_signal_new ("selected-object-changed",
                              G_TYPE_FROM_CLASS (object_class),
                              G_SIGNAL_RUN_LAST,
                              G_STRUCT_OFFSET (GnomeDbSelectorClass, selected_object_changed),
                              NULL, NULL,
                              extra_marshal_VOID__OBJECT_OBJECT_POINTER, G_TYPE_NONE, 3,
                              GNOME_DB_TYPE_SELECTOR_PART, GTK_TYPE_TREE_STORE, G_TYPE_POINTER);

	class->selected_object_changed = NULL;
	object_class->dispose = gnome_db_selector_dispose;

	/* Properties */
	object_class->set_property = gnome_db_selector_set_property;
	object_class->get_property = gnome_db_selector_get_property;
}

static void
selection_changed_cb (GtkTreeSelection *selection, GnomeDbSelector *wid)
{
	GtkTreeIter iter;
	GtkTreeModel *model;
	if (gtk_tree_selection_get_selected (selection, &model, &iter)) {
		GnomeDbSelectorPart *part;
		gtk_tree_model_get (GTK_TREE_MODEL (model),  &iter, GNOME_DB_SELECTOR_PART_PART_COLUMN, &part, -1);
		g_signal_emit (wid, gnome_db_selector_signals [SELECTED_OBJECT_CHANGED],
			       0, part, model, &iter);
		if (part)
			g_object_unref (part);
	}
}

static void
gnome_db_selector_init (GnomeDbSelector *wid)
{
	GtkTreeModel *model;
	GtkCellRenderer *renderer;
	GtkTreeViewColumn *column;
	GtkTreeSelection *selection;

	wid->priv = g_new0 (GnomeDbSelectorPriv, 1);
	wid->priv->parts = NULL;

	/* create data model */
	model = GTK_TREE_MODEL (gtk_tree_store_new (NUM_COLUMNS, 
						    GDK_TYPE_PIXBUF, /* PIXBUF_COLUMN */
						    G_TYPE_STRING,   /* DISPLAY_COLUMN */
						    GNOME_DB_TYPE_SELECTOR_PART,  /* PART_COLUMN */
						    G_TYPE_OBJECT, /* PRIV_OBJ_COLUMN */
						    G_TYPE_INT /* PRIV_DATA_COLUMN */
						    ));  

	gtk_tree_view_set_model (GTK_TREE_VIEW (wid), model);
	g_object_unref (model);

	/* column */
	gtk_tree_view_set_headers_visible (GTK_TREE_VIEW (wid), FALSE);

	column = gtk_tree_view_column_new ();
	renderer = gtk_cell_renderer_pixbuf_new ();
	gtk_tree_view_column_pack_start (column, renderer, FALSE);
	gtk_tree_view_column_add_attribute (column, renderer, "pixbuf", PIXBUF_COLUMN);

	renderer = gtk_cell_renderer_text_new ();
	g_object_set (G_OBJECT (renderer), 
		      "strikethrough", TRUE,
		      "strikethrough-set", FALSE, NULL);
	gtk_tree_view_column_pack_start (column, renderer, TRUE);
	gtk_tree_view_column_add_attribute (column, renderer, "text", DISPLAY_COLUMN);

	gtk_tree_view_append_column (GTK_TREE_VIEW (wid), column);

	/* selection */
	selection = gtk_tree_view_get_selection (GTK_TREE_VIEW (wid));
	gtk_tree_selection_set_mode (selection, GTK_SELECTION_SINGLE);
	g_signal_connect (G_OBJECT (selection), "changed",
			  G_CALLBACK (selection_changed_cb), wid);
}

static void 
gnome_db_selector_get_property (GObject *object,
				guint param_id,
				GValue *value,
				GParamSpec *pspec)
{
	/*GnomeDbSelector* sel = GNOME_DB_SELECTOR (object);*/
	switch (param_id) {
	}

}


static void
gnome_db_selector_set_property (GObject *object,
				guint param_id,
				const GValue *value,
				GParamSpec *pspec)
{
	/*GnomeDbSelector* sel = GNOME_DB_SELECTOR(object);*/
	switch (param_id) {
	}
}

/**
 * gnome_db_selector_new
 *
 * Creates a new #GnomeDbSelector widget. If @features is different than GNOME_DB_SELECTOR_FEATURE_NONE
 * then some #GnomeDbSelectorPart will be created and inserted by default in the new selector widget,
 * avoiding much hand written code.
 *
 * Returns: the new widget
 */
GtkWidget *
gnome_db_selector_new (void)
{
	return GTK_WIDGET (g_object_new (GNOME_DB_TYPE_SELECTOR, NULL));
}

static void
gnome_db_selector_dispose (GObject *object)
{
	GnomeDbSelector *sel;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOME_DB_IS_SELECTOR (object));
	sel = GNOME_DB_SELECTOR (object);

	if (sel->priv) {
		/* parts */
		if (sel->priv->parts) {
			GSList *list;
			for (list = sel->priv->parts; list; list = list->next) {
				SelPart *part = (SelPart*) list->data;
				g_free (part->name);
				g_object_unref (part->part);
				g_free (part);
			}
			g_slist_free (sel->priv->parts);
			sel->priv->parts = NULL;
		}

		/* the private area itself */
		g_free (sel->priv);
		sel->priv = NULL;
	}

	/* for the parent class */
	parent_class->dispose (object);
}

static void
part_populate_model (GnomeDbSelector *sel, GtkTreeStore *model, SelPart *selpart)
{
	GtkTreeIter iter;
	static GdkPixbuf *fallback_pixbuf = NULL;
	GdkPixbuf *part_pixbuf;
		
	gtk_tree_store_append (model, &iter, NULL);
	part_pixbuf = gnome_db_selector_part_get_pixbuf (selpart->part);
	if (!part_pixbuf && !fallback_pixbuf)
		fallback_pixbuf = gtk_widget_render_icon ((GtkWidget*) sel,
							  GTK_STOCK_DIRECTORY, GTK_ICON_SIZE_MENU, NULL);
	
	gtk_tree_store_set (model, &iter, 
			    DISPLAY_COLUMN, selpart->name, 
			    PIXBUF_COLUMN, part_pixbuf ? part_pixbuf : fallback_pixbuf, -1);
	if (part_pixbuf)
		g_object_unref (part_pixbuf);
	PART_CLASS (selpart->part)->fill_tree_store (selpart->part, model, &iter);
}

/**
 * gnome_db_selector_add_part
 * @sel: a #GnomeDbSelector widget
 * @name:
 * @part: a #GnomeDbSelectorPart object
 *
 *
 */
void
gnome_db_selector_add_part (GnomeDbSelector *sel, const gchar *name, GnomeDbSelectorPart *part)
{
	GtkTreeStore *model;
	g_return_if_fail (GNOME_DB_IS_SELECTOR (sel));
	g_return_if_fail (GNOME_DB_IS_SELECTOR_PART (part));

	model = GTK_TREE_STORE (gtk_tree_view_get_model (GTK_TREE_VIEW (sel)));

	/* New SelPart structure */
	SelPart *selpart;
	selpart = g_new0 (SelPart, 1);
	selpart->name = g_strdup (name);
	if (part) {
		selpart->part = part;
		g_object_ref (part);
	}
	sel->priv->parts = g_slist_append (sel->priv->parts, selpart);

	if (!sel->priv->parts->next) {
		/* first part => directly as root */
		PART_CLASS (selpart->part)->fill_tree_store (selpart->part, model, NULL);
	}
	else {
		if (!sel->priv->parts->next->next) {
			/* 2 parts where the was only one => clear the model first */
			gtk_tree_store_clear (model);
			part_populate_model (sel, model, (SelPart*) sel->priv->parts->data);
		}
		part_populate_model (sel, model, (SelPart*) selpart);
	}
}

/**
 * gnome_db_selector_add_parts_for_feature
 * @sel: a #GnomeDbSelector widget
 * @store: a #GdaMetaStore object
 * @feature: a #GnomeDbSelectorFeature feature
 *
 * Adds some predefined parts to @sel to implement some features
 */
void
gnome_db_selector_add_parts_for_feature (GnomeDbSelector *sel, GdaMetaStore *store, GnomeDbSelectorFeature feature)
{
	g_return_if_fail (!store || GDA_IS_META_STORE (store));

	if (feature & GNOME_DB_SELECTOR_FEATURE_SCHEMAS) {
		GnomeDbSelectorPart *part;
		part = (GnomeDbSelectorPart*) g_object_new (GNOME_DB_TYPE_SELECTOR_SCHEMA, 
							    "meta-store", store, 
							    "auto-features", feature, NULL);
		gnome_db_selector_add_part (sel, _("Structure"), part);
		g_object_unref (part);
	}
}


GdkPixbuf *
_gnome_db_selector_create_pixbuf (const gchar *filename)
{
	GdkPixbuf *pixbuf;
	gchar *file;

	file = gnome_db_gbr_get_file_path (GNOME_DB_DATA_DIR, "pixmaps", 
					   LIBGNOMEDB_ABI_NAME, "extra", filename, NULL);
	pixbuf = gdk_pixbuf_new_from_file (file, NULL);
	g_free (file);
	return pixbuf;
}
