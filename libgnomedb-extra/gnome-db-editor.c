/* GNOME DB library
 * Copyright (C) 1999 - 2008 The GNOME Foundation.
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *      Vivien Malerba <malerba@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <string.h>
#include <gtk/gtkscrolledwindow.h>
#ifdef HAVE_GTKSOURCEVIEW
#include <gtksourceview/gtksourceview.h>
#include <gtksourceview/gtksourcelanguagemanager.h>
#include <gtksourceview/gtksourcebuffer.h>
#endif
#include <libgda/gda-util.h>
#include <libgda/gda-config.h>
#include <libgnomedb-extra/gnome-db-editor.h>
#include <libgnomedb/gnome-db-util.h>

#ifdef HAVE_GCONF
#include <gconf/gconf-client.h>
#define GNOME_DB_CONFIG_KEY_BROWSER_PANED_POSITION   "/apps/gnome-db/Browser/PanedPosition"
#define GNOME_DB_CONFIG_KEY_EDITOR_HIGHLIGHT         "/apps/gnome-db/Editor/Highlight"
#define GNOME_DB_CONFIG_KEY_EDITOR_SHOW_LINE_NUMBERS "/apps/gnome-db/Editor/ShowLineNumbers"
#define GNOME_DB_CONFIG_KEY_EDITOR_TAB_STOP          "/apps/gnome-db/Editor/TabStop"
#endif

#define PARENT_TYPE GTK_TYPE_VBOX

typedef void (* CreateTagsFunc) (GnomeDbEditor *editor, const gchar *language);

struct _GnomeDbEditorPrivate {
	GtkWidget *scrolled_window;
	GtkWidget *text;
	guint config_lid;
};

static void gnome_db_editor_class_init (GnomeDbEditorClass *klass);
static void gnome_db_editor_init       (GnomeDbEditor *editor, GnomeDbEditorClass *klass);
static void gnome_db_editor_finalize   (GObject *object);

static GObjectClass *parent_class = NULL;
static GHashTable *supported_languages = NULL;
static gint number_of_objects = 0;

/*
 * Private functions
 */
static void
create_tags_for_sql (GnomeDbEditor *editor, const gchar *language)
{
	GtkTextBuffer *buffer;
#ifdef HAVE_GTKSOURCEVIEW
	GtkSourceLanguageManager *mgr;
	GtkSourceLanguage *lang;
#endif

	g_return_if_fail (language != NULL);
	g_return_if_fail (!strcmp (language, GNOME_DB_EDITOR_LANGUAGE_SQL));

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (editor->priv->text));
#ifdef HAVE_GTKSOURCEVIEW
	mgr = gtk_source_language_manager_new ();
	lang = gtk_source_language_manager_get_language (mgr, "sql");

	if (lang) {
		gtk_source_buffer_set_language (GTK_SOURCE_BUFFER (buffer), lang);
		/* FIXME: extend for Gda's variables syntax */
	}

	g_object_unref (mgr);
#endif
}

/*
 * GnomeDbEditor class implementation
 */

static void
gnome_db_editor_class_init (GnomeDbEditorClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gnome_db_editor_finalize;
}

#ifdef HAVE_GCONF
static void
configuration_changed_cb (GConfClient *conf_client, guint cnxn_id, GConfEntry *entry, gpointer user_data)
{
	GnomeDbEditor *editor = (GnomeDbEditor *) user_data;

	g_return_if_fail (GNOME_DB_IS_EDITOR (editor));

	if (!strcmp (entry->key, GNOME_DB_CONFIG_KEY_EDITOR_SHOW_LINE_NUMBERS)) {
#ifdef HAVE_GTKSOURCEVIEW
		gtk_source_view_set_show_line_numbers (
			GTK_SOURCE_VIEW (editor->priv->text),
			gconf_client_get_bool (gconf_client_get_default (), GNOME_DB_CONFIG_KEY_EDITOR_SHOW_LINE_NUMBERS, NULL));
#else
#endif
	} else if (!strcmp (entry->key, GNOME_DB_CONFIG_KEY_EDITOR_TAB_STOP)) {
#ifdef HAVE_GTKSOURCEVIEW
		int tab = gconf_client_get_int (gconf_client_get_default (), GNOME_DB_CONFIG_KEY_EDITOR_TAB_STOP, NULL);
		gtk_source_view_set_tab_width (GTK_SOURCE_VIEW (editor->priv->text),
					       tab ? tab : 8);
#else
#endif
	} else if (!strcmp (entry->key, GNOME_DB_CONFIG_KEY_EDITOR_HIGHLIGHT)) {
		gnome_db_editor_set_highlight_syntax (editor,
					       gconf_client_get_bool (gconf_client_get_default (), 
								      GNOME_DB_CONFIG_KEY_EDITOR_HIGHLIGHT, NULL));
	}
}
#endif

static void
gnome_db_editor_init (GnomeDbEditor *editor, GnomeDbEditorClass *klass)
{
	int tab = 8;
	gboolean highlight = TRUE;
	gboolean showlinesno = FALSE;

	g_return_if_fail (GNOME_DB_IS_EDITOR (editor));

	/* allocate private structure */
	editor->priv = g_new0 (GnomeDbEditorPrivate, 1);

#ifdef HAVE_GCONF
	gconf_client_add_dir (gconf_client_get_default (), "/apps/gnome-db/Editor", 
			      GCONF_CLIENT_PRELOAD_ONELEVEL, NULL);
	
	editor->priv->config_lid = gconf_client_notify_add (gconf_client_get_default (), 
							    "/apps/gnome-db/Editor",
							    (GConfClientNotifyFunc) configuration_changed_cb,
							    editor, NULL, NULL);
	highlight = gconf_client_get_bool (gconf_client_get_default (),
					   GNOME_DB_CONFIG_KEY_EDITOR_HIGHLIGHT, NULL);
	showlinesno = gconf_client_get_bool (gconf_client_get_default (),
					     GNOME_DB_CONFIG_KEY_EDITOR_SHOW_LINE_NUMBERS, NULL);
	tab = gconf_client_get_int (gconf_client_get_default (), GNOME_DB_CONFIG_KEY_EDITOR_TAB_STOP, NULL);
#endif
	/* set up widgets */
	editor->priv->scrolled_window = gnome_db_new_scrolled_window_widget ();
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (editor->priv->scrolled_window),
					GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_box_pack_start (GTK_BOX (editor), editor->priv->scrolled_window, TRUE, TRUE, 2);

#ifdef HAVE_GTKSOURCEVIEW
	editor->priv->text = gtk_source_view_new ();
	gtk_source_buffer_set_highlight_syntax (GTK_SOURCE_BUFFER (gtk_text_view_get_buffer (GTK_TEXT_VIEW (editor->priv->text))),
					 highlight);
	gtk_source_view_set_show_line_numbers (GTK_SOURCE_VIEW (editor->priv->text), showlinesno);
	gtk_source_view_set_tab_width (GTK_SOURCE_VIEW (editor->priv->text), tab);
	gtk_widget_show (editor->priv->text);
#else
	editor->priv->text = gnome_db_new_text_widget (NULL);
#endif

	gtk_container_add (GTK_CONTAINER (editor->priv->scrolled_window), editor->priv->text);

	/* initialize common data */
	number_of_objects++;
	if (!supported_languages) {
		supported_languages = g_hash_table_new (g_str_hash, g_str_equal);

		/* add the built-in languages */
		g_hash_table_insert (supported_languages, GNOME_DB_EDITOR_LANGUAGE_SQL, create_tags_for_sql);
	}

	create_tags_for_sql (editor, GNOME_DB_EDITOR_LANGUAGE_SQL);
}

static void
gnome_db_editor_finalize (GObject *object)
{
	GnomeDbEditor *editor = (GnomeDbEditor *) object;

	g_return_if_fail (GNOME_DB_IS_EDITOR (editor));

	/* free memory */
#ifdef HAVE_GCONF
	gconf_client_notify_remove (gconf_client_get_default (), editor->priv->config_lid);
#endif
	g_free (editor->priv);
	editor->priv = NULL;

	parent_class->finalize (object);

	/* update common data */
	number_of_objects--;
	if (number_of_objects == 0) {
		g_hash_table_destroy (supported_languages);
		supported_languages = NULL;
	}
}

GType
gnome_db_editor_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0)) {
		static const GTypeInfo info = {
			sizeof (GnomeDbEditorClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_editor_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbEditor),
			0,
			(GInstanceInitFunc) gnome_db_editor_init
		};
		type = g_type_register_static (PARENT_TYPE, "GnomeDbEditor", &info, 0);
	}
	return type;
}

/**
 * gnome_db_editor_new
 *
 * Create a new #GnomeDbEditor widget, which is a multiline text widget
 * with support for several languages used to write database stored
 * procedures and functions. If libgnomedb was compiled with gtksourceview
 * in, this widget will support syntax highlighting for all supported
 * languages.
 *
 * Returns: the newly created widget.
 */
GtkWidget *
gnome_db_editor_new (void)
{
	GnomeDbEditor *editor;

	editor = g_object_new (GNOME_DB_TYPE_EDITOR, NULL);

	return GTK_WIDGET (editor);
}

/**
 * gnome_db_editor_get_editable
 * @editor: a #GnomeDbEditor widget.
 *
 * Retrieve the editable status of the given editor widget.
 *
 * Returns: the editable status.
 */
gboolean
gnome_db_editor_get_editable (GnomeDbEditor *editor)
{
        g_return_val_if_fail (GNOME_DB_IS_EDITOR (editor), FALSE);

        return gtk_text_view_get_editable (GTK_TEXT_VIEW (editor->priv->text));
}

/**
 * gnome_db_editor_set_editable
 * @editor: a #GnomeDbEditor widget.
 * @editable: editable state.
 *
 * Set the editable state of the given editor widget.
 */
void
gnome_db_editor_set_editable (GnomeDbEditor *editor, gboolean editable)
{
	g_return_if_fail (GNOME_DB_IS_EDITOR (editor));
	gtk_text_view_set_editable (GTK_TEXT_VIEW (editor->priv->text), editable);
}

/**
 * gnome_db_editor_get_highlight
 * @editor: a #GnomeDbEditor widget.
 *
 * Retrieve the highlighting status of the given editor widget.
 *
 * Returns: the highlighting status.
 */
gboolean
gnome_db_editor_get_highlight (GnomeDbEditor *editor)
{
	g_return_val_if_fail (GNOME_DB_IS_EDITOR (editor), FALSE);

#ifdef HAVE_GTKSOURCEVIEW
	return gtk_source_buffer_get_highlight_syntax (
		GTK_SOURCE_BUFFER (gtk_text_view_get_buffer (GTK_TEXT_VIEW (editor->priv->text))));
#else
	return FALSE;
#endif
}

/**
 * gnome_db_editor_set_highlight
 * @editor: a #GnomeDbEditor widget.
 * @highlight: highlighting status.
 *
 * Set the highlighting status on the given editor widget.
 */
void
gnome_db_editor_set_highlight (GnomeDbEditor *editor, gboolean highlight)
{
	g_return_if_fail (GNOME_DB_IS_EDITOR (editor));

#ifdef HAVE_GTKSOURCEVIEW
	gtk_source_buffer_set_highlight_syntax (
		GTK_SOURCE_BUFFER (gtk_text_view_get_buffer (GTK_TEXT_VIEW (editor->priv->text))), highlight);
#endif
}

/**
 * gnome_db_editor_set_text
 * @editor: a #GnomeDbEditor widget.
 * @text: text to display in the editor.
 * @len: length of @text.
 *
 * Set the contents of the given editor widget.
 */
void
gnome_db_editor_set_text (GnomeDbEditor *editor, const gchar *text, gint len)
{
	g_return_if_fail (GNOME_DB_IS_EDITOR (editor));
	gnome_db_text_set_text (GTK_TEXT_VIEW (editor->priv->text), text, len);
}

/**
 * gnome_db_editor_get_all_text
 * @editor: a #GnomeDbEditor widget.
 *
 * Retrieve the full contents of the given editor widget.
 *
 * Returns: the current contents of the editor buffer. You must free
 * the returned value when no longer needed.
 */
gchar *
gnome_db_editor_get_all_text (GnomeDbEditor *editor)
{
	g_return_val_if_fail (GNOME_DB_IS_EDITOR (editor), NULL);
	return gnome_db_text_get_text (GTK_TEXT_VIEW (editor->priv->text));
}

/**
 * gnome_db_editor_load_from_file
 * @editor: a #GnomeDbEditor widget.
 * @filename: the file to be loaded.
 *
 * Load the given filename into the editor widget.
 *
 * Returns: TRUE if successful, FALSE otherwise.
 */
gboolean
gnome_db_editor_load_from_file (GnomeDbEditor *editor, const gchar *filename)
{
	gboolean retval = TRUE;
	gchar *contents;

	g_return_val_if_fail (GNOME_DB_IS_EDITOR (editor), FALSE);
	g_return_val_if_fail (filename != NULL, FALSE);

	if (g_file_get_contents (filename, &contents, NULL, NULL)) {
		gnome_db_text_set_text (GTK_TEXT_VIEW (editor->priv->text), contents, strlen (contents));
		g_free (contents);
	}
	else 
		retval = FALSE;

	return retval;
}

/**
 * gnome_db_editor_save_to_file
 * @editor: a #GnomeDbEditor widget.
 * @filename: the file to save to.
 *
 * Save the current editor contents to the given file.
 *
 * Returns: TRUE if successful, FALSE otherwise.
 */
gboolean
gnome_db_editor_save_to_file (GnomeDbEditor *editor, const gchar *filename)
{
	gchar *contents;
	gboolean retval = TRUE;

	g_return_val_if_fail (GNOME_DB_IS_EDITOR (editor), FALSE);
	g_return_val_if_fail (filename != NULL, FALSE);

	contents = gnome_db_text_get_text (GTK_TEXT_VIEW (editor->priv->text));
	if (!g_file_set_contents (filename, contents, strlen (contents), NULL))
		retval = FALSE;

	g_free (contents);

	return retval;
}

/**
 * gnome_db_editor_copy_clipboard
 * @editor: a #GnomeDbEditor widget.
 *
 * Copy currently selected text in the given editor widget to the clipboard.
 */
void
gnome_db_editor_copy_clipboard (GnomeDbEditor *editor)
{
	g_return_if_fail (GNOME_DB_IS_EDITOR (editor));
	gnome_db_text_copy_clipboard (GTK_TEXT_VIEW (editor->priv->text));
}

/**
 * gnome_db_editor_cut_clipboard
 * @editor: a #GnomeDbEditor widget.
 *
 * Moves currently selected text in the given editor widget to the clipboard.
 */
void
gnome_db_editor_cut_clipboard (GnomeDbEditor *editor)
{
	g_return_if_fail (GNOME_DB_IS_EDITOR (editor));
	gnome_db_text_cut_clipboard (GTK_TEXT_VIEW (editor->priv->text));
}

/**
 * gnome_db_editor_paste_clipboard
 * @editor: a #GnomeDbEditor widget.
 *
 * Paste clipboard contents into editor widget, at the current position.
 */
void
gnome_db_editor_paste_clipboard (GnomeDbEditor *editor)
{
	g_return_if_fail (GNOME_DB_IS_EDITOR (editor));
	gnome_db_text_paste_clipboard (GTK_TEXT_VIEW (editor->priv->text));
}
