/* gnome-db-sql-console.h
 *
 * Copyright (C) 2005 - 2008 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNOME_DB_SQL_CONSOLE__
#define __GNOME_DB_SQL_CONSOLE__

#include <gtk/gtk.h>
#include <libgda/gda-decl.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_SQL_CONSOLE          (gnome_db_sql_console_get_type())
#define GNOME_DB_SQL_CONSOLE(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, gnome_db_sql_console_get_type(), GnomeDbSqlConsole)
#define GNOME_DB_SQL_CONSOLE_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, gnome_db_sql_console_get_type (), GnomeDbSqlConsoleClass)
#define GNOME_DB_IS_SQL_CONSOLE(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, gnome_db_sql_console_get_type ())


typedef struct _GnomeDbSqlConsole      GnomeDbSqlConsole;
typedef struct _GnomeDbSqlConsoleClass GnomeDbSqlConsoleClass;
typedef struct _GnomeDbSqlConsolePriv  GnomeDbSqlConsolePriv;

/* struct for the object's data */
struct _GnomeDbSqlConsole
{
	GtkTextView            object;

	GnomeDbSqlConsolePriv *priv;
};

/* struct for the object's class */
struct _GnomeDbSqlConsoleClass
{
	GtkTextViewClass       parent_class;

	/* signals */
	gchar                *(*sql_entered) (GnomeDbSqlConsole *console, const gchar *sql);
};

/* 
 * Generic widget's methods 
*/
GType      gnome_db_sql_console_get_type                 (void) G_GNUC_CONST;

GtkWidget *gnome_db_sql_console_new                      (GdaConnection *cnc, const gchar *message);
void       gnome_db_sql_console_clear                    (GnomeDbSqlConsole *console);
void       gnome_db_sql_console_clear_history            (GnomeDbSqlConsole *console);


G_END_DECLS

#endif



