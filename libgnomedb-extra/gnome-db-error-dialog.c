/* GNOME DB library
 * Copyright (C) 1998 - 2007 The GNOME Foundation.
 *
 * AUTHORS:
 *      Michael Lausch <michael@lausch.at>
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gtk/gtkstock.h>
#include <gtk/gtkwindow.h>
#include <libgnomedb-extra/gnome-db-error-dialog.h>
#include <libgnomedb/gnome-db-util.h>
#include <glib/gi18n-lib.h>
#include <libgnomedb/binreloc/gnome-db-binreloc.h>

struct _GnomeDbErrorDialogPrivate {
	GnomeDbError *error_widget;
	gchar *title;
};

static void gnome_db_error_dialog_class_init   (GnomeDbErrorDialogClass *klass);
static void gnome_db_error_dialog_init         (GnomeDbErrorDialog *error,
					        GnomeDbErrorDialogClass *klass);
static void gnome_db_error_dialog_set_property (GObject *object,
						guint paramid,
						const GValue *value,
						GParamSpec *pspec);
static void gnome_db_error_dialog_get_property (GObject *object,
						guint param_id,
						GValue *value,
						GParamSpec *pspec);
static void gnome_db_error_dialog_finalize   (GObject *object);

enum {
	PROP_0,
	PROP_TITLE
};

static GObjectClass *parent_class = NULL;

/*
 * Callbacks
 */

/*
 * GnomeDbErrorDialog class implementation
 */
GType
gnome_db_error_dialog_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0)) {
		static const GTypeInfo info = {
			sizeof (GnomeDbErrorDialogClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_error_dialog_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbErrorDialog),
			0,
			(GInstanceInitFunc) gnome_db_error_dialog_init
		};
		type = g_type_register_static (GTK_TYPE_DIALOG, "GnomeDbErrorDialog", &info, 0);
	}
	return type;
}

static void
gnome_db_error_dialog_class_init (GnomeDbErrorDialogClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->set_property = gnome_db_error_dialog_set_property;
	object_class->get_property = gnome_db_error_dialog_get_property;
	object_class->finalize = gnome_db_error_dialog_finalize;

	/* add class properties */
	g_object_class_install_property (
		object_class, PROP_TITLE,
		g_param_spec_string ("title", NULL, _("Dialog's title"), _("Error"),
				     (G_PARAM_READABLE | G_PARAM_WRITABLE)));
}
      
static void
gnome_db_error_dialog_init (GnomeDbErrorDialog *dialog, GnomeDbErrorDialogClass *klass)
{
	GdkPixbuf *icon;
	gchar *str;

	g_return_if_fail (GNOME_DB_IS_ERROR_DIALOG (dialog));

	/* allocate private structure */
	dialog->priv = g_new (GnomeDbErrorDialogPrivate, 1);
	dialog->priv->title = NULL;

	/* create the error viewer */
	dialog->priv->error_widget = GNOME_DB_ERROR (gnome_db_error_new ());
	gtk_widget_show (GTK_WIDGET (dialog->priv->error_widget));

	gtk_dialog_add_button (GTK_DIALOG (dialog), GTK_STOCK_GO_BACK, GTK_RESPONSE_YES);
	gtk_dialog_add_button (GTK_DIALOG (dialog), GTK_STOCK_GO_FORWARD, GTK_RESPONSE_NO);
	gtk_dialog_add_button (GTK_DIALOG (dialog), GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE);

	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox),
			    GTK_WIDGET (dialog->priv->error_widget),
			    TRUE, TRUE, 0);

	str = gnome_db_gbr_get_icon_path ("gnome-db.png");
	icon = gdk_pixbuf_new_from_file (str, NULL);
	g_free (str);
	if (icon) {
		gtk_window_set_icon (GTK_WINDOW (dialog), icon);
		g_object_unref (icon);
	}
}

static void
gnome_db_error_dialog_set_property (GObject *object,
				    guint param_id,
				    const GValue *value,
				    GParamSpec *pspec)
{
	GnomeDbErrorDialog *dialog = (GnomeDbErrorDialog *) object;

	g_return_if_fail (GNOME_DB_IS_ERROR_DIALOG (dialog));

	switch (param_id) {
	case PROP_TITLE :
		gnome_db_error_dialog_set_title (dialog, g_value_get_string (value));
		break;
	default :
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}

static void
gnome_db_error_dialog_get_property (GObject *object,
				    guint param_id,
				    GValue *value,
				    GParamSpec *pspec)
{
	GnomeDbErrorDialog *dialog = (GnomeDbErrorDialog *) object;

	g_return_if_fail (GNOME_DB_IS_ERROR_DIALOG (dialog));

	switch (param_id) {
	case PROP_TITLE :
		g_value_set_string (value, dialog->priv->title);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}

static void
gnome_db_error_dialog_finalize (GObject *object)
{
	GnomeDbErrorDialog *dialog = (GnomeDbErrorDialog *) object;

	g_return_if_fail (GNOME_DB_IS_ERROR_DIALOG (dialog));

	/* free memory */
	if (dialog->priv->title)
		g_free (dialog->priv->title);
	g_free (dialog->priv);
	dialog->priv = NULL;

	parent_class->finalize (object);
}

/**
 * gnome_db_error_dialog_new
 * @title: title of the dialog box
 *
 * Create a new GnomeDbErrorDialog, which is a subclass of the GnomeDialog
 * widget, just acting as a container for a GnomeDbError widget
 *
 * Returns: a pointer to the new widget, or NULL on error
 */
GtkWidget*
gnome_db_error_dialog_new (gchar *title)
{
	GnomeDbErrorDialog *dialog;

	dialog = g_object_new (GNOME_DB_TYPE_ERROR_DIALOG,
	                       "title", title, NULL);

	return GTK_WIDGET (dialog);
}

static void
change_sensitiveness (GnomeDbErrorDialog *dialog, GnomeDbError *error_widget, gint max)
{
	gint pos;

	pos = gnome_db_error_get_position (error_widget);
	gtk_dialog_set_response_sensitive (GTK_DIALOG (dialog),
					   GTK_RESPONSE_YES,
					   pos > 0);

	gtk_dialog_set_response_sensitive (GTK_DIALOG (dialog),
					   GTK_RESPONSE_NO,
					   pos < max - 1);
}

/**
 * gnome_db_error_dialog_show_errors
 * @dialog: the GnomeDbErrorDialog widget
 * @error_list: list of errors to show
 *
 * Pops up the given GnomeDbErrorDialog widget, displaying any errors that
 * may be reported for the connection being used by this widget
 */
void
gnome_db_error_dialog_show_errors (GnomeDbErrorDialog *dialog,
                                   GList *error_list)
{
	gint ret;
	GnomeDbError *error_widget;
	gint max;

	g_return_if_fail (GNOME_DB_IS_ERROR_DIALOG (dialog));

	max = g_list_length (error_list);
	error_widget = GNOME_DB_ERROR (dialog->priv->error_widget);

	gnome_db_error_show (GNOME_DB_ERROR (error_widget), error_list);
	change_sensitiveness (dialog, error_widget, max);

	while ((ret = gtk_dialog_run (GTK_DIALOG (dialog))) != GTK_RESPONSE_CLOSE){
		switch (ret) {
		case GTK_RESPONSE_YES :
			gnome_db_error_prev (GNOME_DB_ERROR (dialog->priv->error_widget));
			change_sensitiveness (dialog, error_widget, max);
			break;
		case GTK_RESPONSE_NO :
			gnome_db_error_next (GNOME_DB_ERROR (dialog->priv->error_widget));
			change_sensitiveness (dialog, error_widget, max);
			break;
		}
	}
	gtk_widget_destroy (GTK_WIDGET (dialog));
}

/**
 * gnome_db_error_dialog_set_title
 */
const gchar *
gnome_db_error_dialog_get_title (GnomeDbErrorDialog *dialog)
{
	g_return_val_if_fail (GNOME_DB_IS_ERROR_DIALOG (dialog), NULL);
	return (const gchar *) dialog->priv->title;
}

/**
 * gnome_db_error_dialog_set_title
 * @dialog: the GnomeDbErrorDialog widget
 * @title: title to be shown
 *
 * Change the title for the given GnomeDbErrorDialog widget
 */
void
gnome_db_error_dialog_set_title (GnomeDbErrorDialog *dialog, const gchar *title)
{
	g_return_if_fail(GNOME_DB_IS_ERROR_DIALOG(dialog));
	g_return_if_fail(title != 0);

	if (dialog->priv->title != 0)
		g_free((gpointer) dialog->priv->title);
	dialog->priv->title = g_strdup(title);

	gtk_window_set_title (GTK_WINDOW (dialog), title);
}
