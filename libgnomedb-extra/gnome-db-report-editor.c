/* GNOME DB library
 * Copyright (C) 1999-2002 The GNOME Foundation.
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <libgnomedb-extra/gnome-db-report-editor.h>

#define PARENT_TYPE GTK_TYPE_VBOX

struct _GnomeDbReportEditorPrivate {
};

static void gnome_db_report_editor_class_init   (GnomeDbReportEditorClass *klass);
static void gnome_db_report_editor_init         (GnomeDbReportEditor *editor,
						 GnomeDbReportEditorClass *klass);
static void gnome_db_report_editor_set_property (GObject *object,
						 guint paramid,
						 const GValue *value,
						 GParamSpec *pspec);
static void gnome_db_report_editor_get_property (GObject *object,
						 guint param_id,
						 GValue *value,
						 GParamSpec *pspec);
static void gnome_db_report_editor_finalize     (GObject *object);

static GObjectClass *parent_class = NULL;

/*
 * GnomeDbReportEditor class implementation
 */

static void
gnome_db_report_editor_class_init (GnomeDbReportEditorClass *klass)
{
	GObjectClass* object_class = G_OBJECT_CLASS(klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->set_property = gnome_db_report_editor_set_property;
	object_class->get_property = gnome_db_report_editor_get_property;
	object_class->finalize = gnome_db_report_editor_finalize;
}

static void
gnome_db_report_editor_init (GnomeDbReportEditor *editor, GnomeDbReportEditorClass *klass)
{
	g_return_if_fail (GNOME_DB_IS_REPORT_EDITOR (editor));

	/* allocate private structure */
	editor->priv = g_new0 (GnomeDbReportEditorPrivate, 1);
}

static void
gnome_db_report_editor_set_property (GObject *object,
				     guint param_id,
				     const GValue *value,
				     GParamSpec *pspec)
{
	GnomeDbReportEditor *editor = (GnomeDbReportEditor *) object;

	g_return_if_fail (GNOME_DB_IS_REPORT_EDITOR (editor));

	switch (param_id) {
	default :
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}

static void
gnome_db_report_editor_get_property (GObject *object,
				     guint param_id,
				     GValue *value,
				     GParamSpec *pspec)
{
	GnomeDbReportEditor *editor = (GnomeDbReportEditor *) object;

	g_return_if_fail (GNOME_DB_IS_REPORT_EDITOR (editor));

	switch (param_id) {
	default :
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}


static void
gnome_db_report_editor_finalize (GObject *object)
{
	GnomeDbReportEditor *editor = (GnomeDbReportEditor *) object;

	g_return_if_fail (GNOME_DB_IS_REPORT_EDITOR (editor));

	/* free memory */
	g_free (editor->priv);
	editor->priv = NULL;

	/* chain to parent handler */
	parent_class->finalize (object);
}

GType
gnome_db_report_editor_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0)) {
		static const GTypeInfo info = {
			sizeof (GnomeDbReportEditorClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_report_editor_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbReportEditor),
			0,
			(GInstanceInitFunc) gnome_db_report_editor_init
		};
		type = g_type_register_static (PARENT_TYPE, "GnomeDbReportEditor", &info, 0);
	}
	return type;
}

/**
 * gnome_db_report_editor_new
 *
 * Create a new #GnomeDbReportEditor widget, which is a high-level widget
 * that lets users edit a report file, as defined by the GDA report DTD.
 *
 * Returns: the newly created widget.
 */
GtkWidget *
gnome_db_report_editor_new (void)
{
	GnomeDbReportEditor *editor;

	editor = g_object_new (GNOME_DB_TYPE_REPORT_EDITOR, NULL);
	return GTK_WIDGET (editor);
}
