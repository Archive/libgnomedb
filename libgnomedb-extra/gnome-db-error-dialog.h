/* GNOME DB library
 * Copyright (C) 1998 - 2007 The GNOME Foundation.
 *
 * AUTHORS:
 *	Michael Lausch <michael@lausch.at>
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GNOME_DB_ERROR_DIALOG_H__
#define __GNOME_DB_ERROR_DIALOG_H__ 

#include <gtk/gtkdialog.h>
#include <libgnomedb-extra/gnome-db-error.h>


G_BEGIN_DECLS

#define GNOME_DB_TYPE_ERROR_DIALOG            (gnome_db_error_dialog_get_type())
#define GNOME_DB_ERROR_DIALOG(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GNOME_DB_TYPE_ERROR_DIALOG, GnomeDbErrorDialog))
#define GNOME_DB_ERROR_DIALOG_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GNOME_DB_TYPE_ERROR_DIALOG, GnomeDbErrorDialogClass))
#define GNOME_DB_IS_ERROR_DIALOG(obj)         (G_TYPE_CHECK_INSTANCE_TYPE (obj, GNOME_DB_TYPE_ERROR_DIALOG))
#define GNOME_DB_IS_ERROR_DIALOG_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GNOME_DB_TYPE_ERROR_DIALOG))

typedef struct _GnomeDbErrorDialog        GnomeDbErrorDialog;
typedef struct _GnomeDbErrorDialogClass   GnomeDbErrorDialogClass;
typedef struct _GnomeDbErrorDialogPrivate GnomeDbErrorDialogPrivate;

struct _GnomeDbErrorDialog {
	GtkDialog dialog;
	GnomeDbErrorDialogPrivate *priv;
};

struct _GnomeDbErrorDialogClass {
	GtkDialogClass parent_class;
};

GType        gnome_db_error_dialog_get_type       (void) G_GNUC_CONST;

GtkWidget   *gnome_db_error_dialog_new            (gchar *title);
void         gnome_db_error_dialog_show_errors    (GnomeDbErrorDialog *dialog,
						   GList *error_list);

const gchar *gnome_db_error_dialog_get_title      (GnomeDbErrorDialog *dialog);
void         gnome_db_error_dialog_set_title      (GnomeDbErrorDialog *dialog,
						   const gchar *title);

G_END_DECLS

#endif
