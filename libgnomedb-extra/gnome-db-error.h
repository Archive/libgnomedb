/* GNOME DB library
 * Copyright (C) 1998 - 2007 The GNOME Foundation.
 *
 * AUTHORS:
 *	Michael Lausch <michael@lausch.at>
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GNOME_DB_ERROR_H__
#define __GNOME_DB_ERROR_H__

#include <gtk/gtkvbox.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_ERROR            (gnome_db_error_get_type())
#define GNOME_DB_ERROR(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GNOME_DB_TYPE_ERROR, GnomeDbError))
#define GNOME_DB_ERROR_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GNOME_DB_TYPE_ERROR, GnomeDbErrorClass))
#define GNOME_DB_IS_ERROR(obj)         (G_TYPE_CHECK_INSTANCE_TYPE (obj, GNOME_DB_TYPE_ERROR))
#define GNOME_DB_IS_ERROR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GNOME_DB_TYPE_ERROR))

typedef struct _GnomeDbError        GnomeDbError;
typedef struct _GnomeDbErrorClass   GnomeDbErrorClass;
typedef struct _GnomeDbErrorPrivate GnomeDbErrorPrivate;

struct _GnomeDbError {
	GtkVBox              box;
	GnomeDbErrorPrivate* priv;
};

struct _GnomeDbErrorClass {
	GtkVBoxClass parent_class;
};

GType          gnome_db_error_get_type       (void) G_GNUC_CONST;

GtkWidget*     gnome_db_error_new            (void);
void           gnome_db_error_clear          (GnomeDbError *error_widget);
void           gnome_db_error_show           (GnomeDbError *error_widget,
                                              GList *errors);
void           gnome_db_error_prev           (GnomeDbError *error_widget);
void           gnome_db_error_next           (GnomeDbError *error_widget);
gint	       gnome_db_error_get_position   (GnomeDbError *error_widget);
							 
G_END_DECLS

#endif
