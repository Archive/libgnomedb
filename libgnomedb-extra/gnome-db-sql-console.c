/* gnome-db-sql-console.c
 *
 * Copyright (C) 2005 - 2008 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <string.h>
#include "gnome-db-sql-console.h"
#include <gdk/gdkkeysyms.h>
#include <libgda/libgda.h>
#include <sql-parser/gda-sql-parser.h>
#include <glib/gi18n-lib.h>
#include "gnome-db-basic-form.h"
#include "extra_marshal.h"

static void gnome_db_sql_console_class_init (GnomeDbSqlConsoleClass * class);
static void gnome_db_sql_console_init (GnomeDbSqlConsole * wid);
static void gnome_db_sql_console_dispose (GObject   * object);

static void gnome_db_sql_console_set_property (GObject *object,
                                               guint param_id,
                                               const GValue *value,
                                               GParamSpec *pspec);
static void gnome_db_sql_console_get_property (GObject *object,
                                               guint param_id,
                                               GValue *value,
                                               GParamSpec *pspec);

enum {
	PROP_0,
	PROP_CNC,
	PROP_MESSAGE
};

enum
{
        SQL_ENTERED,
        LAST_SIGNAL
};

static gint gnome_db_sql_console_signals[LAST_SIGNAL] = { 0 };

typedef struct {
	gchar       *cmde;
	gchar       *sql;

	GtkTextMark *begin;
	GtkTextMark *end;

	GtkTextMark *sql_begin;
	GtkTextMark *sql_end;

} HistItem;

struct _GnomeDbSqlConsolePriv
{
	GdaConnection *cnc;
	GdaSqlParser  *parser;
	GtkTextBuffer *buffer;

	GList         *hist_items; /* list of HistItem elements */
	HistItem      *current; /* not NULL if command in preparation is among history */
};

static void hist_item_free (HistItem *item);


/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *parent_class = NULL;

GType
gnome_db_sql_console_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0)) {
		static const GTypeInfo info = {
			sizeof (GnomeDbSqlConsoleClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_sql_console_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbSqlConsole),
			0,
			(GInstanceInitFunc) gnome_db_sql_console_init
		};		
		
		type = g_type_register_static (GTK_TYPE_TEXT_VIEW, "GnomeDbSqlConsole", &info, 0);
	}

	return type;
}

static gboolean
sql_entered_accumulator (GSignalInvocationHint *ihint,
			 GValue *return_accu,
			 const GValue *handler_return,
			 gpointer data)
{
	if (g_value_get_string (handler_return)) {
		if (g_value_get_string (return_accu)) {
			gchar *str;

			str = g_strdup_printf ("%s%s\n", g_value_get_string (return_accu),
					       g_value_get_string (handler_return));
			g_value_take_string (return_accu, str);
		}
		else {
			gchar *str;

			str = g_strdup_printf ("%s\n", g_value_get_string (handler_return));
			g_value_take_string (return_accu, str);
		}
	}

	return TRUE; /* don't stop signal emission */
}

static gchar *
m_sql_entered_cb (GnomeDbSqlConsole *console, const gchar *sql)
{
	/* do nothing */
	return NULL;
}

static void
gnome_db_sql_console_class_init (GnomeDbSqlConsoleClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);
	
	parent_class = g_type_class_peek_parent (class);

	object_class->dispose = gnome_db_sql_console_dispose;
	object_class->set_property = gnome_db_sql_console_set_property;
	object_class->get_property = gnome_db_sql_console_get_property;

	g_object_class_install_property (object_class, PROP_CNC,
	                                 g_param_spec_object ("cnc", NULL, NULL,
							      GDA_TYPE_CONNECTION, G_PARAM_READWRITE));

	g_object_class_install_property (object_class, PROP_MESSAGE,
	                                 g_param_spec_string("message", NULL, NULL, NULL,
	                                                     G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY));

	/* signals */
	gnome_db_sql_console_signals[SQL_ENTERED] =
                g_signal_new ("sql_entered",
                              G_TYPE_FROM_CLASS (object_class),
                              G_SIGNAL_RUN_LAST,
                              G_STRUCT_OFFSET (GnomeDbSqlConsoleClass, sql_entered),
                              sql_entered_accumulator, NULL,
                              extra_marshal_STRING__STRING, G_TYPE_STRING, 1,
                              G_TYPE_STRING);

        class->sql_entered = m_sql_entered_cb;
}

static void gnome_db_sql_console_initialize (GnomeDbSqlConsole *console);
static void add_prompt (GnomeDbSqlConsole *console);

static void
gnome_db_sql_console_init (GnomeDbSqlConsole * wid)
{
	wid->priv = g_new0 (GnomeDbSqlConsolePriv, 1);
	wid->priv->cnc = NULL;
	wid->priv->parser = NULL;

	gnome_db_sql_console_initialize (wid);
}

/**
 * gnome_db_sql_console_new
 * @cnc: a #GdaConnection connection, or %NULL
 *
 * Creates a new #GnomeDbSqlConsole widget 
 *
 * Returns: the new widget
 */
GtkWidget *
gnome_db_sql_console_new (GdaConnection *cnc, const gchar *message)
{
	g_return_val_if_fail (!cnc || GDA_IS_CONNECTION (cnc), NULL);

	return (GtkWidget*) g_object_new (GNOME_DB_TYPE_SQL_CONSOLE, "cnc", cnc, "message", message, NULL);
}


static void
gnome_db_sql_console_dispose (GObject *object)
{
	GnomeDbSqlConsole *console;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOME_DB_IS_SQL_CONSOLE (object));
	console = GNOME_DB_SQL_CONSOLE (object);

	if (console->priv) {
		/* history */
		gnome_db_sql_console_clear_history (console);
		
		if (console->priv->cnc) {
			g_object_unref (console->priv->cnc);
			console->priv->cnc = NULL;
		}
		if (console->priv->parser) {
			g_object_unref (console->priv->parser);
			console->priv->parser = NULL;
		}
		
		/* the private area itself */
		g_free (console->priv);
		console->priv = NULL;
	}

	/* for the parent class */
	parent_class->dispose (object);
}

static void
gnome_db_sql_console_set_property (GObject *object,
				   guint param_id,
				   const GValue *value,
				   GParamSpec *pspec)
{
	GnomeDbSqlConsole *console;
	GtkTextIter iter;

	console = GNOME_DB_SQL_CONSOLE(object);

	switch(param_id) {
	case PROP_CNC:
		if (console->priv->cnc) 
			g_object_unref (console->priv->cnc);
		if (console->priv->parser) {
			g_object_unref (console->priv->parser);
			console->priv->parser = NULL;
		}
		console->priv->cnc = GDA_CONNECTION (g_value_get_object (value));
		if (console->priv->cnc) 
			g_object_ref (console->priv->cnc);
		break;
	case PROP_MESSAGE:
		/* initial text */
		gtk_text_buffer_get_iter_at_offset (console->priv->buffer,
		                                    &iter, 0);
		gtk_text_buffer_insert_with_tags_by_name (console->priv->buffer,
		                                          &iter,
		                                          g_value_get_string (value),
		                                          -1, "header",
		                                          NULL);
		add_prompt (console);
	}
}

static void gnome_db_sql_console_get_property (GObject *object,
                                               guint param_id,
                                               GValue *value,
                                               GParamSpec *pspec)
{
	GnomeDbSqlConsole *console;

	console = GNOME_DB_SQL_CONSOLE(object);

	switch(param_id) {
	case PROP_CNC:
		g_value_set_object (value, G_OBJECT (console->priv->cnc));
		break;
	}
}

static gboolean view_event_cb (GtkWidget *widget, GdkEvent *event, GnomeDbSqlConsole *console);
static void buffer_apply_tag_cb (GtkTextBuffer *textbuffer, GtkTextTag *arg1, GtkTextIter *arg2, GtkTextIter *arg3,
				 GnomeDbSqlConsole *console);
static void buffer_insert_text_cb (GtkTextBuffer *textbuffer, GtkTextIter *arg1, gchar *arg2, gint arg3,
				   GnomeDbSqlConsole *console);
static void buffer_delete_range_cb (GtkTextBuffer *textbuffer, GtkTextIter *arg1, GtkTextIter *arg2,
				    GnomeDbSqlConsole *console);

/*
 * create the entries in the widget
 */
static void 
gnome_db_sql_console_initialize (GnomeDbSqlConsole *console)
{
	GtkTextBuffer *buffer;
	PangoFontDescription *font_desc;

	/* widget init */
	gtk_text_view_set_wrap_mode (GTK_TEXT_VIEW (console), GTK_WRAP_NONE);
	gtk_text_view_set_editable (GTK_TEXT_VIEW (console), TRUE);
	gtk_text_view_set_cursor_visible (GTK_TEXT_VIEW (console), TRUE);

	font_desc = pango_font_description_from_string ("Courier 11");
	gtk_widget_modify_font (GTK_WIDGET (console), font_desc);
	pango_font_description_free (font_desc);

	buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (console));
	console->priv->buffer = buffer;

	g_signal_connect (G_OBJECT (console), "event",
			  G_CALLBACK (view_event_cb), console);
	g_signal_connect (G_OBJECT (buffer), "apply-tag",
			  G_CALLBACK (buffer_apply_tag_cb), console);
	g_signal_connect (G_OBJECT (buffer), "insert-text",
			  G_CALLBACK (buffer_insert_text_cb), console);
	g_signal_connect (G_OBJECT (buffer), "delete_range",
			  G_CALLBACK (buffer_delete_range_cb), console);

	/* tags init */
	gtk_text_buffer_create_tag (buffer, "header",
				    "foreground", "blue",
				    "family", "sans",
				    NULL);

	gtk_text_buffer_create_tag (buffer, "prompt",
				    "justification", GTK_JUSTIFY_CENTER,
				    "family", "sans",
				    "foreground", "green",
				    NULL);

	gtk_text_buffer_create_tag (buffer, "ro-cmde",
				    "foreground", "blue",
				    NULL);

	gtk_text_buffer_create_tag (buffer, "result-ok",
				    "left-margin", 20,
				    "scale", PANGO_SCALE_SMALL,
				    NULL);

	gtk_text_buffer_create_tag (buffer, "result-error",
				    "left-margin", 20,
				    "foreground", "#ff6d6d",
				    NULL);

	gtk_text_buffer_create_tag (buffer, "params_dump",
				    "left-margin", 20,
				    "foreground", "lightblue",
				    NULL);
}

/* Creates the prompt and sets text marks */
static void
add_prompt (GnomeDbSqlConsole *console)
{
	GtkTextIter iter;
	GtkTextMark *mark;

	gtk_text_buffer_get_end_iter (console->priv->buffer, &iter);
	gtk_text_buffer_create_mark (console->priv->buffer, "sql_prompt", &iter, TRUE);	

	/* gtk_text_buffer_insert_with_tags_by_name (console->priv->buffer, &iter,  */
/* 						  "-----------------------------------------------\n", -1, "prompt", NULL); */
	
	mark = gtk_text_buffer_create_mark (console->priv->buffer, "sql_start", &iter, TRUE);	
	gtk_text_view_scroll_mark_onscreen (GTK_TEXT_VIEW (console), mark);

}

static void run_sql_cmde (GnomeDbSqlConsole *console, HistItem *hist_item, const gchar *sql);
static void run_internal_cmde (GnomeDbSqlConsole *console, HistItem *hist_item, const gchar *cmde);

static gboolean
view_event_cb (GtkWidget *widget, GdkEvent *event, GnomeDbSqlConsole *console)
{
	gboolean done = FALSE;

	if (event->type == GDK_KEY_PRESS) {
		GdkEventKey *keyevent = (GdkEventKey *) event;

		/* ENTER pressed */
		if (keyevent->keyval == GDK_Return) {
			GtkTextMark *mark;
			GtkTextIter start_iter, iter;
			gchar *sql;
			gboolean cmde_complete;

			mark = gtk_text_buffer_get_mark (console->priv->buffer, "sql_start");
			gtk_text_buffer_get_iter_at_mark (console->priv->buffer, &start_iter, mark);

			gtk_text_buffer_get_end_iter (console->priv->buffer, &iter);
			
			sql = gtk_text_buffer_get_text (console->priv->buffer, &start_iter, &iter, FALSE);
			sql = g_strstrip (sql);
			cmde_complete = ((sql [strlen(sql)-1] == ';') || (*sql == '\\')) ? TRUE : FALSE;

			if (cmde_complete) {
				HistItem *hist_item;

				/* history */
				console->priv->current = NULL;
				hist_item = g_new0 (HistItem, 1);
				hist_item->sql_begin = gtk_text_buffer_create_mark (console->priv->buffer, NULL, &start_iter, TRUE);
				hist_item->sql_end = gtk_text_buffer_create_mark (console->priv->buffer, NULL, &iter, TRUE);
				console->priv->hist_items = g_list_append (console->priv->hist_items, hist_item);
				
				/* place cursor at the end */
				gtk_text_buffer_place_cursor (console->priv->buffer, &iter);
				
				/* delete "sql_start" and "sql_prompt" marks */
				gtk_text_buffer_delete_mark (console->priv->buffer, mark); /* sql_start */
				mark = gtk_text_buffer_get_mark (console->priv->buffer, "sql_prompt");
				gtk_text_buffer_get_iter_at_mark (console->priv->buffer, &iter, mark);
				hist_item->begin = gtk_text_buffer_create_mark (console->priv->buffer, NULL, &iter, TRUE);
				gtk_text_buffer_delete_mark (console->priv->buffer, mark);

				/* Identify the command */
				gtk_text_buffer_get_end_iter (console->priv->buffer, &iter);
				gtk_text_buffer_apply_tag_by_name (console->priv->buffer, "ro-cmde", &start_iter, &iter);

				/* run command */
				hist_item->cmde = sql;
				if ((*sql == '\\'))
					run_internal_cmde (console, hist_item, sql);
				else
					run_sql_cmde (console, hist_item, sql);
				sql = NULL;

				/* prepare a new prompt */
				gtk_text_buffer_get_end_iter (console->priv->buffer, &iter);
				hist_item->end = gtk_text_buffer_create_mark (console->priv->buffer, NULL, &iter, TRUE);				
				add_prompt (console);
				done = TRUE;
			}

			if (sql)
				g_free (sql);
		}

		/* CTRL + Up pressed */
		if ((keyevent->keyval == GDK_Up) && (keyevent->state & GDK_CONTROL_MASK)) {
			/* previous command */
			HistItem *hist_item = NULL;
			if (console->priv->current) {
				GList *list = g_list_find (console->priv->hist_items, console->priv->current);
				g_assert (list);
				list = g_list_previous (list);
				if (list)
					hist_item = (HistItem *) (list->data);
			}
			else {
				if (console->priv->hist_items)
					hist_item = (HistItem *) (g_list_last (console->priv->hist_items)->data);
			}
			if (hist_item) {
				GtkTextIter start_iter, iter;
				GtkTextMark *mark;

				mark = gtk_text_buffer_get_mark (console->priv->buffer, "sql_start");
				gtk_text_buffer_get_iter_at_mark (console->priv->buffer, &start_iter, mark);
				gtk_text_buffer_get_end_iter (console->priv->buffer, &iter);
				gtk_text_buffer_delete (console->priv->buffer, &start_iter, &iter);
				gtk_text_buffer_get_end_iter (console->priv->buffer, &iter);
				gtk_text_buffer_insert (console->priv->buffer, &iter, hist_item->cmde, -1);
				console->priv->current = hist_item;
			}
			done = TRUE;
		}

		/* CTRL + Down pressed */
		if ((keyevent->keyval == GDK_Down) && (keyevent->state & GDK_CONTROL_MASK)) {
			/* next command */
			HistItem *hist_item = NULL;
			if (console->priv->current) {
				GtkTextIter start_iter, iter;
				GtkTextMark *mark;
				GList *list;

				list = g_list_find (console->priv->hist_items, console->priv->current);
				g_assert (list);
				list = g_list_next (list);
				if (list)
					hist_item = (HistItem *) (list->data);
				
				mark = gtk_text_buffer_get_mark (console->priv->buffer, "sql_start");
				gtk_text_buffer_get_iter_at_mark (console->priv->buffer, &start_iter, mark);
				gtk_text_buffer_get_end_iter (console->priv->buffer, &iter);
				gtk_text_buffer_delete (console->priv->buffer, &start_iter, &iter);
				console->priv->current = NULL;

				if (hist_item) {
					gtk_text_buffer_get_end_iter (console->priv->buffer, &iter);
					gtk_text_buffer_insert (console->priv->buffer, &iter, hist_item->cmde, -1);
					console->priv->current = hist_item;
				}
			}
			done = TRUE;
		}

		/* CTRL - l */
		if (((keyevent->keyval == GDK_l) || (keyevent->keyval == GDK_L)) && (keyevent->state & GDK_CONTROL_MASK)) {
			gnome_db_sql_console_clear (console);
			done = TRUE;
		}
	}

	return done;
}

static gint
fill_context_in_dialog (GnomeDbSqlConsole *console, GdaSet *context)
{
	GtkWidget *dlg;
	GnomeDbBasicForm *sform;
	GtkWidget *parent_window;
	gint result;

	if (!context || gda_set_is_valid (context, NULL)) 
		return GTK_RESPONSE_ACCEPT;
	
	parent_window = gtk_widget_get_toplevel (GTK_WIDGET (console));
	if (! GTK_WIDGET_TOPLEVEL (parent_window))
		parent_window = NULL;

	dlg = gnome_db_basic_form_new_in_dialog (context, GTK_WINDOW (parent_window),
						 _("Values to be filled"), 
						 _("<big><b>Required values:</b></big>\n"
						   "<small>The following values are required to "
						   "execute the query.</small>"));
	sform = g_object_get_data (G_OBJECT (dlg), "form");
	gnome_db_basic_form_set_entries_auto_default (sform, TRUE);
	
	gtk_widget_show (dlg);
	result = gtk_dialog_run (GTK_DIALOG (dlg));
	gtk_widget_destroy (dlg);

	return result;
}

static gchar *compute_events_as_string (GdaConnection *cnc);

static void
run_sql_cmde (GnomeDbSqlConsole *console, HistItem *hist_item, const gchar *sql)
{
	GdaConnection *cnc;
	GtkTextIter iter;
	GdaBatch *batch;
	GError *error = NULL;
	gchar *sigret = NULL;
	const gchar *remain;
	const GSList *stmt_list;

#ifdef debug_signal
	g_print (">> 'SQL_ENTERED' from %s\n", __FUNCTION__);
#endif
	g_signal_emit (G_OBJECT (console), gnome_db_sql_console_signals[SQL_ENTERED], 0, sql, &sigret);
#ifdef debug_signal
	g_print ("<< 'SQL_ENTERED' from %s\n", __FUNCTION__);
#endif

	/* output the return from signals with a '\n' at the end */
	gtk_text_buffer_get_end_iter (console->priv->buffer, &iter);
	gtk_text_buffer_insert_with_tags_by_name (console->priv->buffer, &iter, "\n", 1, "result-ok", NULL);
	if (sigret) {
		gtk_text_buffer_insert_with_tags_by_name (console->priv->buffer, &iter, sigret, -1, "result-ok", NULL);
		g_free (sigret);
	}

	if (!console->priv->cnc) {
		gtk_text_buffer_insert_with_tags_by_name (console->priv->buffer, &iter, "\n", 1, "result-ok", NULL);
		return;
	}

	if (!gda_connection_is_opened (console->priv->cnc)) {
		gtk_text_buffer_insert_with_tags_by_name (console->priv->buffer, &iter, "Connection is closed!\n", 
							  1, "result-error", NULL);
		return;
	}
	
	/* run query */
	if (!console->priv->parser) {
		console->priv->parser = gda_connection_create_parser (console->priv->cnc);
		if (!console->priv->parser)
			console->priv->parser = gda_sql_parser_new ();
	}
	
	batch = gda_sql_parser_parse_string_as_batch (console->priv->parser, sql, &remain, NULL);
	if (!batch) {
		gtk_text_buffer_insert_with_tags_by_name (console->priv->buffer, &iter, "Syntax error in SQL!\n", 
							  1, "result-error", NULL);
		return;
	}
	else if (remain) {
		gtk_text_buffer_insert_with_tags_by_name (console->priv->buffer, &iter, "Syntax error in SQL!\n", 
							  1, "result-error", NULL);
		g_object_unref (batch);
		return;
	}

	/* FIXME: compute a unique GdaSet for all the statements in @batch */
	hist_item->sql = g_strdup (sql);
	for (stmt_list = gda_batch_get_statements (batch); stmt_list; stmt_list = stmt_list->next) {
		GdaStatement *stmt = GDA_STATEMENT (stmt_list->data);
		GdaSet *params;
		if (gda_statement_is_useless (stmt))
			continue;
		if (!gda_statement_get_parameters (stmt, &params, NULL)) {
			gtk_text_buffer_insert_with_tags_by_name (console->priv->buffer, &iter, "Syntax error in SQL!\n", 
								  1, "result-error", NULL);
			g_object_unref (batch);
			return;
		}

		if (fill_context_in_dialog (console, params) != GTK_RESPONSE_ACCEPT) {
			gtk_text_buffer_insert_with_tags_by_name (console->priv->buffer, &iter, _("Cancelled"), -1, 
								  "result-error", NULL);
			gtk_text_buffer_insert_with_tags_by_name (console->priv->buffer, &iter, "\n\n", 2, 
								  "result-error", NULL);
		}
		else {
			if (params && params->holders) {
				/* dump param's values */
				GString *string;
				GSList *list;
				
				string = g_string_new ("");
				for (list = params->holders; list; list = list->next) {
					gchar *str;
					const GValue *value;
					value = gda_holder_get_value (GDA_HOLDER (list->data));
					str = gda_value_stringify ((GValue *) value);
					g_string_append_printf (string, "%s => %s\n", 
								(gchar *) g_object_get_data (G_OBJECT (list->data), "name"),
								str);
					g_free (str);
				}
				gtk_text_buffer_insert_with_tags_by_name (console->priv->buffer, &iter, string->str, -1, 
									  "params_dump", NULL);
				g_string_free (string, TRUE);
			}

			/* run query */
			GObject *res;
			res = gda_connection_statement_execute (console->priv->cnc, stmt, params, 
								GDA_STATEMENT_MODEL_RANDOM_ACCESS, NULL, &error);
			if (res) {
				GString *string;
				string = g_string_new ("");

				if (GDA_IS_DATA_MODEL (res)) {
					gchar *str1;
					
					str1 = gda_data_model_dump_as_string ((GdaDataModel*) res);
					g_string_append (string, str1);
					g_free (str1);
				}
				else if (GDA_IS_SET (res)) {
					GdaHolder *ir;
					ir = gda_set_get_holder (GDA_SET (res), "IMPACTED_ROWS");
					if (ir) {
						const GValue *value;
						gint rows;
						value = gda_holder_get_value (ir);
						rows = g_value_get_int (value);
						g_string_append_printf (string, 
									ngettext ("%d row impacted\n",
										  "%d rows impacted\n", rows), rows);
					} 
				}
				gtk_text_buffer_insert_with_tags_by_name (console->priv->buffer, 
									  &iter, 
									  "\n", 1, "result-ok", NULL);
				gtk_text_buffer_insert_with_tags_by_name (console->priv->buffer, &iter,
									  string->str, -1,"result-ok", NULL);
				gtk_text_buffer_insert_with_tags_by_name (console->priv->buffer, &iter, 
									  "\n\n", 2, "result-ok", NULL);
				g_string_free (string, TRUE);
				g_object_unref (res);
			}
			else {
				gchar *str = NULL;
				if (error) {
					if (error->message)
						str = g_strdup (error->message);
					else
						str = g_strdup (_("Error: no details"));
					g_error_free (error);
				}
				if (!str) {
					str = compute_events_as_string (cnc);
					if (!str)
						str = g_strdup (_("Ok."));
				}
				gtk_text_buffer_insert_with_tags_by_name (console->priv->buffer, &iter, "\n", 1, 
									  "result-error", NULL);
				gtk_text_buffer_insert_with_tags_by_name (console->priv->buffer, &iter, str, -1, 
									  "result-error", NULL);
				gtk_text_buffer_insert_with_tags_by_name (console->priv->buffer, &iter, "\n\n", 2, 
									  "result-error", NULL);
				g_free (str);
				break; /* stop statements execution */
			}
		}
	}
	g_object_unref (batch);
}

static gchar *
compute_events_as_string (GdaConnection *cnc)
{
	GList *events;
	gchar *str = NULL;
	
	events = (GList *) gda_connection_get_events (cnc);
	if (events) {
		GString *string = NULL;
		GdaConnectionEvent *event;
		
		while (events) {
			event = GDA_CONNECTION_EVENT (events->data);
			if (!string)
				string = g_string_new ("");
			else
				g_string_append_c (string, '\n');
			switch (gda_connection_event_get_event_type (event)) {
			case GDA_CONNECTION_EVENT_NOTICE:
				g_string_append (string,
						 gda_connection_event_get_description (event));
				break;
			case GDA_CONNECTION_EVENT_WARNING:
				g_string_append (string, _("WARNING: "));
				g_string_append (string,
						 gda_connection_event_get_description (event));
				break;
			case GDA_CONNECTION_EVENT_ERROR:
				g_string_append (string, _("ERROR: "));
				g_string_append (string,
						 gda_connection_event_get_description (event));
				break;
			default:
				g_warning ("Unknown GdaConnectionEvent type");
				break;
			}
			
			events = g_list_next (events);
		}
		str = string->str;
		g_string_free (string, FALSE);
	}
	
	return str;
}

static void
run_internal_cmde (GnomeDbSqlConsole *console, HistItem *hist_item, const gchar *cmde)
{
	GtkTextIter iter;
	const gchar *pcmde; /* not allocated! */
	gboolean cmde_ok = FALSE;
	gchar *str = NULL;

	gtk_text_buffer_get_end_iter (console->priv->buffer, &iter);		
	
	g_assert (*cmde == '\\');
	pcmde = cmde+1;

	/* help */
	if (!cmde_ok && !strcmp (pcmde, "?")) {
		cmde_ok = TRUE;
		GString *string;

		string = g_string_new (_("SQL statements:\n"
					 "  they can span several lines, and are executed\n"
					 "  when a semi-colon (;) finishes the statement and the ENTER key is pressed.\n"));
		g_string_append (string, _("Console commands:\n"
					   "  they start with a '\\' and are interpreted as soon\n"
					   "  as the ENTER key is pressed. Commands are:\n"
					   "  \\? to print this help\n"
					   "  \\h to list commands history\n"
					   "  \\c or CTRL-L to clear buffer\n"
					   "  \\ch to clear commands history\n"));
		g_string_append (string, _("Use CTRL-Up and CTRL-Down to browse through history\n"));
		str = string->str;
		g_string_free (string, FALSE);
	}

	/* history */
	if (!cmde_ok && !strcmp (pcmde, "h")) {
		cmde_ok = TRUE;
		if (g_list_length (console->priv->hist_items) > 1) {
			GList *list;
			GString *string;
			gint i;
			
			string = g_string_new (_("History:\n"));
			list = console->priv->hist_items;
			i = 0;
			while (list && g_list_next (list)) {
				g_string_append_printf (string, "% 3d => %s\n", i, ((HistItem *)(list->data))->cmde);
				
				i++;
				list = g_list_next (list);
			}
			str = string->str;
			g_string_free (string, FALSE);
		}
		else 
			str = g_strdup_printf ("Empty history!\n");
	}

	/* history clear */
	if (!cmde_ok && !strcmp (pcmde, "ch")) {
		cmde_ok = TRUE;
		gnome_db_sql_console_clear_history (console);
		str = g_strdup (_("History cleared.\n"));
	}

	if (!cmde_ok && !strcmp (pcmde, "c")) {
		cmde_ok = TRUE;
		gnome_db_sql_console_clear (console);
	}

	if (!cmde_ok) 
		str = g_strdup (_("Unknown console command!"));

	/* actually print something */
	if (str) {
		gtk_text_buffer_insert_with_tags_by_name (console->priv->buffer, &iter, "\n", 1, cmde_ok ? "result-ok" : "result-error", NULL);
		gtk_text_buffer_insert_with_tags_by_name (console->priv->buffer, &iter, str, -1, cmde_ok ? "result-ok" : "result-error", NULL);	
		gtk_text_buffer_insert_with_tags_by_name (console->priv->buffer, &iter, "\n\n", 2, cmde_ok ? "result-ok" : "result-error", NULL);
	}

	g_free (str);
}

/* the aim of this callback is to remove any tag when pasting text */
static void
buffer_apply_tag_cb (GtkTextBuffer *textbuffer, GtkTextTag *arg1, GtkTextIter *arg2, GtkTextIter *arg3,
		     GnomeDbSqlConsole *console)
{
	GtkTextMark *mark;

	mark = gtk_text_buffer_get_mark (console->priv->buffer, "sql_start");
	if (mark)
		*arg3 = *arg2;
}

/* make sure text is inserted AFTER the "sql_start" mark if it exists */
static void 
buffer_insert_text_cb (GtkTextBuffer *textbuffer, GtkTextIter *arg1, gchar *arg2, gint arg3,
		       GnomeDbSqlConsole *console)
{
	GtkTextMark *mark;

	mark = gtk_text_buffer_get_mark (console->priv->buffer, "sql_start");
	if (mark) {
		GtkTextIter start;
		gtk_text_buffer_get_iter_at_mark (console->priv->buffer, &start, mark);

		if (gtk_text_iter_compare (arg1, &start) < 0) {
			gtk_text_buffer_get_end_iter (console->priv->buffer, arg1);
			gtk_text_buffer_place_cursor (console->priv->buffer, arg1);
			gtk_text_view_place_cursor_onscreen (GTK_TEXT_VIEW (console));
		}
	}
}

/* make sure we don't delete any text before the "sql_start" mark if it exists */
static void
buffer_delete_range_cb (GtkTextBuffer *textbuffer, GtkTextIter *arg1, GtkTextIter *arg2,
			GnomeDbSqlConsole *console)
{
	GtkTextMark *mark;
	
	mark = gtk_text_buffer_get_mark (console->priv->buffer, "sql_start");
	if (mark) {
		GtkTextIter start;
		GtkTextIter *left, *right;

		gtk_text_buffer_get_iter_at_mark (console->priv->buffer, &start, mark);
		if (gtk_text_iter_compare (arg1, arg2) <= 0) {
			left = arg1;
			right = arg2;
		}
		else {
			left = arg2;
			right = arg1;
		}
		
		if (gtk_text_iter_compare (left, &start) < 0)
			*left = start;
		if (gtk_text_iter_compare (right, &start) < 0)
			*right = start;
	}
}

/**
 * gnome_db_sql_console_clear
 * @console: a #GnomeDbSqlConsole widget
 *
 * Clears the contents of the @console widget
 */
void
gnome_db_sql_console_clear (GnomeDbSqlConsole *console)
{
	GtkTextMark *mark;
	GtkTextIter start, end;
	GList *list;

	g_return_if_fail (console && GNOME_DB_IS_SQL_CONSOLE (console));
	g_return_if_fail (console->priv);
	
	mark = gtk_text_buffer_get_mark (console->priv->buffer, "sql_start");
	if (mark)
		gtk_text_buffer_delete_mark (console->priv->buffer, mark);
	mark = gtk_text_buffer_get_mark (console->priv->buffer, "sql_prompt");
	if (mark)
		gtk_text_buffer_delete_mark (console->priv->buffer, mark);
	gtk_text_buffer_get_bounds (console->priv->buffer, &start, &end);
	gtk_text_buffer_delete (console->priv->buffer, &start, &end);
	add_prompt (console);

	list = console->priv->hist_items;
	while (list) {
		HistItem *item = (HistItem *)(list->data);
		item->begin = NULL;
		item->end = NULL;
		item->sql_begin = NULL;
		item->sql_end = NULL;
		list = g_list_next (list);
	}
}

/**
 * gnome_db_sql_console_clear_history
 * @console: a #GnomeDbSqlConsole widget
 *
 * Clears the queries history of the @console widget
 */
void
gnome_db_sql_console_clear_history (GnomeDbSqlConsole *console)
{
	g_return_if_fail (console && GNOME_DB_IS_SQL_CONSOLE (console));
	g_return_if_fail (console->priv);

	if (console->priv->hist_items) {
		g_list_foreach (console->priv->hist_items, (GFunc) hist_item_free, NULL);
		g_list_free (console->priv->hist_items);
		console->priv->hist_items = NULL;
	}
}

static void
hist_item_free (HistItem *hist_item)
{
	if (hist_item->cmde)
		g_free (hist_item->cmde);
	if (hist_item->sql)
		g_free (hist_item->sql);
}
