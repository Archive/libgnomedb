/* GNOME DB library
 * Copyright (C) 1999 - 2008 The GNOME Foundation.
 *
 * AUTHORS:
 * 	Rodrigo Moya <rodrigo@gnome-db.org>
 *      Vivien Malerba <malerba@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
 
#ifndef __LIBGNOME_DB_EXTRA_H_
#define __LIBGNOME_DB_EXTRA_H_

#include <libgnomedb-extra/gnome-db-editor.h>
#include <libgnomedb-extra/gnome-db-error-dialog.h>
#include <libgnomedb-extra/gnome-db-error.h>
#include <libgnomedb-extra/gnome-db-gray-bar.h>
#include <libgnomedb-extra/gnome-db-report-editor.h>
#include <libgnomedb-extra/gnome-db-sql-console.h>
#include <libgnomedb-extra/gnome-db-selector.h>
#include <libgnomedb-extra/gnome-db-selector-part.h>
#include <libgnomedb-extra/gnome-db-selector-schema.h>
#include <libgnomedb-extra/gnome-db-selector-meta.h>
#include <libgnomedb-extra/gnome-db-selector-table.h>
#include <libgnomedb-extra/gnome-db-selector-view.h>

#endif
