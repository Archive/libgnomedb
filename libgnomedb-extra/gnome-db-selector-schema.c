/* 
 * Copyright (C) 2008 The GNOME Foundation.
 *
 * AUTHORS:
 *      Vivien Malerba <malerba@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <glib/gi18n-lib.h>
#include "gnome-db-selector-schema.h"
#include "gnome-db-selector.h"
#include "gnome-db-selector-meta.h"
#include "gnome-db-selector-table.h"
#include "gnome-db-selector-view.h"
#include <libgda/libgda.h>

static const gchar *node_names [GNOME_DB_SELECTOR_SCHEMA_LAST] = {
	N_("Tables"),
	N_("Views"),
	N_("Sequences"),
	N_("Procedures"),
	N_("Domains"),
};

static const gchar *node_icons [GNOME_DB_SELECTOR_SCHEMA_LAST] = {
	"Tables.png",
	"Views.png",
	"Sequences.png",
	"Functions.png",
	"Domains.png",
};

struct _GnomeDbSelectorSchemaPrivate {
	GnomeDbSelectorPart *parent;
	gchar               *name;
	GnomeDbSelectorPart **parts;
	GnomeDbSelectorPart *all_schemas_part;
	GdaMetaStore        *store;
	GdaSet              *set;
	GnomeDbSelectorFeature auto_features;
};

/* properties */
enum
{
        PROP_0,
	PROP_META_STORE,
	PROP_AUTO_FEATURES
};

static void gnome_db_selector_schema_class_init (GnomeDbSelectorSchemaClass *klass);
static void gnome_db_selector_schema_init       (GnomeDbSelectorSchema *part, GnomeDbSelectorSchemaClass *klass);
static void gnome_db_selector_schema_finalize   (GObject *object);
static void gnome_db_selector_schema_set_property (GObject *object,
						   guint param_id,
						   const GValue *value,
						   GParamSpec *pspec);
static void gnome_db_selector_schema_get_property (GObject *object,
						   guint param_id,
						   GValue *value,
						   GParamSpec *pspec);

/* GnomeDbSelectorPart interface */
static void       gnome_db_selector_schema_part_iface_init (GnomeDbSelectorPartClass *iface);
static void       gnome_db_selector_schema_set_name        (GnomeDbSelectorPart *part, const gchar *name);
static const gchar *gnome_db_selector_schema_get_name        (GnomeDbSelectorPart *part);
static void       gnome_db_selector_schema_set_parent_part (GnomeDbSelectorPart *part, GnomeDbSelectorPart *parent_part);
static GnomeDbSelectorPart *gnome_db_selector_schema_get_parent_part (GnomeDbSelectorPart *part);
static GdkPixbuf *gnome_db_selector_schema_get_pixbuf (GnomeDbSelectorPart *part);
static void       gnome_db_selector_schema_fill_tree_store (GnomeDbSelectorPart *part, 
							    GtkTreeStore *store, GtkTreeIter *parent_iter);
static GdaSet    *gnome_db_selector_schema_get_params (GnomeDbSelectorPart *part, GtkTreeStore *store, GtkTreeIter *at_iter);
static GdaSet    *gnome_db_selector_schema_get_data (GnomeDbSelectorPart *part, GtkTreeStore *store, GtkTreeIter *at_iter);
static GObjectClass *parent_class = NULL;

/* auto creation of sub parts */
static gboolean add_all_tables_sub_part (GnomeDbSelectorSchema *spart);
static gboolean add_schema_tables_sub_part (GnomeDbSelectorSchema *spart);
static gboolean add_all_views_sub_part (GnomeDbSelectorSchema *spart);
static gboolean add_schema_views_sub_part (GnomeDbSelectorSchema *spart);
static gboolean add_all_domains_sub_part (GnomeDbSelectorSchema *spart);
static gboolean add_schema_domains_sub_part (GnomeDbSelectorSchema *spart);

/*
 * GnomeDbSelectorSchema class implementation
 */

static void
gnome_db_selector_schema_class_init (GnomeDbSelectorSchemaClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gnome_db_selector_schema_finalize;

	/* Properties */
        object_class->set_property = gnome_db_selector_schema_set_property;
        object_class->get_property = gnome_db_selector_schema_get_property;

	g_object_class_install_property (object_class, PROP_META_STORE,
                                         g_param_spec_object ("meta-store",
							      _("GdaMetaStore"),
							      NULL, GDA_TYPE_META_STORE,
							      G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY |
							      G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_AUTO_FEATURES,
                                         g_param_spec_uint ("auto-features",
							    _("Automatically added features"),
							    NULL, 0, G_MAXUINT, 0,
							    G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY |
							    G_PARAM_READABLE));
}

static void
gnome_db_selector_schema_part_iface_init (GnomeDbSelectorPartClass *iface)
{
	iface->set_name = gnome_db_selector_schema_set_name;
	iface->get_name = gnome_db_selector_schema_get_name;
	iface->set_parent_part = gnome_db_selector_schema_set_parent_part;
	iface->get_parent_part = gnome_db_selector_schema_get_parent_part;
	iface->get_pixbuf = gnome_db_selector_schema_get_pixbuf;
	iface->fill_tree_store = gnome_db_selector_schema_fill_tree_store;
	iface->get_params = gnome_db_selector_schema_get_params;
	iface->get_data = gnome_db_selector_schema_get_data;
}

static void
gnome_db_selector_schema_init (GnomeDbSelectorSchema *part, GnomeDbSelectorSchemaClass *klass)
{
	part->priv = g_new0 (GnomeDbSelectorSchemaPrivate, 1);
	part->priv->parts = g_new0 (GnomeDbSelectorPart*, GNOME_DB_SELECTOR_SCHEMA_LAST);
	part->priv->set = gda_set_new_inline (2, "catalog", G_TYPE_STRING, NULL,
					      "schema", G_TYPE_STRING, NULL);
	part->priv->parent = NULL;
	part->priv->name = g_strdup ("SCHEMA");
	part->priv->all_schemas_part = NULL;
}

static void
gnome_db_selector_schema_finalize (GObject *object)
{
	GnomeDbSelectorSchema *part = (GnomeDbSelectorSchema *) object;
	GnomeDbSelectorSchemaNodeType type;

	/* free memory */
	if (part->priv->store) {
		g_object_unref (part->priv->store);
		part->priv->store = NULL;
	}
	if (part->priv->parent)
		g_object_remove_weak_pointer ((GObject*) part->priv->parent, (gpointer*) &(part->priv->parent));

	for (type = GNOME_DB_SELECTOR_SCHEMA_TABLES; type < GNOME_DB_SELECTOR_SCHEMA_LAST; type++) {
		if (part->priv->parts [type])
			g_object_unref (part->priv->parts [type]);
	}
	g_free (part->priv->parts);

	if (part->priv->all_schemas_part)
		g_object_unref (part->priv->all_schemas_part);

	g_object_unref (part->priv->set);
	g_free (part->priv->name);
	g_free (part->priv);
	part->priv = NULL;

	/* chain to parent class */
	parent_class->finalize (object);
}

GType
gnome_db_selector_schema_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0)) {
		static GTypeInfo info = {
			sizeof (GnomeDbSelectorSchemaClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_selector_schema_class_init,
			NULL, NULL,
			sizeof (GnomeDbSelectorSchema),
			0,
			(GInstanceInitFunc) gnome_db_selector_schema_init
		};
		
		static const GInterfaceInfo selector_part = {
			(GInterfaceInitFunc) gnome_db_selector_schema_part_iface_init,
			NULL,
			NULL
		};
		
		type = g_type_register_static (G_TYPE_OBJECT, "GnomeDbSelectorSchema", &info, 0);
		g_type_add_interface_static (type, GNOME_DB_TYPE_SELECTOR_PART, &selector_part);
	}

	return type;
}

static void
gnome_db_selector_schema_set_property (GObject *object,
				       guint param_id,
				       const GValue *value,
				       GParamSpec *pspec)
{
        GnomeDbSelectorSchema *part;

        part = GNOME_DB_SELECTOR_SCHEMA (object);
        if (part->priv) {
                switch (param_id) {
		case PROP_META_STORE:
			part->priv->store = g_value_get_object (value);
			if (part->priv->store)
				g_object_ref (part->priv->store);
			break;
		case PROP_AUTO_FEATURES:
			part->priv->auto_features = g_value_get_uint (value);
			break;
                }
        }
}

static void
gnome_db_selector_schema_get_property (GObject *object,
				       guint param_id,
				       GValue *value,
				       GParamSpec *pspec)
{
        GnomeDbSelectorSchema *part;

        part = GNOME_DB_SELECTOR_SCHEMA (object);
        if (part->priv) {
                switch (param_id) {
		case PROP_META_STORE:
			g_value_set_object (value, part->priv->store);
			break;
		case PROP_AUTO_FEATURES:
			g_value_set_uint (value, part->priv->auto_features);
			break;
                }
        }
}

static void
gnome_db_selector_schema_set_name (GnomeDbSelectorPart *part, const gchar *name)
{
	GnomeDbSelectorSchema *spart = (GnomeDbSelectorSchema*) part;
	g_free (spart->priv->name);
	if (name)
		spart->priv->name = g_strdup (name);
	else
		spart->priv->name = NULL;
}

static const gchar *
gnome_db_selector_schema_get_name (GnomeDbSelectorPart *part)
{
	GnomeDbSelectorSchema *spart = (GnomeDbSelectorSchema*) part;
	return spart->priv->name;
}

static void
gnome_db_selector_schema_set_parent_part (GnomeDbSelectorPart *part, GnomeDbSelectorPart *parent_part)
{
	GnomeDbSelectorSchema *spart = (GnomeDbSelectorSchema*) part;
	if (spart->priv->parent) 
		g_object_remove_weak_pointer ((GObject*) spart->priv->parent, (gpointer *) &(spart->priv->parent));
	spart->priv->parent = parent_part;
	g_object_add_weak_pointer ((GObject*) spart->priv->parent, (gpointer *) &(spart->priv->parent));
}

static GnomeDbSelectorPart *
gnome_db_selector_schema_get_parent_part (GnomeDbSelectorPart *part)
{
	return ((GnomeDbSelectorSchema*) part)->priv->parent;
}

static GdkPixbuf *
gnome_db_selector_schema_get_pixbuf (GnomeDbSelectorPart *part)
{
	return _gnome_db_selector_create_pixbuf ("Schema.png");
}

static void
create_sub_schemas_if_no_schema_specified (GnomeDbSelectorSchema *spart, GnomeDbSelectorPartFillMode *mode, 
					   GtkTreeStore *store, GtkTreeIter *parent_iter)
{
	GnomeDbSelectorPart *as;
	as = gnome_db_selector_meta_new_with_sql (spart->priv->store,
						  "SELECT schema_name as schema, catalog_name as catalog "
						  "FROM _schemata WHERE schema_internal = FALSE",
						  NULL);
	g_object_set (G_OBJECT (as), "pixbuf-file", "Schema.png", NULL);
	gnome_db_selector_part_set_parent_part (as, (GnomeDbSelectorPart*) spart);
	
	GnomeDbSelectorPart *subschema;
	subschema = (GnomeDbSelectorPart*) g_object_new (GNOME_DB_TYPE_SELECTOR_SCHEMA,
							 "meta-store", spart->priv->store,
							 "auto-features", spart->priv->auto_features, NULL);
	gnome_db_selector_meta_set_sub_part (GNOME_DB_SELECTOR_META (as), subschema);
	g_object_unref (subschema);

	/* Final add of part */
	GtkTreeIter iter;
	GdkPixbuf *pixbuf;

	gnome_db_selector_part_obtain_new_store_row ((GnomeDbSelectorPart*) spart, mode, store, &iter, parent_iter);
	pixbuf = _gnome_db_selector_create_pixbuf ("Schemas.png");
	gtk_tree_store_set (store, &iter,
			    GNOME_DB_SELECTOR_PART_LABEL_COLUMN, _("Schemas"),
			    GNOME_DB_SELECTOR_PART_PIXBUF_COLUMN, pixbuf, 
			    GNOME_DB_SELECTOR_PART_PART_COLUMN, spart, -1);
	if (pixbuf)
		g_object_unref (pixbuf);
	gnome_db_selector_part_fill_tree_store (as, store, &iter);
	spart->priv->all_schemas_part = as;
}

static void
gnome_db_selector_schema_fill_tree_store (GnomeDbSelectorPart *part, 
					  GtkTreeStore *store, GtkTreeIter *parent_iter)
{
	GtkTreeIter iter;
	GdkPixbuf *pixbuf;
	GnomeDbSelectorSchemaNodeType type;
	GnomeDbSelectorSchema *spart = (GnomeDbSelectorSchema*) part;
	GnomeDbSelectorPartFillMode mode = GNOME_DB_SELECTOR_PART_FILL_MODE_FIRST_FILL;
	gboolean schema_set = TRUE;

	/* determine if the "schema" holder has been set */
	const GValue *cvalue;
	cvalue = gda_set_get_holder_value (spart->priv->set, "schema");
	if (gda_value_is_null (cvalue) || !g_value_get_string (cvalue))
		schema_set = FALSE;

	/* find the first iter below @parent_iter for which GNOME_DB_SELECTOR_PART_PART_COLUMN = @part */
	if (gtk_tree_model_iter_children ((GtkTreeModel*) store, &iter, parent_iter)) {
		do {
			GnomeDbSelectorPart *apart;
			gtk_tree_model_get ((GtkTreeModel*) store, &iter,
					    GNOME_DB_SELECTOR_PART_PART_COLUMN, &apart, -1);
			if (apart) {
				g_object_unref (apart);
				if (apart == part) {
					mode = GNOME_DB_SELECTOR_PART_FILL_MODE_OVW_FIRST;
					break;
				}
			}
		}
		while (gtk_tree_model_iter_next ((GtkTreeModel*) store, &iter));
	}

	/* create nodes for parts */
	for (type = GNOME_DB_SELECTOR_SCHEMA_TABLES; type < GNOME_DB_SELECTOR_SCHEMA_LAST; type++) {
		if (!spart->priv->parts [type]) {
			gboolean added = FALSE;
			if ((type == GNOME_DB_SELECTOR_SCHEMA_TABLES) && 
			    (spart->priv->auto_features & GNOME_DB_SELECTOR_FEATURE_SCHEMA_TABLES)) {
				if (schema_set)
					added = add_schema_tables_sub_part (spart);
				else
					added = add_all_tables_sub_part (spart);
			}
			else if ((type == GNOME_DB_SELECTOR_SCHEMA_VIEWS) && 
				 (spart->priv->auto_features & GNOME_DB_SELECTOR_FEATURE_SCHEMA_VIEWS)) {
				if (schema_set)
					added = add_schema_views_sub_part (spart);
				else
					added = add_all_views_sub_part (spart);
			}
			else if ((type == GNOME_DB_SELECTOR_SCHEMA_DOMAINS) && 
				 (spart->priv->auto_features & GNOME_DB_SELECTOR_FEATURE_SCHEMA_DOMAINS)) {
				if (schema_set)
					added = add_schema_domains_sub_part (spart);
				else
					added = add_all_domains_sub_part (spart);
			}
			
			if (!added)
				continue;
		}
		pixbuf = NULL;
		gnome_db_selector_part_obtain_new_store_row (part, &mode, store, &iter, parent_iter);
		pixbuf = gnome_db_selector_part_get_pixbuf (spart->priv->parts [type]);
		if (!pixbuf)
			pixbuf = _gnome_db_selector_create_pixbuf (node_icons[type]);
		gtk_tree_store_set (store, &iter,
				    GNOME_DB_SELECTOR_PART_LABEL_COLUMN, node_names[type], 
				    GNOME_DB_SELECTOR_PART_PIXBUF_COLUMN, pixbuf, 
				    GNOME_DB_SELECTOR_PART_PART_COLUMN, part, -1);
		if (pixbuf)
			g_object_unref (pixbuf);

		/* sub part */
		gnome_db_selector_part_fill_tree_store (spart->priv->parts [type], store, &iter);
	}

	/* if the "schema" holder has not been set, then add a list of schemas as well */
	if (!schema_set && spart->priv->store && (mode == GNOME_DB_SELECTOR_PART_FILL_MODE_FIRST_FILL)) 
		create_sub_schemas_if_no_schema_specified (spart, &mode, store, parent_iter);
}

static GdaSet *
gnome_db_selector_schema_get_params (GnomeDbSelectorPart *part, GtkTreeStore *store, GtkTreeIter *at_iter)
{
	GnomeDbSelectorSchema *spart = (GnomeDbSelectorSchema*) part;
	return spart->priv->set;
}

static GdaSet *
gnome_db_selector_schema_get_data (GnomeDbSelectorPart *part, GtkTreeStore *store, GtkTreeIter *at_iter)
{
	return NULL;
}


/**
 * gnome_db_selector_schema_new
 *
 * Returns: a new #GnomeDbSelectorPart object
 */
GnomeDbSelectorPart *
gnome_db_selector_schema_new (void)
{
	return GNOME_DB_SELECTOR_PART (g_object_new (GNOME_DB_TYPE_SELECTOR_SCHEMA, NULL));
}

/**
 * gnome_db_selector_schema_set_sub_part
 * @part:
 * @type:
 * @subpart:
 *
 * Attach a #GnomeDbSelectorPart as a sub part of @part
 */
void
gnome_db_selector_schema_set_sub_part (GnomeDbSelectorSchema *part, GnomeDbSelectorSchemaNodeType type,
				       GnomeDbSelectorPart *subpart)
{
	g_return_if_fail (GNOME_DB_IS_SELECTOR_SCHEMA (part));
	g_return_if_fail (GNOME_DB_IS_SELECTOR_PART (subpart));
	g_return_if_fail ((type >= 0) && (type < GNOME_DB_SELECTOR_SCHEMA_LAST));

	/* add new sub part */
	if (part->priv->parts [type])
		g_object_unref (part->priv->parts [type]);
	part->priv->parts [type] = subpart;
	g_object_ref (subpart);

	gnome_db_selector_part_set_parent_part (subpart, (GnomeDbSelectorPart*) part);
}

/*
 * auto creation of sub parts
 */
static gboolean
add_all_tables_sub_part (GnomeDbSelectorSchema *spart)
{
	GnomeDbSelectorPart *sc;

	g_print ("%s()\n", __FUNCTION__);
	sc = gnome_db_selector_meta_new_with_sql (spart->priv->store,
						  "SELECT table_name FROM _tables " 
						  "WHERE table_short_name != table_full_name "
						  "AND table_type='BASE TABLE' "
						  "ORDER BY table_name",
						  NULL);
	if (!sc) {
		g_warning ("Can't create GnomeDbSelectorMeta object for tables\n");
		return FALSE;
	}

	g_object_set (G_OBJECT (sc), "pixbuf-file", "Table.png", NULL);
	gnome_db_selector_part_set_name (sc, "_tables");
	gnome_db_selector_schema_set_sub_part (spart, GNOME_DB_SELECTOR_SCHEMA_TABLES, sc);

	if (spart->priv->auto_features & GNOME_DB_SELECTOR_FEATURE_SCHEMA_TABLE_COLUMNS) {
		GnomeDbSelectorPart *colspart;
		colspart = (GnomeDbSelectorPart *) g_object_new (GNOME_DB_TYPE_SELECTOR_TABLE, 
								 "meta-store", spart->priv->store, 
								 "auto-features", spart->priv->auto_features,
								 NULL);
		gnome_db_selector_meta_set_sub_part (GNOME_DB_SELECTOR_META (sc), colspart);
		g_object_unref (colspart);
	}

	g_object_unref (sc);
	return TRUE;
}

static gboolean
add_schema_tables_sub_part (GnomeDbSelectorSchema *spart)
{
	GnomeDbSelectorPart *sc;

	g_print ("%s()\n", __FUNCTION__);
	sc = gnome_db_selector_meta_new_with_sql (spart->priv->store,
						  "SELECT table_name FROM _tables " 
						  "WHERE table_catalog = ##catalog::string AND "
						  "table_schema = ##schema::string "
						  "AND table_type='BASE TABLE' "
						  "ORDER BY table_name",
						  NULL);
	if (!sc) {
		g_warning ("Can't create GnomeDbSelectorMeta object for tables\n");
		return FALSE;
	}

	g_object_set (G_OBJECT (sc), "pixbuf-file", "Table.png", NULL);
	gnome_db_selector_part_set_name (sc, "_tables");
	gnome_db_selector_schema_set_sub_part (spart, GNOME_DB_SELECTOR_SCHEMA_TABLES, sc);

	if (spart->priv->auto_features & GNOME_DB_SELECTOR_FEATURE_SCHEMA_TABLE_COLUMNS) {
		GnomeDbSelectorPart *colspart;
		colspart = (GnomeDbSelectorPart *) g_object_new (GNOME_DB_TYPE_SELECTOR_TABLE, 
								 "meta-store", spart->priv->store, 
								 "auto-features", spart->priv->auto_features,
								 NULL);
		gnome_db_selector_meta_set_sub_part (GNOME_DB_SELECTOR_META (sc), colspart);
		g_object_unref (colspart);
	}

	g_object_unref (sc);
	
	return TRUE;
}

static gboolean
add_all_views_sub_part (GnomeDbSelectorSchema *spart)
{
	GnomeDbSelectorPart *sc;

	g_print ("%s()\n", __FUNCTION__);
	sc = gnome_db_selector_meta_new_with_sql (spart->priv->store,
						  "SELECT table_name AS view_name FROM _tables " 
						  "WHERE table_short_name != table_full_name "
						  "AND table_type='VIEW' "
						  "ORDER BY table_name",
						  NULL);
	if (!sc) {
		g_warning ("Can't create GnomeDbSelectorMeta object for tables\n");
		return FALSE;
	}

	g_object_set (G_OBJECT (sc), "pixbuf-file", "View.png", NULL);
	gnome_db_selector_part_set_name (sc, "_views");
	gnome_db_selector_schema_set_sub_part (spart, GNOME_DB_SELECTOR_SCHEMA_VIEWS, sc);

	if (spart->priv->auto_features & GNOME_DB_SELECTOR_FEATURE_SCHEMA_VIEW_COLUMNS) {
		GnomeDbSelectorPart *colspart;
		colspart = (GnomeDbSelectorPart *) g_object_new (GNOME_DB_TYPE_SELECTOR_VIEW, 
								 "meta-store", spart->priv->store, 
								 "auto-features", spart->priv->auto_features,
								 NULL);
		gnome_db_selector_meta_set_sub_part (GNOME_DB_SELECTOR_META (sc), colspart);
		g_object_unref (colspart);
	}

	g_object_unref (sc);
	return TRUE;
}

static gboolean
add_schema_views_sub_part (GnomeDbSelectorSchema *spart)
{
	GnomeDbSelectorPart *sc;

	g_print ("%s()\n", __FUNCTION__);
	sc = gnome_db_selector_meta_new_with_sql (spart->priv->store,
						  "SELECT table_name AS view_name FROM _tables " 
						  "WHERE table_catalog = ##catalog::string AND "
						  "table_schema = ##schema::string "
						  "AND table_type='VIEW' "
						  "ORDER BY table_name",
						  NULL);
	if (!sc) {
		g_warning ("Can't create GnomeDbSelectorMeta object for tables\n");
		return FALSE;
	}

	g_object_set (G_OBJECT (sc), "pixbuf-file", "View.png", NULL);
	gnome_db_selector_part_set_name (sc, "_views");
	gnome_db_selector_schema_set_sub_part (spart, GNOME_DB_SELECTOR_SCHEMA_VIEWS, sc);

	if (spart->priv->auto_features & GNOME_DB_SELECTOR_FEATURE_SCHEMA_VIEW_COLUMNS) {
		GnomeDbSelectorPart *colspart;
		colspart = (GnomeDbSelectorPart *) g_object_new (GNOME_DB_TYPE_SELECTOR_VIEW, 
								 "meta-store", spart->priv->store, 
								 "auto-features", spart->priv->auto_features,
								 NULL);
		gnome_db_selector_meta_set_sub_part (GNOME_DB_SELECTOR_META (sc), colspart);
		g_object_unref (colspart);
	}

	g_object_unref (sc);
	return TRUE;
}

static gboolean
add_all_domains_sub_part (GnomeDbSelectorSchema *spart)
{
	GnomeDbSelectorPart *sc;

	g_print ("%s()\n", __FUNCTION__);
	sc = gnome_db_selector_meta_new_with_sql (spart->priv->store,
						  "SELECT domain_name FROM _domains "
						  "WHERE domain_short_name != domain_full_name "
						  "ORDER BY domain_name",
						  NULL);
	if (!sc) {
		g_warning ("Can't create GnomeDbSelectorMeta object for tables\n");
		return FALSE;
	}

	g_object_set (G_OBJECT (sc), "pixbuf-file", "Domain.png", NULL);
	gnome_db_selector_part_set_name (sc, "_domains");
	gnome_db_selector_schema_set_sub_part (spart, GNOME_DB_SELECTOR_SCHEMA_DOMAINS, sc);
	g_object_unref (sc);
	return TRUE;
}

static gboolean
add_schema_domains_sub_part (GnomeDbSelectorSchema *spart)
{
	GnomeDbSelectorPart *sc;

	g_print ("%s()\n", __FUNCTION__);
	sc = gnome_db_selector_meta_new_with_sql (spart->priv->store,
						  "SELECT domain_name FROM _domains "
						  "WHERE domain_catalog = ##catalog::string AND "
						  "domain_schema = ##schema::string "
						  "ORDER BY domain_name",
						  NULL);
	if (!sc) {
		g_warning ("Can't create GnomeDbSelectorMeta object for tables\n");
		return FALSE;
	}

	g_object_set (G_OBJECT (sc), "pixbuf-file", "Domain.png", NULL);
	gnome_db_selector_part_set_name (sc, "_domains");
	gnome_db_selector_schema_set_sub_part (spart, GNOME_DB_SELECTOR_SCHEMA_DOMAINS, sc);
	g_object_unref (sc);
	return TRUE;
}
