/* gnome-db-selector.h
 *
 * Copyright (C) 2002 - 2008 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNOME_DB_SELECTOR__
#define __GNOME_DB_SELECTOR__

#include <gtk/gtk.h>
#include <libgnomedb-extra/gnome-db-selector-part.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_SELECTOR          (gnome_db_selector_get_type())
#define GNOME_DB_SELECTOR(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, gnome_db_selector_get_type(), GnomeDbSelector)
#define GNOME_DB_SELECTOR_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, gnome_db_selector_get_type (), GnomeDbSelectorClass)
#define GNOME_DB_IS_SELECTOR(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, gnome_db_selector_get_type ())

typedef struct _GnomeDbSelector      GnomeDbSelector;
typedef struct _GnomeDbSelectorClass GnomeDbSelectorClass;
typedef struct _GnomeDbSelectorPriv  GnomeDbSelectorPriv;

/* struct for the object's data */
struct _GnomeDbSelector
{
	GtkTreeView          object;
	GnomeDbSelectorPriv *priv;
};

/* struct for the object's class */
struct _GnomeDbSelectorClass
{
	GtkTreeViewClass    parent_class;
	void              (*selected_object_changed) (GnomeDbSelector *sel, GnomeDbSelectorPart *part, 
						      GtkTreeStore *store, GtkTreeIter *iter);
};

typedef enum {
	GNOME_DB_SELECTOR_FEATURE_NONE = 0,
	GNOME_DB_SELECTOR_FEATURE_SCHEMAS = 1 << 0,
	GNOME_DB_SELECTOR_FEATURE_SCHEMA_TABLES = 1 << 1,
	GNOME_DB_SELECTOR_FEATURE_SCHEMA_VIEWS = 1 << 2,
	GNOME_DB_SELECTOR_FEATURE_SCHEMA_DOMAINS = 1 << 3,

	GNOME_DB_SELECTOR_FEATURE_SCHEMA_TABLE_COLUMNS = 1 << 4,
	GNOME_DB_SELECTOR_FEATURE_SCHEMA_TABLE_CONSTRAINTS = 1 << 5,
	GNOME_DB_SELECTOR_FEATURE_SCHEMA_TABLE_TRIGGERS = 1 << 6,
	
	GNOME_DB_SELECTOR_FEATURE_SCHEMA_TABLE_DETAILS = (GNOME_DB_SELECTOR_FEATURE_SCHEMA_TABLE_COLUMNS | GNOME_DB_SELECTOR_FEATURE_SCHEMA_TABLE_CONSTRAINTS | GNOME_DB_SELECTOR_FEATURE_SCHEMA_TABLE_TRIGGERS),

	GNOME_DB_SELECTOR_FEATURE_SCHEMA_VIEW_COLUMNS = 1 << 7,
	GNOME_DB_SELECTOR_FEATURE_SCHEMA_VIEW_DETAILS = (GNOME_DB_SELECTOR_FEATURE_SCHEMA_VIEW_COLUMNS)
} GnomeDbSelectorFeature;

GType      gnome_db_selector_get_type              (void) G_GNUC_CONST;
GtkWidget *gnome_db_selector_new                   (void);
void       gnome_db_selector_add_part              (GnomeDbSelector *sel, const gchar *name, GnomeDbSelectorPart *part);
void       gnome_db_selector_add_parts_for_feature (GnomeDbSelector *sel, GdaMetaStore *store, 
						    GnomeDbSelectorFeature feature);
/* private utility function */
GdkPixbuf *_gnome_db_selector_create_pixbuf        (const gchar *filename);

G_END_DECLS

#endif
