/* GNOME DB library
 * Copyright (C) 1999 - 2007 The GNOME Foundation.
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GNOME_DB_EDITOR_H__
#define __GNOME_DB_EDITOR_H__

#include <gtk/gtkvbox.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_EDITOR            (gnome_db_editor_get_type())
#define GNOME_DB_EDITOR(obj)            (G_TYPE_CHECK_INSTANCE_CAST (obj, GNOME_DB_TYPE_EDITOR, GnomeDbEditor))
#define GNOME_DB_EDITOR_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST (klass, GNOME_DB_TYPE_EDITOR, GnomeDbEditorClass))
#define GNOME_DB_IS_EDITOR(obj)         (G_TYPE_CHECK_INSTANCE_TYPE (obj, GNOME_DB_TYPE_EDITOR))
#define GNOME_DB_IS_EDITOR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GNOME_DB_TYPE_EDITOR))

typedef struct _GnomeDbEditor        GnomeDbEditor;
typedef struct _GnomeDbEditorClass   GnomeDbEditorClass;
typedef struct _GnomeDbEditorPrivate GnomeDbEditorPrivate;

struct _GnomeDbEditor {
	GtkVBox parent;
	GnomeDbEditorPrivate *priv;
};

struct _GnomeDbEditorClass {
	GtkVBoxClass parent_class;

	/* signals */
	void (* text_changed) (GnomeDbEditor editor);
};

#define GNOME_DB_EDITOR_LANGUAGE_SQL "sql"

GType      gnome_db_editor_get_type (void) G_GNUC_CONST;
GtkWidget *gnome_db_editor_new (void);

gboolean   gnome_db_editor_get_editable (GnomeDbEditor *editor);
void       gnome_db_editor_set_editable (GnomeDbEditor *editor, gboolean editable);
gboolean   gnome_db_editor_get_highlight (GnomeDbEditor *editor);
void       gnome_db_editor_set_highlight (GnomeDbEditor *editor, gboolean highlight);
void       gnome_db_editor_set_text (GnomeDbEditor *editor, const gchar *text, gint len);
gchar     *gnome_db_editor_get_all_text (GnomeDbEditor *editor);
gboolean   gnome_db_editor_load_from_file (GnomeDbEditor *editor, const gchar *filename);
gboolean   gnome_db_editor_save_to_file (GnomeDbEditor *editor, const gchar *filename);

void       gnome_db_editor_copy_clipboard (GnomeDbEditor *editor);
void       gnome_db_editor_cut_clipboard (GnomeDbEditor *editor);
void       gnome_db_editor_paste_clipboard (GnomeDbEditor *editor);

G_END_DECLS

#endif
