/* 
 * Copyright (C) 2008 The GNOME Foundation.
 *
 * AUTHORS:
 *      Vivien Malerba <malerba@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "gnome-db-selector-part.h"
#include <glib/gi18n-lib.h>

static void gnome_db_selector_part_class_init (gpointer g_class);

GType
gnome_db_selector_part_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0)) {
		static const GTypeInfo info = {
			sizeof (GnomeDbSelectorPartClass),
			(GBaseInitFunc) gnome_db_selector_part_class_init,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) NULL,
			NULL,
			NULL,
			0,
			0,
			(GInstanceInitFunc) NULL
		};
		
		type = g_type_register_static (G_TYPE_INTERFACE, "GnomeDbSelectorPart", &info, 0);
		g_type_interface_add_prerequisite (type, G_TYPE_OBJECT);
	}
	return type;
}

static void
gnome_db_selector_part_class_init (gpointer g_class)
{
	static gboolean initialized = FALSE;

	if (! initialized) {
		initialized = TRUE;
	}
}

/**
 * gnome_db_selector_part_set_name
 * @part: a #GnomeDbSelectorPart object
 * @name: a name for @part
 *
 * Set @part's name to make it easy to identify
 */
void
gnome_db_selector_part_set_name (GnomeDbSelectorPart *part, const gchar *name)
{
	g_return_if_fail (GNOME_DB_IS_SELECTOR_PART (part));
	if (GNOME_DB_SELECTOR_PART_GET_CLASS (part)->set_name) 
		(GNOME_DB_SELECTOR_PART_GET_CLASS (part)->set_name) (part, name);
	else
		g_warning ("GnomeDbSelectorPart interface implementation error: "
			   "%s does not implement the set_name() mathod",
			   G_OBJECT_CLASS_NAME (G_OBJECT_GET_CLASS (part)));
}

/**
 * gnome_db_selector_part_get_name
 * @part: a #GnomeDbSelectorPart object
 *
 * Retreive @part's name. 
 *
 * Returns: @part's name
 */
const gchar *
gnome_db_selector_part_get_name (GnomeDbSelectorPart *part)
{
	g_return_val_if_fail (GNOME_DB_IS_SELECTOR_PART (part), NULL);
	if (GNOME_DB_SELECTOR_PART_GET_CLASS (part)->get_name) 
		return (GNOME_DB_SELECTOR_PART_GET_CLASS (part)->get_name) (part);
	else {
		g_warning ("GnomeDbSelectorPart interface implementation error: "
			   "%s does not implement the get_name() mathod",
			   G_OBJECT_CLASS_NAME (G_OBJECT_GET_CLASS (part)));
		return NULL;
	}
}

/**
 * gnome_db_selector_part_set_parent_part
 * @part: a #GnomeDbSelectorPart object
 * @parent_part: a #GnomeDbSelectorPart object, or %NULL
 *
 * Declares to @part that its part parent is @parent_part
 */
void
gnome_db_selector_part_set_parent_part (GnomeDbSelectorPart *part, GnomeDbSelectorPart *parent_part)
{
	g_return_if_fail (GNOME_DB_IS_SELECTOR_PART (part));
	g_return_if_fail (!parent_part || GNOME_DB_IS_SELECTOR_PART (parent_part));
	if (GNOME_DB_SELECTOR_PART_GET_CLASS (part)->set_parent_part) 
		(GNOME_DB_SELECTOR_PART_GET_CLASS (part)->set_parent_part) (part, parent_part);
	else
		g_warning ("GnomeDbSelectorPart interface implementation error: "
			   "%s does not implement the set_parent_part() mathod",
			   G_OBJECT_CLASS_NAME (G_OBJECT_GET_CLASS (part)));
}

/**
 * gnome_db_selector_part_get_parent_part
 * @part: #GnomeDbSelectorPart object
 *
 * Get the parent of @part.
 *
 * Returns: the parent part, or %NULL if there is none
 */
GnomeDbSelectorPart *
gnome_db_selector_part_get_parent_part (GnomeDbSelectorPart *part)
{
	g_return_val_if_fail (GNOME_DB_IS_SELECTOR_PART (part), NULL);
	if (GNOME_DB_SELECTOR_PART_GET_CLASS (part)->get_parent_part) 
		return (GNOME_DB_SELECTOR_PART_GET_CLASS (part)->get_parent_part) (part);
	else {
		g_warning ("GnomeDbSelectorPart interface implementation error: "
			   "%s does not implement the get_parent_part() mathod",
			   G_OBJECT_CLASS_NAME (G_OBJECT_GET_CLASS (part)));
		return NULL;
	}
}

/**
 * gnome_db_selector_part_get_pixbuf
 * @part: #GnomeDbSelectorPart object
 *
 * Get the preferred pixbug to be used when representing @part
 *
 * Returns: a new #GdkPixbuf (or %NULL). The caller must call g_object_unref() on it when finished
 */
GdkPixbuf *
gnome_db_selector_part_get_pixbuf (GnomeDbSelectorPart *part)
{
	g_return_val_if_fail (GNOME_DB_IS_SELECTOR_PART (part), NULL);
	if (GNOME_DB_SELECTOR_PART_GET_CLASS (part)->get_pixbuf) 
		return (GNOME_DB_SELECTOR_PART_GET_CLASS (part)->get_pixbuf) (part);
	else
		return NULL;
}

/**
 * gnome_db_selector_part_update_params
 * @part: a #GnomeDbSelectorPart object.
 * @parent_part: a #GnomeDbSelectorPart object.
 * @store: a #GtkTreeStore 
 * @parent_iter:
 */
static void
gnome_db_selector_part_update_params (GdaSet *params, GnomeDbSelectorPart *parent_part,
				      GtkTreeStore *store, GtkTreeIter *parent_iter)
{
	if (params) {
		GSList *list;
		
		if (parent_part) 
			for (list = params->holders; list; list = list->next) {
				GdaHolder *holder = GDA_HOLDER (list->data);
				const GValue *cvalue;
				cvalue = gnome_db_selector_part_get_value (parent_part, 
									   gda_holder_get_id (holder),
									   store, parent_iter);
				if (cvalue)
					gda_holder_set_value (holder, cvalue, NULL);
			}
		if (!gda_set_is_valid (params, NULL)) {
			g_warning (_("GnomeDbSelectorPart object requires parameters which cannot be obtained or are invalid"));
			return;
		}
	}
}

/**
 * gnome_db_selector_part_fill_tree_store
 * @part: a #GnomeDbSelectorPart object.
 * @parent_part: a #GnomeDbSelectorPart object.
 * @store: a #GtkTreeStore 
 * @parent_iter:
 *
 * Make @part populate (or refresh) the contents of @store, under the @parent node
 */
void
gnome_db_selector_part_fill_tree_store (GnomeDbSelectorPart *part, 
					GtkTreeStore *store, GtkTreeIter *parent_iter)
{
	g_return_if_fail (GNOME_DB_IS_SELECTOR_PART (part));
	if (GNOME_DB_SELECTOR_PART_GET_CLASS (part)->fill_tree_store) {
		GnomeDbSelectorPart *parent_part = gnome_db_selector_part_get_parent_part (part);
		GdaSet *params;
		params = gnome_db_selector_part_get_params (part, store, parent_iter);
		gnome_db_selector_part_update_params (params, parent_part, store, parent_iter);
		(GNOME_DB_SELECTOR_PART_GET_CLASS (part)->fill_tree_store) (part, store, parent_iter);
	}
	else 
		g_warning ("GnomeDbSelectorPart interface implementation error: "
			   "%s does not implement the fill_tree_store() mathod",
			   G_OBJECT_CLASS_NAME (G_OBJECT_GET_CLASS (part)));
}

/**
 * gnome_db_selector_part_get_params
 * @part: a #GnomeDbSelectorPart object
 * @store: a #GtkTreeStore, or %NULL
 * @at_iter: a #GtkTreeIter, or %NULL
 *
 * The caller does not own the returned #GdaSet.
 */
GdaSet *
gnome_db_selector_part_get_params (GnomeDbSelectorPart *part, GtkTreeStore *store, GtkTreeIter *at_iter)
{
	g_return_val_if_fail (GNOME_DB_IS_SELECTOR_PART (part), NULL);
	if (GNOME_DB_SELECTOR_PART_GET_CLASS (part)->get_params) {
		GdaSet *params;
		params = (GNOME_DB_SELECTOR_PART_GET_CLASS (part)->get_params) (part, store, at_iter);
		if (params) {
			GtkTreeIter parent_iter;
			if (store && gtk_tree_model_iter_parent (GTK_TREE_MODEL (store), &parent_iter, at_iter)) {
				GnomeDbSelectorPart *parent_part;
				parent_part = gnome_db_selector_part_get_parent_part (part);
				gnome_db_selector_part_update_params (params, parent_part, store, &parent_iter);
			}
		}
		return params;
	}
	else
		return NULL;
}

/**
 * gnome_db_selector_part_get_data
 * @part: a #GnomeDbSelectorPart object
 * @store: a #GtkTreeStore, or %NULL
 * @at_iter: a #GtkTreeIter, or %NULL
 *
 * The caller does not own the returned #GdaSet.
 */
GdaSet *
gnome_db_selector_part_get_data (GnomeDbSelectorPart *part, GtkTreeStore *store, GtkTreeIter *at_iter)
{
	g_return_val_if_fail (GNOME_DB_IS_SELECTOR_PART (part), NULL);
	if (GNOME_DB_SELECTOR_PART_GET_CLASS (part)->get_data) 
		return (GNOME_DB_SELECTOR_PART_GET_CLASS (part)->get_data) (part, store, at_iter);
	else
		return NULL;
}

/**
 * gnome_db_selector_part_get_value
 * @part: a #GnomeDbSelectorPart object
 * @id: the ID of the requested value
 * @store: a #GtkTreeStore, or %NULL
 * @at_iter: a #GtkTreeIter, or %NULL
 *
 * Looks for a value which id is @id in @part (also looks in its parent hierarchy)
 *
 * Returns: the (non modifiable) #GValue, or %NULL if not found
 */ 
const GValue *
gnome_db_selector_part_get_value (GnomeDbSelectorPart *part, const gchar *id,
				  GtkTreeStore *store, GtkTreeIter *at_iter)
{
	GdaHolder *holder;
	GdaSet *set;

	/* search in @part */
	set = gnome_db_selector_part_get_params (part, store, at_iter);
	if (set) {
		holder = gda_set_get_holder (set, id);
		if (holder)
			return gda_holder_get_value (holder);
	}
	set = gnome_db_selector_part_get_data (part, store, at_iter);
	if (set) {
		holder = gda_set_get_holder (set, id);
		if (holder)
			return gda_holder_get_value (holder);
	}

	/* search in part's parent */
	GnomeDbSelectorPart *parent;
	parent = gnome_db_selector_part_get_parent_part (part);
	if (parent) {
		GtkTreeIter piter;
		if (gtk_tree_model_iter_parent (GTK_TREE_MODEL (store), &piter, at_iter)) 
			return gnome_db_selector_part_get_value (parent, id, store, &piter);
	}
	
	return NULL;
}

/**
 * gnome_db_selector_part_obtain_new_store_row
 *
 * Reserved for #GnomeDbSelectorPart implementations
 */
void
gnome_db_selector_part_obtain_new_store_row (GnomeDbSelectorPart *part, GnomeDbSelectorPartFillMode *in_out_mode,
					     GtkTreeStore *store, GtkTreeIter *iter, GtkTreeIter *parent_iter)
{
	if (*in_out_mode == GNOME_DB_SELECTOR_PART_FILL_MODE_FIRST_FILL) 
		gtk_tree_store_append (store, iter, parent_iter);
	else if (*in_out_mode == GNOME_DB_SELECTOR_PART_FILL_MODE_OVW_FIRST)
		*in_out_mode = GNOME_DB_SELECTOR_PART_FILL_MODE_OVW_NEXT;
	else {
		GtkTreeIter next = *iter;
		gboolean add_one = TRUE;
		if (gtk_tree_model_iter_next ((GtkTreeModel*) store, &next)) {
			GnomeDbSelectorPart *apart;
			gtk_tree_model_get ((GtkTreeModel*) store, &next,
					    GNOME_DB_SELECTOR_PART_PART_COLUMN, &apart, -1);
			if (apart) {
				g_object_unref (apart);
				if (apart == part) 
					add_one = FALSE;
			}
		}
		if (add_one) 
			gtk_tree_store_insert_after (store, &next, parent_iter, iter);
		
		*iter = next;
	}
}
