/* 
 * Copyright (C) 2008 The GNOME Foundation.
 *
 * AUTHORS:
 *      Vivien Malerba <malerba@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GNOME_DB_SELECTOR_TABLE_H__
#define __GNOME_DB_SELECTOR_TABLE_H__

#include <glib-object.h>
#include <gtk/gtk.h>
#include <libgnomedb-extra/gnome-db-selector-part.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_SELECTOR_TABLE            (gnome_db_selector_table_get_type())
#define GNOME_DB_SELECTOR_TABLE(obj)            (G_TYPE_CHECK_INSTANCE_CAST (obj, GNOME_DB_TYPE_SELECTOR_TABLE, GnomeDbSelectorTable))
#define GNOME_DB_SELECTOR_TABLE_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST (klass, GNOME_DB_TYPE_SELECTOR_TABLE, GnomeDbSelectorTableClass))
#define GNOME_DB_IS_SELECTOR_TABLE(obj)         (G_TYPE_CHECK_INSTANCE_TYPE (obj, GNOME_DB_TYPE_SELECTOR_TABLE))
#define GNOME_DB_IS_SELECTOR_TABLE_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GNOME_DB_TYPE_SELECTOR_TABLE))

typedef struct _GnomeDbSelectorTable        GnomeDbSelectorTable;
typedef struct _GnomeDbSelectorTableClass   GnomeDbSelectorTableClass;
typedef struct _GnomeDbSelectorTablePrivate GnomeDbSelectorTablePrivate;

typedef enum {
	GNOME_DB_SELECTOR_TABLE_COLUMNS = 0,
	GNOME_DB_SELECTOR_TABLE_INDEXES,
	GNOME_DB_SELECTOR_TABLE_CONSTRAINTS,
	GNOME_DB_SELECTOR_TABLE_TRIGGERS,
	GNOME_DB_SELECTOR_TABLE_RULES,

	GNOME_DB_SELECTOR_TABLE_LAST
} GnomeDbSelectorTableNodeType;

struct _GnomeDbSelectorTable {
	GObject                 object;
	GnomeDbSelectorTablePrivate *priv;
};

struct _GnomeDbSelectorTableClass {
	GObjectClass            parent_class;
};

GType                gnome_db_selector_table_get_type       (void) G_GNUC_CONST;
GnomeDbSelectorPart *gnome_db_selector_table_new            (GdaMetaStore *store);

G_END_DECLS

#endif
