/* 
 * Copyright (C) 2008 The GNOME Foundation.
 *
 * AUTHORS:
 *      Vivien Malerba <malerba@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <glib/gi18n-lib.h>
#include "gnome-db-selector-meta.h"
#include "gnome-db-selector.h"
#include <libgda/libgda.h>
#include <sql-parser/gda-sql-parser.h>

struct _GnomeDbSelectorMetaPrivate {
	GnomeDbSelectorPart *parent;
	gchar               *name;
	GdaMetaStore        *store;
	GSList              *stmt_list;
	GSList              *row_ref_list;

	GdaSet              *params;

	GdkPixbuf           *parent_pixbuf;
	GnomeDbSelectorPart *sub_part;
	GdkPixbuf           *entry_pixbuf;

	GnomeDbSelectorMetaPixbufFunc pix_func;
	gint                          pix_func_col;
};

/* properties */
enum
{
        PROP_0,
	PROP_META_STORE,
	PROP_STMT,
	PROP_PARENT_PIXBUF,
	PROP_PIXBUF,
	PROP_PARENT_PIX_FILE,
	PROP_PIX_FILE
};

static void gnome_db_selector_meta_class_init (GnomeDbSelectorMetaClass *klass);
static void gnome_db_selector_meta_init       (GnomeDbSelectorMeta *part, GnomeDbSelectorMetaClass *klass);
static void gnome_db_selector_meta_dispose   (GObject *object);
static void gnome_db_selector_meta_finalize   (GObject *object);
static void gnome_db_selector_meta_set_property (GObject *object,
						 guint param_id,
						 const GValue *value,
						 GParamSpec *pspec);
static void gnome_db_selector_meta_get_property (GObject *object,
						 guint param_id,
						 GValue *value,
						 GParamSpec *pspec);

/* GnomeDbSelectorPart interface */
static void       gnome_db_selector_meta_part_iface_init (GnomeDbSelectorPartClass *iface);
static void       gnome_db_selector_meta_set_name        (GnomeDbSelectorPart *part, const gchar *name);
static const gchar *gnome_db_selector_meta_get_name        (GnomeDbSelectorPart *part);
static void       gnome_db_selector_meta_set_parent_part (GnomeDbSelectorPart *part, GnomeDbSelectorPart *parent_part);
static GnomeDbSelectorPart *gnome_db_selector_meta_get_parent_part (GnomeDbSelectorPart *part);
static GdkPixbuf *gnome_db_selector_meta_get_pixbuf (GnomeDbSelectorPart *part);
static void       gnome_db_selector_meta_fill_tree_store (GnomeDbSelectorPart *part, 
							  GtkTreeStore *store, GtkTreeIter *parent_iter);
static GdaSet    *gnome_db_selector_meta_get_params (GnomeDbSelectorPart *part, GtkTreeStore *store, GtkTreeIter *at_iter);
static GdaSet    *gnome_db_selector_meta_get_data (GnomeDbSelectorPart *part, GtkTreeStore *store, GtkTreeIter *at_iter);
static GObjectClass *parent_class = NULL;

static void meta_store_changed_cb (GdaMetaStore *store, GSList *changes, GnomeDbSelectorMeta *meta);
static void meta_store_reset_cb (GdaMetaStore *store, GnomeDbSelectorMeta *meta);

/*
 * GnomeDbSelectorMeta class implementation
 */

static void
gnome_db_selector_meta_class_init (GnomeDbSelectorMetaClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->dispose = gnome_db_selector_meta_dispose;
	object_class->finalize = gnome_db_selector_meta_finalize;

	/* Properties */
        object_class->set_property = gnome_db_selector_meta_set_property;
        object_class->get_property = gnome_db_selector_meta_get_property;

	g_object_class_install_property (object_class, PROP_META_STORE,
                                         g_param_spec_object ("meta-store",
                                                               _("GdaMetaStore"),
							      NULL, GDA_TYPE_META_STORE,
							      G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY |
							      G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_STMT,
					 g_param_spec_object ("select-stmt",
                                                               _("Select statement"),
							      NULL, GDA_TYPE_STATEMENT,
							      G_PARAM_WRITABLE | G_PARAM_CONSTRUCT_ONLY |
							      G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_PARENT_PIXBUF,
					 g_param_spec_object ("parent-pixbuf",
                                                               _("Parent node's pixbuf"),
							      NULL, GDK_TYPE_PIXBUF,
							      G_PARAM_WRITABLE | G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_PIXBUF,
					 g_param_spec_object ("pixbuf",
                                                               _("Contents' node's pixbuf"),
							      NULL, GDK_TYPE_PIXBUF,
							      G_PARAM_WRITABLE | G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_PARENT_PIX_FILE,
					 g_param_spec_string ("parent-pixbuf-file",
							      _("Parent node's pixbuf file"),
							      NULL, NULL,
							      G_PARAM_WRITABLE));
	g_object_class_install_property (object_class, PROP_PIX_FILE,
					 g_param_spec_string ("pixbuf-file",
							      _("Contents' node's pixbuf file"),
							      NULL, NULL,
							      G_PARAM_WRITABLE));
}

static void
gnome_db_selector_meta_part_iface_init (GnomeDbSelectorPartClass *iface)
{
	iface->set_name = gnome_db_selector_meta_set_name;
	iface->get_name = gnome_db_selector_meta_get_name;
	iface->set_parent_part = gnome_db_selector_meta_set_parent_part;
	iface->get_parent_part = gnome_db_selector_meta_get_parent_part;
	iface->get_pixbuf = gnome_db_selector_meta_get_pixbuf;
	iface->fill_tree_store = gnome_db_selector_meta_fill_tree_store;
	iface->get_params = gnome_db_selector_meta_get_params;
	iface->get_data = gnome_db_selector_meta_get_data;
}

static void
gnome_db_selector_meta_init (GnomeDbSelectorMeta *part, GnomeDbSelectorMetaClass *klass)
{
	part->priv = g_new0 (GnomeDbSelectorMetaPrivate, 1);
	part->priv->name = g_strdup ("META");
	part->priv->store = NULL;
	part->priv->stmt_list = NULL;
	part->priv->params = NULL;
	part->priv->sub_part = NULL;
	part->priv->parent_pixbuf = NULL;
	part->priv->entry_pixbuf = NULL;
	part->priv->parent = NULL;
	part->priv->pix_func = NULL;
	part->priv->row_ref_list = NULL;
}

static void
gnome_db_selector_meta_dispose (GObject *object)
{
	GnomeDbSelectorMeta *part = (GnomeDbSelectorMeta *) object;

	/* free memory */
	if (part->priv->parent)
		g_object_remove_weak_pointer ((GObject*) part->priv->parent, (gpointer*) &(part->priv->parent));

	if (part->priv->store) {
		g_signal_handlers_disconnect_by_func (G_OBJECT (part->priv->store),
						      G_CALLBACK (meta_store_changed_cb), part);
		g_signal_handlers_disconnect_by_func (G_OBJECT (part->priv->store), 
						      G_CALLBACK (meta_store_reset_cb), part);
		g_object_unref (part->priv->store);
		part->priv->store = NULL;
	}
	if (part->priv->stmt_list) {
		g_slist_foreach (part->priv->stmt_list, (GFunc) g_object_unref, NULL);
		g_slist_free (part->priv->stmt_list);
		part->priv->stmt_list = NULL;
	}
	if (part->priv->params) {
		g_object_unref (part->priv->params);
		part->priv->params = NULL;
	}
	
	g_free (part->priv->name);
	if (part->priv->row_ref_list) {
		g_slist_foreach (part->priv->row_ref_list, (GFunc) gtk_tree_row_reference_free, NULL);
		g_slist_free (part->priv->row_ref_list);
		part->priv->row_ref_list = NULL;
	}

	/* chain to parent class */
	parent_class->dispose (object);
}

static void
gnome_db_selector_meta_finalize (GObject *object)
{
	GnomeDbSelectorMeta *part = (GnomeDbSelectorMeta *) object;

	/* free memory */
	g_free (part->priv);
	part->priv = NULL;

	/* chain to parent class */
	parent_class->finalize (object);
}

GType
gnome_db_selector_meta_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0)) {
		static GTypeInfo info = {
			sizeof (GnomeDbSelectorMetaClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_selector_meta_class_init,
			NULL, NULL,
			sizeof (GnomeDbSelectorMeta),
			0,
			(GInstanceInitFunc) gnome_db_selector_meta_init
		};
		
		static const GInterfaceInfo selector_part = {
			(GInterfaceInitFunc) gnome_db_selector_meta_part_iface_init,
			NULL,
			NULL
		};
		
		type = g_type_register_static (G_TYPE_OBJECT, "GnomeDbSelectorMeta", &info, 0);
		g_type_add_interface_static (type, GNOME_DB_TYPE_SELECTOR_PART, &selector_part);
	}

	return type;
}

static void
gnome_db_selector_meta_set_property (GObject *object,
				     guint param_id,
				     const GValue *value,
				     GParamSpec *pspec)
{
        GnomeDbSelectorMeta *part;

        part = GNOME_DB_SELECTOR_META (object);
        if (part->priv) {
                switch (param_id) {
		case PROP_META_STORE:
			part->priv->store = g_value_get_object (value);
			if (part->priv->store) {
				g_object_ref (part->priv->store);
				g_signal_connect (G_OBJECT (part->priv->store), "meta-changed",
						  G_CALLBACK (meta_store_changed_cb), part);
				g_signal_connect (G_OBJECT (part->priv->store), "meta-reset",
						  G_CALLBACK (meta_store_reset_cb), part);
			}
			break;
		case PROP_STMT: {
			GdaStatement *stmt;
			stmt = g_value_get_object (value);
			if (stmt) {
				if ((gda_statement_get_statement_type (stmt) != GDA_SQL_STATEMENT_SELECT) &&
				    (gda_statement_get_statement_type (stmt) != GDA_SQL_STATEMENT_COMPOUND)) {
					g_warning (_("SQL statement is not a SELECT statement"));
				}
				else {
					if (!gda_statement_get_parameters (stmt, &(part->priv->params), NULL)) 
						g_warning (_("Can't get the SELECT statement's parameters"));
					else {
						part->priv->stmt_list = g_slist_append (part->priv->stmt_list, stmt);
						g_object_ref (stmt);
					}
				}
			}
			break;
		}
		case PROP_PARENT_PIXBUF:
			if (part->priv->parent_pixbuf)
				g_object_unref (part->priv->parent_pixbuf);
			part->priv->parent_pixbuf = g_value_get_object (value);
			if (part->priv->parent_pixbuf)
				g_object_ref (part->priv->parent_pixbuf);
			break;
		case PROP_PIXBUF:
			if (part->priv->entry_pixbuf)
				g_object_unref (part->priv->entry_pixbuf);
			part->priv->entry_pixbuf = g_value_get_object (value);
			if (part->priv->entry_pixbuf)
				g_object_ref (part->priv->entry_pixbuf);
			break;
		case PROP_PARENT_PIX_FILE:
			if (part->priv->parent_pixbuf)
				g_object_unref (part->priv->parent_pixbuf);
			part->priv->parent_pixbuf = gdk_pixbuf_new_from_file (g_value_get_string (value), NULL);
			if (!part->priv->parent_pixbuf)
				part->priv->parent_pixbuf = _gnome_db_selector_create_pixbuf 
					(g_value_get_string (value));
			break;
		case PROP_PIX_FILE:
			if (part->priv->entry_pixbuf)
				g_object_unref (part->priv->entry_pixbuf);
			part->priv->entry_pixbuf = gdk_pixbuf_new_from_file (g_value_get_string (value), NULL);
			if (!part->priv->entry_pixbuf)
				part->priv->entry_pixbuf = _gnome_db_selector_create_pixbuf 
					(g_value_get_string (value));
			break;
                }
        }
}

static void
gnome_db_selector_meta_get_property (GObject *object,
				     guint param_id,
				     GValue *value,
				     GParamSpec *pspec)
{
        GnomeDbSelectorMeta *part;

        part = GNOME_DB_SELECTOR_META (object);
        if (part->priv) {
                switch (param_id) {
		case PROP_META_STORE:
			g_value_set_object (value, part->priv->store);
			break;
		case PROP_STMT:
			if (part->priv->stmt_list)
				g_value_set_object (value, (GObject*) part->priv->stmt_list->data);
			else
				g_value_set_object (value, NULL);
			break;
		case PROP_PARENT_PIXBUF:
			g_value_set_object (value, part->priv->parent_pixbuf);
			break;
		case PROP_PIXBUF:
			g_value_set_object (value, part->priv->entry_pixbuf);
			break;
                }
        }
}

static void
gnome_db_selector_meta_set_name (GnomeDbSelectorPart *part, const gchar *name)
{
	GnomeDbSelectorMeta *mpart = (GnomeDbSelectorMeta*) part;
	g_free (mpart->priv->name);
	if (name)
		mpart->priv->name = g_strdup (name);
	else
		mpart->priv->name = NULL;
}

static const gchar *
gnome_db_selector_meta_get_name (GnomeDbSelectorPart *part)
{
	GnomeDbSelectorMeta *mpart = (GnomeDbSelectorMeta*) part;
	return mpart->priv->name;
}

static void
gnome_db_selector_meta_set_parent_part (GnomeDbSelectorPart *part, GnomeDbSelectorPart *parent_part)
{
	GnomeDbSelectorMeta *mpart = (GnomeDbSelectorMeta*) part;
	if (mpart->priv->parent) 
		g_object_remove_weak_pointer ((GObject*) mpart->priv->parent, (gpointer *) &(mpart->priv->parent));
	mpart->priv->parent = parent_part;
	g_object_add_weak_pointer ((GObject*) mpart->priv->parent, (gpointer *) &(mpart->priv->parent));
}

static GnomeDbSelectorPart *
gnome_db_selector_meta_get_parent_part (GnomeDbSelectorPart *part)
{
	return ((GnomeDbSelectorMeta*) part)->priv->parent;
}

static GdkPixbuf *
gnome_db_selector_meta_get_pixbuf (GnomeDbSelectorPart *part)
{
	GnomeDbSelectorMeta *mpart = (GnomeDbSelectorMeta*) part;
	return mpart->priv->parent_pixbuf;
}

static void
gnome_db_selector_meta_fill_tree_store (GnomeDbSelectorPart *part,
					GtkTreeStore *store, GtkTreeIter *parent_iter)
{
	GtkTreeIter iter;
	GnomeDbSelectorMeta *mpart = (GnomeDbSelectorMeta*) part;
	GdaDataModel *model = NULL;
	GdaDataModelIter *model_iter = NULL;
	GnomeDbSelectorPartFillMode mode = GNOME_DB_SELECTOR_PART_FILL_MODE_FIRST_FILL;
	GtkTreeRowReference *row_ref = NULL;

	/* compute a GdaDataModel with the contents to add */
	if (mpart->priv->store && mpart->priv->stmt_list) {
		GdaConnection *cnc = gda_meta_store_get_internal_connection (mpart->priv->store);
		GSList *list;
		for (list = mpart->priv->stmt_list; list; list = list->next) {
			model = gda_connection_statement_execute_select (cnc, GDA_STATEMENT (list->data),
									 mpart->priv->params, NULL);
			if (model) {
				if (gda_data_model_get_n_rows (model) == 0) {
					g_object_unref (model);
					model = NULL;
				}
				else {
					model_iter = gda_data_model_create_iter (model);
					g_object_set_data_full (G_OBJECT (model), "_iter", model_iter, g_object_unref);
					break;
				}
			}
		}
	}

	/* find the first iter below @parent_iter for which GNOME_DB_SELECTOR_PART_PART_COLUMN = @part */
	if (gtk_tree_model_iter_children ((GtkTreeModel*) store, &iter, parent_iter)) {
		do {
			GnomeDbSelectorPart *apart;
			gtk_tree_model_get ((GtkTreeModel*) store, &iter,
					    GNOME_DB_SELECTOR_PART_PART_COLUMN, &apart, -1);
			if (apart) {
				g_object_unref (apart);
				if (apart == part) {
					mode = GNOME_DB_SELECTOR_PART_FILL_MODE_OVW_FIRST;
					break;
				}
			}
		}
		while (gtk_tree_model_iter_next ((GtkTreeModel*) store, &iter));
	}
	
	if (!model) {
		gnome_db_selector_part_obtain_new_store_row (part, &mode, store, &iter, parent_iter);
		if (!row_ref && (mode == GNOME_DB_SELECTOR_PART_FILL_MODE_FIRST_FILL)) {
			GtkTreePath *path;
			path = gtk_tree_model_get_path ((GtkTreeModel*) store, &iter);
			row_ref = gtk_tree_row_reference_new ((GtkTreeModel*) store, path);
			gtk_tree_path_free (path);
			mpart->priv->row_ref_list = g_slist_prepend (mpart->priv->row_ref_list, row_ref);
		}

		gtk_tree_store_set (store, &iter, 
				    GNOME_DB_SELECTOR_PART_LABEL_COLUMN, _("(None)"), 
				    GNOME_DB_SELECTOR_PART_PART_COLUMN, part, -1);
	}
	else {
		gint i, nrows;
		GdkPixbuf *pixbuf = NULL;
		GdaDataHandler *dh = NULL;

		if (mpart->priv->sub_part)
			pixbuf = gnome_db_selector_part_get_pixbuf (mpart->priv->sub_part);
		nrows = gda_data_model_get_n_rows (model);
		for (i = 0; i < nrows; i++) {
			gchar *str;
			const GValue *value;
			GdkPixbuf *funcpixbuf = NULL;
			if (!pixbuf && mpart->priv->pix_func) 
				funcpixbuf = (mpart->priv->pix_func) 
					(gda_data_model_get_value_at (model, mpart->priv->pix_func_col, i, NULL));

			value = gda_data_model_get_value_at (model, 0, i, NULL); 
			if (value && !gda_value_is_null (value)) {
				if (!dh)
					dh = gda_get_default_handler (G_VALUE_TYPE (value));
				str = gda_data_handler_get_str_from_value (dh, value);
			}
			else
				str = g_strdup ("");
			gnome_db_selector_part_obtain_new_store_row (part, &mode, store, &iter, parent_iter);
			if (!row_ref && (mode == GNOME_DB_SELECTOR_PART_FILL_MODE_FIRST_FILL)) {
				GtkTreePath *path;
				path = gtk_tree_model_get_path ((GtkTreeModel*) store, &iter);
				row_ref = gtk_tree_row_reference_new ((GtkTreeModel*) store, path);
				gtk_tree_path_free (path);
				mpart->priv->row_ref_list = g_slist_prepend (mpart->priv->row_ref_list, row_ref);
			}
			gtk_tree_store_set (store, &iter, 
					    GNOME_DB_SELECTOR_PART_PRIVATE_OBJ_COLUMN, model,
					    GNOME_DB_SELECTOR_PART_PRIVATE_INT_COLUMN, i,
					    GNOME_DB_SELECTOR_PART_LABEL_COLUMN, str,
					    GNOME_DB_SELECTOR_PART_PIXBUF_COLUMN, 
					    pixbuf ? pixbuf : (funcpixbuf ? funcpixbuf : mpart->priv->entry_pixbuf), 
					    GNOME_DB_SELECTOR_PART_PART_COLUMN, part, -1);
			g_free (str);
			if (funcpixbuf)
				g_object_unref (funcpixbuf);

			if (mpart->priv->sub_part) 
				gnome_db_selector_part_fill_tree_store (mpart->priv->sub_part, store, &iter);
		}
		if (pixbuf)
			g_object_unref (pixbuf);
		g_object_unref (model);
	}

	if ((mode != GNOME_DB_SELECTOR_PART_FILL_MODE_FIRST_FILL) && 
	    gtk_tree_model_iter_next ((GtkTreeModel*) store, &iter)) {
		/* remove remaining nodes */
		for (;;) {
			GnomeDbSelectorPart *apart;
			gtk_tree_model_get ((GtkTreeModel*) store, &iter,
					    GNOME_DB_SELECTOR_PART_PART_COLUMN, &apart, -1);
			if (apart) {
				g_object_unref (apart);
				if (apart == part) {
					if (!gtk_tree_store_remove (store, &iter))
						/* iter is now invalid => end of job */
						break;
				}
				else
					break;
			}
		}
	}
}

static GdaSet *
gnome_db_selector_meta_get_params (GnomeDbSelectorPart *part, GtkTreeStore *store, GtkTreeIter *at_iter)
{
	GnomeDbSelectorMeta *mpart = (GnomeDbSelectorMeta*) part;
	return mpart->priv->params;
}

static GdaSet *
gnome_db_selector_meta_get_data (GnomeDbSelectorPart *part, GtkTreeStore *store, GtkTreeIter *at_iter)
{
	if (store && at_iter) {
		gint row;
		GdaDataModel *model;
		
		gtk_tree_model_get (GTK_TREE_MODEL (store), at_iter, 
				    GNOME_DB_SELECTOR_PART_PRIVATE_OBJ_COLUMN, &model, 
				    GNOME_DB_SELECTOR_PART_PRIVATE_INT_COLUMN, &row, -1);
		if (model) {
			GdaDataModelIter *iter;
			iter = g_object_get_data (G_OBJECT (model), "_iter");
			if (gda_data_model_iter_move_to_row (iter, row))
				return (GdaSet*) iter;
		}
	}

	return NULL;
}

/**
 * gnome_db_selector_meta_new
 * @store: a #GdaMetaStore object
 * @sql: the SELECT statement to execute
 *
 * Returns: a new #GnomeDbSelectorPart object, or %NULL if @sql is not correct
 */
GnomeDbSelectorPart *
gnome_db_selector_meta_new_with_sql (GdaMetaStore *store, const gchar *sql, GError **error)
{
	GnomeDbSelectorMeta *part;
	GdaStatement *stmt;
	GdaSqlParser *parser;
	GdaConnection *cnc;

	cnc = gda_meta_store_get_internal_connection (store);
	parser = gda_connection_create_parser (cnc);
	if (!parser)
		parser = gda_sql_parser_new ();
	stmt = gda_sql_parser_parse_string (parser, sql, NULL, error);
	g_object_unref (parser);
	if (!stmt)
		return NULL;
	if ((gda_statement_get_statement_type (stmt) != GDA_SQL_STATEMENT_SELECT) &&
	    (gda_statement_get_statement_type (stmt) != GDA_SQL_STATEMENT_COMPOUND)) {
		g_object_unref (stmt);
		g_warning (_("SQL statement is not a SELECT statement"));
		return NULL;
	}
	part = (GnomeDbSelectorMeta*) g_object_new (GNOME_DB_TYPE_SELECTOR_META, "meta-store", store,
						    "select-stmt", stmt, NULL);
	g_object_unref (stmt);

	return GNOME_DB_SELECTOR_PART (part);
}

/**
 * gnome_db_selector_meta_add_sql
 *
 * 
 */
gboolean
gnome_db_selector_meta_add_sql (GnomeDbSelectorMeta *part, const gchar *sql, GError **error)
{
	GdaStatement *stmt;
	GdaSqlParser *parser;
	GdaConnection *cnc;
	GdaSet *params;

	g_return_val_if_fail (GNOME_DB_IS_SELECTOR_META (part), FALSE);
	g_return_val_if_fail (sql, FALSE);
	g_return_val_if_fail (part->priv->store, FALSE);

	cnc = gda_meta_store_get_internal_connection (part->priv->store);
	parser = gda_connection_create_parser (cnc);
	if (!parser)
		parser = gda_sql_parser_new ();
	stmt = gda_sql_parser_parse_string (parser, sql, NULL, error);
	g_object_unref (parser);
	if (!stmt)
		return FALSE;
	if ((gda_statement_get_statement_type (stmt) != GDA_SQL_STATEMENT_SELECT) &&
	    (gda_statement_get_statement_type (stmt) != GDA_SQL_STATEMENT_COMPOUND)) {
		g_object_unref (stmt);
		g_set_error (error, 0, 0,
			     _("SQL statement is not a SELECT statement"));
		return FALSE;
	}

	if (!gda_statement_get_parameters (stmt, &params, error)) {
		g_object_unref (stmt);
		return FALSE;
	}

	part->priv->stmt_list = g_slist_append (part->priv->stmt_list, stmt);
	if (params) {
		if (part->priv->params) {
			gda_set_merge_with_set (part->priv->params, params);
			g_object_unref (params);
		}
		else
			part->priv->params = params;	
	}
	return TRUE;
}

/**
 * gnome_db_selector_meta_set_sub_part
 * @part:
 * @subpart:
 *
 * Attach a #GnomeDbSelectorPart as a sub part of @part
 */
void
gnome_db_selector_meta_set_sub_part (GnomeDbSelectorMeta *part, GnomeDbSelectorPart *subpart)
{
	g_return_if_fail (GNOME_DB_IS_SELECTOR_META (part));
	g_return_if_fail (GNOME_DB_IS_SELECTOR_PART (subpart));

	if (part->priv->sub_part)
		g_object_unref (part->priv->sub_part);
	part->priv->sub_part = subpart;
	g_object_ref (subpart);

	gnome_db_selector_part_set_parent_part (subpart, (GnomeDbSelectorPart*) part);
}

/**
 * gnome_db_selector_meta_set_pixbuf_func
 * @part: a #GnomeDbSelectorMeta object
 * @func: a #GnomeDbSelectorMetaPixbufFunc function, or %NULL
 * @column: the column number
 *
 *
 * Sets the function which is used to compute each content node's associated pixbuf.
 */
void
gnome_db_selector_meta_set_pixbuf_func (GnomeDbSelectorMeta *part, gint column,
					GnomeDbSelectorMetaPixbufFunc func)
{
	g_return_if_fail (GNOME_DB_IS_SELECTOR_META (part));
	g_return_if_fail (column >= 0);
	
	part->priv->pix_func = func;
	part->priv->pix_func_col = column;
}

static void
meta_store_changed_cb (GdaMetaStore *store, GSList *changes, GnomeDbSelectorMeta *meta)
{
	meta_store_reset_cb (store, meta);
}

static void
meta_store_reset_cb (GdaMetaStore *store, GnomeDbSelectorMeta *meta)
{
	GtkTreePath *path;
	GtkTreeIter iter, parent;
	GtkTreeModel *treemodel;
	GSList *list;

	for (list = meta->priv->row_ref_list; list; list = list->next) {
		GtkTreeRowReference *row_ref = (GtkTreeRowReference *) list->data;
		g_assert (gtk_tree_row_reference_valid (row_ref));
	
		treemodel = gtk_tree_row_reference_get_model (row_ref);
		path = gtk_tree_row_reference_get_path (row_ref);
		g_assert (gtk_tree_model_get_iter (treemodel, &iter, path));
		gtk_tree_path_free (path);
		
		if (gtk_tree_model_iter_parent (treemodel, &parent, &iter))
			gnome_db_selector_meta_fill_tree_store ((GnomeDbSelectorPart*) meta, 
								(GtkTreeStore*) treemodel, &parent);
		else
			gnome_db_selector_meta_fill_tree_store ((GnomeDbSelectorPart*) meta, 
								(GtkTreeStore*) treemodel, NULL);
	}
}
