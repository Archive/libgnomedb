-- MySQL dump 10.10
--
-- Host: localhost    Database: sales
-- ------------------------------------------------------
-- Server version	5.0.21-Debian_3-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `customers`
--

DROP TABLE IF EXISTS `customers`;
CREATE TABLE `customers` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(35) NOT NULL default '',
  `default_served_by` int(11) default NULL,
  `country` varchar(20) default NULL,
  `city` varchar(30) default NULL,
  PRIMARY KEY  (`id`),
  KEY `default_served_by` (`default_served_by`),
  KEY `country` (`country`,`city`),
  CONSTRAINT `customers_ibfk_1` FOREIGN KEY (`default_served_by`) REFERENCES `salesrep` (`id`),
  CONSTRAINT `customers_ibfk_2` FOREIGN KEY (`country`, `city`) REFERENCES `locations` (`country`, `city`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customers`
--


/*!40000 ALTER TABLE `customers` DISABLE KEYS */;
LOCK TABLES `customers` WRITE;
INSERT INTO `customers` VALUES (2,'Ed Lamton',4,'SP','MDR'),(3,'Lew Bonito',1,'FR','TLS'),(4,'Mark Lawrencep',NULL,'SP','MDR'),(9,'Greg Popoff',2,'SP','MDR'),(10,'Vladimir Zirkov',4,NULL,NULL);
UNLOCK TABLES;
/*!40000 ALTER TABLE `customers` ENABLE KEYS */;

--
-- Table structure for table `locations`
--

DROP TABLE IF EXISTS `locations`;
CREATE TABLE `locations` (
  `country` varchar(20) NOT NULL default '',
  `city` varchar(30) NOT NULL default '',
  `shortcut` varchar(50) NOT NULL default '',
  PRIMARY KEY  (`country`,`city`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `locations`
--


/*!40000 ALTER TABLE `locations` DISABLE KEYS */;
LOCK TABLES `locations` WRITE;
INSERT INTO `locations` VALUES ('FR','TLS','Toulouse, France'),('SP','MDR','Madrid, Spain');
UNLOCK TABLES;
/*!40000 ALTER TABLE `locations` ENABLE KEYS */;

--
-- Table structure for table `order_contents`
--

DROP TABLE IF EXISTS `order_contents`;
CREATE TABLE `order_contents` (
  `order_id` int(11) NOT NULL auto_increment,
  `product_ref` varchar(15) NOT NULL default '',
  `quantity` int(11) NOT NULL default '1',
  `discount` double NOT NULL default '0',
  PRIMARY KEY  (`order_id`),
  KEY `product_ref` (`product_ref`),
  CONSTRAINT `order_contents_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  CONSTRAINT `order_contents_ibfk_2` FOREIGN KEY (`product_ref`) REFERENCES `products` (`ref`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_contents`
--


/*!40000 ALTER TABLE `order_contents` DISABLE KEYS */;
LOCK TABLES `order_contents` WRITE;
INSERT INTO `order_contents` VALUES (1,'SC1',1,2),(2,'MOUSE1',1,0),(3,'SC3',1,0),(5,'SC2',1,0),(6,'SC2',3,10),(7,'MOUSE2',6,0);
UNLOCK TABLES;
/*!40000 ALTER TABLE `order_contents` ENABLE KEYS */;

--
-- Table structure for table `orders`
--

DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL auto_increment,
  `customer` int(11) NOT NULL default '0',
  `creation_date` date NOT NULL default '0000-00-00',
  `delivery_before` date default NULL,
  `delivery_date` date default NULL,
  PRIMARY KEY  (`id`),
  KEY `customer` (`customer`),
  CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`customer`) REFERENCES `customers` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--


/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
LOCK TABLES `orders` WRITE;
INSERT INTO `orders` VALUES (1,3,'2003-06-28',NULL,'2004-02-01'),(2,3,'2004-02-02',NULL,'0000-00-00'),(3,4,'2004-02-02',NULL,NULL),(4,4,'2004-02-02',NULL,NULL),(5,3,'2004-02-02',NULL,NULL),(6,3,'2004-01-01','2004-03-31',NULL),(7,9,'2004-02-02',NULL,NULL);
UNLOCK TABLES;
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
  `ref` varchar(30) NOT NULL default '',
  `name` varchar(20) NOT NULL default '',
  `price` double default NULL,
  `wh_stored` int(11) default NULL,
  PRIMARY KEY  (`ref`),
  KEY `wh_stored` (`wh_stored`),
  CONSTRAINT `products_ibfk_1` FOREIGN KEY (`wh_stored`) REFERENCES `warehouses` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products`
--


/*!40000 ALTER TABLE `products` DISABLE KEYS */;
LOCK TABLES `products` WRITE;
INSERT INTO `products` VALUES ('MOUSE1','USB Mouse',40,NULL),('MOUSE2','Wheel Mouse',42,NULL),('SC1','Screen 17\"',180,NULL),('SC2','Screen 19\"',220,NULL),('SC3','Flat Screen 15\"',200,NULL),('SC4','Flat Screen 19\"',700,NULL);
UNLOCK TABLES;
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL auto_increment,
  `role` varchar(30) NOT NULL default '',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `roles`
--


/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
LOCK TABLES `roles` WRITE;
INSERT INTO `roles` VALUES (1,'Manager'),(2,'Superviser'),(3,'The boss');
UNLOCK TABLES;
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;

--
-- Table structure for table `sales_orga`
--

DROP TABLE IF EXISTS `sales_orga`;
CREATE TABLE `sales_orga` (
  `id_salesrep` int(11) NOT NULL default '0',
  `id_role` int(11) NOT NULL default '0',
  `note` text,
  KEY `id_salesrep` (`id_salesrep`),
  KEY `id_role` (`id_role`),
  CONSTRAINT `sales_orga_ibfk_1` FOREIGN KEY (`id_salesrep`) REFERENCES `salesrep` (`id`),
  CONSTRAINT `sales_orga_ibfk_2` FOREIGN KEY (`id_role`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sales_orga`
--


/*!40000 ALTER TABLE `sales_orga` DISABLE KEYS */;
LOCK TABLES `sales_orga` WRITE;
INSERT INTO `sales_orga` VALUES (4,3,NULL),(1,1,NULL),(1,2,NULL);
UNLOCK TABLES;
/*!40000 ALTER TABLE `sales_orga` ENABLE KEYS */;

--
-- Table structure for table `salesrep`
--

DROP TABLE IF EXISTS `salesrep`;
CREATE TABLE `salesrep` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(30) NOT NULL default '',
  `year_salary` double NOT NULL default '0',
  `date_empl` date NOT NULL default '0000-00-00',
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `salesrep`
--


/*!40000 ALTER TABLE `salesrep` DISABLE KEYS */;
LOCK TABLES `salesrep` WRITE;
INSERT INTO `salesrep` VALUES (1,'Chris Johnson',20,'2002-10-03'),(2,'Marc Swayn',25,'2003-05-08'),(3,'John Tremor',23,'2003-09-18'),(4,'Tom Gregor',28,'2001-12-12');
UNLOCK TABLES;
/*!40000 ALTER TABLE `salesrep` ENABLE KEYS */;

--
-- Table structure for table `warehouses`
--

DROP TABLE IF EXISTS `warehouses`;
CREATE TABLE `warehouses` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(30) NOT NULL default '',
  `country` varchar(20) default NULL,
  `city` varchar(30) default NULL,
  PRIMARY KEY  (`id`),
  KEY `country` (`country`,`city`),
  CONSTRAINT `warehouses_ibfk_1` FOREIGN KEY (`country`, `city`) REFERENCES `locations` (`country`, `city`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `warehouses`
--


/*!40000 ALTER TABLE `warehouses` DISABLE KEYS */;
LOCK TABLES `warehouses` WRITE;
UNLOCK TABLES;
/*!40000 ALTER TABLE `warehouses` ENABLE KEYS */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

