-- MySQL dump 8.23
--
-- Host: localhost    Database: sales
---------------------------------------------------------
-- Server version	3.23.58

--
-- Table structure for table `customers`
--

CREATE TABLE customers (
  id int(11) NOT NULL auto_increment,
  name varchar(35) NOT NULL default '',
  default_served_by int(11) default NULL,
  country varchar(20) default NULL,
  city varchar(30) default NULL,
  PRIMARY KEY  (id)
) TYPE=MyISAM;

--
-- Dumping data for table `customers`
--



--
-- Table structure for table `locations`
--

CREATE TABLE locations (
  country varchar(20) NOT NULL default '',
  city varchar(30) NOT NULL default '',
  shortcut varchar(50) NOT NULL default '',
  PRIMARY KEY  (country,city)
) TYPE=MyISAM;

--
-- Dumping data for table `locations`
--



--
-- Table structure for table `order_contents`
--

CREATE TABLE order_contents (
  order_id int(11) NOT NULL auto_increment,
  product_ref varchar(15) NOT NULL default '',
  quantity int(11) NOT NULL default '1',
  discount double NOT NULL default '0',
  PRIMARY KEY  (order_id)
) TYPE=MyISAM;

--
-- Dumping data for table `order_contents`
--



--
-- Table structure for table `orders`
--

CREATE TABLE orders (
  id int(11) NOT NULL auto_increment,
  customer int(11) NOT NULL default '0',
  creation_date date NOT NULL default '0000-00-00',
  delivery_before date default NULL,
  delivery_date date default NULL,
  PRIMARY KEY  (id)
) TYPE=MyISAM;

--
-- Dumping data for table `orders`
--



--
-- Table structure for table `products`
--

CREATE TABLE products (
  ref varchar(30) NOT NULL default '',
  name varchar(20) NOT NULL default '',
  price double default NULL,
  wh_stored int(11) default NULL,
  PRIMARY KEY  (ref)
) TYPE=MyISAM;

--
-- Dumping data for table `products`
--



--
-- Table structure for table `roles`
--

CREATE TABLE roles (
  id int(11) NOT NULL auto_increment,
  role varchar(30) NOT NULL default '',
  PRIMARY KEY  (id)
) TYPE=MyISAM;

--
-- Dumping data for table `roles`
--



--
-- Table structure for table `sales_orga`
--

CREATE TABLE sales_orga (
  id_salesrep int(11) NOT NULL default '0',
  id_role int(11) NOT NULL default '0',
  note text
) TYPE=MyISAM;

--
-- Dumping data for table `sales_orga`
--



--
-- Table structure for table `salesrep`
--

CREATE TABLE salesrep (
  id int(11) NOT NULL auto_increment,
  name varchar(30) NOT NULL default '',
  year_salary double NOT NULL default '0',
  date_empl date NOT NULL default '0000-00-00',
  PRIMARY KEY  (id)
) TYPE=MyISAM;

--
-- Dumping data for table `salesrep`
--



--
-- Table structure for table `warehouses`
--

CREATE TABLE warehouses (
  id int(11) NOT NULL auto_increment,
  name varchar(30) NOT NULL default '',
  country varchar(20) default NULL,
  city varchar(30) default NULL,
  PRIMARY KEY  (id)
) TYPE=MyISAM;

--
-- Dumping data for table `warehouses`
--



