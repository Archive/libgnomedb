#include <stdio.h>
#include <string.h>
#include "../libgnomedb/libgnomedb.h"

#define MAKE_DUMPS 1

#define STEP_SEPARATOR "------------------"
#define xmlfile "RS_tests.xml"


const gchar *compare_with_expected (xmlNodePtr case_node, gchar *query_type, gchar *sql);

gint main (int argc, char **argv) {
	GdaDict *dict = NULL;
	GError *error = NULL;
	xmlDocPtr doc;
	xmlNodePtr node, subnode;
	FILE *out = stdout;
	gboolean in_file = FALSE;

	gtk_init (&argc, &argv);

	g_print ("# This test creates\n");
	g_print ("# GnomeDbResultSet objects from queries as statements\n\n");


	if (! g_file_test (xmlfile, G_FILE_TEST_EXISTS)) {
		g_print ("cant' find file '%s'\n", xmlfile);
		exit (1);
	}

	doc = xmlParseFile (xmlfile);
	if (!doc) {
		g_print ("Cant' load XML file '%s'\n", xmlfile);
		exit (1);
	}

	node = xmlDocGetRootElement (doc);
	if (strcmp (node->name, "test_scenario")) {
		g_print ("XML file top node is not <test_scenario>\n");
		exit (1);
	}

	subnode = node->children;
	while (subnode) {
		/* new dictionary */
		if (!strcmp (subnode->name, "dictionary")) {
			gchar *filename = xmlGetProp (subnode, "name");
			if (dict) {
				g_object_unref (G_OBJECT (dict));
				dict = NULL;
			}

			if (filename) {
				dict = gda_dict_new ();
				g_print ("Loading dictionary %s\n", filename);
				if (!gda_dict_load_xml_file (dict, filename, &error)) {
					g_print ("Error occurred:\n\t%s\n", error->message);
					g_error_free (error);
					exit (1);
				}
				g_free (filename);
			}
		}
		
		/* new output file */
		if (!strcmp (subnode->name, "output_file")) {
			gchar *filename = xmlGetProp (subnode, "name");
			if (in_file) {
				/* HTML end */
				g_fprintf (out, "</table>\n");
				g_fprintf (out, "</body>\n");
				g_fprintf (out, "</html>\n");
				fclose (out);
			}
			out = stdout;

			if (filename) {
				g_print ("Writing to %s\n", filename);
				out = fopen (filename, "w");
				if (!out) {
					g_print ("Can't open '%s' to write\n", filename);
					exit (1);
				}
				g_free (filename);
			}

			/* HTML init */
			g_fprintf (out, "<html>\n");
			g_fprintf (out, "<body>\n");
			g_fprintf (out, "<h2>Note:</h2>"
				   "This test reads the " xmlfile " file and for each SELECT query listed in the file "
				   "creates a GdaQuery object, parses the SQL statement a first time (from an empty query), "
				   "and parses a second time, and makes the comparison.\n");
			g_fprintf (out, "<table cellspacing=\"3\" cellpadding=\"3\" border=\"1\" width=\"100%\">\n");

			in_file = TRUE;
		}
		
		/* new test group */
		if (!strcmp (subnode->name, "test_group")) {
			xmlNodePtr test = subnode->children;
			gchar *descr = xmlGetProp (subnode, "descr");

			g_fprintf (out, "<tr><th colspan=\"4\" bgcolor=\"green4\">%s</th></tr>\n", descr);
			g_free (descr);

			while (test) {
				if (!strcmp (test->name, "test")) {
					gchar *sql = xmlGetProp (test, "query");
					GdaQuery *query;

					query = GDA_QUERY (gda_query_new_from_sql (dict, sql, NULL));
					g_fprintf (out, "<tr><th>Query</th><th>Modified target</th></tr>\n");
					if (gda_query_get_query_type (query) == GDA_QUERY_TYPE_SELECT) {
						xmlNodePtr cases = test->children;
						while (cases) {
							if (strcmp (cases->name, "case")) {
								cases = cases->next;
								continue;
							}
							gchar *sql_target = xmlGetProp (cases, "target");
							GdaQueryTarget *target;

							target = gda_query_get_target_by_alias (query, sql_target);
							if (target) {
								gchar *to_sql;
								GnomeDbResultSet *rs;
								GdaQuery *iquery;
								guint opt = 0 & GDA_RENDERER_EXTRA_VAL_ATTRS;

								g_fprintf (out, "<tr><td>%s</td><td>%s</td></tr>", 
									   sql, sql_target);

								rs = GNOME_DB_RESULT_SET (gnome_db_result_set_new (query, target));
#ifdef debug
								gda_object_dump (GDA_OBJECT (rs), 0);
#endif

								g_object_get (G_OBJECT (rs), "query_select", &iquery, NULL);
								to_sql = gda_renderer_render_as_sql (GDA_RENDERER (iquery),
												     NULL, NULL, opt, NULL);
								
								g_fprintf (out, "<tr><td%s>%s</td></tr>", 
									   compare_with_expected (cases, "SEL", to_sql),
									   to_sql);
								g_free (to_sql);

								g_object_get (G_OBJECT (rs), "query_update", &iquery, NULL);
								to_sql = gda_renderer_render_as_sql (GDA_RENDERER (iquery),
												     NULL, NULL, opt, NULL);
								g_fprintf (out, "<tr><td%s>%s</td></tr>", 
									   compare_with_expected (cases, "UPD", to_sql),
									   to_sql);
								g_free (to_sql);

								g_object_get (G_OBJECT (rs), "query_delete", &iquery, NULL);
								to_sql = gda_renderer_render_as_sql (GDA_RENDERER (iquery),
												     NULL, NULL, opt, NULL);
								g_fprintf (out, "<tr><td%s>%s</td></tr>", 
									   compare_with_expected (cases, "DEL", to_sql),
									   to_sql);
								g_free (to_sql);

								g_object_get (G_OBJECT (rs), "query_insert", &iquery, NULL);
								to_sql = gda_renderer_render_as_sql (GDA_RENDERER (iquery),
												     NULL, NULL, opt, NULL);
								g_fprintf (out, "<tr><td%s>%s</td></tr>", 
									   compare_with_expected (cases, "INS", to_sql),
									   to_sql);
								g_free (to_sql);

								g_object_unref (rs);
							}
							else {
								
							}
							cases = cases->next;
						}
					}
					else {
						
					}
					g_object_unref (query);
					g_free (sql);
				}
				test = test->next;
			}
		}
		subnode = subnode->next;
	}
	xmlFreeDoc (doc);

	if (in_file) {
		/* HTML end */
		g_fprintf (out, "</table>\n");
		g_fprintf (out, "</body>\n");
		g_fprintf (out, "</html>\n");
		fclose (out);
	}

	if (dict)
		g_object_unref (G_OBJECT (dict));
	return 0;
}

const gchar *
compare_with_expected (xmlNodePtr case_node, gchar *query_type, gchar *sql)
{
	gboolean found = FALSE;
	gchar *retval;
	xmlNodePtr node = case_node->children;

	while (node && !found) {
		if (!strcmp (node->name, "result")) {
			gchar *type = xmlGetProp (node, "type");
			
			if (type && !strcmp (type, query_type)) {
				gchar *rsql = xmlGetProp (node, "sql");
				found = TRUE;
				
				retval = !strcmp (rsql, sql) ? "" : " bgcolor=\"orange\"";
				g_free (rsql);
			}
			g_free (type);
		}
		node = node->next;
	}

	if (!found)
		retval = " bgcolor=\"yellow\"";

	return retval;
}

