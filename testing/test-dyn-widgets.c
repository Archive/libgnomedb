#include <glib/gi18n-lib.h>
#include <libgnomedb/libgnomedb.h>
#include <libgnomedb-extra/libgnomedb-extra.h>
#include <gtk/gtk.h>
#include <unistd.h>
#include <string.h>
#ifdef HAVE_LIBGNOMECANVAS_NO
#include <libgnomedb-graph/libgnomedb-graph.h>
#endif

#define SQL_QUERY_ROWS "SELECT c.name, o.creation_date FROM orders o INNER JOIN customers c ON (c.id=o.customer) ORDER BY c.name, o.creation_date"
#define SQL_QUERY_COLS "SELECT p.name, p.price FROM products p ORDER BY p.name, p.price"

/* options */
gint test_number = 0;
gboolean list_tests = FALSE;

static GOptionEntry entries[] = {
	{ "test-number", 't', 0, G_OPTION_ARG_INT, &test_number, "Test number to run", "test number"},
	{ "list", 'l', 0, G_OPTION_ARG_NONE, &list_tests, "List tests", NULL},
	{ NULL }
};

/* 
 * Global configuration 
 */ 
typedef struct {
	/* General section */
	GdaConnection *cnc;
	gchar         *filename;

	GtkWidget     *mainwin;
	GtkWidget     *tests_nb;
	GtkWidget     *cnc_status;

	GSList        *tests; /* list of ATest structures */

	/* Menu */
	GtkWidget     *menu_save;
	GtkWidget     *menu_db_selectds;
	GtkWidget     *menu_db_action;

	GtkWidget     *selector;
	GtkWidget     *del_obj_button;
	GtkWidget     *del_test_button;
	GtkWidget     *nb;
} MainConfig;

gboolean use_local_db = FALSE;

/*
 * Structure for each test type
 */
typedef struct _ATest {
	gchar        *test_name;
	gchar        *test_descr;
	GtkWidget  *(*make_test_widget)     (MainConfig *config);
	void        (*clean_data)           (MainConfig *config, GtkWidget *test_widget);
	gboolean    (*signal_conn_changed)  (MainConfig *config, GtkWidget *test_widget);
} ATest;


/*
 * Functions for all tests
 */
static void       build_tests (MainConfig *config);
static void       signal_tests(MainConfig *config);

/*
 * Utility functions
 */
static GdaDataModel *get_data_model_from_statement (MainConfig *config, GdaStatement *stmt, const gchar *ident);
static void          param_list_param_changed_cb (GdaSet *paramlist, GdaHolder *param, gchar *prefix);

/*
 * list of tests' functions
 */
static GtkWidget *editor_test_make   (MainConfig *config);

static GtkWidget *combo_test_make (MainConfig *config);
static void       combo_test_clean (MainConfig *config, GtkWidget *test_widget);
static gboolean   combo_test_signal (MainConfig *config, GtkWidget *test_widget);

static GtkWidget *console_test_make   (MainConfig *config);

static GtkWidget *xml_spec_test_make   (MainConfig *config);
static void       xml_spec_test_clean  (MainConfig *config, GtkWidget *test_widget);

static GtkWidget *import_test_make   (MainConfig *config);

static GtkWidget *transaction_status_test_make (MainConfig *config);
static gboolean   transaction_status_test_signal (MainConfig *config, GtkWidget *test_widget);

static GtkWidget *format_entry_test_make   (MainConfig *config);

static GtkWidget *dsn_selector_test_make   (MainConfig *config);
static GtkWidget *dsn_editor_test_make   (MainConfig *config);
static GtkWidget *dsn_assistant_test_make   (MainConfig *config);

static GtkWidget *provider_selector_test_make   (MainConfig *config);
static GtkWidget *provider_spec_editor_test_make   (MainConfig *config);
static GtkWidget *provider_auth_editor_test_make   (MainConfig *config);

static GtkWidget *connection_properties_test_make (MainConfig *config);


/*
 * tests decalaration
 */
static void
build_tests (MainConfig *config)
{
	GSList *list = NULL;
	ATest *test;

	/* XML GdaSet test */
	test = g_new0 (ATest, 1);
	test->test_name = "Basic form & XML parameters spec.";
	test->test_descr = "";
	test->make_test_widget = xml_spec_test_make;
	test->clean_data = xml_spec_test_clean;
	test->signal_conn_changed = NULL;
	list = g_slist_append (list, test);

	/* GnomeDbEditor test */
	test = g_new0 (ATest, 1);
	test->test_name = "GnomeDbEditor";
	test->test_descr = "";
	test->make_test_widget = editor_test_make;
	test->clean_data = NULL;
	test->signal_conn_changed = NULL;
	list = g_slist_append (list, test);

	/* GnomeDbCombo test */
	test = g_new0 (ATest, 1);
	test->test_name = "Combo";
	test->test_descr = "Requires a selected table or SELECT query, and an opened connection";
	test->make_test_widget = combo_test_make;
	test->clean_data = combo_test_clean;
	test->signal_conn_changed = combo_test_signal;
	list = g_slist_append (list, test);

	/* GnomeDbSqlConsole test */
	test = g_new0 (ATest, 1);
	test->test_name = "SQL Console";
	test->test_descr = "";
	test->make_test_widget = console_test_make;
	test->clean_data = NULL;
	test->signal_conn_changed = NULL;
	list = g_slist_append (list, test);

	/* GnomeDbDataImport test */
	test = g_new0 (ATest, 1);
	test->test_name = "Data import test";
	test->test_descr = "";
	test->make_test_widget = import_test_make;
	test->clean_data = NULL;
	test->signal_conn_changed = NULL;
	list = g_slist_append (list, test);

	/* GnomeDbTransactionStatus test */
	test = g_new0 (ATest, 1);
	test->test_name = "Transaction status";
	test->test_descr = "";
	test->make_test_widget = transaction_status_test_make;
	test->clean_data = NULL;
	test->signal_conn_changed = transaction_status_test_signal;
	list = g_slist_append (list, test);

	/* GnomeDbFormatEntry test */
	test = g_new0 (ATest, 1);
	test->test_name = "Formatted entry test";
	test->test_descr = "";
	test->make_test_widget = format_entry_test_make;
	test->clean_data = NULL;
	test->signal_conn_changed = NULL;
	list = g_slist_append (list, test);

	/* GnomeDbDsnSelector test */
	test = g_new0 (ATest, 1);
	test->test_name = "GnomeDbDsnSelector";
	test->test_descr = "";
	test->make_test_widget = dsn_selector_test_make;
	test->clean_data = NULL;
	test->signal_conn_changed = NULL;
	list = g_slist_append (list, test);

	/* GnomeDbDsnEditor test */
	test = g_new0 (ATest, 1);
	test->test_name = "GnomeDbDsnEditor";
	test->test_descr = "";
	test->make_test_widget = dsn_editor_test_make;
	test->clean_data = NULL;
	test->signal_conn_changed = NULL;
	list = g_slist_append (list, test);

	/* GnomeDbDsnAssistant test */
	test = g_new0 (ATest, 1);
	test->test_name = "GnomeDbDsnAssistant";
	test->test_descr = "";
	test->make_test_widget = dsn_assistant_test_make;
	test->clean_data = NULL;
	test->signal_conn_changed = NULL;
	list = g_slist_append (list, test);

	/* GnomeDbProviderSelector test */
	test = g_new0 (ATest, 1);
	test->test_name = "GnomeDbProviderSelector";
	test->test_descr = "";
	test->make_test_widget = provider_selector_test_make;
	test->clean_data = NULL;
	test->signal_conn_changed = NULL;
	list = g_slist_append (list, test);

	/* GnomeDbProviderSpecEditor test */
	test = g_new0 (ATest, 1);
	test->test_name = "GnomeDbProviderSpecEditor";
	test->test_descr = "";
	test->make_test_widget = provider_spec_editor_test_make;
	test->clean_data = NULL;
	test->signal_conn_changed = NULL;
	list = g_slist_append (list, test);

	/* GnomeDbProviderAuthEditor test */
	test = g_new0 (ATest, 1);
	test->test_name = "GnomeDbProviderAuthEditor";
	test->test_descr = "";
	test->make_test_widget = provider_auth_editor_test_make;
	test->clean_data = NULL;
	test->signal_conn_changed = NULL;
	list = g_slist_append (list, test);

	/* GnomeDbConnectionProperties test */
	test = g_new0 (ATest, 1);
	test->test_name = "GnomeDbConnectionProperties";
	test->test_descr = "";
	test->make_test_widget = connection_properties_test_make;
	test->clean_data = NULL;
	test->signal_conn_changed = NULL;
	list = g_slist_append (list, test);

	config->tests = list;
}

static void
signal_tests (MainConfig *config)
{
	gint nb_pages, i;

	nb_pages = gtk_notebook_get_n_pages (GTK_NOTEBOOK (config->tests_nb));
	for (i=0; i<nb_pages; i++) {
		GtkWidget *page = gtk_notebook_get_nth_page (GTK_NOTEBOOK (config->tests_nb), i);
		ATest *test = g_object_get_data (G_OBJECT (page), "atest");
		if (test->signal_conn_changed)
			(test->signal_conn_changed) (config, page);
	}
}


/*
 * MAIN
 */
static void add_test_page (MainConfig *config, ATest *test);
static void open_connection (MainConfig *config, const gchar *string);
static void destroy (GtkWidget *widget, gpointer data);
static gboolean delete_event (GtkWidget *widget, GdkEvent  *event, gpointer data);
static GtkWidget *build_menu (MainConfig *config, GtkWidget *mainwin);
static GtkWidget *build_page (MainConfig *config);
int 
main (int argc, char **argv)
{
	GtkWidget *mainwin, *vbox, *menu, *page;
	MainConfig *config;
	GError *error = NULL;	
	GOptionContext *context;
	
	/* Initialize i18n support */
	gtk_set_locale ();

	/* command line parsing */
	context = g_option_context_new ("[DSN or connection string] - GnomeDb widgets testing");
	g_option_context_add_main_entries (context, entries, GETTEXT_PACKAGE);
	if (!g_option_context_parse (context, &argc, &argv, &error)) {
		g_warning ("Can't parse arguments: %s", error->message);
		exit (1);
	}
	g_option_context_free (context);
	
	/* Initialize the widget set */
	gnome_db_init ();
	gtk_init (&argc, &argv);

	/* Test Configuration */
	config = g_new0 (MainConfig, 1);
	build_tests (config);

	if (list_tests) {
		GSList *list = config->tests;
		gint i = 0;

		while (list) {
			ATest *test = (ATest *)(list->data);
			g_print ("%02d - %s (%s)\n", i++, test->test_name, test->test_descr);
			list = g_slist_next (list);
		}
		exit (0);
	}
	
	/* Create the main window */
	mainwin = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_container_set_border_width (GTK_CONTAINER (mainwin), 0);
	g_signal_connect (G_OBJECT (mainwin), "delete_event",
			  G_CALLBACK (delete_event), NULL);
	g_signal_connect (G_OBJECT (mainwin), "destroy",
			  G_CALLBACK (destroy), NULL);
	config->mainwin = mainwin;

	vbox = gtk_vbox_new (FALSE, 0);
	gtk_container_add (GTK_CONTAINER (mainwin), vbox);
	gtk_widget_show (vbox);

	/* menu */
	menu = build_menu (config, mainwin);
	gtk_widget_show (menu);
	gtk_box_pack_start (GTK_BOX (vbox), menu, FALSE, FALSE, 0);

	/* tables and views page */
	page = build_page (config);
	gtk_box_pack_start (GTK_BOX (vbox), page, TRUE, TRUE, 0);

	/* Show the application window */
	gtk_widget_set_size_request (mainwin, 800, 600);
	gtk_widget_show_all (mainwin);

	/* Application init */
	if (argc > 1) {
		open_connection (config, argv[1]);
	}
	else {
		gchar *path, *file;
		gchar *str;

		str = g_get_current_dir ();
		path = gda_rfc1738_encode (str);
		g_free (str);
		file = gda_rfc1738_encode ("localdata");
		str = g_strdup_printf ("Sqlite://DB_DIR=%s;DB_NAME=%s", path, file);
		g_free (path);
		g_free (file);

		open_connection (config, str);
		g_free (str);
	}

	/* start with the first test */
	ATest *test;
	test = g_slist_nth_data (config->tests, test_number);
	if (!test)
		test = config->tests->data;
	add_test_page (config, test);
	
	gtk_main ();

	return 0;
}



/*
 * Common callbacks
 */
static gboolean delete_event (GtkWidget *widget, GdkEvent  *event, gpointer data)
{
    g_print ("Leaving DB test...\n");

    return FALSE;
}

static void destroy (GtkWidget *widget, gpointer data)
{
    gtk_main_quit ();
}

static void
conn_status_changed_cb (GdaConnection *cnc, MainConfig *config)
{
	gchar *str;
	if (gda_connection_is_opened (cnc))
		str = "<i>Connection is opened</i>";
	else
		str = "<i>Connection is closed</i>";
	gtk_label_set_markup (GTK_LABEL (config->cnc_status), str);
}


static void
open_connection (MainConfig *config, const gchar *str)
{
	GdaDsnInfo *info;
	GdaConnection *newcnc;
	gchar *user, *pass, *real_cnc, *real_provider, *real_auth_string = NULL;

	if (config->cnc) {
		g_signal_handlers_disconnect_by_func (newcnc,
						      G_CALLBACK (conn_status_changed_cb), config);
		g_object_unref (config->cnc);
		config->cnc = NULL;
	}
	gtk_window_set_title (GTK_WINDOW (config->mainwin), "No connection");

	gda_connection_string_split (str, &real_cnc, &real_provider, &user, &pass);
	if (!real_cnc) {
		g_free (user);
		g_free (pass);
		g_free (real_provider);
		g_print ("Malformed connection string '%s'", str);
		exit (1);
	}

	if (user && !*user) {
		gchar buf[80];
		g_print ("\tUsername: ");
		if (scanf ("%80s", buf) == -1) {
			g_free (real_cnc);
			g_free (user);
			g_free (pass);
			g_free (real_provider);
			g_print ("No username for '%s'", str);
			exit (1);
		}
		g_free (user);
		user = g_strdup (buf);
	}
	if (pass && !*pass) {
		gchar buf[80];
		g_print ("\tPassword for");
		if (scanf ("%80s", buf) == -1) {
			g_free (real_cnc);
			g_free (user);
			g_free (pass);
			g_free (real_provider);
			g_print ("No password for '%s'", str);
			exit (1);
		}
		g_free (pass);
		pass = g_strdup (buf);
	}
	if (user || pass) {
		gchar *s1;
		s1 = gda_rfc1738_encode (user);
		if (pass) {
			gchar *s2;
			s2 = gda_rfc1738_encode (pass);
			real_auth_string = g_strdup_printf ("USERNAME=%s;PASSWORD=%s", s1, s2);
			g_free (s2);
		}
		else
			real_auth_string = g_strdup_printf ("USERNAME=%s", s1);
		g_free (s1);
	}
	
	info = gda_config_get_dsn_info (real_cnc);
	if (info && !real_provider)
		newcnc = gda_connection_open_from_dsn (str, real_auth_string, 0, NULL);
	else 
		newcnc = gda_connection_open_from_string (NULL, str, real_auth_string, 0, NULL);

	if (newcnc)
		gtk_window_set_title (GTK_WINDOW (config->mainwin), real_cnc);
	else
		gtk_window_set_title (GTK_WINDOW (config->mainwin), "No connection");
	
	g_free (real_cnc);
	g_free (user);
	g_free (pass);
	g_free (real_provider);
	g_free (real_auth_string);

	config->cnc = newcnc;

	if (newcnc) {
		g_signal_connect (newcnc, "conn_opened",
				  G_CALLBACK (conn_status_changed_cb), config);
		g_signal_connect (newcnc, "conn_closed",
				  G_CALLBACK (conn_status_changed_cb), config);
		conn_status_changed_cb (newcnc, config);
		signal_tests (config);
	}
	else {
		g_print ("Error opening connection");
		exit (1);
	}
}


/*
 * Menu building
 */
static void add_test_cb (GtkWidget *wid, MainConfig *config);
static void select_ds_cb (GtkWidget *wid, MainConfig *config);
static void sync_action_cb (GtkWidget *wid, MainConfig *config);
static void open_conn_cb (GtkWidget *wid, MainConfig *config);
GtkWidget *
build_menu (MainConfig *config, GtkWidget *mainwin)
{
	GtkWidget *menubar1, *menuitem1, *menuitem1_menu, *entry;
	GtkAccelGroup *accel_group;
	GSList *list;

	accel_group = gtk_accel_group_new ();

	menubar1 = gtk_menu_bar_new ();
	gtk_widget_show (menubar1);

	/* File menu */
	menuitem1 = gtk_menu_item_new_with_mnemonic (_("_File"));
	gtk_widget_show (menuitem1);
	gtk_container_add (GTK_CONTAINER (menubar1), menuitem1);
	
	menuitem1_menu = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem1), menuitem1_menu);
	
	entry = gtk_image_menu_item_new_from_stock (GTK_STOCK_QUIT, accel_group);
	gtk_widget_show (entry);
	gtk_container_add (GTK_CONTAINER (menuitem1_menu), entry);

	g_signal_connect (G_OBJECT (entry), "activate",
			  G_CALLBACK (destroy), config);

	/* Database menu */
	menuitem1 = gtk_menu_item_new_with_mnemonic (_("_Database"));
	gtk_widget_show (menuitem1);
	gtk_container_add (GTK_CONTAINER (menubar1), menuitem1);
	
	menuitem1_menu = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem1), menuitem1_menu);
	
	entry = gtk_menu_item_new_with_mnemonic (_("Open connection"));
	gtk_widget_show (entry);
	gtk_container_add (GTK_CONTAINER (menuitem1_menu), entry);
	g_signal_connect (G_OBJECT (entry), "activate",
			  G_CALLBACK (open_conn_cb), config);

	entry = gtk_menu_item_new_with_mnemonic (_("Select datasource"));
	gtk_widget_show (entry);
	gtk_container_add (GTK_CONTAINER (menuitem1_menu), entry);

	g_signal_connect (G_OBJECT (entry), "activate",
			  G_CALLBACK (select_ds_cb), config);

	entry = gtk_menu_item_new_with_mnemonic (_("Synchronise metadata with DBMS"));
	gtk_widget_show (entry);
	gtk_container_add (GTK_CONTAINER (menuitem1_menu), entry);

	g_signal_connect (G_OBJECT (entry), "activate",
			  G_CALLBACK (sync_action_cb), config);

	/* Tests menu */
	menuitem1 = gtk_menu_item_new_with_mnemonic (_("_New Test"));
	gtk_widget_show (menuitem1);
	gtk_container_add (GTK_CONTAINER (menubar1), menuitem1);

	menuitem1_menu = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem1), menuitem1_menu);

	list = config->tests;
	while (list) {
		ATest *test = (ATest*) (list->data);
		entry = gtk_menu_item_new_with_mnemonic (test->test_name);
		gtk_widget_show (entry);
		gtk_container_add (GTK_CONTAINER (menuitem1_menu), entry);
		g_object_set_data (G_OBJECT (entry), "atest", test);
		g_signal_connect (G_OBJECT (entry), "activate",
				  G_CALLBACK (add_test_cb), config);
		
		list = g_slist_next (list);
	}

	gtk_window_add_accel_group (GTK_WINDOW (mainwin), accel_group);

	return menubar1;
}

static void
add_test_cb (GtkWidget *wid, MainConfig *config)
{
	ATest *test;

	test = g_object_get_data (G_OBJECT (wid), "atest");
	add_test_page (config, test);
}

static void
sync_action_cb (GtkWidget *wid, MainConfig *config)
{
	gda_connection_update_meta_store (config->cnc, NULL, NULL);
}

static void
select_ds_cb (GtkWidget *wid, MainConfig *config)
{
	GtkWidget *props;
                                                                                                                    
        props = gnome_db_login_dialog_new (_("Connection's configuration"), NULL);
        if (gnome_db_login_dialog_run (GNOME_DB_LOGIN_DIALOG (props))) {
		//gda_connection_set_dsn (config->cnc, gnome_db_login_dialog_get_dsn (GNOME_DB_LOGIN_DIALOG (props)));
		TO_IMPLEMENT;
		g_object_set (G_OBJECT (config->cnc),
			      "auth-string", gnome_db_login_dialog_get_dsn (GNOME_DB_LOGIN_DIALOG (props)), NULL);
		gtk_widget_destroy (props);
		sync_action_cb (NULL, config);
	}
	else
		gtk_widget_destroy (props);
}

static void
open_conn_cb (GtkWidget *wid, MainConfig *config)
{
	TO_IMPLEMENT;
	g_print ("This opens another connection\n");
}

/*
 * Main page building
 */
static void selection_changed_cb (GnomeDbSelector *mgsel, GObject *obj, MainConfig *config);
static void left_del_clicked_cb (GtkWidget *button, MainConfig *config);
static void right_del_clicked_cb (GtkWidget *button, MainConfig *config);
static GtkWidget *
build_page (MainConfig *config)
{
	GtkWidget *wid, *label, *paned, *vb, *bb, *button, *nb;
	gchar *str;

	paned = gtk_hpaned_new ();
	gtk_container_set_border_width (GTK_CONTAINER (paned), 5);

	/* left part */
	vb = gtk_vbox_new (FALSE, 5);
	gtk_container_set_border_width (GTK_CONTAINER (vb), 5);
	gtk_paned_add1 (GTK_PANED (paned), vb);

	label = gtk_label_new (NULL);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0);
	str = g_strdup_printf ("<b>%s</b>", _("Available objects:"));
	gtk_label_set_markup (GTK_LABEL (label), str);
	g_free (str);
	gtk_box_pack_start (GTK_BOX (vb), label, FALSE, TRUE, 0);

	label = gtk_label_new (NULL);
	gtk_label_set_markup (GTK_LABEL (label), "<i>Connection is closed</i>");
	gtk_box_pack_start (GTK_BOX (vb), label, FALSE, TRUE, 0);
	config->cnc_status = label;

	wid = gtk_label_new ("TO_IMPLEMENT");
	/*
	wid = gnome_db_selector_new (config->dict, NULL, 
			       GNOME_DB_SELECTOR_TABLES | GNOME_DB_SELECTOR_FIELDS |
			       GNOME_DB_SELECTOR_QUERIES | GNOME_DB_SELECTOR_QVIS_FIELDS |
			       GNOME_DB_SELECTOR_GRAPHS |
			       GNOME_DB_SELECTOR_FORMS, 0);
	gnome_db_selector_set_headers_visible (GNOME_DB_SELECTOR (wid), FALSE);
	g_signal_connect (G_OBJECT (wid), "selection_changed",
			  G_CALLBACK (selection_changed_cb), config);
	*/
	gtk_box_pack_start (GTK_BOX (vb), wid, TRUE, TRUE, 0);
	gtk_widget_set_size_request (wid, 200, 300);
	config->selector = wid;

	bb = gtk_vbutton_box_new ();
	gtk_box_pack_start (GTK_BOX (vb), bb, FALSE, FALSE, 0);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (bb), GTK_BUTTONBOX_SPREAD);
	button = gtk_button_new_from_stock (GTK_STOCK_DELETE);
	gtk_container_add (GTK_CONTAINER (bb), button);
	config->del_obj_button = button;
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (left_del_clicked_cb), config);

	/* right part */
	vb = gtk_vbox_new (FALSE, 5);
	gtk_container_set_border_width (GTK_CONTAINER (vb), 5);
	gtk_paned_add2 (GTK_PANED (paned), vb);

	label = gtk_label_new (NULL);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0);
	str = g_strdup_printf ("<b>%s</b>", _("Tested widget:"));
	gtk_label_set_markup (GTK_LABEL (label), str);
	g_free (str);
	gtk_box_pack_start (GTK_BOX (vb), label, FALSE, FALSE, 0);

	nb = gtk_notebook_new ();
	config->tests_nb = nb;
	gtk_box_pack_start (GTK_BOX (vb), nb, TRUE, TRUE, 0);
	gtk_widget_show (nb);

	bb = gtk_hbutton_box_new ();
	gtk_box_pack_start (GTK_BOX (vb), bb, FALSE, FALSE, 0);
	gtk_button_box_set_layout (GTK_BUTTON_BOX (bb), GTK_BUTTONBOX_SPREAD);
	button = gtk_button_new_from_stock (GTK_STOCK_DELETE);
	config->del_test_button = button;
	gtk_container_add (GTK_CONTAINER (bb), button);
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (right_del_clicked_cb), config);

	return paned;
}

/*
 * utility function to provide a new GdaDataModel
 *
 * Returns: NULL if an error occurred, or if the user did not enter any required parameter
 */
static GdaDataModel *
get_data_model_from_statement (MainConfig *config, GdaStatement *stmt, const gchar *ident)
{
	GdaDataModel *model = NULL;

	if (GDA_IS_STATEMENT (stmt) && (gda_statement_get_statement_type (stmt) == GDA_SQL_STATEMENT_SELECT)) {
		GdaSet *dset;
		
		if (! gda_statement_get_parameters (stmt, &dset, NULL))
			return NULL;

		if (dset && !gda_set_is_valid (dset, NULL)) {
			GtkWidget *dlg;
			gint status;
			gchar *str;

			str = g_strdup_printf (_("%s: parameters required"), ident);
			dlg = gnome_db_basic_form_new_in_dialog (dset, NULL, str,
								 _("Some parameters are required to execute this query"));
			g_free (str);
			status = gtk_dialog_run (GTK_DIALOG (dlg));
			gtk_widget_destroy (dlg);
			if (status == GTK_RESPONSE_REJECT) 
				return NULL;
		}

		model = (GdaDataModel*) gda_connection_statement_execute_select (config->cnc, stmt, dset, NULL);
	}

	return model;
}


/* Add a new tested widget */
static void
add_test_page (MainConfig *config, ATest *test)
{
	GtkWidget *wid, *label;
	gint page;

	if (!test)
		return;

	wid = (test->make_test_widget) (config);
	g_object_set_data (G_OBJECT (wid), "atest", test);
	label = gtk_label_new (test->test_name);
	page = gtk_notebook_append_page (GTK_NOTEBOOK (config->tests_nb), wid, label);
	gtk_widget_show (wid);
	gtk_widget_show (label);
	gtk_widget_set_sensitive (config->del_test_button, TRUE);
	gtk_notebook_set_current_page (GTK_NOTEBOOK (config->tests_nb), page);
}

static void
selection_changed_cb (GnomeDbSelector *mgsel, GObject *obj, MainConfig *config)
{
	gtk_widget_set_sensitive (config->del_obj_button, obj ? TRUE : FALSE);
}

static void
left_del_clicked_cb (GtkWidget *button, MainConfig *config)
{
	/*
	GObject *obj = gnome_db_selector_get_selected_object (GNOME_DB_SELECTOR (config->selector));

	if (obj) 
		gda_object_destroy (GDA_OBJECT (obj));
	*/
}

static void
right_del_clicked_cb (GtkWidget *button, MainConfig *config)
{
	GtkWidget *page;
	ATest *test;
	gint pageno = gtk_notebook_get_current_page (GTK_NOTEBOOK (config->tests_nb));

	page = gtk_notebook_get_nth_page (GTK_NOTEBOOK (config->tests_nb), pageno);
	test = g_object_get_data (G_OBJECT (page), "atest");
	if (test->clean_data) 
		(test->clean_data) (config, page);		
	gtk_notebook_remove_page (GTK_NOTEBOOK (config->tests_nb), pageno);
	gtk_widget_set_sensitive (config->del_test_button,
				  gtk_notebook_get_n_pages (GTK_NOTEBOOK (config->tests_nb)) != 0);
}


static void
param_list_param_changed_cb (GdaSet *paramlist, GdaHolder *param, gchar *prefix)
{
	const GValue *value;
	gchar *str;

	value = gda_holder_get_value (param);
	if (value)
		str = gda_value_stringify ((GValue *) value);
	else
		str = "_null_";
	if (strlen (str) > 30) {
		str [29] = '.';
		str [28] = '.';
		str [27] = '.';
		str [30] = 0;		
	}
	g_print ("%s: parameter %p '%s' changed to %s%s\n", prefix,
		 param, gda_holder_get_id (param), str,
		 gda_holder_is_valid (param) ? "" : " (invalid)");
	if (value)
		g_free (str);
}






















/*
 * GnomeDbEditor test
 */
static GtkWidget *
editor_test_make (MainConfig *config)
{
	GtkWidget *ret;
	
	ret = gnome_db_editor_new ();
	gnome_db_editor_set_highlight (GNOME_DB_EDITOR (ret), TRUE);

	return ret;
}

/*
 * GnomeDbSqlConsole test
 */
static GtkWidget *
console_test_make (MainConfig *config)
{
	GtkWidget *sw, *console;
	
	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (sw), GTK_SHADOW_IN);	
	
	console = gnome_db_sql_console_new (config->cnc, _("SQL Console (type \\? + ENTER for help)\n"));
	gtk_container_add (GTK_CONTAINER (sw), console);
	gtk_widget_show (console);

	return sw;
}

/*
 * GnomeDbDataImport test
 */
static GtkWidget *
import_test_make (MainConfig *config)
{
	GtkWidget *import;
	
	import = gnome_db_data_import_new ();
	return import;
}

/*
 * GnomeDbCombo test
 */
typedef struct {
	GtkWidget  *combo_vbox;
	GtkWidget  *combo;
	gulong      signal_handler;
} TestData8;

static void combo_test_combo_changed (GnomeDbSelector *mgsel, GObject *obj, TestData8 *data);
static GtkWidget *
combo_test_make (MainConfig *config)
{
	GtkWidget *ret, *wid;
	TestData8 *data;
	
	ret = gtk_vbox_new (FALSE, 0);

	/* private test data */
	data = g_new0 (TestData8, 1);
	data->combo = NULL;
	data->signal_handler = g_signal_connect (G_OBJECT (config->selector), "selection_changed",
						 G_CALLBACK (combo_test_combo_changed), data);
	g_object_set_data (G_OBJECT (ret), "private_data", data);

	wid = gtk_label_new ("Combo box test, select a SELECT query");
	gtk_box_pack_start (GTK_BOX (ret), wid, FALSE, FALSE, 10);
	gtk_widget_show (wid);

	wid = gtk_vbox_new (FALSE, 0);
	gtk_box_pack_start (GTK_BOX (ret), wid, FALSE, FALSE, 10);
	gtk_widget_show (wid);
	data->combo_vbox = wid;
	
	return ret;
}

static void
combo_test_combo_changed (GnomeDbSelector *mgsel, GObject *obj, TestData8 *data)
{
	/*
	GdaDataModel *model = NULL;
	GtkWidget *wid;

	gint i, n_cols;
	gint *cols_index;

	if (GDA_IS_DICT_TABLE (obj)) {
		gchar *str;
		GdaQuery *query;
		GdaDict *dict = gda_object_get_dict (GDA_OBJECT (obj));

		str = g_strdup_printf ("SELECT * FROM %s", gda_object_get_name (GDA_OBJECT (obj)));
		query = gda_query_new_from_sql (dict, str, NULL);
		g_free (str);
		g_assert (gda_query_get_query_type (query) == GDA_QUERY_TYPE_SELECT);

		model = get_data_model_from_statement (query, gda_object_get_name (GDA_OBJECT (obj)));
		g_object_unref (G_OBJECT (query));
	}

	if (GDA_IS_QUERY (obj) && gda_query_is_select_query (GDA_QUERY (obj))) {
		GdaQuery *query = GDA_QUERY (obj);
		model = get_data_model_from_statement (query, gda_object_get_name (GDA_OBJECT (obj)));
	}

	if (!model)
		return;

	if (data->combo) {
		gtk_widget_destroy (data->combo);
		data->combo = NULL;
	}

	n_cols = gda_data_model_get_n_columns (model);
	cols_index = g_new0 (gint, n_cols);
	for (i=0; i<n_cols; i++)
		cols_index [i] = i;
	wid = gnome_db_combo_new_with_model (model, n_cols, cols_index);
	g_free (cols_index);

	gtk_box_pack_start (GTK_BOX (data->combo_vbox), wid, FALSE, FALSE, 0);
	gtk_widget_show (wid);
	data->combo = wid;
	
	g_object_unref (model);
	*/
}

static void
combo_test_clean (MainConfig *config, GtkWidget *test_widget)
{
	TestData8 *data = g_object_get_data (G_OBJECT (test_widget), "private_data");

	if (data) {
		g_signal_handler_disconnect (G_OBJECT (config->selector), data->signal_handler);
		g_free (data);
	}
}

static gboolean
combo_test_signal (MainConfig *config, GtkWidget *test_widget)
{
	/*
	GObject *obj;
	TestData8 *data = g_object_get_data (G_OBJECT (test_widget), "private_data");

	obj = gnome_db_selector_get_selected_object (GNOME_DB_SELECTOR (config->selector));
	combo_test_combo_changed (GNOME_DB_SELECTOR (config->selector), obj, data);
	*/

	return TRUE;
}


/*
 * XML GdaSet spec test
 */
typedef struct {
	GtkTextBuffer    *buffer;
	GtkWidget        *paned;

	GdaSet *paramlist;
	GtkWidget        *form_vbox;
	GtkWidget        *form1;
	GtkWidget        *form2;

	GtkWidget        *glade_file;
	GtkWidget        *root_element;
	GtkWidget        *form_prefix;
} TestData9;

static void xml_spec_button_clicked_cb (GtkWidget *button, MainConfig *config);
static GtkWidget *
xml_spec_test_make (MainConfig *config)
{
	GtkTextBuffer *buffer;
	GtkWidget *vbox, *view, *sw, *wid, *paned, *label, *table;
	GtkTextIter iter;
	TestData9 *data;
	gchar *test_file;
	gsize length;
	GError *error = NULL;
	GtkFileFilter *filter;

	/* private test data */
	data = g_new0 (TestData9, 1);
	data->paramlist = NULL;

	/* paned widget */
	paned = gtk_vpaned_new ();
	data->paned = paned;

	/* a box for text and button */
	vbox = gtk_vbox_new (FALSE, 5);
	g_object_set_data (G_OBJECT (vbox), "private_data", data);
	gtk_paned_add1 (GTK_PANED (paned), vbox);
	gtk_widget_show (vbox);

	/* Text at the top */
	label = gtk_label_new ("<b>Tested XML and Glade file specifications:</b>");
	gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, TRUE, 2);
	gtk_misc_set_alignment (GTK_MISC (label), 0, -1);
	g_object_set (label, "use-markup", TRUE, NULL);
	gtk_widget_show (label);

	buffer = gtk_text_buffer_new (NULL);
	data->buffer = buffer;
	gtk_text_buffer_get_start_iter (buffer, &iter);
	if (g_file_get_contents ("TEST_xml_spec.xml", &test_file, &length, &error)) {
		gtk_text_buffer_insert (buffer, &iter, test_file, length);
		g_free (test_file);
	}
	else {
		g_print ("%s\n", error->message);
		g_error_free (error);
		gtk_text_buffer_insert (buffer, &iter,
					"<?xml version=\"1.0\"?>\n"
					"<data-set-spec>\n"
					"  <parameter name=\"Name\" descr=\"A description\" gda-type=\"string\"/>\n"
					"</data-set-spec>", -1);
	}
	view = gtk_text_view_new_with_buffer (buffer);
	g_object_unref (buffer);
	
	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_box_pack_start (GTK_BOX (vbox), sw, TRUE, TRUE, 2);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (sw), GTK_SHADOW_IN);	
	
	gtk_container_add (GTK_CONTAINER (sw), view);
	gtk_widget_show (view);
	gtk_widget_show (sw);

	/* table to hold optional stuff andccommand button */
	table = gtk_table_new (4, 2, FALSE);
	gtk_box_pack_start (GTK_BOX (vbox), table, FALSE, TRUE, 5);
	gtk_widget_show (table);

	/* file chooser button for glade XML file */
	label = gtk_label_new ("Glade file:");
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 0, 1, 0, 0, 0, 0);
	gtk_widget_show (label);

	wid = gtk_file_chooser_button_new ("Choose an Glade file", GTK_FILE_CHOOSER_ACTION_OPEN);
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 1, 2, 0, 1);
	gtk_widget_show (wid);
	data->glade_file = wid;

	filter = gtk_file_filter_new ();
	gtk_file_filter_add_pattern (filter, "*.glade");
	gtk_file_chooser_add_filter (GTK_FILE_CHOOSER (wid), filter);

	label = gtk_label_new ("Root element:");
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 1, 2, 0, 0, 0, 0);
	gtk_widget_show (label);

	wid = gtk_entry_new ();
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 1, 2, 1, 2);
	gtk_widget_show (wid);
	data->root_element = wid;

	label = gtk_label_new ("Form prefix:");
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 2, 3, 0, 0, 0, 0);
	gtk_widget_show (label);

	wid = gtk_entry_new ();
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 1, 2, 2, 3);
	gtk_widget_show (wid);
	data->form_prefix = wid;

	/* button */
	wid = gtk_button_new_with_label ("Test the XML spec above");
	gtk_table_attach_defaults (GTK_TABLE (table), wid, 0, 2, 3, 4);	
	gtk_widget_show (wid);
	g_signal_connect (G_OBJECT (wid), "clicked",
			  G_CALLBACK (xml_spec_button_clicked_cb), config);
	g_object_set_data (G_OBJECT (wid), "private_data", data);

	/* form */
	vbox = gtk_vbox_new (FALSE, 5);
	gtk_paned_add2 (GTK_PANED (data->paned), vbox);
	data->form_vbox = vbox;
	gtk_widget_show (vbox);

	label = gtk_label_new ("<b>GnomeDbBasicForm widgets:</b>\n<small>Two widgets are created to demonstrate"
			       "that they do operate on the same GdaSet object, they should always be"
			       "in sync.</small>");
	gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, TRUE, 2);
	gtk_misc_set_alignment (GTK_MISC (label), 0, -1);
	g_object_set (label, "use-markup", TRUE, NULL);
	gtk_widget_show (label);

	data->form1 = NULL;
	data->form2 = NULL;

	return paned;
}

static void
xml_spec_button_clicked_cb (GtkWidget *button, MainConfig *config)
{
	TestData9 *data;
	GdaSet *dset;
	GtkTextIter start, end;
	gchar *str;
	GError *error = NULL;

	data = g_object_get_data (G_OBJECT (button), "private_data");
	if (data->form1) {
		gtk_widget_destroy (data->form1);
		data->form1 = NULL;
	}
	if (data->form2) {
		gtk_widget_destroy (data->form2);
		data->form2 = NULL;
	}

	if (data->paramlist) {
		g_signal_handlers_disconnect_by_func (data->paramlist,
						      G_CALLBACK (param_list_param_changed_cb), data);

		g_object_unref (data->paramlist);
		data->paramlist = NULL;
	}

	gtk_text_buffer_get_bounds (data->buffer, &start, &end);
	str = gtk_text_buffer_get_text (data->buffer, &start, &end, FALSE);
	dset = GDA_SET (gda_set_new_from_spec_string (str, &error));
	g_free (str);
	if (dset) {
		GtkWidget *form;

		/* form 1 */
		form = gnome_db_basic_form_new (dset);
		gtk_box_pack_start (GTK_BOX (data->form_vbox), form, TRUE, TRUE, 5);
		gtk_widget_show (form);
		data->form1 = form;
		
		/* form 2 */
		str = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (data->glade_file));
		if (str) {
			form = gnome_db_basic_form_new_custom (dset, str,
							       gtk_entry_get_text (GTK_ENTRY (data->root_element)),
							       gtk_entry_get_text (GTK_ENTRY (data->form_prefix)));
			g_free (str);
		}
		else
			form = gnome_db_basic_form_new (dset);
		gtk_box_pack_start (GTK_BOX (data->form_vbox), form, TRUE, TRUE, 5);
		gtk_widget_show (form);
		data->form2 = form;

		g_signal_connect (dset, "param_changed",
				  G_CALLBACK (param_list_param_changed_cb), "XML Spec test");
		data->paramlist = dset;
	}
	else {
		GtkWidget *label;

		str = g_strdup_printf ("XML spec is invalid:\n%s", error ? error->message : "???");
		label = gtk_label_new (str);
		g_free (str);
		gtk_box_pack_start (GTK_BOX (data->form_vbox), label, TRUE, TRUE, 5);
		gtk_widget_show (label);
		data->form1 = label;
	}
}

static void
xml_spec_test_clean (MainConfig *config, GtkWidget *test_widget)
{
	TestData9 *data = g_object_get_data (G_OBJECT (test_widget), "private_data");

	if (data) 
		g_free (data);
}

/*
 *
 * Transaction Status test
 *
 */
typedef struct {
	MainConfig              *config;
	GtkWidget               *form_vbox;
	GtkWidget               *status;
	GtkWidget               *console;
} TestData11;

static gchar *transaction_status_test_sql_entered_cb (GnomeDbSqlConsole *console, const gchar *sql, TestData11 *data);


static GtkWidget *
transaction_status_test_make (MainConfig *config)
{
	GtkWidget *vbox, *label;
	GtkWidget *sw, *console;
	TestData11 *data;

	/* private test data */
	data = g_new0 (TestData11, 1);
	data->config = config;
	data->form_vbox = gtk_vbox_new (FALSE, 0);

	vbox = gtk_vbox_new (FALSE, 0);
	g_object_set_data (G_OBJECT (vbox), "private_data", data);

	/* tested widget */
	label = gtk_label_new ("<b>GnomeDbTransactionStatus widget</b>");
	gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);
	gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	gtk_box_pack_start (GTK_BOX (vbox), data->form_vbox, TRUE, TRUE, 0);
	gtk_widget_show (data->form_vbox);
	transaction_status_test_signal (config, vbox);

	/* console */
	label = gtk_label_new ("<b>Console to enter transaction orders</b>\nwhich are not executed by the connection!!!");
	gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);
	gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (sw), GTK_SHADOW_IN);	
	
	console = gnome_db_sql_console_new (NULL, _("SQL Console (type \\? + ENTER for help)\n"));
	gtk_container_add (GTK_CONTAINER (sw), console);
	gtk_widget_show (console);
	gtk_box_pack_start (GTK_BOX (vbox), sw, TRUE, TRUE, 0);
	gtk_widget_show (sw);

	g_signal_connect (G_OBJECT (console), "sql_entered",
			  G_CALLBACK (transaction_status_test_sql_entered_cb), data);

	return vbox;
}

static gboolean
transaction_status_test_signal (MainConfig *config, GtkWidget *test_widget)
{
	TestData11 *data = g_object_get_data (G_OBJECT (test_widget), "private_data");
	GtkWidget *status;

	if (data->status)
		gtk_widget_destroy (data->status);

	status = gnome_db_transaction_status_new (data->config->cnc);
	gtk_box_pack_start (GTK_BOX (data->form_vbox), status, TRUE, TRUE, 0);
	gtk_widget_show (status);
	data->status = status;

	return TRUE;
}

static gchar *
transaction_status_test_sql_entered_cb (GnomeDbSqlConsole *console, const gchar *sql, TestData11 *data)
{
	static GdaSqlParser *parser = NULL;
	GdaStatement *stmt;
	GError *error = NULL;
	const gchar *remain;
	if (!parser) {
		parser = gda_connection_create_parser (data->config->cnc);
		if (!parser)
			parser = gda_sql_parser_new ();
	}
	stmt = gda_sql_parser_parse_string (parser, sql, &remain, &error);
	if (!stmt) {
		gchar *ret = g_strdup (error && error->message ? error->message : "No detail");
		if (error)
			g_error_free (error);
		return ret;
	}

	if (remain) {
		gchar *ret = g_strdup_printf ("Remains: %s\n", remain); 
		g_object_unref (stmt);
		return ret;
	}

	GObject *obj;
	obj = gda_connection_statement_execute (data->config->cnc, stmt, NULL, GDA_STATEMENT_MODEL_RANDOM_ACCESS, NULL, &error);
	g_object_unref (stmt);
	if (!obj) {
		gchar *ret = g_strdup (error && error->message ? error->message : "No detail");
		if (error)
			g_error_free (error);
		return ret;
	}
	else if (GDA_IS_DATA_MODEL (obj)) {
		gchar *ret = gda_data_model_dump_as_string (GDA_DATA_MODEL (obj));
		g_object_unref (obj);
		return ret;
	}
	else if (GDA_IS_SET (obj)) {
		gchar *ret = g_strdup ("Ok\n");
		g_object_unref (obj);
		return ret;
	}
	else {
		gchar *ret = g_strdup ("???\n");
		g_object_unref (obj);
		return ret;
	}
}

/*
 * GnomeDbFormatEntry test
 */
typedef struct {
	MainConfig              *config;
	GtkWidget               *table;
	GtkWidget               *format;
	GtkWidget               *mask;
	GtkWidget               *prefix;
	GtkWidget               *suffix;
	GtkWidget               *thousands;
	GtkWidget               *decimals;
	GtkWidget               *n_decimals;
	GtkWidget               *max_length;
	GnomeDbFormatEntry      *entry;
	GtkWidget               *set_text;
} TestData12;

static void format_entry_test_max_length_set_cb (GtkWidget *button, TestData12 *data);
static void format_entry_test_format_set_cb (GtkWidget *button, TestData12 *data);
static void format_entry_test_mask_set_cb (GtkWidget *button, TestData12 *data);
static void format_entry_test_prefix_set_cb (GtkWidget *button, TestData12 *data);
static void format_entry_test_suffix_set_cb (GtkWidget *button, TestData12 *data);
static void format_entry_test_edited_type_changed_cb (GnomeDbCombo *combo, TestData12 *data);
static void format_entry_test_n_decimals_set_cb (GtkWidget *button, TestData12 *data);
static void format_entry_test_decimal_sep_set_cb (GtkWidget *button, TestData12 *data);
static void format_entry_test_thousands_sep_set_cb (GtkWidget *button, TestData12 *data);
static void format_entry_test_show_value_cb (GtkWidget *button, TestData12 *data);
static void format_entry_test_set_text_cb (GtkWidget *button, TestData12 *data);
static GdaDataModel *format_entry_test_make_editable_types (void);

static GtkWidget *
format_entry_test_make (MainConfig *config)
{
	GtkWidget *entry;
	GtkWidget *table, *label, *button;
	TestData12 *data;
	GdaDataModel *types;

	/* private test data */
	data = g_new0 (TestData12, 1);
	data->config = config;

	table = gtk_table_new (8, 3, FALSE);
	g_object_set_data (G_OBJECT (table), "private_data", data);
	data->table = table;

	/* format */
	label = gtk_label_new ("Format:");
        gtk_table_attach (GTK_TABLE (table), label, 0, 1, 0, 1, 0, 0, 0, 0);
	entry = gtk_entry_new ();
	gtk_table_attach (GTK_TABLE (table), entry, 1, 2, 0, 1, GTK_FILL | GTK_EXPAND, 0, 0, 0);
	data->format = entry;
	button = gtk_button_new_with_label ("Set");
	gtk_table_attach (GTK_TABLE (table), button, 2, 3, 0, 1, 0, 0, 0, 0);
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (format_entry_test_format_set_cb), data);

	/* mask */
	label = gtk_label_new ("Mask:");
        gtk_table_attach (GTK_TABLE (table), label, 0, 1, 1, 2, 0, 0, 0, 0);
	entry = gtk_entry_new ();
	gtk_table_attach (GTK_TABLE (table), entry, 1, 2, 1, 2, GTK_FILL | GTK_EXPAND, 0, 0, 0);
	data->mask = entry;
	button = gtk_button_new_with_label ("Set");
	gtk_table_attach (GTK_TABLE (table), button, 2, 3, 1, 2, 0, 0, 0, 0);
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (format_entry_test_mask_set_cb), data);

	/* prefix */
	label = gtk_label_new ("Prefix:");
        gtk_table_attach (GTK_TABLE (table), label, 0, 1, 2, 3, 0, 0, 0, 0);
	entry = gtk_entry_new ();
	gtk_table_attach (GTK_TABLE (table), entry, 1, 2, 2, 3, GTK_FILL | GTK_EXPAND, 0, 0, 0);
	data->prefix = entry;
	button = gtk_button_new_with_label ("Set");
	gtk_table_attach (GTK_TABLE (table), button, 2, 3, 2, 3, 0, 0, 0, 0);
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (format_entry_test_prefix_set_cb), data);

	/* suffix */
	label = gtk_label_new ("Suffix:");
        gtk_table_attach (GTK_TABLE (table), label, 0, 1, 3, 4, 0, 0, 0, 0);
	entry = gtk_entry_new ();
	gtk_table_attach (GTK_TABLE (table), entry, 1, 2, 3, 4, GTK_FILL | GTK_EXPAND, 0, 0, 0);
	data->suffix = entry;
	button = gtk_button_new_with_label ("Set");
	gtk_table_attach (GTK_TABLE (table), button, 2, 3, 3, 4, 0, 0, 0, 0);
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (format_entry_test_suffix_set_cb), data);

	/* edited type */
	types = format_entry_test_make_editable_types ();
	label = gtk_label_new ("Edited type:");
        gtk_table_attach (GTK_TABLE (table), label, 0, 1, 4, 5, 0, 0, 0, 0);
	entry = gnome_db_combo_new_with_model (types, 0, NULL);
	gtk_combo_box_set_active (GTK_COMBO_BOX (entry), 0);
	g_object_unref (types);
	gtk_table_attach (GTK_TABLE (table), entry, 1, 2, 4, 5, GTK_FILL | GTK_EXPAND, 0, 0, 0);
	g_signal_connect (G_OBJECT (entry), "changed",
			  G_CALLBACK (format_entry_test_edited_type_changed_cb), data);

	/* nb decimals */
	label = gtk_label_new ("Decimal places:");
        gtk_table_attach (GTK_TABLE (table), label, 0, 1, 5, 6, 0, 0, 0, 0);
	entry = gnome_db_format_entry_new ();
	gnome_db_format_entry_set_edited_type (GNOME_DB_FORMAT_ENTRY (entry), G_TYPE_INT);
	gtk_table_attach (GTK_TABLE (table), entry, 1, 2, 5, 6, GTK_FILL | GTK_EXPAND, 0, 0, 0);
	data->n_decimals = entry;
	button = gtk_button_new_with_label ("Set");
	gtk_table_attach (GTK_TABLE (table), button, 2, 3, 5, 6, 0, 0, 0, 0);
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (format_entry_test_n_decimals_set_cb), data);

	/* decimal sep */
	label = gtk_label_new ("Decimal sep.:");
        gtk_table_attach (GTK_TABLE (table), label, 0, 1, 6, 7, 0, 0, 0, 0);
	entry = gtk_entry_new ();
	gtk_entry_set_max_length (GTK_ENTRY (entry), 1);
	gtk_table_attach (GTK_TABLE (table), entry, 1, 2, 6, 7, GTK_FILL | GTK_EXPAND, 0, 0, 0);
	data->decimals = entry;
	button = gtk_button_new_with_label ("Set");
	gtk_table_attach (GTK_TABLE (table), button, 2, 3, 6, 7, 0, 0, 0, 0);
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (format_entry_test_decimal_sep_set_cb), data);

	/* thousands sep */
	label = gtk_label_new ("Thousands sep.:");
        gtk_table_attach (GTK_TABLE (table), label, 0, 1, 7, 8, 0, 0, 0, 0);
	entry = gtk_entry_new ();
	gtk_entry_set_max_length (GTK_ENTRY (entry), 1);
	gtk_table_attach (GTK_TABLE (table), entry, 1, 2, 7, 8, GTK_FILL | GTK_EXPAND, 0, 0, 0);
	data->thousands = entry;
	button = gtk_button_new_with_label ("Set");
	gtk_table_attach (GTK_TABLE (table), button, 2, 3, 7, 8, 0, 0, 0, 0);
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (format_entry_test_thousands_sep_set_cb), data);

	/* max length */
	label = gtk_label_new ("Max length:");
        gtk_table_attach (GTK_TABLE (table), label, 0, 1, 8, 9, 0, 0, 0, 0);
	entry = gtk_entry_new ();
	gtk_entry_set_max_length (GTK_ENTRY (entry), 1);
	gtk_table_attach (GTK_TABLE (table), entry, 1, 2, 8, 9, GTK_FILL | GTK_EXPAND, 0, 0, 0);
	data->max_length = entry;
	button = gtk_button_new_with_label ("Set");
	gtk_table_attach (GTK_TABLE (table), button, 2, 3, 8, 9, 0, 0, 0, 0);
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (format_entry_test_max_length_set_cb), data);

	/* tested widget */
	label = gtk_label_new ("<b>Format entry:</b>");
	gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 9, 10, 0, 0, 0, 0);
	entry = gnome_db_format_entry_new ();
	gtk_table_attach_defaults (GTK_TABLE (table), entry, 1, 2, 9, 10);
	data->entry = GNOME_DB_FORMAT_ENTRY (entry);
	button = gtk_button_new_with_label ("Show value");
	gtk_table_attach (GTK_TABLE (table), button, 2, 3, 9, 10, 0, 0, 0, 0);
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (format_entry_test_show_value_cb), data);

	gtk_widget_show_all (table);

	
	/* set value */
	label = gtk_label_new ("Set text:");
        gtk_table_attach (GTK_TABLE (table), label, 0, 1, 10, 11, 0, 0, 0, 0);
	entry = gtk_entry_new ();
	gtk_table_attach (GTK_TABLE (table), entry, 1, 2, 10, 11, GTK_FILL | GTK_EXPAND, 0, 0, 0);
	data->set_text = entry;
	button = gtk_button_new_with_label ("Set");
	gtk_table_attach (GTK_TABLE (table), button, 2, 3, 10, 11, 0, 0, 0, 0);
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (format_entry_test_set_text_cb), data);

	/* misc initializations */
	gtk_entry_set_text (GTK_ENTRY (data->prefix), "=>");
	gtk_entry_set_text (GTK_ENTRY (data->suffix), "<=");
	gtk_entry_set_text (GTK_ENTRY (data->thousands), " ");

	return table;
}

static GdaDataModel *
format_entry_test_make_editable_types (void)
{
	gint row;
	GValue *value;
	GdaDataModel *types;

	types = gda_data_model_array_new_with_g_types (2, G_TYPE_UINT, G_TYPE_STRING);

	row = gda_data_model_append_row (types, NULL);
	g_value_set_uint ((value = gda_value_new (G_TYPE_UINT)), G_TYPE_STRING);
	gda_data_model_set_value_at (types, 0, row, value, NULL); gda_value_free (value);
	g_value_set_string ((value = gda_value_new (G_TYPE_STRING)), "string");
	gda_data_model_set_value_at (types, 1, row, value, NULL); gda_value_free (value);

	row = gda_data_model_append_row (types, NULL);
	g_value_set_uint ((value = gda_value_new (G_TYPE_UINT)), G_TYPE_UINT64);
	gda_data_model_set_value_at (types, 0, row, value, NULL); gda_value_free (value);
	g_value_set_string ((value = gda_value_new (G_TYPE_STRING)), "uint64");
	gda_data_model_set_value_at (types, 1, row, value, NULL); gda_value_free (value);

	row = gda_data_model_append_row (types, NULL);
	g_value_set_uint ((value = gda_value_new (G_TYPE_UINT)), G_TYPE_INT64);
	gda_data_model_set_value_at (types, 0, row, value, NULL); gda_value_free (value);
	g_value_set_string ((value = gda_value_new (G_TYPE_STRING)), "int64");
	gda_data_model_set_value_at (types, 1, row, value, NULL); gda_value_free (value);

	row = gda_data_model_append_row (types, NULL);
	g_value_set_uint ((value = gda_value_new (G_TYPE_UINT)), G_TYPE_UINT);
	gda_data_model_set_value_at (types, 0, row, value, NULL); gda_value_free (value);
	g_value_set_string ((value = gda_value_new (G_TYPE_STRING)), "uint");
	gda_data_model_set_value_at (types, 1, row, value, NULL); gda_value_free (value);

	row = gda_data_model_append_row (types, NULL);
	g_value_set_uint ((value = gda_value_new (G_TYPE_UINT)), G_TYPE_INT);
	gda_data_model_set_value_at (types, 0, row, value, NULL); gda_value_free (value);
	g_value_set_string ((value = gda_value_new (G_TYPE_STRING)), "int");
	gda_data_model_set_value_at (types, 1, row, value, NULL); gda_value_free (value);

	row = gda_data_model_append_row (types, NULL);
	g_value_set_uint ((value = gda_value_new (G_TYPE_UINT)), G_TYPE_UCHAR);
	gda_data_model_set_value_at (types, 0, row, value, NULL); gda_value_free (value);
	g_value_set_string ((value = gda_value_new (G_TYPE_STRING)), "uchar");
	gda_data_model_set_value_at (types, 1, row, value, NULL); gda_value_free (value);

	row = gda_data_model_append_row (types, NULL);
	g_value_set_uint ((value = gda_value_new (G_TYPE_UINT)), G_TYPE_CHAR);
	gda_data_model_set_value_at (types, 0, row, value, NULL); gda_value_free (value);
	g_value_set_string ((value = gda_value_new (G_TYPE_STRING)), "char");
	gda_data_model_set_value_at (types, 1, row, value, NULL); gda_value_free (value);

	row = gda_data_model_append_row (types, NULL);
	g_value_set_uint ((value = gda_value_new (G_TYPE_UINT)), G_TYPE_FLOAT);
	gda_data_model_set_value_at (types, 0, row, value, NULL); gda_value_free (value);
	g_value_set_string ((value = gda_value_new (G_TYPE_STRING)), "float");
	gda_data_model_set_value_at (types, 1, row, value, NULL); gda_value_free (value);

	return types;
}

static void
format_entry_test_format_set_cb (GtkWidget *button, TestData12 *data)
{
	gnome_db_format_entry_set_format (data->entry, gtk_entry_get_text (GTK_ENTRY (data->format)), NULL, NULL);
}

static void
format_entry_test_mask_set_cb (GtkWidget *button, TestData12 *data)
{
	gnome_db_format_entry_set_format (data->entry, NULL, gtk_entry_get_text (GTK_ENTRY (data->mask)), NULL);
}

static void
format_entry_test_prefix_set_cb (GtkWidget *button, TestData12 *data)
{
	gnome_db_format_entry_set_prefix (data->entry, gtk_entry_get_text (GTK_ENTRY (data->prefix)));
}
static void
format_entry_test_suffix_set_cb (GtkWidget *button, TestData12 *data)
{
	gnome_db_format_entry_set_suffix (data->entry, gtk_entry_get_text (GTK_ENTRY (data->suffix)));
}

static void
format_entry_test_edited_type_changed_cb (GnomeDbCombo *combo, TestData12 *data)
{
	GSList *values;
	GType type;

	values = gnome_db_combo_get_values_ext (combo, 0, NULL);
	type = g_value_get_uint ((GValue *)(values->data));
	gnome_db_format_entry_set_edited_type (data->entry, type);
	g_slist_free (values);
}

static void
format_entry_test_n_decimals_set_cb (GtkWidget *button, TestData12 *data)
{
	gnome_db_format_entry_set_decimal_places (data->entry, 
						  atoi (gtk_entry_get_text (GTK_ENTRY (data->n_decimals))));
}

static void
format_entry_test_max_length_set_cb (GtkWidget *button, TestData12 *data)
{
	gnome_db_format_entry_set_max_length (data->entry, 
					      atoi (gtk_entry_get_text (GTK_ENTRY (data->max_length))));
}

static void
format_entry_test_decimal_sep_set_cb (GtkWidget *button, TestData12 *data)
{
	const gchar *th, *dec;

	th = gtk_entry_get_text (GTK_ENTRY (data->thousands));
	dec = gtk_entry_get_text (GTK_ENTRY (data->decimals));
	gnome_db_format_entry_set_separators (data->entry, (dec && *dec) ? *dec : 0, (th && *th) ? *th : 0);
}

static void
format_entry_test_thousands_sep_set_cb (GtkWidget *button, TestData12 *data)
{
	const gchar *th, *dec;

	th = gtk_entry_get_text (GTK_ENTRY (data->thousands));
	dec = gtk_entry_get_text (GTK_ENTRY (data->decimals));
	gnome_db_format_entry_set_separators (data->entry, (dec && *dec) ? *dec : 0, (th && *th) ? *th : 0);
}

static void
format_entry_test_show_value_cb (GtkWidget *button, TestData12 *data)
{
	gchar *str;

	str = gnome_db_format_entry_get_text (data->entry);
	g_print ("GnomeDbFormatEntry's text is now: #%s#\n", str);
	g_free (str);
}

static void
format_entry_test_set_text_cb (GtkWidget *button, TestData12 *data)
{
	gtk_entry_set_text (GTK_ENTRY (data->entry), gtk_entry_get_text (GTK_ENTRY (data->set_text)));
}

/*
 * GnomeDbDsnSelector test
 */
static GtkWidget *
dsn_selector_test_make (MainConfig *config)
{
	GtkWidget *ret, *wid, *dsnsel;
	
	ret = gtk_table_new (2, 2, FALSE);
	gtk_table_set_row_spacings (GTK_TABLE (ret), 5);
	wid = gtk_label_new ("");
	gtk_label_set_markup (GTK_LABEL (wid), "<b>DSN:</b>");
	gtk_misc_set_alignment (GTK_MISC (wid), 0., 0.);
	gtk_table_attach (GTK_TABLE (ret), wid, 0, 1, 0, 1, GTK_FILL, GTK_FILL, 0, 0);

	dsnsel = gnome_db_dsn_selector_new ();
	gtk_table_attach (GTK_TABLE (ret), dsnsel, 1, 2, 0, 1, GTK_FILL, 0, 0, 0);

	gtk_widget_show_all (ret);

	return ret;
}


/*
 * GnomeDbDsnEditor test
 */

static void 
dsn_editor_dsn_changed_cb (GnomeDbDsnSelector *dsel, GnomeDbDsnEditor *editor)
{
	GdaDsnInfo *info;
	const gchar *dsn;
	dsn = gnome_db_dsn_selector_get_dsn (GNOME_DB_DSN_SELECTOR (dsel));
	info = gda_config_get_dsn_info (dsn);
	gnome_db_dsn_editor_set_dsn (GNOME_DB_DSN_EDITOR (editor), info);
}

static GtkWidget *
dsn_editor_test_make (MainConfig *config)
{
	GtkWidget *ret, *wid, *dsel;
	
	ret = gtk_table_new (2, 2, FALSE);
	gtk_table_set_row_spacings (GTK_TABLE (ret), 5);
	wid = gtk_label_new ("");
	gtk_label_set_markup (GTK_LABEL (wid), "<b>DSN:</b>");
	gtk_misc_set_alignment (GTK_MISC (wid), 0., 0.);
	gtk_table_attach (GTK_TABLE (ret), wid, 0, 1, 0, 1, GTK_FILL, GTK_FILL, 0, 0);

	dsel = gnome_db_dsn_selector_new ();
	gnome_db_combo_add_undef_choice (GNOME_DB_COMBO (dsel), TRUE);
	gtk_table_attach (GTK_TABLE (ret), dsel, 1, 2, 0, 1, GTK_FILL, 0, 0, 0);

	wid = gtk_label_new ("");
	gtk_label_set_markup (GTK_LABEL (wid), "<b>DSN editor:</b>");
	gtk_misc_set_alignment (GTK_MISC (wid), 0., 0.);
	gtk_table_attach (GTK_TABLE (ret), wid, 0, 1, 1, 2, GTK_FILL, GTK_FILL, 0, 0);

	wid = gnome_db_dsn_editor_new ();
	gtk_table_attach_defaults (GTK_TABLE (ret), wid, 1, 2, 1, 2);
	g_signal_connect (dsel, "changed", G_CALLBACK (dsn_editor_dsn_changed_cb), wid);

	dsn_editor_dsn_changed_cb (GNOME_DB_DSN_SELECTOR (dsel), GNOME_DB_DSN_EDITOR (wid));

	gtk_widget_show_all (ret);

	return ret;
}

/*
 * GnomeDbDsnAssistant test
 */
static GtkWidget *
dsn_assistant_test_make (MainConfig *config)
{
	GtkWidget *ret, *wid;
	
	ret = gtk_table_new (3, 3, FALSE);
	wid = gnome_db_dsn_assistant_new ();
	gtk_widget_show (wid);

	return ret;
}

/*
 * GnomeDbProviderSelector test
 */
static GtkWidget *
provider_selector_test_make (MainConfig *config)
{
	GtkWidget *ret, *wid, *psel;
	
	ret = gtk_table_new (2, 2, FALSE);
	gtk_table_set_row_spacings (GTK_TABLE (ret), 5);
	wid = gtk_label_new ("");
	gtk_label_set_markup (GTK_LABEL (wid), "<b>Provider:</b>");
	gtk_misc_set_alignment (GTK_MISC (wid), 0., 0.);
	gtk_table_attach (GTK_TABLE (ret), wid, 0, 1, 0, 1, GTK_FILL, GTK_FILL, 0, 0);

	psel = gnome_db_provider_selector_new ();
	gtk_table_attach (GTK_TABLE (ret), psel, 1, 2, 0, 1, GTK_FILL, 0, 0, 0);

	gtk_widget_show_all (ret);

	return ret;
}

/*
 * GnomeDbProviderSpecEditor test
 */
static void
provider_spec_prov_changed_cb (GnomeDbProviderSelector *prov_sel, GnomeDbProviderSpecEditor *editor)
{
	gnome_db_provider_spec_editor_set_provider (GNOME_DB_PROVIDER_SPEC_EDITOR (editor), 
						    gnome_db_provider_selector_get_provider (GNOME_DB_PROVIDER_SELECTOR (prov_sel)));
}

static GtkWidget *
provider_spec_editor_test_make (MainConfig *config)
{
	GtkWidget *ret, *wid, *psel;
	
	ret = gtk_table_new (2, 2, FALSE);
	gtk_table_set_row_spacings (GTK_TABLE (ret), 5);
	wid = gtk_label_new ("");
	gtk_label_set_markup (GTK_LABEL (wid), "<b>Provider:</b>");
	gtk_misc_set_alignment (GTK_MISC (wid), 0., 0.);
	gtk_table_attach (GTK_TABLE (ret), wid, 0, 1, 0, 1, GTK_FILL, GTK_FILL, 0, 0);

	psel = gnome_db_provider_selector_new ();
	gnome_db_combo_add_undef_choice (GNOME_DB_COMBO (psel), TRUE);
	gtk_table_attach (GTK_TABLE (ret), psel, 1, 2, 0, 1, GTK_FILL, 0, 0, 0);

	wid = gtk_label_new ("");
	gtk_label_set_markup (GTK_LABEL (wid), "<b>Spec editor:</b>");
	gtk_misc_set_alignment (GTK_MISC (wid), 0., 0.);
	gtk_table_attach (GTK_TABLE (ret), wid, 0, 1, 1, 2, GTK_FILL, GTK_FILL, 0, 0);

	wid = gnome_db_provider_spec_editor_new (NULL);
	gtk_table_attach_defaults (GTK_TABLE (ret), wid, 1, 2, 1, 2);
	g_signal_connect (psel, "changed", G_CALLBACK (provider_spec_prov_changed_cb), wid);

	provider_spec_prov_changed_cb (GNOME_DB_PROVIDER_SELECTOR (psel), GNOME_DB_PROVIDER_SPEC_EDITOR (wid));

	gtk_widget_show_all (ret);

	return ret;
}

/*
 * GnomeDbProviderAuthEditor test
 */
static void
provider_auth_prov_changed_cb (GnomeDbProviderSelector *prov_sel, GnomeDbProviderAuthEditor *editor)
{
	gnome_db_provider_auth_editor_set_provider (GNOME_DB_PROVIDER_AUTH_EDITOR (editor), 
						    gnome_db_provider_selector_get_provider (GNOME_DB_PROVIDER_SELECTOR (prov_sel)));
}

static GtkWidget *
provider_auth_editor_test_make (MainConfig *config)
{
	GtkWidget *ret, *wid, *psel;
	
	ret = gtk_table_new (2, 2, FALSE);
	gtk_table_set_row_spacings (GTK_TABLE (ret), 5);
	wid = gtk_label_new ("");
	gtk_label_set_markup (GTK_LABEL (wid), "<b>Provider:</b>");
	gtk_misc_set_alignment (GTK_MISC (wid), 0., 0.);
	gtk_table_attach (GTK_TABLE (ret), wid, 0, 1, 0, 1, GTK_FILL, GTK_FILL, 0, 0);

	psel = gnome_db_provider_selector_new ();
	gnome_db_combo_add_undef_choice (GNOME_DB_COMBO (psel), TRUE);
	gtk_table_attach (GTK_TABLE (ret), psel, 1, 2, 0, 1, GTK_FILL, 0, 0, 0);

	wid = gtk_label_new ("");
	gtk_label_set_markup (GTK_LABEL (wid), "<b>Authentication editor:</b>");
	gtk_misc_set_alignment (GTK_MISC (wid), 0., 0.);
	gtk_table_attach (GTK_TABLE (ret), wid, 0, 1, 1, 2, GTK_FILL, GTK_FILL, 0, 0);

	wid = gnome_db_provider_auth_editor_new (NULL);
	gtk_table_attach_defaults (GTK_TABLE (ret), wid, 1, 2, 1, 2);
	g_signal_connect (psel, "changed", G_CALLBACK (provider_auth_prov_changed_cb), wid);

	provider_auth_prov_changed_cb (GNOME_DB_PROVIDER_SELECTOR (psel), GNOME_DB_PROVIDER_AUTH_EDITOR (wid));

	gtk_widget_show_all (ret);

	return ret;
}

/*
 * GnomeDbConnectionProperties test
 */
static GtkWidget *
connection_properties_test_make (MainConfig *config)
{
	GtkWidget *vbox, *wid;
	
	vbox = gtk_vbox_new (FALSE, 5);
	wid = gnome_db_connection_properties_new (config->cnc);
	gtk_box_pack_start (GTK_BOX (vbox), wid, TRUE, TRUE, 0);
	gtk_widget_show (wid);

	return vbox;
}
