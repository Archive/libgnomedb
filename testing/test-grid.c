/* GNOME DB test program
 * Copyright (C) 1999 - 2006 The GNOME Foundation.
 *
 * AUTHORS:
 * 	Rodrigo Moya <rodrigo@gnome-db.org>
 *      Vivien Malerba <malerba@gnome-db.org>
 *
 * This Program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this Program; see the file COPYING.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <libgda/libgda.h>
#include <libgnomedb/libgnomedb.h>
#include <libgnomedb/gnome-db-grid.h>
#ifdef BUILD_WITH_GNOME
#include <libgnomedb/gnome-db-window.h>
#include <libgnomeui/gnome-ui-init.h>
#else
#include <gtk/gtk.h>
#endif

static void
window_closed_cb (GtkWindow *window, gpointer user_data)
{
	gnome_db_main_quit ();
}

static GdaDataModel *
create_data_model (void)
{
	gint i;
	GdaDataModel *model;

	model = gda_data_model_array_new (6);

	for (i = 0; i < 50; i++) {
		GList *values = NULL;
		GValue *tmpval;

		g_value_set_string (tmpval = gda_value_new (G_TYPE_STRING), "This is a string");
		values = g_list_append (values, tmpval);

		g_value_set_int (tmpval = gda_value_new (G_TYPE_INT), 200);
		values = g_list_append (values, tmpval);

		g_value_set_boolean (tmpval = gda_value_new (G_TYPE_BOOLEAN), FALSE);
		values = g_list_append (values, tmpval);

		g_value_set_float (tmpval = gda_value_new (G_TYPE_FLOAT), 3.1416);
		values = g_list_append (values, tmpval);

		g_value_set_string (tmpval = gda_value_new (G_TYPE_STRING), "Another string");
		values = g_list_append (values, tmpval);

		g_value_set_double (tmpval = gda_value_new (G_TYPE_DOUBLE), 4560.45672113323093);
		values = g_list_append (values, tmpval);

		gda_data_model_append_values (model, values);

		g_list_foreach (values, (GFunc) gda_value_free, NULL);
		g_list_free (values);
	}

	return model;
}

static void
create_window (gpointer user_data)
{
	GtkWidget *window;
	GtkWidget *grid;
	GdaDataModel *model;

#ifdef BUILD_WITH_GNOME
	window = gnome_db_window_new ("test-grid", NULL, NULL, NULL, NULL);
#else
	window = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_window_set_title (GTK_WINDOW(window), "test-grid");
#endif
	g_signal_connect (G_OBJECT (window), "close", G_CALLBACK (window_closed_cb), NULL);
	gtk_widget_set_usize (window, 500, 400);
	grid = gnome_db_grid_new ();
	gtk_widget_show (grid);
#ifdef BUILD_WITH_GNOME
	gnome_db_window_set_contents (GNOME_DB_WINDOW (window), grid);
#else
	gtk_container_add (GTK_CONTAINER (window), grid);
#endif

	/* create the data model to be shown */
	model = create_data_model ();
	gnome_db_grid_set_model (GNOME_DB_GRID (grid), GDA_DATA_MODEL (model));
	g_object_unref (G_OBJECT (model));
	
#ifdef BUILD_WITH_GNOME
	gnome_db_window_show (GNOME_DB_WINDOW (window));
#else
	gtk_widget_show_all (window);
#endif
}

int
main (int argc, char *argv[])
{
	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
	textdomain (GETTEXT_PACKAGE);	

	gda_init ("test-grid", PACKAGE_VERSION, argc, argv);
#ifdef BUILD_WITH_GNOME
	gnome_program_init ("test-grid", PACKAGE_VERSION,
                            LIBGNOMEUI_MODULE,
                            argc, argv, NULL);
#else
	gtk_init (&argc, &argv);
#endif

	gnome_db_main_run ((GdaInitFunc) create_window, NULL);
	return 0;
}
