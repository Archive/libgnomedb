#include <stdio.h>
#include <string.h>
#include "../libgnomedb/libgnomedb.h"

/* options */
gchar *pass = NULL;
gchar *user = NULL;
gboolean with_dt = FALSE;
gboolean with_func = FALSE;
gboolean with_agg = FALSE;
gboolean with_tables = FALSE;
gboolean with_views = FALSE;
gchar *table_fields = NULL;
gboolean show_contents = FALSE;

static GOptionEntry entries[] = 
{
  { "user", 'U', 0, G_OPTION_ARG_STRING, &user, "Username", "username" },
  { "password", 'P', 0, G_OPTION_ARG_STRING, &pass, "Password", "password" },
  { "data-types", 'y', 0, G_OPTION_ARG_NONE, &with_dt, "Test data types", NULL },
  { "functions", 'p', 0, G_OPTION_ARG_NONE, &with_func, "Test functions", NULL },
  { "aggregates", 'a', 0, G_OPTION_ARG_NONE, &with_agg, "Test aggregates", NULL },
  { "tables", 't', 0, G_OPTION_ARG_NONE, &with_tables, "Test tables", NULL },
  { "views", 'v', 0, G_OPTION_ARG_NONE, &with_views, "Test views", NULL },
  { "table-fields", 'f', 0, G_OPTION_ARG_STRING, &table_fields, "Test table fields", "table" },
  { "details", 'd', 0, G_OPTION_ARG_NONE, &show_contents, "Show the details of each tested section", NULL },
  { NULL }
};

static void test_data_types (GdaConnection *conn);
static void test_functions (GdaConnection *conn);
static void test_aggregates (GdaConnection *conn);
static void test_tables (GdaConnection *conn);
static void test_views (GdaConnection *conn);
static void test_table_fields (GdaConnection *conn);

static void test_dsn (GdaDsnInfo *dsn);

gint
main (int argc, char **argv) {
	GError *error = NULL;	
	GOptionContext *context;
	
	gtk_set_locale ();

	/* command line parsing */
	context = g_option_context_new ("<Data source> - Provider schemas testing");
	g_option_context_add_main_entries (context, entries, GETTEXT_PACKAGE);
	g_option_context_add_group (context, gtk_get_option_group (TRUE));
	if (!g_option_context_parse (context, &argc, &argv, &error)) {
		g_warning ("Can't parse arguments: %s", error->message);
		exit (1);
	}
	g_option_context_free (context);

	/* if no show option is provided, then set all of them */
	if (!table_fields && !with_dt && !with_func && !with_agg && !with_tables && !with_views) 
		with_dt = with_func = with_agg = with_tables = with_views = TRUE;

	gnome_db_init ();
	gtk_init (&argc, &argv);

	if (argc == 2) {
		GdaDsnInfo *dsn;
		dsn = gda_config_find_data_source (argv[1]);
		
		if (!dsn) {
			g_warning ("Can't find data source: %s\n", argv[1]);
			exit (1);
		}
		test_dsn (dsn);
		gda_data_source_info_free (dsn);
	}
	else {
		GList *dsns, *list;

		dsns = gda_config_get_data_source_list ();
		list = dsns;
		while (list) {
			if (list != dsns)
				g_print ("\n\n");
			test_dsn ((GdaDsnInfo *) (list->data));
			list = g_list_next (list);
		}
		gda_config_free_data_source_list (dsns);
	}

	return 0;
}

static void
test_dsn (GdaDsnInfo *dsn)
{
	static GdaClient *client = NULL;
	GdaConnection *conn;
	GError *error = NULL;

	g_print ("*********** Testing DSN ***********\n");
	g_print ("* name:     %s\n", dsn->name);
	g_print ("* provider: %s\n", dsn->provider);
	g_print ("***********************************\n");

	if (!client)
		client = gda_client_new ();

	conn = gda_client_open_connection (client, dsn->name, user, pass, 0, &error);	
	if (! conn) {
		if (error) {
			g_warning ("Can't open connection: user=%s, pass=%s: %s\n", user, pass, 
				   error->message ? error->message : "Unknown");
			g_error_free (error);
		}
		else
			g_warning ("Can't open connection: user=%s, pass=%s\n", user, pass);
	}
	else {
		if (with_dt)
			test_data_types (conn);
		if (with_func)
			test_functions (conn);
		if (with_agg)
			test_aggregates (conn);
		if (with_tables)
			test_tables (conn);
		if (with_views)
			test_views (conn);
		if (table_fields)
			test_table_fields (conn);
		gda_connection_close (conn);
	}
}


static void
test_data_types (GdaConnection *conn)
{
	GdaDataModel *rs;

	if (show_contents)
		g_print ("------------------- DATA TYPES -------------------\n");
	rs = gda_connection_get_schema (conn, GDA_CONNECTION_SCHEMA_TYPES, NULL);
	if (!rs) {
		g_warning ("Can't get list of data types");
		return;
	}
	
	if (!gnome_db_result_set_check_data_model (rs, 4, 
						   G_TYPE_STRING, 
						   G_TYPE_STRING,
						   G_TYPE_STRING,
						   G_TYPE_ULONG)) 
		g_warning ("Schema for list of data types is wrong");
	else
		g_print ("Schema for list of data types is OK\n");
	
	if (show_contents)
		gda_data_model_dump (rs, stdout);
	g_object_unref (G_OBJECT (rs));
}

static void
test_functions (GdaConnection *conn)
{
	GdaDataModel *rs;

	if (show_contents)
		g_print ("------------------- FUNCTIONS - PROCEDURES -------------------\n");
	rs = gda_connection_get_schema (conn, GDA_CONNECTION_SCHEMA_PROCEDURES, NULL);
	if (!rs) {
		g_warning ("Can't get list of functions/procedures");
		return;
	}
	
	if (!gnome_db_result_set_check_data_model (rs, 8,
						   G_TYPE_STRING,
						   G_TYPE_STRING,
						   G_TYPE_STRING,
						   G_TYPE_STRING,
						   G_TYPE_STRING,
						   G_TYPE_INT,
						   G_TYPE_STRING,
						   G_TYPE_STRING))
		g_warning ("Schema for list of functions/procedures is wrong");
	else
		g_print ("Schema for list of functions/procedures is OK\n");
	
	if (show_contents)
		gda_data_model_dump (rs, stdout);
	g_object_unref (G_OBJECT (rs));
}

static void
test_aggregates (GdaConnection *conn)
{
	GdaDataModel *rs;

	if (show_contents)
		g_print ("------------------- AGGREGATES - PROCEDURES -------------------\n");
	rs = gda_connection_get_schema (conn, GDA_CONNECTION_SCHEMA_AGGREGATES, NULL);
	if (!rs) {
		g_warning ("Can't get list of aggregates");
		return;
	}
	
	if (!gnome_db_result_set_check_data_model (rs, 7,
                                                   G_TYPE_STRING,
                                                   G_TYPE_STRING,
                                                   G_TYPE_STRING,
                                                   G_TYPE_STRING,
                                                   G_TYPE_STRING,
                                                   G_TYPE_STRING,
                                                   G_TYPE_STRING))
		g_warning ("Schema for list of aggregates is wrong");
	else
		g_print ("Schema for list of aggregates is OK\n");
	
	if (show_contents)
		gda_data_model_dump (rs, stdout);
	g_object_unref (G_OBJECT (rs));
}

static void
test_tables (GdaConnection *conn)
{
	GdaDataModel *rs;

	if (show_contents)
		g_print ("------------------- TABLES -------------------\n");
	rs = gda_connection_get_schema (conn, GDA_CONNECTION_SCHEMA_TABLES, NULL);
	if (!rs) {
		g_warning ("Can't get list of tables");
		return;
	}
	
	if (!gnome_db_result_set_check_data_model (rs, 4,
                                                   G_TYPE_STRING,
                                                   G_TYPE_STRING,
                                                   G_TYPE_STRING,
                                                   G_TYPE_STRING))
		g_warning ("Schema for list of tables is wrong");
	else
		g_print ("Schema for list of tables is OK\n");
	
	if (show_contents)
		gda_data_model_dump (rs, stdout);
	g_object_unref (G_OBJECT (rs));
}

static void
test_views (GdaConnection *conn)
{
	GdaDataModel *rs;

	if (show_contents)
		g_print ("------------------- VIEWS -------------------\n");
	rs = gda_connection_get_schema (conn, GDA_CONNECTION_SCHEMA_VIEWS, NULL);
	if (!rs) {
		g_warning ("Can't get list of views");
		return;
	}
	
	if (!gnome_db_result_set_check_data_model (rs, 4,
                                                   G_TYPE_STRING,
                                                   G_TYPE_STRING,
                                                   G_TYPE_STRING,
                                                   G_TYPE_STRING))
		g_warning ("Schema for list of views is wrong");
	else
		g_print ("Schema for list of views is OK\n");
	
	if (show_contents)
		gda_data_model_dump (rs, stdout);
	g_object_unref (G_OBJECT (rs));
}

static void
test_table_fields (GdaConnection *conn)
{
	GdaDataModel *rs;
	GdaSet *paramlist;
	GdaHolder *param;

	if (show_contents)
		g_print ("------------------- TABLES's fields -------------------\n");

	paramlist = gda_set_new ();
        param = gda_holder_new_string ("name", table_fields);
        gda_set_add_parameter (paramlist, param);
        rs = gda_connection_get_schema (conn, GDA_CONNECTION_SCHEMA_FIELDS, paramlist);
        gda_set_free (paramlist);
	
	if (!rs) {
		g_warning ("Can't get list of fields for table '%s'", table_fields);
		return;
	}
	
	if (!gnome_db_result_set_check_data_model (rs, 9,
                                                   G_TYPE_STRING,
                                                   G_TYPE_STRING,
                                                   G_TYPE_INT,
                                                   G_TYPE_INT,
                                                   G_TYPE_BOOLEAN,
                                                   G_TYPE_BOOLEAN,
                                                   G_TYPE_BOOLEAN,
                                                   G_TYPE_STRING, -1))
		g_warning ("Schema for list of tables is wrong");
	else
		g_print ("Schema for list of tables is OK\n");
	
	if (show_contents)
		gda_data_model_dump (rs, stdout);
	g_object_unref (G_OBJECT (rs));
}
