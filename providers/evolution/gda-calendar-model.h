/* GDA Evolution Provider
 * Copyright (C) 2003 Rodrigo Moya
 *
 * AUTHORS:
 *         Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#if !defined(__gda_calendar_model_h__)
#  define __gda_calendar_model_h__

#include <libgda/gda-connection.h>
#include <libgda/gda-data-model-array.h>
#include <libecal/e-cal.h>

G_BEGIN_DECLS

#define GDA_TYPE_CALENDAR_MODEL            (gda_calendar_model_get_type())
#define GDA_CALENDAR_MODEL(obj)            (G_TYPE_CHECK_INSTANCE_CAST (obj, GDA_TYPE_CALENDAR_MODEL, GdaCalendarModel))
#define GDA_CALENDAR_MODEL_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST (klass, GDA_TYPE_CALENDAR_MODEL, GdaCalendarModelClass))
#define GDA_IS_CALENDAR_MODEL(obj)         (G_TYPE_CHECK_INSTANCE_TYPE (obj, GDA_TYPE_CALENDAR_MODEL))
#define GDA_IS_CALENDAR_MODEL_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GDA_TYPE_CALENDAR_MODEL))

typedef struct {
	GdaDataModelArray parent;

	/* private */
	GdaConnection *cnc;
	ECal *cal_client;
	ECalSourceType obj_type;
	GList *uids;
} GdaCalendarModel;

typedef struct {
	GdaDataModelArrayClass parent_class;
} GdaCalendarModelClass;

GType         gda_calendar_model_get_type (void) G_GNUC_CONST;
GdaDataModel *gda_calendar_model_new (GdaConnection *cnc, ECal *cal_client, ECalSourceType obj_type);

G_END_DECLS

#endif
