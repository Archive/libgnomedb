/* GDA Evolution Provider
 * Copyright (C) 2003 Rodrigo Moya
 *
 * AUTHORS:
 *         Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#if !defined(_gda_evolution_connection_h__)
#  define __gda_evolution_connection_h__

#include <libgda/gda-connection.h>
#include <libgda/gda-data-model.h>

gboolean      gda_evolution_connection_init (GdaConnection *cnc);
gboolean      gda_evolution_connection_dispose (GdaConnection *cnc);

GdaDataModel *gda_evolution_connection_get_table (GdaConnection *cnc, const gchar *name);
GdaDataModel *gda_evolution_connection_get_fields_schema (GdaConnection *cnc,
							  GdaSet *params);
GdaDataModel *gda_evolution_connection_get_tables_schema (GdaConnection *cnc,
							  GdaSet *params);
GdaDataModel *gda_evolution_connection_get_types_schema (GdaConnection *cnc);

#endif
