/* GDA Evolution Provider
 * Copyright (C) 2003 Rodrigo Moya
 *
 * AUTHORS:
 *         Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <bonobo-activation/bonobo-activation.h>
#include <glib/gi18n.h>
#include <libecal/e-cal.h>
#include <libebook/e-book.h>
#include "gda-evolution-connection.h"
#include "gda-calendar-model.h"

typedef struct {
	GdaConnection *cnc;
	GHashTable *tables;

	/* calendar data */
	ECal *cal_client;
	ECal *tasks_client;

	/* contacts data */
	EBook *e_book;
} GdaEvolutionConnectionPrivate;

static void
add_table (GdaEvolutionConnectionPrivate *priv, const gchar *name, GdaDataModel *model)
{
	g_return_if_fail (GDA_IS_DATA_MODEL (model));

	g_hash_table_insert (priv->tables, g_strdup (name), model);
}

static void
calendar_opened_cb (ECal *client, CalClientOpenStatus status, gpointer user_data)
{
	GdaEvolutionConnectionPrivate *priv = user_data;

	if (status == CAL_CLIENT_OPEN_SUCCESS) {
		if (priv->cal_client == client) {
			add_table (priv, "calendar", gda_calendar_model_new (client, E_CAL_SOURCE_TYPE_EVENT));
		} else if (priv->tasks_client == client) {
			add_table (priv, "tasks", gda_calendar_model_new (client, E_CAL_SOURCE_TYPE_TODO));
		}
	} else {
		if (priv->cal_client == client) {
			gda_connection_add_event_string (priv->cnc,
							 _("Could not open the default calendar folder"));
		} else if (priv->tasks_client == client) {
			gda_connection_add_event_string (priv->cnc,
							 _("Could not open the default tasks folder"));
		}
	}
}

static void
book_opened_cb (EBook *book, EBookStatus status, gpointer user_data)
{
	GdaEvolutionConnectionPrivate *priv = user_data;

	if (status == E_BOOK_STATUS_SUCCESS) {
	} else if (status == E_BOOK_STATUS_AUTHENTICATION_REQUIRED) {
		gda_connection_add_event_string (
			priv->cnc,
			_("Authentication is required to access the default contacts folder"));
	} else {
		gda_connection_add_event_string (priv->cnc,
						 _("Could not open the default contacts folder"));
	}
}

gboolean
gda_evolution_connection_init (GdaConnection *cnc)
{
	gint success = 0;
	GdaEvolutionConnectionPrivate *priv;

	/* initialize needed libraries */
	if (!bonobo_activation_is_initialized ()) {
		int argc = 0;
		char *argv[1] = { NULL };

		bonobo_activation_init (argc, argv);
		if (!bonobo_init (&argc, argv)) {
			gda_connection_add_event_string (cnc, _("Could not initialize Bonobo"));
			return FALSE;
		}
	}

	/* create the private structure */
	priv = g_new0 (GdaEvolutionConnectionPrivate, 1);
	priv->cnc = cnc;
	priv->tables = g_hash_table_new (g_str_hash, g_str_equal);

	/* connect to calendar */
	priv->cal_client = cal_client_new ();
	if (cal_client_open_default_calendar (priv->cal_client, FALSE))
		success++;
	else {
		g_object_unref (priv->cal_client);
		priv->cal_client = NULL;
	}

	/* connect to tasks */
	priv->tasks_client = cal_client_new ();
	g_signal_connect (G_OBJECT (priv->tasks_client), "cal_opened",
			  G_CALLBACK (calendar_opened_cb), priv);
	if (cal_client_open_default_tasks (priv->tasks_client, FALSE))
		success++;
	else {
		g_object_unref (priv->tasks_client);
		priv->tasks_client = NULL;
	}

	/* connect to contacts */
	priv->e_book = e_book_new ();
	e_book_load_default_book (priv->e_book, book_opened_cb, priv);

	g_object_set_data (G_OBJECT (cnc), "GDA_Evolution_ConnectionPrivate", priv);

	return success > 0 ? TRUE : FALSE;
}

static gboolean
remove_table_hash (gpointer key, gpointer value, gpointer user_data)
{
	g_free (key);
	g_object_unref (value);

	return TRUE;
}

gboolean
gda_evolution_connection_dispose (GdaConnection *cnc)
{
	GdaEvolutionConnectionPrivate *priv;

	priv = g_object_get_data (G_OBJECT (cnc), "GDA_Evolution_ConnectionPrivate");
	g_return_val_if_fail (priv != NULL, FALSE);

	g_hash_table_foreach_remove (priv->tables, (GHRFunc) remove_table_hash, NULL);
	g_hash_table_destroy (priv->tables);
	priv->tables = NULL;

	if (priv->cal_client) {
		g_object_unref (priv->cal_client);
		priv->cal_client = NULL;
	}

	if (priv->tasks_client) {
		g_object_unref (priv->tasks_client);
		priv->tasks_client = NULL;
	}

	if (priv->e_book) {
		g_object_unref (priv->e_book);
		priv->e_book = NULL;
	}

	g_free (priv);
	g_object_set_data (G_OBJECT (cnc), "GDA_Evolution_ConnectionPrivate", NULL);

	return TRUE;
}

GdaDataModel *
gda_evolution_connection_get_table (GdaConnection *cnc, const gchar *name)
{
	GdaEvolutionConnectionPrivate *priv;
	GdaDataModel *model;

	priv = g_object_get_data (G_OBJECT (cnc), "GDA_Evolution_ConnectionPrivate");
	g_return_val_if_fail (priv != NULL, NULL);

	model = g_hash_table_lookup (priv->tables, name);
	if (GDA_IS_DATA_MODEL (model))
		g_object_ref (model);
	else
		model = NULL;

	return model;
}

GdaDataModel *
gda_evolution_connection_get_fields_schema (GdaConnection *cnc, GdaSet *params)
{
	GdaDataModelArray *model;
	GdaDataModel *table;
	const gchar *table_name;
	GdaHolder *par;
	GdaEvolutionConnectionPrivate *priv;
	gint r, total;
	struct {
		const gchar *name;
		GType type;
	} fields_desc[] = {
		{ N_("Field name")	, G_TYPE_STRING  },
		{ N_("Data type")	, G_TYPE_STRING  },
		{ N_("Size")		, G_TYPE_INT },
		{ N_("Scale")		, G_TYPE_INT },
		{ N_("Not null?")	, G_TYPE_BOOLEAN },
		{ N_("Primary key?")	, G_TYPE_BOOLEAN },
		{ N_("Unique index?")	, G_TYPE_BOOLEAN },
		{ N_("References")	, G_TYPE_STRING  },
		{ N_("Default value")   , G_TYPE_STRING  }
	};

	priv = g_object_get_data (G_OBJECT (cnc), "GDA_Evolution_ConnectionPrivate");
	g_return_val_if_fail (priv != NULL, NULL);

	/* get parameters sent by client */
	par = gda_set_find (params, "name");
	if (!par) {
		gda_connection_add_event_string (
			cnc,
			_("Table name is needed but none specified in parameter list"));
		return NULL;
	}

	table_name = g_value_get_string ((GValue *) gda_holder_get_value (par));
	if (!table_name) {
		gda_connection_add_event_string (
			cnc,
			_("Table name is needed but none specified in parameter list"));
		return NULL;
	}

	table = gda_evolution_connection_get_table (cnc, table_name);
	if (!GDA_IS_DATA_MODEL (table)) {
		gda_connection_add_event_string (
			cnc,
			_("Table %s does not exist"), table_name);
		return NULL;
	}

	/* create the model */
	model = gda_data_model_array_new (9);
	for (r = 0; r < sizeof (fields_desc) / sizeof (fields_desc[0]); r++) {
		gda_data_model_set_column_title (GDA_DATA_MODEL (model), r,
						_(fields_desc[r].name));
	}

	total = gda_data_model_get_n_columns (table);
	for (r = 0; r < total; r++) {
		GdaColumn *fa;

		fa = gda_data_model_describe_column (GDA_DATA_MODEL (table), r);
		if (fa) {
			GList *value_list = NULL;

			value_list = g_list_append (
				value_list,
				TO_REMOVE_new_string (gda_column_get_name (fa)));
			value_list = g_list_append (
				value_list,
				TO_REMOVE_new_string (
					gda_g_type_to_string (gda_column_get_g_type (fa))));
			value_list = g_list_append (
				value_list,
				TO_REMOVE_new_integer (gda_column_get_defined_size (fa)));
			value_list = g_list_append (
				value_list,
				TO_REMOVE_new_integer (gda_column_get_scale (fa)));
			value_list = g_list_append (
				value_list,
				TO_REMOVE_new_boolean (!gda_column_get_allow_null (fa)));
			value_list = g_list_append (
				value_list,
				TO_REMOVE_new_boolean (gda_column_get_primary_key (fa)));
			value_list = g_list_append (
				value_list,
				TO_REMOVE_new_boolean (gda_column_get_unique_key (fa)));
			value_list = g_list_append (
				value_list,
				TO_REMOVE_new_string (gda_column_get_references (fa)));
			value_list = g_list_append (
				value_list,
				TO_REMOVE_new_string (""));

			gda_data_model_append_values (GDA_DATA_MODEL (model), value_list);

			gda_column_free (fa);
			g_list_foreach (value_list, (GFunc) gda_value_free, NULL);
			g_list_free (value_list);
		}
	}

	return model;
}

static void
add_table_schema (gpointer key, gpointer value, gpointer user_data)
{
	GList *value_list = NULL;
	gchar *table_name = key;
	GdaDataModelArray *model = user_data;

	value_list = g_list_append (value_list, TO_REMOVE_new_string (table_name));
	value_list = g_list_append (value_list, TO_REMOVE_new_string (g_get_user_name ()));

	if (!strcmp (table_name, "calendar")) {
		value_list = g_list_append (value_list,
					    TO_REMOVE_new_string (_("Scheduled events")));
	} else if (!strcmp (table_name, "tasks")) {
		value_list = g_list_append (value_list,
					    TO_REMOVE_new_string (_("TODO list")));
	}

	value_list = g_list_append (value_list, TO_REMOVE_new_string (""));

	gda_data_model_append_values (GDA_DATA_MODEL (model), value_list);

	g_list_foreach (value_list, (GFunc) gda_value_free, NULL);
	g_list_free (value_list);
}

GdaDataModel *
gda_evolution_connection_get_tables_schema (GdaConnection *cnc, GdaSet *params)
{
	GdaEvolutionConnectionPrivate *priv;
	GdaDataModelArray *model;

	priv = g_object_get_data (G_OBJECT (cnc), "GDA_Evolution_ConnectionPrivate");
	g_return_val_if_fail (priv != NULL, NULL);

	model = gda_data_model_array_new (4);
	gda_data_model_set_column_title (GDA_DATA_MODEL (model), 0, _("Name"));
	gda_data_model_set_column_title (GDA_DATA_MODEL (model), 1, _("Owner"));
	gda_data_model_set_column_title (GDA_DATA_MODEL (model), 2, _("Comments"));
	gda_data_model_set_column_title (GDA_DATA_MODEL (model), 3, "SQL");

	g_hash_table_foreach (priv->tables, (GHFunc) add_table_schema, model);

	return model;
}

GdaDataModel *
gda_evolution_connection_get_types_schema (GdaConnection *cnc)
{
	GdaEvolutionConnectionPrivate *priv;
	GdaDataModelArray *model;
	gint i;
	struct {
		const gchar *name;
		const gchar *owner;
		const gchar *comments;
		GType type;
	} types[] = {
		{ "string", "", "String", G_TYPE_STRING },
		{ "timestamp", "", "Date & Time values", GDA_TYPE_TIMESTAMP },
	};

	g_return_val_if_fail (GDA_IS_CONNECTION (cnc), NULL);

	priv = g_object_get_data (G_OBJECT (cnc), "GDA_Evolution_ConnectionPrivate");
	g_return_val_if_fail (priv != NULL, NULL);

	model = (GdaDataModelArray *) gda_data_model_array_new (4);
	gda_data_model_set_column_title (GDA_DATA_MODEL (model), 0, _("Type"));
	gda_data_model_set_column_title (GDA_DATA_MODEL (model), 1, _("Owner"));
	gda_data_model_set_column_title (GDA_DATA_MODEL (model), 2, _("Comments"));
	gda_data_model_set_column_title (GDA_DATA_MODEL (model), 3, _("GDA type"));

	/* fill the model */
	for (i = 0; i < sizeof (types) / sizeof (types[0]); i++) {
		GList *value_list = NULL;

		value_list = g_list_append (value_list, TO_REMOVE_new_string (types[i].name));
		value_list = g_list_append (value_list, TO_REMOVE_new_string (types[i].owner));
		value_list = g_list_append (value_list, TO_REMOVE_new_string (types[i].comments));
		value_list = g_list_append (value_list, TO_REMOVE_new_gdatype (types[i].type));

		gda_data_model_append_values (GDA_DATA_MODEL (model), value_list);

		g_list_foreach (value_list, (GFunc) gda_value_free, NULL);
		g_list_free (value_list);
	}

	return GDA_DATA_MODEL (model);
}
