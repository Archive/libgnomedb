/*
 * Copyright (C) 2006 - 2008
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
 *
 * Authors:
 *   Juan Pablo Ugarte
 *   Daniel Espinosa Ortiz <esodan@gmail.com>
 */
#include <gladeui/glade.h>
#include <libgnomedb/libgnomedb.h>
#include <string.h>

#ifdef G_OS_WIN32
#define GLADEGNOMEDB_API __declspec(dllexport)
#else
#define GLADEGNOMEDB_API
#endif

/* This function does absolutely nothing
 * (and is for use in overriding post_create functions).
 */
void GLADEGNOMEDB_API
empty (GObject *container, GladeCreateReason reason)
{
}

/* Catalog init function */
void GLADEGNOMEDB_API
glade_gnome_db_init (void)
{
	//Initation of the libgnomedb library
	
	gnome_db_init ();
}

/* Widgets' post init functions */

void
do_data_entry_post_init (GnomeDbDataEntry *object, GType type)
{
	GdaDataHandler *dh;
	
	dh = gda_get_default_handler (type);
	g_object_set (object, "handler", dh, NULL);
	gnome_db_data_entry_set_value_type (GNOME_DB_DATA_ENTRY (object), type);
}

void
glade_gnome_db_entry_boolean_post_init (GObject *adaptor, GObject *object, GladeCreateReason reason)
{
	do_data_entry_post_init (GNOME_DB_DATA_ENTRY (object), G_TYPE_BOOLEAN);
}

void
glade_gnome_db_entry_string_post_init (GObject *adaptor, GObject *object, GladeCreateReason reason)
{
	do_data_entry_post_init (GNOME_DB_DATA_ENTRY (object), G_TYPE_STRING);
}

void
glade_gnome_db_entry_time_post_init (GObject *adaptor, GObject *object, GladeCreateReason reason)
{
	do_data_entry_post_init (GNOME_DB_DATA_ENTRY (object), GDA_TYPE_TIME);
}

void
glade_gnome_db_entry_date_post_init (GObject *adaptor, GObject *object, GladeCreateReason reason)
{
	do_data_entry_post_init (GNOME_DB_DATA_ENTRY (object), G_TYPE_DATE);
}

void
glade_gnome_db_entry_timestamp_post_init (GObject *adaptor, GObject *object, GladeCreateReason reason)
{
	do_data_entry_post_init (GNOME_DB_DATA_ENTRY (object), GDA_TYPE_TIMESTAMP);
}

void
glade_gnome_db_entry_filesel_post_init (GObject *adaptor, GObject *object, GladeCreateReason reason)
{
	do_data_entry_post_init (GNOME_DB_DATA_ENTRY (object), G_TYPE_STRING);
}

void
glade_gnome_db_entry_text_post_init (GObject *adaptor, GObject *object, GladeCreateReason reason)
{
	do_data_entry_post_init (GNOME_DB_DATA_ENTRY (object), G_TYPE_STRING);
}

void
glade_gnome_db_entry_pass_post_init (GObject *adaptor, GObject *object, GladeCreateReason reason)
{
	do_data_entry_post_init (GNOME_DB_DATA_ENTRY (object), G_TYPE_STRING);
}

/* custom properties's functions */
void GLADEGNOMEDB_API
glade_gnome_db_basic_form_prop_set (GladeWidgetAdaptor *adaptor,
				    GObject *object,
				    const gchar *property_name,
				    const GValue *value)
{
	g_print ("Set %s\n", property_name);
	if (!strcmp (property_name, "paramlist-text")) {
		const gchar *spec = g_value_get_string (value);
		GdaSet *plist;

		plist = gda_set_new_from_spec_string (spec, NULL);
		if (plist) {
			g_object_set (object, "paramlist", plist, NULL);
			g_object_unref (plist);
		}
	}
	else
		GWA_GET_CLASS (G_TYPE_OBJECT)->set_property (adaptor, object, property_name, value);
}

/* custom properties's functions */
void GLADEGNOMEDB_API
glade_gnome_db_basic_form_prop_get (GladeWidgetAdaptor *adaptor,
				    GObject *object,
				    const gchar *property_name,
				    GValue *value)
{
	g_print ("Get %s\n", property_name);
	if (!strcmp (property_name, "paramlist-text")) {
		g_print ("Get paramlist\n");
	}
	else
		GWA_GET_CLASS (G_TYPE_OBJECT)->get_property (adaptor, object, property_name, value);
}
