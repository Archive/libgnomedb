<!-- ##### SECTION Title ##### -->
GnomeDbDataProxy

<!-- ##### SECTION Short_Description ##### -->
Proxy to hold modifications for any #GdaDataModel, and provides the #GtkTreeModel interface

<!-- ##### SECTION Long_Description ##### -->
<para>
  This object stores all the modifications made to a #GdaDataModel object until asked to make the changes
  inside the #GdaDataModel.
</para>
<para>
  This object implements the #GtkTreeModel interface (which makes it easy to display data contents in a 
  <link linkend="GtkTreeView">GtkTreeView</link>) through which the data values can be tested for modifications. 
  The #GtkTreeModel implementation has the following columns:
<itemizedlist mark="bullet">
  <listitem><para>PROXY_COL_MODEL_N_COLUMNS: gives the number of columns in the #GdaDataModel (for the un-modified data)</para></listitem>
  <listitem><para>PROXY_COL_MODEL_ROW: gives the number of rows in the #GdaDataModel (for the un-modified data)</para></listitem>
  <listitem><para>PROXY_COL_MODEL_POINTER: gives a pointer to the #GdaDataModel itself</para></listitem>
  <listitem><para>PROXY_COL_MODIFIED: TRUE if the row has been modified (compared to what's in the #GdaDataModel)</para></listitem>
  <listitem><para>PROXY_COL_TO_DELETE: TRUE if the row has been marked to be deleted</para></listitem>
  <listitem><para>0 &lt;= column &lt; number of columns in the #GdaDataModel: the #GValue for the column in the corresponding row</para></listitem>
  <listitem><para>number of columns in the #GdaDataModel &lt;= column &lt; 2*number of columns in the #GdaDataModel: the attributes of the value for the column in the corresponding row; the attribute is a guint value of OR'ed fields from the #GdaValueAttribute enumerated.</para></listitem>
</itemizedlist>
Note that only the PROXY_COL_TO_DELETE column and the columns corresponding to the #GValue values are writable, and that all the columns can be read.
</para>

<!-- ##### SECTION See_Also ##### -->
<para>

</para>

<!-- ##### SECTION Stability_Level ##### -->


<!-- ##### STRUCT GnomeDbDataProxy ##### -->
<para>

</para>


<!-- ##### ARG GnomeDbDataProxy:autocommit ##### -->
<para>

</para>

<!-- ##### ARG GnomeDbDataProxy:prepend-null-entry ##### -->
<para>

</para>

<!-- ##### FUNCTION gnome_db_data_proxy_get_type ##### -->
<para>

</para>

@Returns: 


<!-- ##### FUNCTION gnome_db_data_proxy_new ##### -->
<para>

</para>

@model: 
@Returns: 


<!-- ##### FUNCTION gnome_db_data_proxy_is_read_only ##### -->
<para>

</para>

@proxy: 
@Returns: 


<!-- ##### FUNCTION gnome_db_data_proxy_get_n_rows ##### -->
<para>

</para>

@proxy: 
@Returns: 


<!-- ##### FUNCTION gnome_db_data_proxy_get_n_columns ##### -->
<para>

</para>

@proxy: 
@Returns: 


<!-- ##### FUNCTION gnome_db_data_proxy_set_value ##### -->
<para>

</para>

@proxy: 
@iter: 
@col: 
@value: 
@Returns: 


<!-- ##### FUNCTION gnome_db_data_proxy_get_value ##### -->
<para>

</para>

@proxy: 
@iter: 
@col: 
@Returns: 


<!-- ##### FUNCTION gnome_db_data_proxy_get_values ##### -->
<para>

</para>

@proxy: 
@iter: 
@cols_index: 
@n_cols: 
@Returns: 


<!-- ##### FUNCTION gnome_db_data_proxy_delete ##### -->
<para>

</para>

@proxy: 
@iter: 


<!-- ##### FUNCTION gnome_db_data_proxy_undelete ##### -->
<para>

</para>

@proxy: 
@iter: 


<!-- ##### FUNCTION gnome_db_data_proxy_append ##### -->
<para>

</para>

@proxy: 
@iter: 


<!-- ##### FUNCTION gnome_db_data_proxy_get_iter_from_values ##### -->
<para>

</para>

@proxy: 
@iter: 
@values: 
@cols_index: 
@Returns: 


<!-- ##### FUNCTION gnome_db_data_proxy_get_model ##### -->
<para>

</para>

@proxy: 
@Returns: 


<!-- ##### FUNCTION gnome_db_data_proxy_has_been_modified ##### -->
<para>

</para>

@proxy: 
@Returns: 


<!-- ##### FUNCTION gnome_db_data_proxy_reset_all ##### -->
<para>

</para>

@proxy: 


<!-- ##### FUNCTION gnome_db_data_proxy_reset_value ##### -->
<para>

</para>

@proxy: 
@iter: 
@col: 


<!-- ##### FUNCTION gnome_db_data_proxy_commit_all ##### -->
<para>

</para>

@proxy: 
@error: 
@Returns: 


<!-- ##### FUNCTION gnome_db_data_proxy_commit_row ##### -->
<para>

</para>

@proxy: 
@iter: 
@error: 
@Returns: 


<!-- ##### FUNCTION gnome_db_data_proxy_set_sample_size ##### -->
<para>

</para>

@proxy: 
@sample_size: 


<!-- ##### FUNCTION gnome_db_data_proxy_get_sample_size ##### -->
<para>

</para>

@proxy: 
@Returns: 


<!-- ##### FUNCTION gnome_db_data_proxy_set_sample_start ##### -->
<para>

</para>

@proxy: 
@sample_start: 


<!-- ##### FUNCTION gnome_db_data_proxy_get_sample_start ##### -->
<para>

</para>

@proxy: 
@Returns: 


<!-- ##### FUNCTION gnome_db_data_proxy_get_sample_end ##### -->
<para>

</para>

@proxy: 
@Returns: 


