@echo off
del *~
del *.wixobj
REM compute all the .wxs to .wixobj
for %%f in (*.wxs) do candle.exe "%%f"

REM Create gda-module.msm
light.exe -out gda-module.msm gda-module.wixobj glib-fragment.wixobj gda-share.wixobj bdb.wixobj mdb.wixobj mysql.wixobj postgres.wixobj

REM Create gda-sql.msi
light.exe gda-sql.wixobj
