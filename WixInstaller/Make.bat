@echo off

REM Values to change include VERSION and INSTALLDIR, both below.

REM The subdirectory to install into
SET INSTALLDIR="GnomeDb-4.0"

if NOT "%1"=="" SET VERSION="%1"
if NOT "%1"=="" GOTO GOT_VERSION

REM The full version number of the build in XXXX.XX.XX format
SET VERSION="3.99.3"

echo.
echo Version not specified - defaulting to %VERSION%
echo.

:GOT_VERSION

echo.
echo Compiling individual .wxs files

for %%f in (*.wxs) do (
candle -nologo -dVERSION=%VERSION% -dINSTALLDIR=%INSTALLDIR% "%%f"
IF ERRORLEVEL 1 GOTO ERR_HANDLER
)

echo.
echo Creating GnomeDb module...
light -nologo -out gnomedb-module-%VERSION%.msm gnomedb-module.wixobj gtk-fragment.wixobj gnomedb-share.wixobj
IF ERRORLEVEL 1 GOTO ERR_HANDLER

echo.
echo Creating GnomeDbProperties installer
light -nologo -out gnome-db-properties-%VERSION%.msi -dVERSION=%VERSION% -dINSTALLDIR=%INSTALLDIR% -ext WixUIExtension -cultures:en-us gnomedb-properties.wixobj
IF ERRORLEVEL 1 GOTO ERR_HANDLER

echo.
echo Done!
GOTO EXIT

:ERR_HANDLER
echo.
echo Aborting build!
GOTO EXIT

:EXIT
