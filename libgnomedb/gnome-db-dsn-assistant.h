/* GNOME DB library
 * Copyright (C) 1999 - 2008 The GNOME Foundation
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *      Vivien Malerba <malerba@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GNOME_DB_DSN_ASSISTANT_H__
#define __GNOME_DB_DSN_ASSISTANT_H__

#include <libgda/gda-config.h>
#include <gtk/gtkassistant.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_DSN_ASSISTANT            (gnome_db_dsn_assistant_get_type())
#define GNOME_DB_DSN_ASSISTANT(obj)            (G_TYPE_CHECK_INSTANCE_CAST (obj, GNOME_DB_TYPE_DSN_ASSISTANT, GnomeDbDsnAssistant))
#define GNOME_DB_DSN_ASSISTANT_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST (klass, GNOME_DB_TYPE_DSN_ASSISTANT, GnomeDbDsnAssistantClass))
#define GNOME_DB_IS_DSN_ASSISTANT(obj)         (G_TYPE_CHECK_INSTANCE_TYPE (obj, GNOME_DB_TYPE_DSN_ASSISTANT))
#define GNOME_DB_IS_DSN_ASSISTANT_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GNOME_DB_TYPE_DSN_ASSISTANT))

typedef struct _GnomeDbDsnAssistant        GnomeDbDsnAssistant;
typedef struct _GnomeDbDsnAssistantClass   GnomeDbDsnAssistantClass;
typedef struct _GnomeDbDsnAssistantPrivate GnomeDbDsnAssistantPrivate;

struct _GnomeDbDsnAssistant {
	GtkAssistant                assistant;
	GnomeDbDsnAssistantPrivate *priv;
};

struct _GnomeDbDsnAssistantClass {
	GtkAssistantClass           parent_class;

	void                     (* finished) (GnomeDbDsnAssistant *assistant, gboolean error);
};

GType                    gnome_db_dsn_assistant_get_type (void) G_GNUC_CONST;

GtkWidget               *gnome_db_dsn_assistant_new      (void);
const GdaDsnInfo *gnome_db_dsn_assistant_get_dsn  (GnomeDbDsnAssistant *assistant);

G_END_DECLS

#endif
