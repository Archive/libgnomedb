/* GNOME DB library
 * Copyright (C) 1999 - 2008 The GNOME Foundation.
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *      Vivien Malerba <malerba@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "gnome-db-decl.h"
#include <libgda/gda-config.h>
#include <libgnomedb/gnome-db-dsn-selector.h>
#ifdef HAVE_GTKTWOTEN
#include <libgnomedb/gnome-db-dsn-assistant.h>
#endif
#include <glib/gi18n-lib.h>
#include <libgnomedb/gnome-db-login.h>
#include <libgnomedb/gnome-db-util.h>
#include <libgnomedb/gnome-db-basic-form.h>
#include <libgnomedb/gnome-db-provider-auth-editor.h>
#include <gtk/gtklabel.h>
#include <gtk/gtktable.h>
#include <gtk/gtkstock.h>

struct _GnomeDbLoginPrivate {
	gchar     *dsn_name;
	GtkWidget *dsn_label;
	GtkWidget *dsn_entry;
	GtkWidget *create_dsn_button;
	gboolean   hiding_button;
	gboolean   hiding_dsn;

	GtkWidget *auth_widget;
};

static void gnome_db_login_class_init   (GnomeDbLoginClass *klass);
static void gnome_db_login_init         (GnomeDbLogin *login, GnomeDbLoginClass *klass);
static void gnome_db_login_set_property (GObject *object,
					 guint paramid,
					 const GValue *value,
					 GParamSpec *pspec);
static void gnome_db_login_get_property (GObject *object,
					 guint param_id,
					 GValue *value,
					 GParamSpec *pspec);
static void gnome_db_login_finalize     (GObject *object);

enum {
	PROP_0,
	PROP_DSN,
	PROP_USERNAME,
	PROP_PASSWORD,
	PROP_HIDING_BUTTON,
	PROP_HIDING_DSN
};

static GObjectClass *parent_class = NULL;

/* signals */
enum
{
        DSN_CHANGED,
        LAST_SIGNAL
};

static gint gnome_db_login_signals[LAST_SIGNAL] = { 0 };


#ifdef HAVE_GTKTWOTEN
static void
assistant_finished_cb (GnomeDbDsnAssistant *assistant, gboolean error, GnomeDbLogin *login);
static void
add_dsn_cb (GtkButton *button, GnomeDbLogin *login);
#endif
static void
dsn_entry_changed_cb (GnomeDbDsnSelector *sel, GnomeDbLogin *login);

/*
 * GnomeDbLogin class implementation
 */

static void
gnome_db_login_class_init (GnomeDbLoginClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	gnome_db_login_signals[DSN_CHANGED] =
                g_signal_new ("dsn-changed",
                              G_TYPE_FROM_CLASS (object_class),
                              G_SIGNAL_RUN_FIRST,
                              G_STRUCT_OFFSET (GnomeDbLoginClass, dsn_changed),
                              NULL, NULL,
                              g_cclosure_marshal_VOID__VOID, G_TYPE_NONE,
                              0);

	klass->dsn_changed = NULL;

	object_class->set_property = gnome_db_login_set_property;
	object_class->get_property = gnome_db_login_get_property;
	object_class->finalize = gnome_db_login_finalize;

	/* add class properties */
	g_object_class_install_property (
		object_class, PROP_DSN,
		g_param_spec_string ("dsn", NULL, NULL, NULL, G_PARAM_READABLE));
	g_object_class_install_property (
		object_class, PROP_USERNAME,
		g_param_spec_string ("username", NULL, NULL, NULL, G_PARAM_READABLE));
	g_object_class_install_property (
		object_class, PROP_PASSWORD,
		g_param_spec_string ("password", NULL, NULL, NULL, G_PARAM_READABLE));
	g_object_class_install_property (
		object_class, PROP_HIDING_BUTTON,
		g_param_spec_boolean("hiding_button", NULL, NULL, TRUE, G_PARAM_READWRITE));
	g_object_class_install_property (
		object_class, PROP_HIDING_DSN,
		g_param_spec_boolean("hiding_dsn", NULL, NULL, FALSE, G_PARAM_READWRITE));
		
}

static void
gnome_db_login_init (GnomeDbLogin *login, GnomeDbLoginClass *klass)
{
	GtkWidget *table;
	GtkWidget *label;
	
	/* allocate the internal structure */
	login->priv = g_new0 (GnomeDbLoginPrivate, 1);
	
	/* Init the properties*/
	login->priv->dsn_name = NULL;
	login->priv->hiding_button = TRUE;
	login->priv->hiding_dsn = FALSE;
	
	/* construct the widget */
	table = gnome_db_new_table_widget (3, 3, FALSE);
	gtk_box_pack_start (GTK_BOX (login), table, TRUE, TRUE, 0);
	
	/* Create the Label for the data source selector*/
	label = gnome_db_new_label_widget (_("Data Source:"));
        gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
	gtk_label_set_selectable (GTK_LABEL (label), FALSE);
        gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 0, 1, GTK_FILL, GTK_FILL, 0, 0);
	login->priv->dsn_label = label;
	
	/* Create the DSN selector*/
	login->priv->dsn_entry = gnome_db_dsn_selector_new ();
	gnome_db_dsn_selector_set_dsn (GNOME_DB_DSN_SELECTOR (login->priv->dsn_entry), login->priv->dsn_name);
	gtk_widget_show (login->priv->dsn_entry); /* Show the DSN selector */
	gtk_table_attach (GTK_TABLE (table), login->priv->dsn_entry, 1, 2, 0, 1,
			  GTK_FILL | GTK_EXPAND | GTK_SHRINK, GTK_FILL, 0, 0);
	g_signal_connect (G_OBJECT (login->priv->dsn_entry), "changed",
			  G_CALLBACK (dsn_entry_changed_cb), login);
			  
	/* Create the DSN add button */
#ifdef HAVE_GTKTWOTEN
	login->priv->create_dsn_button = gnome_db_new_button_widget_from_stock (GTK_STOCK_ADD);
	g_signal_connect (G_OBJECT (login->priv->create_dsn_button), "clicked",
			  G_CALLBACK (add_dsn_cb), login);
	gtk_widget_hide (login->priv->create_dsn_button); /* Hide the create DSN button */
	gtk_table_attach (GTK_TABLE (table), login->priv->create_dsn_button, 2, 3, 0, 1,
			  GTK_FILL, GTK_FILL, 0, 0);
#else
	login->priv->create_dsn_button = NULL;
#endif
			  
	/* Create the authentication part */
	login->priv->auth_widget = gnome_db_provider_auth_editor_new (NULL);
	gtk_table_attach_defaults (GTK_TABLE (table), login->priv->auth_widget, 0, 3, 1, 2);
	gtk_widget_show (login->priv->auth_widget);
}

static void
gnome_db_login_set_property (GObject *object,
			     guint param_id,
			     const GValue *value,
			     GParamSpec *pspec)
{
	GnomeDbLogin *login = (GnomeDbLogin *) object;

	g_return_if_fail (GNOME_DB_IS_LOGIN (login));

	switch (param_id) {
	
	case PROP_HIDING_BUTTON:
		gnome_db_login_set_enable_create_button(login, !g_value_get_boolean(value));
		break;
	case PROP_HIDING_DSN:
		gnome_db_login_set_show_dsn_selector(login, !g_value_get_boolean(value));
		break;
	default :
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}

static void
gnome_db_login_get_property (GObject *object,
			       guint param_id,
			       GValue *value,
			       GParamSpec *pspec)
{
	GnomeDbLogin *login = (GnomeDbLogin *) object;

	g_return_if_fail (GNOME_DB_IS_LOGIN (login));

	switch (param_id) {
	case PROP_DSN :
		g_value_set_string (value, gnome_db_login_get_dsn (login));
		break;
	case PROP_USERNAME :
		g_value_set_string (value, gnome_db_login_get_username (login));
		break;
	case PROP_PASSWORD :
		g_value_set_string (value, gnome_db_login_get_password (login));
		break;
	case PROP_HIDING_BUTTON:
		g_value_set_boolean(value, gnome_db_login_get_enable_create_button(login));
		break;
	case PROP_HIDING_DSN:
		g_value_set_boolean(value, gnome_db_login_get_show_dsn_selector(login));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}

static void
gnome_db_login_finalize (GObject *object)
{
	GnomeDbLogin *login = (GnomeDbLogin *) object;

	g_return_if_fail (GNOME_DB_IS_LOGIN (login));

	/* free memory */
	if (login->priv->dsn_name != NULL) {
		g_free (login->priv->dsn_name);
		login->priv->dsn_name = NULL;
	}

	g_free (login->priv);
	login->priv = NULL;

	parent_class->finalize (object);
}

GType
gnome_db_login_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0)) {
		static const GTypeInfo info = {
			sizeof (GnomeDbLoginClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_login_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbLogin),
			0,
			(GInstanceInitFunc) gnome_db_login_init
		};
		type = g_type_register_static (GTK_TYPE_VBOX, "GnomeDbLogin", &info, 0);
	}
	return type;
}

#ifdef HAVE_GTKTWOTEN
static gboolean
idle_assistant_destroy_cb (GtkWidget *assist)
{
	gtk_widget_destroy (assist);
	return FALSE;
}

static void
assistant_finished_cb (GnomeDbDsnAssistant *assistant, gboolean error, GnomeDbLogin *login)
{
	GtkWidget *toplevel;

	toplevel = gtk_widget_get_toplevel (GTK_WIDGET (login));
	g_idle_add_full (G_PRIORITY_HIGH_IDLE, (GSourceFunc) idle_assistant_destroy_cb, 
			 gtk_widget_get_toplevel (GTK_WIDGET (assistant)), NULL);
	gtk_widget_set_sensitive (toplevel, TRUE);
	gtk_window_set_modal (GTK_WINDOW (toplevel), 
			      GPOINTER_TO_INT (g_object_get_data (G_OBJECT (toplevel), "__modal")));
}

static void
add_dsn_cb (GtkButton *button, GnomeDbLogin *login)
{
	GtkWidget *assistant;
	GtkWidget *toplevel;
	gboolean is_modal;

	toplevel = gtk_widget_get_toplevel (GTK_WIDGET (login));
	gtk_widget_set_sensitive (toplevel, FALSE);
	is_modal = gtk_window_get_modal (GTK_WINDOW (toplevel));
	g_object_set_data (G_OBJECT (toplevel), "__modal", GINT_TO_POINTER (is_modal));
	gtk_window_set_modal (GTK_WINDOW (toplevel), FALSE);

	assistant = gnome_db_dsn_assistant_new ();
	gtk_window_set_modal (GTK_WINDOW (assistant), is_modal);
	gtk_window_set_transient_for (GTK_WINDOW (assistant), GTK_WINDOW (toplevel));

	g_signal_connect (G_OBJECT (assistant), "finished",
			  G_CALLBACK (assistant_finished_cb), login);
	gtk_widget_show (assistant);
}
#endif

static void
dsn_entry_changed_cb (GnomeDbDsnSelector *sel, GnomeDbLogin *login)
{
	gchar *dsn;
	GdaDsnInfo *info = NULL;
        
        dsn = gnome_db_dsn_selector_get_dsn (sel);
        
	info = gda_config_get_dsn_info (dsn);

	if (info)
		TO_IMPLEMENT;
}

/**
 * gnome_db_login_new
 * @dsn:
 *
 *
 *
 * Returns:
 */
GtkWidget *
gnome_db_login_new (const gchar *dsn)
{
	GtkWidget *login;

	login = GTK_WIDGET(g_object_new (GNOME_DB_TYPE_LOGIN, NULL));
	return login;
}

/**
 * gnome_db_login_get_dsn
 * @login:
 *
 *
 *
 * Returns:
 */
const gchar *
gnome_db_login_get_dsn (GnomeDbLogin *login)
{
	g_return_val_if_fail (GNOME_DB_IS_LOGIN (login), NULL);

	if (login->priv->dsn_name)
		return (const gchar *) login->priv->dsn_name;

	return gnome_db_dsn_selector_get_dsn (GNOME_DB_DSN_SELECTOR (login->priv->dsn_entry));
}

/**
 * gnome_db_login_set_dsn
 * @login: A #GnomeDbLogin widget.
 * @dsn: Data source name.
 *
 * Set the data source name on the given #GnomeDbLogin widget.
 */
void
gnome_db_login_set_dsn (GnomeDbLogin *login, const gchar *dsn)
{
	GdaDsnInfo *src;
	g_return_if_fail (GNOME_DB_IS_LOGIN (login));

	src = gda_config_get_dsn_info (dsn);
	if (!src)
		g_warning (_("Datasource '%s' is not declared"), dsn);
	else {
		gnome_db_dsn_selector_set_dsn (GNOME_DB_DSN_SELECTOR (login->priv->dsn_entry), 
					       dsn);
		gnome_db_provider_auth_editor_set_provider 
			(GNOME_DB_PROVIDER_AUTH_EDITOR (login->priv->auth_widget), src->provider);
		g_signal_emit (login, gnome_db_login_signals[DSN_CHANGED], 0);
	}
}

/**
 * gnome_db_login_get_auth
 * @login: a #GnomeDbLogin widget
 *
 * Returns: the authentication string
 */
const gchar *
gnome_db_login_get_auth (GnomeDbLogin *login)
{
	g_return_val_if_fail (GNOME_DB_IS_LOGIN (login), NULL);
	if (! login->priv->auth_widget)
		return NULL;

	return gnome_db_provider_auth_editor_get_auth (GNOME_DB_PROVIDER_AUTH_EDITOR (login->priv->auth_widget));
}

/**
 * gnome_db_login_set_auth
 * @login: a #GnomeDbLogin widget
 * @auth: an authentication string, or %NULL
 *
 * Update the authentication part of @login using @auth
 */
void
gnome_db_login_set_auth (GnomeDbLogin *login, const gchar *auth)
{
	g_return_if_fail (GNOME_DB_IS_LOGIN (login));
	if (! login->priv->auth_widget)
		return;

	gnome_db_provider_auth_editor_set_auth (GNOME_DB_PROVIDER_AUTH_EDITOR (login->priv->auth_widget),
						auth);
}

/**
 * gnome_db_login_get_username
 * @login: A #GnomeDbLogin widget.
 *
 * Get the value currently entered in the username text entry.
 *
 * Returns: the username being entered in the login widget.
 */
const gchar *
gnome_db_login_get_username (GnomeDbLogin *login)
{
	g_return_val_if_fail (GNOME_DB_IS_LOGIN (login), NULL);
	TO_IMPLEMENT;
	return NULL;
}

/**
 * gnome_db_login_set_username
 * @login: a #GnomeDbLogin widget.
 * @username: username to display in the widget.
 *
 * Sets the user name to be displayed in the given #GnomeDbLogin widget.
 */
void
gnome_db_login_set_username (GnomeDbLogin *login, const gchar *username)
{
	g_return_if_fail (GNOME_DB_IS_LOGIN (login));
	TO_IMPLEMENT;
}

/**
 * gnome_db_login_get_password
 * @login:
 *
 *
 *
 * Returns:
 */
const gchar *
gnome_db_login_get_password (GnomeDbLogin *login)
{
	g_return_val_if_fail (GNOME_DB_IS_LOGIN (login), NULL);
	TO_IMPLEMENT;
	return NULL;
}

/**
 * gnome_db_login_set_password
 * @login: a #GnomeDbLogin widget.
 * @password: password to display in the widget.
 *
 * Sets the password to be displayed in the given #GnomeDbLogin widget.
 */
void
gnome_db_login_set_password (GnomeDbLogin *login, const gchar *password)
{
	g_return_if_fail (GNOME_DB_IS_LOGIN (login));
	TO_IMPLEMENT;
}

/**
 * gnome_db_login_set_enable_create_button
 * @login: A #GnomeDbLogin widget.
 * @enable: Flag to enable/disable the button.
 *
 * Enable/disable the button allowing the user to create new data sources
 * from the login widget. This button is disabled by default, and when enabled,
 * allows the user to start the database properties configuration applet.
 */
void
gnome_db_login_set_enable_create_button (GnomeDbLogin *login, gboolean enable)
{
	g_return_if_fail (GNOME_DB_IS_LOGIN (login));
	
	if (login->priv->hiding_button != enable)
		return;

	if (!login->priv->create_dsn_button)
		return;

	login->priv->hiding_button = !enable;
	if (enable) {
		if (!login->priv->hiding_dsn)
			gtk_widget_show (login->priv->create_dsn_button);
	} else 
		gtk_widget_hide (login->priv->create_dsn_button);
}

/**
 * gnome_db_login_get_enable_create_button
 * @login: A #GnomeDbLogin widget.
 *
 * Tell if the the button allowing the user to create new data sources is shown
 * or not. This button is disabled by default,
 *
 * Returns: TRUE if the button is shown
 */
gboolean
gnome_db_login_get_enable_create_button (GnomeDbLogin *login)
{
	g_return_val_if_fail (GNOME_DB_IS_LOGIN (login), FALSE);

	return ! login->priv->hiding_button;
}


/**
 * gnome_db_login_set_show_dsn_selector
 * @login: A #GnomeDbLogin widget.
 * @show:
 *
 * Show/hide the combo box which allows to select the datasource to log into.
 */
void
gnome_db_login_set_show_dsn_selector (GnomeDbLogin *login, gboolean show)
{
	g_return_if_fail (GNOME_DB_IS_LOGIN (login));

	login->priv->hiding_dsn = !show;
	if (show) {
		gtk_widget_show (login->priv->dsn_entry);
		gtk_widget_show (login->priv->dsn_label);
		if (login->priv->create_dsn_button && !login->priv->hiding_button)
			gtk_widget_show (login->priv->create_dsn_button);
	}
	else {
		gtk_widget_hide (login->priv->dsn_entry);
		gtk_widget_hide (login->priv->dsn_label);
		if (login->priv->create_dsn_button)
			gtk_widget_hide (login->priv->create_dsn_button);
	}
}

/**
 * gnome_db_login_get_show_dsn_selector
 * @login: A #GnomeDbLogin widget.
 *
 * Tells if the combo box which allows to select the datasource to log into is shown
 * or not.
 *
 * Returns: TRUE if the combo box is shown
 */
gboolean
gnome_db_login_get_show_dsn_selector (GnomeDbLogin *login)
{
	g_return_val_if_fail (GNOME_DB_IS_LOGIN (login), FALSE);

	return ! login->priv->hiding_dsn;
}
