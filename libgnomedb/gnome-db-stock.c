/* GNOME DB library
 * Copyright (C) 1999 - 2006 The GNOME Foundation.
 *
 * AUTHORS:
 * 	Rodrigo Moya <rodrigo@gnome-db.org>
 *      vivien Malerba <malerba@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gtk/gtkiconfactory.h>
#include <gtk/gtkstock.h>
#include <libgnomedb/gnome-db-stock.h>
#include <glib/gi18n-lib.h>
#include <libgda/gda-log.h>
#include <libgnomedb/binreloc/gnome-db-binreloc.h>

static GtkStockItem builtin_icons [] = {
	{ GNOME_DB_STOCK_AGGREGATES, N_("Aggregates"), 0, 0, GETTEXT_PACKAGE },
	{ GNOME_DB_STOCK_COMMIT, N_("Commit"), 0, 0, GETTEXT_PACKAGE },
	{ GNOME_DB_STOCK_CONNECT, N_("Open connection"), 0, 0, GETTEXT_PACKAGE },
	{ GNOME_DB_STOCK_DATABASE, N_("Database"), 0, 0, GETTEXT_PACKAGE },
	{ GNOME_DB_STOCK_DESIGNER, N_("Designer"), 0, 0, GETTEXT_PACKAGE },
	{ GNOME_DB_STOCK_DISCONNECT, N_("Close connection"), 0, 0, GETTEXT_PACKAGE },
	{ GNOME_DB_STOCK_NO_TRANSACTION, N_("NoTransaction"), 0, 0, GETTEXT_PACKAGE },
	{ GNOME_DB_STOCK_PROCEDURES, N_("Procedures"), 0, 0, GETTEXT_PACKAGE },
	{ GNOME_DB_STOCK_QUERY, N_("Query"), 0, 0, GETTEXT_PACKAGE },
	{ GNOME_DB_STOCK_ROLLBACK, N_("Rollback"), 0, 0, GETTEXT_PACKAGE },
	{ GNOME_DB_STOCK_SEQUENCES, N_("Sequences"), 0, 0, GETTEXT_PACKAGE },
	{ GNOME_DB_STOCK_SQL, N_("SQL"), 0, 0, GETTEXT_PACKAGE },
	{ GNOME_DB_STOCK_TABLES, N_("Tables"), 0, 0, GETTEXT_PACKAGE },
	{ GNOME_DB_STOCK_TYPES, N_("Types"), 0, 0, GETTEXT_PACKAGE },
	{ GNOME_DB_STOCK_WITHIN_TRANSACTION, N_("WithinTransaction"), 0, 0, GETTEXT_PACKAGE },
	{ GNOME_DB_STOCK_SQL_CONSOLE, N_("SQLConsole"), 0, 0, GETTEXT_PACKAGE },
};

void gnome_db_stock_init (void);

/*
 * Private functions
 */

static void
add_sized (GtkIconFactory *factory,
	   gchar *pixfile,
	   GtkIconSize size,
	   const gchar *stock_id)
{
	GtkIconSet *set;
	GtkIconSource *source;
	GdkPixbuf *pixbuf;
	GError *err = NULL;

	pixbuf = gdk_pixbuf_new_from_file (pixfile, &err);
	g_free (pixfile);
	if (err) {
		gda_log_error (_("Error: %s"), err->message);
		g_error_free (err);
		return;
	}

	source = gtk_icon_source_new ();
	gtk_icon_source_set_pixbuf (source, pixbuf);
	gtk_icon_source_set_size (source, size);

	set = gtk_icon_set_new ();
	gtk_icon_set_add_source (set, source);

	gtk_icon_factory_add (factory, stock_id, set);

	g_object_unref (G_OBJECT (pixbuf));
	gtk_icon_source_free (source);
	gtk_icon_set_unref (set);
}

static void
get_stock_icons (GtkIconFactory *factory)
{
	add_sized (factory, gnome_db_gbr_get_icon_path ("gnome-db-aggregates_24x24.png"),
		   GTK_ICON_SIZE_BUTTON, GNOME_DB_STOCK_AGGREGATES);
	add_sized (factory, gnome_db_gbr_get_icon_path ("gnome-db-commit_24x24.png"),
		   GTK_ICON_SIZE_BUTTON, GNOME_DB_STOCK_COMMIT);
	add_sized (factory, gnome_db_gbr_get_icon_path ("gnome-db-connect_24x24.png"),
		   GTK_ICON_SIZE_BUTTON, GNOME_DB_STOCK_CONNECT);
	add_sized (factory, gnome_db_gbr_get_icon_path ("gnome-db-database_24x24.png"),
		   GTK_ICON_SIZE_BUTTON, GNOME_DB_STOCK_DATABASE);
	add_sized (factory,  gnome_db_gbr_get_icon_path ("gnome-db-designer_24x24.png"),
		   GTK_ICON_SIZE_BUTTON, GNOME_DB_STOCK_DESIGNER);
	add_sized (factory, gnome_db_gbr_get_icon_path ("gnome-db-disconnect_24x24.png"),
		   GTK_ICON_SIZE_BUTTON, GNOME_DB_STOCK_DISCONNECT);
	add_sized (factory, gnome_db_gbr_get_icon_path ("gnome-db-no-transaction_24x24.png"),
		   GTK_ICON_SIZE_BUTTON, GNOME_DB_STOCK_NO_TRANSACTION);
	add_sized (factory, gnome_db_gbr_get_icon_path ("gnome-db-procedures_24x24.png"),
		   GTK_ICON_SIZE_BUTTON, GNOME_DB_STOCK_PROCEDURES);
	add_sized (factory, gnome_db_gbr_get_icon_path ("gnome-db-query_24x24.png"),
		   GTK_ICON_SIZE_BUTTON, GNOME_DB_STOCK_QUERY);
	add_sized (factory, gnome_db_gbr_get_icon_path ("gnome-db-rollback_24x24.png"),
		   GTK_ICON_SIZE_BUTTON, GNOME_DB_STOCK_ROLLBACK);
	add_sized (factory, gnome_db_gbr_get_icon_path ("gnome-db-sequences_24x24.png"),
		   GTK_ICON_SIZE_BUTTON, GNOME_DB_STOCK_SEQUENCES);
	add_sized (factory,  gnome_db_gbr_get_icon_path ("gnome-db-sql_24x24.png"),
		   GTK_ICON_SIZE_BUTTON, GNOME_DB_STOCK_SQL);
	add_sized (factory,  gnome_db_gbr_get_icon_path ("gnome-db-tables_24x24.png"),
		   GTK_ICON_SIZE_BUTTON, GNOME_DB_STOCK_TABLES);
	add_sized (factory, gnome_db_gbr_get_icon_path ("gnome-db-types_24x24.png"),
		   GTK_ICON_SIZE_BUTTON, GNOME_DB_STOCK_TYPES);
	add_sized (factory, gnome_db_gbr_get_icon_path ("gnome-db-within-transaction_24x24.png"),
		   GTK_ICON_SIZE_BUTTON, GNOME_DB_STOCK_WITHIN_TRANSACTION);
	add_sized (factory, gnome_db_gbr_get_icon_path ("gnome-db-relations_24x24.png"),
                   GTK_ICON_SIZE_BUTTON, GNOME_DB_STOCK_RELATIONS);
	add_sized (factory, gnome_db_gbr_get_icon_path ("gnome-db-console_24x24.png"),
                   GTK_ICON_SIZE_BUTTON, GNOME_DB_STOCK_SQL_CONSOLE);
}

/**
 * gnome_db_stock_init
 *
 * Private function for initialization of the stock icons.
 */
void
gnome_db_stock_init (void)
{
	GtkIconFactory *factory;
	static gboolean initialized = FALSE;

	if (initialized)
		return;

	/* install our stock icons */
	factory = gtk_icon_factory_new ();
	get_stock_icons (factory);
	gtk_icon_factory_add_default (factory);

	gtk_stock_add_static (builtin_icons, G_N_ELEMENTS (builtin_icons));

	initialized = TRUE;
}

/**
 * gnome_db_stock_get_icon_pixbuf
 * @stock_id: ID of icon to get.
 *
 * Get the given stock icon as a GdkPixbuf.
 *
 * Returns: a GdkPixbuf representing the loaded icon.
 */
GdkPixbuf *
gnome_db_stock_get_icon_pixbuf (const gchar *stock_id)
{
	gchar *path;
	GdkPixbuf *pixbuf;

	g_return_val_if_fail (stock_id != NULL, NULL);

	path = gnome_db_stock_get_icon_path (stock_id);
	if (!path)
		return NULL;

	pixbuf = gdk_pixbuf_new_from_file (path, NULL);

	g_free (path);

	return pixbuf;
}

/**
 * gnome_db_stock_get_icon_pixbuf_file
 * @pixmapfile: the real filename of the icon to get.
 *
 * Get the given icon as a GdkPixbuf.
 *
 * Returns: a GdkPixbuf representing the loaded icon.
 */
GdkPixbuf *
gnome_db_stock_get_icon_pixbuf_file (const gchar *pixmapfile)
{
	gchar *path;
	GdkPixbuf *pixbuf;

	g_return_val_if_fail (pixmapfile != NULL, NULL);

	path = gnome_db_gbr_get_icon_path (pixmapfile);
	if (!path)
		return NULL;

	pixbuf = gdk_pixbuf_new_from_file (path, NULL);

	g_free (path);

	return pixbuf;
}

/**
 * gnome_db_stock_get_icon_path
 * @stock_id: Icon id to get path.
 *
 * Return the full path of the given stock icon.
 *
 * Returns: the full path of the given stock icon. The returned
 * value must be freed by the caller when no longer needed.
 */
gchar *
gnome_db_stock_get_icon_path (const gchar *stock_id)
{
	gchar *file, *path;

	g_return_val_if_fail (stock_id != NULL, NULL);
	file = g_strdup_printf ("%s_16x16.png", stock_id);
	path = gnome_db_gbr_get_icon_path (file);
	g_free (file);

	return path;
}
