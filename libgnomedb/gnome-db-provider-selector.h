/* GNOME DB library
 * Copyright (C) 1999 - 2008 The GNOME Foundation.
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *      Vivien Malerba <malerba@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GNOME_DB_PROVIDER_SELECTOR_H__
#define __GNOME_DB_PROVIDER_SELECTOR_H__

#include <libgnomedb/gnome-db-combo.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_PROVIDER_SELECTOR            (gnome_db_provider_selector_get_type())
#define GNOME_DB_PROVIDER_SELECTOR(obj)            (G_TYPE_CHECK_INSTANCE_CAST (obj, GNOME_DB_TYPE_PROVIDER_SELECTOR, GnomeDbProviderSelector))
#define GNOME_DB_PROVIDER_SELECTOR_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST (klass, GNOME_DB_TYPE_PROVIDER_SELECTOR, GnomeDbProviderSelectorClass))
#define GNOME_DB_IS_PROVIDER_SELECTOR(obj)         (G_TYPE_CHECK_INSTANCE_TYPE (obj, GNOME_DB_TYPE_PROVIDER_SELECTOR))
#define GNOME_DB_IS_PROVIDER_SELECTOR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GNOME_DB_TYPE_PROVIDER_SELECTOR))

typedef struct _GnomeDbProviderSelector        GnomeDbProviderSelector;
typedef struct _GnomeDbProviderSelectorClass   GnomeDbProviderSelectorClass;
typedef struct _GnomeDbProviderSelectorPrivate GnomeDbProviderSelectorPrivate;

struct _GnomeDbProviderSelector {
	GnomeDbCombo                    parent;
	GnomeDbProviderSelectorPrivate *priv;
};

struct _GnomeDbProviderSelectorClass {
	GnomeDbComboClass               parent_class;
};

GType              gnome_db_provider_selector_get_type         (void) G_GNUC_CONST;
GtkWidget         *gnome_db_provider_selector_new              (void);

GdaServerProvider *gnome_db_provider_selector_get_provider_obj (GnomeDbProviderSelector *selector);
const gchar       *gnome_db_provider_selector_get_provider     (GnomeDbProviderSelector *selector);
gboolean           gnome_db_provider_selector_set_provider     (GnomeDbProviderSelector *selector, const gchar *provider);

G_END_DECLS

#endif
