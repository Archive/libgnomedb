/* GNOME DB library
 * Copyright (C) 2008 The GNOME Foundation
 *
 * AUTHORS:
 *      Vivien Malerba <malerba@gnome_db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GNOME_DB_PROVIDER_AUTH_EDITOR_H__
#define __GNOME_DB_PROVIDER_AUTH_EDITOR_H__

#include <gtk/gtkvbox.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_PROVIDER_AUTH_EDITOR            (gnome_db_provider_auth_editor_get_type())
#define GNOME_DB_PROVIDER_AUTH_EDITOR(obj)            (G_TYPE_CHECK_INSTANCE_CAST (obj, GNOME_DB_TYPE_PROVIDER_AUTH_EDITOR, GnomeDbProviderAuthEditor))
#define GNOME_DB_PROVIDER_AUTH_EDITOR_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST (klass, GNOME_DB_TYPE_PROVIDER_AUTH_EDITOR, GnomeDbProviderAuthEditorClass))
#define GNOME_DB_IS_PROVIDER_AUTH_EDITOR(obj)         (G_TYPE_CHECK_INSTANCE_TYPE (obj, GNOME_DB_TYPE_PROVIDER_AUTH_EDITOR))
#define GNOME_DB_IS_PROVIDER_AUTH_EDITOR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GNOME_DB_TYPE_PROVIDER_AUTH_EDITOR))

typedef struct _GnomeDbProviderAuthEditor        GnomeDbProviderAuthEditor;
typedef struct _GnomeDbProviderAuthEditorClass   GnomeDbProviderAuthEditorClass;
typedef struct _GnomeDbProviderAuthEditorPrivate GnomeDbProviderAuthEditorPrivate;

struct _GnomeDbProviderAuthEditor {
	GtkVBox                box;
	GnomeDbProviderAuthEditorPrivate *priv;
};

struct _GnomeDbProviderAuthEditorClass {
	GtkVBoxClass           parent_class;

	/* signals */
	void                (* changed) (GnomeDbProviderAuthEditor *auth);
};

GType       gnome_db_provider_auth_editor_get_type     (void) G_GNUC_CONST;
GtkWidget  *gnome_db_provider_auth_editor_new          (const gchar *provider);

void        gnome_db_provider_auth_editor_set_provider (GnomeDbProviderAuthEditor *auth, const gchar *provider);
gboolean    gnome_db_provider_auth_editor_is_valid     (GnomeDbProviderAuthEditor *auth);

gchar      *gnome_db_provider_auth_editor_get_auth     (GnomeDbProviderAuthEditor *auth);
void        gnome_db_provider_auth_editor_set_auth     (GnomeDbProviderAuthEditor *auth, const gchar *auth_string);

G_END_DECLS

#endif
