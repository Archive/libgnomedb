/* gnome-db-raw-grid.h
 *
 * Copyright (C) 2002 - 2006 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNOME_DB_RAW_GRID__
#define __GNOME_DB_RAW_GRID__

#include <gtk/gtk.h>
#include <libgda/gda-decl.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_RAW_GRID          (gnome_db_raw_grid_get_type())
#define GNOME_DB_RAW_GRID(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, gnome_db_raw_grid_get_type(), GnomeDbRawGrid)
#define GNOME_DB_RAW_GRID_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, gnome_db_raw_grid_get_type (), GnomeDbRawGridClass)
#define GNOME_DB_IS_RAW_GRID(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, gnome_db_raw_grid_get_type ())


typedef struct _GnomeDbRawGrid      GnomeDbRawGrid;
typedef struct _GnomeDbRawGridClass GnomeDbRawGridClass;
typedef struct _GnomeDbRawGridPriv  GnomeDbRawGridPriv;

/* struct for the object's data */
struct _GnomeDbRawGrid
{
	GtkTreeView             object;

	GnomeDbRawGridPriv     *priv;
};

/* struct for the object's class */
struct _GnomeDbRawGridClass
{
	GtkTreeViewClass        parent_class;

	void             (* selection_changed) (GnomeDbRawGrid *grid, gboolean row_selected);
	void             (* double_clicked)    (GnomeDbRawGrid *grid, gint row);
        void             (* populate_popup)    (GnomeDbRawGrid *grid, GtkMenu *menu);
};

/* 
 * Generic widget's methods 
 */
GType             gnome_db_raw_grid_get_type                  (void) G_GNUC_CONST;

GtkWidget        *gnome_db_raw_grid_new                       (GdaDataModel *model);

void              gnome_db_raw_grid_set_sample_size           (GnomeDbRawGrid *grid, gint sample_size);
void              gnome_db_raw_grid_set_sample_start          (GnomeDbRawGrid *grid, gint sample_start);

GList            *gnome_db_raw_grid_get_selection             (GnomeDbRawGrid *grid);

G_END_DECLS

#endif



