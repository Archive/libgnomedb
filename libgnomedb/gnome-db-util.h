/* GNOME DB library
 * Copyright (C) 1999-2002 The GNOME Foundation.
 *
 * AUTHORS:
 * 	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#if !defined(__gnome_db_util_h__)
#  define __gnome_db_util_h__

#include <libgda/gda-data-model.h>
#include <gtk/gtkentry.h>
#include <gtk/gtkmessagedialog.h>
#include <gtk/gtkoptionmenu.h>
#include <gtk/gtktextview.h>
#include <gtk/gtktoolbar.h>
#include <gtk/gtktreemodel.h>
#include <gtk/gtkwidget.h>
#include <gtk/gtkwindow.h>
#ifdef BUILD_WITH_GNOME
#include <libgnomeui/gnome-popup-menu.h>
#endif

G_BEGIN_DECLS

GtkWidget   *gnome_db_new_button_widget (const gchar *label);
GtkWidget   *gnome_db_new_button_widget_from_stock (const gchar *stock_id);
GtkWidget   *gnome_db_new_entry_widget (gint max_length, gboolean editable);
GtkWidget   *gnome_db_new_hbox_widget (gboolean homogenous, gint spacing);
GtkWidget   *gnome_db_new_label_widget (const gchar *text);
GtkWidget   *gnome_db_new_notebook_widget (void);
GtkWidget   *gnome_db_new_scrolled_window_widget (void);
GtkWidget   *gnome_db_new_table_widget (gint rows, gint cols, gboolean homogenous);
GtkWidget   *gnome_db_new_text_widget (const gchar *contents);
GtkWidget   *gnome_db_new_tree_view_widget (GtkTreeModel *model);
GtkWidget   *gnome_db_new_vbox_widget (gboolean homogenous, gint spacing);

GtkWidget   *gnome_db_option_menu_add_item (GtkOptionMenu *option_menu, const gchar *label);
GtkWidget   *gnome_db_option_menu_add_stock_item (GtkOptionMenu *option_menu, const gchar *stock_id);
void         gnome_db_option_menu_add_separator (GtkOptionMenu *option_menu);
const gchar *gnome_db_option_menu_get_selection (GtkOptionMenu *option_menu);
void         gnome_db_option_menu_set_selection (GtkOptionMenu *option_menu,
						 const gchar *selection);

void         gnome_db_text_clear (GtkTextView *text);
void         gnome_db_text_copy_clipboard (GtkTextView *text);
void         gnome_db_text_cut_clipboard (GtkTextView *text);
gint         gnome_db_text_get_char_count (GtkTextView *text);
gint         gnome_db_text_get_line_count (GtkTextView *text);
gchar       *gnome_db_text_get_text (GtkTextView *text);
void         gnome_db_text_insert_at_cursor (GtkTextView *text, const gchar *contents, gint len);
void         gnome_db_text_paste_clipboard (GtkTextView *text);
void         gnome_db_text_set_text (GtkTextView *text, const gchar *contents, gint len);

gchar       *gnome_db_select_file_dialog (GtkWidget *parent, const gchar *title);

GtkWidget   *gnome_db_new_alert (GtkWindow *parent,
				 GtkMessageType type,
				 const gchar *primary_text,
				 const gchar *secondary_text);

void         gnome_db_show_error (GtkWindow *parent, const gchar *format, ...);

void         gnome_db_push_cursor_busy (GtkWidget *window);
void         gnome_db_pop_cursor_busy (GtkWidget *window);

GtkWidget   *gnome_db_new_menu_item (const gchar *label,
				     gboolean pixmap,
				     GCallback cb_func,
				     gpointer user_data);
GtkWidget   *gnome_db_new_check_menu_item (const gchar *label,
					   gboolean active,
					   GCallback cb_func,
					   gpointer user_data);

G_END_DECLS

#endif
