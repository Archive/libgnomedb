/* gnome-db-transaction-status.c
 *
 * Copyright (C) 2002 - 2008 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <string.h>
#include <gtk/gtk.h>
#include <glib/gi18n-lib.h>
#include "gnome-db-transaction-status.h"

static void gnome_db_transaction_status_class_init (GnomeDbTransactionStatusClass * class);
static void gnome_db_transaction_status_init (GnomeDbTransactionStatus *wid);
static void gnome_db_transaction_status_dispose (GObject *object);

static void gnome_db_transaction_status_set_property (GObject *object,
						      guint param_id,
						      const GValue *value,
						      GParamSpec *pspec);
static void gnome_db_transaction_status_get_property (GObject *object,
						      guint param_id,
						      GValue *value,
						      GParamSpec *pspec);

static void gnome_db_transaction_status_refresh (GnomeDbTransactionStatus *status);
static void gnome_db_transaction_status_clean (GnomeDbTransactionStatus *status);

static void start_transaction_clicked_cb (GtkButton *button, GnomeDbTransactionStatus *status);
static void trans_commit_clicked_cb (GtkButton *button, GdaTransactionStatus *gdastatus);
static void trans_rollback_clicked_cb (GtkButton *button, GdaTransactionStatus *gdastatus);
static void trans_savepoint_add_clicked_cb (GtkButton *button, GdaTransactionStatus *gdastatus);
static void trans_savepoint_rollback_clicked_cb (GtkButton *button, GdaTransactionStatus *gdastatus);
static void trans_savepoint_del_clicked_cb (GtkButton *button, GdaTransactionStatus *gdastatus);

/* properties */
enum
{
        PROP_0,
        PROP_CNC
};

struct _GnomeDbTransactionStatusPriv
{
	GdaConnection *cnc;
	gint           svp_counter;
	GtkWidget     *sw;
};


/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *parent_class = NULL;

GType
gnome_db_transaction_status_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0)) {
		static const GTypeInfo info = {
			sizeof (GnomeDbTransactionStatusClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_transaction_status_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbTransactionStatus),
			0,
			(GInstanceInitFunc) gnome_db_transaction_status_init
		};		
		
		type = g_type_register_static (GTK_TYPE_VBOX, "GnomeDbTransactionStatus", &info, 0);
	}

	return type;
}

static void
gnome_db_transaction_status_class_init (GnomeDbTransactionStatusClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);
	
	parent_class = g_type_class_peek_parent (class);

	object_class->dispose = gnome_db_transaction_status_dispose;

	/* Properties */
        object_class->set_property = gnome_db_transaction_status_set_property;
        object_class->get_property = gnome_db_transaction_status_get_property;

	g_object_class_install_property (object_class, PROP_CNC,
                                         g_param_spec_object ("connection", 
                                                               _("Used connection"), NULL, GDA_TYPE_CONNECTION,
                                                               G_PARAM_READABLE | G_PARAM_WRITABLE));
}

static void
gnome_db_transaction_status_init (GnomeDbTransactionStatus * wid)
{
	wid->priv = g_new0 (GnomeDbTransactionStatusPriv, 1);
	wid->priv->cnc = NULL;
	wid->priv->svp_counter = 1;
}

/**
 * gnome_db_transaction_status_new
 * @cnc: a #GdaConnection object
 *
 * Creates a new #GnomeDbTransactionStatus widget showing the transaction status of @cnc
 *
 * Returns: the new widget
 */
GtkWidget *
gnome_db_transaction_status_new (GdaConnection *cnc)
{
	g_return_val_if_fail (!cnc || GDA_IS_CONNECTION (cnc), NULL);

	return (GtkWidget *) g_object_new (GNOME_DB_TYPE_TRANSACTION_STATUS, "connection", cnc, NULL);
}

static void
cnc_weak_ref_func (GnomeDbTransactionStatus *status, GdaConnection *cnc)
{
	status->priv->cnc = NULL;
	gnome_db_transaction_status_clean (status);
}

static void
transaction_status_changed_cb (GdaConnection *cnc, GnomeDbTransactionStatus *status)
{
	gnome_db_transaction_status_refresh (status);
}

static void
gnome_db_transaction_status_dispose (GObject *object)
{
	GnomeDbTransactionStatus *status;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOME_DB_IS_TRANSACTION_STATUS (object));
	status = GNOME_DB_TRANSACTION_STATUS (object);

	if (status->priv) {
		/* paramlist */
		if (status->priv->cnc) {
			g_signal_handlers_disconnect_by_func (G_OBJECT (status->priv->cnc),
							      G_CALLBACK (transaction_status_changed_cb), status);
			g_object_weak_unref (G_OBJECT (status->priv->cnc), (GWeakNotify) cnc_weak_ref_func, status);
		}

		/* the private area itself */
		g_free (status->priv);
		status->priv = NULL;
	}

	/* for the parent class */
	parent_class->dispose (object);
}

static void
gnome_db_transaction_status_set_property (GObject *object,
					  guint param_id,
					  const GValue *value,
					  GParamSpec *pspec)
{
	GnomeDbTransactionStatus *status;
	GdaConnection *cnc = NULL;

        status = GNOME_DB_TRANSACTION_STATUS (object);
        if (status->priv) {
                switch (param_id) {
		case PROP_CNC:
			cnc = g_value_get_object (value);
			if (status->priv->cnc) {
				gnome_db_transaction_status_clean (status);
				g_object_weak_unref (G_OBJECT (status->priv->cnc), (GWeakNotify) cnc_weak_ref_func, status);
				g_signal_handlers_disconnect_by_func (G_OBJECT (status->priv->cnc),
								      G_CALLBACK (transaction_status_changed_cb), status);
				status->priv->cnc = NULL;
			}

			if (cnc) {
				g_return_if_fail (GDA_IS_CONNECTION (cnc));

				status->priv->cnc = cnc;
				g_object_weak_ref (G_OBJECT (cnc), (GWeakNotify) cnc_weak_ref_func, status);
				g_signal_connect (G_OBJECT (cnc), "transaction_status_changed",
						  G_CALLBACK (transaction_status_changed_cb), status);
				gnome_db_transaction_status_refresh (status);
			}
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
			break;
		}
	}
}

static void
gnome_db_transaction_status_get_property (GObject *object,
					  guint param_id,
					  GValue *value,
					  GParamSpec *pspec)
{
	GnomeDbTransactionStatus *status;

        status = GNOME_DB_TRANSACTION_STATUS (object);
        if (status->priv) {
                switch (param_id) {
		case PROP_CNC:
			g_value_set_object (value, status->priv->cnc);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
			break;
                }
        }	
}

static void
transaction_status_get_depth_n_length (GdaTransactionStatus *status, gint *depth, gint *length)
{
	GList *list;
	
	g_return_if_fail (status);
	g_return_if_fail (depth);
	g_return_if_fail (length);

	*depth = 1;  /* for @status itself */
	*length = 1; /* for @status itself */
	for (list = status->events; list ; list = list->next) {
		GdaTransactionStatusEvent *ev = (GdaTransactionStatusEvent*)(list->data);
		switch (ev->type) {
		case GDA_TRANSACTION_STATUS_EVENT_SAVEPOINT:
		case GDA_TRANSACTION_STATUS_EVENT_SQL:
			(*length)++;
			break;
		case GDA_TRANSACTION_STATUS_EVENT_SUB_TRANSACTION: {
			gint ldepth, llength;
			transaction_status_get_depth_n_length (ev->pl.sub_trans, &ldepth, &llength);
			*length += llength;
			*depth += ldepth;
			break;
		}
		default:
			g_assert_not_reached ();
		}
	}
}

static GtkWidget *
make_padding_widget ()
{
	GtkWidget *vbox, *wid;

	vbox = gtk_vbox_new (FALSE, 0);
	wid = gtk_arrow_new (GTK_ARROW_RIGHT, GTK_SHADOW_NONE);
	gtk_misc_set_padding (GTK_MISC (wid), 5, 5);
	gtk_misc_set_alignment (GTK_MISC (wid), 0.6, 0.3);
	gtk_box_pack_start (GTK_BOX (vbox), wid, FALSE, FALSE, 0);
	gtk_widget_show (wid);

	return vbox;
}

static GtkWidget *
create_widget_for_tstatus (GnomeDbTransactionStatus *status, GdaTransactionStatus *gdastatus)
{
	gchar *str;
	GtkWidget *button, *label, *hbox, *pad;
	gint evidx;
	GList *list;
	GtkTable *table;
	gboolean has_sub_trans = FALSE;
	gboolean trans_alive = (gdastatus->state == GDA_TRANSACTION_STATUS_STATE_OK) ? TRUE : FALSE;

	table = (GtkTable*) gtk_table_new (g_list_length (gdastatus->events) + 2, 2, FALSE);
	
	/* transaction label */
	hbox = gtk_hbox_new (FALSE, 0);
	gtk_table_attach (table, hbox, 0, 2, 0, 1, GTK_EXPAND | GTK_FILL, 0, 0, 0);
	gtk_widget_show (hbox);

	if (gdastatus->name)
		str = g_strdup_printf ("<b>%s:</b> %s", _("Transaction started"), gdastatus->name);
	else
		str = g_strdup_printf ("<b>%s</b>", _("Transaction started"));
	label = gtk_label_new (str);
	g_free (str);
	gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
	gtk_misc_set_alignment (GTK_MISC (label), 0., 0.5);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	if (trans_alive) {
                /* Allow this to build with both GTK+ 2.8 and GTK+ 2.10.
                 * However, a GtkLinkButton is probably bad UI anyway.
                 * The URI (e.g. "commit") does not seem to be used either)
                 */
                #if GTK_MINOR_VERSION >= 10
		button = gtk_link_button_new_with_label ("commit", _("Commit"));
                #else
                button = gtk_button_new_with_label (_("Commit"));
                #endif

		gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);
		gtk_widget_show (button);
		g_object_set_data (G_OBJECT (button), "_gnomedb_trans_status", status);
		g_signal_connect (G_OBJECT (button), "clicked",
				  G_CALLBACK (trans_commit_clicked_cb), gdastatus);
	}

        #if GTK_MINOR_VERSION >= 10
	button = gtk_link_button_new_with_label ("rollback", _("Rollback"));
        #else
        button = gtk_button_new_with_label (_("Rollback"));
        #endif

	gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);
	gtk_widget_show (button);
	g_object_set_data (G_OBJECT (button), "_gnomedb_trans_status", status);
	g_signal_connect (G_OBJECT (button), "clicked",
			  G_CALLBACK (trans_rollback_clicked_cb), gdastatus);

	/* lines for the events */
	for (evidx = 1, list = gdastatus->events; list; list = list->next, evidx++) {
		GdaTransactionStatusEvent *ev = (GdaTransactionStatusEvent*)(list->data);

		pad = make_padding_widget();
		gtk_table_attach (table, pad, 0, 1, evidx, evidx + 1, 0, GTK_FILL, 0, 0);
		gtk_widget_show (pad);

		switch (ev->type) {
		case GDA_TRANSACTION_STATUS_EVENT_SAVEPOINT: {
			GtkWidget *hbox;

			hbox = gtk_hbox_new (FALSE, 0);
			gtk_table_attach (table, hbox, 1, 2, evidx, evidx + 1,
					  GTK_EXPAND | GTK_FILL, 0, 0, 0);
			gtk_widget_show (hbox);

			if (ev->pl.svp_name)
				str = g_strdup_printf ("<b>%s:</b> %s", _("Savepoint added"), ev->pl.svp_name);
			else
				str = g_strdup_printf ("<b>%s</b>", _("Savepoint added"));
			label = gtk_label_new (str);
			g_free (str);
			gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
			gtk_misc_set_alignment (GTK_MISC (label), 0., 0.5);
			gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
			gtk_widget_show (label);

			if (gda_connection_supports_feature (status->priv->cnc, 
							     GDA_CONNECTION_FEATURE_SAVEPOINTS) &&
			    trans_alive) {
                                #if GTK_MINOR_VERSION >= 10
	                        button = gtk_link_button_new_with_label ("rollbacksvp", _("Rollback"));
                                #else
                                button = gtk_button_new_with_label (_("Rollback"));
                                #endif
				
				gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);
				gtk_widget_show (button);
				g_object_set_data (G_OBJECT (button), "_gnomedb_trans_status", status);
				g_object_set_data (G_OBJECT (button), "_gnomedb_trans_ev", ev);
				g_signal_connect (G_OBJECT (button), "clicked",
						  G_CALLBACK (trans_savepoint_rollback_clicked_cb), gdastatus);

				if (gda_connection_supports_feature (status->priv->cnc, 
								     GDA_CONNECTION_FEATURE_SAVEPOINTS_REMOVE)) {
                                        #if GTK_MINOR_VERSION >= 10
					button = gtk_link_button_new_with_label ("delsvp", _("Delete"));
                                        #else
                                        button = gtk_button_new_with_label (_("Delete"));
                                        #endif

					gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);
					gtk_widget_show (button);
					g_object_set_data (G_OBJECT (button), "_gnomedb_trans_status", status);
					g_object_set_data (G_OBJECT (button), "_gnomedb_trans_ev", ev);
					g_signal_connect (G_OBJECT (button), "clicked",
							  G_CALLBACK (trans_savepoint_del_clicked_cb), gdastatus);
				}
			}
			break;
		}
		case GDA_TRANSACTION_STATUS_EVENT_SQL: {
			gchar *str, *ptr;

			str = g_strdup (ev->pl.sql);
			for (ptr = str; *ptr; ptr++)
				if (*ptr == '\n') *ptr = ' ';
			label = gtk_label_new (str);
			g_free (str);
			gtk_misc_set_alignment (GTK_MISC (label), 0., -1);
			gtk_label_set_ellipsize (GTK_LABEL (label), PANGO_ELLIPSIZE_END);
			gtk_table_attach (table, label, 1, 2, evidx, evidx + 1, 
					  GTK_EXPAND | GTK_FILL, 0, 0, 0);
			gtk_widget_show (label);
			break;
		}
		case GDA_TRANSACTION_STATUS_EVENT_SUB_TRANSACTION: {
			GtkWidget *wid;
			wid = create_widget_for_tstatus (status, ev->pl.sub_trans);
			gtk_table_attach (table, wid, 1, 2, evidx, evidx + 1, 
					  GTK_EXPAND | GTK_FILL, 0, 0, 0);
			gtk_widget_show (wid);
			has_sub_trans = TRUE;
			break;
		}
		default:
			g_assert_not_reached ();
		}
	}

	/* Add savepoint button */
	if (!has_sub_trans && status->priv->cnc && 
	    gda_connection_supports_feature (status->priv->cnc, 
					     GDA_CONNECTION_FEATURE_SAVEPOINTS) &&
	    trans_alive) {
		hbox = gtk_hbox_new (FALSE, 0);
		gtk_table_attach (table, hbox, 1, 2, evidx, evidx + 1, GTK_FILL | GTK_EXPAND, 0, 0, 0);
		gtk_widget_show (hbox);

		pad = make_padding_widget();
		gtk_table_attach (table, pad, 0, 1, evidx, evidx + 1, 0, 0, 0, 0);
		gtk_widget_show (pad);

                #if GTK_MINOR_VERSION >= 10
		button = gtk_link_button_new_with_label ("addsvp", _("Add save point"));
                #else
                button = gtk_button_new_with_label(_("Add save point"));
                #endif

		gtk_widget_show (button);
		gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);
		g_object_set_data (G_OBJECT (button), "_gnomedb_trans_status", status);
		g_signal_connect (G_OBJECT (button), "clicked",
				  G_CALLBACK (trans_savepoint_add_clicked_cb), gdastatus);
		evidx++;
	}

	/* message about transaction state */
	str = NULL;
	switch (gdastatus->state) {
	case GDA_TRANSACTION_STATUS_STATE_OK:
		break;
	case GDA_TRANSACTION_STATUS_STATE_FAILED:
		str = _("Transaction locked (any further query will fail),\nroll back the transaction");
		break;
	default:
		g_warning (_("Unknown transaction state %d, please report error"), gdastatus->state);
	}
	if (str) {
		gchar *str2;

		str2 = g_strdup_printf ("<span foreground=\"red\">%s</span>", str);
		label = gtk_label_new (str2);
		g_free (str2);
		gtk_label_set_use_markup (GTK_LABEL (label), TRUE);
		gtk_misc_set_alignment (GTK_MISC (label), 0., 0.5);
		gtk_table_attach (table, label, 0, 2, evidx, evidx + 1, 0, GTK_FILL, 0, 0);
		gtk_widget_show (label);
	}

	return (GtkWidget *) table;
}

static void
gnome_db_transaction_status_clean (GnomeDbTransactionStatus *status)
{
	if (!status->priv->sw)
		return;
	else {
		GList *children, *list;
		
		children = gtk_container_get_children (GTK_CONTAINER (status->priv->sw));
		for (list = children; list; list = list->next)
			gtk_widget_destroy (GTK_WIDGET (list->data));
		g_list_free (children);
	}
}

static void
gnome_db_transaction_status_refresh (GnomeDbTransactionStatus *status)
{
	GdaTransactionStatus *gdastatus;
	GtkWidget *sw;

	gnome_db_transaction_status_clean (status);
	if (!status->priv->cnc)
		return;

	sw = status->priv->sw;
	if (! sw) {
		sw = gtk_scrolled_window_new (NULL, NULL);
		gtk_box_pack_start (GTK_BOX (status), sw, TRUE, TRUE, 0);
		gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw), GTK_POLICY_AUTOMATIC,
						GTK_POLICY_AUTOMATIC);
		gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (sw), GTK_SHADOW_NONE);
		gtk_widget_show (sw);
		status->priv->sw = sw;
	}

	gdastatus = gda_connection_get_transaction_status (status->priv->cnc);
	if (!gdastatus) {
		GtkWidget *vbox, *hbox, *label;

		vbox = gtk_vbox_new (FALSE, 0);
		gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (sw), vbox);
		gtk_widget_show (vbox);

		hbox = gtk_hbox_new (FALSE, 5);
		gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);
		gtk_widget_show (hbox);

		label = gtk_label_new (_("No transaction has been started."));
		gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
		gtk_widget_show (label);

		if (gda_connection_is_opened (status->priv->cnc) && 
		    gda_connection_supports_feature (status->priv->cnc, 
						     GDA_CONNECTION_FEATURE_TRANSACTIONS)) {
			GtkWidget *button;

                        #if GTK_MINOR_VERSION >= 10
		        button = gtk_link_button_new_with_label ("begin", _("Begin transaction"));
                        #else
                        button = gtk_button_new_with_label(_("Begin transaction"));
                        #endif

			gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, FALSE, 0);
			gtk_widget_show (button);
			g_signal_connect (G_OBJECT (button), "clicked",
					  G_CALLBACK (start_transaction_clicked_cb), status);
		}
	}
	else {
		GtkWidget *wid;

		wid = create_widget_for_tstatus (status, gdastatus);
		gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (sw), wid);
		gtk_widget_show (wid);
	}

	{
		GList *children;
		GtkWidget *vp;

		children = gtk_container_get_children (GTK_CONTAINER (status->priv->sw));
		vp = (GtkWidget *) (children->data);
		g_list_free (children);
		gtk_viewport_set_shadow_type (GTK_VIEWPORT (vp), GTK_SHADOW_NONE);
	}
}

static void
start_transaction_clicked_cb (GtkButton *button, GnomeDbTransactionStatus *status)
{
	/* FIXME: error handling */
	gda_connection_begin_transaction (status->priv->cnc, NULL, GDA_TRANSACTION_ISOLATION_UNKNOWN, NULL);
}

static void
trans_commit_clicked_cb (GtkButton *button, GdaTransactionStatus *gdastatus)
{
	GnomeDbTransactionStatus *status;

	status = g_object_get_data (G_OBJECT (button), "_gnomedb_trans_status");
	g_assert (GNOME_DB_IS_TRANSACTION_STATUS (status));
	
	/* FIXME: error handling */
	gda_connection_commit_transaction (status->priv->cnc, gdastatus->name, NULL);
}

static void
trans_rollback_clicked_cb (GtkButton *button, GdaTransactionStatus *gdastatus)
{
	GnomeDbTransactionStatus *status;

	status = g_object_get_data (G_OBJECT (button), "_gnomedb_trans_status");
	g_assert (GNOME_DB_IS_TRANSACTION_STATUS (status));
	
	/* FIXME: error handling */
	gda_connection_rollback_transaction (status->priv->cnc, gdastatus->name, NULL);
}

static void
trans_savepoint_add_clicked_cb (GtkButton *button, GdaTransactionStatus *gdastatus)
{
	GnomeDbTransactionStatus *status;
	gchar *name;

	status = g_object_get_data (G_OBJECT (button), "_gnomedb_trans_status");
	g_assert (GNOME_DB_IS_TRANSACTION_STATUS (status));
	
	/* FIXME: error handling */
	name = g_strdup_printf ("savepoint_%d", status->priv->svp_counter++);
	gda_connection_add_savepoint (status->priv->cnc, name, NULL);
	g_free (name);
}

static void
trans_savepoint_rollback_clicked_cb (GtkButton *button, GdaTransactionStatus *gdastatus)
{
	GnomeDbTransactionStatus *status;
	GdaTransactionStatusEvent *ev;

	ev = g_object_get_data (G_OBJECT (button), "_gnomedb_trans_ev");
	status = g_object_get_data (G_OBJECT (button), "_gnomedb_trans_status");
	g_assert (GNOME_DB_IS_TRANSACTION_STATUS (status));
	g_assert (ev->type == GDA_TRANSACTION_STATUS_EVENT_SAVEPOINT);

	/* FIXME: error handling */
	gda_connection_rollback_savepoint (status->priv->cnc, ev->pl.svp_name, NULL);
}

static void
trans_savepoint_del_clicked_cb (GtkButton *button, GdaTransactionStatus *gdastatus)
{
	GnomeDbTransactionStatus *status;
	GdaTransactionStatusEvent *ev;

	ev = g_object_get_data (G_OBJECT (button), "_gnomedb_trans_ev");
	status = g_object_get_data (G_OBJECT (button), "_gnomedb_trans_status");
	g_assert (GNOME_DB_IS_TRANSACTION_STATUS (status));
	g_assert (ev->type == GDA_TRANSACTION_STATUS_EVENT_SAVEPOINT);

	/* FIXME: error handling */
	gda_connection_delete_savepoint (status->priv->cnc, ev->pl.svp_name, NULL);
}

