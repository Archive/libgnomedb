/* gnome-db-set.h
 *
 * Copyright (C) 2009 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNOME_DB_SET__
#define __GNOME_DB_SET__

#include <gtk/gtk.h>
#include <libgda/gda-decl.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_SET          (gnome_db_set_get_type())
#define GNOME_DB_SET(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, gnome_db_set_get_type(), GnomeDbSet)
#define GNOME_DB_SET_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, gnome_db_set_get_type (), GnomeDbSetClass)
#define GNOME_DB_IS_SET(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, gnome_db_set_get_type ())


typedef struct _GnomeDbSet      GnomeDbSet;
typedef struct _GnomeDbSetClass GnomeDbSetClass;
typedef struct _GnomeDbSetPriv  GnomeDbSetPriv;

typedef struct _GnomeDbSetGroup GnomeDbSetGroup;
typedef struct _GnomeDbSetSource GnomeDbSetSource;

struct _GnomeDbSetGroup {
        GdaSetGroup      *group;
        GnomeDbSetSource *source; /* if NULL, then @group->nodes contains exactly one entry */

        /* Padding for future expansion */
        gpointer      _gda_reserved1;
        gpointer      _gda_reserved2;
};

#define GNOME_DB_SET_GROUP(x) ((GnomeDbSetGroup*)(x))

struct _GnomeDbSetSource {
        GdaSetSource   *source;

	/* displayed columns in 'source->data_model' */
 	gint shown_n_cols;
 	gint *shown_cols_index;

 	/* columns used as a reference (corresponding to PK values) in 'source->data_model' */
 	gint ref_n_cols;
 	gint *ref_cols_index; 

        /* Padding for future expansion */
        gpointer        _gda_reserved1;
        gpointer        _gda_reserved2;
        gpointer        _gda_reserved3;
        gpointer        _gda_reserved4;
};

#define GNOME_DB_SET_SOURCE(x) ((GnomeDbSetSource*)(x))

/* struct for the object's data */
struct _GnomeDbSet
{
	GObject         object;
	GnomeDbSetPriv *priv;

	/*< public >*/
	GSList         *sources_list; /* list of GnomeDbSetSource */
        GSList         *groups_list;  /* list of GnomeDbSetGroup */
};

/* struct for the object's class */
struct _GnomeDbSetClass
{
	GObjectClass       parent_class;
	void             (*public_data_changed)   (GnomeDbSet *set);
};

/* 
 * Generic widget's methods 
 */
GType             gnome_db_set_get_type            (void) G_GNUC_CONST;

GnomeDbSet       *gnome_db_set_new                 (GdaSet *set);
GnomeDbSetGroup  *_gnome_db_set_get_group           (GnomeDbSet *dbset, GdaHolder *holder);

G_END_DECLS

#endif



