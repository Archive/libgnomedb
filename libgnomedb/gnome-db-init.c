/* GNOME DB library
 * Copyright (C) 1999 - 2008 The GNOME Foundation.
 *
 * AUTHORS:
 * 	Rodrigo Moya <rodrigo@gnome-db.org>
 *      Vivien Malerba <malerba@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gtk/gtk.h>
#include <glib/gi18n-lib.h>
#ifdef HAVE_LIBGLADE
#include <glade/glade-init.h>
#endif
#include <libgda/libgda.h>
#ifdef HAVE_GCONF
#include <gconf/gconf-client.h>
#endif
#include <libgnomedb/binreloc/gnome-db-binreloc.h>

/* plugins list */
GHashTable *gnome_db_plugins_hash = NULL; /* key = plugin name, value = GnomeDbPlugin structure pointer */

extern void gnome_db_stock_init (void);

/**
 * gnome_db_init
 *
 * Initialization of the libgnomedb library, must be called before any usage of the library.
 * Note: gtk_init() is not called by this function and should also be called.
 */
void
gnome_db_init (void)
{
	static gboolean initialized = FALSE;
	gchar *str;

	if (initialized) {
		gda_log_error (_("Attempt to initialize an already initialized library"));
		return;
	}

	gnome_db_gbr_init ();
	str = gnome_db_gbr_get_locale_dir_path ();
	bindtextdomain (GETTEXT_PACKAGE, str);
	g_free (str);

	bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");

	gda_init ();

#ifdef HAVE_LIBGLADE
	glade_init ();
#endif
	gnome_db_stock_init ();

#ifdef HAVE_GCONF
	/* init GConf */
	if (!gconf_is_initialized ())
		gconf_init (0, NULL, NULL);
#endif

	initialized = TRUE;
}

/**
 * gnome_db_get_application_exec_path
 * @app_name: the name of the application to find
 *
 * Find the path to the application identified by @app_name. For example if the application
 * is "gnome-database-properties", then calling this function will return
 * "/your/prefix/bin/gnome-database-properties-4.0" if Libgnomedb is installed in
 * the "/your/prefix" prefix (which would usually be "/usr"), and for the ABI version 4.0.
 *
 * Returns: the path as a new string, or %NULL if the application cannot be found
 */
gchar *
gnome_db_get_application_exec_path (const gchar *app_name)
{
	gchar *str;
	gchar *fname;
	g_return_val_if_fail (app_name, NULL);

	gnome_db_gbr_init ();
	fname = g_strdup_printf ("%s-%s", app_name, ABI_VERSION);
	str = gnome_db_gbr_get_file_path (GNOME_DB_BIN_DIR, fname, NULL);
	g_free (fname);
	if (!g_file_test (str, G_FILE_TEST_EXISTS | G_FILE_TEST_IS_EXECUTABLE)) {
		g_free (str);
		str = NULL;
	}

	return str;
}
