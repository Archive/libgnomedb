/* gnome-db-decl.h
 *
 * Copyright (C) 2003 - 2008 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNOME_DB_DECL_H_
#define __GNOME_DB_DECL_H_

typedef struct _GnomeDbDataStore      GnomeDbDataStore;
typedef struct _GnomeDbDataStoreClass GnomeDbDataStoreClass;
typedef struct _GnomeDbDataStorePriv  GnomeDbDataStorePriv;

/* 
 * Interfaces 
 */
typedef struct _GnomeDbDataEntry        GnomeDbDataEntry;
typedef struct _GnomeDbDataEntryIface   GnomeDbDataEntryIface;

typedef struct _GnomeDbDataWidget       GnomeDbDataWidget;
typedef struct _GnomeDbDataWidgetIface  GnomeDbDataWidgetIface;

/* 
 * Colors
 */
#define GNOME_DB_COLOR_NORMAL_NULL "#00cd66"
#define GNOME_DB_COLOR_PRELIGHT_NULL "#00ef77"

#define GNOME_DB_COLOR_NORMAL_DEFAULT "#6495ed"
#define GNOME_DB_COLOR_PRELIGHT_DEFAULT "#75a6fe"

#define GNOME_DB_COLOR_NORMAL_MODIF "#cacaee"
#define GNOME_DB_COLOR_PRELIGHT_MODIF "#cfcffe"

#define GNOME_DB_COLOR_NORMAL_INVALID "#ff6a6a"
#define GNOME_DB_COLOR_PRELIGHT_INVALID "#ff7b7b"

#define GNOME_DB_REFERER_ACTIVE   "#e0ffe0"
#define GNOME_DB_REFERER_INACTIVE "#ffe0e0"
#define GNOME_DB_REFERER_UNKNOWN  "#e0e0ff"

#endif
