/* GNOME DB library
 * Copyright (C) 1999 - 2005 The GNOME Foundation.
 *
 * AUTHORS:
 *      Gustavo R. Montesino <ikki_gerrard@yahoo.com.br>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */


#include <gtk/gtkcomboboxentry.h>
#include <gtk/gtkdialog.h>
#include <gtk/gtkentry.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkstock.h>
#include <gtk/gtktable.h>
#include <libgda/gda-data-model.h>

#include "gnome-db-find-dialog.h"
#include <glib/gi18n-lib.h>

struct _GnomeDbFindDialogPrivate {
	GtkComboBoxEntry *fields;
	GtkEntry *text;
};

enum {
	P_FIELD = 1,
  	P_STRING,
  	P_MODEL
};

static GObjectClass *parent_class = NULL;

/* Private misc functions */

static void
gnome_db_find_dialog_set_property (GObject *object, guint param_id,
				   const GValue *value, GParamSpec *spec)
{
	GnomeDbFindDialog *dialog = (GnomeDbFindDialog *) object;
	g_return_if_fail (GNOME_DB_IS_FIND_DIALOG (dialog));

	switch (param_id) {
	
	case P_MODEL:
	{
		GdaDataModel *model = GDA_DATA_MODEL(g_value_get_object (value));
		if (model)
			g_return_if_fail (GDA_IS_DATA_MODEL (model));
		gnome_db_find_dialog_add_fields_from_model (GNOME_DB_FIND_DIALOG(object), model);
		break;
	}
	
	default: 
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, spec);
		break;
  	}
}

static void
gnome_db_find_dialog_get_property (GObject *object, guint param_id, 
				   GValue *value, GParamSpec *spec)
{
	GnomeDbFindDialog *dialog = (GnomeDbFindDialog *) object;
	g_return_if_fail (GNOME_DB_IS_FIND_DIALOG (dialog));

  	switch (param_id) {
	case P_FIELD:
		g_value_set_string (value, gtk_entry_get_text (
					    GTK_ENTRY (GTK_BIN (dialog -> priv -> fields) -> child)));
		break;
      
	case P_STRING:
		g_value_set_string (value, 
				    gtk_entry_get_text (dialog -> priv -> text));
		break;	
      
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, spec);
		break;
  	}
}

static void
gnome_db_find_dialog_init (GnomeDbFindDialog *dialog, GnomeDbFindDialogClass *klass)
{
	GtkTable  *table;
	GtkLabel  *label;

	g_return_if_fail (GNOME_DB_IS_FIND_DIALOG (dialog));

	dialog -> priv = g_new0 (GnomeDbFindDialogPrivate, 1);

	gtk_dialog_add_button (GTK_DIALOG (dialog), GTK_STOCK_FIND, GTK_RESPONSE_OK);
	gtk_dialog_add_button (GTK_DIALOG (dialog), GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);

	table = GTK_TABLE (gtk_table_new (2, 2, FALSE));
	gtk_widget_show (GTK_WIDGET (table));
	gtk_container_set_border_width (GTK_CONTAINER (table), 10);
	gtk_table_set_col_spacings (table, 30);
	gtk_table_set_row_spacings (table, 5);
	gtk_container_add (GTK_CONTAINER (GTK_DIALOG (dialog) -> vbox), GTK_WIDGET (table));

	label = GTK_LABEL (gtk_label_new_with_mnemonic (_("_Field:")));
	gtk_widget_show (GTK_WIDGET (label));
	gtk_table_attach_defaults (table, GTK_WIDGET (label), 0, 1, 0, 1);

	dialog -> priv -> fields = GTK_COMBO_BOX_ENTRY (gtk_combo_box_entry_new_text());
	gtk_widget_show (GTK_WIDGET (dialog -> priv -> fields));
	gtk_table_attach_defaults (table, GTK_WIDGET (dialog -> priv -> fields), 1, 2, 0, 1);
 
	gtk_label_set_mnemonic_widget (label, GTK_WIDGET (dialog -> priv -> fields));

	label = GTK_LABEL (gtk_label_new_with_mnemonic (_("_Text:")));
	gtk_widget_show (GTK_WIDGET (label));
	gtk_table_attach_defaults (table, GTK_WIDGET (label), 0, 1, 1, 2);

	dialog -> priv -> text = GTK_ENTRY (gtk_entry_new());
	gtk_widget_show (GTK_WIDGET (dialog -> priv -> text));
	gtk_table_attach_defaults (table, GTK_WIDGET (dialog -> priv -> text), 1, 2, 1, 2);

	gtk_label_set_mnemonic_widget (GTK_LABEL (label), GTK_WIDGET (dialog -> priv -> text));
}

/* Class */

static void
gnome_db_find_dialog_finalize (GObject *object)
{
	GnomeDbFindDialog *dialog = (GnomeDbFindDialog *) object;
	g_return_if_fail (GNOME_DB_IS_FIND_DIALOG (dialog));

	g_free (dialog -> priv);
	dialog -> priv = NULL;

	parent_class -> finalize (object); 
}

static void
gnome_db_find_dialog_class_init (GnomeDbFindDialogClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);
	parent_class = g_type_class_peek_parent (klass);

	object_class -> set_property = gnome_db_find_dialog_set_property;
	object_class -> get_property = gnome_db_find_dialog_get_property;
	object_class -> finalize     = gnome_db_find_dialog_finalize;

	/* Properties */
	g_object_class_install_property (object_class, P_FIELD, 
					 g_param_spec_string ("field", NULL, NULL, NULL, G_PARAM_READABLE));
  
	g_object_class_install_property (object_class, P_STRING,
					 g_param_spec_string ("text", NULL, NULL, NULL, G_PARAM_READABLE));
	
	g_object_class_install_property (object_class, P_MODEL,
           g_param_spec_object ("model", _("Data to display"), NULL, GDA_TYPE_DATA_MODEL,
                                                               G_PARAM_CONSTRUCT_ONLY|G_PARAM_WRITABLE));
    
}

/* Type Registration */

GType
gnome_db_find_dialog_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0)) {
		static const GTypeInfo info = {
			sizeof (GnomeDbFindDialogClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_find_dialog_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbFindDialog),
			0,
			(GInstanceInitFunc) gnome_db_find_dialog_init,
		};
		type = g_type_register_static (GTK_TYPE_DIALOG, "GnomeDbFindDialog", &info, 0);
	}
	
	return type;
}

/**
 * gnome_db_find_dialog_new
 * @title: the title of the dialog
 * 
 * Creates a new #GnomeDbFindDialog.
 *
 * Returns: a #GnomeDbFindDialog
 */
GtkWidget *
gnome_db_find_dialog_new (const gchar *title)
{
	GnomeDbFindDialog *dialog;
	
	dialog = g_object_new (GNOME_DB_TYPE_FIND_DIALOG, "title", title, NULL);

	return GTK_WIDGET(dialog);
}

/**
 * gnome_db_find_dialog_new_with_model
 * @title: the title of the dialog
 * @dm: a #GdaDataModel
 *
 * Creates a new #GnomeDbFindDialog and adds the fields of @dm to the Fields 
 * combo.
 *
 * Returns: a #GnomeDbFindDialog
 */
GtkWidget *
gnome_db_find_dialog_new_with_model (const gchar *title, GdaDataModel *model)
{
	GnomeDbFindDialog *dialog = g_object_new (GNOME_DB_TYPE_FIND_DIALOG, "title", title, "model", model, NULL);

	return GTK_WIDGET(dialog);
}

/**
 * gnome_db_find_dialog_run
 * @dialog: a #GnomeDbFindDialog
 *
 * Shows the @dialog to the user and wait for his/her input.
 *
 * Returns: true if the user clicks "find", false otherwise.
 */
gboolean
gnome_db_find_dialog_run (GnomeDbFindDialog *dialog)
{
	g_return_val_if_fail (GNOME_DB_IS_FIND_DIALOG (dialog), FALSE);

	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_OK)
		return TRUE;
  
	return FALSE;
}

/**
 * gnome_db_find_dialog_add_field
 * @dialog: a #GnomeDbFindDialog
 * @field: the field's name
 * 
 * Adds @field to the Fields combo.
 */
void
gnome_db_find_dialog_add_field (GnomeDbFindDialog *dialog, const gchar *field)
{
	g_return_if_fail (GNOME_DB_IS_FIND_DIALOG (dialog));
  
	gtk_combo_box_append_text (GTK_COMBO_BOX (dialog -> priv -> fields), field);
}

/**
 * gnome_db_find_dialog_add_fields_from_model
 * @dialog: a #GnomeDbFindDialog
 * @dm: a #GdaDataModel
 *
 * Adds all the fields of @dm into the Fields combo.
 */
void
gnome_db_find_dialog_add_fields_from_model (GnomeDbFindDialog *dialog, GdaDataModel *dm)
{
	gint i;

	g_return_if_fail (GNOME_DB_IS_FIND_DIALOG (dialog));
	g_return_if_fail (GDA_IS_DATA_MODEL (dm));

	g_object_ref (dm);

	for (i = 0; i < gda_data_model_get_n_columns (dm); i++) {
		gnome_db_find_dialog_add_field (dialog, 
						gda_data_model_get_column_title (dm, i));
	}

	g_object_unref (dm);
}

/**
 * gnome_db_find_dialog_get_text
 * @dialog: a #GnomeDbFindDialog widget
 * 
 * Gets the current search text in @dialog.
 *
 * Returns: text to find
 */
G_CONST_RETURN gchar*
gnome_db_find_dialog_get_text (GnomeDbFindDialog *dialog)
{
	g_return_val_if_fail (GNOME_DB_IS_FIND_DIALOG (dialog), NULL);

	return gtk_entry_get_text (GTK_ENTRY (dialog -> priv -> text));
}

/**
 * gnome_db_find_dialog_get_field
 * @dialog: a #GnomeDbFindDialog widget
 *
 * Gets the currently selected field in @dialog.
 *
 * Returns: name of the selected field
 */
G_CONST_RETURN gchar*
gnome_db_find_dialog_get_field (GnomeDbFindDialog *dialog)
{
	g_return_val_if_fail (GNOME_DB_IS_FIND_DIALOG (dialog), NULL);

	return gtk_entry_get_text (GTK_ENTRY (GTK_BIN (
		dialog -> priv -> fields) -> child));
}


