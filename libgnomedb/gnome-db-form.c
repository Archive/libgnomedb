/* gnome-db-form.c
 *
 * Copyright (C) 2002 - 2007 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <string.h>
#include <glib/gi18n-lib.h>
#include <libgda/libgda.h>
#include "gnome-db-form.h"
#include "gnome-db-data-widget.h"
#include "gnome-db-raw-form.h"
#include "gnome-db-data-widget-info.h"

static void gnome_db_form_class_init (GnomeDbFormClass * class);
static void gnome_db_form_init (GnomeDbForm *wid);

static void gnome_db_form_set_property (GObject *object,
					guint param_id,
					const GValue *value,
					GParamSpec *pspec);
static void gnome_db_form_get_property (GObject *object,
					guint param_id,
					GValue *value,
					GParamSpec *pspec);

struct _GnomeDbFormPriv
{
	GtkWidget *raw_form;
	GtkWidget *info;
};

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *parent_class = NULL;

/* properties */
enum
{
        PROP_0,
        PROP_RAW_FORM,
	PROP_INFO,
	PROP_MODEL
};

GType
gnome_db_form_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0)) {
		static const GTypeInfo info = {
			sizeof (GnomeDbFormClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_form_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbForm),
			0,
			(GInstanceInitFunc) gnome_db_form_init
		};		

		type = g_type_register_static (GTK_TYPE_VBOX, "GnomeDbForm", &info, 0);
	}

	return type;
}

static void
gnome_db_form_class_init (GnomeDbFormClass *class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);
	
	parent_class = g_type_class_peek_parent (class);

	/* Properties */
	
        object_class->set_property = gnome_db_form_set_property;
        object_class->get_property = gnome_db_form_get_property;
	g_object_class_install_property (object_class, PROP_RAW_FORM,
                                         g_param_spec_object ("raw_form", NULL, NULL, GNOME_DB_TYPE_RAW_FORM,
                                                               G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_INFO,
                                         g_param_spec_object ("widget_info", NULL, NULL, GNOME_DB_TYPE_DATA_WIDGET_INFO,
							      G_PARAM_READABLE));
	
	g_object_class_install_property (object_class, PROP_MODEL,
					 g_param_spec_object ("model", NULL, NULL,
							      GDA_TYPE_DATA_MODEL,
							      G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));
}

static void
gnome_db_form_init (GnomeDbForm *form)
{
	form->priv = g_new0 (GnomeDbFormPriv, 1);
	form->priv->raw_form = NULL;
	form->priv->info = NULL;
	
	form->priv->raw_form = gnome_db_raw_form_new (NULL);
	gtk_box_pack_start (GTK_BOX (form), form->priv->raw_form, TRUE, TRUE, 0);
	gtk_widget_show (form->priv->raw_form);

	form->priv->info = gnome_db_data_widget_info_new (GNOME_DB_DATA_WIDGET (form->priv->raw_form), 
							  GNOME_DB_DATA_WIDGET_INFO_CURRENT_ROW |
							  GNOME_DB_DATA_WIDGET_INFO_ROW_MODIFY_BUTTONS |
							  GNOME_DB_DATA_WIDGET_INFO_ROW_MOVE_BUTTONS);
	gtk_box_pack_start (GTK_BOX (form), form->priv->info, FALSE, TRUE, 0);
	gtk_widget_show (form->priv->info);

}

/**
 * gnome_db_form_new
 * @model: a #GdaDataModel
 *
 * Creates a new #GnomeDbForm widget suitable to display the data in @model
 *
 *  Returns: the new widget
 */
GtkWidget *
gnome_db_form_new (GdaDataModel *model)
{
	GnomeDbForm *form;
	
	g_return_val_if_fail (!model || GDA_IS_DATA_MODEL (model), NULL);
	
	form = (GnomeDbForm *) g_object_new (GNOME_DB_TYPE_FORM,
	                                     "model", model, NULL);
	
	return (GtkWidget *) form;
}


static void
gnome_db_form_set_property (GObject *object,
				 guint param_id,
				 const GValue *value,
				 GParamSpec *pspec)
{
	GnomeDbForm *form;
	GdaDataModel *model;

	form = GNOME_DB_FORM (object);
	
	if (form->priv) {
		switch (param_id) {
		case PROP_MODEL:
			model = GDA_DATA_MODEL (g_value_get_object (value));
			g_object_set (G_OBJECT(form->priv->raw_form),
			              "model", model, NULL);
			break;
			
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
			break;
		}
	}
}

static void
gnome_db_form_get_property (GObject *object,
				 guint param_id,
				 GValue *value,
				 GParamSpec *pspec)
{
	GnomeDbForm *form;
	GdaDataModel *model;

        form = GNOME_DB_FORM (object);
        if (form->priv) {
		switch (param_id) {
		case PROP_RAW_FORM:
			g_value_set_object (value, form->priv->raw_form);
			break;
		case PROP_INFO:
			g_value_set_object (value, form->priv->info);
			break;
		case PROP_MODEL:
			g_object_get (G_OBJECT (form->priv->raw_form),
			              "model", &model, NULL);
			g_value_take_object (value, G_OBJECT (model));
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
			break;
		}
        }
}
