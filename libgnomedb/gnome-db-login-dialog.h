/* GNOME DB library
 * Copyright (C) 1999 - 2008 The GNOME Foundation.
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *      Vivien Malerba <malerba@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GNOME_DB_LOGIN_DIALOG_H__
#define __GNOME_DB_LOGIN_DIALOG_H__

#include <libgnomedb/gnome-db-login.h>
#include <gtk/gtkdialog.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_LOGIN_DIALOG            (gnome_db_login_dialog_get_type())
#define GNOME_DB_LOGIN_DIALOG(obj)            (G_TYPE_CHECK_INSTANCE_CAST (obj, GNOME_DB_TYPE_LOGIN_DIALOG, GnomeDbLoginDialog))
#define GNOME_DB_LOGIN_DIALOG_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST (klass, GNOME_DB_TYPE_LOGIN_DIALOG, GnomeDbLoginDialogClass))
#define GNOME_DB_IS_LOGIN_DIALOG(obj)         (G_TYPE_CHECK_INSTANCE_TYPE (obj, GNOME_DB_TYPE_LOGIN_DIALOG))
#define GNOME_DB_IS_LOGIN_DIALOG_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GNOME_DB_TYPE_LOGIN_DIALOG))

typedef struct _GnomeDbLoginDialog        GnomeDbLoginDialog;
typedef struct _GnomeDbLoginDialogClass   GnomeDbLoginDialogClass;
typedef struct _GnomeDbLoginDialogPrivate GnomeDbLoginDialogPrivate;

struct _GnomeDbLoginDialog {
	GtkDialog dialog;
	GnomeDbLoginDialogPrivate *priv;
};

struct _GnomeDbLoginDialogClass {
	GtkDialogClass parent_class;
};

GType        gnome_db_login_dialog_get_type         (void) G_GNUC_CONST;
GtkWidget   *gnome_db_login_dialog_new              (const gchar *title, GtkWindow *parent);
gboolean     gnome_db_login_dialog_run              (GnomeDbLoginDialog *dialog);

const gchar *gnome_db_login_dialog_get_dsn          (GnomeDbLoginDialog *dialog);
const gchar *gnome_db_login_dialog_get_auth         (GnomeDbLoginDialog *dialog);
const gchar *gnome_db_login_dialog_get_username     (GnomeDbLoginDialog *dialog);
const gchar *gnome_db_login_dialog_get_password     (GnomeDbLoginDialog *dialog);

GnomeDbLogin   *gnome_db_login_dialog_get_login_widget (GnomeDbLoginDialog *dialog);

G_END_DECLS

#endif
