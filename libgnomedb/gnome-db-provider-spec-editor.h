/* GNOME DB library
 * Copyright (C) 2005 - 2008 The GNOME Foundation
 *
 * AUTHORS:
 *      Vivien Malerba <malerba@gnome_db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GNOME_DB_PROVIDER_SPEC_EDITOR_H__
#define __GNOME_DB_PROVIDER_SPEC_EDITOR_H__

#include <gtk/gtkvbox.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_PROVIDER_SPEC_EDITOR            (gnome_db_provider_spec_editor_get_type())
#define GNOME_DB_PROVIDER_SPEC_EDITOR(obj)            (G_TYPE_CHECK_INSTANCE_CAST (obj, GNOME_DB_TYPE_PROVIDER_SPEC_EDITOR, GnomeDbProviderSpecEditor))
#define GNOME_DB_PROVIDER_SPEC_EDITOR_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST (klass, GNOME_DB_TYPE_PROVIDER_SPEC_EDITOR, GnomeDbProviderSpecEditorClass))
#define GNOME_DB_IS_PROVIDER_SPEC_EDITOR(obj)         (G_TYPE_CHECK_INSTANCE_TYPE (obj, GNOME_DB_TYPE_PROVIDER_SPEC_EDITOR))
#define GNOME_DB_IS_PROVIDER_SPEC_EDITOR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GNOME_DB_TYPE_PROVIDER_SPEC_EDITOR))

typedef struct _GnomeDbProviderSpecEditor        GnomeDbProviderSpecEditor;
typedef struct _GnomeDbProviderSpecEditorClass   GnomeDbProviderSpecEditorClass;
typedef struct _GnomeDbProviderSpecEditorPrivate GnomeDbProviderSpecEditorPrivate;

struct _GnomeDbProviderSpecEditor {
	GtkVBox                box;
	GnomeDbProviderSpecEditorPrivate *priv;
};

struct _GnomeDbProviderSpecEditorClass {
	GtkVBoxClass           parent_class;

	/* signals */
	void                (* changed) (GnomeDbProviderSpecEditor *spec);
};

GType       gnome_db_provider_spec_editor_get_type     (void) G_GNUC_CONST;
GtkWidget  *gnome_db_provider_spec_editor_new          (const gchar *provider);

void        gnome_db_provider_spec_editor_set_provider (GnomeDbProviderSpecEditor *spec, const gchar *provider);
gboolean    gnome_db_provider_spec_editor_is_valid     (GnomeDbProviderSpecEditor *spec);
gchar      *gnome_db_provider_spec_editor_get_specs    (GnomeDbProviderSpecEditor *spec);
void        gnome_db_provider_spec_editor_set_specs    (GnomeDbProviderSpecEditor *spec, const gchar *specs_string);

G_END_DECLS

#endif
