/* utility.h
 *
 * Copyright (C) 2003 - 2007 Vivien Malerba <malerba@gnome-db.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gtk/gtk.h>
#include <libgda/gda-set.h>
#include "gnome-db-data-entry.h"
#include "gnome-db-data-store.h"
#include "gnome-db-set.h"

/*
 *
 * Data Entry utilities
 *
 */
GtkWidget              *gnome_db_utility_entry_build_actions_menu      (GObject *obj_data, guint attrs, GCallback function);
GdkColor              **gnome_db_utility_entry_build_info_colors_array (void);
gchar                  *gnome_db_utility_markup_title                  (const gchar *title, gboolean optional);

/*
 * Computes attributes and values list from the attributes and values stored in @store and
 * corresponding to the columns for the parameters in @group (as described by @model_iter), 
 * at row pointed by @tree_iter
 */
guint            gnome_db_utility_proxy_compute_attributes_for_group (GnomeDbSetGroup *group, 
								      GnomeDbDataStore *store, GdaDataModelIter *model_iter, 
								      GtkTreeIter *tree_iter, 
								      gboolean *to_be_deleted);
GList           *gnome_db_utility_proxy_compute_values_for_group     (GnomeDbSetGroup *group, 
								      GnomeDbDataStore *store, GdaDataModelIter *model_iter, 
								      GtkTreeIter *tree_iter, gboolean model_values);

/*
 * Some dialogs used by GnomeDbDataWidget widgets
 */
gboolean   gnome_db_utility_display_error_with_keep_or_discard_choice (GnomeDbDataWidget *form, GError *filled_error);
void       gnome_db_utility_display_error                             (GnomeDbDataWidget *form, gboolean can_discard, GError *filled_error);
void       gnome_db_utility_set_data_layout_from_file (GnomeDbDataWidget  *data_widget, const gchar  *file_name,  const gchar  *parent_table);
