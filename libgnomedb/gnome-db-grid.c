/* gnome-db-grid.c
 *
 * Copyright (C) 2002 - 2006 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <string.h>
#include <glib/gi18n-lib.h>
#include <libgda/libgda.h>
#include "gnome-db-grid.h"
#include "gnome-db-data-widget.h"
#include "gnome-db-raw-grid.h"
#include "gnome-db-data-widget-info.h"

static void gnome_db_grid_class_init (GnomeDbGridClass * class);
static void gnome_db_grid_init (GnomeDbGrid *wid);

static void gnome_db_grid_set_property (GObject *object,
					guint param_id,
					const GValue *value,
					GParamSpec *pspec);
static void gnome_db_grid_get_property (GObject *object,
					guint param_id,
					GValue *value,
					GParamSpec *pspec);

struct _GnomeDbGridPriv
{
	GtkWidget *raw_grid;
	GtkWidget *info;
};

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *parent_class = NULL;

/* properties */
enum
{
        PROP_0,
        PROP_RAW_GRID,
	PROP_INFO,
	PROP_MODEL
};

GType
gnome_db_grid_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0)) {
		static const GTypeInfo info = {
			sizeof (GnomeDbGridClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_grid_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbGrid),
			0,
			(GInstanceInitFunc) gnome_db_grid_init
		};		

		type = g_type_register_static (GTK_TYPE_VBOX, "GnomeDbGrid", &info, 0);
	}

	return type;
}

static void
gnome_db_grid_class_init (GnomeDbGridClass *class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);
	
	parent_class = g_type_class_peek_parent (class);

	/* Properties */
        object_class->set_property = gnome_db_grid_set_property;
        object_class->get_property = gnome_db_grid_get_property;
	g_object_class_install_property (object_class, PROP_RAW_GRID,
                                         g_param_spec_object ("raw_grid", NULL, NULL, 
							      GNOME_DB_TYPE_RAW_GRID,
							      G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_INFO,
                                         g_param_spec_object ("widget_info", NULL, NULL, 
							      GNOME_DB_TYPE_DATA_WIDGET_INFO,
							      G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_MODEL,
	                                 g_param_spec_object ("model", NULL, NULL,
	                                                      GDA_TYPE_DATA_MODEL,
	                                                      G_PARAM_READWRITE | G_PARAM_CONSTRUCT_ONLY));
}

static void
gnome_db_grid_init (GnomeDbGrid *grid)
{
	GtkWidget *sw;
	
	grid->priv = g_new0 (GnomeDbGridPriv, 1);
	grid->priv->raw_grid = NULL;
	grid->priv->info = NULL;
	
	sw = gtk_scrolled_window_new (NULL, NULL);
        gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
        gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (sw), GTK_SHADOW_IN);
	gtk_box_pack_start (GTK_BOX (grid), sw, TRUE, TRUE, 0);
	gtk_widget_show (sw);

	grid->priv->raw_grid = gnome_db_raw_grid_new (NULL);
	gtk_container_add (GTK_CONTAINER (sw), grid->priv->raw_grid);
	gtk_widget_show (grid->priv->raw_grid);

	grid->priv->info = gnome_db_data_widget_info_new (GNOME_DB_DATA_WIDGET (grid->priv->raw_grid), 
							  GNOME_DB_DATA_WIDGET_INFO_CURRENT_ROW |
							  GNOME_DB_DATA_WIDGET_INFO_ROW_MODIFY_BUTTONS |
							  GNOME_DB_DATA_WIDGET_INFO_CHUNCK_CHANGE_BUTTONS);
	gtk_box_pack_start (GTK_BOX (grid), grid->priv->info, FALSE, TRUE, 0);
	gtk_widget_show (grid->priv->info);
}

/**
 * gnome_db_grid_new
 * @model: a #GdaDataModel
 *
 * Creates a new #GnomeDbGrid widget suitable to display the data in @model
 *
 *  Returns: the new widget
 */
GtkWidget *
gnome_db_grid_new (GdaDataModel *model)
{
	GnomeDbGrid *grid;
	
	g_return_val_if_fail (!model || GDA_IS_DATA_MODEL (model), NULL);

	grid = (GnomeDbGrid *) g_object_new (GNOME_DB_TYPE_GRID,
	                                     "model", model, NULL);

	return (GtkWidget *) grid;
}


static void
gnome_db_grid_set_property (GObject *object,
				 guint param_id,
				 const GValue *value,
				 GParamSpec *pspec)
{
	GnomeDbGrid *grid;
	GdaDataModel *model;
	
	grid = GNOME_DB_GRID (object);
	
	switch (param_id) {
	case PROP_MODEL:
		model = GDA_DATA_MODEL (g_value_get_object (value));
		g_object_set(G_OBJECT (grid->priv->raw_grid),
		             "model", model, NULL);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}

static void
gnome_db_grid_get_property (GObject *object,
				 guint param_id,
				 GValue *value,
				 GParamSpec *pspec)
{
	GnomeDbGrid *grid;
	GdaDataModel *model;

	grid = GNOME_DB_GRID (object);
	
	switch (param_id) {
	case PROP_RAW_GRID:
		g_value_set_object (value, grid->priv->raw_grid);
		break;
	case PROP_INFO:
		g_value_set_object (value, grid->priv->info);
		break;
	case PROP_MODEL:
		g_object_get (G_OBJECT (grid->priv->raw_grid),
		              "model", &model, NULL);
		g_value_take_object (value, G_OBJECT (model));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}	
}

/**
 * gnome_db_grid_get_selection
 * @grid: a #GnomeDbGrid widget
 * 
 * Returns the list of the currently selected rows in a #GnomeDbGrid widget. 
 * The returned value is a list of integers, which represent each of the selected rows.
 *
 * If new rows have been inserted, then those new rows will have a row number equal to -1.
 * This function is a wrapper around the gnome_db_raw_grid_get_selection() function.
 *
 * Returns: a new list, should be freed (by calling g_list_free) when no longer needed.
 */
GList *
gnome_db_grid_get_selection (GnomeDbGrid *grid)
{
	g_return_val_if_fail (grid && GNOME_DB_IS_GRID (grid), NULL);
	g_return_val_if_fail (grid->priv, NULL);

	return gnome_db_raw_grid_get_selection (GNOME_DB_RAW_GRID (grid->priv->raw_grid));
}

/**
 * gnome_db_grid_set_sample_size
 * @grid: a #GnomeDbGrid widget
 * @sample_size:
 *
 *
 */
void
gnome_db_grid_set_sample_size (GnomeDbGrid *grid, gint sample_size)
{
	g_return_if_fail (grid && GNOME_DB_IS_GRID (grid));
	g_return_if_fail (grid->priv);

	gnome_db_raw_grid_set_sample_size (GNOME_DB_RAW_GRID (grid->priv->raw_grid), sample_size);
}
