/* gnome-db-server-operation.h
 *
 * Copyright (C) 2006 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNOME_DB_SERVER_OPERATION__
#define __GNOME_DB_SERVER_OPERATION__

#include <gtk/gtk.h>
#include <libgda/libgda.h>
#ifdef HAVE_LIBGLADE
#include <glade/glade.h>
#endif

G_BEGIN_DECLS

#define GNOME_DB_TYPE_SERVER_OPERATION          (gnome_db_server_operation_get_type())
#define GNOME_DB_SERVER_OPERATION(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, gnome_db_server_operation_get_type(), GnomeDbServerOperation)
#define GNOME_DB_SERVER_OPERATION_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, gnome_db_server_operation_get_type (), GnomeDbServerOperationClass)
#define GNOME_DB_IS_SERVER_OPERATION(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, gnome_db_server_operation_get_type ())


typedef struct _GnomeDbServerOperation      GnomeDbServerOperation;
typedef struct _GnomeDbServerOperationClass GnomeDbServerOperationClass;
typedef struct _GnomeDbServerOperationPriv  GnomeDbServerOperationPriv;

/* struct for the object's data */
struct _GnomeDbServerOperation
{
	GtkVBox                     object;
	GnomeDbServerOperationPriv *priv;
};

/* struct for the object's class */
struct _GnomeDbServerOperationClass
{
	GtkVBoxClass                parent_class;
};

/* 
 * Generic widget's methods 
*/
GType             gnome_db_server_operation_get_type      (void) G_GNUC_CONST;
GtkWidget        *gnome_db_server_operation_new           (GdaServerOperation *op);
GtkWidget        *gnome_db_server_operation_new_in_dialog (GdaServerOperation *op, GtkWindow *parent,
							   const gchar *title, const gchar *header);

G_END_DECLS

#endif



