/* gnome-db-data-widget-info.c
 *
 * Copyright (C) 2006 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <string.h>
#include <glib/gi18n-lib.h>
#include <libgda/libgda.h>
#include "gnome-db-data-widget.h"
#include "gnome-db-raw-grid.h"
#include "gnome-db-data-widget-info.h"
#include "gnome-db-enum-types.h"

static void gnome_db_data_widget_info_class_init (GnomeDbDataWidgetInfoClass * class);
static void gnome_db_data_widget_info_init (GnomeDbDataWidgetInfo *wid);
static void gnome_db_data_widget_info_dispose (GObject *object);

static void gnome_db_data_widget_info_set_property (GObject *object,
						    guint param_id,
						    const GValue *value,
						    GParamSpec *pspec);
static void gnome_db_data_widget_info_get_property (GObject *object,
						    guint param_id,
						    GValue *value,
						    GParamSpec *pspec);

static void modif_buttons_make (GnomeDbDataWidgetInfo *info);
static void modif_buttons_update (GnomeDbDataWidgetInfo *info);

static void data_widget_proxy_changed_cb (GnomeDbDataWidget *data_widget, GdaDataProxy *proxy, GnomeDbDataWidgetInfo *info);
static void proxy_changed_cb (GdaDataProxy *proxy, GnomeDbDataWidgetInfo *info);
static void proxy_sample_changed_cb (GdaDataProxy *proxy, gint sample_start, gint sample_end, GnomeDbDataWidgetInfo *info);
static void proxy_row_changed_cb (GdaDataProxy *proxy, gint row, GnomeDbDataWidgetInfo *info);
static void raw_grid_selection_changed_cb (GnomeDbRawGrid *grid, gboolean row_selected, 
					   GnomeDbDataWidgetInfo *info);


struct _GnomeDbDataWidgetInfoPriv
{
	GnomeDbDataWidget *data_widget;
	GdaDataProxy      *proxy;
	GdaDataModelIter  *iter;
	GnomeDbDataWidgetInfoFlag flags; /* ORed values. */

	GtkUIManager      *uimanager;

	GtkWidget         *buttons_bar;
	GtkWidget         *current_sample;
	GtkWidget         *row_spin;
};

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *parent_class = NULL;

/* properties */
enum
{
        PROP_0,
        PROP_DATA_WIDGET,
	PROP_FLAGS
};

GType
gnome_db_data_widget_info_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0)) {
		static const GTypeInfo info = {
			sizeof (GnomeDbDataWidgetInfoClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_data_widget_info_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbDataWidgetInfo),
			0,
			(GInstanceInitFunc) gnome_db_data_widget_info_init
		};		

		type = g_type_register_static (GTK_TYPE_HBOX, "GnomeDbDataWidgetInfo", &info, 0);
	}

	return type;
}

static void
gnome_db_data_widget_info_class_init (GnomeDbDataWidgetInfoClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);
	
	parent_class = g_type_class_peek_parent (class);


	object_class->dispose = gnome_db_data_widget_info_dispose;

	/* Properties */
        object_class->set_property = gnome_db_data_widget_info_set_property;
        object_class->get_property = gnome_db_data_widget_info_get_property;
	g_object_class_install_property (object_class, PROP_DATA_WIDGET,
                                         g_param_spec_object ("data_widget", NULL, NULL, GNOME_DB_TYPE_DATA_WIDGET,
                                                               G_PARAM_READABLE | G_PARAM_WRITABLE));
	g_object_class_install_property (object_class, PROP_FLAGS,
                                         g_param_spec_flags ("flags", NULL, NULL, GNOME_DB_TYPE_DB_DATA_WIDGET_INFO_FLAG, 
							    GNOME_DB_DATA_WIDGET_INFO_CURRENT_ROW,
							    G_PARAM_READABLE | G_PARAM_WRITABLE));
}

static void
gnome_db_data_widget_info_init (GnomeDbDataWidgetInfo * wid)
{
	wid->priv = g_new0 (GnomeDbDataWidgetInfoPriv, 1);
	wid->priv->data_widget = NULL;
	wid->priv->proxy = NULL;
}

/**
 * gnome_db_data_widget_info_new
 * @data_widget: a widget implementing the #GnomeDbDataWidget interface
 * @flags: OR'ed values, specifying what to display in the new widget
 *
 * Creates a new #GnomeDbDataWidgetInfo widget suitable to display information about @data_widget
 *
 * Returns: the new widget
 */
GtkWidget *
gnome_db_data_widget_info_new (GnomeDbDataWidget *data_widget, GnomeDbDataWidgetInfoFlag flags)
{
	GtkWidget *info;

	g_return_val_if_fail (!data_widget || GNOME_DB_IS_DATA_WIDGET (data_widget), NULL);

	info = (GtkWidget *) g_object_new (GNOME_DB_TYPE_DATA_WIDGET_INFO, 
					   "data_widget", data_widget, 
					   "flags", flags, NULL);

	return info;
}

static void
data_widget_destroyed_cb (GnomeDbDataWidget *wid, GnomeDbDataWidgetInfo *info)
{
	g_assert (wid == info->priv->data_widget);
	g_signal_handlers_disconnect_by_func (G_OBJECT (wid),
					      G_CALLBACK (data_widget_destroyed_cb), info);
	g_signal_handlers_disconnect_by_func (G_OBJECT (wid),
					      G_CALLBACK (data_widget_proxy_changed_cb), info);
	if (GNOME_DB_IS_RAW_GRID (info->priv->data_widget))
		g_signal_handlers_disconnect_by_func (info->priv->data_widget,
						      G_CALLBACK (raw_grid_selection_changed_cb), info);

	info->priv->data_widget = NULL;
}


static void
release_proxy (GnomeDbDataWidgetInfo *info)
{
	g_signal_handlers_disconnect_by_func (G_OBJECT (info->priv->proxy),
					      G_CALLBACK (proxy_changed_cb), info);
	g_signal_handlers_disconnect_by_func (G_OBJECT (info->priv->proxy),
					      G_CALLBACK (proxy_sample_changed_cb), info);
	g_signal_handlers_disconnect_by_func (G_OBJECT (info->priv->proxy),
					      G_CALLBACK (proxy_row_changed_cb), info);
	g_object_unref (info->priv->proxy);
	info->priv->proxy = NULL;
}

static void iter_row_changed_cb (GdaDataModelIter *iter, gint row, GnomeDbDataWidgetInfo *info);
static void
release_iter (GnomeDbDataWidgetInfo *info)
{
	g_signal_handlers_disconnect_by_func (info->priv->iter,
					      G_CALLBACK (iter_row_changed_cb), info);
	info->priv->iter = NULL;
}

static void
data_widget_proxy_changed_cb (GnomeDbDataWidget *data_widget, GdaDataProxy *proxy, GnomeDbDataWidgetInfo *info)
{
	g_object_set (G_OBJECT (info), "data_widget", data_widget, NULL);
}

static void
gnome_db_data_widget_info_dispose (GObject *object)
{
	GnomeDbDataWidgetInfo *info;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOME_DB_IS_DATA_WIDGET_INFO (object));
	info = GNOME_DB_DATA_WIDGET_INFO (object);

	if (info->priv) {
		if (info->priv->proxy)
			release_proxy (info);
		if (info->priv->iter)
			release_iter (info);
		if (info->priv->data_widget)
			data_widget_destroyed_cb (info->priv->data_widget, info);

		/* the private area itself */
		g_free (info->priv);
		info->priv = NULL;
	}

	/* for the parent class */
	parent_class->dispose (object);
}

static void
gnome_db_data_widget_info_set_property (GObject *object,
					guint param_id,
					const GValue *value,
					GParamSpec *pspec)
{
	GnomeDbDataWidgetInfo *info;

        info = GNOME_DB_DATA_WIDGET_INFO (object);
        if (info->priv) {
                switch (param_id) {
                case PROP_DATA_WIDGET:
			if (info->priv->data_widget)
				data_widget_destroyed_cb (info->priv->data_widget, info);
			if (info->priv->iter) 
				release_iter (info);
			if (info->priv->proxy)
				release_proxy (info);

			info->priv->data_widget = GNOME_DB_DATA_WIDGET (g_value_get_object (value));
			if (info->priv->data_widget) {
				GdaDataProxy *proxy;
				GdaDataModelIter *iter;

				/* data widget */
				g_signal_connect (info->priv->data_widget, "destroy",
						  G_CALLBACK (data_widget_destroyed_cb), info);
				g_signal_connect (info->priv->data_widget, "proxy_changed",
						  G_CALLBACK (data_widget_proxy_changed_cb), info);
				if (GNOME_DB_IS_RAW_GRID (info->priv->data_widget))
					g_signal_connect (info->priv->data_widget, "selection_changed",
							  G_CALLBACK (raw_grid_selection_changed_cb), info);

				/* proxy */
				proxy = gnome_db_data_widget_get_proxy (info->priv->data_widget);
				if (proxy) {
					info->priv->proxy = proxy;
					g_object_ref (info->priv->proxy);
					g_signal_connect (G_OBJECT (proxy), "changed",
							  G_CALLBACK (proxy_changed_cb), info);
					g_signal_connect (G_OBJECT (proxy), "sample_changed",
							  G_CALLBACK (proxy_sample_changed_cb), info);
					g_signal_connect (G_OBJECT (proxy), "row_inserted",
							  G_CALLBACK (proxy_row_changed_cb), info);
					g_signal_connect (G_OBJECT (proxy), "row_removed",
							  G_CALLBACK (proxy_row_changed_cb), info);
					
					/* iter */
					iter = gnome_db_data_widget_get_current_data (GNOME_DB_DATA_WIDGET 
										      (info->priv->data_widget));
					info->priv->iter = iter;
					if (iter) {
						g_object_ref (G_OBJECT (iter));
						g_signal_connect (iter, "row_changed",
								  G_CALLBACK (iter_row_changed_cb), info);
					}
				}
				modif_buttons_update (info);
			}
                        break;
                case PROP_FLAGS:
			info->priv->flags = g_value_get_flags (value);
			if (info->priv->buttons_bar) {
				gtk_widget_destroy (info->priv->buttons_bar);
				info->priv->buttons_bar = NULL;
			}
			if (info->priv->current_sample) {
				gtk_widget_destroy (info->priv->current_sample);
				info->priv->current_sample = NULL;
			}
			if (info->priv->row_spin) {
				gtk_widget_destroy (info->priv->row_spin);
				info->priv->row_spin = NULL;
			}

			modif_buttons_make (info);
			modif_buttons_update (info);
                        break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
			break;
                }
        }
}

static void
gnome_db_data_widget_info_get_property (GObject *object,
					guint param_id,
					GValue *value,
					GParamSpec *pspec)
{
	GnomeDbDataWidgetInfo *info;

        info = GNOME_DB_DATA_WIDGET_INFO (object);
        if (info->priv) {
                switch (param_id) {
		case PROP_DATA_WIDGET:
			g_value_set_pointer (value, info->priv->data_widget);
			break;
		case PROP_FLAGS:
			g_value_set_flags (value, info->priv->flags);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
			break;
                }
        }	
}


static void
proxy_changed_cb (GdaDataProxy *proxy, GnomeDbDataWidgetInfo *info)
{
	modif_buttons_update (info);
}

static void
proxy_sample_changed_cb (GdaDataProxy *proxy, gint sample_start, gint sample_end, GnomeDbDataWidgetInfo *info)
{
	modif_buttons_update (info);
}

static void
proxy_row_changed_cb (GdaDataProxy *proxy, gint row, GnomeDbDataWidgetInfo *info)
{
	modif_buttons_update (info);
}

static void
iter_row_changed_cb (GdaDataModelIter *iter, gint row, GnomeDbDataWidgetInfo *info)
{
	modif_buttons_update (info);
}

static void
raw_grid_selection_changed_cb (GnomeDbRawGrid *grid, gboolean row_selected, GnomeDbDataWidgetInfo *info)
{
	modif_buttons_update (info);
}

/*
 *
 * Modification buttons (Commit changes, Reset info, New entry, Delete)
 *
 */
static const gchar *ui_row_modif =
	"<ui>"
	"  <toolbar  name='ToolBar'>"
	"    <toolitem action='ActionNew'/>"
	"    <toolitem action='ActionDelete'/>"
	"    <toolitem action='ActionUndelete'/>"
	"    <toolitem action='ActionCommit'/>"
	"    <toolitem action='ActionReset'/>"
	"  </toolbar>"
	"</ui>";
static const gchar *ui_row_move =
	"<ui>"
	"  <toolbar  name='ToolBar'>"
	"    <toolitem action='ActionFirstRecord'/>"
	"    <toolitem action='ActionPrevRecord'/>"
	"    <toolitem action='ActionNextRecord'/>"
	"    <toolitem action='ActionLastRecord'/>"
	"    <toolitem action='ActionFilter'/>"
	"  </toolbar>"
	"</ui>";
static const gchar *ui_chunck_change =
	"<ui>"
	"  <toolbar  name='ToolBar'>"
	"    <toolitem action='ActionFirstChunck'/>"
	"    <toolitem action='ActionPrevChunck'/>"
	"    <toolitem action='ActionNextChunck'/>"
	"    <toolitem action='ActionLastChunck'/>"
	"    <toolitem action='ActionFilter'/>"
	"  </toolbar>"
	"</ui>";

static void row_spin_changed_cb (GtkSpinButton *spin, GnomeDbDataWidgetInfo *info);
static void
modif_buttons_make (GnomeDbDataWidgetInfo *info)
{
	GtkWidget *wid;
	GnomeDbDataWidgetInfoFlag flags = info->priv->flags;

	if (! info->priv->data_widget)
		return;

	if (flags & (GNOME_DB_DATA_WIDGET_INFO_ROW_MODIFY_BUTTONS |
		     GNOME_DB_DATA_WIDGET_INFO_ROW_MOVE_BUTTONS |
		     GNOME_DB_DATA_WIDGET_INFO_CHUNCK_CHANGE_BUTTONS)) {
		GtkActionGroup *actions;
		GtkUIManager *ui;

		actions = gnome_db_data_widget_get_actions_group (info->priv->data_widget);
		ui = gtk_ui_manager_new ();
		gtk_ui_manager_insert_action_group (ui, actions, 0);
		if (flags & GNOME_DB_DATA_WIDGET_INFO_ROW_MODIFY_BUTTONS)
			gtk_ui_manager_add_ui_from_string (ui, ui_row_modif, -1, NULL);
		if (flags & GNOME_DB_DATA_WIDGET_INFO_ROW_MOVE_BUTTONS)
			gtk_ui_manager_add_ui_from_string (ui, ui_row_move, -1, NULL);
		if (flags & GNOME_DB_DATA_WIDGET_INFO_CHUNCK_CHANGE_BUTTONS)
			gtk_ui_manager_add_ui_from_string (ui, ui_chunck_change, -1, NULL);

		info->priv->uimanager = ui;
		info->priv->buttons_bar = gtk_ui_manager_get_widget (ui, "/ToolBar");
		g_object_set (G_OBJECT (info->priv->buttons_bar), "toolbar-style", GTK_TOOLBAR_ICONS, NULL);
		gtk_toolbar_set_tooltips (GTK_TOOLBAR (info->priv->buttons_bar), TRUE);
		gtk_box_pack_start (GTK_BOX (info), info->priv->buttons_bar, TRUE, TRUE, 0);
		gtk_widget_show (info->priv->buttons_bar);
	}

	if (flags & GNOME_DB_DATA_WIDGET_INFO_CURRENT_ROW) {
		if (flags & GNOME_DB_DATA_WIDGET_INFO_ROW_MOVE_BUTTONS) {
			/* read-write spin counter (mainly for forms) */
			wid = gtk_spin_button_new_with_range (0, 1, 1);
			gtk_spin_button_set_digits (GTK_SPIN_BUTTON (wid), 0);
			gtk_spin_button_set_numeric (GTK_SPIN_BUTTON (wid), TRUE);
			gtk_box_pack_start (GTK_BOX (info), wid, FALSE, TRUE, 2);
			gtk_widget_show (wid);
			gtk_widget_set_sensitive (wid, FALSE);
			info->priv->row_spin = wid;
			g_signal_connect (G_OBJECT (wid), "value_changed",
					  G_CALLBACK (row_spin_changed_cb), info);

			/* rows counter */
			wid = gtk_label_new (" /?");
			gtk_widget_show (wid);
			info->priv->current_sample = wid;
			gtk_box_pack_start (GTK_BOX (info), wid, FALSE, FALSE, 2);
		}
		else {
			/* read-only counter (mainly for grids) */
			wid = gtk_label_new ("? - ? /?");
			gtk_widget_show (wid);
			info->priv->current_sample = wid;
			gtk_box_pack_start (GTK_BOX (info), wid, FALSE, FALSE, 2);
		}
	}
}

static void
row_spin_changed_cb (GtkSpinButton *spin, GnomeDbDataWidgetInfo *info)
{
	gint row;
	gint value = gtk_spin_button_get_value (spin);

	if ((value >= 1) &&
	    (value <= gda_data_model_get_n_rows (GDA_DATA_MODEL (info->priv->proxy))))
		row = value - 1;

	gda_data_model_iter_move_to_row (gnome_db_data_widget_get_current_data (info->priv->data_widget), row);
}

#define BLOCK_SPIN (g_signal_handlers_block_by_func (G_OBJECT (info->priv->row_spin), \
						     G_CALLBACK (row_spin_changed_cb), info))
#define UNBLOCK_SPIN (g_signal_handlers_unblock_by_func (G_OBJECT (info->priv->row_spin), \
							 G_CALLBACK (row_spin_changed_cb), info))
static void
modif_buttons_update (GnomeDbDataWidgetInfo *info)
{
	GdaDataModelIter *model_iter;
	gboolean wrows, filtered_proxy = FALSE;
	gint has_selection;	
	gint row;

	gint proxy_rows = 0;
	gint proxied_rows = 0;
	gint all_rows = 0;

	GtkAction *action;
	gint sample_first_row = 0, sample_last_row = 0, sample_size = 0;
	GnomeDbDataWidgetInfoFlag flags = 0;

	model_iter = gnome_db_data_widget_get_current_data (info->priv->data_widget);
	if (info->priv->proxy) {
		filtered_proxy = gda_data_proxy_get_filter_expr (info->priv->proxy) ? TRUE : FALSE;
		proxy_rows = gda_data_model_get_n_rows (GDA_DATA_MODEL (info->priv->proxy));
		if (filtered_proxy) {
			proxied_rows = gda_data_proxy_get_filtered_n_rows (info->priv->proxy);
			all_rows = proxied_rows;
		}
		else {
			proxied_rows = gda_data_proxy_get_proxied_model_n_rows (info->priv->proxy);
			all_rows = proxied_rows + gda_data_proxy_get_n_new_rows (info->priv->proxy);
		}

		/* samples don't take into account the proxy's inserted rows */
		sample_first_row = gda_data_proxy_get_sample_start (info->priv->proxy);
		sample_last_row = gda_data_proxy_get_sample_end (info->priv->proxy);
		sample_size = gda_data_proxy_get_sample_size (info->priv->proxy);

		flags = gda_data_model_get_access_flags (GDA_DATA_MODEL (info->priv->proxy));
	}

	/* sensitiveness of the text indications and of the spin button */
	wrows = (proxy_rows <= 0) ? FALSE : TRUE;
	row = model_iter ? gda_data_model_iter_get_row (model_iter) : 0;
	if (info->priv->flags & GNOME_DB_DATA_WIDGET_INFO_CURRENT_ROW) {
		if (proxy_rows < 0) {
			if (info->priv->flags & GNOME_DB_DATA_WIDGET_INFO_ROW_MOVE_BUTTONS) {
				BLOCK_SPIN;
				gtk_spin_button_set_range (GTK_SPIN_BUTTON (info->priv->row_spin), 0, 1);
				gtk_spin_button_set_value (GTK_SPIN_BUTTON (info->priv->row_spin), 0);
				UNBLOCK_SPIN;
				gtk_label_set_text (GTK_LABEL (info->priv->current_sample), " /?");
			}
			else
				gtk_label_set_text (GTK_LABEL (info->priv->current_sample), "? - ? /?");
		}
		else {
			gchar *str;
			gint total;
			
			total = sample_first_row + proxy_rows;
			if (info->priv->flags & GNOME_DB_DATA_WIDGET_INFO_ROW_MOVE_BUTTONS) {
				if (total <= 0)
					str = g_strdup (" / 0");
				else {
					if (filtered_proxy)
						str = g_strdup_printf (" / (%d)", proxy_rows);
					else
						str = g_strdup_printf (" / %d", proxy_rows);
				}
				BLOCK_SPIN;
				gtk_spin_button_set_range (GTK_SPIN_BUTTON (info->priv->row_spin), 1, proxy_rows);
				if (row >= 0)
					if (gtk_spin_button_get_value (GTK_SPIN_BUTTON (info->priv->row_spin)) != row+1)
						gtk_spin_button_set_value (GTK_SPIN_BUTTON (info->priv->row_spin), row+1);
				UNBLOCK_SPIN;
			}
			else {
				if (total <= 0)
					str = g_strdup_printf ("0 - 0 / 0");
				else {
					if (all_rows < 0)
						str = g_strdup_printf ("%d - %d /?", sample_first_row + 1, total);
					else {
						if (filtered_proxy) 
							str = g_strdup_printf ("%d - %d / (%d)", 
									       sample_first_row + 1, total, all_rows);
						else
							str = g_strdup_printf ("%d - %d / %d", 
									       sample_first_row + 1, total, all_rows);
					}
				}
			}

			gtk_label_set_text (GTK_LABEL (info->priv->current_sample), str);
			g_free (str);
		}

		gtk_widget_set_sensitive (info->priv->current_sample, wrows);
		if (info->priv->row_spin)
			gtk_widget_set_sensitive (info->priv->row_spin, wrows && (row >= 0));
	}

	/* current row modifications */
	if (info->priv->buttons_bar) {
		gboolean changed = FALSE;
		gboolean to_be_deleted = FALSE;
		gboolean is_inserted = FALSE;
		gboolean force_del_btn = FALSE;
		gboolean force_undel_btn = FALSE;

		if (info->priv->proxy) {
			changed = gda_data_proxy_has_changed (info->priv->proxy);
			
			has_selection = (row >= 0) ? TRUE : FALSE;
			if (has_selection) {
				to_be_deleted = gda_data_proxy_row_is_deleted (info->priv->proxy, row);
				is_inserted = gda_data_proxy_row_is_inserted (info->priv->proxy, row);
			}
			else 
				if (GNOME_DB_IS_RAW_GRID (info->priv->data_widget)) {
					/* bad for encapsulation, but very useful... */
					GList *sel, *list;
					
					sel = gnome_db_raw_grid_get_selection ((GnomeDbRawGrid*) info->priv->data_widget);
					if (sel) {
						list = sel;
						while (list && (!force_del_btn || !force_undel_btn)) {
							if ((GPOINTER_TO_INT (list->data) != -1) && 
							    gda_data_proxy_row_is_deleted (info->priv->proxy, 
											   GPOINTER_TO_INT (list->data)))
								force_undel_btn = TRUE;
							else
								force_del_btn = TRUE;
							list = g_list_next (list);
						}
						list = sel;

						is_inserted = TRUE;
						while (list && is_inserted) {
							if (GPOINTER_TO_INT (list->data) != -1)
								is_inserted = FALSE;
							list = g_list_next (list);
						}
						g_list_free (sel);
					}
				}
		}

		if (info->priv->flags & GNOME_DB_DATA_WIDGET_INFO_ROW_MODIFY_BUTTONS) {
			GnomeDbDataWidgetWriteMode mode;
			mode = gnome_db_data_widget_get_write_mode (info->priv->data_widget);

			action = gtk_ui_manager_get_action (info->priv->uimanager, "/ToolBar/ActionCommit");
			g_object_set (G_OBJECT (action), "sensitive", changed ? TRUE : FALSE, NULL);
			if (mode == GNOME_DB_DATA_WIDGET_WRITE_ON_VALUE_CHANGE)
				gtk_action_set_visible (action, FALSE);
			else
				gtk_action_set_visible (action, TRUE);
			
			action = gtk_ui_manager_get_action (info->priv->uimanager, "/ToolBar/ActionReset");
			g_object_set (G_OBJECT (action), "sensitive", changed ? TRUE : FALSE, NULL);
			if (mode == GNOME_DB_DATA_WIDGET_WRITE_ON_VALUE_CHANGE)
				gtk_action_set_visible (action, FALSE);
			else
				gtk_action_set_visible (action, TRUE);
			
			action = gtk_ui_manager_get_action (info->priv->uimanager, "/ToolBar/ActionNew");
			g_object_set (G_OBJECT (action), "sensitive", 
				      flags & GDA_DATA_MODEL_ACCESS_INSERT ? TRUE : FALSE, NULL);
			
			action = gtk_ui_manager_get_action (info->priv->uimanager, "/ToolBar/ActionDelete");
			wrows = is_inserted ||
				((flags & GDA_DATA_MODEL_ACCESS_DELETE) && 
				 (force_del_btn || (! to_be_deleted && has_selection)));
			g_object_set (G_OBJECT (action), "sensitive", wrows, NULL);
			
			action = gtk_ui_manager_get_action (info->priv->uimanager, "/ToolBar/ActionUndelete");
			wrows = (flags & GDA_DATA_MODEL_ACCESS_DELETE) && 
				(force_undel_btn || (to_be_deleted && has_selection));
			g_object_set (G_OBJECT (action), "sensitive", wrows, NULL);

			if ((mode == GNOME_DB_DATA_WIDGET_WRITE_ON_ROW_CHANGE) ||
			    (mode == GNOME_DB_DATA_WIDGET_WRITE_ON_VALUE_CHANGE) ||
			    (mode == GNOME_DB_DATA_WIDGET_WRITE_ON_VALUE_ACTIVATED))
				gtk_action_set_visible (action, FALSE);
			else 
				gtk_action_set_visible (action, TRUE);
		}
	}

	/* current row moving */
	if (info->priv->flags & GNOME_DB_DATA_WIDGET_INFO_ROW_MOVE_BUTTONS) {
		action = gtk_ui_manager_get_action (info->priv->uimanager, "/ToolBar/ActionFirstRecord");
		g_object_set (G_OBJECT (action), "sensitive", (row <= 0) ? FALSE : TRUE, NULL);
		action = gtk_ui_manager_get_action (info->priv->uimanager, "/ToolBar/ActionPrevRecord");
		g_object_set (G_OBJECT (action), "sensitive", (row <= 0) ? FALSE : TRUE, NULL);
		action = gtk_ui_manager_get_action (info->priv->uimanager, "/ToolBar/ActionNextRecord");
		g_object_set (G_OBJECT (action), "sensitive", (row == proxy_rows -1) || (row < 0) ? FALSE : TRUE, 
			      NULL);
		action = gtk_ui_manager_get_action (info->priv->uimanager, "/ToolBar/ActionLastRecord");
		g_object_set (G_OBJECT (action), "sensitive", (row == proxy_rows -1) || (row < 0) ? FALSE : TRUE, 
			      NULL);
	}

	/* chunck indications */
	if (info->priv->flags & GNOME_DB_DATA_WIDGET_INFO_CHUNCK_CHANGE_BUTTONS) {
		gboolean abool;
		wrows = (sample_size > 0) ? TRUE : FALSE;
		action = gtk_ui_manager_get_action (info->priv->uimanager, "/ToolBar/ActionFirstChunck");
		g_object_set (G_OBJECT (action), "sensitive", wrows && sample_first_row > 0 ? TRUE : FALSE, NULL);
		action = gtk_ui_manager_get_action (info->priv->uimanager, "/ToolBar/ActionPrevChunck");
		g_object_set (G_OBJECT (action), "sensitive", wrows && sample_first_row > 0 ? TRUE : FALSE, NULL);
		action = gtk_ui_manager_get_action (info->priv->uimanager, "/ToolBar/ActionNextChunck");
		abool = (proxied_rows != -1) ? wrows && (sample_last_row < proxied_rows - 1) : TRUE;
		g_object_set (G_OBJECT (action), "sensitive", abool, NULL);
		action = gtk_ui_manager_get_action (info->priv->uimanager, "/ToolBar/ActionLastChunck");
		g_object_set (G_OBJECT (action), "sensitive", wrows && (sample_last_row < proxied_rows - 1), NULL);
	}

	/* filter */
	if (info->priv->flags & GNOME_DB_DATA_WIDGET_INFO_NO_FILTER) {
		action = gtk_ui_manager_get_action (info->priv->uimanager, "/ToolBar/ActionFilter");
		g_object_set (G_OBJECT (action), "visible", FALSE, NULL);
	}
	
	if (info->priv->uimanager)
		gtk_ui_manager_ensure_update (info->priv->uimanager); 
}
