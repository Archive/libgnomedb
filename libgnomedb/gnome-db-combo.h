/* GNOME DB library
 *
 * Copyright (C) 1999 - 2005 The Free Software Foundation
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *      Vivien Malerba <malerba@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GNOME_DB_COMBO_H__
#define __GNOME_DB_COMBO_H__

#include "gnome-db-decl.h"
#include <gtk/gtk.h>
#include <libgda/libgda.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_COMBO            (gnome_db_combo_get_type())
#define GNOME_DB_COMBO(obj)            (G_TYPE_CHECK_INSTANCE_CAST (obj, GNOME_DB_TYPE_COMBO, GnomeDbCombo))
#define GNOME_DB_COMBO_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST (klass, GNOME_DB_TYPE_COMBO, GnomeDbComboClass))
#define GNOME_DB_IS_COMBO(obj)         (G_TYPE_CHECK_INSTANCE_TYPE (obj, GNOME_DB_TYPE_COMBO))
#define GNOME_DB_IS_COMBO_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GNOME_DB_TYPE_COMBO))

typedef struct _GnomeDbCombo        GnomeDbCombo;
typedef struct _GnomeDbComboClass   GnomeDbComboClass;
typedef struct _GnomeDbComboPrivate GnomeDbComboPrivate;

struct _GnomeDbCombo {
	GtkComboBox          object;
	GnomeDbComboPrivate *priv;
};

struct _GnomeDbComboClass {
	GtkComboBoxClass     parent_class;
};

GType         gnome_db_combo_get_type         (void) G_GNUC_CONST;

GtkWidget    *gnome_db_combo_new              (void);
GtkWidget    *gnome_db_combo_new_with_model   (GdaDataModel *model, gint n_cols, gint *cols_index);

void          gnome_db_combo_set_model        (GnomeDbCombo *combo, GdaDataModel *model, gint n_cols, gint *cols_index);
GdaDataModel *gnome_db_combo_get_model        (GnomeDbCombo *combo);
void          gnome_db_combo_add_undef_choice (GnomeDbCombo *combo, gboolean add_undef_choice);

gboolean      gnome_db_combo_set_values       (GnomeDbCombo *combo, const GSList *values);
GSList       *gnome_db_combo_get_values       (GnomeDbCombo *combo);
gboolean      gnome_db_combo_undef_selected   (GnomeDbCombo *combo);

gboolean      gnome_db_combo_set_values_ext   (GnomeDbCombo *combo, const GSList *values, gint *cols_index);
GSList       *gnome_db_combo_get_values_ext   (GnomeDbCombo *combo, gint n_cols, gint *cols_index);


G_END_DECLS

#endif
