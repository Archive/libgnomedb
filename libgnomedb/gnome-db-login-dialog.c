/* GNOME DB library
 * Copyright (C) 1999 - 2005 The GNOME Foundation.
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <gtk/gtkimage.h>
#include <gtk/gtklabel.h>
#include <gtk/gtknotebook.h>
#include <gtk/gtkstock.h>
#include <gtk/gtktable.h>
#include <gtk/gtkhbox.h>
#include <gtk/gtkwindow.h>
#include <libgda/gda-config.h>
#include <libgnomedb/gnome-db-login.h>
#include <libgnomedb/gnome-db-login-dialog.h>
#include <libgnomedb/gnome-db-stock.h>
#include <libgnomedb/gnome-db-util.h>
#include <libgnomedb/binreloc/gnome-db-binreloc.h>
#include <glib/gi18n-lib.h>

extern void gnome_db_stock_init (void);


struct _GnomeDbLoginDialogPrivate {
	GtkWidget *notice;
	GtkWidget *login;
};

static void gnome_db_login_dialog_class_init   (GnomeDbLoginDialogClass *klass);
static void gnome_db_login_dialog_init         (GnomeDbLoginDialog *dialog,
						GnomeDbLoginDialogClass *klass);
static void gnome_db_login_dialog_set_property (GObject *object,
						guint paramid,
						const GValue *value,
						GParamSpec *pspec);
static void gnome_db_login_dialog_get_property (GObject *object,
						guint param_id,
						GValue *value,
						GParamSpec *pspec);
static void gnome_db_login_dialog_finalize     (GObject *object);

enum {
	PROP_0,
	PROP_DSN,
	PROP_USERNAME,
	PROP_PASSWORD
};

static GObjectClass *parent_class = NULL;

/*
 * GnomeDbLoginDialog class implementation
 */

static void
gnome_db_login_dialog_class_init (GnomeDbLoginDialogClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->set_property = gnome_db_login_dialog_set_property;
	object_class->get_property = gnome_db_login_dialog_get_property;
	object_class->finalize = gnome_db_login_dialog_finalize;

	/* add class properties */
	g_object_class_install_property (object_class, PROP_DSN,
					 g_param_spec_string ("dsn", NULL, NULL, NULL, G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_USERNAME,
					 g_param_spec_string ("username", NULL, NULL, NULL, G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_PASSWORD,
					 g_param_spec_string ("password", NULL, NULL, NULL, G_PARAM_READABLE));

	gnome_db_stock_init ();
}

static void
login_dsn_changed_cb (GnomeDbLogin *login, GnomeDbLoginDialog *dialog)
{
	gchar *str;
	if (gda_config_dsn_needs_authentication (gnome_db_login_get_dsn (login))) {
		str = g_strdup_printf ("<b>%s:</b>\n%s", _("Connection opening"),
				       _("Fill in the following authentication arguments below\n"
					 "to open a connection"));
		gtk_label_set_markup (GTK_LABEL (dialog->priv->notice), str);
		g_free (str);
		gtk_widget_show (dialog->priv->login);
	}
	else {
		str = g_strdup_printf ("<b>%s:</b>\n%s", _("Connection opening"),
				       _("No authentication required,\n"
					 "confirm connection opening"));
		gtk_label_set_markup (GTK_LABEL (dialog->priv->notice), str);
		g_free (str);
		gtk_widget_hide (dialog->priv->login);
	}
}

static void
gnome_db_login_dialog_init (GnomeDbLoginDialog *dialog, GnomeDbLoginDialogClass *klass)
{
	GtkWidget *hbox, *vbox, *image, *label;
	GtkWidget *nb;
	gchar *str;
	GdkPixbuf *icon;

	g_return_if_fail (GNOME_DB_IS_LOGIN_DIALOG (dialog));

	dialog->priv = g_new0 (GnomeDbLoginDialogPrivate, 1);
        
	gtk_dialog_add_button (GTK_DIALOG (dialog), GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL);
	gtk_dialog_add_button (GTK_DIALOG (dialog), GNOME_DB_STOCK_CONNECT, GTK_RESPONSE_OK);
	gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_OK);

	gtk_container_set_border_width (GTK_CONTAINER (dialog), 6);
        gtk_box_set_spacing (GTK_BOX (GTK_DIALOG (dialog)->vbox), 12);
        gtk_dialog_set_has_separator (GTK_DIALOG (dialog), FALSE);
        gtk_window_set_resizable (GTK_WINDOW (dialog), FALSE);

	hbox = gtk_hbox_new (FALSE, 12);
	gtk_container_set_border_width (GTK_CONTAINER (hbox), 6);
	gtk_widget_show (hbox);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), hbox, TRUE, TRUE, 0);

	image = gtk_image_new_from_stock (GTK_STOCK_DIALOG_AUTHENTICATION, GTK_ICON_SIZE_DIALOG);
        gtk_misc_set_alignment (GTK_MISC (image), 0.5, 0.0);
	gtk_widget_show (image);
	gtk_box_pack_start (GTK_BOX (hbox), image, FALSE, FALSE, 5);

	nb = gtk_notebook_new ();
	gtk_notebook_set_show_tabs (GTK_NOTEBOOK (nb), FALSE);
	gtk_notebook_set_show_border (GTK_NOTEBOOK (nb), FALSE);
	gtk_box_pack_start (GTK_BOX (hbox), nb, TRUE, TRUE, 0);
	gtk_widget_show (nb);
	g_object_set_data (G_OBJECT (dialog), "main_part", nb);	

	vbox = gtk_vbox_new (FALSE, 12);
	gtk_widget_show (vbox);
	gtk_notebook_append_page (GTK_NOTEBOOK (nb), vbox, NULL);
	gtk_widget_show (vbox);

	label = gtk_label_new ("");
	dialog->priv->notice = label;

	gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
	gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
	gtk_label_set_selectable (GTK_LABEL (label), FALSE);
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.0);
	gtk_box_pack_start (GTK_BOX (vbox), label, TRUE, TRUE, 0);
	gtk_widget_show (label);

	dialog->priv->login = gnome_db_login_new (NULL);
	gnome_db_login_set_enable_create_button (GNOME_DB_LOGIN (dialog->priv->login), TRUE);
	gtk_widget_show (dialog->priv->login);
	gtk_box_pack_start (GTK_BOX (vbox), dialog->priv->login, TRUE, TRUE, 0);
	g_signal_connect (G_OBJECT (dialog->priv->login), "dsn-changed",
			  G_CALLBACK (login_dsn_changed_cb), dialog);

	str = gnome_db_gbr_get_icon_path ("gnome-db.png");
	icon = gdk_pixbuf_new_from_file (str, NULL);
	g_free (str);
	if (icon) {
		gtk_window_set_icon (GTK_WINDOW (dialog), icon);
		g_object_unref (icon);
	}

}

static void
gnome_db_login_dialog_set_property (GObject *object,
				    guint param_id,
				    const GValue *value,
				    GParamSpec *pspec)
{
	GnomeDbLoginDialog *dialog = (GnomeDbLoginDialog *) object;

	g_return_if_fail (GNOME_DB_IS_LOGIN_DIALOG (dialog));

	switch (param_id) {
	default :
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}

static void
gnome_db_login_dialog_get_property (GObject *object,
				    guint param_id,
				    GValue *value,
				    GParamSpec *pspec)
{
	GnomeDbLoginDialog *dialog = (GnomeDbLoginDialog *) object;

	g_return_if_fail (GNOME_DB_IS_LOGIN_DIALOG (dialog));

	switch (param_id) {
	case PROP_DSN :
		g_value_set_string (
			value,
			gnome_db_login_get_dsn (GNOME_DB_LOGIN (dialog->priv->login)));
		break;
	case PROP_USERNAME :
		g_value_set_string (
			value,
			gnome_db_login_get_username (GNOME_DB_LOGIN (dialog->priv->login)));
		break;
	case PROP_PASSWORD :
		g_value_set_string (
			value,
			gnome_db_login_get_password (GNOME_DB_LOGIN (dialog->priv->login)));
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}

static void
gnome_db_login_dialog_finalize (GObject *object)
{
	GnomeDbLoginDialog *dialog = (GnomeDbLoginDialog *) object;

	g_return_if_fail (GNOME_DB_IS_LOGIN_DIALOG (dialog));

	/* free memory */
	g_free (dialog->priv);
	dialog->priv = NULL;

	parent_class->finalize (object);
}

GType
gnome_db_login_dialog_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0)) {
		static const GTypeInfo info = {
			sizeof (GnomeDbLoginDialogClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_login_dialog_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbLoginDialog),
			0,
			(GInstanceInitFunc) gnome_db_login_dialog_init
		};
		type = g_type_register_static (GTK_TYPE_DIALOG, "GnomeDbLoginDialog", &info, 0);
	}
	return type;
}

/**
 * gnome_db_login_dialog_new
 * @title: title of the dialog, or %NULL
 * @parent: transient parent of the dialog, or %NULL
 *
 * Creates a new login dialog widget.
 *
 * Returns: the new widget
 */
GtkWidget *
gnome_db_login_dialog_new (const gchar *title, GtkWindow *parent)
{
	GnomeDbLoginDialog *dialog;

	dialog = g_object_new (GNOME_DB_TYPE_LOGIN_DIALOG, "transient-for", parent, "title", title, NULL);

	return GTK_WIDGET (dialog);
}

/**
 * gnome_db_login_dialog_run
 * @dialog:
 *
 * Shows the login dialog and waits for the user to enter its username and
 * password and perform an action on the dialog.
 *
 * Returns: TRUE if the user wants to connect
 */
gboolean
gnome_db_login_dialog_run (GnomeDbLoginDialog *dialog)
{
	g_return_val_if_fail (GNOME_DB_IS_LOGIN_DIALOG (dialog), FALSE);

	if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_OK)
		return TRUE;

	return FALSE;
}

/**
 * gnome_db_login_dialog_get_dsn
 * @dialog: a #GnomeDbLoginDialog dialog
 *
 * Get the currently defined data source parameters
 *
 * Returns: a DSN string, which must not be modified
 */
const gchar *
gnome_db_login_dialog_get_dsn (GnomeDbLoginDialog *dialog)
{
	g_return_val_if_fail (GNOME_DB_IS_LOGIN_DIALOG (dialog), NULL);
	return gnome_db_login_get_dsn (GNOME_DB_LOGIN (dialog->priv->login));
}

/**
 * gnome_db_login_dialog_get_auth
 * @dialog: a #GnomeDbLoginDialog dialog
 *
 * Get the currently defined authentication parameters
 *
 * Returns: a string, which must not be modified
 */
const gchar *
gnome_db_login_dialog_get_auth (GnomeDbLoginDialog *dialog)
{
	g_return_val_if_fail (GNOME_DB_IS_LOGIN_DIALOG (dialog), NULL);
	return gnome_db_login_get_auth (GNOME_DB_LOGIN (dialog->priv->login));
}

/**
 * gnome_db_login_dialog_get_username
 * @dialog:
 *
 *
 *
 * Returns:
 */
const gchar *
gnome_db_login_dialog_get_username (GnomeDbLoginDialog *dialog)
{
	g_return_val_if_fail (GNOME_DB_IS_LOGIN_DIALOG (dialog), NULL);
	return gnome_db_login_get_username (GNOME_DB_LOGIN (dialog->priv->login));
}

/**
 * gnome_db_login_dialog_get_password
 * @dialog:
 *
 *
 *
 * Returns:
 */
const gchar *
gnome_db_login_dialog_get_password (GnomeDbLoginDialog *dialog)
{
	g_return_val_if_fail (GNOME_DB_IS_LOGIN_DIALOG (dialog), NULL);
	return gnome_db_login_get_password (GNOME_DB_LOGIN (dialog->priv->login));
}

/**
 * gnome_db_login_dialog_get_login_widget
 * @dialog: A #GnomeDbLoginDialog widget.
 *
 * Get the #GnomeDbLogin widget contained in a #GnomeDbLoginDialog.
 *
 * Returns: the login widget contained in the dialog.
 */
GnomeDbLogin *
gnome_db_login_dialog_get_login_widget (GnomeDbLoginDialog *dialog)
{
	g_return_val_if_fail (GNOME_DB_IS_LOGIN_DIALOG (dialog), NULL);
	return GNOME_DB_LOGIN (dialog->priv->login);
}

