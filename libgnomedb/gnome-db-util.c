/* GNOME DB library
 * Copyright (C) 1999 - 2005 The GNOME Foundation.
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *      Carlos Perell� Mar�n <carlos@gnome-db.org>
 *      Vivien Malerba <malerba@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <string.h>
#include <libintl.h>
#include <glib-object.h>
#include <gtk/gtkbutton.h>
#include <gtk/gtkcheckbutton.h>
#include <gtk/gtkcheckmenuitem.h>
#include <gtk/gtkcombo.h>
#include <gtk/gtkfilesel.h>
#include <gtk/gtkframe.h>
#include <gtk/gtkhbbox.h>
#include <gtk/gtkhbox.h>
#include <gtk/gtkhpaned.h>
#include <gtk/gtkhseparator.h>
#include <gtk/gtkimage.h>
#include <gtk/gtkimagemenuitem.h>
#include <gtk/gtklabel.h>
#include <gtk/gtkmain.h>
#include <gtk/gtkmenu.h>
#include <gtk/gtkmenuitem.h>
#include <gtk/gtkmenushell.h>
#include <gtk/gtkmessagedialog.h>
#include <gtk/gtknotebook.h>
#include <gtk/gtkradiobutton.h>
#include <gtk/gtkscrolledwindow.h>
#include <gtk/gtkseparatormenuitem.h>
#include <gtk/gtkstatusbar.h>
#include <gtk/gtkstock.h>
#include <gtk/gtktable.h>
#include <gtk/gtktextbuffer.h>
#include <gtk/gtktreeview.h>
#include <gtk/gtkvbox.h>
#include <gtk/gtkversion.h>
#if GTK_CHECK_VERSION(2, 4, 0)
#  include <gtk/gtkfilechooserdialog.h>
#endif
#include <gtk/gtkvpaned.h>
#include <gtk/gtkvseparator.h>
#include <gtk/gtkwindow.h>
#include <libgnomedb/gnome-db-grid.h>
#include <libgnomedb/gnome-db-util.h>
#ifdef BUILD_WITH_GNOME
#include <libgnomeui/gnome-file-entry.h>
#endif
#include <glib/gi18n-lib.h>

/**
 * gnome_db_new_button_widget
 * @label:
 *
 *
 *
 * Returns:
 */
GtkWidget *
gnome_db_new_button_widget (const gchar *label)
{
	GtkWidget *button;

	if (label != NULL)
		button = gtk_button_new_with_label (label);
	else
		button = gtk_button_new ();
	gtk_widget_show (button);

	return button;
}

/**
 * gnome_db_new_button_widget_from_stock
 * @stock_id:
 *
 *
 *
 * Returns:
 */
GtkWidget *
gnome_db_new_button_widget_from_stock (const gchar *stock_id)
{
	GtkWidget *button;

	button = gtk_button_new_from_stock (stock_id);
	gtk_widget_show (button);

	return button;
}


/**
 * gnome_db_new_entry_widget
 * @max_length:
 * @editable:
 *
 *
 *
 * Returns:
 */
GtkWidget *
gnome_db_new_entry_widget (gint max_length, gboolean editable)
{
	GtkWidget *entry;

	entry = gtk_entry_new ();
	if (max_length > 0)
		gtk_entry_set_max_length (GTK_ENTRY (entry), max_length);
	gtk_editable_set_editable (GTK_EDITABLE (entry), editable);
	gtk_widget_show (entry);

	return entry;
}

/**
 * gnome_db_new_hbox_widget
 * @homogenous:
 * @spacing:
 *
 *
 *
 * Returns:
 */
GtkWidget *
gnome_db_new_hbox_widget (gboolean homogenous, gint spacing)
{
	GtkWidget *hbox;

	hbox = gtk_hbox_new (homogenous, spacing);
	gtk_widget_show (hbox);

	return hbox;
}

/**
 * gnome_db_new_label_widget
 * @text:
 *
 *
 *
 * Returns:
 */
GtkWidget *
gnome_db_new_label_widget (const gchar *text)
{
	GtkWidget *label;

	label = gtk_label_new_with_mnemonic (text);
	gtk_label_set_selectable (GTK_LABEL (label), TRUE);
	gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.5);
	gtk_widget_show (label);

	return label;
}

/**
 * gnome_db_new_notebook_widget
 *
 *
 *
 * Returns:
 */
GtkWidget *
gnome_db_new_notebook_widget (void)
{
	GtkWidget *notebook;

	notebook = gtk_notebook_new ();
	gtk_notebook_set_show_tabs (GTK_NOTEBOOK (notebook), TRUE);
	gtk_notebook_set_scrollable (GTK_NOTEBOOK (notebook), TRUE);
	gtk_notebook_popup_enable (GTK_NOTEBOOK (notebook));
	gtk_widget_show (notebook);

	return notebook;
}

/**
 * gnome_db_new_scrolled_window_widget
 *
 *
 *
 * Returns:
 */
GtkWidget *
gnome_db_new_scrolled_window_widget (void)
{
	GtkWidget *scroll;

	scroll = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (scroll),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scroll),
					     GTK_SHADOW_IN);
	gtk_widget_show (scroll);

	return scroll;
}

/**
 * gnome_db_new_table_widget
 * @rows:
 * @cols:
 * @homogenous:
 *
 *
 *
 * Returns:
 */
GtkWidget *
gnome_db_new_table_widget (gint rows, gint cols, gboolean homogenous)
{
	GtkWidget *table;

	table = gtk_table_new (rows, cols, homogenous);
	gtk_table_set_row_spacings (GTK_TABLE (table), 6);
        gtk_table_set_col_spacings (GTK_TABLE (table), 6);

	gtk_widget_show (table);

	return table;
}

/**
 * gnome_db_new_text_widget
 * @contents:
 *
 *
 *
 * Returns:
 */
GtkWidget *
gnome_db_new_text_widget (const gchar *contents)
{
	GtkWidget *text;
	GtkTextBuffer *buffer;

	if (contents != NULL) {
		buffer = gtk_text_buffer_new (NULL);
		gtk_text_buffer_set_text (buffer, contents, strlen (contents));

		text = gtk_text_view_new_with_buffer (buffer);
	}
	else
		text = gtk_text_view_new ();

	gtk_widget_show (text);

	return text;
}

/**
 * gnome_db_new_tree_view_widget
 * @model:
 *
 *
 *
 * Returns:
 */
GtkWidget *
gnome_db_new_tree_view_widget (GtkTreeModel *model)
{
	GtkWidget *tree_view;

	tree_view = model ?
		gtk_tree_view_new_with_model (model) :
		gtk_tree_view_new ();
	gtk_tree_view_set_rules_hint (GTK_TREE_VIEW (tree_view), TRUE);
	gtk_widget_show (tree_view);

	return tree_view;
}

/**
 * gnome_db_new_vbox_widget
 * @homogenous:
 * @spacing:
 *
 *
 *
 * Returns:
 */
GtkWidget *
gnome_db_new_vbox_widget (gboolean homogenous, gint spacing)
{
	GtkWidget *vbox;

	vbox = gtk_vbox_new (homogenous, spacing);
	gtk_widget_show (vbox);

	return vbox;
}


/**
 * gnome_db_option_menu_add_item
 * @option_menu:
 * @label:
 *
 *
 *
 * Returns:
 */
GtkWidget *
gnome_db_option_menu_add_item (GtkOptionMenu *option_menu, const gchar *label)
{
	GtkWidget *menu;
	GtkWidget *menu_item;

	g_return_val_if_fail (GTK_IS_OPTION_MENU (option_menu), NULL);
	g_return_val_if_fail (label != NULL, NULL);

	menu = gtk_option_menu_get_menu (option_menu);
	if (!GTK_IS_MENU (menu)) {
		menu = gtk_menu_new ();
		gtk_option_menu_set_menu (option_menu, menu);
		gtk_widget_show (menu);
	}

	menu_item = gtk_menu_item_new_with_label (label);
	gtk_widget_show (menu_item);
	g_object_set_data_full (G_OBJECT (menu_item), "GNOME:Database:MenuItemLabel",
				g_strdup (label), (GDestroyNotify) g_free);

	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_item);

	return menu_item;
}

/**
 * gnome_db_option_menu_add_stock_item
 * @option_menu:
 * @stock_id:
 *
 *
 *
 * Returns:
 */
GtkWidget *
gnome_db_option_menu_add_stock_item (GtkOptionMenu *option_menu, const gchar *stock_id)
{
	GtkWidget *menu;
	GtkWidget *menu_item;

	g_return_val_if_fail (GTK_IS_OPTION_MENU (option_menu), NULL);
	g_return_val_if_fail (stock_id != NULL, NULL);

	menu = gtk_option_menu_get_menu (option_menu);
	if (!GTK_IS_MENU (menu)) {
		menu = gtk_menu_new ();
		gtk_option_menu_set_menu (option_menu, menu);
		gtk_widget_show (menu);
	}

	menu_item = gtk_image_menu_item_new_from_stock (stock_id, NULL);
	gtk_widget_show (menu_item);
	g_object_set_data_full (G_OBJECT (menu_item), "GNOME:Database:MenuItemLabel",
				g_strdup (stock_id), (GDestroyNotify) g_free);

	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_item);

	return menu_item;
}

/**
 * gnome_db_option_menu_add_separator
 * @option_menu:
 *
 */
void
gnome_db_option_menu_add_separator (GtkOptionMenu *option_menu)
{
	GtkWidget *menu;
	GtkWidget *menu_item;

	g_return_if_fail (GTK_IS_OPTION_MENU (option_menu));

	menu = gtk_option_menu_get_menu (option_menu);
	if (!GTK_IS_MENU (menu)) {
		menu = gtk_menu_new ();
		gtk_option_menu_set_menu (option_menu, menu);
		gtk_widget_show (menu);
	}

	menu_item = gtk_separator_menu_item_new ();
	gtk_widget_show (menu_item);

	gtk_menu_shell_append (GTK_MENU_SHELL (menu), menu_item);
}

/**
 * gnome_db_option_menu_get_selection
 * @option_menu:
 *
 *
 *
 * Returns:
 */
const gchar *
gnome_db_option_menu_get_selection (GtkOptionMenu *option_menu)
{
	GtkWidget *menu_item;
	GtkWidget *menu;

	g_return_val_if_fail (GTK_IS_OPTION_MENU (option_menu), NULL);

	menu = gtk_option_menu_get_menu(option_menu);
	menu_item = gtk_menu_get_active(GTK_MENU(menu));

	if (GTK_IS_MENU_ITEM (menu_item)) {
		return (const gchar *) g_object_get_data (G_OBJECT (menu_item),
							  "GNOME:Database:MenuItemLabel");
	}

	return NULL;
}

/**
 * gnome_db_option_menu_set_selection
 * @option_menu:
 * @selection:
 *
 */
void
gnome_db_option_menu_set_selection (GtkOptionMenu *option_menu, const gchar *selection)
{
	GList *l;
	gint i;
	GtkWidget *menu;

	g_return_if_fail (GTK_IS_OPTION_MENU (option_menu));
	g_return_if_fail (selection != NULL);

	menu = gtk_option_menu_get_menu (option_menu);
	if (!GTK_IS_MENU_SHELL (menu))
		return;

	for (l = GTK_MENU_SHELL (menu)->children, i = 0; l != NULL; l = l->next, i++) {
		const gchar *str;
		GtkWidget *menu_item = GTK_WIDGET (l->data);

		str = (const gchar *) g_object_get_data (G_OBJECT (menu_item),
							 "GNOME:Database:MenuItemLabel");
		if (str && !strcmp (str, selection)) {
			gtk_option_menu_set_history (option_menu, i);
			break;
		}
	}
}

/**
 * gnome_db_text_clear
 * @text: a #GtkTextView widget
 *
 * Clear the contents of the given text view widget.
 */
void
gnome_db_text_clear (GtkTextView *text)
{
	GtkTextIter start;
	GtkTextIter end;
	gint char_count;
	GtkTextBuffer *buffer;

	g_return_if_fail (GTK_IS_TEXT_VIEW (text));

	buffer = gtk_text_view_get_buffer (text);
	char_count = gnome_db_text_get_char_count (text);

	gtk_text_buffer_get_iter_at_offset (buffer, &start, 0);
	gtk_text_buffer_get_iter_at_offset (buffer, &end, char_count);

	gtk_text_buffer_delete (buffer, &start, &end);
}

/**
 * gnome_db_text_copy_clipboard
 * @text:
 */
void
gnome_db_text_copy_clipboard (GtkTextView *text)
{
	g_return_if_fail (GTK_IS_TEXT_VIEW (text));
	gtk_text_buffer_copy_clipboard (gtk_text_view_get_buffer (text),
					gtk_clipboard_get (GDK_SELECTION_CLIPBOARD));
}

/**
 * gnome_db_text_cut_clipboard
 * @text:
 *
 */
void
gnome_db_text_cut_clipboard (GtkTextView *text)
{
	g_return_if_fail (GTK_IS_TEXT_VIEW (text));
	gtk_text_buffer_cut_clipboard (gtk_text_view_get_buffer (text),
				       gtk_clipboard_get (GDK_SELECTION_CLIPBOARD),
				       gtk_text_view_get_editable (text));
}

/**
 * gnome_db_text_get_char_count
 * @text:
 *
 *
 *
 * Returns:
 */
gint
gnome_db_text_get_char_count (GtkTextView *text)
{
	g_return_val_if_fail (GTK_IS_TEXT_VIEW (text), -1);
	return gtk_text_buffer_get_char_count (gtk_text_view_get_buffer (text));
}

/**
 * gnome_db_text_get_line_count
 * @text:
 *
 *
 *
 * Returns:
 */
gint
gnome_db_text_get_line_count (GtkTextView *text)
{
	g_return_val_if_fail (GTK_IS_TEXT_VIEW (text), -1);
	return gtk_text_buffer_get_line_count (gtk_text_view_get_buffer (text));
}

/**
 * gnome_db_text_get_text
 * @text:
 *
 *
 *
 * Returns:
 */
gchar *
gnome_db_text_get_text (GtkTextView *text)
{
	GtkTextBuffer *buffer;
	GtkTextIter start;
	GtkTextIter end;
	gint char_count;

	g_return_val_if_fail (GTK_IS_TEXT_VIEW (text), NULL);

	buffer = gtk_text_view_get_buffer (text);
	char_count = gnome_db_text_get_char_count (text);

	gtk_text_buffer_get_iter_at_offset (buffer, &start, 0);
	gtk_text_buffer_get_iter_at_offset (buffer, &end, char_count);

	return gtk_text_buffer_get_text (gtk_text_view_get_buffer (text),
					 &start, &end, FALSE);
}

/**
 * gnome_db_text_insert_at_cursor
 * @text:
 * @contents:
 * @len:
 */
void
gnome_db_text_insert_at_cursor (GtkTextView *text, const gchar *contents, gint len)
{
	g_return_if_fail (GTK_IS_TEXT_VIEW (text));
	g_return_if_fail (contents != NULL);

	gtk_text_buffer_insert_at_cursor (gtk_text_view_get_buffer (text), contents, len);
}

/**
 * gnome_db_text_paste_clipboard
 * @text:
 */
void
gnome_db_text_paste_clipboard (GtkTextView *text)
{
	g_return_if_fail (GTK_IS_TEXT_VIEW (text));
	gtk_text_buffer_paste_clipboard (gtk_text_view_get_buffer (text),
					 gtk_clipboard_get (GDK_SELECTION_CLIPBOARD),
					 NULL,
					 gtk_text_view_get_editable (text));
}

/**
 * gnome_db_text_set_text
 * @text:
 * @contents:
 * @len:
 *
 */
void
gnome_db_text_set_text (GtkTextView *text, const gchar *contents, gint len)
{
	g_return_if_fail (GTK_IS_TEXT_VIEW (text));
	gtk_text_buffer_set_text (gtk_text_view_get_buffer (text), contents, len);
}

/**
 * gnome_db_select_file_dialog
 * @parent:
 * @title:
 *
 *
 *
 * Returns:
 */
gchar *
gnome_db_select_file_dialog (GtkWidget *parent, const gchar *title)
{
	GtkWidget *filesel;
        gchar *res = NULL;

#if GTK_CHECK_VERSION(2,4,0)
	/* create dialog */
	filesel = gtk_file_chooser_dialog_new (title, GTK_WINDOW(parent), GTK_FILE_CHOOSER_ACTION_SAVE,
					       GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					       GTK_STOCK_OK, GTK_RESPONSE_OK,
					       NULL);
#else
        /* create dialog */
        filesel = gtk_file_selection_new (title);
	if (GTK_IS_WINDOW (parent)) {
		gtk_window_set_transient_for (GTK_WINDOW (filesel),
				              GTK_WINDOW (parent));
	}
        gtk_file_selection_show_fileop_buttons (GTK_FILE_SELECTION (filesel));
#endif

	gtk_dialog_set_default_response (GTK_DIALOG (filesel), GTK_RESPONSE_OK);

        /* wait for selection from user */
	if (gtk_dialog_run (GTK_DIALOG (filesel)) == GTK_RESPONSE_OK)
#if GTK_CHECK_VERSION(2, 4, 0)
		res = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (filesel));
#else
		res = g_strdup (gtk_file_selection_get_filename (GTK_FILE_SELECTION (filesel)));
#endif
        else
		res = NULL;

        /* clean up */
        gtk_widget_destroy (filesel);

        return res;
}

/**
 * gnome_db_new_alert
 * @parent:
 * @type:
 * @primary_text:
 * @secondary_text:
 *
 *
 *
 * Returns:
 */
GtkWidget *
gnome_db_new_alert (GtkWindow *parent,
		    GtkMessageType type,
		    const gchar *primary_text,
		    const gchar *secondary_text)
{
	GtkWidget *dialog;
	gchar *str;

	str = g_strconcat ("<span weight=\"bold\">",
			   primary_text,
			   "</span>\n",
			   secondary_text,
			   NULL);

	dialog = gtk_message_dialog_new_with_markup (parent, 
						     GTK_DIALOG_DESTROY_WITH_PARENT |
						     GTK_DIALOG_MODAL, type,
						     GTK_BUTTONS_CLOSE, str);
	
	return dialog;
}

/**
 * gnome_db_show_error
 * @format:
 * @...:
 *
 */
void
gnome_db_show_error (GtkWindow *parent, const gchar *format, ...)
{
	va_list args;
        gchar sz[2048];
	GtkWidget *dialog;

	/* build the message string */
        va_start (args, format);
        vsnprintf (sz, sizeof sz, format, args);
        va_end (args);

	/* create the error message dialog */
	dialog = gnome_db_new_alert (parent, GTK_MESSAGE_ERROR, _("Error:"), sz);
	gtk_dialog_add_action_widget (GTK_DIALOG (dialog),
				      gtk_button_new_from_stock (GTK_STOCK_OK),
				      GTK_RESPONSE_OK);
	gtk_widget_show_all (dialog);
	gtk_dialog_run (GTK_DIALOG (dialog));
	gtk_widget_destroy (dialog);
}

void
gnome_db_push_cursor_busy (GtkWidget *window)
{
        int busy = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (window), "GnomeDb:WindowBusy"));
                                                                                
        busy ++;
                                                                                
        if (busy == 1) {
                gtk_widget_set_sensitive (window, FALSE);
                if (window->window != NULL) {
                        GdkCursor *cursor = gdk_cursor_new (GDK_WATCH);
                        gdk_window_set_cursor (window->window, cursor);
                        gdk_cursor_unref (cursor);
                        gdk_flush ();
                }
        }
                                                                                
        g_object_set_data (G_OBJECT (window), "GnomeDb:WindowBusy", GINT_TO_POINTER (busy));
}
                                                                                
void
gnome_db_pop_cursor_busy (GtkWidget *window)
{
        int busy = GPOINTER_TO_INT (g_object_get_data (G_OBJECT (window), "GnomeDb:WindowBusy"));
        busy --;
                                                                                
        if (busy <= 0) {
                gtk_widget_set_sensitive (window, TRUE);
                if (window->window != NULL)
                        gdk_window_set_cursor (window->window, NULL);
                g_object_set_data (G_OBJECT (window),
                                   "Panel:WindowBusy", NULL);
        } else {
                g_object_set_data (G_OBJECT (window), "Panel:WindowBusy", GINT_TO_POINTER (busy));
        }
}

GtkWidget *
gnome_db_new_check_menu_item (const gchar *label,
			      gboolean active,
			      GCallback cb_func,
			      gpointer user_data)
{
	GtkWidget *item;
	
	item = gtk_check_menu_item_new_with_mnemonic (label);
	gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (item), active);
	
	g_signal_connect (G_OBJECT (item), "toggled",
			  G_CALLBACK (cb_func), user_data);

	return item;
}

GtkWidget *
gnome_db_new_menu_item (const gchar *label,
		 	gboolean pixmap,
		        GCallback cb_func,
			gpointer user_data)
{
	GtkWidget *item;

	if (pixmap)
		item = gtk_image_menu_item_new_from_stock (label, NULL);
	else
		item = gtk_menu_item_new_with_mnemonic (label);
	
	g_signal_connect (G_OBJECT (item), "activate",
			  G_CALLBACK (cb_func), user_data);

	return item;
}
