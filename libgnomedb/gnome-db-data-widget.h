/* gnome-db-data-widget.h
 *
 * Copyright (C) 2004 - 2007 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __GNOME_DB_DATA_WIDGET_H_
#define __GNOME_DB_DATA_WIDGET_H_

#include <glib-object.h>
#include <gtk/gtk.h>
#include <libgda/gda-decl.h>
#include "gnome-db-decl.h"
#include "gnome-db-enums.h"

G_BEGIN_DECLS

#define GNOME_DB_TYPE_DATA_WIDGET          (gnome_db_data_widget_get_type())
#define GNOME_DB_DATA_WIDGET(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, GNOME_DB_TYPE_DATA_WIDGET, GnomeDbDataWidget)
#define GNOME_DB_IS_DATA_WIDGET(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, GNOME_DB_TYPE_DATA_WIDGET)
#define GNOME_DB_DATA_WIDGET_GET_IFACE(obj)  (G_TYPE_INSTANCE_GET_INTERFACE ((obj), GNOME_DB_TYPE_DATA_WIDGET, GnomeDbDataWidgetIface))

typedef enum {
	GNOME_DB_DATA_WIDGET_WRITE_ON_DEMAND           = 0, /* write only when explicitely requested */
	GNOME_DB_DATA_WIDGET_WRITE_ON_ROW_CHANGE       = 1, /* write when the current selected row changes */
	GNOME_DB_DATA_WIDGET_WRITE_ON_VALUE_ACTIVATED  = 2, /* write when user activates a value change */
	GNOME_DB_DATA_WIDGET_WRITE_ON_VALUE_CHANGE     = 3  /* write when a parameters's value changes */
} GnomeDbDataWidgetWriteMode;

/* struct for the interface */
struct _GnomeDbDataWidgetIface
{
	GTypeInterface           g_iface;

	/* virtual table */
	GdaDataProxy        *(* get_proxy)           (GnomeDbDataWidget *iface);
	void                 (* col_set_show)        (GnomeDbDataWidget *iface, gint column, gboolean shown);
	void                 (* set_column_editable) (GnomeDbDataWidget *iface, gint column, gboolean editable);
	void                 (* show_column_actions) (GnomeDbDataWidget *iface, gint column, gboolean show_actions);
	GtkActionGroup      *(* get_actions_group)   (GnomeDbDataWidget *iface);
	GdaDataModelIter    *(* get_data_set)        (GnomeDbDataWidget *iface);

	GdaDataModel        *(* get_gda_model)       (GnomeDbDataWidget *iface);
	void                 (* set_gda_model)       (GnomeDbDataWidget *iface, GdaDataModel *model);
	gboolean             (* set_write_mode)      (GnomeDbDataWidget *iface, GnomeDbDataWidgetWriteMode mode);
	GnomeDbDataWidgetWriteMode (* get_write_mode)(GnomeDbDataWidget *iface);
	void                 (* set_data_layout)     (GnomeDbDataWidget *iface, const gpointer data);

	/* signals */
	void                 (* proxy_changed)       (GnomeDbDataWidget *iface, GdaDataProxy *proxy);
	void                 (* iter_changed)        (GnomeDbDataWidget *iface, GdaDataModelIter *iter);
};

GType             gnome_db_data_widget_get_type                  (void) G_GNUC_CONST;

GdaDataProxy     *gnome_db_data_widget_get_proxy                 (GnomeDbDataWidget *iface);
GtkActionGroup   *gnome_db_data_widget_get_actions_group         (GnomeDbDataWidget *iface);
void              gnome_db_data_widget_perform_action            (GnomeDbDataWidget *iface, GnomeDbAction action);

void              gnome_db_data_widget_column_show               (GnomeDbDataWidget *iface, gint column);
void              gnome_db_data_widget_column_hide               (GnomeDbDataWidget *iface, gint column);
void              gnome_db_data_widget_column_set_editable       (GnomeDbDataWidget *iface, gint column, gboolean editable);
void              gnome_db_data_widget_column_show_actions       (GnomeDbDataWidget *iface, gint column, gboolean show_actions);

GdaDataModel     *gnome_db_data_widget_get_gda_model             (GnomeDbDataWidget *iface);
void              gnome_db_data_widget_set_gda_model             (GnomeDbDataWidget *iface, 
								  GdaDataModel *model);
GdaDataModelIter *gnome_db_data_widget_get_current_data          (GnomeDbDataWidget *iface);

gboolean          gnome_db_data_widget_set_write_mode            (GnomeDbDataWidget *iface, GnomeDbDataWidgetWriteMode mode);
GnomeDbDataWidgetWriteMode gnome_db_data_widget_get_write_mode   (GnomeDbDataWidget *iface);

void              gnome_db_data_widget_set_data_layout           (GnomeDbDataWidget *iface, const gpointer data);

G_END_DECLS

#endif
