/* gnome-db-basic-form.h
 *
 * Copyright (C) 2002 - 2006 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNOME_DB_BASIC_FORM__
#define __GNOME_DB_BASIC_FORM__

#include <gtk/gtk.h>
#include <libgda/libgda.h>
#ifdef HAVE_LIBGLADE
#include <glade/glade.h>
#endif

G_BEGIN_DECLS

#define GNOME_DB_TYPE_BASIC_FORM          (gnome_db_basic_form_get_type())
#define GNOME_DB_BASIC_FORM(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, gnome_db_basic_form_get_type(), GnomeDbBasicForm)
#define GNOME_DB_BASIC_FORM_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, gnome_db_basic_form_get_type (), GnomeDbBasicFormClass)
#define GNOME_DB_IS_BASIC_FORM(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, gnome_db_basic_form_get_type ())

typedef struct {
#ifdef HAVE_LIBGLADE
	GladeXML *xml_object; /* one of xml_object or */
#endif
	gchar    *xml_file;   /* xml_file is required */

	gchar    *root_element; /* required */
	gchar    *form_prefix;  /* required */
} GnomeDbFormLayoutSpec;

typedef struct _GnomeDbBasicForm      GnomeDbBasicForm;
typedef struct _GnomeDbBasicFormClass GnomeDbBasicFormClass;
typedef struct _GnomeDbBasicFormPriv  GnomeDbBasicFormPriv;

/* struct for the object's data */
struct _GnomeDbBasicForm
{
	GtkVBox               object;

	GnomeDbBasicFormPriv *priv;
};

/* struct for the object's class */
struct _GnomeDbBasicFormClass
{
	GtkVBoxClass          parent_class;

	/* signals */
        void       (*param_changed)         (GnomeDbBasicForm *form, GdaHolder *param, gboolean is_user_modif);
	void       (*activated)             (GnomeDbBasicForm *form);
};

/* 
 * Generic widget's methods 
*/
GType             gnome_db_basic_form_get_type                 (void) G_GNUC_CONST;
GtkWidget        *gnome_db_basic_form_new                      (GdaSet *data_set);
GtkWidget        *gnome_db_basic_form_new_custom               (GdaSet *data_set, const gchar *glade_file, 
								const gchar *root_element, const gchar *form_prefix);
GtkWidget        *gnome_db_basic_form_new_in_dialog            (GdaSet *data_set, GtkWindow *parent,
								const gchar *title, const gchar *header);
GdaSet           *gnome_db_basic_form_get_data_set             (GnomeDbBasicForm *form);
gboolean          gnome_db_basic_form_is_valid                 (GnomeDbBasicForm *form);
gboolean          gnome_db_basic_form_has_been_changed         (GnomeDbBasicForm *form);
void              gnome_db_basic_form_reset                    (GnomeDbBasicForm *form);
void              gnome_db_basic_form_set_current_as_orig      (GnomeDbBasicForm *form);

void              gnome_db_basic_form_show_entry_actions       (GnomeDbBasicForm *form, gboolean show_actions);
void              gnome_db_basic_form_entry_show               (GnomeDbBasicForm *form, 
								GdaHolder *param, gboolean show);
void              gnome_db_basic_form_entry_grab_focus         (GnomeDbBasicForm *form, GdaHolder *param);
void              gnome_db_basic_form_entry_set_editable       (GnomeDbBasicForm *form, GdaHolder *param, 
								gboolean editable);
void              gnome_db_basic_form_set_entries_auto_default (GnomeDbBasicForm *form, gboolean auto_default);
void              gnome_db_basic_form_set_entries_default      (GnomeDbBasicForm *form);

GtkWidget        *gnome_db_basic_form_get_entry_widget         (GnomeDbBasicForm *form, GdaHolder *param);
GtkWidget        *gnome_db_basic_form_get_label_widget         (GnomeDbBasicForm *form, GdaHolder *param);



G_END_DECLS

#endif
