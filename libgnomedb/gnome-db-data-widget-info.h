/* gnome-db-data-widget-info.h
 *
 * Copyright (C) 2006 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNOME_DB_DATA_WIDGET_INFO__
#define __GNOME_DB_DATA_WIDGET_INFO__

#include <gtk/gtk.h>
#include <libgda/gda-decl.h>
#include <libgnomedb/gnome-db-decl.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_DATA_WIDGET_INFO          (gnome_db_data_widget_info_get_type())
#define GNOME_DB_DATA_WIDGET_INFO(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, gnome_db_data_widget_info_get_type(), GnomeDbDataWidgetInfo)
#define GNOME_DB_DATA_WIDGET_INFO_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, gnome_db_data_widget_info_get_type (), GnomeDbDataWidgetInfoClass)
#define GNOME_DB_IS_DATA_WIDGET_INFO(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, gnome_db_data_widget_info_get_type ())


typedef struct _GnomeDbDataWidgetInfo      GnomeDbDataWidgetInfo;
typedef struct _GnomeDbDataWidgetInfoClass GnomeDbDataWidgetInfoClass;
typedef struct _GnomeDbDataWidgetInfoPriv  GnomeDbDataWidgetInfoPriv;

typedef enum 
{
	GNOME_DB_DATA_WIDGET_INFO_NONE = 0,
	GNOME_DB_DATA_WIDGET_INFO_CURRENT_ROW    = 1 << 0,
	GNOME_DB_DATA_WIDGET_INFO_ROW_MODIFY_BUTTONS = 1 << 2,
	GNOME_DB_DATA_WIDGET_INFO_ROW_MOVE_BUTTONS = 1 << 3,
	GNOME_DB_DATA_WIDGET_INFO_CHUNCK_CHANGE_BUTTONS = 1 << 4,
	GNOME_DB_DATA_WIDGET_INFO_NO_FILTER = 1 << 5
} GnomeDbDataWidgetInfoFlag;

/* struct for the object's data */
struct _GnomeDbDataWidgetInfo
{
	GtkHBox                    object;

	GnomeDbDataWidgetInfoPriv *priv;
};

/* struct for the object's class */
struct _GnomeDbDataWidgetInfoClass
{
	GtkHBoxClass               parent_class;
};

/* 
 * Generic widget's methods 
 */
GType             gnome_db_data_widget_info_get_type                  (void) G_GNUC_CONST;

GtkWidget        *gnome_db_data_widget_info_new                       (GnomeDbDataWidget *data_widget, GnomeDbDataWidgetInfoFlag flags);

G_END_DECLS

#endif



