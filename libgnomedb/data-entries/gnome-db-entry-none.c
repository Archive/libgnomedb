/* gnome-db-entry-none.c
 *
 * Copyright (C) 2003 - 2006 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "gnome-db-entry-none.h"
#include <libgda/gda-data-handler.h>
#include <glib/gi18n-lib.h>

/* 
 * Main static functions 
 */
static void gnome_db_entry_none_class_init (GnomeDbEntryNoneClass * class);
static void gnome_db_entry_none_init (GnomeDbEntryNone *srv);
static void gnome_db_entry_none_dispose (GObject *object);
static void gnome_db_entry_none_finalize (GObject *object);

/* virtual functions */
static GtkWidget *create_entry (GnomeDbEntryWrapper *mgwrap);
static void       real_set_value (GnomeDbEntryWrapper *mgwrap, const GValue *value);
static GValue  *real_get_value (GnomeDbEntryWrapper *mgwrap);
static void       connect_signals(GnomeDbEntryWrapper *mgwrap, GCallback modify_cb, GCallback activate_cb);
static gboolean   expand_in_layout (GnomeDbEntryWrapper *mgwrap);


/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

/* private structure */
struct _GnomeDbEntryNonePrivate
{
	GValue *stored_value; /* this value is never modified */
};


GType
gnome_db_entry_none_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0)) {
		static const GTypeInfo info = {
			sizeof (GnomeDbEntryNoneClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_entry_none_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbEntryNone),
			0,
			(GInstanceInitFunc) gnome_db_entry_none_init
		};
		
		type = g_type_register_static (GNOME_DB_TYPE_ENTRY_WRAPPER, "GnomeDbEntryNone", &info, 0);
	}
	return type;
}

static void
gnome_db_entry_none_class_init (GnomeDbEntryNoneClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->dispose = gnome_db_entry_none_dispose;
	object_class->finalize = gnome_db_entry_none_finalize;

	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->create_entry = create_entry;
	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->real_set_value = real_set_value;
	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->real_get_value = real_get_value;
	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->connect_signals = connect_signals;
	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->expand_in_layout = expand_in_layout;
}

static void
gnome_db_entry_none_init (GnomeDbEntryNone * entry)
{
	entry->priv = g_new0 (GnomeDbEntryNonePrivate, 1);
	entry->priv->stored_value = NULL;
}

/**
 * gnome_db_entry_none_new
 * @type: the requested data type (compatible with @dh)
 *
 * Creates a new widget which is mainly a GtkEntry
 *
 * Returns: the new widget
 */
GtkWidget *
gnome_db_entry_none_new (GType type)
{
	GObject *obj;
	GnomeDbEntryNone *entry;

	g_return_val_if_fail ((type == G_TYPE_INVALID) || (type != GDA_TYPE_NULL) , NULL);

	obj = g_object_new (GNOME_DB_TYPE_ENTRY_NONE, NULL);
	entry = GNOME_DB_ENTRY_NONE (obj);
	gnome_db_data_entry_set_value_type (GNOME_DB_DATA_ENTRY (entry), type);

	return GTK_WIDGET (obj);
}


static void
gnome_db_entry_none_dispose (GObject   * object)
{
	GnomeDbEntryNone *entry;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOME_DB_IS_ENTRY_NONE (object));

	entry = GNOME_DB_ENTRY_NONE (object);
	if (entry->priv) {
		if (entry->priv->stored_value) {
			gda_value_free (entry->priv->stored_value);
			entry->priv->stored_value = NULL;
		}
	}

	/* parent class */
	parent_class->dispose (object);
}

static void
gnome_db_entry_none_finalize (GObject   * object)
{
	GnomeDbEntryNone *entry;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOME_DB_IS_ENTRY_NONE (object));

	entry = GNOME_DB_ENTRY_NONE (object);
	if (entry->priv) {
		g_free (entry->priv);
		entry->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}

static GtkWidget *
create_entry (GnomeDbEntryWrapper *mgwrap)
{
	GtkWidget *frame, *label;
	GnomeDbEntryNone *entry;

	g_return_val_if_fail (mgwrap && GNOME_DB_IS_ENTRY_NONE (mgwrap), NULL);
	entry = GNOME_DB_ENTRY_NONE (mgwrap);
	g_return_val_if_fail (entry->priv, NULL);

	frame = gtk_frame_new (NULL);
	gtk_container_set_border_width (GTK_CONTAINER (frame), 5);
	
	label = gtk_label_new (_("<non-printable>"));
	gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_CENTER);

	gtk_container_add (GTK_CONTAINER (frame), label);
	gtk_widget_show (label);

	return frame;
}

static void
real_set_value (GnomeDbEntryWrapper *mgwrap, const GValue *value)
{
	GnomeDbEntryNone *entry;

	g_return_if_fail (mgwrap && GNOME_DB_IS_ENTRY_NONE (mgwrap));
	entry = GNOME_DB_ENTRY_NONE (mgwrap);
	g_return_if_fail (entry->priv);

	if (entry->priv->stored_value) {
		gda_value_free (entry->priv->stored_value);
		entry->priv->stored_value = NULL;
	}

	if (value)
		entry->priv->stored_value = gda_value_copy ((GValue *) value);
}

static GValue *
real_get_value (GnomeDbEntryWrapper *mgwrap)
{
	GnomeDbEntryNone *entry;

	g_return_val_if_fail (mgwrap && GNOME_DB_IS_ENTRY_NONE (mgwrap), NULL);
	entry = GNOME_DB_ENTRY_NONE (mgwrap);
	g_return_val_if_fail (entry->priv, NULL);

	if (entry->priv->stored_value)
		return gda_value_copy (entry->priv->stored_value);
	else
		return gda_value_new_null ();
}

static void
connect_signals(GnomeDbEntryWrapper *mgwrap, GCallback modify_cb, GCallback activate_cb)
{
	GnomeDbEntryNone *entry;

	g_return_if_fail (mgwrap && GNOME_DB_IS_ENTRY_NONE (mgwrap));
	entry = GNOME_DB_ENTRY_NONE (mgwrap);
	g_return_if_fail (entry->priv);
}

static gboolean
expand_in_layout (GnomeDbEntryWrapper *mgwrap)
{
	return FALSE;
}
