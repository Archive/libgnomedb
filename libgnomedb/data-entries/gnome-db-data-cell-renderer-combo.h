/* gnome-db-data-cell-renderer-combo.h
 *
 * Copyright (C) 2000 Red Hat, Inc.,  Jonathan Blandford <jrb@redhat.com>
 * Copyright (C) 2003 - 2005 Vivien Malerba <malerba@gnome-db.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GNOME_DB_DATA_CELL_RENDERER_COMBO_H__
#define __GNOME_DB_DATA_CELL_RENDERER_COMBO_H__

#include <gtk/gtk.h>
#include <pango/pango.h>
#include <libgnomedb/gnome-db-decl.h>
#include <libgnomedb/gnome-db-set.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define GNOME_DB_TYPE_DATA_CELL_RENDERER_COMBO		(gnome_db_data_cell_renderer_combo_get_type ())
#define GNOME_DB_DATA_CELL_RENDERER_COMBO(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), GNOME_DB_TYPE_DATA_CELL_RENDERER_COMBO, GnomeDbDataCellRendererCombo))
#define GNOME_DB_DATA_CELL_RENDERER_COMBO_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), GNOME_DB_TYPE_DATA_CELL_RENDERER_COMBO, GnomeDbDataCellRendererComboClass))
#define GNOME_DB_IS_DATA_CELL_RENDERER_COMBO(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GNOME_DB_TYPE_DATA_CELL_RENDERER_COMBO))
#define GNOME_DB_IS_DATA_CELL_RENDERER_COMBO_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), GNOME_DB_TYPE_DATA_CELL_RENDERER_COMBO))
#define GNOME_DB_DATA_CELL_RENDERER_COMBO_GET_CLASS(obj)   (G_TYPE_INSTANCE_GET_CLASS ((obj), GNOME_DB_TYPE_DATA_CELL_RENDERER_COMBO, GnomeDbDataCellRendererComboClass))

typedef struct _GnomeDbDataCellRendererCombo        GnomeDbDataCellRendererCombo;
typedef struct _GnomeDbDataCellRendererComboClass   GnomeDbDataCellRendererComboClass;
typedef struct _GnomeDbDataCellRendererComboPrivate GnomeDbDataCellRendererComboPrivate;

struct _GnomeDbDataCellRendererCombo
{
	GtkCellRendererText            parent;

	GnomeDbDataCellRendererComboPrivate *priv;
};

struct _GnomeDbDataCellRendererComboClass
{
	GtkCellRendererTextClass parent_class;
	
	void (* changed) (GnomeDbDataCellRendererCombo *cell_renderer_combo,
			  const gchar *path,
			  GSList *new_values, GSList *all_new_values);
};

GType            gnome_db_data_cell_renderer_combo_get_type (void) G_GNUC_CONST;
GtkCellRenderer *gnome_db_data_cell_renderer_combo_new      (GnomeDbSet *paramlist, GnomeDbSetSource *source);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GNOME_DB_DATA_CELL_RENDERER_COMBO_H__ */
