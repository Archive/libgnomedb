/* gnome-db-entry-boolean.h
 *
 * Copyright (C) 2003 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __GNOME_DB_ENTRY_BOOLEAN_H_
#define __GNOME_DB_ENTRY_BOOLEAN_H_

#include "gnome-db-entry-wrapper.h"
#include <libgda/gda-data-handler.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_ENTRY_BOOLEAN          (gnome_db_entry_boolean_get_type())
#define GNOME_DB_ENTRY_BOOLEAN(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, gnome_db_entry_boolean_get_type(), GnomeDbEntryBoolean)
#define GNOME_DB_ENTRY_BOOLEAN_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, gnome_db_entry_boolean_get_type (), GnomeDbEntryBooleanClass)
#define GNOME_DB_IS_ENTRY_BOOLEAN(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, gnome_db_entry_boolean_get_type ())


typedef struct _GnomeDbEntryBoolean GnomeDbEntryBoolean;
typedef struct _GnomeDbEntryBooleanClass GnomeDbEntryBooleanClass;
typedef struct _GnomeDbEntryBooleanPrivate GnomeDbEntryBooleanPrivate;


/* struct for the object's data */
struct _GnomeDbEntryBoolean
{
	GnomeDbEntryWrapper              object;
	GnomeDbEntryBooleanPrivate       *priv;
};

/* struct for the object's class */
struct _GnomeDbEntryBooleanClass
{
	GnomeDbEntryWrapperClass         parent_class;
};

GType        gnome_db_entry_boolean_get_type        (void) G_GNUC_CONST;
GtkWidget   *gnome_db_entry_boolean_new             (GdaDataHandler *dh, GType type);


G_END_DECLS

#endif
