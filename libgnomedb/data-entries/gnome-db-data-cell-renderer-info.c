/* gnome-db-data-cell-renderer-info.c
 *
 * Copyright (C) 2000 Red Hat, Inc.,  Jonathan Blandford <jrb@redhat.com>
 * Copyright (C) 2003 - 2008 Vivien Malerba <malerba@gnome-db.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <stdlib.h>
#include <glib/gi18n-lib.h>
#include "gnome-db-data-cell-renderer-info.h"
#include "marshal.h"
#include <libgda/libgda.h>
#include <gdk/gdkkeysyms.h>
#include "utility.h"
#include <libgda/gda-enum-types.h>

static void gnome_db_data_cell_renderer_info_get_property  (GObject *object,
							    guint param_id,
							    GValue *value,
							    GParamSpec *pspec);
static void gnome_db_data_cell_renderer_info_set_property  (GObject *object,
							    guint param_id,
							    const GValue *value,
							    GParamSpec *pspec);
static void gnome_db_data_cell_renderer_info_init       (GnomeDbDataCellRendererInfo      *celltext);
static void gnome_db_data_cell_renderer_info_class_init (GnomeDbDataCellRendererInfoClass *class);
static void gnome_db_data_cell_renderer_info_dispose    (GObject *object);
static void gnome_db_data_cell_renderer_info_finalize   (GObject *object);

static void gnome_db_data_cell_renderer_info_get_size   (GtkCellRenderer            *cell,
							 GtkWidget                  *widget,
							 GdkRectangle               *cell_area,
							 gint                       *x_offset,
							 gint                       *y_offset,
							 gint                       *width,
							 gint                       *height);
static void gnome_db_data_cell_renderer_info_render     (GtkCellRenderer            *cell,
							 GdkWindow                  *window,
							 GtkWidget                  *widget,
							 GdkRectangle               *background_area,
							 GdkRectangle               *cell_area,
							 GdkRectangle               *expose_area,
							 GtkCellRendererState        flags);
static gboolean gnome_db_data_cell_renderer_info_activate  (GtkCellRenderer            *cell,
							    GdkEvent                   *event,
							    GtkWidget                  *widget,
							    const gchar                *path,
							    GdkRectangle               *background_area,
							    GdkRectangle               *cell_area,
							    GtkCellRendererState        flags);


enum {
	STATUS_CHANGED,
	LAST_SIGNAL
};

enum {
	PROP_ZERO,
	PROP_VALUE_ATTRIBUTES,
	PROP_EDITABLE,
	PROP_TO_BE_DELETED,
	PROP_STORE,
	PROP_ITER,
	PROP_GROUP
};

struct _GnomeDbDataCellRendererInfoPriv {
	/* attributes valid for the while life of the object */
	GnomeDbDataStore      *store;
	GdaDataModelIter      *iter;
	GnomeDbSetGroup       *group;

	/* attribute valid only for drawing */
	gboolean               active;
	guint                  attributes;
};

#define INFO_WIDTH 6
#define INFO_HEIGHT 14
static GObjectClass *parent_class = NULL;
static guint info_cell_signals[LAST_SIGNAL] = { 0 };


GType
gnome_db_data_cell_renderer_info_get_type (void)
{
	static GType cell_info_type = 0;

	if (!cell_info_type) {
		static const GTypeInfo cell_info_info = {
			sizeof (GnomeDbDataCellRendererInfoClass),
			NULL,		/* base_init */
			NULL,		/* base_finalize */
			(GClassInitFunc) gnome_db_data_cell_renderer_info_class_init,
			NULL,		/* class_finalize */
			NULL,		/* class_data */
			sizeof (GnomeDbDataCellRendererInfo),
			0,              /* n_preallocs */
			(GInstanceInitFunc) gnome_db_data_cell_renderer_info_init,
		};
		
		cell_info_type =
			g_type_register_static (GTK_TYPE_CELL_RENDERER, "GnomeDbDataCellRendererInfo",
						&cell_info_info, 0);
	}

	return cell_info_type;
}

static void
gnome_db_data_cell_renderer_info_init (GnomeDbDataCellRendererInfo *cellinfo)
{
	cellinfo->priv = g_new0 (GnomeDbDataCellRendererInfoPriv, 1);

	GTK_CELL_RENDERER (cellinfo)->mode = GTK_CELL_RENDERER_MODE_ACTIVATABLE;
	GTK_CELL_RENDERER (cellinfo)->xpad = 1;
	GTK_CELL_RENDERER (cellinfo)->ypad = 1;
}

static void
gnome_db_data_cell_renderer_info_class_init (GnomeDbDataCellRendererInfoClass *class)
{
	GObjectClass *object_class = G_OBJECT_CLASS (class);
	GtkCellRendererClass *cell_class = GTK_CELL_RENDERER_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

    	object_class->dispose = gnome_db_data_cell_renderer_info_dispose;
	object_class->finalize = gnome_db_data_cell_renderer_info_finalize;

	object_class->get_property = gnome_db_data_cell_renderer_info_get_property;
	object_class->set_property = gnome_db_data_cell_renderer_info_set_property;

	cell_class->get_size = gnome_db_data_cell_renderer_info_get_size;
	cell_class->render = gnome_db_data_cell_renderer_info_render;
	cell_class->activate = gnome_db_data_cell_renderer_info_activate;
  
	g_object_class_install_property (object_class,
					 PROP_VALUE_ATTRIBUTES,
					 g_param_spec_flags ("value_attributes", NULL, NULL, GDA_TYPE_VALUE_ATTRIBUTE,
                                                            GDA_VALUE_ATTR_NONE, G_PARAM_READWRITE));

	g_object_class_install_property (object_class,
					 PROP_EDITABLE,
					 g_param_spec_boolean ("editable",
							       _("Editable"),
							       _("The information and status changer can be activated"),
							       TRUE,G_PARAM_READWRITE));
	g_object_class_install_property (object_class,
					 PROP_TO_BE_DELETED,
					 g_param_spec_boolean ("to_be_deleted", NULL, NULL, FALSE,
                                                               G_PARAM_WRITABLE));

	g_object_class_install_property (object_class,
					 PROP_STORE,
					 g_param_spec_object ("store", NULL, NULL, GNOME_DB_TYPE_DATA_STORE,
                                                               G_PARAM_WRITABLE|G_PARAM_CONSTRUCT_ONLY));
	g_object_class_install_property (object_class,
					 PROP_ITER,
					 g_param_spec_object ("iter", NULL, NULL, GDA_TYPE_DATA_MODEL_ITER,
                                                               G_PARAM_WRITABLE|G_PARAM_CONSTRUCT_ONLY));

	/* Ideally, GdaPropertyListGroup would be a boxed type, but it is not yet, so we use g_param_spec_pointer. */
	g_object_class_install_property (object_class,
					 PROP_GROUP,
					 g_param_spec_pointer ("group", NULL, NULL,
                                                               G_PARAM_WRITABLE|G_PARAM_CONSTRUCT_ONLY));
	info_cell_signals[STATUS_CHANGED] =
		g_signal_new ("status_changed",
			      G_OBJECT_CLASS_TYPE (object_class),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (GnomeDbDataCellRendererInfoClass, status_changed),
			      NULL, NULL,
			      gnome_db_marshal_VOID__STRING_ENUM,
			      G_TYPE_NONE, 2,
			      G_TYPE_STRING,
			      GDA_TYPE_VALUE_ATTRIBUTE);
}

static void
gnome_db_data_cell_renderer_info_dispose (GObject *object)
{
	GnomeDbDataCellRendererInfo *cellinfo = GNOME_DB_DATA_CELL_RENDERER_INFO (object);

	if (cellinfo->priv->store) {
		g_object_unref (cellinfo->priv->store);
		cellinfo->priv->store = NULL;
	}

	if (cellinfo->priv->iter) {
		g_object_unref (cellinfo->priv->iter);
		cellinfo->priv->iter = NULL;
	}

	/* parent class */
	parent_class->dispose (object);
}

static void
gnome_db_data_cell_renderer_info_finalize (GObject *object)
{
	GnomeDbDataCellRendererInfo *cellinfo = GNOME_DB_DATA_CELL_RENDERER_INFO (object);

	if (cellinfo->priv) {
		g_free (cellinfo->priv);
		cellinfo->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}


static void
gnome_db_data_cell_renderer_info_get_property (GObject *object,
					       guint        param_id,
					       GValue *value,
					       GParamSpec *pspec)
{
	GnomeDbDataCellRendererInfo *cellinfo = GNOME_DB_DATA_CELL_RENDERER_INFO (object);
  
	switch (param_id) {
	case PROP_VALUE_ATTRIBUTES:
		g_value_set_flags (value, cellinfo->priv->attributes);
		break;
	case PROP_EDITABLE:
		g_value_set_boolean (value, cellinfo->priv->active);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}


static void
gnome_db_data_cell_renderer_info_set_property (GObject *object,
					       guint         param_id,
					       const GValue *value,
					       GParamSpec *pspec)
{
	GnomeDbDataCellRendererInfo *cellinfo = GNOME_DB_DATA_CELL_RENDERER_INFO (object);
  
	switch (param_id) {
	case PROP_VALUE_ATTRIBUTES:
		cellinfo->priv->attributes = g_value_get_flags (value);
		g_object_set (object, "sensitive", 
			      !(cellinfo->priv->attributes & GDA_VALUE_ATTR_NO_MODIF), NULL);
		break;
	case PROP_EDITABLE:
		cellinfo->priv->active = g_value_get_boolean (value);
		g_object_notify (G_OBJECT(object), "editable");
		break;
	case PROP_TO_BE_DELETED:
		break;
	case PROP_STORE:
		if (cellinfo->priv->store)
			g_object_unref (cellinfo->priv->store);

		cellinfo->priv->store = GNOME_DB_DATA_STORE(g_value_get_object(value));
		if(cellinfo->priv->store)
			g_object_ref(cellinfo->priv->store);
    		break;
	case PROP_ITER:
		if (cellinfo->priv->iter)
			g_object_unref(cellinfo->priv->iter);

		cellinfo->priv->iter = GDA_DATA_MODEL_ITER (g_value_get_object(value));
		if( cellinfo->priv->iter)
			g_object_ref (cellinfo->priv->iter);

		g_object_ref (G_OBJECT (cellinfo->priv->iter));
    		break;
	case PROP_GROUP:
		cellinfo->priv->group = GNOME_DB_SET_GROUP (g_value_get_pointer(value));
   		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}

/**
 * gnome_db_data_cell_renderer_info_new:
 * @store:
 * @iter:
 * @node:
 * 
 * Creates a new #GnomeDbDataCellRendererInfo. Adjust rendering
 * parameters using object properties. Object properties can be set
 * globally (with g_object_set()). Also, with #GtkTreeViewColumn, you
 * can bind a property to a value in a #GtkTreeModel. For example, you
 * can bind the "active" property on the cell renderer to a boolean value
 * in the model, thus causing the check button to reflect the state of
 * the model.
 * 
 * Returns: the new cell renderer
 **/
GtkCellRenderer *
gnome_db_data_cell_renderer_info_new (GnomeDbDataStore *store, 
				      GdaDataModelIter *iter, GnomeDbSetGroup *group)
{
	GObject *obj;

	g_return_val_if_fail (GNOME_DB_IS_DATA_STORE (store), NULL);
	g_return_val_if_fail (GDA_IS_SET (iter), NULL);
	g_return_val_if_fail (group, NULL);

	obj = g_object_new (GNOME_DB_TYPE_DATA_CELL_RENDERER_INFO, 
			    "store", store, "iter", iter, "group", group, NULL);

	return (GtkCellRenderer *) obj;
}

static void
gnome_db_data_cell_renderer_info_get_size (GtkCellRenderer *cell,
					   GtkWidget       *widget,
					   GdkRectangle    *cell_area,
					   gint            *x_offset,
					   gint            *y_offset,
					   gint            *width,
					   gint            *height)
{
	gint calc_width;
	gint calc_height;

	calc_width = (gint) cell->xpad * 2 + INFO_WIDTH;
	calc_height = (gint) cell->ypad * 2 + INFO_HEIGHT;

	if (width)
		*width = calc_width;

	if (height)
		*height = calc_height;

	if (cell_area) {
		if (x_offset) {
			*x_offset = cell->xalign * (cell_area->width - calc_width);
			*x_offset = MAX (*x_offset, 0);
		}
		if (y_offset) {
			*y_offset = cell->yalign * (cell_area->height - calc_height);
			*y_offset = MAX (*y_offset, 0);
		}
	}
}


static void
gnome_db_data_cell_renderer_info_render (GtkCellRenderer      *cell,
					 GdkWindow            *window,
					 GtkWidget            *widget,
					 GdkRectangle         *background_area,
					 GdkRectangle         *cell_area,
					 GdkRectangle         *expose_area,
					 GtkCellRendererState  flags)
{
	GnomeDbDataCellRendererInfo *cellinfo = (GnomeDbDataCellRendererInfo *) cell;
	gint width, height;
	gint x_offset, y_offset;
	GtkStateType state = 0;

	static GdkColor **colors = NULL;
	GdkColor *normal = NULL, *prelight = NULL;
	GdkColor *orig_normal, *orig_prelight;
	GtkStyle *style;

	if (!colors)
		colors = gnome_db_utility_entry_build_info_colors_array ();

	style = gtk_style_copy (widget->style);
	orig_normal = & (style->bg[GTK_STATE_NORMAL]);
	orig_prelight = & (style->bg[GTK_STATE_PRELIGHT]);
	if (cellinfo->priv->attributes & GDA_VALUE_ATTR_IS_NULL) {
		normal = colors[0];
		prelight = colors[1];
	}

	if (cellinfo->priv->attributes & GDA_VALUE_ATTR_IS_DEFAULT) {
		normal = colors[2];
		prelight = colors[3];
	}

	if (cellinfo->priv->attributes & GDA_VALUE_ATTR_DATA_NON_VALID) {
		normal = colors[4];
		prelight = colors[5];
	}

	if (!normal)
		normal = orig_normal;
	if (!prelight)
		prelight = orig_prelight;

	style->bg[GTK_STATE_NORMAL] = *normal;
	style->bg[GTK_STATE_ACTIVE] = *normal;
	style->bg[GTK_STATE_PRELIGHT] = *prelight;
	style = gtk_style_attach (style, window); /* Note that we must use the return value, because this function is documented as sometimes returning a new object. */
	gnome_db_data_cell_renderer_info_get_size (cell, widget, cell_area,
						   &x_offset, &y_offset,
						   &width, &height);
	width -= cell->xpad*2;
	height -= cell->ypad*2;

	if (width <= 0 || height <= 0)
		return;

	state = GTK_STATE_NORMAL;

	gtk_paint_box (style,
		       window,
		       state, GTK_SHADOW_NONE,
		       cell_area, widget, "cellcheck",
		       cell_area->x + x_offset + cell->xpad,
		       cell_area->y + y_offset + cell->ypad,
		       width - 1, height - 1);
	gtk_style_detach (style);
	g_object_unref (G_OBJECT (style));
}


static void mitem_activated_cb (GtkWidget *mitem, GnomeDbDataCellRendererInfo *cellinfo);
static gint
gnome_db_data_cell_renderer_info_activate (GtkCellRenderer      *cell,
					   GdkEvent             *event,
					   GtkWidget            *widget,
					   const gchar          *path,
					   GdkRectangle         *background_area,
					   GdkRectangle         *cell_area,
					   GtkCellRendererState  flags)
{
	GnomeDbDataCellRendererInfo *cellinfo;
	gchar *tmp;

	cellinfo = GNOME_DB_DATA_CELL_RENDERER_INFO (cell);

	/* free any pre-allocated path */
	if ((tmp = g_object_get_data (G_OBJECT (cellinfo), "path"))) {
		g_free (tmp);
		g_object_set_data (G_OBJECT (cellinfo), "path", NULL);
	}

	if (cellinfo->priv->active) {
		GtkWidget *menu;
		guint attributes = 0;
		GtkTreeIter iter;
		GtkTreePath *treepath;

		treepath = gtk_tree_path_new_from_string (path);
		if (! gtk_tree_model_get_iter (GTK_TREE_MODEL (cellinfo->priv->store), &iter, treepath)) {
			g_warning ("Can't set iter on model from path %s", path);
			gtk_tree_path_free (treepath);
			return FALSE;
		}
		gtk_tree_path_free (treepath);

		/* we want the attributes */
		if (! cellinfo->priv->group->group->nodes_source) {
			gint col;
			GdaDataModel *proxied_model;
			GdaDataProxy *proxy;

			proxy = gnome_db_data_store_get_proxy (cellinfo->priv->store);
			proxied_model = gda_data_proxy_get_proxied_model (proxy);

			g_assert (g_slist_length (cellinfo->priv->group->group->nodes) == 1);
			col = g_slist_index (GDA_SET (cellinfo->priv->iter)->holders,
					     GDA_SET_NODE (cellinfo->priv->group->group->nodes->data)->holder);

			gtk_tree_model_get (GTK_TREE_MODEL (cellinfo->priv->store), &iter, 
					    gda_data_model_get_n_columns (proxied_model) + col, 
					    &attributes, -1);
		}
		else 
			attributes = gnome_db_utility_proxy_compute_attributes_for_group (cellinfo->priv->group, 
											  cellinfo->priv->store,
											  cellinfo->priv->iter,
											  &iter, NULL);
		
		/* build the popup menu */
		menu = gnome_db_utility_entry_build_actions_menu (G_OBJECT (cellinfo), attributes, 
							 G_CALLBACK (mitem_activated_cb));
		g_object_set_data (G_OBJECT (cellinfo), "path", g_strdup (path));
		gtk_menu_popup (GTK_MENU (menu), NULL, NULL, NULL, NULL,
				0, gtk_get_current_event_time ());
		return TRUE;		
	}

	return FALSE;
}

static void
mitem_activated_cb (GtkWidget *mitem, GnomeDbDataCellRendererInfo *cellinfo)
{
	guint action;
	gchar *path;

	action = GPOINTER_TO_UINT (g_object_get_data (G_OBJECT (mitem), "action"));
	path = g_object_get_data (G_OBJECT (cellinfo), "path");
#ifdef debug_signal
        g_print (">> 'STATUS_CHANGED' from %s\n", __FUNCTION__);
#endif
	g_signal_emit (cellinfo, info_cell_signals[STATUS_CHANGED], 0, path, action); 
#ifdef debug_signal
        g_print ("<< 'STATUS_CHANGED' from %s\n", __FUNCTION__);
#endif
	g_free (path);
	g_object_set_data (G_OBJECT (cellinfo), "path", NULL);
}
