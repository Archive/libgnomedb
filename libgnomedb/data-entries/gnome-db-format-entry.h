/* gnome-db-format-entry.h
 *
 * Copyright (C) 2007 Vivien Malerba <malerba@gnome-db.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GNOME_DB_FORMAT_ENTRY_H__
#define __GNOME_DB_FORMAT_ENTRY_H__

#include <gdk/gdk.h>
#include <gtk/gtkentry.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_FORMAT_ENTRY                 (gnome_db_format_entry_get_type ())
#define GNOME_DB_FORMAT_ENTRY(obj)                 (G_TYPE_CHECK_INSTANCE_CAST ((obj), GNOME_DB_TYPE_FORMAT_ENTRY, GnomeDbFormatEntry))
#define GNOME_DB_FORMAT_ENTRY_CLASS(klass)         (G_TYPE_CHECK_CLASS_CAST ((klass), GNOME_DB_TYPE_FORMAT_ENTRY, GnomeDbFormatEntry))
#define GNOME_DB_IS_FORMAT_ENTRY(obj)              (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GNOME_DB_TYPE_FORMAT_ENTRY))
#define GNOME_DB_IS_FORMAT_ENTRY_CLASS(klass)      (G_TYPE_CHECK_CLASS_TYPE ((klass), GNOME_DB_TYPE_FORMAT_ENTRY))
#define GNOME_DB_FORMAT_ENTRY_GET_CLASS(obj)       (G_TYPE_INSTANCE_GET_CLASS ((obj), GNOME_DB_TYPE_FORMAT_ENTRY, GnomeDbFormatEntry))


typedef struct _GnomeDbFormatEntry        GnomeDbFormatEntry;
typedef struct _GnomeDbFormatEntryClass   GnomeDbFormatEntryClass;
typedef struct _GnomeDbFormatEntryPrivate GnomeDbFormatEntryPrivate;

struct _GnomeDbFormatEntry
{
	GtkEntry                   entry;
	GnomeDbFormatEntryPrivate *priv;
};

struct _GnomeDbFormatEntryClass
{
	GtkEntryClass              parent_class;
};

GType                 gnome_db_format_entry_get_type           (void) G_GNUC_CONST;
GtkWidget            *gnome_db_format_entry_new                (void);

void                  gnome_db_format_entry_set_max_length     (GnomeDbFormatEntry *entry, gint max);
void                  gnome_db_format_entry_set_format         (GnomeDbFormatEntry *entry, 
								const gchar *format, const gchar *mask, const gchar *completion);
void                  gnome_db_format_entry_set_decimal_places (GnomeDbFormatEntry *entry, gint nb_decimals);
void                  gnome_db_format_entry_set_separators     (GnomeDbFormatEntry *entry, guchar decimal, guchar thousands);
void                  gnome_db_format_entry_set_edited_type    (GnomeDbFormatEntry *entry, GType type);
void                  gnome_db_format_entry_set_prefix         (GnomeDbFormatEntry *entry, const gchar *prefix);
void                  gnome_db_format_entry_set_suffix         (GnomeDbFormatEntry *entry, const gchar *suffix);

void                  gnome_db_format_entry_set_text           (GnomeDbFormatEntry *entry, const gchar *text);
gchar                *gnome_db_format_entry_get_text           (GnomeDbFormatEntry *entry);


G_END_DECLS

#endif
