/* gnome-db-entry-boolean.c
 *
 * Copyright (C) 2003 - 2006 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "gnome-db-entry-boolean.h"

/* 
 * Main static functions 
 */
static void gnome_db_entry_boolean_class_init (GnomeDbEntryBooleanClass * class);
static void gnome_db_entry_boolean_init (GnomeDbEntryBoolean * srv);
static void gnome_db_entry_boolean_dispose (GObject   * object);
static void gnome_db_entry_boolean_finalize (GObject   * object);

/* virtual functions */
static GtkWidget *create_entry (GnomeDbEntryWrapper *mgwrap);
static void       real_set_value (GnomeDbEntryWrapper *mgwrap, const GValue *value);
static GValue    *real_get_value (GnomeDbEntryWrapper *mgwrap);
static void       connect_signals(GnomeDbEntryWrapper *mgwrap, GCallback modify_cb, GCallback activate_cb);
static gboolean   expand_in_layout (GnomeDbEntryWrapper *mgwrap);
static void       set_editable (GnomeDbEntryWrapper *mgwrap, gboolean editable);
static void       grab_focus (GnomeDbEntryWrapper *mgwrap);

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

/* private structure */
struct _GnomeDbEntryBooleanPrivate
{
	GtkWidget *hbox;
	GtkWidget *check;
};


GType
gnome_db_entry_boolean_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0)) {
		static const GTypeInfo info = {
			sizeof (GnomeDbEntryBooleanClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_entry_boolean_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbEntryBoolean),
			0,
			(GInstanceInitFunc) gnome_db_entry_boolean_init
		};
		
		type = g_type_register_static (GNOME_DB_TYPE_ENTRY_WRAPPER, "GnomeDbEntryBoolean", &info, 0);
	}
	return type;
}

static void
gnome_db_entry_boolean_class_init (GnomeDbEntryBooleanClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->dispose = gnome_db_entry_boolean_dispose;
	object_class->finalize = gnome_db_entry_boolean_finalize;

	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->create_entry = create_entry;
	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->real_set_value = real_set_value;
	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->real_get_value = real_get_value;
	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->connect_signals = connect_signals;
	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->expand_in_layout = expand_in_layout;
	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->set_editable = set_editable;
	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->grab_focus = grab_focus;
}

static void
gnome_db_entry_boolean_init (GnomeDbEntryBoolean * gnome_db_entry_boolean)
{
	gnome_db_entry_boolean->priv = g_new0 (GnomeDbEntryBooleanPrivate, 1);
	gnome_db_entry_boolean->priv->hbox = NULL;
	gnome_db_entry_boolean->priv->check = NULL;
}

/**
 * gnome_db_entry_boolean_new
 * @dh: the data handler to be used by the new widget
 * @type: the requested data type (compatible with @dh)
 *
 * Creates a new widget which is mainly a GtkEntry
 *
 * Returns: the new widget
 */
GtkWidget *
gnome_db_entry_boolean_new (GdaDataHandler *dh, GType type)
{
	GObject *obj;
	GnomeDbEntryBoolean *mgbool;

	g_return_val_if_fail (GDA_IS_DATA_HANDLER (dh), NULL);
	g_return_val_if_fail (type != G_TYPE_INVALID, NULL);
	g_return_val_if_fail (gda_data_handler_accepts_g_type (dh, type), NULL);

	obj = g_object_new (GNOME_DB_TYPE_ENTRY_BOOLEAN, "handler", dh, NULL);
	mgbool = GNOME_DB_ENTRY_BOOLEAN (obj);
	gnome_db_data_entry_set_value_type (GNOME_DB_DATA_ENTRY (mgbool), type);

	return GTK_WIDGET (obj);
}


static void
gnome_db_entry_boolean_dispose (GObject   * object)
{
	GnomeDbEntryBoolean *gnome_db_entry_boolean;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOME_DB_IS_ENTRY_BOOLEAN (object));

	gnome_db_entry_boolean = GNOME_DB_ENTRY_BOOLEAN (object);
	if (gnome_db_entry_boolean->priv) {

	}

	/* parent class */
	parent_class->dispose (object);
}

static void
gnome_db_entry_boolean_finalize (GObject   * object)
{
	GnomeDbEntryBoolean *gnome_db_entry_boolean;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOME_DB_IS_ENTRY_BOOLEAN (object));

	gnome_db_entry_boolean = GNOME_DB_ENTRY_BOOLEAN (object);
	if (gnome_db_entry_boolean->priv) {

		g_free (gnome_db_entry_boolean->priv);
		gnome_db_entry_boolean->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}

static GtkWidget *
create_entry (GnomeDbEntryWrapper *mgwrap)
{
	GtkWidget *hbox, *cb;
	GnomeDbEntryBoolean *mgbool;

	g_return_val_if_fail (GNOME_DB_IS_ENTRY_BOOLEAN (mgwrap), NULL);
	mgbool = GNOME_DB_ENTRY_BOOLEAN (mgwrap);
	g_return_val_if_fail (mgbool->priv, NULL);

	hbox = gtk_hbox_new (FALSE, 5);
	mgbool->priv->hbox = hbox;

	cb = gtk_check_button_new ();
	mgbool->priv->check = cb;
	gtk_box_pack_start (GTK_BOX (hbox), cb, FALSE, FALSE, 0);
	gtk_widget_show (cb);

	return hbox;
}

static void
real_set_value (GnomeDbEntryWrapper *mgwrap, const GValue *value)
{
	GnomeDbEntryBoolean *mgbool;

	g_return_if_fail (GNOME_DB_IS_ENTRY_BOOLEAN (mgwrap));
	mgbool = GNOME_DB_ENTRY_BOOLEAN (mgwrap);
	g_return_if_fail (mgbool->priv);

	if (value) {
		if (gda_value_is_null ((GValue *) value)) {
			gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (mgbool->priv->check), FALSE);
			gtk_toggle_button_set_inconsistent (GTK_TOGGLE_BUTTON (mgbool->priv->check), TRUE);
		}
		else {
			gtk_toggle_button_set_inconsistent (GTK_TOGGLE_BUTTON (mgbool->priv->check), FALSE);
			if (g_value_get_boolean ((GValue *) value)) 
				gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (mgbool->priv->check), TRUE);
			else 
				gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (mgbool->priv->check), FALSE);
		}
	}
	else {
		gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (mgbool->priv->check), FALSE);
		gtk_toggle_button_set_inconsistent (GTK_TOGGLE_BUTTON (mgbool->priv->check), TRUE);
	}
}

static GValue *
real_get_value (GnomeDbEntryWrapper *mgwrap)
{
	GValue *value;
	GnomeDbEntryBoolean *mgbool;
	GdaDataHandler *dh;
	const gchar *str;

	g_return_val_if_fail (GNOME_DB_IS_ENTRY_BOOLEAN (mgwrap), NULL);
	mgbool = GNOME_DB_ENTRY_BOOLEAN (mgwrap);
	g_return_val_if_fail (mgbool->priv, NULL);

	dh = gnome_db_data_entry_get_handler (GNOME_DB_DATA_ENTRY (mgwrap));
	if (gtk_toggle_button_get_active (GTK_TOGGLE_BUTTON (mgbool->priv->check)))
		str = "TRUE";
	else
		str = "FALSE";
	value = gda_data_handler_get_value_from_sql (dh, str, gnome_db_data_entry_get_value_type (GNOME_DB_DATA_ENTRY (mgwrap)));

	return value;
}

static void check_toggled_cb (GtkToggleButton *toggle, GnomeDbEntryBoolean *mgbool);
static void
connect_signals(GnomeDbEntryWrapper *mgwrap, GCallback modify_cb, GCallback activate_cb)
{
	GnomeDbEntryBoolean *mgbool;

	g_return_if_fail (GNOME_DB_IS_ENTRY_BOOLEAN (mgwrap));
	mgbool = GNOME_DB_ENTRY_BOOLEAN (mgwrap);
	g_return_if_fail (mgbool->priv);

	g_signal_connect (G_OBJECT (mgbool->priv->check), "toggled",
			  modify_cb, mgwrap);
	g_signal_connect (G_OBJECT (mgbool->priv->check), "toggled",
			  activate_cb, mgwrap);
	g_signal_connect (G_OBJECT (mgbool->priv->check), "toggled",
                          G_CALLBACK (check_toggled_cb), mgwrap);
}

static void
check_toggled_cb (GtkToggleButton *toggle, GnomeDbEntryBoolean *mgbool)
{
	gtk_toggle_button_set_inconsistent (toggle, FALSE);
}

static gboolean
expand_in_layout (GnomeDbEntryWrapper *mgwrap)
{
	return FALSE;
}

static void
set_editable (GnomeDbEntryWrapper *mgwrap, gboolean editable)
{
	GnomeDbEntryBoolean *mgbool;

	g_return_if_fail (GNOME_DB_IS_ENTRY_BOOLEAN (mgwrap));
	mgbool = GNOME_DB_ENTRY_BOOLEAN (mgwrap);
	g_return_if_fail (mgbool->priv);

	gtk_widget_set_sensitive (mgbool->priv->check, editable);
}

static void
grab_focus (GnomeDbEntryWrapper *mgwrap)
{
	GnomeDbEntryBoolean *mgbool;

	g_return_if_fail (GNOME_DB_IS_ENTRY_BOOLEAN (mgwrap));
	mgbool = GNOME_DB_ENTRY_BOOLEAN (mgwrap);
	g_return_if_fail (mgbool->priv);

	gtk_widget_grab_focus (mgbool->priv->check);
}
