/* gnome-db-entry-shell.c
 *
 * Copyright (C) 2003 - 2008 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <gdk/gdkkeysyms.h>
#include "gnome-db-entry-shell.h"
#include "gnome-db-entry-none.h"
#include <libgda/gda-data-handler.h>
#include <libgnomedb/utility.h>
#include <glib/gi18n-lib.h>

static void gnome_db_entry_shell_class_init (GnomeDbEntryShellClass *class);
static void gnome_db_entry_shell_init (GnomeDbEntryShell *wid);
static void gnome_db_entry_shell_dispose (GObject *object);

static void gnome_db_entry_shell_set_property (GObject *object,
					       guint param_id,
					       const GValue *value,
					       GParamSpec *pspec);
static void gnome_db_entry_shell_get_property (GObject *object,
					       guint param_id,
					       GValue *value,
					       GParamSpec *pspec);


static gint event_cb (GtkWidget *widget, GdkEvent *event, GnomeDbEntryShell *shell);
static void button_show_cb (GtkWidget *widget, GnomeDbEntryShell *shell);
static void contents_modified_cb (GnomeDbEntryShell *shell, gpointer unused);
static void gnome_db_entry_shell_refresh_status_display (GnomeDbEntryShell *shell);

/* properties */
enum
{
	PROP_0,
	PROP_HANDLER,
	PROP_ACTIONS,
	PROP_IS_CELL_RENDERER
};

struct  _GnomeDbEntryShellPriv {
        GtkWidget           *top_box;
	GtkWidget           *hbox;
        GtkWidget           *button;
        GtkStyle            *orig_style;
        GdaDataHandler      *data_handler;
	gboolean             show_actions;

	gboolean             value_is_null;
	gboolean             value_is_modified;
	gboolean             value_is_default;
	gboolean             value_is_non_valid;

	gboolean             is_cell_renderer;
};

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *parent_class = NULL;

/**
 * gnome_db_entry_shell_get_type
 * 
 * Register the GnomeDbEntryShell class on the GLib type system.
 * 
 * Returns: the GType identifying the class.
 */
GType
gnome_db_entry_shell_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0)) {
		static const GTypeInfo info = {
			sizeof (GnomeDbEntryShellClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_entry_shell_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbEntryShell),
			0,
			(GInstanceInitFunc) gnome_db_entry_shell_init
		};		

		type = g_type_register_static (GTK_TYPE_VIEWPORT, "GnomeDbEntryShell", &info, 0);
	}
	return type;
}


static void
gnome_db_entry_shell_class_init (GnomeDbEntryShellClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);
	
	parent_class = g_type_class_peek_parent (class);

	object_class->dispose = gnome_db_entry_shell_dispose;
	
	/* Properties */
	object_class->set_property = gnome_db_entry_shell_set_property;
	object_class->get_property = gnome_db_entry_shell_get_property;
	g_object_class_install_property (object_class, PROP_HANDLER,
					 g_param_spec_pointer ("handler", NULL, NULL, 
							       (G_PARAM_READABLE | G_PARAM_WRITABLE)));
	g_object_class_install_property (object_class, PROP_ACTIONS,
					 g_param_spec_boolean ("actions", NULL, NULL, TRUE,
							       (G_PARAM_READABLE | G_PARAM_WRITABLE)));

	g_object_class_install_property (object_class, PROP_IS_CELL_RENDERER,
					 g_param_spec_boolean ("is_cell_renderer", NULL, NULL, TRUE,
							       (G_PARAM_READABLE | G_PARAM_WRITABLE)));
}

static void
gnome_db_entry_shell_init (GnomeDbEntryShell * shell)
{
	GtkWidget *button, *hbox, *vbox, *arrow;
	GValue *gval;

	/* Private structure */
	shell->priv = g_new0 (GnomeDbEntryShellPriv, 1);
	shell->priv->top_box = NULL;
	shell->priv->button = NULL;
	shell->priv->show_actions = TRUE;
	shell->priv->data_handler = NULL;

	shell->priv->value_is_null = FALSE;
	shell->priv->value_is_modified = FALSE;
	shell->priv->value_is_default = FALSE;
	shell->priv->value_is_non_valid = FALSE;

	shell->priv->is_cell_renderer = FALSE;

	/* Setting the initial layout */
	gtk_viewport_set_shadow_type (GTK_VIEWPORT (shell), GTK_SHADOW_NONE);
	gtk_container_set_border_width (GTK_CONTAINER (shell), 0);
	shell->priv->orig_style = gtk_style_copy (gtk_widget_get_style (GTK_WIDGET (shell)));

	/* hbox */
	hbox = gtk_hbox_new (FALSE, 0);
	gtk_container_add (GTK_CONTAINER (shell), hbox);
	gtk_widget_show (hbox);
	shell->priv->hbox = hbox;

	/* vbox to insert the real widget to edit data */
	vbox = gtk_vbox_new (FALSE, 0); 
	gtk_box_pack_start (GTK_BOX (hbox), vbox, TRUE, TRUE, 0); 
	shell->priv->top_box = vbox;
	gtk_widget_show (vbox);

	/* button to change the entry's state and to display that state */
	arrow = gtk_arrow_new (GTK_ARROW_RIGHT, GTK_SHADOW_NONE);
	button = gtk_button_new ();
	gtk_container_add (GTK_CONTAINER (button), arrow);
	gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, TRUE, 0);  
	shell->priv->button = button;
	gtk_widget_show_all (button);
	gtk_widget_set_size_request (button, 15, 15);

	g_signal_connect (G_OBJECT (button), "event",
			  G_CALLBACK (event_cb), shell);
	g_signal_connect_after (G_OBJECT (button), "show",
				G_CALLBACK (button_show_cb), shell);

	/* focus */
	gval = g_new0 (GValue, 1);
	g_value_init (gval, G_TYPE_BOOLEAN);
	g_value_set_boolean (gval, TRUE);
	g_object_set_property (G_OBJECT (button), "can-focus", gval);
	g_free (gval);
}

static void
gnome_db_entry_shell_dispose (GObject   * object)
{
	GnomeDbEntryShell *shell;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOME_DB_IS_ENTRY_SHELL (object));

	shell = GNOME_DB_ENTRY_SHELL (object);

	if (shell->priv) {
		if (shell->priv->data_handler)
			g_object_unref (shell->priv->data_handler);
		
		g_free (shell->priv);
		shell->priv = NULL;
	}

	/* for the parent class */
	parent_class->dispose (object);
}

static void
gnome_db_entry_shell_set_property (GObject *object,
				   guint param_id,
				   const GValue *value,
				   GParamSpec *pspec)
{
	gpointer ptr;
	GnomeDbEntryShell *shell;

	shell = GNOME_DB_ENTRY_SHELL (object);
	if (shell->priv) {
		switch (param_id) {
		case PROP_HANDLER:
			ptr = g_value_get_pointer (value);
			if (shell->priv->data_handler) {
				g_object_unref (shell->priv->data_handler);
				shell->priv->data_handler = NULL;
			}

			if (ptr) {
				g_assert (GDA_IS_DATA_HANDLER (ptr));
				shell->priv->data_handler = GDA_DATA_HANDLER (ptr);
				g_object_ref (G_OBJECT (shell->priv->data_handler));
			}
			else 
				g_message (_("Widget of class '%s' does not have any associated GdaDataHandler, "
					     "(to be set using the 'handler' property) expect some mis-behaviours"), 
					   G_OBJECT_TYPE_NAME (object));
			break;
		case PROP_ACTIONS:
			shell->priv->show_actions = g_value_get_boolean (value);
			if (shell->priv->show_actions) 
				gtk_widget_show (shell->priv->button);
			else
				gtk_widget_hide (shell->priv->button);
			break;
		case PROP_IS_CELL_RENDERER:
			if (GTK_IS_CELL_EDITABLE (shell) &&
			    (g_value_get_boolean (value) != shell->priv->is_cell_renderer)) {
				shell->priv->is_cell_renderer = g_value_get_boolean (value);
			}
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
			break;
		}
	}
}

static void
gnome_db_entry_shell_get_property (GObject *object,
				   guint param_id,
				   GValue *value,
				   GParamSpec *pspec)
{
	GnomeDbEntryShell *shell;

	shell = GNOME_DB_ENTRY_SHELL (object);
	if (shell->priv) {
		switch (param_id) {
		case PROP_HANDLER:
			if (!shell->priv->data_handler && !GNOME_DB_IS_ENTRY_NONE (object))
				g_message (_("Widget of class '%s' does not have any associated GdaDataHandler, "
					     "(to be set using the 'handler' property) expect some mis-behaviours"), 
					   G_OBJECT_TYPE_NAME (object));
			g_value_set_pointer (value, shell->priv->data_handler);
			break;
		case PROP_ACTIONS:
			g_value_set_boolean (value, shell->priv->show_actions);
			break;
		case PROP_IS_CELL_RENDERER:
			g_value_set_boolean (value, shell->priv->is_cell_renderer);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
			break;
		}
	}
}


/**
 * gnome_db_entry_shell_pack_entry
 * @shell: a #GnomeDbEntryShell object
 * @main_widget: a #GtkWidget to pack into @shell
 *
 * Packs a #GTkWidget widget into the GnomeDbEntryShell.
 */
void
gnome_db_entry_shell_pack_entry (GnomeDbEntryShell *shell, GtkWidget *main_widget)
{
	GList *focus_chain;
	g_return_if_fail (shell && GNOME_DB_IS_ENTRY_SHELL (shell));
	g_return_if_fail (main_widget && GTK_IS_WIDGET (main_widget));

	gtk_box_pack_start (GTK_BOX (shell->priv->top_box), main_widget, TRUE, TRUE, 0);
	focus_chain = g_list_prepend (NULL, main_widget);
	gtk_container_set_focus_chain (GTK_CONTAINER (shell->priv->hbox), focus_chain);

	/* signals */
	g_signal_connect (G_OBJECT (shell), "contents_modified",
			  G_CALLBACK (contents_modified_cb), NULL);

	g_signal_connect (G_OBJECT (shell), "status_changed",
			  G_CALLBACK (contents_modified_cb), NULL);
}

static void
contents_modified_cb (GnomeDbEntryShell *shell, gpointer unused)
{
	gnome_db_entry_shell_refresh (shell);
}

static void
button_show_cb (GtkWidget *widget, GnomeDbEntryShell *shell)
{
	if (! shell->priv->show_actions)
		gtk_widget_hide (widget);
}

static void mitem_activated_cb (GtkWidget *mitem, GnomeDbEntryShell *shell);
static guint gnome_db_entry_shell_refresh_attributes (GnomeDbEntryShell *shell);
static gint
event_cb (GtkWidget *widget, GdkEvent *event, GnomeDbEntryShell *shell)
{
	gboolean done = FALSE;

	if (!shell->priv->show_actions)
		return done;

	if (event->type == GDK_BUTTON_PRESS) {
		GdkEventButton *bevent = (GdkEventButton *) event; 
		if ((bevent->button == 1) || (bevent->button == 3)) {
			GtkWidget *menu;
			guint attributes;
			
			attributes = gnome_db_entry_shell_refresh_attributes (shell);
			menu = gnome_db_utility_entry_build_actions_menu (G_OBJECT (shell), attributes,
								 G_CALLBACK (mitem_activated_cb));
			gtk_menu_popup (GTK_MENU (menu), NULL, NULL, NULL, NULL,
					bevent->button, bevent->time);
			done = TRUE;
		}
	}

	if (event->type == GDK_KEY_PRESS) {
		GtkWidget *menu;
		GdkEventKey *kevent = (GdkEventKey *) event;

		if (kevent->keyval == GDK_space) {
			guint attributes;
			
			attributes = gnome_db_entry_shell_refresh_attributes (shell);
			menu = gnome_db_utility_entry_build_actions_menu (G_OBJECT (shell), attributes,
								 G_CALLBACK (mitem_activated_cb));

			gtk_menu_popup (GTK_MENU (menu), NULL, NULL, NULL, NULL,
					0, kevent->time);
			done = TRUE;
		}
		else {
			if (kevent->keyval == GDK_Tab)
				done = FALSE;
			else
				done = TRUE;
		}
	}
	
	return done;
}

static void
mitem_activated_cb (GtkWidget *mitem, GnomeDbEntryShell *shell)
{
	guint action;

	action = GPOINTER_TO_UINT (g_object_get_data (G_OBJECT (mitem), "action"));
	gnome_db_data_entry_set_attributes (GNOME_DB_DATA_ENTRY (shell), action, action);
}

static void 
gnome_db_entry_shell_refresh_status_display (GnomeDbEntryShell *shell)
{
	static GdkColor **colors = NULL;
	GdkColor *normal = NULL, *prelight = NULL;
	GdkColor *orig_normal, *orig_prelight;

	g_return_if_fail (shell && GNOME_DB_IS_ENTRY_SHELL (shell));

	orig_normal = & (shell->priv->orig_style->bg[GTK_STATE_NORMAL]);
	orig_prelight = & (shell->priv->orig_style->bg[GTK_STATE_PRELIGHT]);

	if (!colors)
		colors = gnome_db_utility_entry_build_info_colors_array ();

	if (shell->priv->value_is_null) {
		normal = colors[0];
		prelight = colors[1];
	}

	if (shell->priv->value_is_default) {
		normal = colors[2];
		prelight = colors[3];
	}

	if (shell->priv->value_is_non_valid) {
		normal = colors[4];
		prelight = colors[5];
	}

	if (!normal)
		normal = orig_normal;
	if (!prelight)
		prelight = orig_prelight;

	gtk_widget_modify_bg (shell->priv->button, GTK_STATE_NORMAL, normal);
	gtk_widget_modify_bg (shell->priv->button, GTK_STATE_ACTIVE, normal);
	gtk_widget_modify_bg (shell->priv->button, GTK_STATE_PRELIGHT, prelight);
}

static GdaValueAttribute
gnome_db_entry_shell_refresh_attributes (GnomeDbEntryShell *shell)
{
	GdaValueAttribute attrs;

	attrs = gnome_db_data_entry_get_attributes (GNOME_DB_DATA_ENTRY (shell));
	shell->priv->value_is_null = attrs & GDA_VALUE_ATTR_IS_NULL;
	shell->priv->value_is_modified = ! (attrs & GDA_VALUE_ATTR_IS_UNCHANGED);
	shell->priv->value_is_default = attrs & GDA_VALUE_ATTR_IS_DEFAULT;
	shell->priv->value_is_non_valid = attrs & GDA_VALUE_ATTR_DATA_NON_VALID;

	return attrs;
}

/**
 * gnome_db_entry_shell_refresh
 * @shell: the GnomeDbEntryShell widget to refresh
 *
 * Forces the shell to refresh its display (mainly the color of the
 * button).
 */
void
gnome_db_entry_shell_refresh (GnomeDbEntryShell *shell)
{
	g_return_if_fail (shell && GNOME_DB_IS_ENTRY_SHELL (shell));
	gnome_db_entry_shell_refresh_attributes (shell);
	gnome_db_entry_shell_refresh_status_display (shell);
}
