/* gnome-db-data-cell-renderer-info.h
 *
 * Copyright (C) 2000  Red Hat, Inc.,  Jonathan Blandford <jrb@redhat.com>
 * Copyright (C) 2003 - 2005 Vivien Malerba <malerba@gnome-db.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GNOME_DB_DATA_CELL_RENDERER_INFO_H__
#define __GNOME_DB_DATA_CELL_RENDERER_INFO_H__

#include <gtk/gtk.h>
#include <libgnomedb/gnome-db-decl.h>
#include <libgda/gda-enums.h>
#include <libgda/gda-decl.h>
#include <libgnomedb/gnome-db-set.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_DATA_CELL_RENDERER_INFO			(gnome_db_data_cell_renderer_info_get_type ())
#define GNOME_DB_DATA_CELL_RENDERER_INFO(obj)			(G_TYPE_CHECK_INSTANCE_CAST ((obj), GNOME_DB_TYPE_DATA_CELL_RENDERER_INFO, GnomeDbDataCellRendererInfo))
#define GNOME_DB_DATA_CELL_RENDERER_INFO_CLASS(klass)		(G_TYPE_CHECK_CLASS_CAST ((klass), GNOME_DB_TYPE_DATA_CELL_RENDERER_INFO, GnomeDbDataCellRendererInfoClass))
#define GNOME_DB_IS_DATA_CELL_RENDERER_INFO(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GNOME_DB_TYPE_DATA_CELL_RENDERER_INFO))
#define GNOME_DB_IS_DATA_CELL_RENDERER_INFO_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), GNOME_DB_TYPE_DATA_CELL_RENDERER_INFO))
#define GNOME_DB_DATA_CELL_RENDERER_INFO_GET_CLASS(obj)         (G_TYPE_INSTANCE_GET_CLASS ((obj), GNOME_DB_TYPE_DATA_CELL_RENDERER_INFO, GnomeDbDataCellRendererInfoClass))

typedef struct _GnomeDbDataCellRendererInfo GnomeDbDataCellRendererInfo;
typedef struct _GnomeDbDataCellRendererInfoClass GnomeDbDataCellRendererInfoClass;
typedef struct _GnomeDbDataCellRendererInfoPriv GnomeDbDataCellRendererInfoPriv;
/* typedef enum   _GnomeDbDataCellRendererInfoStatus GnomeDbDataCellRendererInfoStatus; */

struct _GnomeDbDataCellRendererInfo
{
	GtkCellRenderer             parent;

	GnomeDbDataCellRendererInfoPriv *priv;
};

struct _GnomeDbDataCellRendererInfoClass
{
	GtkCellRendererClass parent_class;
	
	void (* status_changed) (GnomeDbDataCellRendererInfo *cell_renderer_info,
				 const gchar                 *path,
				 GdaValueAttribute            requested_action);
};

GType            gnome_db_data_cell_renderer_info_get_type  (void) G_GNUC_CONST;
GtkCellRenderer *gnome_db_data_cell_renderer_info_new       (GnomeDbDataStore *store,
							     GdaDataModelIter *iter, 
							     GnomeDbSetGroup *group);
G_END_DECLS

#endif /* __GNOME_DB_DATA_CELL_RENDERER_INFO_H__ */
