/* gnome-db-entry-timestamp.h
 *
 * Copyright (C) 2003 - 2006 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __GNOME_DB_ENTRY_TIMESTAMP_H_
#define __GNOME_DB_ENTRY_TIMESTAMP_H_

#include "gnome-db-entry-common-time.h"

G_BEGIN_DECLS

#define GNOME_DB_TYPE_ENTRY_TIMESTAMP          (gnome_db_entry_timestamp_get_type())
#define GNOME_DB_ENTRY_TIMESTAMP(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, gnome_db_entry_timestamp_get_type(), GnomeDbEntryTimestamp)
#define GNOME_DB_ENTRY_TIMESTAMP_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, gnome_db_entry_timestamp_get_type (), GnomeDbEntryTimestampClass)
#define GNOME_DB_IS_ENTRY_TIMESTAMP(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, gnome_db_entry_timestamp_get_type ())


typedef struct _GnomeDbEntryTimestamp GnomeDbEntryTimestamp;
typedef struct _GnomeDbEntryTimestampClass GnomeDbEntryTimestampClass;
typedef struct _GnomeDbEntryTimestampPrivate GnomeDbEntryTimestampPrivate;


/* struct for the object's data */
struct _GnomeDbEntryTimestamp
{
	GnomeDbEntryCommonTime           object;
};

/* struct for the object's class */
struct _GnomeDbEntryTimestampClass
{
	GnomeDbEntryCommonTimeClass      parent_class;
};

GType        gnome_db_entry_timestamp_get_type        (void) G_GNUC_CONST;
GtkWidget   *gnome_db_entry_timestamp_new             (GdaDataHandler *dh);


G_END_DECLS

#endif
