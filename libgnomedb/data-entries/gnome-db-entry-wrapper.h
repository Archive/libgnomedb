/* gnome-db-entry-wrapper.h
 *
 * Copyright (C) 2003 - 2007 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNOME_DB_ENTRY_WRAPPER__
#define __GNOME_DB_ENTRY_WRAPPER__

#include <gtk/gtk.h>
#include "gnome-db-entry-shell.h"
#include <libgnomedb/gnome-db-data-entry.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_ENTRY_WRAPPER          (gnome_db_entry_wrapper_get_type())
#define GNOME_DB_ENTRY_WRAPPER(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, gnome_db_entry_wrapper_get_type(), GnomeDbEntryWrapper)
#define GNOME_DB_ENTRY_WRAPPER_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, gnome_db_entry_wrapper_get_type (), GnomeDbEntryWrapperClass)
#define GNOME_DB_IS_ENTRY_WRAPPER(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, gnome_db_entry_wrapper_get_type ())


typedef struct _GnomeDbEntryWrapper      GnomeDbEntryWrapper;
typedef struct _GnomeDbEntryWrapperClass GnomeDbEntryWrapperClass;
typedef struct _GnomeDbEntryWrapperPriv  GnomeDbEntryWrapperPriv;


/* struct for the object's data */
struct _GnomeDbEntryWrapper
{
	GnomeDbEntryShell        object;

	GnomeDbEntryWrapperPriv  *priv;
};

/* struct for the object's class */
struct _GnomeDbEntryWrapperClass
{
	GnomeDbEntryShellClass   parent_class;

	/* pure virtual functions */
	GtkWidget        *(*create_entry)     (GnomeDbEntryWrapper *mgwrp);
	void              (*real_set_value)   (GnomeDbEntryWrapper *mgwrp, const GValue *value);
	GValue           *(*real_get_value)   (GnomeDbEntryWrapper *mgwrp);
	void              (*connect_signals)  (GnomeDbEntryWrapper *mgwrp, GCallback modify_cb, GCallback activate_cb);
	gboolean          (*expand_in_layout) (GnomeDbEntryWrapper *mgwrp);
	void              (*set_editable)     (GnomeDbEntryWrapper *mgwrp, gboolean editable);

	gboolean          (*value_is_equal_to)(GnomeDbEntryWrapper *mgwrp, const GValue *value);
	gboolean          (*value_is_null)    (GnomeDbEntryWrapper *mgwrp);
	gboolean          (*is_valid)         (GnomeDbEntryWrapper *mgwrp); /* not used yet */
	void              (*grab_focus)       (GnomeDbEntryWrapper *mgwrp);
};


GType           gnome_db_entry_wrapper_get_type           (void) G_GNUC_CONST;
void            gnome_db_entry_wrapper_contents_changed   (GnomeDbEntryWrapper *mgwrp);
void            gnome_db_entry_wrapper_contents_activated (GnomeDbEntryWrapper *mgwrp);

G_END_DECLS

#endif
