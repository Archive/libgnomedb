/* gnome-db-entry-string.c
 *
 * Copyright (C) 2003 - 2008 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <glib/gi18n-lib.h>
#include <string.h>
#include "gnome-db-entry-string.h"
#include "gnome-db-format-entry.h"
#include <libgda/gda-data-handler.h>
#include "gdk/gdkkeysyms.h"
#include <pango/pango.h>

#define MAX_ACCEPTED_STRING_LENGTH 500

/* 
 * Main static functions 
 */
static void gnome_db_entry_string_class_init (GnomeDbEntryStringClass * class);
static void gnome_db_entry_string_init (GnomeDbEntryString * srv);
static void gnome_db_entry_string_dispose (GObject   * object);
static void gnome_db_entry_string_finalize (GObject   * object);

static void gnome_db_entry_string_set_property (GObject *object,
					  guint param_id,
					  const GValue *value,
					  GParamSpec *pspec);
static void gnome_db_entry_string_get_property (GObject *object,
					  guint param_id,
					  GValue *value,
					  GParamSpec *pspec);

/* properties */
enum
{
	PROP_0,
	PROP_MULTILINE,
	PROP_EDITING_CANCELLED,
	PROP_OPTIONS
};

/* GtkCellEditable interface */
static void gnome_db_entry_string_cell_editable_init (GtkCellEditableIface *iface);
static void gnome_db_entry_string_start_editing (GtkCellEditable *iface, GdkEvent *event);
static void sync_entry_options (GnomeDbEntryString *mgstr);

/* virtual functions */
static GtkWidget *create_entry (GnomeDbEntryWrapper *mgwrap);
static void       real_set_value (GnomeDbEntryWrapper *mgwrap, const GValue *value);
static GValue    *real_get_value (GnomeDbEntryWrapper *mgwrap);
static void       connect_signals(GnomeDbEntryWrapper *mgwrap, GCallback modify_cb, GCallback activate_cb);
static gboolean   expand_in_layout (GnomeDbEntryWrapper *mgwrap);
static void       set_editable (GnomeDbEntryWrapper *mgwrap, gboolean editable);
static void       grab_focus (GnomeDbEntryWrapper *mgwrap);

/* options */
static void set_entry_options (GnomeDbEntryString *mgstr, const gchar *options);

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

typedef enum {
	TYPE_UNKNOWN, /* when value is undefined */
	TYPE_NUMERIC,
	TYPE_NOT_NUMERIC
} InternalType;

/* private structure */
struct _GnomeDbEntryStringPrivate
{
	gboolean       multiline;
	InternalType   internal_type;
	GtkWidget     *vbox;

	GtkWidget     *entry;

	GtkTextBuffer *buffer;
	GtkWidget     *sw;
	GtkWidget     *view;

	gint           maxsize;
	guchar         thousand_sep;
	gint           nb_decimals;
	gchar         *currency;

	gulong         entry_change_sig;
};

static void
gnome_db_entry_string_cell_editable_init (GtkCellEditableIface *iface)
{
	iface->start_editing = gnome_db_entry_string_start_editing;
}

GType
gnome_db_entry_string_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0)) {
		static const GTypeInfo info = {
			sizeof (GnomeDbEntryStringClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_entry_string_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbEntryString),
			0,
			(GInstanceInitFunc) gnome_db_entry_string_init
		};
		
		static const GInterfaceInfo cell_editable_info = {
			(GInterfaceInitFunc) gnome_db_entry_string_cell_editable_init,    /* interface_init */
			NULL,                                                 /* interface_finalize */
			NULL                                                  /* interface_data */
		};
		
		type = g_type_register_static (GNOME_DB_TYPE_ENTRY_WRAPPER, "GnomeDbEntryString", &info, 0);
		g_type_add_interface_static (type, GTK_TYPE_CELL_EDITABLE, &cell_editable_info);
	}
	return type;
}

static void
gnome_db_entry_string_class_init (GnomeDbEntryStringClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->dispose = gnome_db_entry_string_dispose;
	object_class->finalize = gnome_db_entry_string_finalize;

	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->create_entry = create_entry;
	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->real_set_value = real_set_value;
	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->real_get_value = real_get_value;
	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->connect_signals = connect_signals;
	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->expand_in_layout = expand_in_layout;
	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->set_editable = set_editable;
	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->grab_focus = grab_focus;

	/* Properties */
	object_class->set_property = gnome_db_entry_string_set_property;
	object_class->get_property = gnome_db_entry_string_get_property;

	g_object_class_install_property (object_class, PROP_MULTILINE,
					 g_param_spec_boolean ("multiline", NULL, NULL, FALSE, 
							       G_PARAM_READABLE | G_PARAM_WRITABLE));
	g_object_class_install_property (object_class, PROP_EDITING_CANCELLED,
					 g_param_spec_boolean ("editing_cancelled", NULL, NULL, FALSE, G_PARAM_READABLE));
	g_object_class_install_property (object_class, PROP_OPTIONS,
					 g_param_spec_string ("options", NULL, NULL, NULL, G_PARAM_WRITABLE));
}

static void
gnome_db_entry_string_init (GnomeDbEntryString * mgstr)
{
	mgstr->priv = g_new0 (GnomeDbEntryStringPrivate, 1);
	mgstr->priv->multiline = FALSE;
	mgstr->priv->vbox = NULL;
	mgstr->priv->entry = NULL;
	mgstr->priv->buffer = NULL;
	mgstr->priv->view = NULL;
	mgstr->priv->sw = NULL;

	mgstr->priv->internal_type = TYPE_UNKNOWN;
	mgstr->priv->maxsize = 0; /* unlimited */
	mgstr->priv->thousand_sep = 0;
	mgstr->priv->nb_decimals = -1; /* unlimited number of decimals */
	mgstr->priv->currency = NULL;

	mgstr->priv->entry_change_sig = 0;
}

static InternalType
determine_internal_type (GType type)
{
	if ((type == G_TYPE_INT64) || (type == G_TYPE_UINT64) || (type == G_TYPE_DOUBLE) ||
	    (type == G_TYPE_INT) || (type == GDA_TYPE_NUMERIC) || (type == G_TYPE_FLOAT) ||
	    (type == GDA_TYPE_SHORT) || (type == GDA_TYPE_USHORT) || (type == G_TYPE_CHAR) ||
	    (type == G_TYPE_UCHAR) || (type == G_TYPE_LONG) || (type ==  G_TYPE_ULONG) || (type == G_TYPE_UINT))
		return TYPE_NUMERIC;
	else
		return TYPE_NOT_NUMERIC;
}

/**
 * gnome_db_entry_string_new
 * @dh: the data handler to be used by the new widget
 * @type: the requested data type (compatible with @dh)
 *
 * Creates a new widget which is mainly a GtkEntry
 *
 * Returns: the new widget
 */
GtkWidget *
gnome_db_entry_string_new (GdaDataHandler *dh, GType type, const gchar *options)
{
	GObject *obj;
	GnomeDbEntryString *mgstr;

	g_return_val_if_fail (GDA_IS_DATA_HANDLER (dh), NULL);
	g_return_val_if_fail (type != G_TYPE_INVALID, NULL);
	g_return_val_if_fail (gda_data_handler_accepts_g_type (dh, type), NULL);

	obj = g_object_new (GNOME_DB_TYPE_ENTRY_STRING, "handler", dh, NULL);
	mgstr = GNOME_DB_ENTRY_STRING (obj);
	gnome_db_data_entry_set_value_type (GNOME_DB_DATA_ENTRY (mgstr), type);

	mgstr->priv->internal_type = determine_internal_type (type);
	g_object_set (obj, "options", options, NULL);

	return GTK_WIDGET (obj);
}

static gboolean focus_out_cb (GtkWidget *widget, GdkEventFocus *event, GnomeDbEntryString *mgstr);
static void
gnome_db_entry_string_dispose (GObject   * object)
{
	GnomeDbEntryString *mgstr;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOME_DB_IS_ENTRY_STRING (object));

	mgstr = GNOME_DB_ENTRY_STRING (object);
	if (mgstr->priv) {
		if (mgstr->priv->entry) {
			g_signal_handlers_disconnect_by_func (G_OBJECT (mgstr->priv->entry),
							      G_CALLBACK (focus_out_cb), mgstr);
			mgstr->priv->entry = NULL;
		}
		
		if (mgstr->priv->view) {
			g_signal_handlers_disconnect_by_func (G_OBJECT (mgstr->priv->view),
							      G_CALLBACK (focus_out_cb), mgstr);
			mgstr->priv->view = NULL;
		}
	}

	/* parent class */
	parent_class->dispose (object);
}

static void
gnome_db_entry_string_finalize (GObject   * object)
{
	GnomeDbEntryString *mgstr;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOME_DB_IS_ENTRY_STRING (object));

	mgstr = GNOME_DB_ENTRY_STRING (object);
	if (mgstr->priv) {
		g_free (mgstr->priv->currency);
		g_free (mgstr->priv);
		mgstr->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}

static void
gnome_db_entry_string_set_property (GObject *object,
				    guint param_id,
				    const GValue *value,
				    GParamSpec *pspec)
{
	GnomeDbEntryString *mgstr;

	mgstr = GNOME_DB_ENTRY_STRING (object);
	if (mgstr->priv) {
		switch (param_id) {
		case PROP_MULTILINE:
			if (mgstr->priv->internal_type == TYPE_UNKNOWN)
				mgstr->priv->internal_type = 
					determine_internal_type (gnome_db_data_entry_get_value_type 
								 (GNOME_DB_DATA_ENTRY (mgstr)));

			if (mgstr->priv->internal_type == TYPE_NUMERIC)
				return;

			if (g_value_get_boolean (value) != mgstr->priv->multiline) {
				mgstr->priv->multiline = g_value_get_boolean (value);
				if (mgstr->priv->multiline) {
					gtk_widget_hide (mgstr->priv->entry);
					gtk_widget_show (mgstr->priv->sw);
				}
				else {
					gtk_widget_show (mgstr->priv->entry);
					gtk_widget_hide (mgstr->priv->sw);
				}
			}
			break;
		case PROP_OPTIONS:
			set_entry_options (mgstr, g_value_get_string (value));
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
			break;
		}
	}
}

static void
gnome_db_entry_string_get_property (GObject *object,
			     guint param_id,
			     GValue *value,
			     GParamSpec *pspec)
{
	GnomeDbEntryString *mgstr;

	mgstr = GNOME_DB_ENTRY_STRING (object);
	if (mgstr->priv) {
		switch (param_id) {
		case PROP_MULTILINE:
			g_value_set_boolean (value, mgstr->priv->multiline);
			break;
		case PROP_EDITING_CANCELLED:
			g_value_set_boolean (value, GTK_ENTRY (mgstr->priv->entry)->editing_canceled);
			break;
		default:
			G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
			break;
		}
	}
}

static void widget_shown_cb (GtkWidget *wid, GnomeDbEntryString *mgstr);

static GtkWidget *
create_entry (GnomeDbEntryWrapper *mgwrap)
{
	GtkWidget *vbox;
	GnomeDbEntryString *mgstr;

	g_return_val_if_fail (GNOME_DB_IS_ENTRY_STRING (mgwrap), NULL);
	mgstr = GNOME_DB_ENTRY_STRING (mgwrap);
	g_return_val_if_fail (mgstr->priv, NULL);

	vbox = gtk_vbox_new (FALSE, 0);
	mgstr->priv->vbox = vbox;

	/* one line entry */
	mgstr->priv->entry = gnome_db_format_entry_new ();
	sync_entry_options (mgstr);
	gtk_box_pack_start (GTK_BOX (vbox), mgstr->priv->entry, FALSE, TRUE, 0);
	g_signal_connect_after (G_OBJECT (mgstr->priv->entry), "show", 
				G_CALLBACK (widget_shown_cb), mgstr);

	/* multiline entry */
	mgstr->priv->view = gtk_text_view_new ();
	mgstr->priv->buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (mgstr->priv->view));
	mgstr->priv->sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (mgstr->priv->sw), GTK_SHADOW_IN);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (mgstr->priv->sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_container_add (GTK_CONTAINER (mgstr->priv->sw), mgstr->priv->view);
	gtk_widget_show (mgstr->priv->view);
	gtk_box_pack_start (GTK_BOX (vbox), mgstr->priv->sw, TRUE, TRUE, 0);
	g_signal_connect_after (G_OBJECT (mgstr->priv->sw), "show", 
				G_CALLBACK (widget_shown_cb), mgstr);


	/* show widgets if they need to be shown */
	gtk_widget_show (mgstr->priv->entry);
	gtk_widget_show (mgstr->priv->sw);

	return vbox;
}

static void
widget_shown_cb (GtkWidget *wid, GnomeDbEntryString *mgstr)
{
	if ((wid == mgstr->priv->entry) && mgstr->priv->multiline)
		gtk_widget_hide (mgstr->priv->entry);

	if ((wid == mgstr->priv->sw) && !mgstr->priv->multiline)
		gtk_widget_hide (mgstr->priv->sw);
}


static void
real_set_value (GnomeDbEntryWrapper *mgwrap, const GValue *value)
{
	GnomeDbEntryString *mgstr;
	GdaDataHandler *dh;

	PangoLayout *layout;
	gchar *text;
	static gchar *too_long_msg = NULL;
	static gint too_long_msg_len;
	
	g_return_if_fail (GNOME_DB_IS_ENTRY_STRING (mgwrap));
	mgstr = GNOME_DB_ENTRY_STRING (mgwrap);
	g_return_if_fail (mgstr->priv);

	dh = gnome_db_data_entry_get_handler (GNOME_DB_DATA_ENTRY (mgwrap));

	/* do we need to go into multi line mode ? */
	if (!too_long_msg) {
		too_long_msg = _("<string cut because too long>");
		too_long_msg_len = strlen (too_long_msg);
	}
	text = gda_data_handler_get_str_from_value (dh, value);
	if (text && (strlen (text) > MAX_ACCEPTED_STRING_LENGTH + too_long_msg_len)) {
		memmove (text + too_long_msg_len, text, MAX_ACCEPTED_STRING_LENGTH);
		memcpy (text, too_long_msg, too_long_msg_len);
		text [MAX_ACCEPTED_STRING_LENGTH + too_long_msg_len + 1] = 0;
	}

	layout = gtk_widget_create_pango_layout (GTK_WIDGET (mgwrap), text);
	if (pango_layout_get_line_count (layout) > 1) 
		g_object_set (G_OBJECT (mgwrap), "multiline", TRUE, NULL);
	g_object_unref (G_OBJECT (layout));
	
	/* fill the single line widget */
	if (value) {
		if (gda_value_is_null ((GValue *) value))
			gnome_db_format_entry_set_text (GNOME_DB_FORMAT_ENTRY (mgstr->priv->entry), NULL);
		else 
			gnome_db_format_entry_set_text (GNOME_DB_FORMAT_ENTRY (mgstr->priv->entry), text);
	}
	else
		gnome_db_format_entry_set_text (GNOME_DB_FORMAT_ENTRY (mgstr->priv->entry), NULL);

	/* fill the multiline widget */
	if (value) {
		if (gda_value_is_null ((GValue *) value) || !text)
                        gtk_text_buffer_set_text (mgstr->priv->buffer, "", -1);
		else 
			gtk_text_buffer_set_text (mgstr->priv->buffer, text, -1);
	}
	else 
		gtk_text_buffer_set_text (mgstr->priv->buffer, "", -1);

	g_free (text);
}

static GValue *
real_get_value (GnomeDbEntryWrapper *mgwrap)
{
	GValue *value;
	GnomeDbEntryString *mgstr;
	GdaDataHandler *dh;

	g_return_val_if_fail (GNOME_DB_IS_ENTRY_STRING (mgwrap), NULL);
	mgstr = GNOME_DB_ENTRY_STRING (mgwrap);
	g_return_val_if_fail (mgstr->priv, NULL);

	dh = gnome_db_data_entry_get_handler (GNOME_DB_DATA_ENTRY (mgwrap));
	if (! mgstr->priv->multiline) {
		gchar *cstr;
		cstr = gnome_db_format_entry_get_text (GNOME_DB_FORMAT_ENTRY (mgstr->priv->entry));
		value = gda_data_handler_get_value_from_str (dh, cstr, gnome_db_data_entry_get_value_type (GNOME_DB_DATA_ENTRY (mgwrap)));
		g_free (cstr);
	}
	else {
		GtkTextIter start, end;
		gchar *str;
		gtk_text_buffer_get_start_iter (mgstr->priv->buffer, &start);
		gtk_text_buffer_get_end_iter (mgstr->priv->buffer, &end);
		str = gtk_text_buffer_get_text (mgstr->priv->buffer, &start, &end, FALSE);
		value = gda_data_handler_get_value_from_str (dh, str, gnome_db_data_entry_get_value_type (GNOME_DB_DATA_ENTRY (mgwrap)));
		g_free (str);
	}

	if (!value) {
		/* in case the gda_data_handler_get_value_from_str() returned an error because
		   the contents of the GtkEntry cannot be interpreted as a GValue */
		value = gda_value_new_null ();
	}

	return value;
}

typedef void (*Callback2) (gpointer, gpointer);
static gboolean
focus_out_cb (GtkWidget *widget, GdkEventFocus *event, GnomeDbEntryString *mgstr)
{
	GCallback activate_cb;
	activate_cb = g_object_get_data (G_OBJECT (widget), "_activate_cb");
	g_assert (activate_cb);
	((Callback2)activate_cb) (widget, mgstr);

	return FALSE;
}

static void
connect_signals(GnomeDbEntryWrapper *mgwrap, GCallback modify_cb, GCallback activate_cb)
{
	GnomeDbEntryString *mgstr;

	g_return_if_fail (GNOME_DB_IS_ENTRY_STRING (mgwrap));
	mgstr = GNOME_DB_ENTRY_STRING (mgwrap);
	g_return_if_fail (mgstr->priv);
	g_object_set_data (G_OBJECT (mgstr->priv->entry), "_activate_cb", activate_cb);
	g_object_set_data (G_OBJECT (mgstr->priv->view), "_activate_cb", activate_cb);

	mgstr->priv->entry_change_sig = g_signal_connect (G_OBJECT (mgstr->priv->entry), "changed",
							  modify_cb, mgwrap);
	g_signal_connect (G_OBJECT (mgstr->priv->entry), "activate",
			  activate_cb, mgwrap);
	g_signal_connect (G_OBJECT (mgstr->priv->entry), "focus-out-event",
			  G_CALLBACK (focus_out_cb), mgstr);

	g_signal_connect (G_OBJECT (mgstr->priv->buffer), "changed",
			  modify_cb, mgwrap);
	g_signal_connect (G_OBJECT (mgstr->priv->view), "focus-out-event",
			  G_CALLBACK (focus_out_cb), mgstr);
	/* FIXME: how does the user "activates" the GtkTextView widget ? */
}

static gboolean
expand_in_layout (GnomeDbEntryWrapper *mgwrap)
{
	GnomeDbEntryString *mgstr;

	g_return_val_if_fail (GNOME_DB_IS_ENTRY_STRING (mgwrap), FALSE);
	mgstr = GNOME_DB_ENTRY_STRING (mgwrap);
	g_return_val_if_fail (mgstr->priv, FALSE);

	return mgstr->priv->multiline;
}

static void
set_editable (GnomeDbEntryWrapper *mgwrap, gboolean editable)
{
	GnomeDbEntryString *mgstr;

	g_return_if_fail (GNOME_DB_IS_ENTRY_STRING (mgwrap));
	mgstr = GNOME_DB_ENTRY_STRING (mgwrap);
	g_return_if_fail (mgstr->priv);

	gtk_entry_set_editable (GTK_ENTRY (mgstr->priv->entry), editable);
	gtk_text_view_set_editable (GTK_TEXT_VIEW (mgstr->priv->view), editable);
}

static void
grab_focus (GnomeDbEntryWrapper *mgwrap)
{
	GnomeDbEntryString *mgstr;

	g_return_if_fail (GNOME_DB_IS_ENTRY_STRING (mgwrap));
	mgstr = GNOME_DB_ENTRY_STRING (mgwrap);
	g_return_if_fail (mgstr->priv);

	if (mgstr->priv->multiline)
		gtk_widget_grab_focus (mgstr->priv->view);
	else
		gtk_widget_grab_focus (mgstr->priv->entry);
}

/*
 * GtkCellEditable interface
 */
static void
gtk_cell_editable_entry_editing_done_cb (GtkEntry *entry, GnomeDbEntryString *mgstr) 
{
	gtk_cell_editable_editing_done (GTK_CELL_EDITABLE (mgstr));
}

static void
gtk_cell_editable_entry_remove_widget_cb (GtkEntry *entry, GnomeDbEntryString *mgstr) 
{
	gtk_cell_editable_remove_widget (GTK_CELL_EDITABLE (mgstr));
}

static void
gnome_db_entry_string_start_editing (GtkCellEditable *iface, GdkEvent *event)
{
	GnomeDbEntryString *mgstr;

	g_return_if_fail (iface && GNOME_DB_IS_ENTRY_STRING (iface));
	mgstr = GNOME_DB_ENTRY_STRING (iface);
	g_return_if_fail (mgstr->priv);

	g_object_set (G_OBJECT (mgstr->priv->entry), "has_frame", FALSE, "xalign", 0., NULL);
	gtk_text_view_set_border_window_size (GTK_TEXT_VIEW (mgstr->priv->view), GTK_TEXT_WINDOW_LEFT, 0);
	gtk_text_view_set_border_window_size (GTK_TEXT_VIEW (mgstr->priv->view), GTK_TEXT_WINDOW_RIGHT, 0);
	gtk_text_view_set_border_window_size (GTK_TEXT_VIEW (mgstr->priv->view), GTK_TEXT_WINDOW_TOP, 0);
	gtk_text_view_set_border_window_size (GTK_TEXT_VIEW (mgstr->priv->view), GTK_TEXT_WINDOW_BOTTOM, 0);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (mgstr->priv->sw), GTK_SHADOW_NONE);
	gtk_container_set_border_width (GTK_CONTAINER (mgstr->priv->sw), 0);

	gtk_cell_editable_start_editing (GTK_CELL_EDITABLE (mgstr->priv->entry), event);
	g_signal_connect (G_OBJECT (mgstr->priv->entry), "editing_done",
			  G_CALLBACK (gtk_cell_editable_entry_editing_done_cb), mgstr);
	g_signal_connect (G_OBJECT (mgstr->priv->entry), "remove_widget",
			  G_CALLBACK (gtk_cell_editable_entry_remove_widget_cb), mgstr);
	gnome_db_entry_shell_refresh (GNOME_DB_ENTRY_SHELL (mgstr));

	gtk_widget_grab_focus (mgstr->priv->entry);
	gtk_widget_queue_draw (GTK_WIDGET (mgstr));
}

/*
 * Options handling
 */

static guchar
get_default_thousands_sep ()
{
	static guchar value = 255;

	if (value == 255) {
		gchar text[20];
		sprintf (text, "%f", 1234.);
		if (text[1] == '2')
			value = 0;
		else
			value = text[1];	
	}
	return value;
}

static void
set_entry_options (GnomeDbEntryString *mgstr, const gchar *options)
{
	g_assert (mgstr->priv);

	if (options && *options) {
                GdaQuarkList *params;
                const gchar *str;
		
		if (mgstr->priv->internal_type == TYPE_UNKNOWN)
			mgstr->priv->internal_type = 
				determine_internal_type (gnome_db_data_entry_get_value_type 
							 (GNOME_DB_DATA_ENTRY (mgstr)));

                params = gda_quark_list_new_from_string (options);
		if (mgstr->priv->internal_type == TYPE_NOT_NUMERIC) {
			/* STRING specific options */
			str = gda_quark_list_find (params, "MAX_SIZE");
			if (str) 
				mgstr->priv->maxsize = atoi (str);

			str = gda_quark_list_find (params, "MULTILINE");
			if (str) {
				if ((*str == 't') || (*str == 'T'))
					mgstr->priv->multiline = TRUE;
				else
					mgstr->priv->multiline = FALSE;
				
				if (mgstr->priv->entry) {
					if (mgstr->priv->multiline) {
						gtk_widget_hide (mgstr->priv->entry);
						gtk_widget_show (mgstr->priv->sw);
					}
					else {
						gtk_widget_show (mgstr->priv->entry);
						gtk_widget_hide (mgstr->priv->sw);
					}
				}
			}
                }
		else {
			/* NUMERIC specific options */
			str = gda_quark_list_find (params, "THOUSAND_SEP");
			if (str) {
				if ((*str == 't') || (*str == 'T'))
					mgstr->priv->thousand_sep = get_default_thousands_sep ();
				else
					mgstr->priv->thousand_sep = 0;
			}
			str = gda_quark_list_find (params, "NB_DECIMALS");
			if (str) 
				mgstr->priv->nb_decimals = atoi (str);
			str = gda_quark_list_find (params, "CURRENCY");
			if (str) {
				g_free (mgstr->priv->currency);
				mgstr->priv->currency = g_strdup_printf ("%s ", str);
			}
		}
                gda_quark_list_free (params);
		sync_entry_options (mgstr);
        }
}

/* sets the correct options for mgstr->priv->entry if it exists */
static void
sync_entry_options (GnomeDbEntryString *mgstr)
{
	if (!mgstr->priv->entry)
		return;

	g_object_set (G_OBJECT (mgstr->priv->entry), 
		      "edited_type", gnome_db_data_entry_get_value_type (GNOME_DB_DATA_ENTRY (mgstr)),
		      "n_decimals", mgstr->priv->nb_decimals,
		      "thousands_sep", mgstr->priv->thousand_sep,
		      "prefix", mgstr->priv->currency,
		      "max_length", mgstr->priv->maxsize,
		      NULL);
	g_signal_emit_by_name (mgstr->priv->entry, "changed");
}
