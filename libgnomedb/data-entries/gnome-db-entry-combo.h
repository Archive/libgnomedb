/* gnome-db-entry-combo.h
 *
 * Copyright (C) 2003 - 2005 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNOME_DB_ENTRY_COMBO__
#define __GNOME_DB_ENTRY_COMBO__

#include <gtk/gtk.h>
#include "gnome-db-entry-shell.h"
#include <libgnomedb/gnome-db-data-entry.h>
#include <libgnomedb/gnome-db-set.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_ENTRY_COMBO          (gnome_db_entry_combo_get_type())
#define GNOME_DB_ENTRY_COMBO(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, gnome_db_entry_combo_get_type(), GnomeDbEntryCombo)
#define GNOME_DB_ENTRY_COMBO_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, gnome_db_entry_combo_get_type (), GnomeDbEntryComboClass)
#define GNOME_DB_IS_ENTRY_COMBO(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, gnome_db_entry_combo_get_type ())


typedef struct _GnomeDbEntryCombo      GnomeDbEntryCombo;
typedef struct _GnomeDbEntryComboClass GnomeDbEntryComboClass;
typedef struct _GnomeDbEntryComboPriv  GnomeDbEntryComboPriv;


/* struct for the object's data */
struct _GnomeDbEntryCombo
{
	GnomeDbEntryShell        object;
	GnomeDbEntryComboPriv   *priv;
};

/* struct for the object's class */
struct _GnomeDbEntryComboClass
{
	GnomeDbEntryShellClass   parent_class;
};


GType           gnome_db_entry_combo_get_type          (void) G_GNUC_CONST;
GtkWidget      *gnome_db_entry_combo_new               (GnomeDbSet *paramlist, GnomeDbSetSource *source);

gboolean        gnome_db_entry_combo_set_values        (GnomeDbEntryCombo *combo, GSList *values);
GSList         *gnome_db_entry_combo_get_values        (GnomeDbEntryCombo *combo);
GSList         *gnome_db_entry_combo_get_all_values    (GnomeDbEntryCombo *combo);
void            gnome_db_entry_combo_set_values_orig   (GnomeDbEntryCombo *combo, GSList *values);
GSList         *gnome_db_entry_combo_get_values_orig   (GnomeDbEntryCombo *combo);
void            gnome_db_entry_combo_set_values_default(GnomeDbEntryCombo *combo, GSList *values);

G_END_DECLS

#endif
