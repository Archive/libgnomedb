/* gnome-db-data-cell-renderer-boolean.h
 * Copyright (C) 2000  Red Hat, Inc.,  Jonathan Blandford <jrb@redhat.com>
 * Copyright (C) 2003  Vivien Malerba <malerba@gnome-db.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GNOME_DB_DATA_CELL_RENDERER_BOOLEAN_H__
#define __GNOME_DB_DATA_CELL_RENDERER_BOOLEAN_H__

#include <gtk/gtk.h>
#include <libgnomedb/gnome-db-decl.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


#define GNOME_DB_TYPE_DATA_CELL_RENDERER_BOOLEAN		(gnome_db_data_cell_renderer_boolean_get_type ())
#define GNOME_DB_DATA_CELL_RENDERER_BOOLEAN(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), GNOME_DB_TYPE_DATA_CELL_RENDERER_BOOLEAN, GnomeDbDataCellRendererBoolean))
#define GNOME_DB_DATA_CELL_RENDERER_BOOLEAN_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), GNOME_DB_TYPE_DATA_CELL_RENDERER_BOOLEAN, GnomeDbDataCellRendererBooleanClass))
#define GNOME_DB_IS_DATA_CELL_RENDERER_BOOLEAN(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GNOME_DB_TYPE_DATA_CELL_RENDERER_BOOLEAN))
#define GNOME_DB_IS_DATA_CELL_RENDERER_BOOLEAN_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), GNOME_DB_TYPE_DATA_CELL_RENDERER_BOOLEAN))
#define GNOME_DB_DATA_CELL_RENDERER_BOOLEAN_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GNOME_DB_TYPE_DATA_CELL_RENDERER_BOOLEAN, GnomeDbDataCellRendererBooleanClass))

typedef struct _GnomeDbDataCellRendererBoolean GnomeDbDataCellRendererBoolean;
typedef struct _GnomeDbDataCellRendererBooleanClass GnomeDbDataCellRendererBooleanClass;
typedef struct _GnomeDbDataCellRendererBooleanPrivate GnomeDbDataCellRendererBooleanPrivate;

struct _GnomeDbDataCellRendererBoolean
{
	GtkCellRendererToggle             parent;
	
	GnomeDbDataCellRendererBooleanPrivate *priv;
};

struct _GnomeDbDataCellRendererBooleanClass
{
	GtkCellRendererToggleClass  parent_class;
	
	void (* changed) (GnomeDbDataCellRendererBoolean *cell_renderer,
			  const gchar               *path,
			  const GValue            *new_value);
};

GType            gnome_db_data_cell_renderer_boolean_get_type  (void) G_GNUC_CONST;
GtkCellRenderer *gnome_db_data_cell_renderer_boolean_new       (GdaDataHandler *dh, GType type);

#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif /* __GNOME_DB_DATA_CELL_RENDERER_BOOLEAN_H__ */
