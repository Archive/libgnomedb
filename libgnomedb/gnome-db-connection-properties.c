/* GNOME DB library
 * Copyright (C) 1999 - 2008 The GNOME Foundation.
 *
 * AUTHORS:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <glib/gi18n-lib.h>
#include <gtk/gtk.h>
#include <libgda/libgda.h>
#include <libgnomedb/gnome-db-util.h>
#include <libgnomedb/gnome-db-grid.h>
#include <libgnomedb/gnome-db-transaction-status.h>
#include "gnome-db-connection-properties.h"

#define PARENT_TYPE GTK_TYPE_VBOX

struct _GnomeDbConnectionPropertiesPrivate {
	GdaConnection *cnc;

	/* widgets */
	GtkWidget *cnc_name;
	GtkWidget *cnc_string;
	GtkWidget *cnc_provider;
	GtkWidget *cnc_username;
	GtkWidget *cnc_status;
	GtkWidget *cnc_trans;
	GtkWidget *feature_list;
};

enum {
	PROP_0,

	PROP_CNC
};

static void gnome_db_connection_properties_class_init (GnomeDbConnectionPropertiesClass *klass);
static void gnome_db_connection_properties_init       (GnomeDbConnectionProperties *props,
						       GnomeDbConnectionPropertiesClass *klass);
static void gnome_db_connection_properties_finalize   (GObject *object);

static void gnome_db_connection_properties_set_property (GObject *object,
                                                         guint param_id,
                                                         const GValue *value,
                                                         GParamSpec *pspec);
static void gnome_db_connection_properties_get_property (GObject* object,
                                                         guint param_id,
                                                         GValue *value,
                                                         GParamSpec *pspec);

static GObjectClass *parent_class = NULL;


static void
refresh_widget (GnomeDbConnectionProperties *props)
{
	const gchar *cstr;
	GdaQuarkList* ql;

	g_return_if_fail (GNOME_DB_IS_CONNECTION_PROPERTIES (props));

	if (props->priv->cnc) {
		cstr = gda_connection_get_dsn (props->priv->cnc);
		gtk_label_set_text (GTK_LABEL (props->priv->cnc_name), cstr ? cstr : "");
		cstr = gda_connection_get_cnc_string (props->priv->cnc);
		gtk_label_set_text (GTK_LABEL (props->priv->cnc_string), cstr ? cstr : "");
		cstr = gda_connection_get_provider_name (props->priv->cnc);
		gtk_label_set_text (GTK_LABEL (props->priv->cnc_provider), cstr ? cstr : "");
		
		gtk_label_set_text (GTK_LABEL (props->priv->cnc_status), 
				    gda_connection_is_opened (props->priv->cnc) ? _("Opened"): _("Closed"));
		
		/* only get USERNAME from the the authentication string */
		cstr =  gda_connection_get_authentication (props->priv->cnc);
		ql = gda_quark_list_new_from_string (cstr);
		cstr = gda_quark_list_find (ql, "USERNAME");
		gtk_label_set_text (GTK_LABEL (props->priv->cnc_username), cstr ? cstr : "");
		gda_quark_list_free (ql);
	}
	else {
		gtk_label_set_markup (GTK_LABEL (props->priv->cnc_name), "");
		gtk_label_set_text (GTK_LABEL (props->priv->cnc_string), "");
		gtk_label_set_text (GTK_LABEL (props->priv->cnc_provider), "");
		gtk_label_set_text (GTK_LABEL (props->priv->cnc_status), "");
		gtk_label_set_text (GTK_LABEL (props->priv->cnc_username), "");
	}
}

static void
cnc_status_changed_cb (GdaConnection *cnc, GnomeDbConnectionProperties *props)
{
	refresh_widget (props);
}

static void
cnc_error_cb (GdaConnection *cnc, GdaConnectionEvent *event, GnomeDbConnectionProperties *props)
{
	refresh_widget (props);
}

/*
 * GnomeDbConnectionProperties class implementation
 */

static void
gnome_db_connection_properties_class_init (GnomeDbConnectionPropertiesClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gnome_db_connection_properties_finalize;
	object_class->set_property = gnome_db_connection_properties_set_property;
	object_class->get_property = gnome_db_connection_properties_get_property;

	g_object_class_install_property (object_class, PROP_CNC,
	                                 g_param_spec_object("connection", NULL, NULL,
					                     GDA_TYPE_CONNECTION, G_PARAM_READWRITE));
}

static const gchar *
get_feature_name (GdaConnectionFeature f)
{
	switch (f) {
	case GDA_CONNECTION_FEATURE_AGGREGATES:
		return _("Aggregates");
	case GDA_CONNECTION_FEATURE_BLOBS:
		return _("Blobs");
	case GDA_CONNECTION_FEATURE_INDEXES:
		return _("Indexes");
	case GDA_CONNECTION_FEATURE_INHERITANCE:
		return _("Inheritance");
	case GDA_CONNECTION_FEATURE_NAMESPACES:
		return _("Namespaces");
	case GDA_CONNECTION_FEATURE_PROCEDURES:
		return _("Procedures");
	case GDA_CONNECTION_FEATURE_SEQUENCES:
		return _("Sequences");
	case GDA_CONNECTION_FEATURE_SQL:
		return _("SQL");
	case GDA_CONNECTION_FEATURE_TRANSACTIONS:
		return _("Transactions");
	case GDA_CONNECTION_FEATURE_SAVEPOINTS:
		return _("Savepoints");
	case GDA_CONNECTION_FEATURE_SAVEPOINTS_REMOVE:
		return _("Savepoint removal");
	case GDA_CONNECTION_FEATURE_TRIGGERS:
		return _("Triggers");
	case GDA_CONNECTION_FEATURE_UPDATABLE_CURSOR:
		return _("Updatable cursors");
	case GDA_CONNECTION_FEATURE_USERS:
		return _("Users definition");
	case GDA_CONNECTION_FEATURE_VIEWS:
		return _("Views");
	default:
		g_warning ("Unknown GdaConnectionFeature %d", f);
		return "Unknown feature";
	}
}

static void
gnome_db_connection_properties_init (GnomeDbConnectionProperties *props,
				     GnomeDbConnectionPropertiesClass *klass)
{
	GtkWidget *table;
	GtkWidget *label;
	GtkWidget *hbox;
	gchar *str;

	g_return_if_fail (GNOME_DB_IS_CONNECTION_PROPERTIES (props));

	/* allocate internal structure */
	props->priv = g_new0 (GnomeDbConnectionPropertiesPrivate, 1);
	props->priv->cnc = NULL;

	/* create widgets */
	str = g_strdup_printf ("<b>%s:</b>", _("Settings"));
	label = gtk_label_new ("");
	gtk_label_set_markup (GTK_LABEL (label), str);
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);
	g_free (str);
	gtk_box_pack_start (GTK_BOX (props), label, FALSE, TRUE, 2);
	
	hbox = gtk_hbox_new (FALSE, 0); /* HIG */
	gtk_box_pack_start (GTK_BOX (props), hbox, FALSE, FALSE, 0);
	gtk_widget_show (hbox);
	label = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	table = gtk_table_new (6, 2, FALSE);
	gtk_box_pack_start (GTK_BOX (hbox), table, TRUE, TRUE, 0);	

	label = gtk_label_new (_("Data source name:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 0, 1, GTK_FILL, GTK_FILL, 2, 2);
	props->priv->cnc_name = gtk_label_new ("");
	gtk_misc_set_alignment (GTK_MISC (props->priv->cnc_name), 0., -1);
	gtk_label_set_selectable (GTK_LABEL (props->priv->cnc_name), TRUE);
	gtk_table_attach (GTK_TABLE (table), props->priv->cnc_name,
			  1, 2, 0, 1, GTK_FILL, GTK_FILL, 2, 2);

	label = gtk_label_new (_("Connection string:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 1, 2, GTK_FILL, GTK_FILL, 2, 2);
	props->priv->cnc_string = gtk_label_new ("");
	gtk_misc_set_alignment (GTK_MISC (props->priv->cnc_string), 0., -1);
	gtk_label_set_line_wrap (GTK_LABEL (props->priv->cnc_string), TRUE);
	gtk_label_set_selectable (GTK_LABEL (props->priv->cnc_string), TRUE);
	gtk_table_attach (GTK_TABLE (table), props->priv->cnc_string,
			  1, 2, 1, 2, GTK_FILL, GTK_FILL, 2, 2);

	label = gtk_label_new (_("Provider:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 2, 3, GTK_FILL, GTK_FILL, 2, 2);
	props->priv->cnc_provider = gtk_label_new ("");
	gtk_misc_set_alignment (GTK_MISC (props->priv->cnc_provider), 0., -1);
	gtk_label_set_selectable (GTK_LABEL (props->priv->cnc_provider), TRUE);
	gtk_table_attach (GTK_TABLE (table), props->priv->cnc_provider,
			  1, 2, 2, 3, GTK_FILL, GTK_FILL, 2, 2);

	label = gtk_label_new (_("User name:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 3, 4, GTK_FILL, GTK_FILL, 2, 2);
	props->priv->cnc_username = gtk_label_new ("");
	gtk_misc_set_alignment (GTK_MISC (props->priv->cnc_username), 0., -1);
	gtk_label_set_selectable (GTK_LABEL (props->priv->cnc_username), TRUE);
	gtk_table_attach (GTK_TABLE (table), props->priv->cnc_username,
			  1, 2, 3, 4, GTK_FILL, GTK_FILL, 2, 2);

	label = gtk_label_new (_("Status:"));
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);
	gtk_table_attach (GTK_TABLE (table), label, 0, 1, 4, 5, GTK_FILL, GTK_FILL, 2, 2);
	props->priv->cnc_status = gtk_label_new ("");
	gtk_misc_set_alignment (GTK_MISC (props->priv->cnc_status), 0., -1);
	gtk_label_set_selectable (GTK_LABEL (props->priv->cnc_status), TRUE);
	gtk_table_attach (GTK_TABLE (table), props->priv->cnc_status,
			  1, 2, 4, 5, GTK_FILL, GTK_FILL, 2, 2);

	/* create feature list */
	GtkWidget *exp;

	exp = gtk_expander_new ("");
	str = g_strdup_printf ("<b>%s:</b>", _("Features"));
	label = gtk_label_new ("");
	gtk_label_set_markup (GTK_LABEL (label), str);
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);
	g_free (str);
	gtk_expander_set_label_widget (GTK_EXPANDER (exp), label);
	
	gtk_box_pack_start (GTK_BOX (props), exp, FALSE, FALSE, 2);

	hbox = gtk_hbox_new (FALSE, 0); /* HIG */
	gtk_container_add (GTK_CONTAINER (exp), hbox);
	gtk_widget_show (hbox);
	label = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	GdaConnectionFeature f;
	props->priv->feature_list = gtk_table_new (GDA_CONNECTION_FEATURE_LAST, 2, FALSE);
	gtk_table_set_row_spacings (GTK_TABLE (props->priv->feature_list), 6);
        gtk_table_set_col_spacings (GTK_TABLE (props->priv->feature_list), 6);
	for (f = 0; f < GDA_CONNECTION_FEATURE_LAST; f++) {
		gchar *str;
		label = gtk_label_new (get_feature_name (f));
		gtk_misc_set_alignment (GTK_MISC (label), 0., -1);
		gtk_table_attach (GTK_TABLE (props->priv->feature_list), label, 0, 1, f, f+1, GTK_FILL, GTK_FILL, 0, 0);
		label = gtk_label_new ("");
		gtk_misc_set_alignment (GTK_MISC (label), 0., -1);
		gtk_table_attach (GTK_TABLE (props->priv->feature_list), label, 1, 2, f, f+1, GTK_FILL, GTK_FILL, 0, 0);
		str = g_strdup_printf ("_f%d", f);
		g_object_set_data (G_OBJECT (props->priv->feature_list), str, label);
		g_free (str);
	}
	gtk_box_pack_start (GTK_BOX (hbox), props->priv->feature_list, TRUE, TRUE, 0);

	/* transaction status */
	exp = gtk_expander_new ("");
	str = g_strdup_printf ("<b>%s:</b>", _("Transaction status"));
	label = gtk_label_new ("");
	gtk_label_set_markup (GTK_LABEL (label), str);
	gtk_misc_set_alignment (GTK_MISC (label), 0., -1);
	g_free (str);
	gtk_expander_set_label_widget (GTK_EXPANDER (exp), label);
	
	gtk_box_pack_start (GTK_BOX (props), exp, FALSE, FALSE, 2);

	hbox = gtk_hbox_new (FALSE, 0); /* HIG */
	gtk_container_add (GTK_CONTAINER (exp), hbox);
	gtk_widget_show (hbox);
	label = gtk_label_new ("    ");
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	props->priv->cnc_trans = gnome_db_transaction_status_new (NULL);
	gtk_box_pack_start (GTK_BOX (hbox), props->priv->cnc_trans, TRUE, TRUE, 0);

	gtk_widget_show_all ((GtkWidget*) props);
	gtk_widget_hide ((GtkWidget*) props);
}

static void
gnome_db_connection_properties_finalize (GObject *object)
{
	GnomeDbConnectionProperties *props = (GnomeDbConnectionProperties *) object;

	g_return_if_fail (GNOME_DB_IS_CONNECTION_PROPERTIES (props));

	/* free memory */
	if (GDA_IS_CONNECTION (props->priv->cnc)) {
		g_signal_handlers_disconnect_by_func (props->priv->cnc,
						      G_CALLBACK (cnc_status_changed_cb), props);
		g_signal_handlers_disconnect_by_func (props->priv->cnc,
						      G_CALLBACK (cnc_error_cb), props);
		g_object_unref (G_OBJECT (props->priv->cnc));
		props->priv->cnc = NULL;
	}

	g_free (props->priv);
	props->priv = NULL;

	/* chain to parent class */
	parent_class->finalize (object);
}

static void
gnome_db_connection_properties_set_property (GObject *object,
					     guint param_id,
					     const GValue *value,
					     GParamSpec *pspec)
{
	GnomeDbConnectionProperties *properties;
	GdaConnection *cnc;

	properties = GNOME_DB_CONNECTION_PROPERTIES (object);

	switch (param_id)
	{
	case PROP_CNC:
		cnc = (GdaConnection*) g_value_get_object (value);
		gnome_db_connection_properties_set_connection (properties, cnc);
		break;
	}
}

static void
gnome_db_connection_properties_get_property (GObject* object,
					     guint param_id,
					     GValue *value,
					     GParamSpec *pspec)
{
	GnomeDbConnectionProperties *properties;

	properties = GNOME_DB_CONNECTION_PROPERTIES (object);

	switch (param_id)
	{
	case PROP_CNC:
		g_value_set_object(value, G_OBJECT(properties->priv->cnc));
		break;
	}
}

GType
gnome_db_connection_properties_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0)) {
		static const GTypeInfo info = {
			sizeof (GnomeDbConnectionPropertiesClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_connection_properties_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbConnectionProperties),
			0,
			(GInstanceInitFunc) gnome_db_connection_properties_init
		};
		type = g_type_register_static (PARENT_TYPE,
					       "GnomeDbConnectionProperties",
					       &info, 0);
	}
	return type;
}

/**
 * gnome_db_connection_properties_new
 * @cnc: a #GdaConnection object.
 *
 * Create a new #GnomeDbConnectionProperties widget.
 *
 * Returns:
 */
GtkWidget *
gnome_db_connection_properties_new (GdaConnection *cnc)
{
	GnomeDbConnectionProperties *props;

	props = g_object_new (GNOME_DB_TYPE_CONNECTION_PROPERTIES,
	                      "connection", cnc, NULL);

	return GTK_WIDGET (props);
}

/**
 * gnome_db_connection_properties_get_connection
 * @props:
 *
 * Returns:
 */
GdaConnection *
gnome_db_connection_properties_get_connection (GnomeDbConnectionProperties *props)
{
	g_return_val_if_fail (GNOME_DB_IS_CONNECTION_PROPERTIES (props), NULL);
	return props->priv->cnc;
}

/**
 * gnome_db_connection_properties_set_connection
 * @props:
 * @cnc:
 *
 */
void
gnome_db_connection_properties_set_connection (GnomeDbConnectionProperties *props, GdaConnection *cnc)
{
	GdaConnectionFeature f;

	g_return_if_fail (GNOME_DB_IS_CONNECTION_PROPERTIES (props));
	g_return_if_fail (!cnc || GDA_IS_CONNECTION (cnc));

	if (GDA_IS_CONNECTION (props->priv->cnc)) {
		g_signal_handlers_disconnect_by_func (props->priv->cnc,
						      G_CALLBACK (cnc_status_changed_cb), props);
		g_signal_handlers_disconnect_by_func (props->priv->cnc,
						      G_CALLBACK (cnc_error_cb), props);
		g_object_unref (G_OBJECT (props->priv->cnc));
	}

	props->priv->cnc = cnc;
	for (f = 0; f < GDA_CONNECTION_FEATURE_LAST; f++) {
		gchar *str;
		GtkWidget *label;
		str = g_strdup_printf ("_f%d", f);
		label = g_object_get_data (G_OBJECT (props->priv->feature_list), str);
		g_free (str);
		if (cnc && gda_connection_supports_feature (cnc, f))
			gtk_label_set_text (GTK_LABEL (label), _("Yes"));
		else
			gtk_label_set_text (GTK_LABEL (label), _("No"));
	}

	if (props->priv->cnc) {
		g_object_ref (G_OBJECT (props->priv->cnc));
		refresh_widget (props);
		g_signal_connect (G_OBJECT (props->priv->cnc), "conn-closed",
				  G_CALLBACK (cnc_status_changed_cb), props);
		g_signal_connect (G_OBJECT (props->priv->cnc), "conn-opened",
				  G_CALLBACK (cnc_status_changed_cb), props);
		g_signal_connect (G_OBJECT (props->priv->cnc), "dsn-changed",
				  G_CALLBACK (cnc_status_changed_cb), props);
		g_signal_connect (G_OBJECT (props->priv->cnc), "error",
				  G_CALLBACK (cnc_error_cb), props);
	}

	g_object_set (G_OBJECT (props->priv->cnc_trans), "connection", props->priv->cnc, NULL);

	g_object_notify (G_OBJECT (props), "connection");
}
