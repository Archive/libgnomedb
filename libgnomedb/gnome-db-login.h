/* GNOME DB library
 * Copyright (C) 1999 - 2008 The GNOME Foundation.
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *      Vivien Malerba <malerba@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GNOME_DB_LOGIN_H__
#define __GNOME_DB_LOGIN_H__

#include <gtk/gtkvbox.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_LOGIN            (gnome_db_login_get_type())
#define GNOME_DB_LOGIN(obj)            (G_TYPE_CHECK_INSTANCE_CAST (obj, GNOME_DB_TYPE_LOGIN, GnomeDbLogin))
#define GNOME_DB_LOGIN_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST (klass, GNOME_DB_TYPE_LOGIN, GnomeDbLoginClass))
#define GNOME_DB_IS_LOGIN(obj)         (G_TYPE_CHECK_INSTANCE_TYPE (obj, GNOME_DB_TYPE_LOGIN))
#define GNOME_DB_IS_LOGIN_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GNOME_DB_TYPE_LOGIN))

typedef struct _GnomeDbLogin        GnomeDbLogin;
typedef struct _GnomeDbLoginClass   GnomeDbLoginClass;
typedef struct _GnomeDbLoginPrivate GnomeDbLoginPrivate;

struct _GnomeDbLogin {
	GtkVBox              parent;
	GnomeDbLoginPrivate *priv;
};

struct _GnomeDbLoginClass {
	GtkVBoxClass         parent_class;

	/* signals */
	void               (*dsn_changed) (GnomeDbLogin *login);
};

GType        gnome_db_login_get_type                 (void) G_GNUC_CONST;
GtkWidget   *gnome_db_login_new                      (const gchar *dsn);
const gchar *gnome_db_login_get_dsn                  (GnomeDbLogin *login);
void         gnome_db_login_set_dsn                  (GnomeDbLogin *login, const gchar *dsn);
const gchar *gnome_db_login_get_auth                 (GnomeDbLogin *login);
void         gnome_db_login_set_auth                 (GnomeDbLogin *login, const gchar *auth);

const gchar *gnome_db_login_get_username             (GnomeDbLogin *login);
void         gnome_db_login_set_username             (GnomeDbLogin *login, const gchar *username);
const gchar *gnome_db_login_get_password             (GnomeDbLogin *login);
void         gnome_db_login_set_password             (GnomeDbLogin *login, const gchar *password);

void         gnome_db_login_set_enable_create_button (GnomeDbLogin *login, gboolean enable);
gboolean     gnome_db_login_get_enable_create_button (GnomeDbLogin *login);
void         gnome_db_login_set_show_dsn_selector    (GnomeDbLogin *login, gboolean show);
gboolean     gnome_db_login_get_show_dsn_selector    (GnomeDbLogin *login);

G_END_DECLS

#endif
