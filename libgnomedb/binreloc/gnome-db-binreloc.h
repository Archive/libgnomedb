/* GNOME_DB common library
 * Copyright (C) 2007 The GNOME Foundation.
 *
 * AUTHORS:
 *      Vivien Malerba <malerba@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GNOME_DB_BINRELOC_H__
#define __GNOME_DB_BINRELOC_H__

#include <glib.h>

G_BEGIN_DECLS

/*
 * Locating files
 */
typedef enum {
	GNOME_DB_NO_DIR,
	GNOME_DB_BIN_DIR,
	GNOME_DB_SBIN_DIR,
	GNOME_DB_DATA_DIR,
	GNOME_DB_LOCALE_DIR,
	GNOME_DB_LIB_DIR,
	GNOME_DB_LIBEXEC_DIR,
	GNOME_DB_ETC_DIR
} GnomeDbPrefixDir;

void     gnome_db_gbr_init          (void);
gchar   *gnome_db_gbr_get_file_path (GnomeDbPrefixDir where, ...);
gchar   *gnome_db_gbr_get_icon_path (const gchar *icon_name);
gchar   *gnome_db_gbr_get_data_dir_path (const gchar *file_name);
gchar   *gnome_db_gbr_get_locale_dir_path (void);

G_END_DECLS

#endif
