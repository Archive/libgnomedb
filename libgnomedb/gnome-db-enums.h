/* gda-enums.h
 *
 * Copyright (C) 2003 - 2005 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNOME_DB_ENUMS__
#define __GNOME_DB_ENUMS__

/* enum for the different modes of action */
typedef enum {
	/* navigation modes */
	GNOME_DB_ACTION_NAVIGATION_ARROWS       = 1 << 0,
	GNOME_DB_ACTION_NAVIGATION_SCROLL       = 1 << 1,

	/* modifications */
	GNOME_DB_ACTION_MODIF_AUTO_COMMIT       = 1 << 2,
	GNOME_DB_ACTION_MODIF_COMMIT_IMMEDIATE  = 1 << 3,
	GNOME_DB_ACTION_ASK_CONFIRM_UPDATE      = 1 << 4,
	GNOME_DB_ACTION_ASK_CONFIRM_DELETE      = 1 << 5,
	GNOME_DB_ACTION_ASK_CONFIRM_INSERT      = 1 << 6,

	/* Error reporting */
	GNOME_DB_ACTION_REPORT_ERROR            = 1 << 7
} GnomeDbActionMode;

/* enum for the different possible actions */
typedef enum {
	/* actions in GnomeDbDataWidget widgets */
	GNOME_DB_ACTION_NEW_DATA,
	GNOME_DB_ACTION_WRITE_MODIFIED_DATA,
	GNOME_DB_ACTION_DELETE_SELECTED_DATA,
	GNOME_DB_ACTION_UNDELETE_SELECTED_DATA,
	GNOME_DB_ACTION_RESET_DATA,
	GNOME_DB_ACTION_MOVE_FIRST_RECORD,
	GNOME_DB_ACTION_MOVE_PREV_RECORD,
	GNOME_DB_ACTION_MOVE_NEXT_RECORD,
	GNOME_DB_ACTION_MOVE_LAST_RECORD,
	GNOME_DB_ACTION_MOVE_FIRST_CHUNCK,
        GNOME_DB_ACTION_MOVE_PREV_CHUNCK,
        GNOME_DB_ACTION_MOVE_NEXT_CHUNCK,
        GNOME_DB_ACTION_MOVE_LAST_CHUNCK
} GnomeDbAction;

/* possible predefined attribute names for gda_holder_get_attribute() or gda_column_get_attribute() */
#define GNOME_DB_ATTRIBUTE_PLUGIN "__gnomedb_attr_plugin" /* G_TYPE_STRING expected */

#endif



