/* gnome-db-data-entry.h
 *
 * Copyright (C) 2003 - 2005 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __GNOME_DB_DATA_ENTRY_H_
#define __GNOME_DB_DATA_ENTRY_H_

#include <glib-object.h>
#include "gnome-db-decl.h"
#include <libgda/libgda.h>
#include <gtk/gtk.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_DATA_ENTRY          (gnome_db_data_entry_get_type())
#define GNOME_DB_DATA_ENTRY(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, GNOME_DB_TYPE_DATA_ENTRY, GnomeDbDataEntry)
#define GNOME_DB_IS_DATA_ENTRY(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, GNOME_DB_TYPE_DATA_ENTRY)
#define GNOME_DB_DATA_ENTRY_GET_IFACE(obj) (G_TYPE_INSTANCE_GET_INTERFACE ((obj), GNOME_DB_TYPE_DATA_ENTRY, GnomeDbDataEntryIface))

/* struct for the interface */
struct _GnomeDbDataEntryIface
{
	GTypeInterface           g_iface;

	/* signals */
	void            (* contents_modified)    (GnomeDbDataEntry *de);
	void            (* contents_activated)   (GnomeDbDataEntry *de);
	void            (* status_changed)       (GnomeDbDataEntry *de);
	gboolean        (* contents_valid)       (GnomeDbDataEntry *de, GError **error);

	/* virtual table */
	void            (*set_value_type)        (GnomeDbDataEntry *de, GType type);
	GType           (*get_value_type)        (GnomeDbDataEntry *de);
	void            (*set_value)             (GnomeDbDataEntry *de, const GValue * value);
	GValue         *(*get_value)             (GnomeDbDataEntry *de);
	void            (*set_value_orig)        (GnomeDbDataEntry *de, const GValue * value);
	const GValue   *(*get_value_orig)        (GnomeDbDataEntry *de);
	void            (*set_value_default)     (GnomeDbDataEntry *de, const GValue * value);
	void            (*set_attributes)        (GnomeDbDataEntry *de, GdaValueAttribute attrs, GdaValueAttribute mask);
	GdaValueAttribute (*get_attributes)      (GnomeDbDataEntry *de);
	GdaDataHandler *(*get_handler)           (GnomeDbDataEntry *de);
	gboolean        (*expand_in_layout)      (GnomeDbDataEntry *de);
	void            (*set_editable)          (GnomeDbDataEntry *de, gboolean editable);
	void            (*grab_focus)            (GnomeDbDataEntry *de);
};




GType           gnome_db_data_entry_get_type               (void) G_GNUC_CONST;

void            gnome_db_data_entry_set_value_type         (GnomeDbDataEntry *de, GType type);
GType           gnome_db_data_entry_get_value_type         (GnomeDbDataEntry *de);

void            gnome_db_data_entry_set_value              (GnomeDbDataEntry *de, const GValue * value);
GValue         *gnome_db_data_entry_get_value              (GnomeDbDataEntry *de);
gboolean        gnome_db_data_entry_is_contents_valid      (GnomeDbDataEntry *de, GError **error);
void            gnome_db_data_entry_set_value_orig         (GnomeDbDataEntry *de, const GValue * value);
const GValue   *gnome_db_data_entry_get_value_orig         (GnomeDbDataEntry *de);
void            gnome_db_data_entry_set_current_as_orig    (GnomeDbDataEntry *de);
void            gnome_db_data_entry_set_value_default      (GnomeDbDataEntry *de, const GValue * value);

void            gnome_db_data_entry_set_attributes         (GnomeDbDataEntry *de, GdaValueAttribute attrs, GdaValueAttribute mask);
GdaValueAttribute gnome_db_data_entry_get_attributes       (GnomeDbDataEntry *de);

GdaDataHandler *gnome_db_data_entry_get_handler            (GnomeDbDataEntry *de);
gboolean        gnome_db_data_entry_expand_in_layout       (GnomeDbDataEntry *de);
void            gnome_db_data_entry_set_editable           (GnomeDbDataEntry *de, gboolean editable);
void            gnome_db_data_entry_grab_focus             (GnomeDbDataEntry *de);

G_END_DECLS

#endif
