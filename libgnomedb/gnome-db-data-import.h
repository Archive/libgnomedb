/* gnome-db-data-import.h
 *
 * Copyright (C) 2006 - 2008 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNOME_DB_DATA_IMPORT__
#define __GNOME_DB_DATA_IMPORT__

#include <gtk/gtk.h>
#include <libgda/gda-decl.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_DATA_IMPORT          (gnome_db_data_import_get_type())
#define GNOME_DB_DATA_IMPORT(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, gnome_db_data_import_get_type(), GnomeDbDataImport)
#define GNOME_DB_DATA_IMPORT_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, gnome_db_data_import_get_type (), GnomeDbDataImportClass)
#define GNOME_DB_IS_DATA_IMPORT(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, gnome_db_data_import_get_type ())


typedef struct _GnomeDbDataImport      GnomeDbDataImport;
typedef struct _GnomeDbDataImportClass GnomeDbDataImportClass;
typedef struct _GnomeDbDataImportPriv  GnomeDbDataImportPriv;

/* struct for the object's data */
struct _GnomeDbDataImport
{
	GtkVPaned              object;

	GnomeDbDataImportPriv *priv;
};

/* struct for the object's class */
struct _GnomeDbDataImportClass
{
	GtkVPanedClass         parent_class;
};

/* 
 * Generic widget's methods 
*/
GType         gnome_db_data_import_get_type     (void) G_GNUC_CONST; 
GtkWidget    *gnome_db_data_import_new          (void);
GdaDataModel *gnome_db_data_import_get_model    (GnomeDbDataImport *import);


G_END_DECLS

#endif



