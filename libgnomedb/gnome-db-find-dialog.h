/* GNOME DB library
 * Copyright (C) 1999-2002 The GNOME Foundation.
 *
 * AUTHORS:
 *      Gustavo R. Montesino <ikki_gerrard@yahoo.com.br>
 * 
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 * 
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 * 
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GNOME_DB_FIND_DIALOG_H__
#define __GNOME_DB_FIND_DIALOG_H__ 

G_BEGIN_DECLS

#define GNOME_DB_TYPE_FIND_DIALOG            (gnome_db_find_dialog_get_type())
#define GNOME_DB_FIND_DIALOG(obj)            (G_TYPE_CHECK_INSTANCE_CAST (obj, GNOME_DB_TYPE_FIND_DIALOG,  GnomeDbFindDialog))
#define GNOME_DB_FIND_DIALOG_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST (obj, GNOME_DB_TYPE_FIND_DIALOG, GnomeDbFindDialogClass))
#define GNOME_DB_IS_FIND_DIALOG(obj)         (G_TYPE_CHECK_INSTANCE_TYPE (obj, GNOME_DB_TYPE_FIND_DIALOG))
#define GNOME_DB_IS_FIND_DIALOG_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GNOME_DB_TYPE_FIND_DIALOG))

typedef struct _GnomeDbFindDialog        GnomeDbFindDialog;
typedef struct _GnomeDbFindDialogClass   GnomeDbFindDialogClass;
typedef struct _GnomeDbFindDialogPrivate GnomeDbFindDialogPrivate;


struct _GnomeDbFindDialog {
	/*< PRIVATE >*/
	GtkDialog dialog;
	GnomeDbFindDialogPrivate *priv;
};

struct _GnomeDbFindDialogClass {
	GtkDialogClass parent_class;
};

GType                  gnome_db_find_dialog_get_type (void) G_GNUC_CONST;

GtkWidget             *gnome_db_find_dialog_new (const gchar *title);
GtkWidget             *gnome_db_find_dialog_new_with_model (const gchar *title, GdaDataModel *dm);

gboolean               gnome_db_find_dialog_run (GnomeDbFindDialog *dialog);

void                   gnome_db_find_dialog_add_field (GnomeDbFindDialog *dialog, const gchar *field);
void                   gnome_db_find_dialog_add_fields_from_model (GnomeDbFindDialog *dialog, GdaDataModel *dm);

G_CONST_RETURN gchar*  gnome_db_find_dialog_get_field (GnomeDbFindDialog *dialog);
G_CONST_RETURN gchar*  gnome_db_find_dialog_get_text (GnomeDbFindDialog *dialog);

G_END_DECLS

#endif
