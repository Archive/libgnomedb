/* GNOME DB library
 * Copyright (C) 1999 - 2008 The GNOME Foundation.
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *      Vivien Malerba <malerba@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GNOME_DB_DSN_SELECTOR_H__
#define __GNOME_DB_DSN_SELECTOR_H__

#include <libgnomedb/gnome-db-combo.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_DSN_SELECTOR            (gnome_db_dsn_selector_get_type())
#define GNOME_DB_DSN_SELECTOR(obj)            (G_TYPE_CHECK_INSTANCE_CAST (obj, GNOME_DB_TYPE_DSN_SELECTOR, GnomeDbDsnSelector))
#define GNOME_DB_DSN_SELECTOR_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST (klass, GNOME_DB_TYPE_DSN_SELECTOR, GnomeDbDsnSelectorClass))
#define GNOME_DB_IS_DSN_SELECTOR(obj)         (G_TYPE_CHECK_INSTANCE_TYPE (obj, GNOME_DB_TYPE_DSN_SELECTOR))
#define GNOME_DB_IS_DSN_SELECTOR_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GNOME_DB_TYPE_DSN_SELECTOR))

typedef struct _GnomeDbDsnSelector        GnomeDbDsnSelector;
typedef struct _GnomeDbDsnSelectorClass   GnomeDbDsnSelectorClass;
typedef struct _GnomeDbDsnSelectorPrivate GnomeDbDsnSelectorPrivate;

struct _GnomeDbDsnSelector {
	GnomeDbCombo                      combo;
	GnomeDbDsnSelectorPrivate *priv;
};

struct _GnomeDbDsnSelectorClass {
	GnomeDbComboClass                 parent_class;
};

GType      gnome_db_dsn_selector_get_type (void) G_GNUC_CONST;
GtkWidget *gnome_db_dsn_selector_new      (void);
gchar     *gnome_db_dsn_selector_get_dsn  (GnomeDbDsnSelector *selector);
void       gnome_db_dsn_selector_set_dsn  (GnomeDbDsnSelector *selector, const gchar *dsn);

G_END_DECLS

#endif
