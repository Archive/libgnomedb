/* gnome-db-tools.h
 *
 * Copyright (C) 2005 - 2008 Vivien Malerba <malerba@gnome-db.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GNOME_DB_TOOLS__
#define __GNOME_DB_TOOLS__

#include <gtk/gtkcellrenderer.h>
#include <libgda/gda-value.h>
#include "gnome-db-decl.h"

G_BEGIN_DECLS

void              gnome_db_util_init_plugins      ();
GnomeDbDataEntry *gnome_db_util_new_data_entry    (GType type, const gchar *plugin_name);
GtkCellRenderer  *gnome_db_util_new_cell_renderer (GType type, const gchar *plugin_name);

G_END_DECLS

#endif
