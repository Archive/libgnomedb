/* gnome-db-transaction-status.h
 *
 * Copyright (C) 2006 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNOME_DB_TRANSACTION_STATUS__
#define __GNOME_DB_TRANSACTION_STATUS__

#include <gtk/gtk.h>
#include <libgda/libgda.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_TRANSACTION_STATUS          (gnome_db_transaction_status_get_type())
#define GNOME_DB_TRANSACTION_STATUS(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, gnome_db_transaction_status_get_type(), GnomeDbTransactionStatus)
#define GNOME_DB_TRANSACTION_STATUS_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, gnome_db_transaction_status_get_type (), GnomeDbTransactionStatusClass)
#define GNOME_DB_IS_TRANSACTION_STATUS(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, gnome_db_transaction_status_get_type ())

typedef struct _GnomeDbTransactionStatus      GnomeDbTransactionStatus;
typedef struct _GnomeDbTransactionStatusClass GnomeDbTransactionStatusClass;
typedef struct _GnomeDbTransactionStatusPriv  GnomeDbTransactionStatusPriv;

/* struct for the object's data */
struct _GnomeDbTransactionStatus
{
	GtkVBox                       object;
	GnomeDbTransactionStatusPriv *priv;
};

/* struct for the object's class */
struct _GnomeDbTransactionStatusClass
{
	GtkVBoxClass                  parent_class;
};

/* 
 * Generic widget's methods 
*/
GType             gnome_db_transaction_status_get_type                 (void) G_GNUC_CONST;
GtkWidget        *gnome_db_transaction_status_new                      (GdaConnection *cnc);

G_END_DECLS

#endif
