/* GNOME DB library
 * Copyright (C) 1999 - 2002 The GNOME Foundation.
 *
 * AUTHORS:
 * 	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GNOME_DB_STOCK_H__
#define __GNOME_DB_STOCK_H__

#include <gdk-pixbuf/gdk-pixbuf.h>

G_BEGIN_DECLS

#define GNOME_DB_STOCK_AGGREGATES         "gnome-db-aggregates"
#define GNOME_DB_STOCK_COMMIT             "gnome-db-commit"
#define GNOME_DB_STOCK_CONNECT            "gnome-db-connect"
#define GNOME_DB_STOCK_DISCONNECT         "gnome-db-disconnect"
#define GNOME_DB_STOCK_DATABASE           "gnome-db-database"
#define GNOME_DB_STOCK_DESIGNER           "gnome-db-designer"
#define GNOME_DB_STOCK_NO_TRANSACTION     "gnome-db-no-transaction"
#define GNOME_DB_STOCK_PROCEDURES         "gnome-db-procedures"
#define GNOME_DB_STOCK_QUERY              "gnome-db-query"
#define GNOME_DB_STOCK_ROLLBACK           "gnome-db-rollback"
#define GNOME_DB_STOCK_SEQUENCES          "gnome-db-sequences"
#define GNOME_DB_STOCK_SQL                "gnome-db-sql"
#define GNOME_DB_STOCK_TABLES             "gnome-db-tables"
#define GNOME_DB_STOCK_TYPES              "gnome-db-types"
#define GNOME_DB_STOCK_WITHIN_TRANSACTION "gnome-db-within-transaction"
#define GNOME_DB_STOCK_RELATIONS          "gnome-db-relations"
#define GNOME_DB_STOCK_SQL_CONSOLE        "gnome-db-console"

GdkPixbuf *gnome_db_stock_get_icon_pixbuf (const gchar *stock_id);
GdkPixbuf *gnome_db_stock_get_icon_pixbuf_file (const gchar *pixmapfile);
gchar     *gnome_db_stock_get_icon_path (const gchar *stock_id);


G_END_DECLS

#endif
