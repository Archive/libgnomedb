/* gnome-db-data-widget.c
 *
 * Copyright (C) 2004 - 2007 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <string.h>
#include <libgda/libgda.h>
#include "gnome-db-data-widget.h"
#include  <glib/gi18n-lib.h>

/* signals */
enum
{
        PROXY_CHANGED,
        ITER_CHANGED,
        LAST_SIGNAL
};

static gint gnome_db_data_widget_signals[LAST_SIGNAL] = { 0, 0 };

static void gnome_db_data_widget_iface_init (gpointer g_class);

GType
gnome_db_data_widget_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0)) {
		static const GTypeInfo info = {
			sizeof (GnomeDbDataWidgetIface),
			(GBaseInitFunc) gnome_db_data_widget_iface_init,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) NULL,
			NULL,
			NULL,
			0,
			0,
			(GInstanceInitFunc) NULL
		};
		
		type = g_type_register_static (G_TYPE_INTERFACE, "GnomeDbDataWidget", &info, 0);
		g_type_interface_add_prerequisite (type, G_TYPE_OBJECT);
	}
	return type;
}


static void
gnome_db_data_widget_iface_init (gpointer g_class)
{
	static gboolean initialized = FALSE;

	if (! initialized) {
		gnome_db_data_widget_signals[PROXY_CHANGED] = 
			g_signal_new ("proxy_changed",
                                      GNOME_DB_TYPE_DATA_WIDGET,
                                      G_SIGNAL_RUN_FIRST,
                                      G_STRUCT_OFFSET (GnomeDbDataWidgetIface, proxy_changed),
                                      NULL, NULL,
                                      g_cclosure_marshal_VOID__OBJECT, G_TYPE_NONE,
                                      1, GDA_TYPE_DATA_PROXY);
		gnome_db_data_widget_signals[ITER_CHANGED] = 
			g_signal_new ("iter_changed",
                                      GNOME_DB_TYPE_DATA_WIDGET,
                                      G_SIGNAL_RUN_FIRST,
                                      G_STRUCT_OFFSET (GnomeDbDataWidgetIface, iter_changed),
                                      NULL, NULL,
                                      g_cclosure_marshal_VOID__POINTER, G_TYPE_NONE,
                                      1, G_TYPE_POINTER);

		initialized = TRUE;
	}
}

/**
 * gnome_db_data_widget_get_proxy
 * @iface: an object which implements the #GnomeDbDataWidget interface
 *
 * Get a pointer to the #GdaDataProxy being used by @iface
 *
 * Returns: a #GdaDataProxy pointer
 */
GdaDataProxy *
gnome_db_data_widget_get_proxy (GnomeDbDataWidget *iface)
{
	g_return_val_if_fail (GNOME_DB_IS_DATA_WIDGET (iface), NULL);
	
	if (GNOME_DB_DATA_WIDGET_GET_IFACE (iface)->get_proxy)
		return (GNOME_DB_DATA_WIDGET_GET_IFACE (iface)->get_proxy) (iface);
	else
		return NULL;
}

/**
 * gnome_db_data_widget_column_show
 * @iface: an object which implements the #GnomeDbDataWidget interface
 * @column: column number to show
 *
 * Shows the data at @column in the data model @iface operates on
 */
void
gnome_db_data_widget_column_show (GnomeDbDataWidget *iface, gint column)
{
	g_return_if_fail (GNOME_DB_IS_DATA_WIDGET (iface));

	if (GNOME_DB_DATA_WIDGET_GET_IFACE (iface)->col_set_show)
		(GNOME_DB_DATA_WIDGET_GET_IFACE (iface)->col_set_show) (iface, column, TRUE);
}


/**
 * gnome_db_data_widget_column_hide
 * @iface: an object which implements the #GnomeDbDataWidget interface
 * @column: column number to hide
 *
 * Hides the data at @column in the data model @iface operates on
 */
void
gnome_db_data_widget_column_hide (GnomeDbDataWidget *iface, gint column)
{
	g_return_if_fail (GNOME_DB_IS_DATA_WIDGET (iface));

	if (GNOME_DB_DATA_WIDGET_GET_IFACE (iface)->col_set_show)
		(GNOME_DB_DATA_WIDGET_GET_IFACE (iface)->col_set_show) (iface, column, FALSE);
}


/**
 * gnome_db_data_widget_column_set_editable
 * @iface: an object which implements the #GnomeDbDataWidget interface
 * @column: column number of the data
 * @editable:
 *
 * Sets if the data entry in the @iface widget at @column (in the data model @iface operates on) can be edited or not.
 */
void 
gnome_db_data_widget_column_set_editable (GnomeDbDataWidget *iface, gint column, gboolean editable)
{
	g_return_if_fail (GNOME_DB_IS_DATA_WIDGET (iface));

	if (GNOME_DB_DATA_WIDGET_GET_IFACE (iface)->set_column_editable)
		(GNOME_DB_DATA_WIDGET_GET_IFACE (iface)->set_column_editable) (iface, column, editable);	
}

/**
 * gnome_db_data_widget_column_show_actions
 * @iface: an object which implements the #GnomeDbDataWidget interface
 * @column: column number of the data, or -1 to apply the setting to all the columns
 * @show_actions:
 * 
 * Sets if the data entry in the @iface widget at @column (in the data model @iface operates on) must show its
 * actions menu or not.
 */
void
gnome_db_data_widget_column_show_actions (GnomeDbDataWidget *iface, gint column, gboolean show_actions)
{
	g_return_if_fail (GNOME_DB_IS_DATA_WIDGET (iface));

	if (GNOME_DB_DATA_WIDGET_GET_IFACE (iface)->show_column_actions)
		(GNOME_DB_DATA_WIDGET_GET_IFACE (iface)->show_column_actions) (iface, column, show_actions);
}

/**
 * gnome_db_data_widget_get_actions_group
 * @iface: an object which implements the #GnomeDbDataWidget interface
 *
 * Each widget imlplementing the #GnomeDbDataWidget interface provides actions. Actions can be triggered
 * using the gnome_db_data_widget_perform_action() method, but using this method allows for the creation of
 * toolbars, menus, etc calling these actions.
 *
 * The actions are among: 
 * <itemizedlist><listitem><para>Data edition actions: "ActionNew", "ActionCommit", 
 *    "ActionDelete, "ActionUndelete, "ActionReset", </para></listitem>
 * <listitem><para>Record by record moving: "ActionFirstRecord", "ActionPrevRecord", 
 *    "ActionNextRecord", "ActionLastRecord",</para></listitem>
 * <listitem><para>Chuncks of records moving: "ActionFirstChunck", "ActionPrevChunck", 
 *     "ActionNextChunck", "ActionLastChunck".</para></listitem>
 * <listitem><para>Filtering: "ActionFilter"</para></listitem></itemizedlist>
 * 
 * Returns: the #GtkActionGroup with all the possible actions on the widget.
 */
GtkActionGroup *
gnome_db_data_widget_get_actions_group (GnomeDbDataWidget *iface)
{
	g_return_val_if_fail (GNOME_DB_IS_DATA_WIDGET (iface), NULL);

	if (GNOME_DB_DATA_WIDGET_GET_IFACE (iface)->get_actions_group)
		return (GNOME_DB_DATA_WIDGET_GET_IFACE (iface)->get_actions_group) (iface);
	return NULL;
}

/**
 * gnome_db_data_widget_perform_action
 * @iface: an object which implements the #GnomeDbDataWidget interface
 * @action: a #GnomeDbAction action
 *
 * Forces the widget to perform the selected @action, as if the user
 * had pressed on the corresponding action button in the @iface widget,
 * if the corresponding action is possible and if the @iface widget
 * supports the action.
 */
void
gnome_db_data_widget_perform_action (GnomeDbDataWidget *iface, GnomeDbAction action)
{
	gchar *action_name = NULL;
	GtkActionGroup *group;
	GtkAction *gtkaction;

	g_return_if_fail (GNOME_DB_IS_DATA_WIDGET (iface));
	group = gnome_db_data_widget_get_actions_group (iface);
	if (!group) {
		g_warning ("Object class %s does not support the gnome_db_data_widget_get_actions_group() method",
			   G_OBJECT_TYPE_NAME (iface));
		return;
	}
	
	switch (action) {
	case GNOME_DB_ACTION_NEW_DATA:
		action_name = "ActionNew";
		break;
	case GNOME_DB_ACTION_WRITE_MODIFIED_DATA:
		action_name = "ActionCommit";
		break;
        case GNOME_DB_ACTION_DELETE_SELECTED_DATA:
		action_name = "ActionDelete";
		break;
        case GNOME_DB_ACTION_UNDELETE_SELECTED_DATA:
		action_name = "ActionUndelete";
		break;
        case GNOME_DB_ACTION_RESET_DATA:
		action_name = "ActionReset";
		break;
        case GNOME_DB_ACTION_MOVE_FIRST_RECORD:
		action_name = "ActionFirstRecord";
		break;
        case GNOME_DB_ACTION_MOVE_PREV_RECORD:
		action_name = "ActionPrevRecord";
		break;
        case GNOME_DB_ACTION_MOVE_NEXT_RECORD:
		action_name = "ActionNextRecord";
		break;
        case GNOME_DB_ACTION_MOVE_LAST_RECORD:
		action_name = "ActionLastRecord";
		break;
        case GNOME_DB_ACTION_MOVE_FIRST_CHUNCK:
		action_name = "ActionFirstChunck";
		break;
        case GNOME_DB_ACTION_MOVE_PREV_CHUNCK:
		action_name = "ActionPrevChunck";
		break;
        case GNOME_DB_ACTION_MOVE_NEXT_CHUNCK:
		action_name = "ActionNextChunck";
		break;
        case GNOME_DB_ACTION_MOVE_LAST_CHUNCK:
		action_name = "ActionLastChunck";
		break;
	default:
		g_assert_not_reached ();
	}

	gtkaction = gtk_action_group_get_action (group, action_name);
	if (gtkaction)
		gtk_action_activate (gtkaction);
}

/**
 * gnome_db_data_widget_get_current_data
 * @iface: an object which implements the #GnomeDbDataWidget interface
 *
 * Get the #GdaDataModelIter object which contains all the parameters which in turn contain the actual
 * data stored in @iface. When the user changes what's displayed or what's selected (depending on the
 * actual widget) in @iface, then the parameter's values change as well.
 *
 * Returns: the #GdaSet object for data (not a new object)
 */
GdaDataModelIter *
gnome_db_data_widget_get_current_data (GnomeDbDataWidget *iface)
{
	g_return_val_if_fail (GNOME_DB_IS_DATA_WIDGET (iface), NULL);

	if (GNOME_DB_DATA_WIDGET_GET_IFACE (iface)->get_data_set)
		return (GNOME_DB_DATA_WIDGET_GET_IFACE (iface)->get_data_set) (iface);
	return NULL;
}

/**
 * gnome_db_data_widget_get_gda_model
 * @iface: an object which implements the #GnomeDbDataWidget interface
 *
 * Get the current #GdaDataModel used by @iface
 *
 * Returns: the #GdaDataModel, or %NULL if there is none
 */
GdaDataModel *
gnome_db_data_widget_get_gda_model (GnomeDbDataWidget *iface)
{
	g_return_val_if_fail (GNOME_DB_IS_DATA_WIDGET (iface), NULL);

	if (GNOME_DB_DATA_WIDGET_GET_IFACE (iface)->get_gda_model)
		return (GNOME_DB_DATA_WIDGET_GET_IFACE (iface)->get_gda_model) (iface);
	return NULL;
}

/**
 * gnome_db_data_widget_set_gda_model
 * @iface: an object which implements the #GnomeDbDataWidget interface
 * @model: a valid #GdaDataModel
 *
 * Sets the data model which is used by @iface.
 */
void
gnome_db_data_widget_set_gda_model (GnomeDbDataWidget *iface, GdaDataModel *model)
{
	g_return_if_fail (GNOME_DB_IS_DATA_WIDGET (iface));
	g_return_if_fail (model && GDA_IS_DATA_MODEL (model));
	
	if (GNOME_DB_DATA_WIDGET_GET_IFACE (iface)->set_gda_model)
		(GNOME_DB_DATA_WIDGET_GET_IFACE (iface)->set_gda_model) (iface, model);
}

/**
 * gnome_db_data_widget_set_write_mode
 * @iface: an object which implements the #GnomeDbDataWidget interface
 * @mode:
 *
 * Specifies the way the modifications stored in the #GdaDataProxy used internally by @iface are written back to
 * the #GdaDataModel which holds the data displayed in @iface.
 *
 * Returns: TRUE if the proposed mode has been taken into account
 */
gboolean
gnome_db_data_widget_set_write_mode (GnomeDbDataWidget *iface, GnomeDbDataWidgetWriteMode mode)
{
	g_return_val_if_fail (GNOME_DB_IS_DATA_WIDGET (iface), FALSE);
	
	if (GNOME_DB_DATA_WIDGET_GET_IFACE (iface)->set_write_mode)
		return (GNOME_DB_DATA_WIDGET_GET_IFACE (iface)->set_write_mode) (iface, mode);
	else 
		return FALSE;
}

/**
 * gnome_db_data_widget_get_write_mode
 * @iface: an object which implements the #GnomeDbDataWidget interface
 *
 * Get the way the modifications stored in the #GdaDataProxy used internally by @iface are written back to
 * the #GdaDataModel which holds the data displayed in @iface.
 *
 * Returns: the write mode used by @iface
 */
GnomeDbDataWidgetWriteMode
gnome_db_data_widget_get_write_mode (GnomeDbDataWidget *iface)
{
	g_return_val_if_fail (GNOME_DB_IS_DATA_WIDGET (iface), GNOME_DB_DATA_WIDGET_WRITE_ON_DEMAND);
	
	if (GNOME_DB_DATA_WIDGET_GET_IFACE (iface)->get_write_mode)
		return (GNOME_DB_DATA_WIDGET_GET_IFACE (iface)->get_write_mode) (iface);
	else 
		return GNOME_DB_DATA_WIDGET_WRITE_ON_DEMAND;
}

/**
 * gnome_db_data_widget_set_data_layout
 * @iface: an object which implements the #GnomeDbDataWidget interface
 * @string: xml string
 *
 * Sets a data layout according an XML string in the @iface widget .
 */
void 
gnome_db_data_widget_set_data_layout (GnomeDbDataWidget *iface, const gpointer data)
{
	g_return_if_fail (GNOME_DB_IS_DATA_WIDGET (iface));

	if (GNOME_DB_DATA_WIDGET_GET_IFACE (iface)->set_data_layout)
		(GNOME_DB_DATA_WIDGET_GET_IFACE (iface)->set_data_layout) (iface, data);	
}
