/* gnome-db-form.h
 *
 * Copyright (C) 2002 - 2006 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNOME_DB_FORM__
#define __GNOME_DB_FORM__

#include <gtk/gtk.h>
#include <libgda/gda-decl.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_FORM          (gnome_db_form_get_type())
#define GNOME_DB_FORM(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, gnome_db_form_get_type(), GnomeDbForm)
#define GNOME_DB_FORM_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, gnome_db_form_get_type (), GnomeDbFormClass)
#define GNOME_DB_IS_FORM(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, gnome_db_form_get_type ())


typedef struct _GnomeDbForm      GnomeDbForm;
typedef struct _GnomeDbFormClass GnomeDbFormClass;
typedef struct _GnomeDbFormPriv  GnomeDbFormPriv;

/* struct for the object's data */
struct _GnomeDbForm
{
	GtkVBox             object;

	GnomeDbFormPriv     *priv;
};

/* struct for the object's class */
struct _GnomeDbFormClass
{
	GtkVBoxClass       parent_class;
};

/* 
 * Generic widget's methods 
 */
GType             gnome_db_form_get_type            (void) G_GNUC_CONST;

GtkWidget        *gnome_db_form_new                 (GdaDataModel *model);

G_END_DECLS

#endif



