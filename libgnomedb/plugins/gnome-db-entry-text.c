/* gnome-db-entry-text.c
 *
 * Copyright (C) 2003 - 2005 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "gnome-db-entry-text.h"
#include <libgda/gda-data-handler.h>

/* 
 * Main static functions 
 */
static void gnome_db_entry_text_class_init (GnomeDbEntryTextClass * class);
static void gnome_db_entry_text_init (GnomeDbEntryText * srv);
static void gnome_db_entry_text_dispose (GObject   * object);
static void gnome_db_entry_text_finalize (GObject   * object);

/* virtual functions */
static GtkWidget *create_entry (GnomeDbEntryWrapper *mgwrap);
static void       real_set_value (GnomeDbEntryWrapper *mgwrap, const GValue *value);
static GValue    *real_get_value (GnomeDbEntryWrapper *mgwrap);
static void       connect_signals(GnomeDbEntryWrapper *mgwrap, GCallback modify_cb, GCallback activate_cb);
static gboolean   expand_in_layout (GnomeDbEntryWrapper *mgwrap);

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

/* private structure */
struct _GnomeDbEntryTextPrivate
{
	GtkTextBuffer *buffer;
	GtkWidget     *view;
};


GType
gnome_db_entry_text_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0)) {
		static const GTypeInfo info = {
			sizeof (GnomeDbEntryTextClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_entry_text_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbEntryText),
			0,
			(GInstanceInitFunc) gnome_db_entry_text_init
		};
		
		type = g_type_register_static (GNOME_DB_TYPE_ENTRY_WRAPPER, "GnomeDbEntryText", &info, 0);
	}
	return type;
}

static void
gnome_db_entry_text_class_init (GnomeDbEntryTextClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->dispose = gnome_db_entry_text_dispose;
	object_class->finalize = gnome_db_entry_text_finalize;

	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->create_entry = create_entry;
	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->real_set_value = real_set_value;
	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->real_get_value = real_get_value;
	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->connect_signals = connect_signals;
	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->expand_in_layout = expand_in_layout;
}

static void
gnome_db_entry_text_init (GnomeDbEntryText * gnome_db_entry_text)
{
	gnome_db_entry_text->priv = g_new0 (GnomeDbEntryTextPrivate, 1);
	gnome_db_entry_text->priv->buffer = NULL;
	gnome_db_entry_text->priv->view = NULL;
}

/**
 * gnome_db_entry_text_new
 * @dh: the data handler to be used by the new widget
 * @type: the requested data type (compatible with @dh)
 *
 * Creates a new widget which is mainly a GtkEntry
 *
 * Returns: the new widget
 */
GtkWidget *
gnome_db_entry_text_new (GdaDataHandler *dh, GType type)
{
	GObject *obj;
	GnomeDbEntryText *mgtxt;

	g_return_val_if_fail (dh && GDA_IS_DATA_HANDLER (dh), NULL);
	g_return_val_if_fail (type != G_TYPE_INVALID, NULL);
	g_return_val_if_fail (gda_data_handler_accepts_g_type (dh, type), NULL);

	obj = g_object_new (GNOME_DB_TYPE_ENTRY_TEXT, "handler", dh, NULL);
	mgtxt = GNOME_DB_ENTRY_TEXT (obj);
	gnome_db_data_entry_set_value_type (GNOME_DB_DATA_ENTRY (mgtxt), type);

	return GTK_WIDGET (obj);
}


static void
gnome_db_entry_text_dispose (GObject   * object)
{
	GnomeDbEntryText *gnome_db_entry_text;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOME_DB_IS_ENTRY_TEXT (object));

	gnome_db_entry_text = GNOME_DB_ENTRY_TEXT (object);
	if (gnome_db_entry_text->priv) {

	}

	/* parent class */
	parent_class->dispose (object);
}

static void
gnome_db_entry_text_finalize (GObject   * object)
{
	GnomeDbEntryText *gnome_db_entry_text;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOME_DB_IS_ENTRY_TEXT (object));

	gnome_db_entry_text = GNOME_DB_ENTRY_TEXT (object);
	if (gnome_db_entry_text->priv) {

		g_free (gnome_db_entry_text->priv);
		gnome_db_entry_text->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}

static GtkWidget *
create_entry (GnomeDbEntryWrapper *mgwrap)
{
	GnomeDbEntryText *mgtxt;
	GtkWidget *sw;

	g_return_val_if_fail (mgwrap && GNOME_DB_IS_ENTRY_TEXT (mgwrap), NULL);
	mgtxt = GNOME_DB_ENTRY_TEXT (mgwrap);
	g_return_val_if_fail (mgtxt->priv, NULL);

	mgtxt->priv->view = gtk_text_view_new ();
	mgtxt->priv->buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW (mgtxt->priv->view));
	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (sw), GTK_SHADOW_IN);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
					GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_container_add (GTK_CONTAINER (sw), mgtxt->priv->view);
	gtk_widget_show (mgtxt->priv->view);
	
	return sw;
}

static void
real_set_value (GnomeDbEntryWrapper *mgwrap, const GValue *value)
{
	GnomeDbEntryText *mgtxt;

	g_return_if_fail (mgwrap && GNOME_DB_IS_ENTRY_TEXT (mgwrap));
	mgtxt = GNOME_DB_ENTRY_TEXT (mgwrap);
	g_return_if_fail (mgtxt->priv);

	gtk_text_buffer_set_text (mgtxt->priv->buffer, "", -1);
	if (value) {
		if (! gda_value_is_null ((GValue *) value)) {
			GdaDataHandler *dh;		
			gchar *str;

			dh = gnome_db_data_entry_get_handler (GNOME_DB_DATA_ENTRY (mgwrap));
			str = gda_data_handler_get_str_from_value (dh, value);
			if (str) {
				gtk_text_buffer_set_text (mgtxt->priv->buffer, str, -1);
				g_free (str);
			}
		}
	}
}

static GValue *
real_get_value (GnomeDbEntryWrapper *mgwrap)
{
	GValue *value;
	GnomeDbEntryText *mgtxt;
	GdaDataHandler *dh;
	gchar *str;
	GtkTextIter start, end;

	g_return_val_if_fail (mgwrap && GNOME_DB_IS_ENTRY_TEXT (mgwrap), NULL);
	mgtxt = GNOME_DB_ENTRY_TEXT (mgwrap);
	g_return_val_if_fail (mgtxt->priv, NULL);

	dh = gnome_db_data_entry_get_handler (GNOME_DB_DATA_ENTRY (mgwrap));
	gtk_text_buffer_get_start_iter (mgtxt->priv->buffer, &start);
	gtk_text_buffer_get_end_iter (mgtxt->priv->buffer, &end);
	str = gtk_text_buffer_get_text (mgtxt->priv->buffer, &start, &end, FALSE);
	value = gda_data_handler_get_value_from_str (dh, str, 
						     gnome_db_data_entry_get_value_type (GNOME_DB_DATA_ENTRY (mgwrap)));
	g_free (str);
	if (!value) {
		/* in case the gda_data_handler_get_value_from_sql() returned an error because
		   the contents of the GtkEntry cannot be interpreted as a GValue */
		value = gda_value_new_null ();
	}

	return value;
}

typedef void (*Callback2) (gpointer, gpointer);
static gboolean
focus_out_cb (GtkWidget *widget, GdkEventFocus *event, GnomeDbEntryText *mgtxt)
{
	GCallback activate_cb;
	activate_cb = g_object_get_data (G_OBJECT (widget), "_activate_cb");
	g_assert (activate_cb);
	((Callback2)activate_cb) (widget, mgtxt);

	return FALSE;
}

static void
connect_signals(GnomeDbEntryWrapper *mgwrap, GCallback modify_cb, GCallback activate_cb)
{
	GnomeDbEntryText *mgtxt;

	g_return_if_fail (mgwrap && GNOME_DB_IS_ENTRY_TEXT (mgwrap));
	mgtxt = GNOME_DB_ENTRY_TEXT (mgwrap);
	g_return_if_fail (mgtxt->priv);

	g_object_set_data (G_OBJECT (mgtxt->priv->view), "_activate_cb", activate_cb);
	g_signal_connect (G_OBJECT (mgtxt->priv->buffer), "changed",
			  modify_cb, mgwrap);
	g_signal_connect (G_OBJECT (mgtxt->priv->view), "focus-out-event",
			  G_CALLBACK (focus_out_cb), mgtxt);
	/* FIXME: how does the user "activates" the GtkTextView widget ? */
}

static gboolean
expand_in_layout (GnomeDbEntryWrapper *mgwrap)
{
	return TRUE;
}
