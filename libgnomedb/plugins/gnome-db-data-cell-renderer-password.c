/* gnome-db-data-cell-renderer-password.c
 *
 * Copyright (C) 2003 - 2006 Vivien Malerba <malerba@gnome-db.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <glib/gi18n-lib.h>
#include <string.h>
#include <stdlib.h>
#include <libgda/libgda.h>
#include "gnome-db-data-cell-renderer-password.h"
#include <libgnomedb/marshal.h>
#include "gnome-db-entry-password.h"
#include <libgda/gda-enum-types.h>

static void gnome_db_data_cell_renderer_password_init       (GnomeDbDataCellRendererPassword      *celltext);
static void gnome_db_data_cell_renderer_password_class_init (GnomeDbDataCellRendererPasswordClass *class);
static void gnome_db_data_cell_renderer_password_dispose    (GObject *object);
static void gnome_db_data_cell_renderer_password_finalize   (GObject *object);

static void gnome_db_data_cell_renderer_password_get_property  (GObject *object,
								guint param_id,
								GValue *value,
								GParamSpec *pspec);
static void gnome_db_data_cell_renderer_password_set_property  (GObject *object,
								guint param_id,
								const GValue *value,
								GParamSpec *pspec);
static void gnome_db_data_cell_renderer_password_get_size   (GtkCellRenderer          *cell,
							     GtkWidget                *widget,
							     GdkRectangle             *cell_area,
							     gint                     *x_offset,
							     gint                     *y_offset,
							     gint                     *width,
							     gint                     *height);
static void gnome_db_data_cell_renderer_password_render     (GtkCellRenderer          *cell,
							     GdkWindow                *window,
							     GtkWidget                *widget,
							     GdkRectangle             *background_area,
							     GdkRectangle             *cell_area,
							     GdkRectangle             *expose_area,
							     GtkCellRendererState      flags);

static GtkCellEditable *gnome_db_data_cell_renderer_password_start_editing (GtkCellRenderer      *cell,
									    GdkEvent             *event,
									    GtkWidget            *widget,
									    const gchar          *path,
									    GdkRectangle         *background_area,
									    GdkRectangle         *cell_area,
									    GtkCellRendererState  flags);

enum {
	CHANGED,
	LAST_SIGNAL
};

enum {
	PROP_0,
	PROP_VALUE,
	PROP_VALUE_ATTRIBUTES,
	PROP_TO_BE_DELETED,
	PROP_DATA_HANDLER,
	PROP_TYPE
};

struct _GnomeDbDataCellRendererPasswordPrivate
{
	GdaDataHandler *dh;
	GType           type;
	gboolean        type_forced; /* TRUE if ->type has been forced by a value and changed from what was specified */
	GValue         *value;
	gboolean        to_be_deleted;
	gchar          *options;
};

typedef struct 
{
	/* text renderer */
	gulong focus_out_id;
} GnomeDbDataCellRendererPasswordInfo;
#define GNOME_DB_DATA_CELL_RENDERER_PASSWORD_INFO_KEY "gnome_db_data_cell_renderer_password_info_key"



static GObjectClass *parent_class = NULL;
static guint text_cell_renderer_password_signals [LAST_SIGNAL];

#define GNOME_DB_DATA_CELL_RENDERER_PASSWORD_PATH "gnome_db_data_cell_renderer_password_path"

GType
gnome_db_data_cell_renderer_password_get_type (void)
{
	static GType cell_text_type = 0;

	if (!cell_text_type) {
		static const GTypeInfo cell_text_info =	{
			sizeof (GnomeDbDataCellRendererPasswordClass),
			NULL,		/* base_init */
			NULL,		/* base_finalize */
			(GClassInitFunc) gnome_db_data_cell_renderer_password_class_init,
			NULL,		/* class_finalize */
			NULL,		/* class_data */
			sizeof (GnomeDbDataCellRendererPassword),
			0,              /* n_preallocs */
			(GInstanceInitFunc) gnome_db_data_cell_renderer_password_init,
		};
		
		cell_text_type =
			g_type_register_static (GTK_TYPE_CELL_RENDERER_TEXT, "GnomeDbDataCellRendererPassword",
						&cell_text_info, 0);
	}

	return cell_text_type;
}

static void
gnome_db_data_cell_renderer_password_init (GnomeDbDataCellRendererPassword *datacell)
{
	datacell->priv = g_new0 (GnomeDbDataCellRendererPasswordPrivate, 1);
	datacell->priv->dh = NULL;
	datacell->priv->type = G_TYPE_INVALID;
	datacell->priv->type_forced = FALSE;
	datacell->priv->value = NULL;
	datacell->priv->options = NULL;

	GTK_CELL_RENDERER (datacell)->xalign = 0.0;
	GTK_CELL_RENDERER (datacell)->yalign = 0.5;
	GTK_CELL_RENDERER (datacell)->xpad = 2;
	GTK_CELL_RENDERER (datacell)->ypad = 2;
}

static void
gnome_db_data_cell_renderer_password_class_init (GnomeDbDataCellRendererPasswordClass *class)
{
	GObjectClass *object_class = G_OBJECT_CLASS (class);
	GtkCellRendererClass *cell_class = GTK_CELL_RENDERER_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

    	object_class->dispose = gnome_db_data_cell_renderer_password_dispose;
	object_class->finalize = gnome_db_data_cell_renderer_password_finalize;

	object_class->get_property = gnome_db_data_cell_renderer_password_get_property;
	object_class->set_property = gnome_db_data_cell_renderer_password_set_property;

	cell_class->get_size = gnome_db_data_cell_renderer_password_get_size;
	cell_class->render = gnome_db_data_cell_renderer_password_render;
	cell_class->start_editing = gnome_db_data_cell_renderer_password_start_editing;
  
	g_object_class_install_property (object_class,
					 PROP_VALUE,
					 g_param_spec_pointer ("value",
							       _("Value"),
							       _("GValue to render"),
							       G_PARAM_WRITABLE));
  
	g_object_class_install_property (object_class,
					 PROP_VALUE_ATTRIBUTES,
					 g_param_spec_flags ("value_attributes", NULL, NULL, GDA_TYPE_VALUE_ATTRIBUTE,
							     GDA_VALUE_ATTR_NONE, G_PARAM_READWRITE));

	g_object_class_install_property (object_class,
					 PROP_TO_BE_DELETED,
					 g_param_spec_boolean ("to_be_deleted", NULL, NULL, FALSE,
                                                               G_PARAM_WRITABLE));
	g_object_class_install_property(object_class,
					PROP_DATA_HANDLER,
					g_param_spec_object("data_handler", NULL, NULL, GDA_TYPE_DATA_HANDLER,
							    G_PARAM_WRITABLE|G_PARAM_CONSTRUCT_ONLY));
	g_object_class_install_property(object_class,
					PROP_TYPE,
					g_param_spec_gtype("type", NULL, NULL, G_TYPE_NONE,
							   G_PARAM_WRITABLE|G_PARAM_CONSTRUCT_ONLY));
  
	text_cell_renderer_password_signals [CHANGED] =
		g_signal_new ("changed",
			      G_OBJECT_CLASS_TYPE (object_class),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (GnomeDbDataCellRendererPasswordClass, changed),
			      NULL, NULL,
			      gnome_db_marshal_VOID__STRING_BOXED,
			      G_TYPE_NONE, 2,
			      G_TYPE_STRING,
			      G_TYPE_VALUE);

}

static void
gnome_db_data_cell_renderer_password_dispose (GObject *object)
{
	GnomeDbDataCellRendererPassword *datacell = GNOME_DB_DATA_CELL_RENDERER_PASSWORD (object);

	if (datacell->priv->dh) {
		g_object_unref (G_OBJECT (datacell->priv->dh));
		datacell->priv->dh = NULL;
	}

	/* parent class */
	parent_class->dispose (object);
}

static void
gnome_db_data_cell_renderer_password_finalize (GObject *object)
{
	GnomeDbDataCellRendererPassword *datacell = GNOME_DB_DATA_CELL_RENDERER_PASSWORD (object);

	if (datacell->priv) {
		g_free (datacell->priv->options);
		g_free (datacell->priv);
		datacell->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}


static void
gnome_db_data_cell_renderer_password_get_property (GObject *object,
						   guint param_id,
						   GValue *value,
						   GParamSpec *pspec)
{
	GnomeDbDataCellRendererPassword *datacell = GNOME_DB_DATA_CELL_RENDERER_PASSWORD (object);

	switch (param_id) {
	case PROP_VALUE_ATTRIBUTES:
		/* nothing to do */
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}

static void
gnome_db_data_cell_renderer_password_set_property (GObject *object,
						   guint         param_id,
						   const GValue *value,
						   GParamSpec *pspec)
{
	GnomeDbDataCellRendererPassword *datacell = GNOME_DB_DATA_CELL_RENDERER_PASSWORD (object);

	switch (param_id) {
	case PROP_VALUE:
		/* FIXME: is it necessary to make a copy of value, couldn't we just use the
		 * value AS IS for performances reasons ? */
		if (datacell->priv->value) {
			gda_value_free (datacell->priv->value);
			datacell->priv->value = NULL;
		}

		if (value) {			
			GValue *gval = g_value_get_pointer (value);
			if (gval && !gda_value_is_null (gval)) {
				gchar *str;
				
				if (G_VALUE_TYPE (gval) != datacell->priv->type) {
					if (!datacell->priv->type_forced) {
						datacell->priv->type_forced = TRUE;
						g_warning (_("Data cell renderer's specified type (%s) differs from actual "
							     "value to display type (%s)"), 
							   g_type_name (datacell->priv->type),
							   g_type_name (G_VALUE_TYPE (gval)));
					}
					else
						g_warning (_("Data cell renderer asked to display values of different "
							     "data types, at least %s and %s, which means the data model has "
							     "some incoherencies"),
							   g_type_name (datacell->priv->type),
							   g_type_name (G_VALUE_TYPE (gval)));
					datacell->priv->type = G_VALUE_TYPE (gval);
				}

				datacell->priv->value = gda_value_copy (gval);

				if (datacell->priv->dh) {
					gchar *ptr;
					str = gda_data_handler_get_str_from_value (datacell->priv->dh, gval);
					for (ptr = str; *ptr; ptr++)
						*ptr = '*';
					g_object_set (G_OBJECT (object), "text", str, NULL);
					g_free (str);
				}
				else
					g_object_set (G_OBJECT (object), "text", _("<non-printable>"), NULL);
			}
			else
				g_object_set (G_OBJECT (object), "text", "", NULL);
		}
		else
			g_object_set (G_OBJECT (object), "text", "", NULL);
			      
		g_object_notify (object, "value");
		break;
	case PROP_VALUE_ATTRIBUTES:
		/*if (g_value_get_flags (value) & GDA_VALUE_ATTR_IS_DEFAULT)
		  g_object_set (G_OBJECT (object), "text", "", NULL);*/
		break;
	case PROP_TO_BE_DELETED:
		datacell->priv->to_be_deleted = g_value_get_boolean (value);
		break;
	case PROP_DATA_HANDLER:
		if (datacell->priv->dh)
			g_object_unref (G_OBJECT (datacell->priv->dh));

		datacell->priv->dh = GDA_DATA_HANDLER(g_value_get_object(value));
		if (datacell->priv->dh)
			g_object_ref (G_OBJECT (datacell->priv->dh));
		break;
	case PROP_TYPE:
		datacell->priv->type = g_value_get_gtype(value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}

/**
 * gnome_db_data_cell_renderer_password_new
 * @dh: a #GdaDataHandler object, or %NULL
 * @type: the #GType being edited
 * 
 * Creates a new #GnomeDbDataCellRendererPassword.
 * 
 * Return value: the new cell renderer
 **/
GtkCellRenderer *
gnome_db_data_cell_renderer_password_new (GdaDataHandler *dh, GType type, const gchar *options)
{
	GObject *obj;

	g_return_val_if_fail (!dh || GDA_IS_DATA_HANDLER (dh), NULL);
	obj = g_object_new (GNOME_DB_TYPE_DATA_CELL_RENDERER_PASSWORD,
			    "type", type, "data_handler", dh, NULL);

	if (options)
		GNOME_DB_DATA_CELL_RENDERER_PASSWORD (obj)->priv->options = g_strdup (options);

	return GTK_CELL_RENDERER (obj);
}


static void
gnome_db_data_cell_renderer_password_get_size (GtkCellRenderer *cell,
					       GtkWidget       *widget,
					       GdkRectangle    *cell_area,
					       gint            *x_offset,
					       gint            *y_offset,
					       gint            *width,
					       gint            *height)
{
	GtkCellRendererClass *text_class = g_type_class_peek (GTK_TYPE_CELL_RENDERER_TEXT);
	(text_class->get_size) (cell, widget, cell_area, x_offset, y_offset, width, height);
}

static void
gnome_db_data_cell_renderer_password_render (GtkCellRenderer      *cell,
					     GdkWindow            *window,
					     GtkWidget            *widget,
					     GdkRectangle         *background_area,
					     GdkRectangle         *cell_area,
					     GdkRectangle         *expose_area,
					     GtkCellRendererState  flags)
	
{
	GtkCellRendererClass *text_class = g_type_class_peek (GTK_TYPE_CELL_RENDERER_TEXT);
	(text_class->render) (cell, window, widget, background_area, cell_area, expose_area, flags);

	if (GNOME_DB_DATA_CELL_RENDERER_PASSWORD (cell)->priv->to_be_deleted)
		gtk_paint_hline (widget->style,
				 window, GTK_STATE_SELECTED,
				 cell_area, 
				 widget,
				 "hline",
				 cell_area->x + cell->xpad, cell_area->x + cell_area->width - cell->xpad,
				 cell_area->y + cell_area->height / 2.);
}

static void
gnome_db_data_cell_renderer_password_editing_done (GtkCellEditable *entry,
						   gpointer         data)
{
	const gchar *path;
	GnomeDbDataCellRendererPasswordInfo *info;
	GValue *value;

	info = g_object_get_data (G_OBJECT (data),
				  GNOME_DB_DATA_CELL_RENDERER_PASSWORD_INFO_KEY);

	if (info->focus_out_id > 0) {
		g_signal_handler_disconnect (entry, info->focus_out_id);
		info->focus_out_id = 0;
	}

	if (g_object_class_find_property (G_OBJECT_GET_CLASS (entry), "editing_cancelled")) {
		gboolean editing_cancelled;

		g_object_get (G_OBJECT (entry), "editing_cancelled", &editing_cancelled, NULL);
		if (editing_cancelled)
			return;
	}

	path = g_object_get_data (G_OBJECT (entry), GNOME_DB_DATA_CELL_RENDERER_PASSWORD_PATH);
	
	value = gnome_db_data_entry_get_value (GNOME_DB_DATA_ENTRY (entry));
	g_signal_emit (data, text_cell_renderer_password_signals[CHANGED], 0, path, value);
	gda_value_free (value);
}

static gboolean
gnome_db_data_cell_renderer_password_focus_out_event (GtkWidget *entry,
						      GdkEvent  *event,
						      gpointer   data)
{
	gnome_db_data_cell_renderer_password_editing_done (GTK_CELL_EDITABLE (entry), data);

	/* entry needs focus-out-event */
	return FALSE;
}

static GtkCellEditable *
gnome_db_data_cell_renderer_password_start_editing (GtkCellRenderer      *cell,
						    GdkEvent             *event,
						    GtkWidget            *widget,
						    const gchar          *path,
						    GdkRectangle         *background_area,
						    GdkRectangle         *cell_area,
						    GtkCellRendererState  flags)
{
	GnomeDbDataCellRendererPassword *datacell;
	GtkWidget *entry;
	GnomeDbDataCellRendererPasswordInfo *info;
	gboolean editable;
	
	datacell = GNOME_DB_DATA_CELL_RENDERER_PASSWORD (cell);

	/* If the cell isn't editable we return NULL. */
	g_object_get (G_OBJECT (cell), "editable", &editable, NULL);
	if (!editable)
		return NULL;
	/* If there is no data handler then the cell also is not editable */
	if (!datacell->priv->dh)
		return NULL;

	entry = gnome_db_entry_password_new (datacell->priv->dh, datacell->priv->type, datacell->priv->options);

	g_object_set (G_OBJECT (entry), "is_cell_renderer", TRUE, "actions", FALSE, NULL);

	gnome_db_data_entry_set_value_orig (GNOME_DB_DATA_ENTRY (entry), datacell->priv->value);
	
	info = g_new0 (GnomeDbDataCellRendererPasswordInfo, 1);
 	g_object_set_data_full (G_OBJECT (entry), GNOME_DB_DATA_CELL_RENDERER_PASSWORD_PATH, g_strdup (path), g_free); 
	g_object_set_data_full (G_OBJECT (cell), GNOME_DB_DATA_CELL_RENDERER_PASSWORD_INFO_KEY, info, g_free);
  
	g_signal_connect (entry, "editing_done",
			  G_CALLBACK (gnome_db_data_cell_renderer_password_editing_done), datacell);
	info->focus_out_id = g_signal_connect (entry, "focus_out_event",
					       G_CALLBACK (gnome_db_data_cell_renderer_password_focus_out_event),
					       datacell);
	gtk_widget_show (entry);
	return GTK_CELL_EDITABLE (entry);
}
