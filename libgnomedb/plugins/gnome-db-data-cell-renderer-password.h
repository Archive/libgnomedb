/* gnome-db-data-cell-renderer-password.h
 * Copyright (C) 2007 Vivien Malerba <malerba@gnome-db.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GNOME_DB_DATA_CELL_RENDERER_PASSWORD_H__
#define __GNOME_DB_DATA_CELL_RENDERER_PASSWORD_H__

#include <gtk/gtk.h>
#include <libgnomedb/gnome-db-decl.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_DATA_CELL_RENDERER_PASSWORD		(gnome_db_data_cell_renderer_password_get_type ())
#define GNOME_DB_DATA_CELL_RENDERER_PASSWORD(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), GNOME_DB_TYPE_DATA_CELL_RENDERER_PASSWORD, GnomeDbDataCellRendererPassword))
#define GNOME_DB_DATA_CELL_RENDERER_PASSWORD_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), GNOME_DB_TYPE_DATA_CELL_RENDERER_PASSWORD, GnomeDbDataCellRendererPasswordClass))
#define GNOME_DB_IS_DATA_CELL_RENDERER_PASSWORD(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GNOME_DB_TYPE_DATA_CELL_RENDERER_PASSWORD))
#define GNOME_DB_IS_DATA_CELL_RENDERER_PASSWORD_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), GNOME_DB_TYPE_DATA_CELL_RENDERER_PASSWORD))
#define GNOME_DB_DATA_CELL_RENDERER_PASSWORD_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GNOME_DB_TYPE_DATA_CELL_RENDERER_PASSWORD, GnomeDbDataCellRendererPasswordClass))

typedef struct _GnomeDbDataCellRendererPassword GnomeDbDataCellRendererPassword;
typedef struct _GnomeDbDataCellRendererPasswordClass GnomeDbDataCellRendererPasswordClass;
typedef struct _GnomeDbDataCellRendererPasswordPrivate GnomeDbDataCellRendererPasswordPrivate;

struct _GnomeDbDataCellRendererPassword
{
	GtkCellRendererText                     parent;
	
	GnomeDbDataCellRendererPasswordPrivate *priv;
};

struct _GnomeDbDataCellRendererPasswordClass
{
	GtkCellRendererTextClass                parent_class;
	
	void (* changed) (GnomeDbDataCellRendererPassword *cell_renderer,
			  const gchar                     *path,
			  const GValue                    *new_value);
};

GType            gnome_db_data_cell_renderer_password_get_type  (void) G_GNUC_CONST;
GtkCellRenderer *gnome_db_data_cell_renderer_password_new       (GdaDataHandler *dh, GType type, const gchar *options);

G_END_DECLS

#endif /* __GNOME_DB_DATA_CELL_RENDERER_PASSWORD_H__ */
