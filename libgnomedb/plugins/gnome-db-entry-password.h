/* gnome-db-entry-password.h
 *
 * Copyright (C) 2003 - 2007 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __GNOME_DB_ENTRY_PASSWORD_H_
#define __GNOME_DB_ENTRY_PASSWORD_H_

#include <libgnomedb/data-entries/gnome-db-entry-wrapper.h>

G_BEGIN_DECLS

#define GNOME_DB_ENTRY_PASSWORD_TYPE          (gnome_db_entry_password_get_type())
#define GNOME_DB_ENTRY_PASSWORD(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, gnome_db_entry_password_get_type(), GnomeDbEntryPassword)
#define GNOME_DB_ENTRY_PASSWORD_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, gnome_db_entry_password_get_type (), GnomeDbEntryPasswordClass)
#define GNOME_DB_IS_ENTRY_PASSWORD(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, gnome_db_entry_password_get_type ())


typedef struct _GnomeDbEntryPassword GnomeDbEntryPassword;
typedef struct _GnomeDbEntryPasswordClass GnomeDbEntryPasswordClass;
typedef struct _GnomeDbEntryPasswordPrivate GnomeDbEntryPasswordPrivate;


/* struct for the object's data */
struct _GnomeDbEntryPassword
{
	GnomeDbEntryWrapper              object;
	GnomeDbEntryPasswordPrivate         *priv;
};

/* struct for the object's class */
struct _GnomeDbEntryPasswordClass
{
	GnomeDbEntryWrapperClass         parent_class;
};

GType        gnome_db_entry_password_get_type        (void) G_GNUC_CONST;
GtkWidget   *gnome_db_entry_password_new             (GdaDataHandler *dh, GType type, const gchar *options);


G_END_DECLS

#endif
