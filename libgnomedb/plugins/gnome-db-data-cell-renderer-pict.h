/* gnome-db-data-cell-renderer-pict.h
 * Copyright (C) 2006 - 2007 Vivien Malerba <malerba@gnome-db.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GNOME_DB_DATA_CELL_RENDERER_PICT_H__
#define __GNOME_DB_DATA_CELL_RENDERER_PICT_H__

#include <gtk/gtk.h>
#include <libgnomedb/gnome-db-decl.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_DATA_CELL_RENDERER_PICT		(gnome_db_data_cell_renderer_pict_get_type ())
#define GNOME_DB_DATA_CELL_RENDERER_PICT(obj)		(G_TYPE_CHECK_INSTANCE_CAST ((obj), GNOME_DB_TYPE_DATA_CELL_RENDERER_PICT, GnomeDbDataCellRendererPict))
#define GNOME_DB_DATA_CELL_RENDERER_PICT_CLASS(klass)	(G_TYPE_CHECK_CLASS_CAST ((klass), GNOME_DB_TYPE_DATA_CELL_RENDERER_PICT, GnomeDbDataCellRendererPictClass))
#define GNOME_DB_IS_DATA_CELL_RENDERER_PICT(obj)		(G_TYPE_CHECK_INSTANCE_TYPE ((obj), GNOME_DB_TYPE_DATA_CELL_RENDERER_PICT))
#define GNOME_DB_IS_DATA_CELL_RENDERER_PICT_CLASS(klass)	(G_TYPE_CHECK_CLASS_TYPE ((klass), GNOME_DB_TYPE_DATA_CELL_RENDERER_PICT))
#define GNOME_DB_DATA_CELL_RENDERER_PICT_GET_CLASS(obj)    (G_TYPE_INSTANCE_GET_CLASS ((obj), GNOME_DB_TYPE_DATA_CELL_RENDERER_PICT, GnomeDbDataCellRendererPictClass))

typedef struct _GnomeDbDataCellRendererPict GnomeDbDataCellRendererPict;
typedef struct _GnomeDbDataCellRendererPictClass GnomeDbDataCellRendererPictClass;
typedef struct _GnomeDbDataCellRendererPictPrivate GnomeDbDataCellRendererPictPrivate;

struct _GnomeDbDataCellRendererPict
{
	GtkCellRendererPixbuf               parent;
	
	GnomeDbDataCellRendererPictPrivate *priv;
};

struct _GnomeDbDataCellRendererPictClass
{
	GtkCellRendererPixbufClass          parent_class;
	
	void (* changed) (GnomeDbDataCellRendererPict *cell_renderer,
			  const gchar                 *path,
			  const GValue                *new_value);
};

GType            gnome_db_data_cell_renderer_pict_get_type  (void) G_GNUC_CONST;
GtkCellRenderer *gnome_db_data_cell_renderer_pict_new       (GdaDataHandler *dh, GType type, const gchar *options);

G_END_DECLS

#endif /* __GNOME_DB_DATA_CELL_RENDERER_PICT_H__ */
