/* gnome-db-entry-filesel.h
 *
 * Copyright (C) 2005 - 2006 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __GNOME_DB_ENTRY_FILESEL_H_
#define __GNOME_DB_ENTRY_FILESEL_H_

#include <libgnomedb/data-entries/gnome-db-entry-wrapper.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_ENTRY_FILESEL          (gnome_db_entry_filesel_get_type())
#define GNOME_DB_ENTRY_FILESEL(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, gnome_db_entry_filesel_get_type(), GnomeDbEntryFilesel)
#define GNOME_DB_ENTRY_FILESEL_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, gnome_db_entry_filesel_get_type (), GnomeDbEntryFileselClass)
#define GNOME_DB_IS_ENTRY_FILESEL(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, gnome_db_entry_filesel_get_type ())


typedef struct _GnomeDbEntryFilesel GnomeDbEntryFilesel;
typedef struct _GnomeDbEntryFileselClass GnomeDbEntryFileselClass;
typedef struct _GnomeDbEntryFileselPrivate GnomeDbEntryFileselPrivate;


/* struct for the object's data */
struct _GnomeDbEntryFilesel
{
	GnomeDbEntryWrapper              object;
	GnomeDbEntryFileselPrivate      *priv;
};

/* struct for the object's class */
struct _GnomeDbEntryFileselClass
{
	GnomeDbEntryWrapperClass         parent_class;
};

GType        gnome_db_entry_filesel_get_type        (void) G_GNUC_CONST;
GtkWidget   *gnome_db_entry_filesel_new             (GdaDataHandler *dh, GType type, const gchar *options);


G_END_DECLS

#endif
