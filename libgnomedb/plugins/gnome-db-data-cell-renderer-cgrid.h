/* gnome-db-data-cell-renderer-cgrid.h
 *
 * Copyright (C) 2007 - 2007 Carlos Savoretti
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNOME_DB_DATA_CELL_RENDERER_CGRID_H__
#define __GNOME_DB_DATA_CELL_RENDERER_CGRID_H__

#include <gtk/gtk.h>
#include <pango/pango.h>
#include <libgnomedb/gnome-db-decl.h>

#include <libgda/libgda.h>
#include <libgda/gda-enum-types.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_DATA_CELL_RENDERER_CGRID            (gnome_db_data_cell_renderer_cgrid_get_type ())
#define GNOME_DB_DATA_CELL_RENDERER_CGRID(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GNOME_DB_TYPE_DATA_CELL_RENDERER_CGRID, GnomeDbDataCellRendererCGrid))
#define GNOME_DB_DATA_CELL_RENDERER_CGRID_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GNOME_DB_TYPE_DATA_CELL_RENDERER_CGRID, GnomeDbDataCellRendererCGridClass))
#define GNOME_DB_IS_DATA_CELL_RENDERER_CGRID(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GNOME_DB_TYPE_DATA_CELL_RENDERER_CGRID))
#define GNOME_DB_IS_DATA_CELL_RENDERER_CGRID_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GNOME_DB_TYPE_DATA_CELL_RENDERER_CGRID))
#define GNOME_DB_DATA_CELL_RENDERER_CGRID_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GNOME_DB_TYPE_DATA_CELL_RENDERER_CGRID, GnomeDbDataCellRendererCGridClass))

typedef struct _GnomeDbDataCellRendererCGrid GnomeDbDataCellRendererCGrid;
typedef struct _GnomeDbDataCellRendererCGridClass GnomeDbDataCellRendererCGridClass;
typedef struct _GnomeDbDataCellRendererCGridPrivate GnomeDbDataCellRendererCGridPrivate;

typedef void (* GnomeDbDataCellRendererCGridCgridChangedFunc) (GnomeDbDataCellRendererCGrid  *cgrid,
							       const GValue                  *gvalue);

struct _GnomeDbDataCellRendererCGrid
{
	GtkCellRendererText                  parent;
	GnomeDbDataCellRendererCGridPrivate *priv;
};

struct _GnomeDbDataCellRendererCGridClass
{
	GtkCellRendererTextClass  parent;
	void                     (* changed) (GnomeDbDataCellRendererCGrid  *cgrid,
					      const GValue                  *gvalue);
};

GType                         gnome_db_data_cell_renderer_cgrid_get_type          (void) G_GNUC_CONST;
GnomeDbDataCellRendererCGrid *gnome_db_data_cell_renderer_cgrid_new               (GdaDataHandler *data_handler, 
										   GType gtype, const gchar *options);
GdaDataHandler               *gnome_db_data_cell_renderer_cgrid_get_data_handler  (GnomeDbDataCellRendererCGrid *cgrid);
void                          gnome_db_data_cell_renderer_cgrid_set_data_handler  (GnomeDbDataCellRendererCGrid *cgrid, 
										   GdaDataHandler *data_handler);
GType                         gnome_db_data_cell_renderer_cgrid_get_gtype         (GnomeDbDataCellRendererCGrid *cgrid);
void                          gnome_db_data_cell_renderer_cgrid_set_gtype         (GnomeDbDataCellRendererCGrid  *cgrid, 
										   GType gtype);
gboolean                      gnome_db_data_cell_renderer_cgrid_get_editable      (GnomeDbDataCellRendererCGrid *cgrid);
void                          gnome_db_data_cell_renderer_cgrid_set_editable      (GnomeDbDataCellRendererCGrid  *cgrid, 
										   gboolean editable);
gboolean                      gnome_db_data_cell_renderer_cgrid_get_to_be_deleted (GnomeDbDataCellRendererCGrid *cgrid);
void                          gnome_db_data_cell_renderer_cgrid_set_to_be_deleted (GnomeDbDataCellRendererCGrid  *cgrid, 
										   gboolean to_be_deleted);

G_END_DECLS

#endif
