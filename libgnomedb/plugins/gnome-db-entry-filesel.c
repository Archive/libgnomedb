/* gnome-db-entry-filesel.c
 *
 * Copyright (C) 2005 - 2006 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <glib/gi18n-lib.h>
#include "gnome-db-entry-filesel.h"
#include <libgda/gda-data-handler.h>

/* 
 * Main static functions 
 */
static void gnome_db_entry_filesel_class_init (GnomeDbEntryFileselClass * class);
static void gnome_db_entry_filesel_init (GnomeDbEntryFilesel * srv);
static void gnome_db_entry_filesel_dispose (GObject   * object);
static void gnome_db_entry_filesel_finalize (GObject   * object);

/* virtual functions */
static GtkWidget *create_entry (GnomeDbEntryWrapper *mgwrap);
static void       real_set_value (GnomeDbEntryWrapper *mgwrap, const GValue *value);
static GValue    *real_get_value (GnomeDbEntryWrapper *mgwrap);
static void       connect_signals(GnomeDbEntryWrapper *mgwrap, GCallback modify_cb, GCallback activate_cb);
static gboolean   expand_in_layout (GnomeDbEntryWrapper *mgwrap);
static void       set_editable (GnomeDbEntryWrapper *mgwrap, gboolean editable);

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

/* private structure */
struct _GnomeDbEntryFileselPrivate
{
	GtkWidget            *entry;
	GtkWidget            *button;
	GtkFileChooserAction  mode;
};

GType
gnome_db_entry_filesel_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0)) {
		static const GTypeInfo info = {
			sizeof (GnomeDbEntryFileselClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_entry_filesel_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbEntryFilesel),
			0,
			(GInstanceInitFunc) gnome_db_entry_filesel_init
		};
		
		type = g_type_register_static (GNOME_DB_TYPE_ENTRY_WRAPPER, "GnomeDbEntryFilesel", &info, 0);
	}
	return type;
}

static void
gnome_db_entry_filesel_class_init (GnomeDbEntryFileselClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->dispose = gnome_db_entry_filesel_dispose;
	object_class->finalize = gnome_db_entry_filesel_finalize;

	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->create_entry = create_entry;
	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->real_set_value = real_set_value;
	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->real_get_value = real_get_value;
	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->connect_signals = connect_signals;
	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->expand_in_layout = expand_in_layout;
	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->set_editable = set_editable;	
}

static void
gnome_db_entry_filesel_init (GnomeDbEntryFilesel * gnome_db_entry_filesel)
{
	gnome_db_entry_filesel->priv = g_new0 (GnomeDbEntryFileselPrivate, 1);
	gnome_db_entry_filesel->priv->entry = NULL;
	gnome_db_entry_filesel->priv->button = NULL;
	gnome_db_entry_filesel->priv->mode = GTK_FILE_CHOOSER_ACTION_OPEN;
}

/**
 * gnome_db_entry_filesel_new
 * @dh: the data handler to be used by the new widget
 * @type: the requested data type (compatible with @dh)
 *
 * Creates a new widget which is mainly a GtkEntry
 *
 * Returns: the new widget
 */
GtkWidget *
gnome_db_entry_filesel_new (GdaDataHandler *dh, GType type, const gchar *options)
{
	GObject *obj;
	GnomeDbEntryFilesel *filesel;

	g_return_val_if_fail (dh && GDA_IS_DATA_HANDLER (dh), NULL);
	g_return_val_if_fail (type != G_TYPE_INVALID, NULL);
	g_return_val_if_fail (gda_data_handler_accepts_g_type (dh, type), NULL);

	obj = g_object_new (GNOME_DB_TYPE_ENTRY_FILESEL, "handler", dh, NULL);
	filesel = GNOME_DB_ENTRY_FILESEL (obj);
	gnome_db_data_entry_set_value_type (GNOME_DB_DATA_ENTRY (filesel), type);

	if (options && *options) {
		GdaQuarkList *params;
		const gchar *str;

		params = gda_quark_list_new_from_string (options);
		str = gda_quark_list_find (params, "MODE");
		if (str) {
			if ((*str == 'O') || (*str == 'o'))
				filesel->priv->mode = GTK_FILE_CHOOSER_ACTION_OPEN;
			else if ((*str == 'S') || (*str == 's'))
				filesel->priv->mode = GTK_FILE_CHOOSER_ACTION_SAVE;
			else if ((*str == 'P') || (*str == 'p'))
				filesel->priv->mode = GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER;
			else if ((*str == 'N') || (*str == 'n'))
				filesel->priv->mode = GTK_FILE_CHOOSER_ACTION_CREATE_FOLDER;
		}
		gda_quark_list_free (params);
	}
	
	return GTK_WIDGET (obj);
}


static void
gnome_db_entry_filesel_dispose (GObject   * object)
{
	GnomeDbEntryFilesel *gnome_db_entry_filesel;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOME_DB_IS_ENTRY_FILESEL (object));

	gnome_db_entry_filesel = GNOME_DB_ENTRY_FILESEL (object);
	if (gnome_db_entry_filesel->priv) {

	}

	/* parent class */
	parent_class->dispose (object);
}

static void
gnome_db_entry_filesel_finalize (GObject   * object)
{
	GnomeDbEntryFilesel *gnome_db_entry_filesel;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOME_DB_IS_ENTRY_FILESEL (object));

	gnome_db_entry_filesel = GNOME_DB_ENTRY_FILESEL (object);
	if (gnome_db_entry_filesel->priv) {
		g_free (gnome_db_entry_filesel->priv);
		gnome_db_entry_filesel->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}

static void
button_clicled_cb (GtkWidget *button, GnomeDbEntryFilesel *filesel)
{
	
	GtkWidget *dlg;
	gint result;
	gchar *title;

	if ((filesel->priv->mode == GTK_FILE_CHOOSER_ACTION_OPEN) ||
	    (filesel->priv->mode == GTK_FILE_CHOOSER_ACTION_SAVE))
		title = _("Choose a file");
	else
		title = _("Choose a directory");
	dlg = gtk_file_chooser_dialog_new (title,
					   (GtkWindow *) gtk_widget_get_ancestor (GTK_WIDGET (filesel), GTK_TYPE_WINDOW), 
					   filesel->priv->mode, 
					   GTK_STOCK_CANCEL, GTK_RESPONSE_CANCEL,
					   GTK_STOCK_APPLY, GTK_RESPONSE_ACCEPT,
					   NULL);
		
	result = gtk_dialog_run (GTK_DIALOG (dlg));
	if (result == GTK_RESPONSE_ACCEPT) {
		gchar *file;

		file = gtk_file_chooser_get_filename (GTK_FILE_CHOOSER (dlg));
		gtk_entry_set_text (GTK_ENTRY (filesel->priv->entry), file);
		g_free (file);
	}
	gtk_widget_destroy (dlg);
}

static GtkWidget *
create_entry (GnomeDbEntryWrapper *mgwrap)
{
	GnomeDbEntryFilesel *filesel;
	GtkWidget *wid, *hbox;

	g_return_val_if_fail (GNOME_DB_IS_ENTRY_FILESEL (mgwrap), NULL);
	filesel = GNOME_DB_ENTRY_FILESEL (mgwrap);
	g_return_val_if_fail (filesel->priv, NULL);

	hbox = gtk_hbox_new (FALSE, 0);

	wid = gtk_entry_new ();
	gtk_box_pack_start (GTK_BOX (hbox), wid, TRUE, TRUE, 0);
	gtk_widget_show (wid);
	filesel->priv->entry = wid;

	wid = gtk_button_new_with_label (_("Choose"));

	filesel->priv->button = wid;
	gtk_box_pack_start (GTK_BOX (hbox), wid, FALSE, TRUE, 5);
	gtk_widget_show (wid);
	g_signal_connect (G_OBJECT (wid), "clicked",
			  G_CALLBACK (button_clicled_cb), filesel);
	
	return hbox;
}

static void
real_set_value (GnomeDbEntryWrapper *mgwrap, const GValue *value)
{
	GnomeDbEntryFilesel *filesel;
	gboolean sensitive = FALSE;

	g_return_if_fail (GNOME_DB_IS_ENTRY_FILESEL (mgwrap));
	filesel = GNOME_DB_ENTRY_FILESEL (mgwrap);
	g_return_if_fail (filesel->priv);

	if (value) {
		if (gda_value_is_null ((GValue *) value)) 
			sensitive = FALSE;
		else {
			GdaDataHandler *dh;		
			gchar *str;

			dh = gnome_db_data_entry_get_handler (GNOME_DB_DATA_ENTRY (mgwrap));
			str = gda_data_handler_get_str_from_value (dh, value);
			if (str) {
				gtk_entry_set_text (GTK_ENTRY (filesel->priv->entry), str);
				g_free (str);
				sensitive = TRUE;
			}
		}
	}
	else 
		sensitive = FALSE;

	if (!sensitive) 
		gtk_entry_set_text (GTK_ENTRY (filesel->priv->entry), "");
}

static GValue *
real_get_value (GnomeDbEntryWrapper *mgwrap)
{
	GValue *value;
	GnomeDbEntryFilesel *filesel;
	GdaDataHandler *dh;
	const gchar *str;

	g_return_val_if_fail (GNOME_DB_IS_ENTRY_FILESEL (mgwrap), NULL);
	filesel = GNOME_DB_ENTRY_FILESEL (mgwrap);
	g_return_val_if_fail (filesel->priv, NULL);

	dh = gnome_db_data_entry_get_handler (GNOME_DB_DATA_ENTRY (mgwrap));
	str = gtk_entry_get_text (GTK_ENTRY (filesel->priv->entry));
	value = gda_data_handler_get_value_from_str (dh, str, 
						     gnome_db_data_entry_get_value_type (GNOME_DB_DATA_ENTRY (mgwrap)));
	if (!value) {
		/* in case the gda_data_handler_get_value_from_sql() returned an error because
		   the contents of the GtkFileChooser cannot be interpreted as a GValue */
		value = gda_value_new_null ();
	}

	return value;
}

static void
connect_signals(GnomeDbEntryWrapper *mgwrap, GCallback modify_cb, GCallback activate_cb)
{
	GnomeDbEntryFilesel *filesel;

	g_return_if_fail (GNOME_DB_IS_ENTRY_FILESEL (mgwrap));
	filesel = GNOME_DB_ENTRY_FILESEL (mgwrap);
	g_return_if_fail (filesel->priv);

	g_signal_connect (G_OBJECT (filesel->priv->entry), "changed",
			  modify_cb, mgwrap);
	g_signal_connect (G_OBJECT (filesel->priv->entry), "activate",
			  activate_cb, mgwrap);
}

static gboolean
expand_in_layout (GnomeDbEntryWrapper *mgwrap)
{
	return FALSE;
}

static void
set_editable (GnomeDbEntryWrapper *mgwrap, gboolean editable)
{
	GnomeDbEntryFilesel *filesel;

	g_return_if_fail (GNOME_DB_IS_ENTRY_FILESEL (mgwrap));
	filesel = GNOME_DB_ENTRY_FILESEL (mgwrap);
	g_return_if_fail (filesel->priv);

	gtk_entry_set_editable (GTK_ENTRY (filesel->priv->entry), editable);
	gtk_widget_set_sensitive (filesel->priv->button, editable);
}
