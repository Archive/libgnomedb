/* gnome-db-entry-text.h
 *
 * Copyright (C) 2003 - 2006 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */


#ifndef __GNOME_DB_ENTRY_TEXT_H_
#define __GNOME_DB_ENTRY_TEXT_H_

#include <libgnomedb/data-entries/gnome-db-entry-wrapper.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_ENTRY_TEXT          (gnome_db_entry_text_get_type())
#define GNOME_DB_ENTRY_TEXT(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, gnome_db_entry_text_get_type(), GnomeDbEntryText)
#define GNOME_DB_ENTRY_TEXT_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, gnome_db_entry_text_get_type (), GnomeDbEntryTextClass)
#define GNOME_DB_IS_ENTRY_TEXT(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, gnome_db_entry_text_get_type ())


typedef struct _GnomeDbEntryText GnomeDbEntryText;
typedef struct _GnomeDbEntryTextClass GnomeDbEntryTextClass;
typedef struct _GnomeDbEntryTextPrivate GnomeDbEntryTextPrivate;


/* struct for the object's data */
struct _GnomeDbEntryText
{
	GnomeDbEntryWrapper              object;
	GnomeDbEntryTextPrivate         *priv;
};

/* struct for the object's class */
struct _GnomeDbEntryTextClass
{
	GnomeDbEntryWrapperClass         parent_class;
};

GType        gnome_db_entry_text_get_type        (void) G_GNUC_CONST;
GtkWidget   *gnome_db_entry_text_new             (GdaDataHandler *dh, GType type);


G_END_DECLS

#endif
