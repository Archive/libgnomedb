/* gnome-db-entry-pict.c
 *
 * Copyright (C) 2006 - 2007 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <glib/gi18n-lib.h>
#include <glib/gstdio.h>
#include <gtk/gtk.h>
#include <gdk-pixbuf/gdk-pixdata.h>
#include "gnome-db-entry-pict.h"
#include "gnome-db-entry-filesel.h"
#include <libgda/gda-data-handler.h>
#include <libgda/gda-blob-op.h>
#include <string.h>
#include <unistd.h>
#include <gdk/gdk.h>

#include "common-pict.h"

/* 
 * Main static functions 
 */
static void gnome_db_entry_pict_class_init (GnomeDbEntryPictClass * class);
static void gnome_db_entry_pict_init (GnomeDbEntryPict *srv);
static void gnome_db_entry_pict_dispose (GObject *object);
static void gnome_db_entry_pict_finalize (GObject *object);

/* virtual functions */
static GtkWidget *create_entry (GnomeDbEntryWrapper *mgwrap);
static void       real_set_value (GnomeDbEntryWrapper *mgwrap, const GValue *value);
static GValue    *real_get_value (GnomeDbEntryWrapper *mgwrap);
static void       connect_signals(GnomeDbEntryWrapper *mgwrap, GCallback modify_cb, GCallback activate_cb);
static gboolean   expand_in_layout (GnomeDbEntryWrapper *mgwrap);
static void       set_editable (GnomeDbEntryWrapper *mgwrap, gboolean editable);
static gboolean   value_is_equal_to (GnomeDbEntryWrapper *mgwrap, const GValue *value);
static gboolean   value_is_null (GnomeDbEntryWrapper *mgwrap);

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

/* private structure */
struct _GnomeDbEntryPictPrivate
{
	GtkWidget     *sw;
	GtkWidget     *pict;
	GtkWidget     *notice;
	gboolean       editable;
	
	PictBinData    bindata;
	PictOptions    options;
	PictMenu       popup_menu;

	PictAllocation size;
};


GType
gnome_db_entry_pict_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0)) {
		static const GTypeInfo info = {
			sizeof (GnomeDbEntryPictClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_entry_pict_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbEntryPict),
			0,
			(GInstanceInitFunc) gnome_db_entry_pict_init
		};
		
		type = g_type_register_static (GNOME_DB_TYPE_ENTRY_WRAPPER, "GnomeDbEntryPict", &info, 0);
	}
	return type;
}

static void
gnome_db_entry_pict_class_init (GnomeDbEntryPictClass * class)
{
	GObjectClass   *object_class = G_OBJECT_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->dispose = gnome_db_entry_pict_dispose;
	object_class->finalize = gnome_db_entry_pict_finalize;

	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->create_entry = create_entry;
	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->real_set_value = real_set_value;
	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->real_get_value = real_get_value;
	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->connect_signals = connect_signals;
	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->expand_in_layout = expand_in_layout;
	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->set_editable = set_editable;
	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->value_is_equal_to = value_is_equal_to;
	GNOME_DB_ENTRY_WRAPPER_CLASS (class)->value_is_null = value_is_null;
}

static void
gnome_db_entry_pict_init (GnomeDbEntryPict * gnome_db_entry_pict)
{
	gnome_db_entry_pict->priv = g_new0 (GnomeDbEntryPictPrivate, 1);
	gnome_db_entry_pict->priv->pict = NULL;
	gnome_db_entry_pict->priv->bindata.data = NULL;
	gnome_db_entry_pict->priv->bindata.data_length = 0;
	gnome_db_entry_pict->priv->options.encoding = ENCODING_NONE;
	gnome_db_entry_pict->priv->options.serialize = FALSE;
	common_pict_init_cache (&gnome_db_entry_pict->priv->options);
	gnome_db_entry_pict->priv->editable = TRUE;
	gnome_db_entry_pict->priv->size.width = 0;
	gnome_db_entry_pict->priv->size.height = 0;
}

/**
 * gnome_db_entry_pict_new
 * @dh: the data handler to be used by the new widget
 * @type: the requested data type (compatible with @dh)
 * @options: optional parameters
 *
 * Creates a new widget which is mainly a GtkEntry
 *
 * Returns: the new widget
 */
GtkWidget *
gnome_db_entry_pict_new (GdaDataHandler *dh, GType type, const gchar *options)
{
	GObject *obj;
	GnomeDbEntryPict *mgpict;

	g_return_val_if_fail (GDA_IS_DATA_HANDLER (dh), NULL);
	g_return_val_if_fail (type != G_TYPE_INVALID, NULL);
	g_return_val_if_fail (gda_data_handler_accepts_g_type (dh, type), NULL);

	obj = g_object_new (GNOME_DB_TYPE_ENTRY_PICT, "handler", dh, NULL);
	mgpict = GNOME_DB_ENTRY_PICT (obj);
	gnome_db_data_entry_set_value_type (GNOME_DB_DATA_ENTRY (mgpict), type);

	common_pict_parse_options (&(mgpict->priv->options), options);

	return GTK_WIDGET (obj);
}


static void
gnome_db_entry_pict_dispose (GObject   * object)
{
	GnomeDbEntryPict *mgpict;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOME_DB_IS_ENTRY_PICT (object));

	mgpict = GNOME_DB_ENTRY_PICT (object);
	if (mgpict->priv) {
		if (mgpict->priv->options.pixbuf_hash) {
			g_hash_table_destroy (mgpict->priv->options.pixbuf_hash);
			mgpict->priv->options.pixbuf_hash = NULL;
		}

		if (mgpict->priv->bindata.data) {
			g_free (mgpict->priv->bindata.data);
			mgpict->priv->bindata.data = NULL;
			mgpict->priv->bindata.data_length = 0;
		}

		if (mgpict->priv->popup_menu.menu) {
			gtk_widget_destroy (mgpict->priv->popup_menu.menu);
			mgpict->priv->popup_menu.menu = NULL;
		}
	}

	/* parent class */
	parent_class->dispose (object);
}

static void
gnome_db_entry_pict_finalize (GObject   * object)
{
	GnomeDbEntryPict *mgpict;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOME_DB_IS_ENTRY_PICT (object));

	mgpict = GNOME_DB_ENTRY_PICT (object);
	if (mgpict->priv) {
		g_free (mgpict->priv);
		mgpict->priv = NULL;
	}

	/* parent class */
	parent_class->finalize (object);
}

static void display_image (GnomeDbEntryPict *mgpict, const GValue *value, const gchar *error_stock, const gchar *notice);
static gboolean popup_menu_cb (GtkWidget *button, GnomeDbEntryPict *mgpict);
static gboolean event_cb (GtkWidget *button, GdkEvent *event, GnomeDbEntryPict *mgpict);
static void size_allocate_cb (GtkWidget *wid, GtkAllocation *allocation, GnomeDbEntryPict *mgpict);

static void
realize_cb (GnomeDbEntryPict *mgpict, GnomeDbEntryWrapper *mgwrap)
{
	display_image (mgpict, NULL, NULL, NULL);
}

static GtkWidget *
create_entry (GnomeDbEntryWrapper *mgwrap)
{
	GtkWidget *vbox, *wid;
	GnomeDbEntryPict *mgpict;

	g_return_val_if_fail (mgwrap && GNOME_DB_IS_ENTRY_PICT (mgwrap), NULL);
	mgpict = GNOME_DB_ENTRY_PICT (mgwrap);
	g_return_val_if_fail (mgpict->priv, NULL);

	vbox = gtk_vbox_new (FALSE, 0);

	/* sw */
	wid = gtk_scrolled_window_new (NULL, NULL);
	gtk_box_pack_start (GTK_BOX (vbox), wid, TRUE, TRUE, 0);
	gtk_widget_show (wid);
	mgpict->priv->sw = wid;
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (wid), 
					GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (wid), GTK_SHADOW_NONE);
	g_signal_connect (G_OBJECT (mgpict->priv->sw), "size-allocate",
			  G_CALLBACK (size_allocate_cb), mgpict);

	/* image */
	wid = gtk_image_new ();
	gtk_misc_set_alignment (GTK_MISC (wid), 0., .5);
	gtk_scrolled_window_add_with_viewport (GTK_SCROLLED_WINDOW (mgpict->priv->sw), wid);
	gtk_widget_show (wid);
	mgpict->priv->pict = wid;

	wid = gtk_bin_get_child (GTK_BIN (mgpict->priv->sw));
	gtk_viewport_set_shadow_type (GTK_VIEWPORT (wid), GTK_SHADOW_NONE);

	/* notice, not shown */
	wid = gtk_label_new ("");
	mgpict->priv->notice = wid;
	gtk_misc_set_alignment (GTK_MISC (wid), 0., .5);
	gtk_box_pack_start (GTK_BOX (vbox), wid, TRUE, TRUE, 0);

	/* connect signals for popup menu */
	g_signal_connect (G_OBJECT (mgpict), "popup-menu",
			  G_CALLBACK (popup_menu_cb), mgpict);
	g_signal_connect (G_OBJECT (mgpict), "event",
			  G_CALLBACK (event_cb), mgpict);

	display_image (mgpict, NULL, GTK_STOCK_MISSING_IMAGE, _("No data to display"));

	g_signal_connect (G_OBJECT (mgpict), "realize",
			  G_CALLBACK (realize_cb), mgwrap);

	return vbox;
}

static void
size_allocate_cb (GtkWidget *wid, GtkAllocation *allocation, GnomeDbEntryPict *mgpict)
{
	if ((mgpict->priv->size.width != allocation->width) ||
	    (mgpict->priv->size.height != allocation->height)) {
		mgpict->priv->size.width = allocation->width;
		mgpict->priv->size.height = allocation->height;
		common_pict_clear_pixbuf_cache (&(mgpict->priv->options));
	}
}

static void
pict_data_changed_cb (GnomeDbEntryPict *mgpict)
{
	display_image (mgpict, NULL, NULL, NULL);
	gnome_db_entry_wrapper_contents_changed (GNOME_DB_ENTRY_WRAPPER (mgpict));
	gnome_db_entry_wrapper_contents_activated (GNOME_DB_ENTRY_WRAPPER (mgpict));
}

static void
do_popup_menu (GtkWidget *widget, GdkEventButton *event, GnomeDbEntryPict *mgpict)
{
	int button, event_time;
	
	if (!mgpict->priv->popup_menu.menu)
		common_pict_create_menu (&(mgpict->priv->popup_menu), widget, &(mgpict->priv->bindata), 
					 &(mgpict->priv->options), 
					 (PictCallback) pict_data_changed_cb, mgpict);

	common_pict_adjust_menu_sensitiveness (&(mgpict->priv->popup_menu), mgpict->priv->editable, 
						       &(mgpict->priv->bindata));
	if (event) {
		button = event->button;
		event_time = event->time;
	}
	else {
		button = 0;
		event_time = gtk_get_current_event_time ();
	}
	
	gtk_menu_popup (GTK_MENU (mgpict->priv->popup_menu.menu), NULL, NULL, NULL, NULL, 
			button, event_time);
}

static gboolean
popup_menu_cb (GtkWidget *widget, GnomeDbEntryPict *mgpict)
{
	do_popup_menu (widget, NULL, mgpict);
	return TRUE;
}

static gboolean
event_cb (GtkWidget *widget, GdkEvent *event, GnomeDbEntryPict *mgpict)
{
	if ((event->type == GDK_BUTTON_PRESS) && (((GdkEventButton *) event)->button == 3)) {
		do_popup_menu (widget, (GdkEventButton *) event, mgpict);
		return TRUE;
	}
	if ((event->type == GDK_2BUTTON_PRESS) && (((GdkEventButton *) event)->button == 1)) {
		if (!mgpict->priv->popup_menu.menu) 
			common_pict_create_menu (&(mgpict->priv->popup_menu), widget, &(mgpict->priv->bindata), 
						 &(mgpict->priv->options), 
						 (PictCallback) pict_data_changed_cb, mgpict);

		common_pict_adjust_menu_sensitiveness (&(mgpict->priv->popup_menu), mgpict->priv->editable, 
						       &(mgpict->priv->bindata));

		gtk_menu_item_activate (GTK_MENU_ITEM (mgpict->priv->popup_menu.load_mitem));
	}
	
	return FALSE;
}

static void
real_set_value (GnomeDbEntryWrapper *mgwrap, const GValue *value)
{
	GnomeDbEntryPict *mgpict;
	const gchar *stock = NULL;
	gchar *notice_msg = NULL;
	GError *error = NULL;

	g_return_if_fail (mgwrap && GNOME_DB_IS_ENTRY_PICT (mgwrap));
	mgpict = GNOME_DB_ENTRY_PICT (mgwrap);
	g_return_if_fail (mgpict->priv);

	if (mgpict->priv->bindata.data) {
		g_free (mgpict->priv->bindata.data);
		mgpict->priv->bindata.data = NULL;
		mgpict->priv->bindata.data_length = 0;
	}

	/* fill in mgpict->priv->data */
	if (!common_pict_load_data (&(mgpict->priv->options), value, &(mgpict->priv->bindata), &stock, &error)) {
		notice_msg = g_strdup (error->message ? error->message : "");
		g_error_free (error);
	}

	/* create (if possible) a pixbuf from mgpict->priv->bindata.data */
	display_image (mgpict, value, stock, notice_msg);
	g_free (notice_msg);
}

static void 
display_image (GnomeDbEntryPict *mgpict, const GValue *value, const gchar *error_stock, const gchar *notice)
{
	const gchar *stock = error_stock;
	gchar *notice_msg = NULL;
	GdkPixbuf *pixbuf;
	PictAllocation alloc;
	GError *error = NULL;

	alloc.width = mgpict->priv->sw->allocation.width;
	alloc.height = mgpict->priv->sw->allocation.height;
	
	pixbuf = common_pict_fetch_cached_pixbuf (&(mgpict->priv->options), value);
	if (pixbuf)
		g_object_ref (pixbuf);
	else {
		pixbuf = common_pict_make_pixbuf (&(mgpict->priv->options), &(mgpict->priv->bindata), &alloc, 
						  &stock, &error);
		if (pixbuf) 
			common_pict_add_cached_pixbuf (&(mgpict->priv->options), value, pixbuf);
	}

	if (pixbuf) {
		gtk_image_set_from_pixbuf (GTK_IMAGE (mgpict->priv->pict), pixbuf);
		g_object_unref (pixbuf);
	}
	else {
		if (error) {
			notice_msg = g_strdup (error->message ? error->message : "");
			g_error_free (error);
		}
		else {
			stock = GTK_STOCK_MISSING_IMAGE;
			notice_msg = g_strdup (_("Empty data"));
		}
	}

	if (stock)
		gtk_image_set_from_stock (GTK_IMAGE (mgpict->priv->pict), 
					  stock, GTK_ICON_SIZE_DIALOG);
	if (notice || notice_msg) {
		gtk_label_set_text (GTK_LABEL (mgpict->priv->notice), notice ? notice : notice_msg);
		gtk_widget_show (mgpict->priv->notice);
		g_free (notice_msg);
	}
	else 
		gtk_widget_hide (mgpict->priv->notice);

	common_pict_adjust_menu_sensitiveness (&(mgpict->priv->popup_menu), mgpict->priv->editable, &(mgpict->priv->bindata));
	gtk_widget_queue_resize ((GtkWidget *) mgpict);
}

static GValue *
real_get_value (GnomeDbEntryWrapper *mgwrap)
{
	GnomeDbEntryPict *mgpict;

	g_return_val_if_fail (mgwrap && GNOME_DB_IS_ENTRY_PICT (mgwrap), NULL);
	mgpict = GNOME_DB_ENTRY_PICT (mgwrap);
	g_return_val_if_fail (mgpict->priv, NULL);

	return common_pict_get_value (&(mgpict->priv->bindata), &(mgpict->priv->options), 
				       gnome_db_data_entry_get_value_type (GNOME_DB_DATA_ENTRY (mgpict)));
}

static void
connect_signals(GnomeDbEntryWrapper *mgwrap, GCallback modify_cb, GCallback activate_cb)
{
	/* doe nothing because we manullay call gnome_db_entry_wrapper_contents_changed() */
}

static gboolean
expand_in_layout (GnomeDbEntryWrapper *mgwrap)
{
	return TRUE;
}

static void
set_editable (GnomeDbEntryWrapper *mgwrap, gboolean editable)
{
	GnomeDbEntryPict *mgpict;

	g_return_if_fail (mgwrap && GNOME_DB_IS_ENTRY_PICT (mgwrap));
	mgpict = GNOME_DB_ENTRY_PICT (mgwrap);
	g_return_if_fail (mgpict->priv);
	
	mgpict->priv->editable = editable;
	common_pict_adjust_menu_sensitiveness (&(mgpict->priv->popup_menu), mgpict->priv->editable, &(mgpict->priv->bindata));
}

static gboolean
value_is_equal_to (GnomeDbEntryWrapper *mgwrap, const GValue *value)
{
	GnomeDbEntryPict *mgpict;

	g_return_val_if_fail (mgwrap && GNOME_DB_IS_ENTRY_PICT (mgwrap), FALSE);
	mgpict = GNOME_DB_ENTRY_PICT (mgwrap);
	g_return_val_if_fail (mgpict->priv, FALSE);
	
	if (value) {
		if (gda_value_is_null (value) && !mgpict->priv->bindata.data)
			return TRUE;
		if (G_VALUE_TYPE (value) == GDA_TYPE_BLOB) {
			GdaBlob *blob;
			GdaBinary *bin;

			blob = (GdaBlob*) gda_value_get_blob ((GValue *) value);
			g_assert (blob);
			bin = (GdaBinary *) blob;
			if (blob->op) 
				gda_blob_op_read_all (blob->op, blob);
			if (mgpict->priv->bindata.data)
				return !memcmp (bin->data, mgpict->priv->bindata.data, MIN (mgpict->priv->bindata.data_length, bin->binary_length));
			else
				return FALSE;
		}
		if (G_VALUE_TYPE (value) == GDA_TYPE_BINARY) {
			GdaBinary *bin;

			bin = (GdaBinary *) gda_value_get_binary ((GValue *) value);
			if (bin && mgpict->priv->bindata.data)
				return !memcmp (bin->data, mgpict->priv->bindata.data, MIN (mgpict->priv->bindata.data_length, bin->binary_length));
			else
				return FALSE;
		}
		if (G_VALUE_TYPE (value) == G_TYPE_STRING) {
			const gchar *cmpstr;
			gchar *curstr = NULL;
			gboolean res;

			cmpstr = g_value_get_string (value);
			switch (mgpict->priv->options.encoding) {
			case ENCODING_NONE:
				curstr = g_strndup ((gchar *) mgpict->priv->bindata.data, 
						    mgpict->priv->bindata.data_length);
				break;
			case ENCODING_BASE64: 
#if (GLIB_MINOR_VERSION >= 12)
				curstr = g_base64_encode (mgpict->priv->bindata.data, mgpict->priv->bindata.data_length);
#else
				g_warning ("Base64 enoding/decoding is not supported in the GLib version %d.%d.%d",
					   glib_major_version, glib_minor_version, glib_micro_version);
#endif
				break;
			default:
				g_assert_not_reached ();
			}
			res = strcmp (curstr, cmpstr) == 0 ? TRUE : FALSE;
			g_free (curstr);
			return res;
		}
		return FALSE;
	}
	else {
		if (mgpict->priv->bindata.data)
			return TRUE;
		else
			return FALSE;
	}
	
	return FALSE;
}

static gboolean
value_is_null (GnomeDbEntryWrapper *mgwrap)
{
	GnomeDbEntryPict *mgpict;

	g_return_val_if_fail (mgwrap && GNOME_DB_IS_ENTRY_PICT (mgwrap), TRUE);
	mgpict = GNOME_DB_ENTRY_PICT (mgwrap);
	g_return_val_if_fail (mgpict->priv, TRUE);

	return mgpict->priv->bindata.data ? FALSE : TRUE;
}
