/* libmain.c
 * Copyright (C) 2006 - 2007 The GNOME Foundation
 *
 * AUTHORS:
 *         Vivien Malerba <malerba@gnome-db.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free
 * Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 */

#include <libgda/libgda.h>
#include <glib/gi18n-lib.h>
#include <libgnomedb/libgnomedb.h>
#include <libgnomedb/gnome-db-plugin.h>
#include <libgnomedb/binreloc/gnome-db-binreloc.h>

#include "gnome-db-entry-filesel.h"
#include "gnome-db-entry-cidr.h"
#include "gnome-db-entry-text.h"
#include "gnome-db-entry-pict.h"
#include "gnome-db-data-cell-renderer-pict.h"
#include "gnome-db-entry-cgrid.h"
#include "gnome-db-data-cell-renderer-cgrid.h"

#ifdef HAVE_LIBGCRYPT
#include "gnome-db-entry-password.h"
#include "gnome-db-data-cell-renderer-password.h"
#endif

static GnomeDbDataEntry *plugin_entry_filesel_create_func (GdaDataHandler *handler, GType type, const gchar *options);
static GnomeDbDataEntry *plugin_entry_cidr_create_func (GdaDataHandler *handler, GType type, const gchar *options);
static GnomeDbDataEntry *plugin_entry_text_create_func (GdaDataHandler *handler, GType type, const gchar *options);
static GnomeDbDataEntry *plugin_entry_pict_create_func (GdaDataHandler *handler, GType type, const gchar *options);
static GtkCellRenderer  *plugin_cell_renderer_pict_create_func (GdaDataHandler *handler, GType type, const gchar *options);
static GnomeDbDataEntry *plugin_entry_cgrid_create_func (GdaDataHandler *handler, GType type, const gchar *options);
static GtkCellRenderer  *plugin_cell_renderer_cgrid_create_func (GdaDataHandler *handler, GType type, const gchar *options);

#ifdef HAVE_LIBGCRYPT
static GnomeDbDataEntry *plugin_entry_password_create_func (GdaDataHandler *handler, GType type, const gchar *options);
static GtkCellRenderer  *plugin_cell_renderer_password_create_func (GdaDataHandler *handler, GType type, const gchar *options);
#endif

GSList *
plugin_init (GError **error)
{
	GnomeDbPlugin *plugin;
	GSList *retlist = NULL;
	gchar *file;

	/* file selector */
	plugin = g_new0 (GnomeDbPlugin, 1);
	plugin->plugin_name = "filesel";
	plugin->plugin_descr = "File selection entry";
	plugin->plugin_file = NULL; /* always leave NULL */
	plugin->nb_g_types = 1;
	plugin->valid_g_types = g_new (GType, plugin->nb_g_types);
	plugin->valid_g_types [0] = G_TYPE_STRING;
	plugin->options_xml_spec = NULL;
	plugin->entry_create_func = plugin_entry_filesel_create_func;
	plugin->cell_create_func = NULL;
	retlist = g_slist_append (retlist, plugin);
	file = gnome_db_gbr_get_data_dir_path ("gnome-db-entry-filesel-spec.xml");
	if (! g_file_test (file, G_FILE_TEST_EXISTS)) {
		if (error && !*error)
			g_set_error (error, 0, 0, _("Missing spec. file '%s'"), file);
        }
	else {
		gsize len;
		g_file_get_contents (file, &(plugin->options_xml_spec), &len, error);
	}
	g_free (file);

	/* CIDR */
	plugin = g_new0 (GnomeDbPlugin, 1);
	plugin->plugin_name = "cird";
	plugin->plugin_descr = "Entry to hold an IPv4 network specification";
	plugin->plugin_file = NULL; /* always leave NULL */
	plugin->nb_g_types = 1;
	plugin->valid_g_types = g_new (GType, plugin->nb_g_types);
	plugin->valid_g_types [0] = G_TYPE_STRING;
	plugin->options_xml_spec = NULL;
	plugin->entry_create_func = plugin_entry_cidr_create_func;
	plugin->cell_create_func = NULL;
	retlist = g_slist_append (retlist, plugin);

#ifdef HAVE_LIBGCRYPT
	/* PASSWORD */
	plugin = g_new0 (GnomeDbPlugin, 1);
	plugin->plugin_name = "password";
	plugin->plugin_descr = "password entry";
	plugin->plugin_file = NULL; /* always leave NULL */
	plugin->nb_g_types = 1;
	plugin->valid_g_types = g_new (GType, plugin->nb_g_types);
	plugin->valid_g_types [0] = G_TYPE_STRING;
	plugin->options_xml_spec = NULL;
	plugin->entry_create_func = plugin_entry_password_create_func;
	plugin->cell_create_func = plugin_cell_renderer_password_create_func;
	retlist = g_slist_append (retlist, plugin);

	file = gnome_db_gbr_get_data_dir_path ("gnome-db-entry-password.xml");
	if (! g_file_test (file, G_FILE_TEST_EXISTS)) {
		if (error && !*error)
			g_set_error (error, 0, 0, _("Missing spec. file '%s'"), file);
        }
	else {
		gsize len;
		g_file_get_contents (file, &(plugin->options_xml_spec), &len, error);
	}
	g_free (file);
#endif
	
	/* TEXT */
	plugin = g_new0 (GnomeDbPlugin, 1);
	plugin->plugin_name = "text";
	plugin->plugin_descr = "Multiline text entry";
	plugin->plugin_file = NULL; /* always leave NULL */
	plugin->nb_g_types = 1;
	plugin->valid_g_types = g_new (GType, plugin->nb_g_types);
	plugin->valid_g_types [0] = G_TYPE_STRING;;
	plugin->options_xml_spec = NULL;
	plugin->entry_create_func = plugin_entry_text_create_func;
	plugin->cell_create_func = NULL;
	retlist = g_slist_append (retlist, plugin);

	/* Picture - binary */
	plugin = g_new0 (GnomeDbPlugin, 1);
	plugin->plugin_name = "picture";
	plugin->plugin_descr = "Picture entry";
	plugin->plugin_file = NULL; /* always leave NULL */
	plugin->nb_g_types = 2;
	plugin->valid_g_types = g_new (GType, plugin->nb_g_types);
	plugin->valid_g_types [0] = GDA_TYPE_BINARY;
	plugin->valid_g_types [1] = GDA_TYPE_BLOB;
	plugin->options_xml_spec = NULL;
	plugin->entry_create_func = plugin_entry_pict_create_func;
	plugin->cell_create_func = plugin_cell_renderer_pict_create_func;
	retlist = g_slist_append (retlist, plugin);

	file = gnome_db_gbr_get_data_dir_path ("gnome-db-entry-pict-spec.xml");
	if (! g_file_test (file, G_FILE_TEST_EXISTS)) {
		if (error && !*error)
			g_set_error (error, 0, 0, _("Missing spec. file '%s'"), file);
        }
	else {
		gsize len;
		g_file_get_contents (file, &(plugin->options_xml_spec), &len, error);
	}
	g_free (file);

	/* picture - string encoded */
	plugin = g_new0 (GnomeDbPlugin, 1);
	plugin->plugin_name = "picture_as_string";
	plugin->plugin_descr = "Picture entry for data stored as a string";
	plugin->plugin_file = NULL; /* always leave NULL */
	plugin->nb_g_types = 1;
	plugin->valid_g_types = g_new (GType, plugin->nb_g_types);
	plugin->valid_g_types [0] = G_TYPE_STRING;
	plugin->options_xml_spec = NULL;
	plugin->entry_create_func = plugin_entry_pict_create_func;
	plugin->cell_create_func = plugin_cell_renderer_pict_create_func;
	retlist = g_slist_append (retlist, plugin);

	file = gnome_db_gbr_get_data_dir_path ("gnome-db-entry-pict-spec_string.xml");
	if (! g_file_test (file, G_FILE_TEST_EXISTS)) {
		if (error && !*error)
			g_set_error (error, 0, 0, _("Missing spec. file '%s'"), file);
        }
	else {
		gsize len;
		g_file_get_contents (file, &(plugin->options_xml_spec), &len, error);
	}
	g_free (file);

	/* CGRID */
	plugin = g_new0 (GnomeDbPlugin, 1);
	plugin->plugin_name = "cgrid";
	plugin->plugin_descr = "ComboGrid entry";
	plugin->plugin_file = NULL; /* always leave NULL */
	plugin->nb_g_types = 1;
	plugin->valid_g_types = g_new (GType, plugin->nb_g_types);
	plugin->valid_g_types [0] = G_TYPE_STRING;;
	plugin->options_xml_spec = NULL;
	plugin->entry_create_func = plugin_entry_cgrid_create_func;
	plugin->cell_create_func = plugin_cell_renderer_cgrid_create_func;
	retlist = g_slist_append (retlist, plugin);

	return retlist;
}

static GnomeDbDataEntry *
plugin_entry_filesel_create_func (GdaDataHandler *handler, GType type, const gchar *options)
{
	return (GnomeDbDataEntry *) gnome_db_entry_filesel_new (handler, type, options);
}

static GnomeDbDataEntry *
plugin_entry_cidr_create_func (GdaDataHandler *handler, GType type, const gchar *options)
{
	return (GnomeDbDataEntry *) gnome_db_entry_cidr_new (handler, type);
}

static GnomeDbDataEntry *
plugin_entry_text_create_func (GdaDataHandler *handler, GType type, const gchar *options)
{
	return (GnomeDbDataEntry *) gnome_db_entry_text_new (handler, type);
}

static GnomeDbDataEntry *
plugin_entry_pict_create_func (GdaDataHandler *handler, GType type, const gchar *options)
{
	return (GnomeDbDataEntry *) gnome_db_entry_pict_new (handler, type, options);
}

static GtkCellRenderer *
plugin_cell_renderer_pict_create_func (GdaDataHandler *handler, GType type, const gchar *options)
{
	return gnome_db_data_cell_renderer_pict_new (handler, type, options);
}

static GnomeDbDataEntry *
plugin_entry_cgrid_create_func (GdaDataHandler *handler, GType type, const gchar *options)
{
	return (GnomeDbDataEntry *) gnome_db_entry_cgrid_new (handler, type, options);
}

static GtkCellRenderer *
plugin_cell_renderer_cgrid_create_func (GdaDataHandler *handler, GType type, const gchar *options)
{
	return (GtkCellRenderer *) gnome_db_data_cell_renderer_cgrid_new (handler, type, options);
}

#ifdef HAVE_LIBGCRYPT
static GnomeDbDataEntry *
plugin_entry_password_create_func (GdaDataHandler *handler, GType type, const gchar *options)
{
        return (GnomeDbDataEntry *) gnome_db_entry_password_new (handler, type, options);
}

static GtkCellRenderer *
plugin_cell_renderer_password_create_func (GdaDataHandler *handler, GType type, const gchar *options)
{
	return gnome_db_data_cell_renderer_password_new (handler, type, options);
}
#endif
