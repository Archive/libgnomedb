/* gnome-db-data-cell-renderer-pict.c
 *
 * Copyright (C) 2006 - 2007 Vivien Malerba <malerba@gnome-db.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <stdlib.h>
#include <string.h>
#include <libgda/libgda.h>
#include <glib/gi18n-lib.h>
#include "gnome-db-data-cell-renderer-pict.h"
#include <libgnomedb/marshal.h>
#include "common-pict.h"
#include <libgda/gda-enum-types.h>


static void gnome_db_data_cell_renderer_pict_get_property  (GObject *object,
							    guint param_id,
							    GValue *value,
							    GParamSpec *pspec);
static void gnome_db_data_cell_renderer_pict_set_property  (GObject *object,
							    guint param_id,
							    const GValue *value,
							    GParamSpec *pspec);
static void gnome_db_data_cell_renderer_pict_dispose       (GObject *object);

static void gnome_db_data_cell_renderer_pict_init       (GnomeDbDataCellRendererPict      *celltext);
static void gnome_db_data_cell_renderer_pict_class_init (GnomeDbDataCellRendererPictClass *class);
static void gnome_db_data_cell_renderer_pict_render     (GtkCellRenderer            *cell,
							 GdkWindow                  *window,
							 GtkWidget                  *widget,
							 GdkRectangle               *background_area,
							 GdkRectangle               *cell_area,
							 GdkRectangle               *expose_area,
							 GtkCellRendererState        flags);
static void gnome_db_data_cell_renderer_pict_get_size   (GtkCellRenderer            *cell,
							 GtkWidget                  *widget,
							 GdkRectangle               *cell_area,
							 gint                       *x_offset,
							 gint                       *y_offset,
							 gint                       *width,
							 gint                       *height);
static gboolean gnome_db_data_cell_renderer_pict_activate  (GtkCellRenderer            *cell,
							    GdkEvent                   *event,
							    GtkWidget                  *widget,
							    const gchar                *path,
							    GdkRectangle               *background_area,
							    GdkRectangle               *cell_area,
							    GtkCellRendererState        flags);

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass  *parent_class = NULL;

enum {
	CHANGED,
	LAST_SIGNAL
};


struct _GnomeDbDataCellRendererPictPrivate 
{
	GdaDataHandler       *dh;
        GType                 type;
        GValue               *value;
	PictBinData           bindata;
	PictOptions           options;
	PictAllocation        size;
	PictMenu              popup_menu;
	gboolean              to_be_deleted;

	gboolean              editable;
	gboolean              active;
	gboolean              null;
};

enum {
	PROP_0,
	PROP_VALUE,
	PROP_VALUE_ATTRIBUTES,
	PROP_EDITABLE,
	PROP_TO_BE_DELETED
};

static guint pixbuf_cell_signals[LAST_SIGNAL] = { 0 };


GType
gnome_db_data_cell_renderer_pict_get_type (void)
{
	static GType cell_type = 0;

	if (!cell_type) {
		static const GTypeInfo cell_info = {
			sizeof (GnomeDbDataCellRendererPictClass),
			NULL,		/* base_init */
			NULL,		/* base_finalize */
			(GClassInitFunc) gnome_db_data_cell_renderer_pict_class_init,
			NULL,		/* class_finalize */
			NULL,		/* class_data */
			sizeof (GnomeDbDataCellRendererPict),
			0,              /* n_preallocs */
			(GInstanceInitFunc) gnome_db_data_cell_renderer_pict_init,
		};
		
		cell_type = g_type_register_static (GTK_TYPE_CELL_RENDERER_PIXBUF, "GnomeDbDataCellRendererPict",
						    &cell_info, 0);
	}

	return cell_type;
}

static void
notify_property_cb (GtkCellRenderer *cell, GParamSpec *pspec, gpointer data)
{
	if (!strcmp (pspec->name, "stock-size")) {
		GnomeDbDataCellRendererPict *pictcell;
		guint size;

		pictcell = (GnomeDbDataCellRendererPict *) cell;
		g_object_get ((GObject *) cell, "stock-size", &size, NULL);
		gtk_icon_size_lookup (size, &(pictcell->priv->size.width), &(pictcell->priv->size.height));
		common_pict_clear_pixbuf_cache (&(pictcell->priv->options));
	}
}

static void
gnome_db_data_cell_renderer_pict_init (GnomeDbDataCellRendererPict *cell)
{
	cell->priv = g_new0 (GnomeDbDataCellRendererPictPrivate, 1);
	cell->priv->dh = NULL;
	cell->priv->type = GDA_TYPE_BINARY;
	cell->priv->editable = FALSE;

	cell->priv->bindata.data = NULL;
	cell->priv->bindata.data_length = 0;
	cell->priv->options.encoding = ENCODING_NONE;
	cell->priv->options.serialize = FALSE;
	common_pict_init_cache (&(cell->priv->options));
	
	gtk_icon_size_lookup (GTK_ICON_SIZE_DIALOG, &(cell->priv->size.width), &(cell->priv->size.height));

	GTK_CELL_RENDERER (cell)->mode = GTK_CELL_RENDERER_MODE_ACTIVATABLE;
	GTK_CELL_RENDERER (cell)->xpad = 2;
	GTK_CELL_RENDERER (cell)->ypad = 2;

	g_signal_connect (G_OBJECT (cell), "notify",
			  G_CALLBACK (notify_property_cb), NULL);
}

static void
gnome_db_data_cell_renderer_pict_class_init (GnomeDbDataCellRendererPictClass *class)
{
	GObjectClass *object_class = G_OBJECT_CLASS (class);
	GtkCellRendererClass *cell_class = GTK_CELL_RENDERER_CLASS (class);

	parent_class = g_type_class_peek_parent (class);

	object_class->get_property = gnome_db_data_cell_renderer_pict_get_property;
	object_class->set_property = gnome_db_data_cell_renderer_pict_set_property;
	object_class->dispose = gnome_db_data_cell_renderer_pict_dispose;

	cell_class->get_size = gnome_db_data_cell_renderer_pict_get_size;
	cell_class->render = gnome_db_data_cell_renderer_pict_render;
	cell_class->activate = gnome_db_data_cell_renderer_pict_activate;
  
	g_object_class_install_property (object_class,
					 PROP_VALUE,
					 g_param_spec_boxed ("value",
                                                               _("Value"),
                                                               _("GValue to render"),
                                                              G_TYPE_VALUE,
                                                               G_PARAM_READWRITE));
  
	g_object_class_install_property (object_class,
					 PROP_VALUE_ATTRIBUTES,
					 g_param_spec_flags ("value_attributes", NULL, NULL, GDA_TYPE_VALUE_ATTRIBUTE,
                                                            GDA_VALUE_ATTR_NONE, G_PARAM_READWRITE));

	g_object_class_install_property (object_class,
					 PROP_EDITABLE,
					 g_param_spec_boolean ("editable",
							       _("Editable"),
							       _("The toggle button can be activated"),
							       TRUE,
							       G_PARAM_READABLE |
							       G_PARAM_WRITABLE));

	g_object_class_install_property (object_class,
					 PROP_TO_BE_DELETED,
					 g_param_spec_boolean ("to_be_deleted", NULL, NULL, FALSE,
                                                               G_PARAM_WRITABLE));

	pixbuf_cell_signals[CHANGED] = g_signal_new ("changed",
						     G_OBJECT_CLASS_TYPE (object_class),
						     G_SIGNAL_RUN_LAST,
						     G_STRUCT_OFFSET (GnomeDbDataCellRendererPictClass, changed),
						     NULL, NULL,
						     gnome_db_marshal_VOID__STRING_BOXED,
						     G_TYPE_NONE, 2,
						     G_TYPE_STRING,
						     G_TYPE_VALUE);
}

static void
gnome_db_data_cell_renderer_pict_dispose (GObject *object)
{
	GnomeDbDataCellRendererPict *cell;

	g_return_if_fail (object != NULL);
	g_return_if_fail (GNOME_DB_IS_DATA_CELL_RENDERER_PICT (object));
	cell = GNOME_DB_DATA_CELL_RENDERER_PICT (object);

	if (cell->priv) {
		g_hash_table_destroy (cell->priv->options.pixbuf_hash);

		/* the private area itself */
		g_free (cell->priv);
		cell->priv = NULL;
	}

	/* for the parent class */
	parent_class->dispose (object);
}

static void
gnome_db_data_cell_renderer_pict_get_property (GObject *object,
					       guint param_id,
					       GValue *value,
					       GParamSpec *pspec)
{
	GnomeDbDataCellRendererPict *cell = GNOME_DB_DATA_CELL_RENDERER_PICT (object);
  
	switch (param_id) {
	case PROP_VALUE:
		g_value_set_boxed (value, cell->priv->value);
		break;
	case PROP_VALUE_ATTRIBUTES:
		break;
	case PROP_EDITABLE:
		g_value_set_boolean (value, cell->priv->editable);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}


static void
gnome_db_data_cell_renderer_pict_set_property (GObject *object,
					       guint param_id,
					       const GValue *value,
					       GParamSpec *pspec)
{
	GnomeDbDataCellRendererPict *cell = GNOME_DB_DATA_CELL_RENDERER_PICT (object);
  
	switch (param_id) {
	case PROP_VALUE:
		/* Because we don't have a copy of the value, we MUST NOT free it! */
                cell->priv->value = NULL;
		g_object_set (G_OBJECT (cell), "pixbuf", NULL, "stock-id", NULL, NULL);
		if (value) {
                        GValue *gval = g_value_get_boxed (value);
			GdkPixbuf *pixbuf = NULL;
			const gchar *stock = NULL;
			GError *error = NULL;
			
			if (cell->priv->bindata.data) {
				g_free (cell->priv->bindata.data);
				cell->priv->bindata.data = NULL;
				cell->priv->bindata.data_length = 0;
			}
			
			/* fill in cell->priv->data */
			if (common_pict_load_data (&(cell->priv->options), gval, &(cell->priv->bindata), &stock, &error)) {
				/* try to make a pixbuf */
				pixbuf = common_pict_fetch_cached_pixbuf (&(cell->priv->options), gval);
				if (pixbuf)
					g_object_ref (pixbuf);
				else {
					pixbuf = common_pict_make_pixbuf (&(cell->priv->options), 
									  &(cell->priv->bindata), &(cell->priv->size),
									  &stock, &error);
					if (pixbuf) 
						common_pict_add_cached_pixbuf (&(cell->priv->options), gval, pixbuf);
				}
				
				if (!pixbuf && !stock)
					stock = GTK_STOCK_MISSING_IMAGE;
			}

			/* display something */
			if (pixbuf) {
				g_object_set (G_OBJECT (cell), "pixbuf", pixbuf, NULL);
				g_object_unref (pixbuf);
			}
			
			if (stock) 
				g_object_set (G_OBJECT (cell), "stock-id", stock, NULL);
			if (error)
				g_error_free (error);
			
                        cell->priv->value = gval;
                }

                g_object_notify (object, "value");
		break;
	case PROP_VALUE_ATTRIBUTES:
		break;
	case PROP_EDITABLE:
		cell->priv->editable = g_value_get_boolean (value);
		/* FIXME */
		/*g_object_notify (G_OBJECT(object), "editable");*/
		break;
	case PROP_TO_BE_DELETED:
		cell->priv->to_be_deleted = g_value_get_boolean (value);
		break;
	default:
		G_OBJECT_WARN_INVALID_PROPERTY_ID (object, param_id, pspec);
		break;
	}
}

/**
 * gnome_db_data_cell_renderer_pict_new:
 * @dh: a #GdaDataHandler object
 * @type:
 * @options: options string
 * 
 * Creates a new #GnomeDbDataCellRendererPict. Adjust rendering
 * parameters using object properties. Object properties can be set
 * globally (with g_object_set()). Also, with #GtkTreeViewColumn, you
 * can bind a property to a value in a #GtkTreeModel. For example, you
 * can bind the "active" property on the cell renderer to a pict value
 * in the model, thus causing the check button to reflect the state of
 * the model.
 * 
 * Return value: the new cell renderer
 */
GtkCellRenderer *
gnome_db_data_cell_renderer_pict_new (GdaDataHandler *dh, GType type, const gchar *options)
{
	GObject *obj;
        GnomeDbDataCellRendererPict *cell;

        g_return_val_if_fail (dh && GDA_IS_DATA_HANDLER (dh), NULL);
        obj = g_object_new (GNOME_DB_TYPE_DATA_CELL_RENDERER_PICT, "stock-size", GTK_ICON_SIZE_DIALOG, NULL);
	
        cell = GNOME_DB_DATA_CELL_RENDERER_PICT (obj);
        cell->priv->dh = dh;
        g_object_ref (G_OBJECT (dh));
        cell->priv->type = type;

	common_pict_parse_options (&(cell->priv->options), options);
	
        return GTK_CELL_RENDERER (obj);
}

static void
gnome_db_data_cell_renderer_pict_get_size (GtkCellRenderer *cell,
					   GtkWidget       *widget,
					   GdkRectangle    *cell_area,
					   gint            *x_offset,
					   gint            *y_offset,
					   gint            *width,
					   gint            *height)
{
	/* FIXME */
	/* GtkIconSize */
	GtkCellRendererClass *pixbuf_class = g_type_class_peek (GTK_TYPE_CELL_RENDERER_PIXBUF);

	(pixbuf_class->get_size) (cell, widget, cell_area, x_offset, y_offset, width, height);
}

static void
gnome_db_data_cell_renderer_pict_render (GtkCellRenderer      *cell,
					 GdkWindow            *window,
					 GtkWidget            *widget,
					 GdkRectangle         *background_area,
					 GdkRectangle         *cell_area,
					 GdkRectangle         *expose_area,
					 GtkCellRendererState  flags)
{
	GtkCellRendererClass *pixbuf_class = g_type_class_peek (GTK_TYPE_CELL_RENDERER_PIXBUF);

	(pixbuf_class->render) (cell, window, widget, background_area, cell_area, expose_area, flags);

	if (GNOME_DB_DATA_CELL_RENDERER_PICT (cell)->priv->to_be_deleted)
		gtk_paint_hline (widget->style,
				 window, GTK_STATE_SELECTED,
				 cell_area, 
				 widget,
				 "hline",
				 cell_area->x + cell->xpad, cell_area->x + cell_area->width - cell->xpad,
				 cell_area->y + cell_area->height / 2.);

}

static void
pict_data_changed_cb (GnomeDbDataCellRendererPict *pictcell)
{
	GValue *value;

	value = common_pict_get_value (&(pictcell->priv->bindata), &(pictcell->priv->options), 
				       pictcell->priv->type);
	g_signal_emit (G_OBJECT (pictcell), pixbuf_cell_signals[CHANGED], 0, 
		       g_object_get_data (G_OBJECT (pictcell), "last_path"), value);
	gda_value_free (value);
}

static gboolean
gnome_db_data_cell_renderer_pict_activate  (GtkCellRenderer            *cell,
					    GdkEvent                   *event,
					    GtkWidget                  *widget,
					    const gchar                *path,
					    GdkRectangle               *background_area,
					    GdkRectangle               *cell_area,
					    GtkCellRendererState        flags)
{
	GnomeDbDataCellRendererPict *pictcell;

	pictcell = GNOME_DB_DATA_CELL_RENDERER_PICT (cell);
	if (pictcell->priv->editable) {
		int event_time;

		g_object_set_data_full (G_OBJECT (pictcell), "last_path", g_strdup (path), g_free);
		if (!pictcell->priv->popup_menu.menu) 
			common_pict_create_menu (&(pictcell->priv->popup_menu), widget, &(pictcell->priv->bindata), 
						 &(pictcell->priv->options),
						 (PictCallback) pict_data_changed_cb, pictcell);

		common_pict_adjust_menu_sensitiveness (&(pictcell->priv->popup_menu), pictcell->priv->editable, 
						       &(pictcell->priv->bindata));
		event_time = gtk_get_current_event_time ();
		gtk_menu_popup (GTK_MENU (pictcell->priv->popup_menu.menu), NULL, NULL, NULL, NULL, 
				0, event_time);
	}

	return FALSE;
}


