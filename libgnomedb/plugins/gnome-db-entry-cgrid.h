/* gnome-db-entry-cgrid.h
 *
 * Copyright (C) 2007-2007 Carlos Savoretti
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNOME_DB_ENTRY_CGRID_H__
#define __GNOME_DB_ENTRY_CGRID_H__

#include <libgnomedb/data-entries/gnome-db-entry-wrapper.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_ENTRY_CGRID            (gnome_db_entry_cgrid_get_type ())
#define GNOME_DB_ENTRY_CGRID(obj)            (G_TYPE_CHECK_INSTANCE_CAST ((obj), GNOME_DB_TYPE_ENTRY_CGRID, GnomeDbEntryCGrid))
#define GNOME_DB_ENTRY_CGRID_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST ((klass), GNOME_DB_TYPE_ENTRY_CGRID, GnomeDbEntryCGridClass))
#define GNOME_DB_IS_ENTRY_CGRID(obj)         (G_TYPE_CHECK_INSTANCE_TYPE ((obj), GNOME_DB_TYPE_ENTRY_CGRID))
#define GNOME_DB_IS_ENTRY_CGRID_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GNOME_DB_TYPE_ENTRY_CGRID))
#define GNOME_DB_ENTRY_CGRID_GET_CLASS(obj)  (G_TYPE_INSTANCE_GET_CLASS ((obj), GNOME_DB_TYPE_ENTRY_CGRID, GnomeDbEntryCGridClass))

typedef struct _GnomeDbEntryCGrid GnomeDbEntryCGrid;
typedef struct _GnomeDbEntryCGridClass GnomeDbEntryCGridClass;
typedef struct _GnomeDbEntryCGridPrivate GnomeDbEntryCGridPrivate;

typedef void (* GnomeDbEntryCGridCgridChangedFunc) (GnomeDbEntryCGrid  *cgrid);

struct _GnomeDbEntryCGrid
{
	GnomeDbEntryWrapper       parent;
	GnomeDbEntryCGridPrivate *priv;
};

struct _GnomeDbEntryCGridClass
{
	GnomeDbEntryWrapperClass   parent;
	void                      (* cgrid_changed) (GnomeDbEntryCGrid  *cgrid);
};

GType               gnome_db_entry_cgrid_get_type            (void) G_GNUC_CONST;
GnomeDbEntryCGrid  *gnome_db_entry_cgrid_new                 (GdaDataHandler *data_handler, GType gtype, 
							      const gchar *options);
gint                gnome_db_entry_cgrid_get_text_column     (GnomeDbEntryCGrid *cgrid);
void                gnome_db_entry_cgrid_set_text_column     (GnomeDbEntryCGrid *cgrid, gint text_column);
gint                gnome_db_entry_cgrid_get_grid_height     (GnomeDbEntryCGrid *cgrid);
void                gnome_db_entry_cgrid_set_grid_height     (GnomeDbEntryCGrid *cgrid, gint grid_height);
gboolean            gnome_db_entry_cgrid_get_headers_visible (GnomeDbEntryCGrid *cgrid);
void                gnome_db_entry_cgrid_set_headers_visible (GnomeDbEntryCGrid *cgrid, gboolean headers_visible);
GdaDataModel       *gnome_db_entry_cgrid_get_model           (GnomeDbEntryCGrid *cgrid);
void                gnome_db_entry_cgrid_set_model           (GnomeDbEntryCGrid *cgrid, GdaDataModel *model);
void                gnome_db_entry_cgrid_append_column       (GnomeDbEntryCGrid *cgrid, GtkTreeViewColumn *column);
gboolean            gnome_db_entry_cgrid_get_active_iter     (GnomeDbEntryCGrid *cgrid, GtkTreeIter *iter);

G_END_DECLS

#endif 
