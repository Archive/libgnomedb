/* GNOME DB library
 * Copyright (C) 1999 - 2008 The GNOME Foundation.
 *
 * AUTHORS:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __GNOME_DB_CONNECTION_PROPERTIES_H__
#define __GNOME_DB_CONNECTION_PROPERTIES_H__

#include <libgda/gda-connection.h>
#include <gtk/gtkvbox.h>

G_BEGIN_DECLS

#define GNOME_DB_TYPE_CONNECTION_PROPERTIES            (gnome_db_connection_properties_get_type())
#define GNOME_DB_CONNECTION_PROPERTIES(obj)            (G_TYPE_CHECK_INSTANCE_CAST (obj, GNOME_DB_TYPE_CONNECTION_PROPERTIES, GnomeDbConnectionProperties))
#define GNOME_DB_CONNECTION_PROPERTIES_CLASS(klass)    (G_TYPE_CHECK_CLASS_CAST (klass, GNOME_DB_TYPE_CONNECTION_PROPERTIES, GnomeDbConnectionPropertiesClass))
#define GNOME_DB_IS_CONNECTION_PROPERTIES(obj)         (G_TYPE_CHECK_INSTANCE_TYPE (obj, GNOME_DB_TYPE_CONNECTION_PROPERTIES))
#define GNOME_DB_IS_CONNECTION_PROPERTIES_CLASS(klass) (G_TYPE_CHECK_CLASS_TYPE ((klass), GNOME_DB_TYPE_CONNECTION_PROPERTIES))

typedef struct _GnomeDbConnectionProperties        GnomeDbConnectionProperties;
typedef struct _GnomeDbConnectionPropertiesClass   GnomeDbConnectionPropertiesClass;
typedef struct _GnomeDbConnectionPropertiesPrivate GnomeDbConnectionPropertiesPrivate;

struct _GnomeDbConnectionProperties {
	GtkVBox vbox;
	GnomeDbConnectionPropertiesPrivate *priv;
};

struct _GnomeDbConnectionPropertiesClass {
	GtkVBoxClass parent_class;
};

GType          gnome_db_connection_properties_get_type       (void) G_GNUC_CONST;
GtkWidget     *gnome_db_connection_properties_new            (GdaConnection *cnc);
GdaConnection *gnome_db_connection_properties_get_connection (GnomeDbConnectionProperties *props);
void           gnome_db_connection_properties_set_connection (GnomeDbConnectionProperties *props,
							      GdaConnection *cnc);

G_END_DECLS

#endif
