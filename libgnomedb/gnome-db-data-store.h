/* gnome-db-data-store.h
 *
 * Copyright (C) 2005 - 2006 Vivien Malerba
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef __GNOME_DB_DATA_STORE__
#define __GNOME_DB_DATA_STORE__

#include <gtk/gtktreemodel.h>
#include <libgda/gda-data-model.h>
#include <libgda/gda-data-proxy.h>
#include "gnome-db-decl.h"

G_BEGIN_DECLS

#define GNOME_DB_TYPE_DATA_STORE          (gnome_db_data_store_get_type())
#define GNOME_DB_DATA_STORE(obj)          G_TYPE_CHECK_INSTANCE_CAST (obj, gnome_db_data_store_get_type(), GnomeDbDataStore)
#define GNOME_DB_DATA_STORE_CLASS(klass)  G_TYPE_CHECK_CLASS_CAST (klass, gnome_db_data_store_get_type (), GnomeDbDataStoreClass)
#define GNOME_DB_IS_DATA_STORE(obj)       G_TYPE_CHECK_INSTANCE_TYPE (obj, gnome_db_data_store_get_type ())

enum {
	DATA_STORE_COL_MODEL_N_COLUMNS = -2, /* number of columns in the GdaDataModel */
	DATA_STORE_COL_MODEL_POINTER = -3, /* pointer to the GdaDataModel */
	DATA_STORE_COL_MODEL_ROW = -4, /* row number in the GdaDataModel, or -1 for new rows */
	DATA_STORE_COL_MODIFIED = -5, /* TRUE if row has been modified */
	DATA_STORE_COL_TO_DELETE = -6, /* TRUE if row is marked to be deleted */
};

/* struct for the object's data */
struct _GnomeDbDataStore
{
	GObject                object;

	GnomeDbDataStorePriv  *priv;
};

/* struct for the object's class */
struct _GnomeDbDataStoreClass
{
	GObjectClass           parent_class;
};


GType           gnome_db_data_store_get_type             (void) G_GNUC_CONST;
GtkTreeModel   *gnome_db_data_store_new                  (GdaDataModel *model);

GdaDataProxy   *gnome_db_data_store_get_proxy            (GnomeDbDataStore *store);
gint            gnome_db_data_store_get_row_from_iter    (GnomeDbDataStore *store, GtkTreeIter *iter);       
gboolean        gnome_db_data_store_get_iter_from_values (GnomeDbDataStore *store, GtkTreeIter *iter,
							  GSList *values, gint *cols_index);

gboolean        gnome_db_data_store_set_value            (GnomeDbDataStore *store, GtkTreeIter *iter,
						          gint col, const GValue *value);
void            gnome_db_data_store_delete               (GnomeDbDataStore *store, GtkTreeIter *iter);
void            gnome_db_data_store_undelete             (GnomeDbDataStore *store, GtkTreeIter *iter);
gboolean        gnome_db_data_store_append               (GnomeDbDataStore *store, GtkTreeIter *iter);

G_END_DECLS

#endif
