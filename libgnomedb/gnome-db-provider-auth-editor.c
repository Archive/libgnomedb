/* GNOME DB library
 * Copyright (C) 2008 The GNOME Foundation
 *
 * AUTHORS:
 *      Vivien Malerba <malerba@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <string.h>
#include <gtk/gtklabel.h>
#include <gtk/gtktable.h>
#include <libgda/libgda.h>
#include <libgnomedb/gnome-db-provider-auth-editor.h>
#include <libgnomedb/gnome-db-util.h>
#include <libgnomedb/gnome-db-basic-form.h>
#include <glib/gi18n-lib.h>

struct _GnomeDbProviderAuthEditorPrivate {
	gchar       *provider;

	GtkWidget   *auth_widget;
	gboolean     auth_needed;
};

static void gnome_db_provider_auth_editor_class_init (GnomeDbProviderAuthEditorClass *klass);
static void gnome_db_provider_auth_editor_init       (GnomeDbProviderAuthEditor *auth,
						      GnomeDbProviderAuthEditorClass *klass);
static void gnome_db_provider_auth_editor_finalize   (GObject *object);

static void gnome_db_provider_auth_editor_set_property (GObject *object,
							guint param_id,
							const GValue *value,
							GParamSpec *pauth);
static void gnome_db_provider_auth_editor_get_property (GObject *object,
							guint param_id,
							GValue *value,
							GParamSpec *pauth);

enum {
	PROP_0,
	PROP_PROVIDER
};

enum {
	CHANGED,
	LAST_SIGNAL
};


static gint gnome_db_provider_auth_editor_signals[LAST_SIGNAL] = { 0 };
static GObjectClass *parent_class = NULL;

/*
 * GnomeDbProviderAuthEditor class implementation
 */

static void
gnome_db_provider_auth_editor_class_init (GnomeDbProviderAuthEditorClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gnome_db_provider_auth_editor_finalize;
	object_class->set_property = gnome_db_provider_auth_editor_set_property;
	object_class->get_property = gnome_db_provider_auth_editor_get_property;
	klass->changed = NULL;

	g_object_class_install_property (object_class, PROP_PROVIDER,
	                                 g_param_spec_string ("provider", NULL, NULL, NULL,
					                      G_PARAM_READWRITE));

	/* add class signals */
	gnome_db_provider_auth_editor_signals[CHANGED] =
		g_signal_new ("changed",
			      G_TYPE_FROM_CLASS (object_class),
			      G_SIGNAL_RUN_LAST,
			      G_STRUCT_OFFSET (GnomeDbProviderAuthEditorClass, changed),
			      NULL, NULL,
			      g_cclosure_marshal_VOID__VOID,
			      G_TYPE_NONE, 0);
}

static void
auth_form_changed (GnomeDbBasicForm *form, GdaHolder *param, gboolean is_user_modif, GnomeDbProviderAuthEditor *auth)
{
	if (! is_user_modif)
		return;

	g_signal_emit (auth, gnome_db_provider_auth_editor_signals[CHANGED], 0, NULL);
}

static void
gnome_db_provider_auth_editor_init (GnomeDbProviderAuthEditor *auth, GnomeDbProviderAuthEditorClass *klass)
{
	g_return_if_fail (GNOME_DB_IS_PROVIDER_AUTH_EDITOR (auth));

	auth->priv = g_new0 (GnomeDbProviderAuthEditorPrivate, 1);
	auth->priv->provider = NULL;
	auth->priv->auth_needed = FALSE;
}

static void
gnome_db_provider_auth_editor_finalize (GObject *object)
{
	GnomeDbProviderAuthEditor *auth = (GnomeDbProviderAuthEditor *) object;

	g_return_if_fail (GNOME_DB_IS_PROVIDER_AUTH_EDITOR (auth));

	/* free memory */
	if (auth->priv->provider)
		g_free (auth->priv->provider);

	g_free (auth->priv);
	auth->priv = NULL;

	/* chain to parent class */
	parent_class->finalize (object);
}

static void
gnome_db_provider_auth_editor_set_property (GObject *object,
                                            guint param_id,
                                            const GValue *value,
                                            GParamSpec *pauth)
{
	GnomeDbProviderAuthEditor *auth;
	auth = GNOME_DB_PROVIDER_AUTH_EDITOR (object);

	switch(param_id) {
	case PROP_PROVIDER:
		gnome_db_provider_auth_editor_set_provider (auth, g_value_get_string (value));
		break;
	}
}

static void
gnome_db_provider_auth_editor_get_property (GObject *object,
                                            guint param_id,
                                            GValue *value,
                                            GParamSpec *pauth)
{
	GnomeDbProviderAuthEditor *auth;
	auth = GNOME_DB_PROVIDER_AUTH_EDITOR (object);

	switch (param_id) {
	case PROP_PROVIDER:
		g_value_set_string (value, auth->priv->provider);
		break;
	}
}

GType
gnome_db_provider_auth_editor_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0)) {
		static const GTypeInfo info = {
			sizeof (GnomeDbProviderAuthEditorClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_provider_auth_editor_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbProviderAuthEditor),
			0,
			(GInstanceInitFunc) gnome_db_provider_auth_editor_init
		};
		type = g_type_register_static (GTK_TYPE_VBOX, "GnomeDbProviderAuthEditor",
					       &info, 0);
	}
	return type;
}

/**
 * gnome_db_provider_auth_editor_new
 * @provider: the provider to be used 
 *
 * Creates a new #GnomeDbProviderAuthEditor widget
 *
 * Returns:
 */
GtkWidget *
gnome_db_provider_auth_editor_new (const gchar *provider)
{
	GnomeDbProviderAuthEditor *auth;

	auth = g_object_new (GNOME_DB_TYPE_PROVIDER_AUTH_EDITOR,
	                     "provider", provider, NULL);

	return GTK_WIDGET (auth);
}

/**
 * gnome_db_provider_auth_editor_set_provider
 * @auth: a #GnomeDbProviderAuthEditor widget
 * @provider: the provider to be used 
 *
 * Updates the displayed fields in @auth to represent the required
 * and possible arguments that a connection to a database through 
 * @provider would require
 */
void
gnome_db_provider_auth_editor_set_provider (GnomeDbProviderAuthEditor *auth, const gchar *provider)
{
	GdaProviderInfo *pinfo = NULL;
	g_return_if_fail (GNOME_DB_IS_PROVIDER_AUTH_EDITOR (auth));
	g_return_if_fail (auth->priv);

	if (auth->priv->provider)
		g_free (auth->priv->provider);
	auth->priv->provider = NULL;
	auth->priv->auth_needed = FALSE;

	if (auth->priv->auth_widget) {
		gtk_widget_destroy (auth->priv->auth_widget);
		auth->priv->auth_widget = NULL;
	}

	if (provider) {
		pinfo = gda_config_get_provider_info (provider);
		if (pinfo) {
			auth->priv->provider = g_strdup (pinfo->id);
			if (pinfo->auth_params && pinfo->auth_params->holders)
				auth->priv->auth_needed = TRUE;
		}
	}

	if (auth->priv->auth_needed) {
		g_assert (pinfo);
		GdaSet *set;
		
		set = gda_set_copy (pinfo->auth_params);
		auth->priv->auth_widget = gnome_db_basic_form_new (set);
		g_signal_connect (G_OBJECT (auth->priv->auth_widget), "param-changed",
				  G_CALLBACK (auth_form_changed), auth);
		g_object_unref (set);
	}
	else if (provider)
		auth->priv->auth_widget = gtk_label_new (_("No authentication necessary."));
		
	if (auth->priv->auth_widget) {
		gtk_container_add (GTK_CONTAINER (auth), auth->priv->auth_widget);
		gtk_widget_show (auth->priv->auth_widget);
	}

	g_signal_emit (auth, gnome_db_provider_auth_editor_signals[CHANGED], 0);
}

/**
 * gnome_db_provider_auth_editor_is_valid
 * @auth: a #GnomeDbProviderAuthEditor widget
 * 
 * Tells if the current information displayed in @auth reauthts the
 * provider's authifications (about non NULL values for example)
 *
 * Returns:
 */
gboolean
gnome_db_provider_auth_editor_is_valid (GnomeDbProviderAuthEditor *auth)
{
	g_return_val_if_fail (GNOME_DB_IS_PROVIDER_AUTH_EDITOR (auth), FALSE);
	g_return_val_if_fail (auth->priv, FALSE);

	if (auth->priv->auth_needed) {
		g_assert (auth->priv->auth_widget);
		return gnome_db_basic_form_is_valid (GNOME_DB_BASIC_FORM (auth->priv->auth_widget));
	}
	else
		return TRUE;
}

static gchar *
params_to_string (GnomeDbProviderAuthEditor *auth)
{
	GString *string = NULL;
	gchar *str;
	GdaSet *dset;
	GSList *list;

	g_assert (auth->priv->auth_widget);
	if (! GNOME_DB_IS_BASIC_FORM (auth->priv->auth_widget))
		return NULL;

	dset = gnome_db_basic_form_get_data_set (GNOME_DB_BASIC_FORM (auth->priv->auth_widget));
	for (list = dset->holders; list; list = list->next) {
		GdaHolder *param = GDA_HOLDER (list->data);
		if (gda_holder_is_valid (param)) {
			const GValue *value;
			value = gda_holder_get_value (param);
			str = NULL;
			if (value && !gda_value_is_null ((GValue *) value)) {
				GdaDataHandler *dh;
				GType dtype;

				dtype = gda_holder_get_g_type (param);
				dh = gda_get_default_handler (dtype);
				str = gda_data_handler_get_str_from_value (dh, value);
			}
			if (str && *str) {
				gchar *name;
				gchar *ename, *evalue;
				if (!string)
					string = g_string_new ("");
				else
					g_string_append_c (string, ';');
				g_object_get (G_OBJECT (list->data), "id", &name, NULL);
				ename = gda_rfc1738_encode (name);
				evalue = gda_rfc1738_encode (str);
				g_string_append_printf (string, "%s=%s", ename, evalue);
				g_free (ename);
				g_free (evalue);
			}
			g_free (str);
		}		
	}

	str = string ? string->str : NULL;
	if (string)
		g_string_free (string, FALSE);
	return str;
}

/**
 * gnome_db_provider_auth_editor_get_auths
 * @auth: a #GnomeDbProviderAuthEditor widget
 *
 * Get the currently displayed provider's authific
 * connection string
 *
 * Returns: a new string, or %NULL if no provider have been set, or no authentication is necessary
 */
gchar *
gnome_db_provider_auth_editor_get_auth (GnomeDbProviderAuthEditor *auth)
{
	g_return_val_if_fail (GNOME_DB_IS_PROVIDER_AUTH_EDITOR (auth), NULL);
	g_return_val_if_fail (auth->priv, NULL);

	if (auth->priv->auth_needed) {
		g_assert (auth->priv->auth_widget);
		return params_to_string (auth);
	}
	else
		return NULL;
}

/**
 * gnome_db_provider_auth_editor_set_auths
 * @auth: a #GnomeDbProviderAuthEditor widget
 * @auth_string: 
 *
 * Sets the connection string to be displayed in the widget
 */
void
gnome_db_provider_auth_editor_set_auth (GnomeDbProviderAuthEditor *auth, const gchar *auth_string)
{
	g_return_if_fail (GNOME_DB_IS_PROVIDER_AUTH_EDITOR (auth));
	g_return_if_fail (auth->priv);

	if (!auth->priv->auth_needed) {
		if (auth_string && *auth_string)
			g_warning (_("Can't set authentification string: no authentication is needed"));
		return;
	}

	gnome_db_basic_form_reset (GNOME_DB_BASIC_FORM (auth->priv->auth_widget));
	if (auth_string) {
		/* split array in a list of named parameters, and for each parameter value, set the correcponding
		   parameter in @dset */
		GdaSet *dset;
		GSList *params_set = NULL;
		g_assert (auth->priv->auth_widget);

		dset = gnome_db_basic_form_get_data_set (GNOME_DB_BASIC_FORM (auth->priv->auth_widget));
		gchar **array = NULL;
		array = g_strsplit (auth_string, ";", 0);
		if (array) {
			gint index = 0;
			gchar *tok;
			gchar *value;
			gchar *name;
			
			for (index = 0; array[index]; index++) {
				name = strtok_r (array [index], "=", &tok);
				if (name)
					value = strtok_r (NULL, "=", &tok);
				else
					value = NULL;
				if (name && value) {
					GdaHolder *param;
					gda_rfc1738_decode (name);
					gda_rfc1738_decode (value);
					
					param = gda_set_get_holder (dset, name);
					if (param)
						if (gda_holder_set_value_str (param, NULL, value, NULL))
							params_set = g_slist_prepend (params_set, param);
				}
			}
			
			g_strfreev (array);
		}
	}

	g_signal_emit (auth, gnome_db_provider_auth_editor_signals[CHANGED], 0);
}
