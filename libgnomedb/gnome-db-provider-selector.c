/* GNOME DB library
 * Copyright (C) 1999 - 2008 The GNOME Foundation.
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *      Vivien Malerba <malerba@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <string.h>
#include <libgda/gda-config.h>
#include <libgnomedb/gnome-db-provider-selector.h>
#include <libgnomedb/gnome-db-util.h>
#include <libgnomedb/gnome-db-combo.h>

#define PARENT_TYPE GNOME_DB_TYPE_COMBO

struct _GnomeDbProviderSelectorPrivate {
};

static void gnome_db_provider_selector_class_init (GnomeDbProviderSelectorClass *klass);
static void gnome_db_provider_selector_init       (GnomeDbProviderSelector *selector,
						   GnomeDbProviderSelectorClass *klass);
static void gnome_db_provider_selector_finalize   (GObject *object);

static GObjectClass *parent_class = NULL;

/* column to display */
static gint cols[] = {0};

/*
 * Private functions
 */

static void
show_providers (GnomeDbProviderSelector *selector)
{
	GdaDataModel *model;
	GSList *list;
	GValue *tmpval;

	model = gda_config_list_providers ();
	gnome_db_combo_set_model (GNOME_DB_COMBO (selector), model, 1, cols);

	g_value_set_string (tmpval = gda_value_new (G_TYPE_STRING), "SQLite");
	list = g_slist_append (NULL, tmpval);
	gnome_db_combo_set_values_ext (GNOME_DB_COMBO (selector), list, cols);
	gda_value_free ((GValue *)(list->data));
	g_slist_free (list);
	g_object_unref (model);
}

/*
 * GnomeDbProviderSelector class implementation
 */

static void
gnome_db_provider_selector_class_init (GnomeDbProviderSelectorClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gnome_db_provider_selector_finalize;
}

static void
gnome_db_provider_selector_init (GnomeDbProviderSelector *selector,
				 GnomeDbProviderSelectorClass *klass)
{
	g_return_if_fail (GNOME_DB_IS_PROVIDER_SELECTOR (selector));

	selector->priv = g_new0 (GnomeDbProviderSelectorPrivate, 1);
	show_providers (selector);
}

static void
gnome_db_provider_selector_finalize (GObject *object)
{
	GnomeDbProviderSelector *selector = (GnomeDbProviderSelector *) object;

	g_return_if_fail (GNOME_DB_IS_PROVIDER_SELECTOR (selector));

	g_free (selector->priv);
	selector->priv = NULL;

	parent_class->finalize (object);
}

GType
gnome_db_provider_selector_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0)) {
		static const GTypeInfo info = {
			sizeof (GnomeDbProviderSelectorClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_provider_selector_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbProviderSelector),
			0,
			(GInstanceInitFunc) gnome_db_provider_selector_init
		};
		type = g_type_register_static (PARENT_TYPE,
					       "GnomeDbProviderSelector",
					       &info, 0);
	}
	return type;
}

/**
 * gnome_db_provider_selector_new
 *
 * Create a new #GnomeDbProviderSelector widget.
 *
 * Returns: the newly created widget.
 */
GtkWidget *
gnome_db_provider_selector_new (void)
{
	GnomeDbProviderSelector *selector;

	selector = g_object_new (GNOME_DB_TYPE_PROVIDER_SELECTOR, NULL);
	return GTK_WIDGET (selector);
}

/**
 * gnome_db_provider_selector_get_provider
 * @selector: a #GnomeDbProviderSelector widget
 *
 * Get the selected provider.
 *
 * Returns: the selected provider
 */
const gchar *
gnome_db_provider_selector_get_provider (GnomeDbProviderSelector *selector)
{
	GSList *list;
	const gchar *str;

	g_return_val_if_fail (GNOME_DB_IS_PROVIDER_SELECTOR (selector), NULL);
	list = gnome_db_combo_get_values (GNOME_DB_COMBO (selector));
	if (list && list->data) {
		str = g_value_get_string ((GValue *)(list->data));
		g_slist_free (list);
		return str;
	}
	else
		return NULL;
}

/**
 * gnome_db_provider_selector_set_provider
 * @selector: a #GnomeDbProviderSelector widget
 * @provider: the provider to be selected, or %NULL for the default (SQLite)
 *
 * Forces @selector to be set on @provider
 *
 * Returns: TRUE if @provider has been selected
 */
gboolean
gnome_db_provider_selector_set_provider (GnomeDbProviderSelector *selector, const gchar *provider)
{
	GSList *list;
	gboolean retval;
	GValue *tmpval;

	g_return_val_if_fail (GNOME_DB_IS_PROVIDER_SELECTOR (selector), FALSE);

	if (provider && *provider)
		g_value_set_string (tmpval = gda_value_new (G_TYPE_STRING), provider);
	else 
		g_value_set_string (tmpval = gda_value_new (G_TYPE_STRING), "SQLite");

	list = g_slist_append (NULL, tmpval);
	retval = gnome_db_combo_set_values_ext (GNOME_DB_COMBO (selector), list, cols);
	gda_value_free ((GValue *)(list->data));
	g_slist_free (list);

	return retval;
}

/**
 * gnome_db_provider_selector_get_provider_obj
 * @selector: a #GnomeDbProviderSelector widget
 *
 * Get the selected provider as a #GdaServerProvider object
 *
 * Returns: a new #GdaServerProvider or %NULL if an error occurred
 */
GdaServerProvider *
gnome_db_provider_selector_get_provider_obj (GnomeDbProviderSelector *selector)
{
	GdaServerProvider *prov;
	const gchar *pname;
	
	g_return_val_if_fail (GNOME_DB_IS_PROVIDER_SELECTOR (selector), NULL);

	pname = gnome_db_provider_selector_get_provider (selector);
	if (pname)
		return gda_config_get_provider (pname, NULL);
	else
		return NULL;
}
