/* GNOME DB library
 * Copyright (C) 1999 - 2007 The GNOME Foundation.
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *      Vivien Malerba <malerba@gnome-db.org>
 *      Daniel Espinosa <esodan@gmail.com>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <string.h>
#include <libgda/gda-config.h>
#include <libgnomedb/gnome-db-dsn-selector.h>
#include <libgnomedb/gnome-db-util.h>
#include <gtk/gtkcelllayout.h>
#include <gtk/gtkcellrenderer.h>
#include <gtk/gtkcellrenderertext.h>

struct _GnomeDbDsnSelectorPrivate {
};

static void gnome_db_dsn_selector_class_init (GnomeDbDsnSelectorClass *klass);
static void gnome_db_dsn_selector_init       (GnomeDbDsnSelector *selector,
						      GnomeDbDsnSelectorClass *klass);
static void gnome_db_dsn_selector_finalize   (GObject *object);

static void gnome_db_dsn_selector_set_property(GObject *object,
                                                       guint param_id,
                                                       const GValue *value,
                                                       GParamSpec *pspec);
static void gnome_db_dsn_selector_get_property(GObject *object,
                                                       guint param_id,
                                                       GValue *value,
                                                       GParamSpec *pspec);

enum {
	PROP_0,

	PROP_SOURCE_NAME
};

/* get a pointer to the parents to be able to call their destructor */
static GObjectClass *parent_class = NULL;

/*
 * GnomeDbDsnSelector class implementation
 */

static void
gnome_db_dsn_selector_class_init (GnomeDbDsnSelectorClass *klass)
{
	GObjectClass *object_class = G_OBJECT_CLASS (klass);

	parent_class = g_type_class_peek_parent (klass);

	object_class->finalize = gnome_db_dsn_selector_finalize;
	object_class->set_property = gnome_db_dsn_selector_set_property;
	object_class->get_property = gnome_db_dsn_selector_get_property;

	g_object_class_install_property (object_class, PROP_SOURCE_NAME,
	                                 g_param_spec_string ("source-name", NULL, NULL, NULL,
	                                                      G_PARAM_WRITABLE | G_PARAM_READABLE));
}


static void
gnome_db_dsn_selector_init (GnomeDbDsnSelector *selector,
				    GnomeDbDsnSelectorClass *klass)
{
	GdaDataModel *model;
	gint cols_index[] = {0};

	g_return_if_fail (GNOME_DB_IS_DSN_SELECTOR (selector));

	selector->priv = g_new0 (GnomeDbDsnSelectorPrivate, 1);

	model = gda_config_list_dsn ();
	gnome_db_combo_set_model (GNOME_DB_COMBO (selector), model, 1, cols_index);
	g_object_unref (model);
}

static void
gnome_db_dsn_selector_finalize (GObject *object)
{
	GnomeDbDsnSelector *selector = (GnomeDbDsnSelector *) object;

	g_return_if_fail (GNOME_DB_IS_DSN_SELECTOR (selector));


	g_free (selector->priv);
	selector->priv = NULL;

	parent_class->finalize (object);
}

static void
gnome_db_dsn_selector_set_property(GObject *object,
				   guint param_id,
				   const GValue *value,
				   GParamSpec *pspec)
{
	GnomeDbDsnSelector *selector;
	GSList *list;
	gint cols_index[] = {0};
	selector = GNOME_DB_DSN_SELECTOR (object);

	switch (param_id) {
	case PROP_SOURCE_NAME:
		list = g_slist_append (NULL, (gpointer) value);
		gnome_db_combo_set_values_ext (GNOME_DB_COMBO (selector), list, cols_index);
		g_slist_free (list);
		break;
	}
}

static void
gnome_db_dsn_selector_get_property (GObject *object,
				    guint param_id,
				    GValue *value,
				    GParamSpec *pspec)
{
	GnomeDbDsnSelector *selector;
	GSList *list;
	gint cols_index[] = {0};
	selector = GNOME_DB_DSN_SELECTOR (object);

	switch (param_id) {
	case PROP_SOURCE_NAME:
		list = gnome_db_combo_get_values_ext (GNOME_DB_COMBO (selector), 1, cols_index);
		if (list && list->data) {
			g_value_set_string (value, g_value_get_string ((GValue*) list->data));
			g_slist_free (list);
		}
		else
			g_value_set_string (value, "");
		break;
	}
}

GType
gnome_db_dsn_selector_get_type (void)
{
	static GType type = 0;

	if (G_UNLIKELY (type == 0)) {
		static const GTypeInfo info = {
			sizeof (GnomeDbDsnSelectorClass),
			(GBaseInitFunc) NULL,
			(GBaseFinalizeFunc) NULL,
			(GClassInitFunc) gnome_db_dsn_selector_class_init,
			NULL,
			NULL,
			sizeof (GnomeDbDsnSelector),
			0,
			(GInstanceInitFunc) gnome_db_dsn_selector_init
		};
		type = g_type_register_static (GNOME_DB_TYPE_COMBO,
					       "GnomeDbDsnSelector",
					       &info, 0);
	}
	return type;
}

/**
 * gnome_db_dsn_selector_new
 *
 * Create a new #GnomeDbDsnSelector, which is just a #GtkComboBox
 * which displays, as its items, all the data sources currently
 * configured in the system. It is useful for connection and configuration
 * screens, where the user has to choose a data source to work with.
 *
 * Returns: the newly created widget.
 */
GtkWidget *
gnome_db_dsn_selector_new (void)
{
	return (GtkWidget*) g_object_new (GNOME_DB_TYPE_DSN_SELECTOR, NULL);
}

/**
 * gnome_db_dsn_selector_get_dsn
 * @name: name of data source to display.
 *
 * Get the Data Source Name (DSN) actualy selected in the #GnomeDbDsnSelector.
 *
 * Returns: the DSN name actualy selected as a new string.
 */
gchar *
gnome_db_dsn_selector_get_dsn (GnomeDbDsnSelector *selector)
{
	gchar *dsn;

	g_object_get (G_OBJECT (selector), "source-name", &dsn, NULL);

	return g_strdup (dsn);
}

/**
 * gnome_db_dsn_selector_set_dsn
 * @name: name of data source to display.
 *
 * Set the selected Data Source Name (DSN) in the #GnomeDbDsnSelector.
 *
 */
void
gnome_db_dsn_selector_set_dsn (GnomeDbDsnSelector *selector, const gchar *dsn)
{
	g_object_set (G_OBJECT (selector), "source-name", dsn, NULL);
}
