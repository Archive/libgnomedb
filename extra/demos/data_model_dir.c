/* Data models/Directory data model
 *
 * A GdaDataModelDir data model listing all the files in a directory displayed in
 * a GnomeDbForm widget where the 'picture' plugin is used to display the contents of each file
 * 
 */

#include <libgnomedb/libgnomedb.h>
#include <libgnomedb/binreloc/gnome-db-binreloc.h>

static GtkWidget *window = NULL;

/* hack to find the directory where Gnome-DB's data pictures are stored */
static gchar *
get_data_path ()
{
	gchar *path;

	if (g_file_test ("demos.h", G_FILE_TEST_EXISTS))
		path = g_strdup ("../../data");
	else {
		gchar *tmp;
		tmp = gnome_db_gbr_get_icon_path ("gnome-db.png");
		path = g_path_get_dirname (tmp);
		g_free (tmp);
		if (!g_file_test (path, G_FILE_TEST_EXISTS)) {
			g_free (path);
			path = NULL;
		}
	}

	if (!path)
		path = g_strdup (".");

	return path;
}

GtkWidget *
do_data_model_dir (GtkWidget *do_widget)
{  
	if (!window) {
		GtkWidget *vbox;
		GtkWidget *label;
		GdaDataModel *model;
		GtkWidget *form, *grid, *nb;
		GnomeDbRawForm *raw;
		GdaSet *data_set;
		GdaHolder *param;
		gchar *path;
		GValue *value;
		
		window = gtk_dialog_new_with_buttons ("GdaDataModelDir data model",
						      GTK_WINDOW (do_widget),
						      0,
						      GTK_STOCK_CLOSE,
						      GTK_RESPONSE_NONE,
						      NULL);
		
		g_signal_connect (window, "response",
				  G_CALLBACK (gtk_widget_destroy), NULL);
		g_signal_connect (window, "destroy",
				  G_CALLBACK (gtk_widget_destroyed), &window);
		
		vbox = gtk_vbox_new (FALSE, 5);
		gtk_box_pack_start (GTK_BOX (GTK_DIALOG (window)->vbox), vbox, TRUE, TRUE, 0);
		gtk_container_set_border_width (GTK_CONTAINER (vbox), 5);
		
		label = gtk_label_new ("The following GnomeDbForm widget displays data from a GdaDataModelDir "
				       "data model which lists the files contained in the selected directory.\n\n"
				       "Each file contents is then displayed using the 'picture' plugin \n"
				       "(right click to open a menu, or double click to load an image).");
		gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 0);
		
		/* GdaDataModelDir object */
		path = get_data_path ();
		model = gda_data_model_dir_new (path);
		g_free (path);

		/* Create the demo widget */
		nb = gtk_notebook_new ();
		gtk_box_pack_start (GTK_BOX (vbox), nb, TRUE, TRUE, 0);

		form = gnome_db_form_new (model);
		gtk_notebook_append_page (GTK_NOTEBOOK (nb), form, gtk_label_new ("Form"));

		grid = gnome_db_grid_new (model);
		gtk_notebook_append_page (GTK_NOTEBOOK (nb), grid, gtk_label_new ("Grid"));
		g_object_unref (model);

		/* specify that we want to use the 'picture' plugin */
		g_object_get (G_OBJECT (form), "raw_form", &raw, NULL);
		data_set = gnome_db_basic_form_get_data_set (GNOME_DB_BASIC_FORM (raw));
		param = gda_set_get_holder (data_set, "data");

		value = gda_value_new_from_string ("picture", G_TYPE_STRING);
		gda_holder_set_attribute_static (param, GNOME_DB_ATTRIBUTE_PLUGIN, value);
		gnome_db_data_widget_column_show_actions (GNOME_DB_DATA_WIDGET (raw), -1, TRUE);

		g_object_get (G_OBJECT (grid), "raw_grid", &raw, NULL);
		data_set = GDA_SET (gnome_db_data_widget_get_current_data (GNOME_DB_DATA_WIDGET (raw)));
		param = gda_set_get_holder (data_set, "data");

		gda_holder_set_attribute_static (param, GNOME_DB_ATTRIBUTE_PLUGIN, value);
		gnome_db_data_widget_column_show_actions (GNOME_DB_DATA_WIDGET (raw), -1, TRUE);

		gda_value_free (value);
	}

	if (!GTK_WIDGET_VISIBLE (window))
		gtk_widget_show_all (window);
	else
		gtk_widget_destroy (window);

	return window;
}


