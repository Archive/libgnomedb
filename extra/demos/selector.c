/* Metadata browser
 *
 * The GnomeDbSeletor widget can be used to display any type of data (it is
 * easily extended), usually a part of the meta data associated to the connection.
 *
 * Here it shows the tables (with their columns and constraints), and the views
 * (with their columns).
 *
 * In this demo program, the meta data has already been extracted and is stored
 * in the 'demo_meta.db' file.
 */

#include <libgnomedb/libgnomedb.h>
#include <libgnomedb-extra/libgnomedb-extra.h>

extern GdaConnection *demo_cnc;
static GtkWidget *window = NULL;

static void selector_selection_changed_cb (GnomeDbSelector *sel, GnomeDbSelectorPart *part,
					   GtkTreeStore *store, GtkTreeIter *iter, gpointer data);

GtkWidget *
do_selector (GtkWidget *do_widget)
{  
	if (!window) {
		GtkWidget *table;
		GtkWidget *label;
		GtkWidget *sw, *selector;

		window = gtk_dialog_new_with_buttons ("Meta data browser",
						      GTK_WINDOW (do_widget),
						      0,
						      GTK_STOCK_CLOSE,
						      GTK_RESPONSE_NONE,
						      NULL);
		
		g_signal_connect (window, "response",
				  G_CALLBACK (gtk_widget_destroy), NULL);
		g_signal_connect (window, "destroy",
				  G_CALLBACK (gtk_widget_destroyed), &window);
		
		table = gtk_table_new (4, 2, FALSE);
		gtk_box_pack_start (GTK_BOX (GTK_DIALOG (window)->vbox), table, TRUE, TRUE, 0);
		gtk_container_set_border_width (GTK_CONTAINER (table), 5);
		gtk_table_set_row_spacings (GTK_TABLE( table), 5);

		label = gtk_label_new ("The following GnomeDbSelector widget displays\n"
				       "the tables and views stored in the GdaMetaStore\n"
				       "object associated to the connection.");
		gtk_table_attach (GTK_TABLE (table), label, 0, 2, 0, 1, 0, 0, 0, 0);
		
		
		/* selector widget preparation */
		selector = gnome_db_selector_new ();
		sw = gtk_scrolled_window_new (NULL, NULL);
		gtk_table_attach_defaults (GTK_TABLE (table), sw, 0, 1, 2, 3);
		gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw), GTK_POLICY_AUTOMATIC,
						GTK_POLICY_AUTOMATIC);

		gtk_container_add (GTK_CONTAINER (sw), selector);
		g_signal_connect (G_OBJECT (selector), "selected-object-changed",
				  G_CALLBACK (selector_selection_changed_cb), NULL);
		gnome_db_selector_add_parts_for_feature (GNOME_DB_SELECTOR (selector),
							 gda_connection_get_meta_store (demo_cnc),
							 GNOME_DB_SELECTOR_FEATURE_SCHEMAS | 
							 GNOME_DB_SELECTOR_FEATURE_SCHEMA_TABLES |
							 GNOME_DB_SELECTOR_FEATURE_SCHEMA_TABLE_COLUMNS |
							 GNOME_DB_SELECTOR_FEATURE_SCHEMA_TABLE_CONSTRAINTS |
							 GNOME_DB_SELECTOR_FEATURE_SCHEMA_VIEWS |
							 GNOME_DB_SELECTOR_FEATURE_SCHEMA_VIEW_COLUMNS);
		
		gtk_widget_set_size_request (window, 350, 400);
	}

	if (!GTK_WIDGET_VISIBLE (window))
		gtk_widget_show_all (window);
	else
		gtk_widget_destroy (window);

	return window;
}

static void
selector_selection_changed_cb (GnomeDbSelector *sel, GnomeDbSelectorPart *part,
                               GtkTreeStore *store, GtkTreeIter *iter, gpointer data)
{
        GnomeDbSelectorPart *apart;
        GtkTreeIter aiter = *iter;
        g_print ("Selection changed, data (listed by selector part):\n");
        for (apart = part; apart; apart = gnome_db_selector_part_get_parent_part (apart)) {
                GdaSet *set;
                g_print ("\t* selector part: %s\n", gnome_db_selector_part_get_name (apart));
                set = gnome_db_selector_part_get_params (apart, store, &aiter);
                if (set) {
                        GSList *list;
                        for (list = set->holders; list; list = list->next) {
                                GdaHolder *holder = GDA_HOLDER (list->data);
                                const GValue *cvalue;
                                gchar *str;
                                cvalue = gda_holder_get_value (holder);
                                str = gda_value_stringify (cvalue);
                                g_print ("\t%s => %s\n", gda_holder_get_id (holder), str);
                                g_free (str);
                        }
                }
                set = gnome_db_selector_part_get_data (apart, store, &aiter);
                if (set) {
                        GSList *list;
                        for (list = set->holders; list; list = list->next) {
                                GdaHolder *holder = GDA_HOLDER (list->data);
                                const GValue *cvalue;
                                gchar *str;
                                cvalue = gda_holder_get_value (holder);
                                str = gda_value_stringify (cvalue);
                                g_print ("\t%s => %s\n", gda_holder_get_id (holder), str);
                                g_free (str);
                        }
                }
                GtkTreeIter tmp;
                if (!gtk_tree_model_iter_parent (GTK_TREE_MODEL (store), &tmp, &aiter))
                        break;
                aiter = tmp;
        }
}
