/* Sql console
 *
 * The GnomeDbSqlConsole widget lets the user enter and run SQL queries
 */

#include <libgnomedb/libgnomedb.h>
#include <libgnomedb-extra/gnome-db-sql-console.h>

extern GdaConnection *demo_cnc;
static GtkWidget *window = NULL;

GtkWidget *
do_console (GtkWidget *do_widget)
{  
	if (!window) {
		GtkWidget *vbox;
		GtkWidget *label;
		GtkWidget *console;
		
		window = gtk_dialog_new_with_buttons ("SQL Console",
						      GTK_WINDOW (do_widget),
						      0,
						      GTK_STOCK_CLOSE,
						      GTK_RESPONSE_NONE,
						      NULL);
		
		g_signal_connect (window, "response",
				  G_CALLBACK (gtk_widget_destroy), NULL);
		g_signal_connect (window, "destroy",
				  G_CALLBACK (gtk_widget_destroyed), &window);
		
		vbox = gtk_vbox_new (FALSE, 5);
		gtk_box_pack_start (GTK_BOX (GTK_DIALOG (window)->vbox), vbox, TRUE, TRUE, 0);
		gtk_container_set_border_width (GTK_CONTAINER (vbox), 5);
		
		label = gtk_label_new ("Here is an SQL console");
		gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 0);
		
		/* Create the demo widget */
		console = gnome_db_sql_console_new (demo_cnc, 
						    "SQL Console (type \\? + ENTER for help)\n");
		gtk_widget_set_size_request (console, 480, 320);

		gtk_box_pack_start (GTK_BOX (vbox), console, TRUE, TRUE, 0);
	}

	if (!GTK_WIDGET_VISIBLE (window))
		gtk_widget_show_all (window);
	else
		gtk_widget_destroy (window);

	return window;
}


