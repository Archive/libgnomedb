/* Graph
 *
 * The GnomeDbCanvasDbRelation graph widget can be used to display
 * parts of a database structure which is held (among other information
 * in the meta data associated to the connection).
 *
 * In this demo program, the meta data has already been extracted and is stored
 * in the 'demo_meta.db' file.
 */

#include <libgnomedb/libgnomedb.h>
#include <libgnomedb-graph/libgnomedb-graph.h>

extern GdaConnection *demo_cnc;
static GtkWidget *window = NULL;

static void
auto_layout_clicked_cb (GtkButton *button, GtkWidget *canvas)
{
	gnome_db_canvas_perform_auto_layout (GNOME_DB_CANVAS (canvas), TRUE, GNOME_DB_CANVAS_LAYOUT_DEFAULT);
}

GtkWidget *
do_graph (GtkWidget *do_widget)
{  
	if (!window) {
		GtkWidget *vbox;
		GtkWidget *label;
		GtkWidget *sw;
		GtkWidget *graphwidget;
		GValue *tname;
		
		window = gtk_dialog_new_with_buttons ("Graph",
						      GTK_WINDOW (do_widget),
						      0,
						      GTK_STOCK_CLOSE,
						      GTK_RESPONSE_NONE,
						      NULL);
		
		g_signal_connect (window, "response",
				  G_CALLBACK (gtk_widget_destroy), NULL);
		g_signal_connect (window, "destroy",
				  G_CALLBACK (gtk_widget_destroyed), &window);
		
		vbox = gtk_vbox_new (FALSE, 5);
		gtk_box_pack_start (GTK_BOX (GTK_DIALOG (window)->vbox), vbox, TRUE, TRUE, 0);
		gtk_container_set_border_width (GTK_CONTAINER (vbox), 5);
		
		label = gtk_label_new ("The following GnomeDbCanvasDbRelation widget displays\n"
				       "a part of the database structure.\n\n"
				       "Right click in it for more...");
		gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 0);
		
		/* Create the demo widget */
		graphwidget = gnome_db_canvas_db_relations_new (gda_connection_get_meta_store (demo_cnc));
		sw = gnome_db_canvas_set_in_scrolled_window (GNOME_DB_CANVAS (graphwidget));
		gtk_widget_set_size_request (graphwidget, 320, 320);
		gtk_box_pack_start (GTK_BOX (vbox), sw, TRUE, TRUE, 0);

		/* add some tables to the graph */
		g_value_set_string ((tname = gda_value_new (G_TYPE_STRING)), "customers");
		gnome_db_canvas_db_relations_add_table_item (GNOME_DB_CANVAS_DB_RELATIONS (graphwidget), NULL, NULL, tname);
		g_value_set_string (tname, "orders");
		gnome_db_canvas_db_relations_add_table_item (GNOME_DB_CANVAS_DB_RELATIONS (graphwidget), NULL, NULL, tname);
		g_value_set_string (tname, "order_contents");
		gnome_db_canvas_db_relations_add_table_item (GNOME_DB_CANVAS_DB_RELATIONS (graphwidget), NULL, NULL, tname);
		g_value_set_string (tname, "products");
		gnome_db_canvas_db_relations_add_table_item (GNOME_DB_CANVAS_DB_RELATIONS (graphwidget), NULL, NULL, tname);
		g_value_set_string (tname, "warehouses");
		gnome_db_canvas_db_relations_add_table_item (GNOME_DB_CANVAS_DB_RELATIONS (graphwidget), NULL, NULL, tname);
		gda_value_free (tname);

		if (gnome_db_canvas_auto_layout_enabled (GNOME_DB_CANVAS (graphwidget))) {
			/* apply auto layout */
			gnome_db_canvas_perform_auto_layout (GNOME_DB_CANVAS (graphwidget), FALSE, 
							  GNOME_DB_CANVAS_LAYOUT_DEFAULT);
			/* button to perform auto layout */
			GtkWidget *button;
			button = gtk_button_new_with_label ("Auto layout");
			gtk_box_pack_start (GTK_BOX (vbox), button, FALSE, FALSE, 0);
			g_signal_connect (button, "clicked",
					  G_CALLBACK (auto_layout_clicked_cb), graphwidget);
		}

		/* we need to display the widget so it is possible to set a correct zoom factor */
		gtk_widget_show_all (window);
		gnome_db_canvas_fit_zoom_factor (GNOME_DB_CANVAS (graphwidget));
	}
	else {
		if (!GTK_WIDGET_VISIBLE (window))
			gtk_widget_show_all (window);
		else
			gtk_widget_destroy (window);
	}

	return window;
}


