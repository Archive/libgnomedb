/* gnome-db-browser.c
 *
 * Copyright (C) 2006 - 2008 Vivien Malerba
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include <glib/gi18n-lib.h>
#include <libgnomedb/libgnomedb.h>
#include <libgnomedb-extra/libgnomedb-extra.h>
#include <libgnomedb/gnome-db-util.h>
#include <gtk/gtk.h>
#include <libgnomedb/binreloc/gnome-db-binreloc.h>
#include <string.h>

/* options */
static GOptionEntry entries[] = {
        { NULL }
};

typedef struct {
	/* General section */
	GdaConnection *cnc;
	gchar         *dsn;
	gchar         *auth;
	GtkWidget     *mainwin;

	GtkWidget     *db_selector;
	GtkWidget     *props_nb;
} BrowserConfig;

static gboolean delete_event (GtkWidget *widget, GdkEvent  *event, gpointer   data )
{
    return FALSE;
}

static void destroy (GtkWidget *widget, gpointer   data )
{
    gtk_main_quit ();
}

static GtkWidget *build_menu (BrowserConfig *config, GtkWidget *mainwin);
static GtkWidget *build_main_area (BrowserConfig *config);
int 
main (int argc, char **argv)
{
	GtkWidget *mainwin, *vbox, *menu, *page;
	BrowserConfig *config;
	GdkPixbuf *icon;
	GOptionContext *context;
	GError *error = NULL;
	gchar *str;

	/* command line parsing */
        context = g_option_context_new ("[Data source name] - Test a data source");
        g_option_context_add_main_entries (context, entries, GETTEXT_PACKAGE);
        if (!g_option_context_parse (context, &argc, &argv, &error)) {
                g_warning ("Can't parse arguments: %s", error->message);
                exit (1);
        }
        g_option_context_free (context);
	
	/* Initialize i18n support */
	str = gnome_db_gbr_get_locale_dir_path ();
	bindtextdomain (GETTEXT_PACKAGE, str);
	g_free (str);

        bind_textdomain_codeset (GETTEXT_PACKAGE, "UTF-8");
        textdomain (GETTEXT_PACKAGE);
	
	/* Initialize the widget set */
	gnome_db_init ();
	gtk_init (&argc, &argv);

	/* Browser config */
	config = g_new0 (BrowserConfig, 1);
	
	/* Open connection */
	if (argc <= 1) {
		g_print (_("A data source name like \"[<username>[:<password>]@<DSN name>\" must be specified.\n"));
		exit (1);
	}

	GdaDsnInfo *src;
	gchar *username, *pass, *real_dsn;
	gboolean ask = FALSE;
	src = gda_config_get_dsn_info (argv[1]);
	if (!src) {
		g_print (_("Datasource '%s' is not declared\n"), argv[1]);
		exit (1);
	}

	gda_dsn_split (argv[1], &real_dsn, &username, &pass);
	config->dsn = real_dsn;
	if (username) {
		gchar *s1;
		if (!*username)
			ask = TRUE;
		s1 = gda_rfc1738_encode (username);
		if (pass) {
			gchar *s2;
			if (!*pass)
				ask = TRUE;
			s2 = gda_rfc1738_encode (pass);
			g_free (pass);
			config->auth = g_strdup_printf ("USERNAME=%s;PASSWORD=%s", s1, s2);
			g_free (s2);
		}
		else
			config->auth = g_strdup_printf ("USERNAME=%s", s1);
		g_free (s1);
		g_free (username);
	}
	
	if (gda_config_dsn_needs_authentication (config->dsn) && (!config->auth || ask)) {
		GtkWidget *dlg;
		GnomeDbLogin *login;
		dlg = gnome_db_login_dialog_new (_("Authentication"), NULL);
		login = gnome_db_login_dialog_get_login_widget (GNOME_DB_LOGIN_DIALOG (dlg));
		gnome_db_login_set_dsn (login, config->dsn);
		gnome_db_login_set_auth (login, config->auth);
		gnome_db_login_set_enable_create_button (GNOME_DB_LOGIN (login), FALSE);
		gnome_db_login_set_show_dsn_selector (GNOME_DB_LOGIN (login), FALSE);
		if (!gnome_db_login_dialog_run (GNOME_DB_LOGIN_DIALOG (dlg))) {
			g_print ("Aborted.\n");
			exit (0);
		}
		g_free (config->auth);
		config->auth = g_strdup (gnome_db_login_get_auth (login));
		gtk_widget_destroy (dlg);
	}

	config->cnc = gda_connection_open_from_dsn (config->dsn, config->auth, GDA_CONNECTION_OPTIONS_NONE, &error);
	if (!config->cnc) {
		g_print (_("Can't open connection to '%s'.: %s\n"), config->dsn,
			 error && error->message ? error->message : _("No detail"));
		exit (1);
	}

	/* GdaMetaStore handling */
	GdaMetaStore *store;
	gboolean update_store = FALSE;
	gchar *filename;
#define LIBGDA_USER_CONFIG_DIR G_DIR_SEPARATOR_S ".libgda"
	filename = g_strdup_printf ("%s%sgda-sql-%s.db",
				    g_get_home_dir (), LIBGDA_USER_CONFIG_DIR G_DIR_SEPARATOR_S,
				    src->name);
	if (! g_file_test (filename, G_FILE_TEST_EXISTS))
		update_store = TRUE;
	store = gda_meta_store_new_with_file (filename);
	g_free (filename);
	g_object_set (G_OBJECT (config->cnc), "meta-store", store, NULL);
	if (update_store) {
		GError *lerror = NULL;
		g_print (_("\tGetting database schema information, "
			   "this may take some time... "));
		fflush (stdout);
		if (!gda_connection_update_meta_store (config->cnc, NULL, &error)) {
			g_print (_("error: %s\n"),
				 lerror && lerror->message ? lerror->message : _("No detail"));
			if (error)
				g_error_free (error);
			error = NULL;
		}
		else
			g_print (_(" Done.\n"));
	}

	/* Create the main window */
	mainwin = gtk_window_new (GTK_WINDOW_TOPLEVEL);
	gtk_container_set_border_width (GTK_CONTAINER (mainwin), 0);
	g_signal_connect (G_OBJECT (mainwin), "delete_event",
			  G_CALLBACK (delete_event), NULL);
	g_signal_connect (G_OBJECT (mainwin), "destroy",
			  G_CALLBACK (destroy), NULL);
	config->mainwin = mainwin;

	gtk_window_set_title (GTK_WINDOW (mainwin), _("Database browser"));
	
	str = gnome_db_gbr_get_icon_path ("gnome-db.png");
	icon = gdk_pixbuf_new_from_file (str, NULL);
	g_free (str);
	if (icon) {
		gtk_window_set_icon (GTK_WINDOW (mainwin), icon);
		g_object_unref (icon);
	}

	vbox = gtk_vbox_new (FALSE, 0);
	gtk_container_add (GTK_CONTAINER (mainwin), vbox);
	gtk_widget_show (vbox);

	/* menu */
	menu = build_menu (config, mainwin);
	gtk_widget_show (menu);
	gtk_box_pack_start (GTK_BOX (vbox), menu, FALSE, FALSE, 0);

	/* main area */
	page = build_main_area (config);
	gtk_box_pack_start (GTK_BOX (vbox), page, TRUE, TRUE, 0);

	/* Show the application window */
	gtk_widget_set_size_request (mainwin, 700, 600);
	gtk_widget_show_all (mainwin);
	
	gtk_main ();

	return 0;
}

static void update_dbms_data_cb (GtkWidget *wid, BrowserConfig *config);
static void sql_console_cb (GtkWidget *wid, BrowserConfig *config);

/*
 * Menu
 */
GtkWidget *
build_menu (BrowserConfig *config, GtkWidget *mainwin)
{
	GtkWidget *menubar1, *menuitem1, *menuitem1_menu, *entry;
	GtkAccelGroup *accel_group;

	accel_group = gtk_accel_group_new ();

	menubar1 = gtk_menu_bar_new ();
	gtk_widget_show (menubar1);

	/* File menu */
	menuitem1 = gtk_menu_item_new_with_mnemonic (_("_File"));
	gtk_widget_show (menuitem1);
	gtk_container_add (GTK_CONTAINER (menubar1), menuitem1);
	
	menuitem1_menu = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem1), menuitem1_menu);
	
	entry = gtk_image_menu_item_new_from_stock (GTK_STOCK_QUIT, accel_group);
	gtk_widget_show (entry);
	gtk_container_add (GTK_CONTAINER (menuitem1_menu), entry);

	g_signal_connect (G_OBJECT (entry), "activate",
			  G_CALLBACK (destroy), config);

	/* Database menu */
	menuitem1 = gtk_menu_item_new_with_mnemonic (_("_Database"));
	gtk_widget_show (menuitem1);
	gtk_container_add (GTK_CONTAINER (menubar1), menuitem1);
	
	menuitem1_menu = gtk_menu_new ();
	gtk_menu_item_set_submenu (GTK_MENU_ITEM (menuitem1), menuitem1_menu);

	entry = gtk_menu_item_new_with_mnemonic (_("Synchronise metadata with DBMS"));
	gtk_widget_show (entry);
	gtk_container_add (GTK_CONTAINER (menuitem1_menu), entry);

	g_signal_connect (G_OBJECT (entry), "activate",
			  G_CALLBACK (update_dbms_data_cb), config);

	entry = gtk_menu_item_new_with_mnemonic (_("SQL Console"));
	gtk_widget_show (entry);
	gtk_container_add (GTK_CONTAINER (menuitem1_menu), entry);

	g_signal_connect (G_OBJECT (entry), "activate",
			  G_CALLBACK (sql_console_cb), config);


	gtk_window_add_accel_group (GTK_WINDOW (mainwin), accel_group);

	return menubar1;
}

static void
update_dbms_data_cb (GtkWidget *wid, BrowserConfig *config)
{
	TO_IMPLEMENT;
	gda_connection_update_meta_store (config->cnc, NULL, NULL);
}

static void
sql_console_cb (GtkWidget *wid, BrowserConfig *config)
{
	GtkWidget *dlg, *sw, *console;

        /* dialog */
        dlg = gtk_dialog_new_with_buttons (_("SQL Console"), NULL, 0,
                                           GTK_STOCK_CLOSE, GTK_RESPONSE_CLOSE, NULL);
        gtk_dialog_set_has_separator (GTK_DIALOG (dlg), FALSE);
        gtk_window_set_title (GTK_WINDOW (dlg), _("SQL Console"));
	g_signal_connect_swapped (dlg, "response", 
				  G_CALLBACK (gtk_widget_destroy), dlg);

        /* console widget */
	sw = gtk_scrolled_window_new (NULL, NULL);
        gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw),
                                        GTK_POLICY_AUTOMATIC,
                                        GTK_POLICY_AUTOMATIC);
        gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (sw), GTK_SHADOW_IN);
        gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dlg)->vbox), sw, TRUE, TRUE, 0);
        gtk_widget_show (sw);

        console = gnome_db_sql_console_new (config->cnc, _("SQL Console (type \\? + ENTER for help)\n"));
        gtk_container_add (GTK_CONTAINER (sw), console);
        gtk_widget_show (console);

        gtk_widget_set_size_request (dlg, 450, 500);
        gtk_widget_show (dlg);
}

/**
 * build_main_area
 *
 * Build the Tables and Views page
 */
static void selection_changed_cb (GnomeDbSelector *sel, GnomeDbSelectorPart *part,
				  GtkTreeStore *store, GtkTreeIter *iter, BrowserConfig *config);
static GtkWidget *
build_main_area (BrowserConfig *config)
{
	GtkWidget *wid, *label, *paned, *vb, *nb;
	GtkWidget *sw;
	gchar *str;

	paned = gtk_hpaned_new ();
	gtk_container_set_border_width (GTK_CONTAINER (paned), 5);

	/* left part */
	vb = gtk_vbox_new (FALSE, 5);
	gtk_container_set_border_width (GTK_CONTAINER (vb), 5);
	gtk_paned_add1 (GTK_PANED (paned), vb);

	label = gtk_label_new (NULL);
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0);
	str = g_strdup_printf ("<b>%s</b>", _("Database objects:"));
	gtk_label_set_markup (GTK_LABEL (label), str);
	g_free (str);
	gtk_box_pack_start (GTK_BOX (vb), label, FALSE, FALSE, 0);

	/* selector */
	wid = gtk_vbox_new (FALSE, 5);
	gtk_container_set_border_width (GTK_CONTAINER (wid), 5);
	gtk_box_pack_start (GTK_BOX (vb), wid, TRUE, TRUE, 0);
	vb = wid;

	wid = gnome_db_selector_new ();
	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_box_pack_start (GTK_BOX (vb), sw, TRUE, TRUE, 0);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw), GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);

	gtk_container_add (GTK_CONTAINER (sw), wid);
	g_signal_connect (G_OBJECT (wid), "selected-object-changed",
			  G_CALLBACK (selection_changed_cb), config);
	gtk_widget_set_usize (wid, 200, 300);
	config->db_selector = wid;
	gnome_db_selector_add_parts_for_feature (GNOME_DB_SELECTOR (wid),
						 gda_connection_get_meta_store (config->cnc),
						 GNOME_DB_SELECTOR_FEATURE_SCHEMAS |
						 GNOME_DB_SELECTOR_FEATURE_SCHEMA_TABLES |
						 GNOME_DB_SELECTOR_FEATURE_SCHEMA_TABLE_COLUMNS |
						 GNOME_DB_SELECTOR_FEATURE_SCHEMA_TABLE_CONSTRAINTS |
						 GNOME_DB_SELECTOR_FEATURE_SCHEMA_VIEWS |
						 GNOME_DB_SELECTOR_FEATURE_SCHEMA_VIEW_COLUMNS);

	/* right part */
	vb = gtk_vbox_new (FALSE, 5);
	gtk_container_set_border_width (GTK_CONTAINER (vb), 5);
	gtk_paned_add2 (GTK_PANED (paned), vb);

	label = gtk_label_new ("");
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0);
	str = g_strdup_printf ("<b>%s</b>", _("Properties:"));
	gtk_label_set_markup (GTK_LABEL (label), str);
	g_free (str);
	gtk_box_pack_start (GTK_BOX (vb), label, FALSE, FALSE, 0);

	nb = gtk_notebook_new ();
	config->props_nb = nb;
	gtk_notebook_set_show_tabs (GTK_NOTEBOOK (nb), FALSE); /* for now... */
	gtk_notebook_set_show_border (GTK_NOTEBOOK (nb), FALSE);
	gtk_box_pack_start (GTK_BOX (vb), nb, TRUE, TRUE, 0);

#define PAGE_EMPTY 0
	vb = gtk_vbox_new (FALSE, 5);
	gtk_container_set_border_width (GTK_CONTAINER (vb), 5);

	label = gtk_label_new (_("Select a database object to display its properties."));
	//gtk_misc_set_alignment (GTK_MISC (label), 0, 0);
	gtk_box_pack_start (GTK_BOX (vb), label, TRUE, TRUE, 0);
	gtk_notebook_append_page (GTK_NOTEBOOK (nb), vb, NULL);

#define PAGE_TABLE 1
	vb = gtk_vbox_new (FALSE, 5);
	gtk_container_set_border_width (GTK_CONTAINER (vb), 5);

	label = gtk_label_new ("");
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0);
	str = g_strdup_printf ("<b>%s</b>", _("Table's properties"));
	gtk_label_set_markup (GTK_LABEL (label), str);
	g_free (str);
	gtk_box_pack_start (GTK_BOX (vb), label, FALSE, FALSE, 0);
	gtk_notebook_append_page (GTK_NOTEBOOK (nb), vb, NULL);

#define PAGE_VIEW 2
	vb = gtk_vbox_new (FALSE, 5);
	gtk_container_set_border_width (GTK_CONTAINER (vb), 5);

	label = gtk_label_new ("");
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0);
	str = g_strdup_printf ("<b>%s</b>", _("View's properties"));
	gtk_label_set_markup (GTK_LABEL (label), str);
	g_free (str);
	gtk_box_pack_start (GTK_BOX (vb), label, FALSE, FALSE, 0);
	gtk_notebook_append_page (GTK_NOTEBOOK (nb), vb, NULL);

#define PAGE_DOMAIN 3
	vb = gtk_vbox_new (FALSE, 5);
	gtk_container_set_border_width (GTK_CONTAINER (vb), 5);

	label = gtk_label_new ("");
	gtk_misc_set_alignment (GTK_MISC (label), 0, 0);
	str = g_strdup_printf ("<b>%s</b>", _("Domain's properties"));
	gtk_label_set_markup (GTK_LABEL (label), str);
	g_free (str);
	gtk_box_pack_start (GTK_BOX (vb), label, FALSE, FALSE, 0);
	gtk_notebook_append_page (GTK_NOTEBOOK (nb), vb, NULL);
	
	return paned;
}

#define NB_VALUE_NAMES 5
const gchar *value_names[NB_VALUE_NAMES] = {
	"catalog",
	"schema",
	"table_name",
	"view_name",
	"domain_name"
};

static void
selection_changed_cb (GnomeDbSelector *sel, GnomeDbSelectorPart *part,
		      GtkTreeStore *store, GtkTreeIter *iter, BrowserConfig *config)
{
	const GValue *values[NB_VALUE_NAMES];
	gint i;

	memset (values, 0, sizeof (values));
	for (i = 0; i < NB_VALUE_NAMES; i++) {
		values[i] = gnome_db_selector_part_get_value (part, value_names[i], store, iter);
		/*g_print ("%d => %s\n", i, values[i] ? gda_value_stringify (values[i]) : "---");*/
	}

	if (values[2]) {
		gtk_notebook_set_current_page (GTK_NOTEBOOK (config->props_nb), PAGE_TABLE);
	}
	else if (values[3]) {
		gtk_notebook_set_current_page (GTK_NOTEBOOK (config->props_nb), PAGE_VIEW);
	}
	else if (values[4]) {
		gtk_notebook_set_current_page (GTK_NOTEBOOK (config->props_nb), PAGE_DOMAIN);
	}
	else
		gtk_notebook_set_current_page (GTK_NOTEBOOK (config->props_nb), PAGE_EMPTY);
}
