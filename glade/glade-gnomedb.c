/* Glade GNOME-DB module
 * Copyright (C) 2001-2002, The GNOME Foundation
 *
 * AUTHORS:
 *	Rodrigo Moya <rodrigo@gnome-db.org>
 *
 * This Library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This Library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this Library; see the file COPYING.LIB.  If not,
 * write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include <string.h>
#include <libgnomedb/libgnomedb.h>
#include <libgnomedb-extra/libgnomedb-extra.h>
#include <glade/glade-init.h>
#include <glade/glade-build.h>

/*
 * Private functions
 */

static GtkWidget *
gnomedb_dialog_find_internal_child (GladeXML *xml,
				    GtkWidget *parent,
				    const gchar *childname)
{
	if (!strcmp(childname, "vbox"))
		return GTK_DIALOG (parent)->vbox;
	else if (!strcmp (childname, "action_area"))
		return GTK_DIALOG (parent)->action_area;

	return NULL;
}

/* this macro puts a version check function into the module */
GLADE_MODULE_CHECK_INIT

void
glade_module_register_widgets (void)
{
	glade_require ("gtk");
	glade_require ("gnome");

	glade_register_widget (GNOME_DB_TYPE_COMBO,
			       glade_standard_build_widget,
			       NULL, NULL);
	glade_register_widget (GNOME_DB_TYPE_CONNECTION_PROPERTIES,
			       glade_standard_build_widget,
			       NULL, NULL);
	glade_register_widget (GNOME_DB_TYPE_DSN_SELECTOR,
			       glade_standard_build_widget,
			       NULL, NULL);
#ifdef HAVE_GTKTWOTEN
	glade_register_widget (GNOME_DB_TYPE_DSN_ASSISTANT,
			       glade_standard_build_widget,
			       NULL, NULL);
#endif
	glade_register_widget (GNOME_DB_TYPE_DSN_EDITOR,
			       glade_standard_build_widget,
			       NULL, NULL);
	glade_register_widget (GNOME_DB_TYPE_EDITOR,
			       glade_standard_build_widget,
			       NULL, NULL);
	glade_register_widget (GNOME_DB_TYPE_ERROR,
			       glade_standard_build_widget,
			       NULL, NULL);
	glade_register_widget (GNOME_DB_TYPE_ERROR_DIALOG,
			       NULL, glade_standard_build_children,
			       gnomedb_dialog_find_internal_child);
	glade_register_widget (GNOME_DB_TYPE_FIND_DIALOG,
			       NULL, glade_standard_build_children,
			       gnomedb_dialog_find_internal_child);
	glade_register_widget (GNOME_DB_TYPE_FORM,
			       glade_standard_build_widget,
			       NULL, NULL);
	glade_register_widget (GNOME_DB_TYPE_GRAY_BAR,
			       glade_standard_build_widget,
			       NULL, NULL);
	glade_register_widget (GNOME_DB_TYPE_GRID,
			       glade_standard_build_widget,
			       NULL, NULL);
	glade_register_widget (GNOME_DB_TYPE_LOGIN,
			       glade_standard_build_widget,
			       NULL, NULL);
	glade_register_widget (GNOME_DB_TYPE_LOGIN_DIALOG,
			       NULL, glade_standard_build_children,
			       gnomedb_dialog_find_internal_child);
	glade_register_widget (GNOME_DB_TYPE_PROVIDER_SELECTOR,
			       glade_standard_build_widget,
			       NULL, NULL);

	glade_provide ("gnomedb");
}
