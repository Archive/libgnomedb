/* GNOME-DB Components
 *
 * Copyright (C) 2000 - 2008 The GNOME Foundation.
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *      Vivien Malerba <malerba@gnome-db.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <glib/gi18n-lib.h>
#include <libgda/gda-config.h>
#include <libgda/gda-data-model-array.h>
#include <gtk/gtkstock.h>
#include <gtk/gtktable.h>
#include <gtk/gtkvbbox.h>
#include <libgnomedb/libgnomedb.h>
#include <libgnomedb-extra/libgnomedb-extra.h>
#include <libgnomedb/gnome-db-util.h>
#include "dsn-config.h"
#include "dsn-properties-dialog.h"

#define DSN_CONFIG_DATA "DSN_ConfigData"

typedef struct {
	GtkWidget *title;
	GtkWidget *dsn_list;
	GtkWidget *dialog;
} DsnConfigPrivate;

static void
free_private_data (gpointer data)
{
	DsnConfigPrivate *priv = (DsnConfigPrivate *) data;

	g_free (priv);
}


/*
 * Callbacks
 */


static void
list_double_clicked_cb (GnomeDbGrid *grid, gint row, gpointer user_data)
{
	dsn_config_edit_properties (GTK_WIDGET (user_data));
}

static void
list_popup_properties_cb (GtkWidget *menu, gpointer user_data)
{
	dsn_config_edit_properties (GTK_WIDGET (user_data));
}

static void
list_popup_delete_cb (GtkWidget *menu, gpointer user_data)
{
	dsn_config_delete (GTK_WIDGET (user_data));
}

static void
list_popup_cb (GnomeDbRawGrid *grid, GtkMenu *menu, gpointer user_data)
{
	GtkWidget *item_delete, *item_properties;
	gboolean ok;
	GList *list;

	item_delete = gnome_db_new_menu_item (GTK_STOCK_DELETE,
					      TRUE,
					      G_CALLBACK (list_popup_delete_cb),
					      user_data);
	item_properties = gnome_db_new_menu_item (GTK_STOCK_PROPERTIES,
						  TRUE,
						  G_CALLBACK (list_popup_properties_cb),
						  user_data);

	list = gnome_db_raw_grid_get_selection (grid);
	ok = list != NULL;
	if (list)
		g_list_free (list);
	gtk_widget_set_sensitive (item_delete, ok);
	gtk_widget_set_sensitive (item_properties, ok);

	gtk_menu_prepend (menu, gtk_separator_menu_item_new ());
	gtk_menu_prepend (menu, item_delete);
	gtk_menu_prepend (menu, gtk_separator_menu_item_new ());
	gtk_menu_prepend (menu, item_properties);
}

/*
 * Public functions
 */

GtkWidget *
dsn_config_new (void)
{
	DsnConfigPrivate *priv;
	GtkWidget *dsn;
	GtkWidget *table;
	GtkWidget *box;
	GtkWidget *button;
	GtkWidget *label;
	GtkWidget *sw;
	gchar *title;
	GdaDataModel *model;

	priv = g_new0 (DsnConfigPrivate, 1);
	dsn = gnome_db_new_vbox_widget (FALSE, 6);
        gtk_container_set_border_width (GTK_CONTAINER (dsn), 6);
	g_object_set_data_full (G_OBJECT (dsn), DSN_CONFIG_DATA, priv,
				(GDestroyNotify) free_private_data);

	/* create the main table */
	table = gnome_db_new_table_widget (3, 1, FALSE);
	gtk_box_pack_start (GTK_BOX (dsn), table, TRUE, TRUE, 0);

	/* title */
	title = g_strdup_printf ("<b>%s</b>\n%s", _("Data Sources"),
				 _("Configured data sources in the system"));
	priv->title = gnome_db_gray_bar_new (title);
	g_free (title);
	gnome_db_gray_bar_set_icon_from_stock (GNOME_DB_GRAY_BAR (priv->title),
					       GNOME_DB_STOCK_DATABASE, GTK_ICON_SIZE_BUTTON);
	gtk_table_attach (GTK_TABLE (table), priv->title, 0, 1, 0, 1,
			  GTK_FILL | GTK_SHRINK,
			  GTK_FILL | GTK_SHRINK,
			  0, 0);
	gtk_widget_show (priv->title);

	/* create the data source list */
	sw = gtk_scrolled_window_new (NULL, NULL);
	gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (sw), GTK_POLICY_AUTOMATIC,
					GTK_POLICY_AUTOMATIC);
	gtk_table_attach (GTK_TABLE (table), sw, 0, 1, 1, 2,
			  GTK_FILL | GTK_SHRINK | GTK_EXPAND,
			  GTK_FILL | GTK_SHRINK | GTK_EXPAND,
			  0, 0);
	
	model = gda_config_list_dsn ();
	priv->dsn_list = gnome_db_raw_grid_new (model);
	g_object_unref (model);
	g_object_set_data (G_OBJECT (dsn), "grid", priv->dsn_list);
 	gnome_db_data_widget_column_set_editable (GNOME_DB_DATA_WIDGET (priv->dsn_list), 0, FALSE);
 	gnome_db_data_widget_column_hide (GNOME_DB_DATA_WIDGET (priv->dsn_list), 3);
 	gnome_db_data_widget_column_hide (GNOME_DB_DATA_WIDGET (priv->dsn_list), 4);
	g_object_set (priv->dsn_list, "info_cell_visible", FALSE, NULL);

	gtk_container_add (GTK_CONTAINER (sw), priv->dsn_list);
	
	gtk_widget_show_all (sw);
	g_signal_connect (priv->dsn_list, "double_clicked",
			  G_CALLBACK (list_double_clicked_cb), dsn);
	g_signal_connect (priv->dsn_list, "populate_popup",
			  G_CALLBACK (list_popup_cb), dsn);

	/* add tip */
	box = gnome_db_new_hbox_widget (FALSE, 6);
        gtk_container_set_border_width (GTK_CONTAINER (box), 6);
	gtk_table_attach (GTK_TABLE (table), box, 0, 1, 2, 3,
			  GTK_FILL,
			  GTK_FILL,
                          0, 0);

	button = gtk_image_new_from_stock (GTK_STOCK_DIALOG_INFO, GTK_ICON_SIZE_DIALOG);
        gtk_misc_set_alignment (GTK_MISC (button), 0.5, 0.0);
	gtk_widget_show (button);
	gtk_box_pack_start (GTK_BOX (box), button, FALSE, FALSE, 0);

	label = gnome_db_new_label_widget (
		_("Data sources are the means by which database "
		  "connections are identified in GNOME-DB. All "
		  "information needed to open a connection to "
		  "a specific provider/database combo is stored using "
		  "a unique name. It is by use of this unique name "
		  "you identify the connections in the applications "
		  "that make use of GNOME-DB for database access."));
	gtk_label_set_line_wrap (GTK_LABEL (label), TRUE);
        gtk_misc_set_alignment (GTK_MISC (label), 0.0, 0.0);
        gtk_label_set_justify (GTK_LABEL (label), GTK_JUSTIFY_LEFT);
	gtk_box_pack_start (GTK_BOX (box), label, TRUE, TRUE, 0);
	
	return dsn;
}

void
dsn_config_edit_properties (GtkWidget *dsn)
{
	DsnConfigPrivate *priv;
	GdaDataModel *model;
	GList *selection;
	gchar *str;
	const GValue *cvalue;

	priv = g_object_get_data (G_OBJECT (dsn), DSN_CONFIG_DATA);
	
	selection = gnome_db_raw_grid_get_selection (GNOME_DB_RAW_GRID (priv->dsn_list));
	if (!selection)
		return;

	model = gnome_db_data_widget_get_gda_model (GNOME_DB_DATA_WIDGET (priv->dsn_list));
	if (!GDA_IS_DATA_MODEL (model))
		return;

	cvalue = gda_data_model_get_value_at (model, 0, GPOINTER_TO_INT (selection->data), NULL);
	if (!cvalue) 
		return;

	str = gda_value_stringify ((GValue *) cvalue);
	dsn_properties_dialog (GTK_WINDOW (gtk_widget_get_toplevel (dsn)), str);

	g_list_free (selection);
	g_free (str);
}

void
dsn_config_delete (GtkWidget *dsn)
{
	DsnConfigPrivate *priv;
	GtkTreeSelection *sel;
	GList *list, *sel_rows;
	GList *sel_dsn = NULL;
	GdaDataModel *model;

	priv = g_object_get_data (G_OBJECT (dsn), DSN_CONFIG_DATA);

	sel = gtk_tree_view_get_selection (GTK_TREE_VIEW (priv->dsn_list));
	sel_rows = gtk_tree_selection_get_selected_rows (sel, NULL);
	model = gnome_db_data_widget_get_gda_model (GNOME_DB_DATA_WIDGET (priv->dsn_list));
	g_assert (GDA_IS_DATA_MODEL (model));

	/* make a list of DSN to remove */
	for (list = sel_rows; list; list = list->next) {
		gchar *dsn;
		gint *row;
		const GValue *cvalue;
		
		row = gtk_tree_path_get_indices ((GtkTreePath *) list->data);
		cvalue = gda_data_model_get_value_at (model, 0, *row, NULL);
		if (cvalue) {
			dsn = gda_value_stringify ((GValue *) cvalue);
			sel_dsn = g_list_prepend (sel_dsn, dsn);
		}
		gtk_tree_path_free ((GtkTreePath *) list->data);
	}
	g_list_free (sel_rows);

	/* actually remove the DSN listed */
	for (list = sel_dsn; list; list = list->next) {
		gchar *str, *dsn;
		GtkWidget *dialog;
		dsn = (gchar *) list->data;

		str = g_strdup_printf (_("Are you sure you want to remove the data source '%s'?"), dsn);
		dialog = gtk_message_dialog_new_with_markup (GTK_WINDOW (gtk_widget_get_toplevel (priv->dsn_list)),
							     GTK_DIALOG_MODAL, GTK_MESSAGE_QUESTION,
							     GTK_BUTTONS_YES_NO, 
							     "<b>%s:</b>\n\n%s", _("Data source removal confirmation"),
							     str);
		g_free (str);
		gtk_dialog_set_default_response (GTK_DIALOG (dialog), GTK_RESPONSE_NO);
		gtk_widget_show (dialog);
		
		if (gtk_dialog_run (GTK_DIALOG (dialog)) == GTK_RESPONSE_YES) 
			gda_config_remove_dsn (dsn, NULL);
		gtk_widget_destroy (dialog);
		
		g_free (dsn);
	}
}
