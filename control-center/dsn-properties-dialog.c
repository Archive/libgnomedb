/* GNOME-DB Components
 * Copyright (C) 2000 - 2008 The GNOME Foundation.
 *
 * AUTHORS:
 *      Rodrigo Moya <rodrigo@gnome-db.org>
 *      Vivien Malerba <malerba@gnome-db.org>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Library General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */

#include <glib/gi18n-lib.h>
#include <gtk/gtkdialog.h>
#include <gtk/gtklabel.h>
#include <gtk/gtknotebook.h>
#include <gtk/gtkstock.h>
#include <gtk/gtktable.h>
#include <libgnomedb/libgnomedb.h>
#include "dsn-properties-dialog.h"
#include <libgnomedb/binreloc/gnome-db-binreloc.h>
#include <string.h>

enum {
	REVERT_BUTTON,
	TEST_BUTTON,
	BROWSE_BUTTON
};

static gboolean
data_source_info_equal (const GdaDsnInfo *info1, const GdaDsnInfo *info2)
{
	#define str_is_equal(x,y) (((x) && (y) && !strcmp ((x),(y))) || (!(x) && (!y)))
	 if (!info1 && !info2)
                return TRUE;
        if (!info1 || !info2)
                return FALSE;

        if (! str_is_equal (info1->name, info2->name))
                return FALSE;
        if (! str_is_equal (info1->provider, info2->provider))
                return FALSE;
        if (! str_is_equal (info1->cnc_string, info2->cnc_string))
                return FALSE;
        if (! str_is_equal (info1->description, info2->description))
                return FALSE;
        if (! str_is_equal (info1->auth_string, info2->auth_string))
                return FALSE;
        return info1->is_system == info2->is_system;
}

static void
dsn_changed_cb (GnomeDbDsnEditor *config, gpointer user_data)
{
	gboolean changed;
	const GdaDsnInfo *dsn_info , *old_dsn_info;

	dsn_info = gnome_db_dsn_editor_get_dsn (config);
	old_dsn_info = g_object_get_data (G_OBJECT (user_data), "old_dsn_info");

	changed = !data_source_info_equal (dsn_info, old_dsn_info);
	gtk_dialog_set_response_sensitive (GTK_DIALOG (user_data), REVERT_BUTTON, changed);

	gda_config_define_dsn (dsn_info, NULL);
}

GdaConnection *
do_connect_dialog_for_dsn (const gchar *dsn)
{
	GtkWidget *dialog;
	gchar *title;
	GdaConnection *cnc = NULL;

	title = g_strdup_printf (_("Login for %s"), dsn);
	dialog = gnome_db_login_dialog_new (title, NULL);
	GnomeDbLogin* login = gnome_db_login_dialog_get_login_widget (GNOME_DB_LOGIN_DIALOG (dialog));
	gnome_db_login_set_dsn (login, dsn);

	if (gnome_db_login_dialog_run (GNOME_DB_LOGIN_DIALOG (dialog))) 
		cnc = gda_connection_open_from_dsn (gnome_db_login_get_dsn (login),
						    gnome_db_login_get_auth (login),
						    GDA_CONNECTION_OPTIONS_NONE, NULL);

	gtk_widget_destroy (dialog);

	return cnc;
}

static void 
data_source_info_free (GdaDsnInfo *info)
{
	g_free (info->provider); 
	g_free (info->cnc_string); 
	g_free (info->description);
	g_free (info->auth_string);
	g_free (info);
}

void
dsn_properties_dialog (GtkWindow *parent, const gchar *dsn)
{
	GdaDsnInfo *dsn_info, *priv_dsn_info;
	GtkWidget *dialog, *props, *label, *hbox;
	GdkPixbuf *icon;
	gchar *str;
	gint result;

	dsn_info = gda_config_get_dsn_info (dsn);
	if (!dsn_info)
		return;

	/* create the dialog */
	dialog = gtk_dialog_new_with_buttons (_("Data Source Properties"),
					      parent, GTK_DIALOG_DESTROY_WITH_PARENT,
					      _("Test"), TEST_BUTTON,
					      _("Browse"), BROWSE_BUTTON,
					      GTK_STOCK_REVERT_TO_SAVED, REVERT_BUTTON,
					      GTK_STOCK_CLOSE, GTK_RESPONSE_OK,
					      NULL);
	gtk_dialog_set_has_separator (GTK_DIALOG (dialog), FALSE);
	gtk_dialog_set_response_sensitive (GTK_DIALOG (dialog), REVERT_BUTTON, FALSE);
	priv_dsn_info = g_new0 (GdaDsnInfo, 1);
	priv_dsn_info->name = g_strdup (dsn_info->name);
	if (dsn_info->provider)
		priv_dsn_info->provider = g_strdup (dsn_info->provider);
	if (dsn_info->cnc_string)
		priv_dsn_info->cnc_string = g_strdup (dsn_info->cnc_string);
	if (dsn_info->description)
		priv_dsn_info->description = g_strdup (dsn_info->description);
	if (dsn_info->auth_string)
		priv_dsn_info->auth_string = g_strdup (dsn_info->auth_string);
	priv_dsn_info->is_system = dsn_info->is_system;
	
	g_object_set_data_full (G_OBJECT (dialog), "old_dsn_info", priv_dsn_info, (GDestroyNotify) data_source_info_free);

	str = gnome_db_gbr_get_icon_path ("gnome-db.png");
	icon = gdk_pixbuf_new_from_file (str, NULL);
	g_free (str);
	if (icon) {
		gtk_window_set_icon (GTK_WINDOW (dialog), icon);
		g_object_unref (icon);
	}

	/* textual explanation */
	if (!dsn_info->is_system || gda_config_can_modify_system_config ())
		str = g_strdup_printf ("<b>%s:</b>\n<small>%s</small>",
				       _("Data Source Properties"),
				       _("Change the data source properties (the name can't be modified)."));
	else
		str = g_strdup_printf ("<b>%s:</b>\n<small>%s</small>",
				       _("Data Source Properties"),
				       _("For information only, this data source is a system wide data source\n"
					 "and you don't have the permission change it."));

	label = gtk_label_new ("");
        gtk_label_set_markup (GTK_LABEL (label), str);
        gtk_misc_set_alignment (GTK_MISC (label), 0., -1);
        g_free (str);
	gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), label, FALSE, FALSE, 0);
	gtk_widget_show (label);

	hbox = gtk_hbox_new (FALSE, 0); /* HIG */
        gtk_box_pack_start (GTK_BOX (GTK_DIALOG (dialog)->vbox), hbox, FALSE, FALSE, 10);
        gtk_widget_show (hbox);
        label = gtk_label_new ("    ");
        gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
        gtk_widget_show (label);

	/* create data source settings page */
	props = gnome_db_dsn_editor_new ();
	gnome_db_dsn_editor_set_dsn (GNOME_DB_DSN_EDITOR (props), dsn_info);
	gtk_widget_show (props);
	g_signal_connect (G_OBJECT (props), "changed", G_CALLBACK (dsn_changed_cb), dialog);	
	gtk_box_pack_start (GTK_BOX (hbox), props, TRUE, TRUE, 0);
	gtk_widget_set_size_request (props, 500, -1);

	/* run the dialog */
	do {
		result = gtk_dialog_run (GTK_DIALOG (dialog));
		switch (result) {
		case REVERT_BUTTON:
			/* reverting changes... */
			gnome_db_dsn_editor_set_dsn (GNOME_DB_DSN_EDITOR (props), priv_dsn_info);
			gtk_dialog_set_response_sensitive (GTK_DIALOG (dialog), REVERT_BUTTON, FALSE);
			break;
		case TEST_BUTTON:
		{
			GtkWidget *dialog = NULL;
			GnomeDbLogin* login = NULL;
			gchar *title;
			GdaConnection *cnc = NULL;
			gboolean auth_needed = gda_config_dsn_needs_authentication (dsn);

			if (auth_needed) {
				title = g_strdup_printf (_("Login for %s"), dsn);
				dialog = gnome_db_login_dialog_new (title, parent);
				g_free (title);
				login = gnome_db_login_dialog_get_login_widget (GNOME_DB_LOGIN_DIALOG (dialog));
				gnome_db_login_set_dsn (login, dsn);
				gnome_db_login_set_show_dsn_selector (login, FALSE);
			}

			if (!auth_needed || gnome_db_login_dialog_run (GNOME_DB_LOGIN_DIALOG (dialog))) {
				GtkWidget *msgdialog;
				GError *error = NULL;

				cnc = gda_connection_open_from_dsn (login ? gnome_db_login_get_dsn (login) : dsn,
								    login ? gnome_db_login_get_auth (login) : NULL,
								    GDA_CONNECTION_OPTIONS_NONE, &error);
				if (cnc) {
					str = g_strdup_printf ("<b>%s</b>", _("Connection successfully opened!"));
					msgdialog = gtk_message_dialog_new_with_markup (dialog ? GTK_WINDOW (dialog) : parent, 
											GTK_DIALOG_MODAL,
											GTK_MESSAGE_INFO, GTK_BUTTONS_OK, str);
					g_free (str);
					gda_connection_close (cnc);
				}
				else {
					if (error) {
						str = g_strdup_printf ("<b>%s:</b>\n%s", _("Could not open connection"), 
								       error->message ? error->message : _("Unknown error"));
						g_error_free (error);
					}
					else
						str = g_strdup_printf ("<b>%s</b>", _("Could not open connection"));
					msgdialog = gtk_message_dialog_new_with_markup (dialog ? GTK_WINDOW (dialog) : parent, 
											GTK_DIALOG_MODAL,
											GTK_MESSAGE_ERROR, GTK_BUTTONS_OK, str);
					g_free (str);
				}
				
				gtk_dialog_run (GTK_DIALOG (msgdialog));
				gtk_widget_destroy (msgdialog);
			}
			if (dialog)
				gtk_widget_destroy (dialog);
		}
		break;
		case BROWSE_BUTTON:
		{
			char *argv[3];
			gboolean sresult;
			
			/* run gnome-database-properties dictig tool */
			argv[0] = (char *) "gnome-db-browser";
			argv[1] = (char *) dsn;
			argv[2] = NULL;
			
			sresult = g_spawn_async (NULL, argv, NULL, G_SPAWN_SEARCH_PATH,
						NULL, NULL, NULL, NULL);
			if (!sresult) {
				GtkWidget *msgdialog;
				str = g_strdup_printf ("<b>%s</b>", _("Could not execute browser program (gnome-db-browser)."));
				msgdialog = gtk_message_dialog_new_with_markup (GTK_WINDOW (dialog), GTK_DIALOG_MODAL,
										GTK_MESSAGE_ERROR, GTK_BUTTONS_OK, str);
				g_free (str);
				gtk_dialog_run (GTK_DIALOG (msgdialog));
				gtk_widget_destroy (msgdialog);
			}
			else
				result = GTK_RESPONSE_OK; /* force closing of this dialog */
		}
		break;
		default:
			break;
		}
	}
	while (result != GTK_RESPONSE_OK);
	gtk_widget_destroy (dialog);
}
